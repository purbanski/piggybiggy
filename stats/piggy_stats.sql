-- phpMyAdmin SQL Dump
-- version 2.8.0.1
-- http://www.phpmyadmin.net
-- 
-- Host: custsql-ipg12.eigbox.net
-- Generation Time: Feb 03, 2013 at 04:42 PM
-- Server version: 5.0.91
-- PHP Version: 4.4.9
-- 
-- Database: `piggy_stats`
-- 

-- --------------------------------------------------------

-- 
-- Table structure for table `pgflow`
-- 

DROP TABLE IF EXISTS `pgflow`;
CREATE TABLE `pgflow` (
  `id` bigint(20) NOT NULL auto_increment,
  `sessionId` bigint(20) NOT NULL,
  `message` varchar(64) character set utf8 collate utf8_bin NOT NULL,
  `timestamp` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=12055 DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci AUTO_INCREMENT=12055 ;

-- 
-- Dumping data for table `pgflow`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `pgsession`
-- 

DROP TABLE IF EXISTS `pgsession`;
CREATE TABLE `pgsession` (
  `id` bigint(20) unsigned NOT NULL auto_increment,
  `userId` bigint(20) unsigned NOT NULL,
  `timestamp` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `ip` varchar(16) collate utf8_polish_ci NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=245 DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci AUTO_INCREMENT=245 ;

-- 
-- Dumping data for table `pgsession`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `pguser`
-- 

DROP TABLE IF EXISTS `pguser`;
CREATE TABLE `pguser` (
  `id` bigint(20) unsigned NOT NULL auto_increment,
  `user` varchar(40) collate utf8_polish_ci NOT NULL,
  `timestamp` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `ip` varchar(16) collate utf8_polish_ci NOT NULL,
  `nickname` varchar(32) collate utf8_polish_ci default NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `user_2` (`user`)
) ENGINE=MyISAM AUTO_INCREMENT=20 DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci AUTO_INCREMENT=20 ;

-- 
-- Dumping data for table `pguser`
-- 

