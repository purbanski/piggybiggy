<?php

require '../config.php';

$con = mysql_connect( $cfg['DBServer'], $cfg['DBUsername'], $cfg['DBPassword'] );
if (!$con)
{
  die('Could not connect: ' . mysql_error());
}

$session_id = $_GET[session_id];

//-----------------------
// Set user id
$sql = "
SELECT *
FROM `$dbname`.`pgflow`
WHERE `$dbname`.`pgflow`.`sessionId` = $session_id
ORDER BY `$dbname`.`pgflow`.`id` desc;";

$result = mysql_query( $sql, $con );

echo "<table border='0' cellpadding='0' cellspacing='0'>\n";
echo "<tr>\n";
echo "<th>ID</th>\n";
echo "<th>Timestamp</th>\n";
echo "<th>Msg</th>\n";
echo "</tr>\n";

while( $row = mysql_fetch_array( $result ))
{
	$value = "";
	
	if ( isset( $row['valueInt1'] ) && $row['valueInt1'] != 0 )
		$value = $row['valueInt1'];
	else if ( isset( $row['valueStr1']))
		$value = $row['valueStr1'];
	
	echo "<tr>\n";
	echo "<td>" . $row['timestamp'] . "</td>\n";
	echo "<td>" . $row['message'] . "</td>\n";
	echo "<td>" . $value . "</td>\n";
	echo "</tr>\n";
}

echo "</table>\n";

mysql_close($con);

?>
