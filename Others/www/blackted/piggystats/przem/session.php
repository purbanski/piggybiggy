<?php

require '../config.php';

$con = mysql_connect( $cfg['DBServer'], $cfg['DBUsername'], $cfg['DBPassword'] );
if (!$con)
{
  die('Could not connect: ' . mysql_error());
}

$user = $_GET[u];

//-----------------------
// Set user id
$sql = "
SELECT
`$dbname`.`pgsession`.`id` as session_id, 
`$dbname`.`pgsession`.`timestamp`,
count(*) as step_count
FROM `$dbname`.`pgsession`
INNER JOIN `$dbname`.`pgflow`
ON `$dbname`.`pgsession`.`id` = `$dbname`.`pgflow`.`sessionId`
WHERE userid=$user
GROUP BY `$dbname`.`pgflow`.`sessionId`
ORDER BY `$dbanem`.`pgsession`.`timestamp` desc; ";

$result = mysql_query( $sql, $con );

// $row = mysql_fetch_row($result);

echo "<table id=\"mytable\" border='0' cellpadding='0' cellspacing='0'>\n";
echo "<tr>\n";
echo "<th>Session ID</th>\n";
echo "<th>Last Timestamp</th>\n";
echo "<th>Step count</th>\n";
echo "<th>View flow</th>\n";
echo "</tr>\n";

while( $row = mysql_fetch_array( $result ))
{
	$flow_link = "<a href='?page=flow&session_id=". $row['session_id'] . "'>flow</a>";
	
	echo "<tr>\n";
	echo "<td>" . $row['session_id'] . "</td>\n";
	echo "<td>" . $row['timestamp'] . "</td>\n";
	echo "<td align='middle'>" . $row['step_count'] . "</td>\n";
	echo "<td align='middle'>" . $flow_link . "</td>\n";
	echo "</tr>\n";
}

echo "</table>\n";

mysql_close($con);

?>
