<?php

require '../config.php';

$con = mysql_connect( $cfg['DBServer'], $cfg['DBUsername'], $cfg['DBPassword'] );
if (!$con)
{
  die('Could not connect: ' . mysql_error());
}

if ( isset ( $_GET['limit' ] ))
	$limit = $_GET['limit' ];
else
	$limit = 22;
//-----------------------
// Set user id
$sql = "
SELECT `$dbname`.`pgflow`.`message` as message, 
count(*) as event_count
FROM `$dbname`.`pgflow`
WHERE `$dbname`.`pgflow`.`message` LIKE 'LEVEL_COMPLETED_%'
GROUP BY `$dbname`.`pgflow`.`message`
ORDER BY `$dbanem`.`pgflow`.`message` asc
LIMIT " . $limit . "; ";
// echo $sql;
$result = mysql_query( $sql, $con );

// $row = mysql_fetch_row($result);

echo "<table id=\"mytable\" border='0' cellpadding='0' cellspacing='0'>\n";
echo "<tr>\n";
echo "<th>Level</th>\n";
echo "<th>Events</th>\n";
echo "<th>Users</th>\n";
echo "<th>Level<br/>failed</th>\n";
echo "<th>Level<br/>skipped</th>\n";
echo "<th>Level<br/>restart</th>\n";
echo "</tr>\n";

while( $row = mysql_fetch_array( $result ))
{
	
	$keywords = preg_split("/_/", $row['message'] );
	$levelNr = $keywords[2];
	
	$sqlUser = "
	SELECT *, count(*) as user_count
	FROM `$dbname`.`pgflow`
	INNER JOIN `$dbname`.`pgsession`
	ON `$dbname`.`pgflow`.`sessionId` = `$dbname`.`pgsession`.`id`
	WHERE `$dbname`.`pgflow`.`message` LIKE '" . $row['message'] . "'";
	
	//echo $sqlUser;
	
	$resultUser = mysql_query( $sqlUser, $con );
	$rowUser = mysql_fetch_array( $resultUser );
	
	if ( !strncmp($levelNr, "0",1) )
	{
		$levelNr = substr( $levelNr, 1, 1);	
	}
	$sqlFailed = "
	SELECT *, count(*) as user_count
	FROM `$dbname`.`pgflow`
	INNER JOIN `$dbname`.`pgsession`
	ON `$dbname`.`pgflow`.`sessionId` = `$dbname`.`pgsession`.`id`
	WHERE `$dbname`.`pgflow`.`message` LIKE 'LEVEL_FAILED_" . $levelNr . "_%'";
	
	//echo $sqlUser;
	
	$resultFailed = mysql_query( $sqlFailed, $con );
	$rowFailed = mysql_fetch_array( $resultFailed );
	
	$sqlSkipped = "
	SELECT *, count(*) as user_count
	FROM `$dbname`.`pgflow`
	INNER JOIN `$dbname`.`pgsession`
	ON `$dbname`.`pgflow`.`sessionId` = `$dbname`.`pgsession`.`id`
	WHERE `$dbname`.`pgflow`.`message` LIKE 'LEVEL_SKIP_" . $levelNr . "_%'";
	
	
	$resultSkipped = mysql_query( $sqlSkipped, $con );
	$rowSkipped = mysql_fetch_array( $resultSkipped );
	

	$resultFailed = mysql_query( $sqlFailed, $con );
	$rowFailed = mysql_fetch_array( $resultFailed );
	
	$sqlRestart = "
	SELECT *, count(*) as user_count
	FROM `$dbname`.`pgflow`
	INNER JOIN `$dbname`.`pgsession`
	ON `$dbname`.`pgflow`.`sessionId` = `$dbname`.`pgsession`.`id`
	WHERE `$dbname`.`pgflow`.`message` LIKE 'LEVEL_RESTART_" . $levelNr . "_%'";
	
	
	$resultRestart = mysql_query( $sqlRestart, $con );
	$rowRestart = mysql_fetch_array( $resultRestart );
	
	echo "<tr>\n";
	echo "<td>" . $levelNr . "</td>\n";
	echo "<td>" . $row['event_count'] . "</td>\n";
	echo "<td>" . $rowUser['user_count'] . "</td>\n";
	echo "<td>" . $rowFailed['user_count'] . "</td>\n";
	echo "<td>" . $rowSkipped['user_count'] . "</td>\n";
	echo "<td>" . $rowRestart['user_count'] . "</td>\n";
	echo "</tr>\n";
}

echo "</table>\n";

mysql_close($con);

?>
