/****************************************************************************
 Copyright (c) 2010 cocos2d-x.org
 
 http://www.cocos2d-x.org
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 ****************************************************************************/

#import <CoreMotion/CoreMotion.h>
#import "AccelerometerDelegateWrapper.h"
#include "MyDebug.h"

@interface AccelerometerDispatcher()

@property (strong) CMMotionManager *motionManager;
@property (strong) NSOperationQueue *deviceQueue;

@end

@implementation AccelerometerDispatcher

static AccelerometerDispatcher* s_pAccelerometerDispatcher;

@synthesize delegate_;
@synthesize acceleration_;

+ (id) sharedAccelerometerDispather
{
    if (s_pAccelerometerDispatcher == nil) {
        s_pAccelerometerDispatcher = [[self alloc] init];
    }
    
    return s_pAccelerometerDispatcher;
}

- (id) init
{
    acceleration_ = new cocos2d::CCAcceleration();
    return self;
}

- (void) dealloc
{
    s_pAccelerometerDispatcher = 0;
    delegate_ = 0;
    delete acceleration_;
    [self releaseMotion];
    [super dealloc];
}

- (void) addDelegate: (cocos2d::CCAccelerometerDelegate *) delegate
{
    delegate_ = delegate;
    
    if (delegate_)
    {
        [self setupMotion];
    }
    else 
    {
        [self releaseMotion];
    }
}

-(void) setupMotion {
    if (self.deviceQueue == nil)
        self.deviceQueue = [[NSOperationQueue alloc] init];
    
    if (self.motionManager == nil)
    {
        self.motionManager = [[CMMotionManager alloc]init];
        self.motionManager.deviceMotionUpdateInterval = 5.0 / 60.0;
        
        [self.motionManager startDeviceMotionUpdatesUsingReferenceFrame:CMAttitudeReferenceFrameXArbitraryZVertical
                                                                toQueue:self.deviceQueue
                                                            withHandler:^(CMDeviceMotion *motion, NSError *error)
         {
             [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                 
                 CGFloat x = motion.gravity.x;
                 CGFloat y = motion.gravity.y;
                 CGFloat z = motion.gravity.z;
                 NSTimeInterval timestamp;
                 timestamp = [[NSDate date]timeIntervalSince1970];
                 
                 [self doDidAccelerate:x y:y z:z timestamp:timestamp];
             }];
         }];
    }
}

-(void) releaseMotion {
    if (self.deviceQueue)
    {
        [self.deviceQueue release];
        self.deviceQueue = nil;
    }
    
    if (self.motionManager)
    {
        [self.motionManager release];
        self.motionManager = nil;
    }
}

-(void)doDidAccelerate:(float)x y:(float)y z:(float)z timestamp:(NSTimeInterval)timestamp
{
    if (! delegate_)
        return;
    
    acceleration_->x = x;
    acceleration_->y = y;
    acceleration_->z = z;
    acceleration_->timestamp = timestamp;
    
    double tmp = acceleration_->x;
    
    switch ([[UIApplication sharedApplication] statusBarOrientation]) 
    {
    case UIInterfaceOrientationLandscapeRight:
        acceleration_->x = -acceleration_->y;
        acceleration_->y = tmp;
        break;
        
    case UIInterfaceOrientationLandscapeLeft:
        acceleration_->x = acceleration_->y;
        acceleration_->y = -tmp;
        break;
        
    case UIInterfaceOrientationPortraitUpsideDown:
        acceleration_->x = -acceleration_->y;
        acceleration_->y = -tmp;
        break;
            
    case UIInterfaceOrientationPortrait:
        break;
    }
    
    delegate_->didAccelerate(acceleration_);
}

@end

