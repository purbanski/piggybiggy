#ifndef __NODESMANAGER_H__
#define __NODESMANAGER_H__

#include <list>
#include "cocos2d.h"
#include "pu/ui/Setter.h"
//#include "Editor/ui/Setters.h"

using namespace std; 
USING_NS_CC;

//---------------------------------------------------------------------
class NodeExt
{
public:
	typedef enum
	{
		eNodeType_Setter = 1,
		eNodeType_SetterWithObject,
		eNodeType_Other
	} NodeType;

	NodeExt( CCNode *node, NodeType type = eNodeType_Other )
	{
		_node = node;
		_nodeType = type;
	}

public:
	CCNode		*_node;
	NodeType	_nodeType;
};
//---------------------------------------------------------------------
class SettersManager : public list<NodeExt>
{
public:
	SettersManager();
	virtual ~SettersManager();
	void AddNodesToParent( CCNode * );
	void AllignNodesVerticallyWithPadding( float startX, float startY, float padding = 70.0f);
	void RetainNodes();
	void ReleaseNodes();
	void SetObjects( std::vector<void *> *objects );
	void UpdateDisplay();
};
//---------------------------------------------------------------------
class BlockSettersManager : public SettersManager
{
public:
	void SetSetters( BlockContainer *blocks );
	void UpdateSetters();
};

//---------------------------------------------------------------------

#endif
