#include "Roller.h"

//--------------------------------------------------------------------------
// Roller
//--------------------------------------------------------------------------
RollerDot::RollerDot()
{
	_dot = CCSprite::spriteWithFile("Images/Other/RollerDot.png");
	_background = CCSprite::spriteWithFile("Images/Other/RollerDot.png");

	_dot->setPosition( ccp( 0.0f, 0.0f ));
	_background->setPosition( ccp( 0.0f, 0.0f ));

	addChild( _dot, 10 );
	addChild( _background, 5 );
}
//--------------------------------------------------------------------------
RollerDot::~RollerDot()
{

}
//--------------------------------------------------------------------------
bool RollerDot::init()
{
	if ( ! CCLayer::init() )
		return false;

	setIsTouchEnabled( true );
	return true;
}
//--------------------------------------------------------------------------
void RollerDot::registerWithTouchDispatcher()
{
	CCTouchDispatcher::sharedDispatcher()->addTargetedDelegate( this, 250, true );
}
//--------------------------------------------------------------------------
bool RollerDot::ccTouchBegan( CCTouch *touch, CCEvent *pEvent )
{
	return false;
}
//--------------------------------------------------------------------------
void RollerDot::ccTouchMoved( CCTouch *touch, CCEvent *pEvent )
{

}
//--------------------------------------------------------------------------
void RollerDot::ccTouchEnded( CCTouch *touch, CCEvent *pEvent )
{

}



//--------------------------------------------------------------------------
// Roller
//--------------------------------------------------------------------------
Roller::Roller()
{
	/*_dot = new RollerDot();
	addChild( _dot );
	setPosition( ccp( 0.0f, 0.0f ));*/
}
//--------------------------------------------------------------------------
Roller::~Roller()
{

}
