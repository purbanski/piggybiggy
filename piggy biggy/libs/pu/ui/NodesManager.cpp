#include "NodesManager.h"

//--------------------------------------------------------------
// SettersManager
//--------------------------------------------------------------
SettersManager::SettersManager()
{
}
//--------------------------------------------------------------
SettersManager::~SettersManager()
{
}
//--------------------------------------------------------------
void SettersManager::AddNodesToParent( CCNode *parent )
{
	SettersManager::iterator it;
	for ( it = begin(); it != end(); it++ )
		parent->addChild( (*it)._node );
}
//--------------------------------------------------------------
void SettersManager::AllignNodesVerticallyWithPadding( float startX, float startY, float padding )
{
	int count = 0;

	SettersManager::iterator it;
	for ( it = begin(); it != end(); it++ )
	{
		if ( (*it)._nodeType == NodeExt::eNodeType_Setter )
		{
			(*it)._node->setPosition( ccp( startX, startY + count * padding ));
			count++;
		}
	}
}
//--------------------------------------------------------------
void SettersManager::RetainNodes()
{
	iterator it;
	for ( it = begin(); it != end(); it++ )
		(*it)._node->retain();
}
//--------------------------------------------------------------
void SettersManager::ReleaseNodes()
{
	iterator it;
	for ( it = begin(); it != end(); it++ )
		(*it)._node->release();

}
//--------------------------------------------------------------
void SettersManager::SetObjects( std::vector<void *> *objects )
{
	iterator it;
	//SetterExtBase *setter;
	for ( it = begin(); it != end(); it++ )
	{
		if ( (*it)._nodeType == NodeExt::eNodeType_SetterWithObject )
		{
		//	setter = (SetterExtBase *)((*it)._node );
		//	setter->SetObjects( objects );
		}
	}
}
//--------------------------------------------------------------
void SettersManager::UpdateDisplay()
{
	iterator it;
//	SetterExtBase *setter;
	for ( it = begin(); it != end(); it++ )
	{
	//	setter = (SetterExtBase *)((*it)._node );
	//	setter->UpdateDisplay();
	}
}
//--------------------------------------------------------------


//--------------------------------------------------------------

//--------------------------------------------------------------
// BlockSettersManager
//--------------------------------------------------------------
void BlockSettersManager::SetSetters( BlockContainer *blocks )
{
	iterator it;
	for ( it = begin(); it != end(); it++ )
	{
	//	BlockSetterBase *setter;
	//	setter = ( BlockSetterBase *)(*it)._node;

	//	if ( !setter )
	//		return;

		//fix me
	//	setter->SetObjects(  (std::vector<void *> *)blocks );
	}
}
//--------------------------------------------------------------
void BlockSettersManager::UpdateSetters()
{
	iterator it;
	for ( it = begin(); it != end(); it++ )
	{
	//	BlockSetterBase *setter;
	//	setter = ( BlockSetterBase *)(*it)._node;

	//	if ( !setter )
		//	return;

		//setter->UpdateDisplay();
	}
}
