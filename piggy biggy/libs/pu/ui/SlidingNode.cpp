#include "SlidingNode.h"
#include "Debug/MyDebug.h"
#include "Tools.h"
#include "Game.h"
#include "SoundEngine.h"

const float SlidingNode::_sAcclearteMax = 30.0f;
const float SlidingNode::_sAcclearte = 3.0f;

//-----------------------------------------------------------------------------------
SlidingNode* SlidingNode::node( int count )
{ 
	SlidingNode *pRet = new SlidingNode( count );
	if ( pRet && pRet->init() )
	{ 
		pRet->autorelease();
		return pRet;
	}
	else
	{
		delete pRet;
		pRet = NULL; 
		return NULL;
	}
}
//-----------------------------------------------------------------------------------
SlidingNode::SlidingNode( int count )
{
	_slideOffset = 0;
	_sliding = false;
	_offestLimit = count;
	_slideLength = Config::GameSize.width;

	_slidingNode = CCNode::node();
	CCLayer::addChild( _slidingNode, 5 );

	_slidingIndex = new SlidingNodeIndex( count );
	_slidingIndex->autorelease();
	//_slidingIndex->setPosition( ccp( Tools::GetScreenMiddleX(),  - 140.0f ));
	_slidingIndex->setPosition( ccp( 0.0f, -285.0f ));
	CCLayer::addChild( _slidingIndex, 10 );

	_listener = NULL;
}
//-----------------------------------------------------------------------------------
SlidingNode::~SlidingNode()
{
	removeAllChildrenWithCleanup( true );
}
//-----------------------------------------------------------------------------------
bool SlidingNode::init()
{
	if ( !CCLayer::init()  )
		return false;

	SoundEngine::Get()->LoadSound( SoundEngine::eSoundSlidingNode_Slide );
	SoundEngine::Get()->LoadSound( SoundEngine::eSoundSlidingNode_SlideBack );
	
	setIsTouchEnabled( true );
	return true;
}
//--------------------------------------------------------------------------------------------------
void SlidingNode::registerWithTouchDispatcher()
{
	CCTouchDispatcher::sharedDispatcher()->addTargetedDelegate( this, 5 /*Config::eMousePriority_SelectLevelScreen*/, true );
}
//-----------------------------------------------------------------------------------
bool SlidingNode::ccTouchBegan( CCTouch *pTouch, CCEvent *pEvent )
{
	if ( _sliding )
		return true;

	_startTouch = Tools::ConvertLocation( pTouch );
	_slideStartPosition = _slidingNode->getPosition();

	gettimeofday(&_startTime, NULL);
	//D_LOG("Time %d %d", _startTime.tv_sec, _startTime.tv_usec );
	return true;
}
//-----------------------------------------------------------------------------------
void SlidingNode::ccTouchMoved( CCTouch *pTouch, CCEvent *pEvent )
{
	D_HIT
	if ( _sliding )
		return;

	b2Vec2 touch = Tools::ConvertLocation( pTouch );

	CCPoint pos;
	pos.x = _slideStartPosition.x + RATIO * ( touch.x - _startTouch.x );
	pos.y = _slideStartPosition.y;

	_slidingNode->setPosition( pos );
}
//-----------------------------------------------------------------------------------
void SlidingNode::ccTouchEnded( CCTouch *pTouch, CCEvent *pEvent )
{
	D_HIT
	if ( _sliding )
		return;

	// Calculate distance 
	b2Vec2 touch = Tools::ConvertLocation( pTouch );
	float deltaX = abs( RATIO * ( touch.x - _startTouch.x ));

	// Leave sliding note at last postition
	CCPoint pos;
	pos.x = _slideStartPosition.x + RATIO * ( touch.x - _startTouch.x );
	pos.y = _slideStartPosition.y;
	_slidingNode->setPosition( pos );

	// Calculate time diffrence
	timeval tv;
	gettimeofday( &tv, NULL );

	int timeDiff;
	timeDiff = ( tv.tv_sec - _startTime.tv_sec ) * 1000000 ;
	timeDiff += ( tv.tv_usec - _startTime.tv_usec );
	D_LOG("Time diff %d distanc %f",  timeDiff, deltaX );

	_acelerate = 2.0f;
	_sliding = true;
	
	if ( ( touch.x - _startTouch.x ) > 0 )
		_slideDirection = 1;
	else
		_slideDirection = -1;

	// Slide
	if (( timeDiff < 350000 && deltaX > 45.0f ) || deltaX > 180.0f )
	{

		// check if it is not the left or right end of level select menu
		if (( _slideOffset == 0 && _slideDirection == 1 ) || ( abs( _slideOffset ) == _offestLimit -1 && _slideDirection == -1 ))
		{
			_slideDirection *= -1;
			schedule( schedule_selector( SlidingNode::SlideBack ));
			SoundEngine::Get()->PlayEffect( SoundEngine::eSoundSlidingNode_SlideBack );
		}
		else
		{
			schedule( schedule_selector( SlidingNode::Slide ));
			SoundEngine::Get()->PlayEffect( SoundEngine::eSoundSlidingNode_Slide );
		}
	}
	// Slide back
	else
	{
		_slideDirection *= -1;
		schedule( schedule_selector( SlidingNode::SlideBack ));
		if ( deltaX >= 15.0f ) 
			SoundEngine::Get()->PlayEffect( SoundEngine::eSoundSlidingNode_SlideBack );
	}
}
//-----------------------------------------------------------------------------------
void SlidingNode::Slide( cocos2d::ccTime dt )
{
	// Set new slide position
	CCPoint pos;
	pos = GetNewPosition();

	// Prepare new bound to be cross checked with new position 
	Range range;
	range = GetBoundRange( eModeSlide );

	// Check if new slide position is in bound
	if ( range.first < pos.x  && pos.x < range.second )
		_slidingNode->setPosition( pos );
	else
	{	
		_slideOffset += (  _slideDirection * 1 );
		_slidingIndex->SetActiveIndex( abs( _slideOffset ));

		_slidingNode->setPosition( ccp(_slideOffset * _slideLength + _initPosition.x, pos.y ));
		_sliding = false;
		unschedule( schedule_selector( SlidingNode::Slide ));
		if ( _listener )
			_listener->SlidingNode_SlideFinish();
	}
}
//-----------------------------------------------------------------------------------
void SlidingNode::SlideBack( cocos2d::ccTime dt )
{
	// Set new slide position
	CCPoint pos;
	Range bound;

	pos = GetNewPosition();
	bound = GetBoundRange( eModeSlideBack );

	if ( bound.first < pos.x  && pos.x < bound.second )
		_slidingNode->setPosition( pos );
	else
	{	
		_slidingNode->setPosition( ccp(_slideOffset * _slideLength + _initPosition.x, pos.y ));
		_sliding = false;
		unschedule( schedule_selector( SlidingNode::SlideBack ));
		if ( _listener )
			_listener->SlidingNode_SlideFinish();
	}
}
//-----------------------------------------------------------------------------------
SlidingNode::Range SlidingNode::GetBoundRange( SlideMode mode )
{
	// Prepare new bound to be cross checked with new position 
	float a,b;
	float atemp;
	float btemp;

	if ( mode == eModeSlide )
		atemp = ( _slideOffset + (  _slideDirection * 1 )) * _slideLength + _initPosition.x;
	else
		atemp = ( _slideOffset - (  _slideDirection * 1 )) * _slideLength + _initPosition.x;

	btemp = _slideOffset * _slideLength + _initPosition.x;

	if ( atemp < btemp )
	{
		a = atemp;
		b = btemp;
	}
	else
	{
		a = btemp;
		b = atemp;
	}
	Range range;
	range.first = a;
	range.second = b;

	return range;
}
//-----------------------------------------------------------------------------------
CCPoint SlidingNode::GetNewPosition()
{
	CCPoint pos;
	pos = _slidingNode->getPosition();
	pos.x += _slideDirection * _acelerate;

	if ( _acelerate < _sAcclearteMax )
		_acelerate *= _sAcclearte;

	return pos;
}
//-----------------------------------------------------------------------------------
void SlidingNode::SetInitPosition( CCPoint point )
{
	_initPosition = point;
	_slidingNode->setPosition( point );
}
//-----------------------------------------------------------------------------------
void SlidingNode::addChild( CCNode * child )
{
	_slidingNode->addChild( child );
}
//-----------------------------------------------------------------------------------
void SlidingNode::addChild( CCNode * child, int zOrder )
{
	_slidingNode->addChild( child, zOrder );
}
//-----------------------------------------------------------------------------------
void SlidingNode::SetIndex( int index )
{
	_slideOffset = -abs(index);
	_slidingIndex->SetActiveIndex( abs( _slideOffset ));
	D_LOG("SlideOffset %d" , _slideOffset );
	//480x320 specific
	_slidingNode->setPosition( ccp(_slideOffset * _slideLength + _initPosition.x, _slidingNode->getPosition().y ));
	_sliding = false;
}
//-----------------------------------------------------------------------------------
int SlidingNode::GetIndex()
{
	return abs( _slideOffset );
}
//-----------------------------------------------------------------------------------
void SlidingNode::removeFromTouchDispatcher()
{
	CCTouchDispatcher::sharedDispatcher()->removeDelegate( this );
}
//-----------------------------------------------------------------------------------
void SlidingNode::onExit()
{
	removeFromTouchDispatcher();
}
//-----------------------------------------------------------------------------------
void SlidingNode::SetListener( SlidingNodeListener *listener )
{
	_listener = listener;
}
//-----------------------------------------------------------------------------------
void SlidingNode::SetSlideLength( float len )
{
	_slideLength = len;
}

//-----------------------------------------------------------------------------------





//-----------------------------------------------------------------------------------
// SlidingNodeIndex
//-----------------------------------------------------------------------------------
float SlidingNodeIndex::_xDelta = 30.0f;

SlidingNodeIndex::SlidingNodeIndex( int count )
{
	_count = count;
	CCSprite *sprite;

	for ( float i = -( _count / 2.0f ); i < ( _count / 2.0f ) ; i++ )
	{
		sprite = CCSprite::spriteWithFile( Skins::GetSkinName( "Images/Other/SlidingNodeDot.png"));
		sprite->setPosition( ccp( (i + 1) * _xDelta, 0.0f ));
		addChild( sprite, 5 );
	}
	_selectedDot = CCSprite::spriteWithFile( Skins::GetSkinName( "Images/Other/SlidingNodeDotSelected.png" ));
	SetActiveIndex( 0 );
	addChild( _selectedDot, 10 );
}
//-----------------------------------------------------------------------------------
SlidingNodeIndex::~SlidingNodeIndex()
{
	removeAllChildrenWithCleanup( true );
}
//-----------------------------------------------------------------------------------
void SlidingNodeIndex::SetActiveIndex( int index )
{
	_selectedDot->setPosition( ccp ( ( (float) ( index + 1 ) - _count / 2.0f ) * _xDelta , 0.0f ));
}






//-----------------------------------------------------------------------------------
// SlidingNodeMenu
//-----------------------------------------------------------------------------------
SlidingNodeMenu * SlidingNodeMenu::menuWithItems( int mousePiority, CCLayer *parent, CCMenuItem* item, ...)
{
	va_list args;
	va_start(args,item);

	SlidingNodeMenu *pRet = new SlidingNodeMenu();
	if (pRet && pRet->initWithItems(mousePiority, item, args))
	{
		pRet->autorelease();
		va_end(args);

		pRet->_parent = parent;
		pRet->_activated = false;
		return pRet;
	}
	va_end(args);
	CC_SAFE_DELETE(pRet)
		return NULL;
}
//-----------------------------------------------------------------------------------
void SlidingNodeMenu::ccTouchMoved( CCTouch* touch, CCEvent* event )
{
	_activated = true;
	if (m_pSelectedItem)
	{
		m_pSelectedItem->unselected();
		_parent->ccTouchBegan( touch, event );
		m_pSelectedItem = NULL;
	}
	else
		_parent->ccTouchMoved( touch, event );
}
//-----------------------------------------------------------------------------------
void SlidingNodeMenu::ccTouchEnded( CCTouch* touch, CCEvent* event )
{
	if ( _activated )
	{
		_parent->ccTouchEnded( touch, event );
		_activated = false;
	}
	CCMenu::ccTouchEnded( touch, event );
}
//-----------------------------------------------------------------------------------
void SlidingNodeMenu::registerWithTouchDispatcher()
{
    CCTouchDispatcher::sharedDispatcher()->addTargetedDelegate( this, _mouseTouchPiority, false );
}
//-----------------------------------------------------------------------------------
