#ifndef __SLIDINGNODE_H__
#define __SLIDINGNODE_H__

#include "cocos2d.h"
#include <Box2D/Box2D.h>

//------------------------------------------------------------------------------------------
using namespace std;
USING_NS_CC;
//------------------------------------------------------------------------------------------
class SlidingNodeIndex : public CCNode
{
public:
	SlidingNodeIndex( int count );
	~SlidingNodeIndex();

	void SetActiveIndex( int count );

private:
	static float _xDelta;
	CCSprite	*_selectedDot;
	int			_count;
};
//------------------------------------------------------------------------------------------
class SlidingNodeListener
{
public:
	virtual void SlidingNode_SlideFinish() = 0;
};
//------------------------------------------------------------------------------------------
class SlidingNode : public CCLayer
{
public:
	~SlidingNode();

	static SlidingNode* node( int count );
	virtual bool init();
	virtual void onExit();
	void SetSlideLength( float len );

	virtual bool ccTouchBegan(CCTouch *touch, CCEvent *pEvent);
	virtual void ccTouchMoved(CCTouch *touch, CCEvent *pEvent);
	virtual void ccTouchEnded(CCTouch *touch, CCEvent *pEvent);
	virtual void registerWithTouchDispatcher();

	void SetInitPosition( CCPoint point );

	void SetSlideCount(int limit ) { _offestLimit = limit; }
	void SetIndex( int index );
	int GetIndex();

	void removeFromTouchDispatcher();

	virtual void Slide( cocos2d::ccTime dt );
	virtual void SlideBack( cocos2d::ccTime dt );

	virtual void addChild(CCNode * child );
	virtual void addChild(CCNode * child, int zOrder);

	void SetListener( SlidingNodeListener *listener );

protected:
	static const float _sAcclearte;
	static const float _sAcclearteMax;

	SlidingNode( int count );

private:
	typedef enum
	{
		eModeSlide = 1,
		eModeSlideBack
	} SlideMode;

	typedef pair<float,float> Range;
	Range GetBoundRange( SlideMode mode );
	CCPoint GetNewPosition();

	CCNode				*_slidingNode;
	SlidingNodeIndex	*_slidingIndex;

	int _slideOffset;
	int _offestLimit;

	float	_acelerate;
	bool	_sliding;
	int		_slideDirection;
	b2Vec2	_startTouch;
	CCPoint	_slideStartPosition;
	CCPoint	_initPosition;
	timeval	_startTime;
	float	_slideLength;

	SlidingNodeListener	*_listener;
};



//------------------------------------------------------------------------------------------
// SlidingNodeMenu
// It is not internal part of slidning node
// but very useful to use it with it, for example when we have menu
// with slidingNodeMenu we can return ccTouch events to slidng Node when
// user starts moving away from menuItem
//------------------------------------------------------------------------------------------
class SlidingNodeMenu : public CCMenu
{
public:
	static SlidingNodeMenu* menuWithItems(int mousePiority, CCLayer *parent, CCMenuItem* item, ...);

	virtual void ccTouchMoved(CCTouch* touch, CCEvent* event);
	virtual void ccTouchEnded(CCTouch* touch, CCEvent* event);
	virtual void registerWithTouchDispatcher();
    
private:
	CCLayer *_parent;
	bool	_activated;
};

#endif
