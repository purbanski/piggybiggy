#ifndef __PU_UI_SETTER_H__
#define __PU_UI_SETTER_H__

#include "pu/ui/menus/MenuItemPU.h"
#include <pu/ui/Settings.h>
#include <sstream>
#include "Debug/MyDebug.h"
#include "CommonDefs.h"

using namespace std;


namespace pu
{

namespace setter
{
	typedef enum {
		eUINormal = 1,
		eUIScaled = 10
	} UIScale;

class SetterListener
{
public:
	virtual void Setter_Callback( void* sender ) = 0;
};

//--------------------------------------------------------------------------
//-- UISetter<Policy>
//--------------------------------------------------------------------------
template <class Policy>
class UISetter : public CCNode
{
public:
	static UISetter* Create( const char *name );
	~UISetter(){};

	typename Policy::Type GetValue() { return _policy.GetValue(); }
	void SetValue( typename Policy::Type value );
	void SetListener( SetterListener *list ) { _listener = list; }

protected:
	UISetter( const char *name );
	void AddItems();

	void Plus_Callback( CCObject* pSender );
	void Minus_Callback( CCObject* pSender );
	void Null_Callback( CCObject* pSender ){};
	virtual void UpdateDisplay();
	

protected:
	CCLabelBMFont		*_setterName;
	CCLabelBMFont		*_displayValue;
	CCMenu				*_menu;

	Policy				_policy;
	SetterListener		*_listener;
};
//------------------------------------------------------------------------------------------------
template <class Policy>
UISetter<Policy>::UISetter( const char *name )
{
	CCLabelBMFont *plusLabel		= CCLabelBMFont::labelWithString( "+",  pu::ui::settings::CONTROL_FONT );
	CCLabelBMFont *minusLabel		= CCLabelBMFont::labelWithString( "-",  pu::ui::settings::CONTROL_FONT );
	MenuItemRepeat *plusItem		= MenuItemRepeat::itemWithLabel( plusLabel, this, menu_selector( UISetter::Plus_Callback ));
	MenuItemRepeat *minusItem		= MenuItemRepeat::itemWithLabel( minusLabel, this, menu_selector( UISetter::Minus_Callback ));

	_menu = CCMenu::menuWithItems( minusItem, plusItem, NULL );
	_menu->alignItemsHorizontallyWithPadding( 40.0f );
	_menu->setPosition( ccp( 160.0f, 0.0f ));
	addChild( _menu );

	_displayValue = CCLabelBMFont::labelWithString( "0.00",  pu::ui::settings::DISPLAY_FONT );
	_displayValue->setPosition( ccp( 0.0f, -15.0f ));
	addChild( _displayValue );

	_setterName	= CCLabelBMFont::labelWithString( name,  pu::ui::settings::NAME_FONT );
	_setterName->setPosition( ccp( 0.0f, 10.0f ));
	addChild( _setterName );

	_listener	= NULL;

}
//------------------------------------------------------------------------------------------------
template <class Policy>
UISetter<Policy> *UISetter<Policy>::Create( const char *name )
{
	UISetter<Policy> *setter;

	setter = new UISetter<Policy>( name );
	setter->autorelease();
	setter->setPosition( CCPoint( 0.0f, 0.0f ));
	setter->UpdateDisplay();

	return setter;
}
//------------------------------------------------------------------------------------------------

//----------------------------------------------------------------------------------------------------------------
template <class Policy>
void UISetter<Policy>::SetValue( typename Policy::Type value )
{ 
	_policy.SetValue( value ); 
	UpdateDisplay();

	if ( _listener )
		_listener->Setter_Callback( (void *) this );
}
//----------------------------------------------------------------------------------------------------------------
template <class Policy>
void UISetter<Policy>::Plus_Callback( CCObject* pSender )
{
 	_policy.Plus();
	UpdateDisplay();

	if ( _listener )
		_listener->Setter_Callback( (void *) this );
}
//----------------------------------------------------------------------------------------------------------------
template <class Policy>
void UISetter<Policy>::Minus_Callback( CCObject* pSender )
{
	_policy.Minus();
	UpdateDisplay();

	if ( _listener )
		_listener->Setter_Callback( (void *) this );
}
//----------------------------------------------------------------------------------------------------------------
template <class Policy>
void UISetter<Policy>::UpdateDisplay()
{
	_displayValue->setString( _policy.GetValueDesc().c_str() );
};


//--------------------------------------------------------------------------
//-- Base policies
//--------------------------------------------------------------------------
// Int policy - ready impl
template <int TStep = 1, int TMin = 0, int TMax = 0>
class IntPolicy
{
public:
	typedef int Type;

	IntPolicy()
	{ 
		if ( TMin != TMax ) _value = 0; 
		else _value = TMin;
	}

	virtual void Plus()
	{ 
		_value += TStep; 
		if ( TMin != TMax )
		{
			if ( _value > TMax )
				_value = TMin;
		}

	}

	virtual void Minus()
	{
		_value -= TStep;
		if ( TMin != TMax )
		{
			if ( _value < TMin )
				_value = TMax;
		}
	}

	virtual void SetValue( Type value )		{ _value = value; }
	virtual Type GetValue()					{ return _value;  }

	string GetValueDesc()
	{
		stringstream ss;
		//ss.precision( 5 );
		ss << GetValue();
		return ss.str();
	}
	Type _value;
};


//--------------------------------------------------------------------------
// Float policy - ready impl
//--------------------------------------------------------------------------
template <int TStep = 10, int TMin = 0, int TMax = 0, UIScale TScale = eUINormal>
class FloatPolicy
{
public:
	typedef float Type;

	FloatPolicy()
	{ 
		if ( TMin != TMax ) _value = 0; 
		else _value = (( float ) TMin ) / ((float) TScale );
	}

	virtual void Plus()
	{ 
		_value += 0.01f * TStep; 
		if ( TMin != TMax )
		{
			if ( _value > (  (( float ) TMax )  /  ((float) TScale ) ) ) 
				_value = (( float ) TMin )  /  ((float) TScale );
		}

	}

	virtual void Minus()
	{
		_value -= 0.01f * TStep;
		if ( TMin != TMax )
		{
			if ( _value < (  (( float ) TMin )  /  ((float) TScale ) ) ) 
				_value = (( float ) TMax )  /  ((float) TScale );
		}
	}

	virtual void SetValue( Type value )		{ _value = value; }
	virtual Type GetValue()					{ return _value;  }

	string GetValueDesc()
	{
		stringstream ss;
		ss.precision( 5 );
		ss << GetValue();
		return ss.str();
	}
	Type _value;
};


//--------------------------------------------------------------------------
//-- Base policies - to be derived
//-- Enum, Bool
//--------------------------------------------------------------------------
class UISetterPolicySetBase
{
public:
	virtual ~UISetterPolicySetBase()
	{
		_policyMap.clear();
	}

	void Plus()
	{
		Plus_();
		while ( ! ( _policyMap[ _index ].length() > 0 ))
			Plus_();
	}

	void Minus()
	{
		Minus_();
		while ( ! ( _policyMap[ _index ].length() > 0 ))
		Minus_();
		
	}

	string GetValueDesc()
	{
		return _policyMap[ _index ];
	}

protected:
	UISetterPolicySetBase() {}
	
	virtual void Plus_()
	{
//		D_LOG("Policy map size: %d", _policyMap.size() );
		if ( _index < ( _policyMap.size() - 1 ) + _startIndex )
			_index++;
		else
			_index = _startIndex;
	}

	virtual void Minus_()
	{
		if ( _index > _startIndex )
			_index--;
		else
			_index = _startIndex + _policyMap.size() - 1 ;
	}

	typedef map<int, string> StringMap;

	StringMap		_policyMap;
	unsigned int	_startIndex;
	unsigned int	_index;
};

//--------------------------------------------------------------------------
// UISetterPolicy Enum
//--------------------------------------------------------------------------
template <class ValueType>
class UISetterPolicyEnum : public UISetterPolicySetBase
{
public:
	typedef ValueType Type;

	ValueType GetValue()
	{
		return ( ValueType ) _index;
	}

	void SetValue( ValueType value )
	{
		_index = ( int ) value;
	}

protected:
	UISetterPolicyEnum() {};
};

//--------------------------------------------------------------------------
// UISetterPolicy Sequence
//--------------------------------------------------------------------------
template <class ValueType>
class UISetterPolicySequence : public UISetterPolicySetBase
{
public:
	typedef ValueType Type;

	ValueType GetValue()
	{
		return _value;
	}

	void SetValue( ValueType value )
	{
		//_index = ( int ) value;
	}

protected:
	UISetterPolicySequence() {};
	Type	_value;
};

//--------------------------------------------------------------------------
// Bool wrap up so it works with enum
class UISetterPolicyBoolBase : public UISetterPolicySetBase
{
public:
	typedef bool Type;

	virtual Type GetValue()
	{
		if ( _index == 0 ) return false;
		else return true;
	}

	virtual void SetValue( Type value )
	{
		if ( value ) _index = 1;
		else _index = 0;
	}

protected:
	UISetterPolicyBoolBase() {};
};


//--------------------------------------------------------------------------
// Bool 
//--------------------------------------------------------------------------
class UISetterPolicyBool : public UISetterPolicyBoolBase
{
public:
	typedef bool Type;
	UISetterPolicyBool() 
	{
		_startIndex = 0;
		_index = _startIndex;

		_policyMap[ 0 ]	= string( "False" ); 
		_policyMap[ 1 ]	= string( "True" );
	}
};



//--------------------------------------------------------------------------
//-- UISetterExt 
// - same as UI plus set/get value of underline object
//--------------------------------------------------------------------------
template <class Policy>
class UISetterExt : public UISetter<Policy>
{
public:
	typedef typename Policy::Object			Object;
	typedef typename Policy::Objects		Objects;

	static UISetterExt* Create( const char *name );
	virtual ~UISetterExt(){};

	virtual void UpdateDisplay();

	virtual void SetObject( Object *object )
	{
		UISetter<Policy>::_policy.SetObject( object ); 
		UpdateDisplay(); 
	}

	virtual void SetObjects( Objects *objects )
	{
		UISetter<Policy>::_policy.SetObjects( objects ); 
		UpdateDisplay(); 
	}

protected:
	UISetterExt( const char *name );
};

//--------------------------------------------------------------------------
template <class Policy>
UISetterExt<Policy> *UISetterExt<Policy>::Create( const char *name )
{
	UISetterExt<Policy> *setter;

	setter = new UISetterExt<Policy>( name );
	setter->setPosition( CCPoint( 0.0f, 0.0f ));
	return setter;
}
//--------------------------------------------------------------------------
template <class Policy>
void pu::setter::UISetterExt<Policy>::UpdateDisplay()
{
	UISetter<Policy>::_policy.UpdateValue();
	UISetter<Policy>::UpdateDisplay();
}
//--------------------------------------------------------------------------
template <class Policy>
UISetterExt<Policy>::UISetterExt( const char *name ) : UISetter<Policy>( name )
{}
//--------------------------------------------------------------------------



//--------------------------------------------------------------------------
//-- UISetterExtAlignable
// - same as UI plus set/get value of underline object
//--------------------------------------------------------------------------
template <class Policy>
class UISetterExtAlignable : public UISetterExt<Policy>
{
public:
	static UISetterExtAlignable* Create( const char *name );
	virtual ~UISetterExtAlignable(){};

protected:
	UISetterExtAlignable( const char *name );

	void AddAlignButton();
	void Align_Callback( CCObject* pSender );
};

//--------------------------------------------------------------------------
template <class Policy>
UISetterExtAlignable<Policy> *UISetterExtAlignable<Policy>::Create( const char *name )
{
	UISetterExtAlignable<Policy> *setter;

	setter = new UISetterExtAlignable<Policy>( name );
	setter->setPosition( CCPoint( 0.0f, 0.0f ));
	return setter;
}
//--------------------------------------------------------------------------
template <class Policy>
UISetterExtAlignable<Policy>::UISetterExtAlignable( const char *name ) : UISetterExt<Policy>( name )
{
	AddAlignButton();
}
//--------------------------------------------------------------------------
template <class Policy>
void UISetterExtAlignable<Policy>::AddAlignButton()
{
#ifdef BUILD_EDITOR4ALL
	CCLabelBMFont *alignLabel = CCLabelBMFont::labelWithString( "cent",  pu::ui::settings::DISPLAY_FONT );
#else
	CCLabelBMFont *alignLabel = CCLabelBMFont::labelWithString( "align",  pu::ui::settings::DISPLAY_FONT );
#endif

	CCMenuItemLabel *alignItem = CCMenuItemLabel::itemWithLabel( alignLabel, this, menu_selector( UISetterExtAlignable<Policy>::Align_Callback ));

	this->_menu->addChild( alignItem, 0 );
	this->_menu->alignItemsHorizontallyWithPadding( 40.0f );

	CCPoint point;
	point = this->_menu->getPosition();
	point.x += 40.0f;

	this->_menu->setPosition( point );
}
//--------------------------------------------------------------------------
template <class Policy>
void UISetterExtAlignable<Policy>::Align_Callback( CCObject* pSender )
{
	this->_policy.AlignObjects();
}


//--------------------------------------------------------------------------
// UISetterExtPolicyBase
//--------------------------------------------------------------------------
template <typename O>
class UISetterExtPolicyBase
{
protected:
	UISetterExtPolicyBase()
	{
		_objects = NULL;
	}

public:
	// fix me
	// the vector below should be really passed not 
	typedef void* ObjectPtrType;
	typedef std::vector<ObjectPtrType> Objects;

	void SetObject( O *object )
	{ 
		if ( ! _objects || ! object )
			return;

		_objects->clear();
		_objects->push_back( (ObjectPtrType) object );
	}

	void SetObjects(  Objects *objects )
	{
		_objects = objects;
	}

	virtual void UpdateValue() = 0;
	virtual void Plus() = 0;
	virtual void Minus() = 0;

protected:
	Objects	*_objects;
};



//--------------------------------------------------------------------------
// UISetterExtPolicyAlignable
//--------------------------------------------------------------------------
template <typename O>
class UISetterExtPolicyAlignable : public UISetterExtPolicyBase<O>
{
protected:
	UISetterExtPolicyAlignable() : UISetterExtPolicyBase<O>()
	{
	}

public:
	virtual void AlignObjects() = 0;
};


//--------------------------------------------------------------------------
// UISetterExtPolicyEnum
//--------------------------------------------------------------------------
template <class SetterImpl, typename TUISetterPolicy = UISetterPolicyEnum< typename SetterImpl::Type > /*, class TUISetterPolicy = UISetterPolicyEnum */>
class UISetterExtPolicyEnum : 
	public UISetterExtPolicyBase< typename SetterImpl::Object>, // underlying object - puBlock, b2Joint
	public TUISetterPolicy										// type to be used - b2BodyType, b2JointType, bool (for enum default, for bool ins)
{
public :
	typedef typename SetterImpl::Type	Type;
	typedef typename SetterImpl::Object	Object;


	virtual void UpdateValue()
	{
		if ( this->_objects && this->_objects->size() > 0 )
		{
			Type value = _setter.GetValue( (Object *) (*( this->_objects->begin() )) );
			TUISetterPolicy::SetValue( value ); 
		}
	}

	virtual void Plus()
	{ 
		if ( ! UISetterExtPolicyBase<Object>::_objects || ! UISetterExtPolicyBase<Object>::_objects->size() )
			return;

		TUISetterPolicy::Plus();

		typename UISetterExtPolicyBase<typename SetterImpl::Object>::Objects::iterator it;
		for ( it = this->_objects->begin(); it != this->_objects->end(); it++ )
		{
			_setter.SetValue( (Object *)(*it), TUISetterPolicy::GetValue());
		}
	}

	virtual void Minus()	
	{ 
		if ( ! UISetterExtPolicyBase<Object>::_objects || ! UISetterExtPolicyBase<Object>::_objects->size() )
			return;

		TUISetterPolicy::Minus();
		typename UISetterExtPolicyBase<typename SetterImpl::Object>::Objects::iterator it;
		for ( it = this->_objects->begin(); it != this->_objects->end(); it++ )
		{
			_setter.SetValue( (Object *)(*it), TUISetterPolicy::GetValue());
		}
	}

	typename SetterImpl::Type GetValue()
	{ 
		if ( ! UISetterExtPolicyBase<Object>::_objects || ! UISetterExtPolicyBase<Object>::_objects->size() )
			return (Type) -1;

		return _setter.GetValue( (Object *) (*( UISetterExtPolicyBase<Object>::_objects->begin() )));
	}

protected:
	SetterImpl		_setter;
};

template <class SetterImpl>
class UISetterExtPolicyBool : public UISetterExtPolicyEnum< SetterImpl, UISetterPolicyBoolBase > 
{};

//--------------------------------------------------------------------------------------------------------
template <class SetterImpl, int TStep = 10, int TMin = 0, int TMax = 0>
class UISetterExtPolicyFloat : 	
	public UISetterExtPolicyAlignable< typename SetterImpl::Object>,
	public FloatPolicy< >
{
public :
	typedef typename SetterImpl::Object		Object;

	virtual void UpdateValue()
	{
		if ( !(  this->_objects && this->_objects->size() > 0 ))
			return;

		this->_value = this->_setter.GetValue( (Object*)( *this->_objects->begin()));
	}

	virtual void AlignObjects()
	{
		if ( !( this->_objects && this->_objects->size() > 1 ))
			return;

		Type value;
		value = this->_setter.GetValue( (Object*) *( this->_objects->begin()));

		if ( this->_objects && this->_objects->size() > 1 )
		{
			typename UISetterExtPolicyBase<typename SetterImpl::Object>::Objects::iterator it;
			for ( it = this->_objects->begin(); it != this->_objects->end(); it++ )
				_setter.SetValue( (Object *)(*it), value );
		}
	}

	virtual void Plus()
	{ 
		if ( ! UISetterExtPolicyBase<Object>::_objects || ! UISetterExtPolicyBase<Object>::_objects->size() )
			return;

		float step;
		step = 0.1f * ((float) TStep ) / 10.0f;

		this->_value += step;
		if ( TMin != TMax )
		{
			float min = ((float) TMin ) / 10.0f;
			float max = ((float) TMax ) / 10.0f;

//			D_FLOAT(min)
//			D_FLOAT(max)

			if ( this->_value < min )
				this->_value = min;

			if ( this->_value > max )
				this->_value = max;
		}
	
		Object* obj;
		Type value;
		for ( typename UISetterExtPolicyBase<typename SetterImpl::Object>::Objects::iterator it = this->_objects->begin(); it != this->_objects->end(); it++ )
		{
			obj = (Object*)( *it );
			value = this->_setter.GetValue( obj );
			value += step;

			if ( TMin != TMax )
			{
				float min = ((float) TMin ) / 10.0f;
				float max = ((float) TMax ) / 10.0f;

				D_FLOAT(min)
				D_FLOAT(max)

				if ( value < min )
					value = min;

				if ( value > max )
					value = max;

			}
			this->_setter.SetValue( obj, value ) ;
		}
	}

	virtual void Minus()	
	{ 
		if ( ! UISetterExtPolicyBase<Object>::_objects || ! UISetterExtPolicyBase<Object>::_objects->size() )
			return;

		float step;
		step = 0.1f * ((float) TStep ) / 10.0f;
		
		this->_value -= step;
		if ( TMin != TMax )
		{
			float min = ((float) TMin ) / 10.0f;
			float max = ((float) TMax ) / 10.0f;

			D_FLOAT(min)
			D_FLOAT(max)

			if ( this->_value < min )
				this->_value = min;

			if ( this->_value > max )
				this->_value = max;
		}

		Object* obj;
		Type value;
		for ( typename UISetterExtPolicyBase<typename SetterImpl::Object>::Objects::iterator it = this->_objects->begin(); it != this->_objects->end(); it++ )
		{
			obj = (Object*)( *it );
			value = this->_setter.GetValue( obj );
			value -= step;

			if ( TMin != TMax )
			{
				float min = ((float) TMin ) / 10.0f;
				float max = ((float) TMax ) / 10.0f;

				D_FLOAT(min)
				D_FLOAT(max)

				if ( value < min )
					value = min;

				if ( value > max )
					value = max;
			}
			this->_setter.SetValue( obj, value ) ;
		}
	}

protected:
	SetterImpl		_setter;
};


} //namespace ui

} //namespace pu

#endif
