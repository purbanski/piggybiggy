#include "HidingMenu.h"

//----------------------------------------------------------------------------------------
HidingMenu::HidingMenu()
{
	_enabled = false;
}
//----------------------------------------------------------------------------------------
HidingMenu::~HidingMenu()
{
	removeAllChildrenWithCleanup( true );
}
//----------------------------------------------------------------------------------------
bool HidingMenu::init()
{
	if ( ! CCLayer::init() )
		return false;

	return true;
}
//----------------------------------------------------------------------------------------
void HidingMenu::Show()
{
	_enabled = true;
	CCMoveBy *move = CCMoveBy::actionWithDuration( 0.5f, _showPoint );
	runAction( move );
}
//----------------------------------------------------------------------------------------
void HidingMenu::Hide()
{

	_enabled = false;
	CCMoveBy *move = CCMoveBy::actionWithDuration( 0.5f, _hidePoint );
	runAction( move );
}
//----------------------------------------------------------------------------------------

