#include "MenuItemPU.h"
#include "Debug/MyDebug.h"

MenuItemRepeat* MenuItemRepeat::itemWithLabel(CCNode*label, CCObject* target, SEL_MenuHandler selector)
{
	MenuItemRepeat *pRet = new MenuItemRepeat();
	pRet->initWithLabel(label, target, selector);
	pRet->autorelease();
	return pRet;
}
//-----------------------------------------------------------------------------------------------------------------
MenuItemRepeat::MenuItemRepeat()
{
	_hitCount = 0;
	_repeat = false;
}
//-----------------------------------------------------------------------------------------------------------------
void MenuItemRepeat::activate()
{
	if(m_bIsEnabled)
	{
		this->stopAllActions();
		this->setScale( m_fOriginalScale );
	}
}
//-----------------------------------------------------------------------------------------------------------------
void MenuItemRepeat::selected()
{ 
	CCMenuItemLabel::selected();
	CCMenuItem::activate();
	schedule( schedule_selector( MenuItemRepeat::Tick ));
}		
//-----------------------------------------------------------------------------------------------------------------
void MenuItemRepeat::unselected() 
{ 
	CCMenuItemLabel::unselected();  
	unschedule( schedule_selector( MenuItemRepeat::Tick ));
	_repeat = false;
}
//-----------------------------------------------------------------------------------------------------------------
void MenuItemRepeat::Tick( cocos2d::ccTime dt )
{
	_hitCount++;
	
	if ( ! _repeat )
	{
		if ( _hitCount > MenuItemRepeat::INITIAL_DELAY )
		{
			_hitCount = 0 ;
			_repeat = true;
		}
	}
	else
	{
		if ( _hitCount > MenuItemRepeat::REPEAT_DELAY )
		{
			CCMenuItem::activate();
			_hitCount = 0;
		}
	}
}
