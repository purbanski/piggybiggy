#ifndef __SLIDINGMENU_H__
#define __SLIDINGMENU_H__

#include "cocos2d.h"

USING_NS_CC;

class SlidingMenu : public CCMenu
{
public:
    static SlidingMenu *Create();
    
	~SlidingMenu();

	virtual bool ccTouchBegan(CCTouch* touch, CCEvent* event);
	virtual void ccTouchMoved(CCTouch* touch, CCEvent* event);
	virtual void ccTouchEnded(CCTouch* touch, CCEvent* event);
	virtual void ccTouchCancelled(CCTouch *touch, CCEvent* event);

protected:
    SlidingMenu();
	void MyInit( CCMenuItem* item, ...);

private:
	float _startPosX, _startPosY;
	float _touchX, _touchY;
	
	bool _isMoving;
};

#endif
