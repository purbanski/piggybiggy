#include "SlidingMenu.h"

SlidingMenu* SlidingMenu::Create()
{
    SlidingMenu *smenu;
    smenu = new SlidingMenu();
	smenu->MyInit( NULL, NULL );
    return smenu;
}

SlidingMenu::SlidingMenu() 
{
	_isMoving = false;
}

SlidingMenu::~SlidingMenu()
{
}

bool SlidingMenu::ccTouchBegan(CCTouch* touch, CCEvent* event)
{
	CCPoint touchLocation = touch->locationInView();	
	
	_touchX = touchLocation.x;
	_touchY = touchLocation.y;

	_startPosX = this->getPosition().x;
	_startPosY = this->getPosition().y;

	return CCMenu::ccTouchBegan( touch, event );;
}

void SlidingMenu::ccTouchEnded(CCTouch* touch, CCEvent* event)
{
	if ( _isMoving == true )
	{
		_isMoving = false ;
		//CCMenu::ccTouchCancelled( touch, event );
	}
	else
		CCMenu::ccTouchEnded( touch, event );
}

void SlidingMenu::ccTouchCancelled(CCTouch *touch, CCEvent* event)
{
	CCMenu::ccTouchCancelled( touch, event );
}

void SlidingMenu::ccTouchMoved(CCTouch* touch, CCEvent* event)
{
	CCPoint touchLocation = touch->locationInView();	

	//float dy = touchLocation.x - _touchX ;
	float dx = touchLocation.y - _touchY; 

	this->setPosition( CCPoint( _startPosX, _startPosY  - dx ));

	if ( _isMoving == false )
	{
		_isMoving = true ;
		CCMenu::ccTouchCancelled( touch, event );
	}
}



void SlidingMenu::MyInit( CCMenuItem* item, ...)
{
	va_list args;
	va_start(args,item);

	this->initWithItems( item, args );
	this->autorelease();

	va_end(args);

	setPosition( CCPoint ( 0.0f , 0.0f ));
}
