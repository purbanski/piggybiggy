#ifndef __MENUITEMPU_H__
#define __MENUITEMPU_H__

#include "cocos2d.h"

USING_NS_CC;

class MenuItemRepeat : public CCMenuItemLabel
{
public:
	static const int REPEAT_DELAY = 1;
	static const int INITIAL_DELAY = 20;

	static MenuItemRepeat* itemWithLabel(CCNode*label, CCObject* target, SEL_MenuHandler selector);

	//Button activations
	virtual void activate();
	virtual void selected();
	virtual void unselected();
	void Tick( cocos2d::ccTime dt );

private :
	MenuItemRepeat();
	int _hitCount;
	bool _repeat;
};

#endif
