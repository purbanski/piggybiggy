#ifndef __MY_MENU_H__
#define __MY_MENU_H__
#include "cocos2d.h"

//#include "cocos2d.h"
#//include "Editor/EditorConfig.h"
#include "GameConfig.h"

USING_NS_CC;

class ChildMenu;

//---------------------------------------------------------------------------------------------------------
// MenuBase
//---------------------------------------------------------------------------------------------------------
class MenuBase : public CCNode
{
public:
	void SetTouchEnabled( bool enabled );
	void SetPosition( float x, float y );
	virtual void SetVisible( bool visible );

protected:
	CCMenu	*_menu;
};
//---------------------------------------------------------------------------------------------------------




//---------------------------------------------------------------------------------------------------------
// MainMenu
//---------------------------------------------------------------------------------------------------------
class MainMenu : public MenuBase
{
public:
	MainMenu();
	virtual ~MainMenu();
	
	virtual void MenuCallback( CCObject* pSender );
	void HideToggle( CCObject* pSender );

protected:
	virtual void ConstructMenu( const char *font );

	void ConstMenus();
	void ConstLabels( const char *font );
	

	bool _hidden;
	vector<ChildMenu *>	_menus;
};
//---------------------------------------------------------------------------------------------------------




//---------------------------------------------------------------------------------------------------------
// ChildMenu
//---------------------------------------------------------------------------------------------------------
class ChildMenu : public MenuBase
{
public:
	ChildMenu(){};
	virtual ~ChildMenu(){};

	virtual void Menu_CreateSimpleBlock( CCObject* pSender ){};
	virtual string GetMenuName() { return string(""); };

protected:
	virtual void ConstructMenu();
	virtual void ConstructMenuPost();
};
//---------------------------------------------------------------------------------------------------------




//---------------------------------------------------------------------------------------------------------
// CallbackMap ChildMenu
//---------------------------------------------------------------------------------------------------------
template<class C>
class CallbackMapChildMenu : public ChildMenu
{
public:
	CallbackMapChildMenu(){};
	virtual ~CallbackMapChildMenu(){};

	virtual void MenuCallback( CCObject* pSender );
	virtual string GetMenuName() { return string(""); };

protected:
	typedef void ( C::*CallbackFunc )();
	typedef map<string, CallbackFunc> FuncMap;

	virtual void ConstructMenu( const char *font );

	FuncMap	_funcMap;
};
//---------------------------------------------------------------------------------------------------------
template<class C>
void CallbackMapChildMenu<C>::ConstructMenu( const char *font )
{
	ChildMenu::ConstructMenu();

	// Construct Labels
	CCLabelBMFont* menuLabel;
	CCMenuItemLabel* menuItem;
	typename FuncMap::iterator it;
	string labelText;
	
	//create labels
	for( it = _funcMap.begin(); it != _funcMap.end(); ++it )
	{
		labelText.clear();
		labelText.append( it->first );
	
		menuLabel = CCLabelBMFont::labelWithString( labelText.c_str(), font );
		menuLabel->setOpacity( Config::EditorMenuOpacity );
		
		menuItem = CCMenuItemLabel::itemWithLabel( menuLabel, this, menu_selector( CallbackMapChildMenu<C>::MenuCallback ));
		menuItem->setUserData( (void *) ( menuLabel ) );

		_menu->addChild( menuItem, 10 );
	}
	
	_menu->alignItemsVertically();
	_menu->setPosition( CCPoint( 0, 0 ));
}
//---------------------------------------------------------------------------------------------------------
template<class C>
void CallbackMapChildMenu<C>::MenuCallback( CCObject* pSender )
{
	CallbackFunc func;
	CCNode *node;
	string functionName;

	node = (CCNode *) pSender;
	functionName = ( (CCLabelBMFont *) node->getUserData() )->getString();
	func = _funcMap[functionName];
	
	C *myselfClass;
	myselfClass  = (C *) this;
	(myselfClass->*func)();
}
//---------------------------------------------------------------------------------------------------------

#endif
