#include "Menu.h"
#include "pu/ui/menus/SlidingMenu.h"
#include "Debug/MyDebug.h"
#include "GameConfig.h"

//----------------------------------------------------------------------------------------
// MenuBase
//----------------------------------------------------------------------------------------
void MenuBase::SetPosition( float x, float y )
{
	CCNode::setPosition( CCPoint( x, y ));
}
//----------------------------------------------------------------------------------------
void MenuBase::SetTouchEnabled( bool enabled )
{
	_menu->setIsTouchEnabled( enabled );
}
//----------------------------------------------------------------------------------------
void MenuBase::SetVisible( bool visible )
{
	_menu->setIsVisible( visible );
	_menu->setIsTouchEnabled( visible );
}



//----------------------------------------------------------------------------------------
// MainMenu
//----------------------------------------------------------------------------------------
MainMenu::MainMenu() 
{
	_menu = CCMenu::menuWithItems( NULL, NULL );

	SetPosition( 0, 0 );
	_hidden = false;
}
//----------------------------------------------------------------------------------------
void MainMenu::ConstructMenu( const char *font )
{
	ConstLabels( font );
	ConstMenus();

	_menu->alignItemsHorizontallyWithPadding( 10.0f );
	addChild( _menu );
}
//----------------------------------------------------------------------------------------
MainMenu::~MainMenu()
{
}
//----------------------------------------------------------------------------------------
void MainMenu::MenuCallback( CCObject* pSender )
{
	vector<ChildMenu *>::iterator it;
	ChildMenu *menu;
	for ( it = _menus.begin(); it != _menus.end(); it++ )
	{
		menu = (*it);
		menu->SetTouchEnabled( false );
		menu->SetVisible( false );
	}

	//-------
	// Callback pointer extraction
	menu = ( ChildMenu *) (( CCNode *) pSender )->getUserData();

	menu->SetVisible( true );
	menu->SetTouchEnabled( true );
}
//----------------------------------------------------------------------------------------
void MainMenu::HideToggle( CCObject* pSender )
{
	vector<ChildMenu *>::iterator it;
	ChildMenu *menu;

	_hidden = _hidden ? false : true;

	if ( _hidden )
	{
		for ( it = _menus.begin(); it != _menus.end(); it++ )
		{
			menu = (*it);
			menu->SetVisible( false );
		}

		CCArray *nodes = _menu->getChildren();
		CCMenuItem *menuItem;

		for ( unsigned int i = 0; i < nodes->count() - 1; i++ )
		{
			menuItem = (CCMenuItem *)( nodes->objectAtIndex( i ) );
			menuItem->setIsVisible( false );
			menuItem->setIsEnabled( false );
		}
	}
	else
	{
		CCArray *nodes = _menu->getChildren();
		CCMenuItem *menuItem;

		for ( unsigned int i = 0; i < nodes->count() - 1; i++ )
		{
			menuItem = (CCMenuItem *)( nodes->objectAtIndex( i ) );
			menuItem->setIsVisible( true );
			menuItem->setIsEnabled( true );
		}
	}
}
//----------------------------------------------------------------------------------------
void MainMenu::ConstLabels( const char *font )
{
	// Construct Labels
	CCLabelBMFont* menuLabel;
	CCMenuItemLabel* menuItem;
	ChildMenu *menu;
	vector<ChildMenu *>::iterator it;
	string labelText;

	for( it = _menus.begin(); it != _menus.end(); ++it )
	{
		menu = *it;

		labelText.clear();
		labelText.append( menu->GetMenuName() );
		D_LOG("Creating Menu: %s", menu->GetMenuName().c_str() )

		menuLabel = CCLabelBMFont::labelWithString( labelText.c_str(), font );
		menuLabel->setOpacity( Config::EditorMenuOpacity );
		menuItem = CCMenuItemLabel::itemWithLabel( menuLabel, this, menu_selector( MainMenu::MenuCallback ));

		menuItem->setUserData( ( void* ) menu );
		_menu->addChild( menuItem, 10 );
	}
}
//----------------------------------------------------------------------------------------
void MainMenu::ConstMenus()
{
	vector<ChildMenu *>::iterator it;

	for( it = _menus.begin(); it != _menus.end(); ++it )
	{
		(*it)->SetVisible( false );
		(*it)->SetPosition( 0.0f, 0.0f );
		addChild( *it );
	}
}
//----------------------------------------------------------------------------------------



//----------------------------------------------------------------------------------------
// ChildMenu
//----------------------------------------------------------------------------------------
void ChildMenu::ConstructMenu()
{
	SlidingMenu* smenu;
	smenu = SlidingMenu::Create();
    
	_menu = smenu;
	addChild( _menu, 10 );
	SetPosition( 0.0f, 0.0f );
}
//----------------------------------------------------------------------------------------
void ChildMenu::ConstructMenuPost()
{
	_menu->alignItemsVerticallyWithPadding( 18.0f );
}
