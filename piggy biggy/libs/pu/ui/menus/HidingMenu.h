#ifndef __HIDINGMENU_H__
#define __HIDINGMENU_H__

#include "cocos2d.h"

USING_NS_CC;

//-----------------------------------------------------------------------------------------------------
class HidingMenu : public CCLayer
{
public:
	LAYER_NODE_FUNC( HidingMenu );
	bool init();

	virtual ~HidingMenu();

	virtual void Show();
	virtual void Hide();

	void SetShowPosition( CCPoint point ) { _showPoint = point; }
	void SetHidePosition( CCPoint point ) { _hidePoint = point; }

protected:
	HidingMenu();

protected:
	bool		_enabled;
	CCPoint		_hidePoint;
	CCPoint		_showPoint;
};
//-----------------------------------------------------------------------------------------------------

#endif
