#ifndef __PUMOVABLELAYER_H__
#define __PUMOVABLELAYER_H__

#include "cocos2d.h"
#include <Box2D/Box2D.h>
#include <pu/ui/NodesManager.h>

USING_NS_CC;

class puMovableLayer : public CCLayer
{
public:
	virtual ~puMovableLayer();

	// CCLayer
	virtual void registerWithTouchDispatcher();

	// CCTouchDelegate
	virtual bool ccTouchBegan(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchMoved(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchEnded(CCTouch *pTouch, CCEvent *pEvent);

	virtual void draw();

	virtual void ToggleAddToParent( CCNode *parent);

	void CenterOnScreen();
	void MyRemoveFromParent();

protected:
	puMovableLayer( CCRect boundingBox );
	bool init();

	void AlignMenu();
	void CloseMe( CCObject* pSender );

	bool MouseDown( const b2Vec2& position );
	void MouseUp( const b2Vec2& position );
	void MouseMove( const b2Vec2& position );

protected:
	CCNode				*_parent;
	CCMenu				*_menu;
	CCRect				_boundingBox;
	float				_deltaX;
	float				_deltaY;
};

#endif
