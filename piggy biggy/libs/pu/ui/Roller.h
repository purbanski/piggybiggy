#ifndef __ROLLER_H__
#define __ROLLER_H__

#include "cocos2d.h"

USING_NS_CC;
//--------------------------------------------------------------------------
class RollerDot : public CCLayer
{
public:
	LAYER_NODE_FUNC( RollerDot );
	bool init();
	~RollerDot();

	//Touch related	
	virtual bool ccTouchBegan(CCTouch *touch, CCEvent *pEvent);
	virtual void ccTouchMoved(CCTouch *touch, CCEvent *pEvent);
	virtual void ccTouchEnded(CCTouch *touch, CCEvent *pEvent);
	virtual void registerWithTouchDispatcher();

private:
	RollerDot();

private:
	CCSprite	*_dot;
	CCSprite	*_background;
};
//--------------------------------------------------------------------------
class Roller : public CCNode
{
public:
	Roller();
	~Roller();

private:
	RollerDot	*_dot;
};
//--------------------------------------------------------------------------
#endif
