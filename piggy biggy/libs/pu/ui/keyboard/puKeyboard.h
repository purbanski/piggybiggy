#ifndef __PUKEYBOARD_H__
#define __PUKEYBOARD_H__

#include "cocos2d.h"
#include "pu/ui/Settings.h"

USING_NS_CC;
//---------------------------------------------------------------------------------------------------------
class puKeyboardListener
{
public:
	virtual void Keyboard_Callback( string text ) = 0;
};
//---------------------------------------------------------------------------------------------------------
class puKeyboard : public CCLayer
{
public:
	static void	Show( puKeyboardListener *listener, const char *text );
	static void	SetMyParent( CCNode *parent, int zOrder );
	
	bool init();
	virtual void registerWithTouchDispatcher();
	virtual bool ccTouchBegan(CCTouch *pTouch, CCEvent *pEvent) { return true; }


private:
	static void Create();
	puKeyboard();

	CCMenu* ConstructMenu( string letters );
	void	ConstructLetter( CCMenu *menu, char c );
	CCMenu* ConstructCustomMenu();
	CCNode* ConstructCustomLetter( CCMenu *menu, const char *letters, string imageFile );

	void KeyPressed( CCObject *sender );


private:	
	static puKeyboard	*_sInstance;
	CCLabelBMFont		*_textFiled;

	CCMenu	*_keys1Row;
	CCMenu	*_keys2Row;
	CCMenu	*_keys3Row;
	CCMenu	*_keys4Row;
	CCMenu	*_keys5Row;

	puKeyboardListener	*_listener;
	CCNode				*_parent;
	int					_zOrder;
};
#endif
