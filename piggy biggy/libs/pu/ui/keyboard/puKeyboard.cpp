#include <sstream>
#include "unDebug.h"
#include "puKeyboard.h"
#include "Tools.h"

puKeyboard* puKeyboard::_sInstance = NULL;
//--------------------------------------------------------------------------------------------------------
void puKeyboard::Create()
{
	if ( ! _sInstance )
	{
	// FIXME 
	// memory leak
		_sInstance = new puKeyboard();
		if ( _sInstance && _sInstance->init() )
		{
			_sInstance->autorelease();
			_sInstance->retain();
		}
		else unAssertMsg( puKeyboard, false, ("Something went wrong"));
	}
}
//--------------------------------------------------------------------------------------------------------
void puKeyboard::Show( puKeyboardListener *listener, const char* text )
{
	Create();
	_sInstance->_textFiled->setString( text );
	_sInstance->_listener = listener;
	_sInstance->_parent->addChild( _sInstance, _sInstance->_zOrder );
}
//--------------------------------------------------------------------------------------------------------
void puKeyboard::SetMyParent( CCNode *parent, int zOrder )
{
	Create();
	_sInstance->_parent = parent;
	_sInstance->_zOrder = zOrder;
}
//--------------------------------------------------------------------------------------------------------
puKeyboard::puKeyboard()
{
	_keys1Row = ConstructMenu( string( "1234567890" ));
	_keys2Row = ConstructMenu( string( "qwertyuiop" ));
	_keys3Row = ConstructMenu( string( "asdfghjkl" ));
	_keys4Row = ConstructMenu( string( "zxcvbnm" ));
	_keys5Row = ConstructCustomMenu();

	float xbase = 1024.0/2.0f;
	float ybase = 440.0f;
	float ydelta = 90.0f;

	_keys1Row->setPosition( ccp( xbase, ybase ));
	_keys2Row->setPosition( ccp( xbase, ybase - ydelta ));
	_keys3Row->setPosition( ccp( xbase, ybase - 2* ydelta ));
	_keys4Row->setPosition( ccp( xbase - 49, ybase - 3 * ydelta ));
	_keys5Row->setPosition( ccp( xbase - 49, ybase - 4 * ydelta ));

	CCSprite *background;
	background = CCSprite::spriteWithFile( "Editor/Images/Keyboard/KeyboardBackground.png");
	background->setPosition( ccp( 1024 / 2, 768 / 2));
	background->setOpacity( 210 );
	addChild( background, 5 );

	_textFiled = CCLabelBMFont::labelWithString( "", pu::ui::settings::KEYBOARD_TEXTFIELD_FONT );
	_textFiled->setPosition( ccp( Tools::GetScreenMiddleX(), 563 ));
	addChild( _textFiled, 5 );
	
	setPosition( ccp( 0, 0 ));
}
//--------------------------------------------------------------------------------------------------------
void puKeyboard::registerWithTouchDispatcher()
{
	CCTouchDispatcher::sharedDispatcher()->addTargetedDelegate( this, kCCMenuTouchPriority, true );
}
//--------------------------------------------------------------------------------------------------------
bool puKeyboard::init()
{
	if ( ! CCLayer::init() )
		return false;

	setIsTouchEnabled( true );
	return true;
}
//--------------------------------------------------------------------------------------------------------
CCMenu* puKeyboard::ConstructMenu( string letters )
{
	CCMenu *menu = CCMenu::menuWithItems( NULL, NULL );

	string::iterator it;
	for ( it = letters.begin(); it != letters.end(); it++ )
		ConstructLetter( menu, *it );
	
	menu->alignItemsHorizontallyWithPadding( 18.0f );
	addChild( menu, 10  );

	return menu;
}
//--------------------------------------------------------------------------------------------------------
CCMenu *puKeyboard::ConstructCustomMenu()
{
	CCMenu *menu = CCMenu::menuWithItems( NULL, NULL );
	CCNode *item;

	item = ConstructCustomLetter( menu, " ", string("KeySpace"));
	item->setPosition( ccp( 36.0f, 0.0f ));

	item = ConstructCustomLetter( menu, "Back", string("KeyBackspace"));
	item->setPosition( ccp( 440.0f, 89.0f ));

	item = ConstructCustomLetter( menu, "Enter", string("KeyEnter"));
	item->setPosition( ccp( 422.0f, 0.0f ));

	item = ConstructCustomLetter( menu, "", string("KeyClose")); // hack ""  = means cancel
	//item->setPosition( ccp( -768/2 + 36, 0.0f )); // old cancel position
	item->setPosition( ccp( 500.0f, 620.0f ));
	
	addChild( menu, 10  );
	return menu;
}
//--------------------------------------------------------------------------------------------------------
CCNode* puKeyboard::ConstructCustomLetter( CCMenu *menu, const char *letters, string imageFile )
{
	CCLabelBMFont *menuLabel, *menuLabelSelected ;

	menuLabel = CCLabelBMFont::labelWithString( letters,  pu::ui::settings::KEYBOARD_FONT );
	menuLabelSelected = CCLabelBMFont::labelWithString( letters,  pu::ui::settings::KEYBOARD_FONT );

	CCSprite *spriteNormal;
	CCSprite *spriteSelected;
	CCSprite *spriteDisabled;

	string filenameSpriteNormal( "Editor/Images/Keyboard/" + imageFile + ".png" );
	string filenameSpriteSelected( "Editor/Images/Keyboard/" + imageFile + "Selected.png" );
	string filenameSpriteDisable( "Editor/Images/Keyboard/" + imageFile + ".png" );

	spriteNormal   = CCSprite::spriteWithFile( filenameSpriteNormal.c_str() );
	spriteSelected = CCSprite::spriteWithFile( filenameSpriteSelected.c_str() );
	spriteDisabled = CCSprite::spriteWithFile( filenameSpriteDisable.c_str() );

	menuLabel->setPosition( ccp( 90.0f, 40.0f ));
	menuLabel->setOpacity( 100 );
	spriteNormal->addChild( menuLabel );

	menuLabelSelected->setPosition( ccp( 90.0f, 40.0f ));
	menuLabelSelected->setOpacity( 100 );
	spriteSelected->addChild( menuLabelSelected );


	CCMenuItemSprite *menuItem;
	menuItem = CCMenuItemSprite::itemFromNormalSprite( spriteNormal, spriteSelected, spriteDisabled, this, menu_selector( puKeyboard::KeyPressed ));
	menuItem->setUserData( (void *) ( menuLabel ) );
	menuItem->setOpacity( 190 );

	menu->addChild( menuItem, 15 );

	return menuItem;
}

//--------------------------------------------------------------------------------------------------------
void puKeyboard::ConstructLetter( CCMenu *menu, char letter )
{
	stringstream ss;
	ss << letter;

	CCLabelBMFont *menuLabel, *menuLabelSelected ;

	menuLabel = CCLabelBMFont::labelWithString( ss.str().c_str(),  pu::ui::settings::KEYBOARD_FONT );
	menuLabelSelected = CCLabelBMFont::labelWithString( ss.str().c_str(),  pu::ui::settings::KEYBOARD_FONT );
	
	CCSprite *spriteNormal;
	CCSprite *spriteSelected;
	CCSprite *spriteDisabled;

	spriteNormal   = CCSprite::spriteWithFile( "Editor/Images/Keyboard/KeyBackground.png" );
	spriteSelected = CCSprite::spriteWithFile( "Editor/Images/Keyboard/KeyBackgroundSelected.png" );
	spriteDisabled = CCSprite::spriteWithFile( "Editor/Images/Keyboard/KeyBackground.png" );

	menuLabel->setPosition( ccp( 40.0f, 40.0f ));
	menuLabel->setOpacity( 100 );
	spriteNormal->addChild( menuLabel );

	menuLabelSelected->setPosition( ccp( 40.0f, 40.0f ));
	menuLabelSelected->setOpacity( 100 );
	spriteSelected->addChild( menuLabelSelected );
	

	CCMenuItemSprite *menuItem;
	menuItem = CCMenuItemSprite::itemFromNormalSprite( spriteNormal, spriteSelected, spriteDisabled, this, menu_selector( puKeyboard::KeyPressed ));
	menuItem->setUserData( (void *) ( menuLabel ) );
	menuItem->setOpacity( 190 );

	menu->addChild( menuItem, 15 );

}

//--------------------------------------------------------------------------------------------------------
void puKeyboard::KeyPressed( CCObject *sender )
{
	string text;
	string letter;
	CCLabelBMFont* label = (CCLabelBMFont*) ((CCMenuItemSprite*) sender)->getUserData();

	letter = label->getString();

	if ( letter.compare( "Back") == 0 )
	{
		text = _textFiled->getString();
		
		if ( text.size() < 1 )
			return;

		text.resize( text.size() - 1 );
		_textFiled->setString( text.c_str() );
	}
	else if ( letter.compare( "Enter" ) == 0 )
	{
		CCNode::getParent()->removeChild( this, false );
		CCTouchDispatcher::sharedDispatcher()->removeDelegate( this );

		_listener->Keyboard_Callback( _textFiled->getString() );
	} 
	else if ( letter.length() == 0  ) // letter.compare( "Cancel" ) == 0 )
	{
		CCNode::getParent()->removeChild( this, false );
		CCTouchDispatcher::sharedDispatcher()->removeDelegate( this );
	}
	else
	{
		text = _textFiled->getString();
		text.append( letter );
		_textFiled->setString( text.c_str() );
	}
}




//--------------------------------------------------------------------------------------------------------

