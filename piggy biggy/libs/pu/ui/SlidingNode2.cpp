#include "SlidingNode2.h"
#include "Debug/MyDebug.h"
#include "Tools.h"
#include "Game.h"
#include "SoundEngine.h"
#include "Components/CustomNodes.h"

const float SlidingNode2::_sAcclearte               = 1.7f;
const float SlidingNode2::_sAcclearteMax            = 20.0f;
const float SlidingNode2::_sAcclearteInit           = 8.0f;
const float SlidingNode2::_sAcclearteSlowdownFactor = 1.065f;

//-----------------------------------------------------------------------------------
SlidingNode2* SlidingNode2::node( const CCSize& size, Config::MousePriority piority )
{ 
	SlidingNode2 *pRet = new SlidingNode2( size, piority );
	if ( pRet && pRet->init() )
	{ 
		pRet->autorelease();
		return pRet;
	}
	else
	{
		delete pRet;
		pRet = NULL; 
		return NULL;
	}
}
//-----------------------------------------------------------------------------------
SlidingNode2::SlidingNode2( const CCSize& size, Config::MousePriority piority )
{
    _upperLimitDelta = 0.0f;
    _lowerLimitDelta = 0.0f;
    _limitType = eLimit_FBRequestMenu;
    
    _slideDuration = 0;
	_slideOffset = 0;
	_sliding = false;
    _mosuePiority = piority;
    
	_scrollWindowSize = size;
	_slideLength = Config::GameSize.width;

	_slidingNode = CCNode::node();
	CCLayer::addChild( _slidingNode, 5 );
    
    _contentNode = NodeAlign::Create();
    _slidingNode->addChild( _contentNode, 10 );
}
//-----------------------------------------------------------------------------------
SlidingNode2::~SlidingNode2()
{
	removeAllChildrenWithCleanup( true );
}
//-----------------------------------------------------------------------------------
bool SlidingNode2::init()
{
	if ( !CCLayer::init()  )
		return false;

	SoundEngine::Get()->LoadSound( SoundEngine::eSoundSlidingNode_Slide );
	SoundEngine::Get()->LoadSound( SoundEngine::eSoundSlidingNode_SlideBack );
	
	setIsTouchEnabled( true );
	return true;
}
//--------------------------------------------------------------------------------------------------
void SlidingNode2::setIsTouchEnabled(bool enabled)
{
//    D_LOG("%i", (int) enabled );
    CCLayer::setIsTouchEnabled(enabled);
}
//--------------------------------------------------------------------------------------------------
void SlidingNode2::registerWithTouchDispatcher()
{
	CCTouchDispatcher::sharedDispatcher()->addTargetedDelegate( this, _mosuePiority, true );
}
//-----------------------------------------------------------------------------------
bool SlidingNode2::ccTouchBegan( CCTouch *pTouch, CCEvent *pEvent )
{
    if ( ! getIsTouchEnabled() )
        return false;
    
    //	if ( _sliding )
	//	return true;

    unschedule( schedule_selector( SlidingNode2::Slide ));
    _sliding = false;
    
    b2Vec2 testTouch;
    
    testTouch= Tools::TouchAdjustToGame( pTouch );
    _startTouch = Tools::ConvertLocation( pTouch );
    
 //    D_LOG( "1) start touch: %f %f", _startTouch.x, _startTouch.y )

    if ( testTouch.x > _clickableAreaEnd.x || testTouch.x < _clickableAreaStart.x ||
        testTouch.y > _clickableAreaEnd.y || testTouch.y < _clickableAreaStart.y )
        return false;
    
    _lastGoodTouch = _startTouch;
	_slideStartPosition = _slidingNode->getPosition();

	gettimeofday(&_startTime, NULL);
	//D_LOG("Time %d %d", _startTime.tv_sec, _startTime.tv_usec );
	return true;
}
//-----------------------------------------------------------------------------------
void SlidingNode2::ccTouchMoved( CCTouch *pTouch, CCEvent *pEvent )
{
    if ( ! getIsTouchEnabled() )
        return;

//    D_LOG("movig");
    
	if ( _sliding )
		return;

	b2Vec2 touch = Tools::ConvertLocation( pTouch );
	CCPoint pos;
    
    if ( IsHorizontal() )
    {
        pos.x = _slideStartPosition.x + RATIO * ( touch.x - _startTouch.x );
        pos.y = _slideStartPosition.y + RATIO ;

    }
    else
    {
        pos.x = _slideStartPosition.x ;
        pos.y = _slideStartPosition.y + RATIO * ( touch.y - _startTouch.y ) / Game::Get()->GetScale();
    }

    if ( isOutbound( pos ))
    {
        // _startTouch = _lastGoodTouch;
        gettimeofday(&_startTime, NULL);
        return;
    }
    _lastGoodTouch = touch;
	_slidingNode->setPosition( pos );
}
//-----------------------------------------------------------------------------------
void SlidingNode2::ccTouchEnded( CCTouch *pTouch, CCEvent *pEvent )
{
    if ( ! getIsTouchEnabled() )
        return;

	if ( _sliding  )
		return;

	// Calculate distance 
	b2Vec2 touch = Tools::ConvertLocation( pTouch );
    
	float deltaX = abs( RATIO * ( touch.x - _startTouch.x ));
    float deltaY = abs( RATIO * ( touch.y - _startTouch.y ));
//
//	// Leave sliding note at last postition
//	CCPoint pos;
//
//    if ( IsHorizontal() )
//    {
//        pos.x = _slideStartPosition.x + RATIO * ( touch.x - _lastGoodTouch.x );
//        pos.y = _slideStartPosition.y ;
//    }
//    else
//    {
//        pos.x = _slideStartPosition.x ;
//        pos.y = _slideStartPosition.y + RATIO * ( touch.y - _lastGoodTouch.y );
//    }
//
//    if ( isOutbound( pos ))
//       return;
//    _slidingNode->setPosition( pos );
//    

    
	// Calculate time diffrence
	timeval tv;
	gettimeofday( &tv, NULL );

	int timeDiff;
	timeDiff = ( tv.tv_sec - _startTime.tv_sec ) * 1000000 ;
	timeDiff += ( tv.tv_usec - _startTime.tv_usec );
//	D_LOG("Time diff %d distanc %f",  timeDiff, deltaX );

	_acelerate = _sAcclearteInit;
	
    if ( IsHorizontal() )
    {
        if ( ( touch.x - _startTouch.x ) > 0 )
            _slideDirection = 1;
        else
            _slideDirection = -1;
    }
    else
    {
        if ( ( touch.y - _startTouch.y ) > 0 )
            _slideDirection = 1;
        else
            _slideDirection = -1;
    }

	// Slide
	if (
        timeDiff < 350000
        && (( IsHorizontal() && deltaX > 25.0f ) || ( ! IsHorizontal() && deltaY > 25.0f ))
        )
	{
        _sliding = true;
        _slideDuration = 0;
//        D_INT( timeDiff )
//        D_FLOAT( deltaY )
        
        _slideDurationFactor = 1.0f * deltaX / 5.0f ;
        schedule( schedule_selector( SlidingNode2::Slide ));
	}
}
//-----------------------------------------------------------------------------------
void SlidingNode2::Slide( cocos2d::ccTime dt )
{
//    D_HIT
    _slideDuration += dt;
    
	// Set new slide position
	CCPoint pos;
	pos = GetNewPosition();

	// Prepare new bound to be cross checked with new position 
	Range range;
	range = GetBoundRange();

	_slidingNode->setPosition( pos );
}
//-----------------------------------------------------------------------------------
SlidingNode2::Range SlidingNode2::GetBoundRange( )
{
	// Prepare new bound to be cross checked with new position 
	float a,b;
	float atemp;
	float btemp;

    float initTemp;
    if ( IsHorizontal() )
        initTemp = _initPosition.x;
    else
        initTemp = _initPosition.y;
        
    atemp = ( _slideOffset + (  _slideDirection * 1 )) * _slideLength + initTemp;
	btemp = _slideOffset * _slideLength + initTemp;

	if ( atemp < btemp )
	{
		a = atemp;
		b = btemp;
	}
	else
	{
		a = btemp;
		b = atemp;
	}
	Range range;
	range.first = a;
	range.second = b;

	return range;
}
//-----------------------------------------------------------------------------------
CCPoint SlidingNode2::GetNewPosition()
{
    CCPoint orginalPos;
	CCPoint pos;
    
	pos = _slidingNode->getPosition();
    orginalPos = _slidingNode->getPosition();
    
    if ( IsHorizontal() )
        pos.x += _slideDirection * _acelerate;
    else
        pos.y += _slideDirection * _acelerate;

//    D_FLOAT(_slideDurationFactor)
    float tempLimit;
    
    tempLimit = 0.5 * _slideDurationFactor;

    if ( tempLimit > 2.2f )
        tempLimit = 2.2f;
    
    if ( tempLimit < 0.8f )
        tempLimit = 0.8f;
    
    tempLimit = 1.75f;
//    D_FLOAT(_slideDurationFactor)
    if ( _slideDuration < tempLimit )
    {
        if ( _acelerate < _sAcclearteMax )
        {
            _acelerate *= _sAcclearte;
            if ( _acelerate > _sAcclearteMax )
                _acelerate = _sAcclearteMax;
        }
    }
    else
    {
        _acelerate /= _sAcclearteSlowdownFactor;
    }
    
//    D_FLOAT(_acelerate );
    if ( _acelerate < 1.0f )
    {
        _sliding = false;
        unschedule( schedule_selector( SlidingNode2::Slide ));
    }
    
    if ( isOutbound(pos))
    {
        return orginalPos;
    }
    return pos;
}
//-----------------------------------------------------------------------------------
void SlidingNode2::SetInitPosition( CCPoint point )
{
	_initPosition = point;
	_slidingNode->setPosition( point );
}
//-----------------------------------------------------------------------------------
void SlidingNode2::addChild( CCNode * child )
{
	_slidingNode->addChild( child );
}
//-----------------------------------------------------------------------------------
void SlidingNode2::addChild( CCNode * child, int zOrder )
{
	_slidingNode->addChild( child, zOrder );
}
//-----------------------------------------------------------------------------------
void SlidingNode2::removeFromTouchDispatcher()
{
	CCTouchDispatcher::sharedDispatcher()->removeDelegate( this );
}
//-----------------------------------------------------------------------------------
void SlidingNode2::onExit()
{
	CCTouchDispatcher::sharedDispatcher()->removeDelegate( this );
}
//-----------------------------------------------------------------------------------
void SlidingNode2::SetSlideLength( float len )
{
	_slideLength = len;
}
//-----------------------------------------------------------------------------------
bool SlidingNode2::IsHorizontal()
{
    return false;
}
//-----------------------------------------------------------------------------------
bool SlidingNode2::isOutbound( const CCPoint &testPoint )
{
    if ( BreaksLowerLimit(testPoint) || BreaksUpperLimit(testPoint) )
    {
        unschedule( schedule_selector( SlidingNode2::Slide ));
        return true;
    }
    
    return false;
}
//-----------------------------------------------------------------------------------
void SlidingNode2::ShowItem( int index )
{
    CCPoint pos;
    pos.x = 0.0f;
    pos.y = _contentNode->GetPadding() * (float)( index - 1 ) - _contentNode->getPosition().y;
    
    if ( BreaksLowerLimit( pos ))
    {
        pos.y = _contentNode->getContentSize().height - Config::GameSize.height / 2.0f - _contentNode->getPosition().y;
    }
    else if ( BreaksUpperLimit( pos ))
    {
        pos.y = _contentNode->GetPadding() - _contentNode->getPosition().y;
    }
    
//    D_POINT(pos);
    _slidingNode->setPosition( pos );
}
//-----------------------------------------------------------------------------------
void SlidingNode2::SetLowerLimitDelta( float delta )
{
    _lowerLimitDelta = delta;
}
//-----------------------------------------------------------------------------------
void SlidingNode2::SetUpperLimitDelta( float delta )
{
    _upperLimitDelta = delta;
}
//-----------------------------------------------------------------------------------
void SlidingNode2::SetLimitsType( SlidingNode2::LimitsType type )
{
    _limitType = type;
}
//-----------------------------------------------------------------------------------
bool SlidingNode2::BreaksUpperLimit( const CCPoint& pos )
{
    float posDelta;
    float upperLimit;

    upperLimit = _contentNode->GetPadding() - _upperLimitDelta;
    posDelta = pos.y + (( _contentNode->getPosition().y - Tools::GetScreenMiddleY() ) + Config::GameSize.height / 4.0f);

//    D_LOG( "upper:%f  posDelta:%f pos.y:%f _conteg.y:%f", upperLimit, posDelta, pos.y, _contentNode->getPosition().y );

    if (( _limitType == eLimit_InLevel_Menu && pos.y < upperLimit ) ||
        ( _limitType == eLimit_FBRequestMenu && posDelta < upperLimit ))
    {
        D_LOG("Upper hit");
        return true;
    }
   
    return false;
}
//-----------------------------------------------------------------------------------
bool SlidingNode2::BreaksLowerLimit( const CCPoint& pos )
{
    float posDelta;
    float lowerLimit;
    
    lowerLimit = _contentNode->getContentSize().height - Config::GameSize.height / 2.0f + _lowerLimitDelta;
    posDelta = pos.y + (( _contentNode->getPosition().y - Tools::GetScreenMiddleY() ) + Config::GameSize.height / 4.0f);

//    D_LOG( "lowerLimit:%f  posDelta:%f pos.y:%f _conteg.y:%f", lowerLimit, posDelta, pos.y, _contentNode->getPosition().y );

    if (( _limitType == eLimit_InLevel_Menu && pos.y > lowerLimit ) ||
        ( _limitType == eLimit_FBRequestMenu && posDelta > lowerLimit ))
    {
        D_LOG("Lower hit");
        return true;
    }

    return false;
}
//-----------------------------------------------------------------------------------
void SlidingNode2::SetClickableArea(const CCPoint &startPoint, const CCPoint &endPoint )
{
    _clickableAreaStart = startPoint;
    _clickableAreaEnd = endPoint;
}
//-----------------------------------------------------------------------------------
NodeAlign* SlidingNode2::GetContentNode()
{
    return _contentNode;
}
//-----------------------------------------------------------------------------------
void SlidingNode2::ClearContent()
{
    _contentNode->removeAllChildrenWithCleanup( true );
    _contentNode->RemoveAllNodes();
}
//-----------------------------------------------------------------------------------
void SlidingNode2::AddToContent( CCNode *node )
{
    _contentNode->AddNode( node );
}
//-----------------------------------------------------------------------------------



//-----------------------------------------------------------------------------------



//-----------------------------------------------------------------------------------
