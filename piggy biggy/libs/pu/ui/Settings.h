#ifndef __PU_UI_SETTINGS_H__
#define __PU_UI_SETTINGS_H__

namespace pu
{

	namespace ui 
	{

		namespace settings
		{
			const char CONTROL_FONT[] = ("Editor/Fonts/arial-56.fnt");
			const char DISPLAY_FONT[] = ("Editor/Fonts/SetterValue.fnt");
			const char BUTTON_FONT[] = ("Editor/Fonts/arial-22.fnt"); //
			const char NAME_FONT[] = ("Editor/Fonts/SetterName.fnt");
			const char MSG_FONT[] = ("Editor/Fonts/arial-20.fnt");
			const char ARIAL_20_WHITE_FONT[] = ("Editor/Fonts/arial-20-white.fnt");
			const char ARIAL_18_WHITE_FONT[] = ("Editor/Fonts/arial-18-white.fnt");
			const char ARIAL_16_RED_FONT[] = ("Editor/Fonts/arial-16-red.fnt");
			const char ARIAL_18_RED_FONT[] = ("Editor/Fonts/arial-18-red.fnt");
			const char SMALL_INFO_FONT[] = ("Editor/Fonts/arial-13.fnt");

			const char ARIAL_30[] = ("Editor/Fonts/arial-30.fnt");

			const char KEYBOARD_FONT[] = ("Editor/Fonts/arial-keyboard.fnt");
			const char KEYBOARD_TEXTFIELD_FONT[] = ("Editor/Fonts/arial-keyboard-textfield.fnt");
			const char TEST_FONT[] = ("Editor/Fonts/test.fnt"); //
		}
	}
}

#endif
