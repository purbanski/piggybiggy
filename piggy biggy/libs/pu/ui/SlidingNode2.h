#ifndef __SLIDINGNODE2_H__
#define __SLIDINGNODE2_H__

#include <cocos2d.h>
#include "GameConfig.h"
#include <Box2D/Box2D.h>
#include "SlidingNode.h"
//#include "Components/CustomNodes.cpp"

//------------------------------------------------------------------------------------------
using namespace std;
USING_NS_CC;
//------------------------------------------------------------------------------------------
class NodeAlign;
//------------------------------------------------------------------------------------------
class SlidingNode2 : public CCLayer
{
public:
	~SlidingNode2();

	static SlidingNode2* node( const CCSize& scrollingWindowSize, Config::MousePriority mousePiority );
	virtual bool init();
	virtual void onExit();
	void SetSlideLength( float len );

    virtual void setIsTouchEnabled(bool enabled);
	virtual bool ccTouchBegan(CCTouch *touch, CCEvent *pEvent);
	virtual void ccTouchMoved(CCTouch *touch, CCEvent *pEvent);
	virtual void ccTouchEnded(CCTouch *touch, CCEvent *pEvent);
	virtual void registerWithTouchDispatcher();

    void SetClickableArea(const CCPoint &startPoint, const CCPoint &endPoint );
    
	void SetInitPosition( CCPoint point );
    void ShowItem( int index );
	void removeFromTouchDispatcher();

	virtual void Slide( cocos2d::ccTime dt );
    
	virtual void addChild(CCNode * child );
	virtual void addChild(CCNode * child, int zOrder);

	void SetListener( SlidingNodeListener *listener );
//    void SetContentSize( float x, float y );
//    void SetSingleElementSize( const CCSize &size );
    
    NodeAlign* GetContentNode();
    void ClearContent();
    void AddToContent( CCNode *node );
    
    //------
    // limits
    typedef enum {
        eLimit_FBRequestMenu = 1,
        eLimit_InLevel_Menu
    } LimitsType;
    
    void SetLowerLimitDelta( float delta );
    void SetUpperLimitDelta( float delta );
    void SetLimitsType( LimitsType type );

protected:
    bool BreaksUpperLimit( const CCPoint& pos );
    bool BreaksLowerLimit( const CCPoint& pos );
    
protected:
	static const float _sAcclearte;
	static const float _sAcclearteMax;
	static const float _sAcclearteInit;
    static const float _sAcclearteSlowdownFactor;
    
	SlidingNode2( const CCSize& scrollWindowSize,  Config::MousePriority mousePiority );
    bool IsHorizontal();
    bool isOutbound( const CCPoint &testpoint );
    
private:
	typedef pair<float,float> Range;
	Range GetBoundRange();
	CCPoint GetNewPosition();

	CCNode				*_slidingNode;
    NodeAlign           *_contentNode;
    
	int _slideOffset;

	float	_acelerate;
	bool	_sliding;
	int		_slideDirection;
	b2Vec2	_startTouch;
	b2Vec2	_lastGoodTouch;
	CCPoint	_slideStartPosition;
	CCPoint	_initPosition;
	timeval	_startTime;
	float	_slideLength;
    float   _slideDuration;
    float   _slideDurationFactor;

    float   _upperLimitDelta;
    float   _lowerLimitDelta;
    LimitsType _limitType;

    CCSize  _scrollWindowSize;
    
    CCPoint _clickableAreaStart;
    CCPoint _clickableAreaEnd;
    Config::MousePriority _mosuePiority;
};

#endif
