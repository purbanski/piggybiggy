#include <Box2D/Box2D.h>
#include "puMovableLayer.h"
#include "Debug/MyDebug.h"
#include "Tools.h"
#include "GameConfig.h"
#include "GLES-Render.h"
#include "GameGlobals.h"

#include <pu/ui/Settings.h>

puMovableLayer::puMovableLayer( CCRect boundingBox )
{
	_deltaX = 0;
	_deltaY = 0;

	_boundingBox = boundingBox;
	
	setIsTouchEnabled( true );

	CCLabelBMFont *closeLabel = CCLabelBMFont::labelWithString( "CLOSE",  pu::ui::settings::BUTTON_FONT );
	CCMenuItemLabel *closeItem = CCMenuItemLabel::itemWithLabel( closeLabel, this, menu_selector( puMovableLayer::CloseMe ));

	_menu = CCMenu::menuWithItems( NULL, NULL );
	_menu->addChild( closeItem, 20 );
	_menu->setTag( 5 ); // fixme - magic number - used in blockDetails to move close button
	_menu->alignItemsHorizontally();
	//_menu->setPosition( CCPoint( boundingBox.origin.x + 20.0f, boundingBox.origin.y - 30.0f ));
#ifdef BUILD_EDITOR4ALL
	_menu->setPosition( CCPoint( boundingBox.size.width - 30.0f, -15.0f ));
#else
	_menu->setPosition( CCPoint( boundingBox.size.width + 5.0f, ( boundingBox.size.height + boundingBox.origin.y ) / 2.0f ));
#endif
	addChild( _menu );

	CenterOnScreen();
}
//------------------------------------------------------------------------------------------------------
void puMovableLayer::AlignMenu()
{
	//_menu->alignItemsHorizontallyWithPadding( 25.0f );
	_menu->alignItemsVerticallyWithPadding( 50.0f );
}
//------------------------------------------------------------------------------------------------------
bool puMovableLayer::init()
{
	if ( ! CCLayer::init() )
		return false;

	setIsTouchEnabled( true );

	return true;
}
//------------------------------------------------------------------------------------------------------
puMovableLayer::~puMovableLayer()
{	
	removeAllChildrenWithCleanup( true );
}
//------------------------------------------------------------------------------------------------------
void puMovableLayer::ToggleAddToParent( CCNode *parent )
{
	if ( CCLayer::getParent() == NULL )
		parent->addChild( this );
	else
		parent->removeChild( this, true );
}
//------------------------------------------------------------------------------------------------------
void puMovableLayer::MyRemoveFromParent()
{
	if ( CCLayer::getParent() != NULL )
		getParent()->removeChild( this, true );
}
//------------------------------------------------------------------------------------------------------
void puMovableLayer::CloseMe( CCObject *sender )
{
	removeFromParentAndCleanup( true );
}
//------------------------------------------------------------------------------------------------------
void puMovableLayer::registerWithTouchDispatcher()
{
	CCTouchDispatcher::sharedDispatcher()->addTargetedDelegate( this, Config::eMousePriority_EditorMovebleLayer, true );
}
//------------------------------------------------------------------------------------------------------
bool puMovableLayer::ccTouchBegan(CCTouch *touch, CCEvent *event)
{
	return MouseDown( Tools::ConvertLocationNoRatio( touch ));
}
//------------------------------------------------------------------------------------------------------
void puMovableLayer::ccTouchMoved(CCTouch *touch, CCEvent *pEvent)
{	
	MouseMove( Tools::ConvertLocationNoRatio( touch ));
}
//------------------------------------------------------------------------------------------------------
void puMovableLayer::ccTouchEnded(CCTouch *touch, CCEvent *pEvent)
{
	MouseUp( Tools::ConvertLocationNoRatio( touch ));
}
//------------------------------------------------------------------------------------------------------
bool puMovableLayer::MouseDown( const b2Vec2& position )
{
	D_LOG("Hit x:%f, y:%f" , position.x, position.y)
	if ( ! CCLayer::getIsVisible() )
		return false;

	CCRect rect = _boundingBox;
	rect.origin.x += getPosition().x;
	rect.origin.y += getPosition().y;
	
	//D_LOG( "--------------------------");
	//D_LOG( "layer position.x %f, y %f\n", getPosition().x, getPosition().y );
	//D_LOG( "touch position.x %f, y %f\n", position.x, position.y );
	//D_LOG( "rect x %f, y %f\n", rect.origin.x, rect.origin.y );
	//D_LOG( "rect width %f, height %f\n", rect.size.width, rect.size.height );
	
	if (( rect.origin.x <= position.x && position.x <= rect.origin.x + rect.size.width ) 
		&& ( rect.origin.y <= position.y && position.y <= rect.origin.y + rect.size.height ) )
	{

		D_LOG("in in in\n");
	}
	

	D_LOG( "if in bounding box %d\n", CCRect::CCRectContainsPoint( rect, CCPoint( position.x, position.y )));
	if ( ! CCRect::CCRectContainsPoint( rect, CCPoint( position.x, position.y ))) 
		return false;

	_deltaX = getPosition().x - position.x;
	_deltaY = getPosition().y - position.y;
	return true;
}
//------------------------------------------------------------------------------------------------------
void puMovableLayer::MouseMove( const b2Vec2& position )
{
	//D_LOG( "Position %f %f \n", getPosition().x, getPosition().y );
	setPosition( CCPoint( position.x + _deltaX, position.y + _deltaY ));
}
//------------------------------------------------------------------------------------------------------
void puMovableLayer::MouseUp( const b2Vec2& position )
{
	setPosition( CCPoint( position.x + _deltaX, position.y + _deltaY ));
}
//------------------------------------------------------------------------------------------------------
void puMovableLayer::draw()
{
	GLESDebugDraw::Get()->LockGL();

	b2Vec2 v[4];
	
	v[0] = ( 1.0f / RATIO ) * b2Vec2( _boundingBox.origin.x,	_boundingBox.origin.y );
	v[1] = ( 1.0f / RATIO ) * b2Vec2( _boundingBox.size.width, _boundingBox.origin.y );
	v[2] = ( 1.0f / RATIO ) * b2Vec2( _boundingBox.size.width, _boundingBox.size.height );
	v[3] = ( 1.0f / RATIO ) * b2Vec2( _boundingBox.origin.x,	_boundingBox.size.height );

	GLESDebugDraw::Get()->DrawSolidPolygon( v, 4, b2Color( 0.2f, 0.2f, 0.5f ), 0.7f );
	GLESDebugDraw::Get()->UnlockGL();
}
//------------------------------------------------------------------------------------------------------
void puMovableLayer::CenterOnScreen()
{
	CCPoint pos;
	pos = Tools::GetScreenMiddle();

	pos.x -= _boundingBox.size.width / 2.0f;
	pos.y -= _boundingBox.size.height / 2.0f;

	setPosition( pos );
}
//------------------------------------------------------------------------------------------------------
