#ifndef __GLOGGER_H__
#define __GLOGGER_H__

#ifdef GOOGLE_ANAL_LOG

#include <stddef.h>
//------------------------------------------------------------------------------------------------------------
#define GLogEvent(eventName, ...)	{ \
						memset( GLogger::_sLogBuffer, 0, sizeof( char ) * GLogger::_sLogBufferSize ); \
						\
						snprintf( GLogger::_sLogBuffer, GLogger::_sLogBufferSize, __VA_ARGS__ ); \
						GLogger::Get()->LogEvent( eventName, GLogger::_sLogBuffer, "" ); \
                    }

#define GLogLevelEvent(level_nr, ...)	{ \
                        stringstream ss; \
                        ss << "Level_" << level_nr; \
						memset( GLogger::_sLogBuffer, 0, sizeof( char ) * GLogger::_sLogBufferSize ); \
						snprintf( GLogger::_sLogBuffer, GLogger::_sLogBufferSize, __VA_ARGS__ ); \
						GLogger::Get()->LogEvent( ss.str().c_str(), GLogger::_sLogBuffer, "" ); \
                    }

#define GSendView(...)	{ \
						memset( GLogger::_sLogBuffer, 0, sizeof( char ) * GLogger::_sLogBufferSize ); \
						\
						snprintf( GLogger::_sLogBuffer, GLogger::_sLogBufferSize, __VA_ARGS__ ); \
						GLogger::Get()->SendView( GLogger::_sLogBuffer );  }

#define GLogPurchased   { GLogger::Get()->PurchaseCompleted(); }


#define GLogInit    { GLogger::Get(); }
#define GLogDestroy { GLogger::Destroy(); }

//------------------------------------------------------------------
class GLogger
{
public:
    static GLogger* Get();
    static void Destroy();

    static const int _sLogBufferSize = 128;
    static char _sLogBuffer[ _sLogBufferSize ];
    
    ~GLogger();
    
    void LogEvent( const char *category, const char* action, const char *label, int value = 0 );
    void SendView( const char *view );
    void PurchaseCompleted();
    
private:
    GLogger();
    void Init();
    
private:
    static GLogger* _sInstance;    
};

//------------------------------------------------------------------

#else

#define GLogInit
#define GLogDestroy
#define GLogLevelEvent(...)
#define GLogEvent(...)
#define GSendView(...)
#define GLogPurchased

#endif


#endif
