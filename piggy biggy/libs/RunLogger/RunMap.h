#ifndef __piggy_biggy__RunMap__
#define __piggy_biggy__RunMap__

#include <map>
#include <string>
#include "GameTypes.h"
#include <cocos2d.h>

//--------------------------------------------------------------------
using namespace std;
using namespace GameTypes;
USING_NS_CC;
//--------------------------------------------------------------------
class RunMap : public map<string, string>
{
public:
    RunMap();
    void SetValueInt1( int i );
    void SetValueInt2( int i );
    void SetValueInt3( int i );
    
    void SetValueString1( const char *str );
    void SetValueString2( const char *str );
    void SetValueString3( const char *str );

    void SetValueString1( int i );
    void SetValueString2( int i );
    void SetValueString3( int i );
    
    void SetMessage( const char *msg );
    
    string GetLogString();
    
private:
    string _message;
};
//--------------------------------------------------------------------

#endif
