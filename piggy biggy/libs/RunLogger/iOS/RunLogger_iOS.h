#import <Foundation/Foundation.h>
#import <Foundation/NSObject.h>
#import <GameKit/GameKit.h>
#import <GameKit/GKAchievementViewController.h>
#import "cocos2dx/platform/ios/EAGLView.h" 
#import <UIKit/UIKit.h>


@interface RunLogger_iOS : NSObject<NSURLConnectionDelegate>

+ (RunLogger_iOS *)sharedInstance;
+ (void)destroy;

- (void) doHttpGet:(const char *)msg;

- (void)connection: (NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response;
- (void)connection: (NSURLConnection *)connection didReceiveData:(NSData *)data;
- (void)connection: (NSURLConnection *)connection didFailWithError:(NSError *)error;
- (void)connectionDidFinishLoading: (NSURLConnection *)connection;


////- (void) showGameCenter;
//- (void) authenticateLocalPlayer;
//- (void) playerAuthOK;
//- (void) playerAuthFailed:(NSError*) error;
//- (void) gameCenterViewControllerDidFinish:(GKGameCenterViewController *)gameCenterViewController;
//- (void) reportScore: (int64_t) score forLeaderboardID: (NSString*) category;
@end
