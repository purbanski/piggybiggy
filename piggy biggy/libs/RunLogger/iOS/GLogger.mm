#include <sstream>
#include "GLogger.h"
#include <stddef.h>
#include "GameConfig.h"
#include "Platform/ToolBox.h"

#import "GAIDictionaryBuilder.h"
#import "GAIFields.h"

#ifdef GOOGLE_ANAL_LOG
//------------------------------------------------------------------

GLogger* GLogger::_sInstance = NULL;
char GLogger::_sLogBuffer[ GLogger::_sLogBufferSize ];

//------------------------------------------------------------------
GLogger* GLogger::Get()
{
    if ( ! _sInstance )
    {
        _sInstance = new GLogger();
        _sInstance->Init();
    }
    
    return _sInstance;
}
//------------------------------------------------------------------
void GLogger::Destroy()
{
    if ( _sInstance )
        delete _sInstance;
    
    _sInstance = NULL; 
}
//------------------------------------------------------------------
GLogger::GLogger()
{
}
//------------------------------------------------------------------
GLogger::~GLogger()
{
}
//------------------------------------------------------------------
void GLogger::Init()
{
    //------------------------------
    // Google analytics

    // Optional: automatically send uncaught exceptions to Google Analytics.
    [GAI sharedInstance].trackUncaughtExceptions = YES;
    
    // Optional: set Google Analytics dispatch interval to e.g. 20 seconds.
    [GAI sharedInstance].dispatchInterval = Config::GoogleAnalyticsDelay;

    // Optional: set Logger to VERBOSE for debug information.
    [[[GAI sharedInstance] logger] setLogLevel:kGAILogLevelWarning];
    
    // Initialize tracker. Replace with your tracking ID.
    [[GAI sharedInstance] trackerWithTrackingId:[ NSString stringWithUTF8String:Config::GoogleAnalyticsID ]];
}
//------------------------------------------------------------------
void GLogger::LogEvent( const char *category, const char* action, const char *label, int value )
{
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:[ NSString stringWithUTF8String: category ]
                                                          action:[ NSString stringWithUTF8String: action ]
                                                           label:[ NSString stringWithUTF8String: label ]
                                                           value:[ NSNumber numberWithInt: value ]] build]];
}
//------------------------------------------------------------------
void GLogger::SendView( const char *view )
{
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    
    [tracker set:kGAIScreenName value:[NSString stringWithUTF8String: view ]];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}
//------------------------------------------------------------------
void GLogger::PurchaseCompleted()
{
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];

    GAIDictionaryBuilder* trans;
    trans = [GAIDictionaryBuilder createItemWithTransactionId:@"0_levelpack"
                                                         name:@"Level pack 01"
                                                          sku:@"L_levelpack"
                                                     category:@"Game expansions"
                                                        price:@0.99F
                                                     quantity:@1
                                                 currencyCode:@"USD"];
    [tracker send:[trans build]];
}
//------------------------------------------------------------------
    
#endif
