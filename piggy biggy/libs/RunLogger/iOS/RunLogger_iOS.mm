#import "RunLogger_iOS.h"
#import "RunLogger.h"
#include "Debug/MyDebug.h"

static RunLogger_iOS *gRunLogger = NULL;

//------------------------------------------------------------------------
// RunLogger_iOS
//------------------------------------------------------------------------
@implementation RunLogger_iOS


//------------------------------------------------------------------------
// statics
//------------------------------------------------------------------------
+ (RunLogger_iOS *)sharedInstance
{
  if ( ! gRunLogger )
    {
        gRunLogger = [[ RunLogger_iOS alloc] init ];
    }
    
    return gRunLogger;
}
//------------------------------------------------------------------------
+ (void) destroy
{
    if ( gRunLogger )
        [ gRunLogger dealloc ];
    
    gRunLogger = NULL;
}
//------------------------------------------------------------------------
// methods
//------------------------------------------------------------------------
- (void) doHttpGet:(const char*)msg
{
    D_LOG("http get: %s", msg );
    
    NSURLRequest *theRequest;
    NSURLConnection *theConnection;

    NSString *urlString;
    NSString *urlStringTemp;
    
    urlStringTemp = [NSString stringWithUTF8String:msg];
    urlString = [urlStringTemp stringByReplacingOccurrencesOfString:@" " withString:@"_"];
    
    theRequest = [NSURLRequest requestWithURL:[NSURL URLWithString: urlString]
                                  cachePolicy:NSURLRequestUseProtocolCachePolicy
                              timeoutInterval:60.0 ];
    

    theConnection = [[NSURLConnection alloc] initWithRequest:theRequest delegate:self];
}
//--------------------------------------------------------------------------------------------
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    RunLogger::Get()->Response( (void*)[data bytes], data.length );
}
//--------------------------------------------------------------------------------------------
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
}
//--------------------------------------------------------------------------------------------
- (void)connection: (NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [ connection release ]; 
}
//--------------------------------------------------------------------------------------------
- (void)connectionDidFinishLoading: (NSURLConnection *)connection
{
    [ connection release ];
}
//--------------------------------------------------------------------------------------------

@end
