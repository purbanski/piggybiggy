#ifndef __RUNLOGGER_H__
#define __RUNLOGGER_H__

#ifdef REMOTE_LOG

#define RLOGMAP( msg, vs1, vs2, vs3, vi1, vi2, vi3 ) { \
                    RunMap map; \
                    map.SetMessage( msg ); \
                    map.SetValueInt1( vi1 ); \
                    map.SetValueInt2( vi2 ); \
                    map.SetValueInt3( vi3 ); \
                    map.SetValueString1( vs1 ); \
                    map.SetValueString2( vs2 ); \
                    map.SetValueString2( vs3 ); \
                    \
                    memset( RunLogger::_sURLBuffer, 0, sizeof( char ) * RunLogger::URLBufferSize ); \
					\
                    snprintf( RunLogger::_sURLBuffer, RunLogger::URLBufferSize, map.GetLogString().c_str() ); \
                    RunLogger::Get()->Log( RunLogger::_sURLBuffer ); \
};

#define RLOG_S( msg, vs1 ) { \
                    RunMap map; \
                    map.SetMessage( msg ); \
                    map.SetValueString1( vs1 ); \
                    \
                    memset( RunLogger::_sURLBuffer, 0, sizeof( char ) * RunLogger::URLBufferSize ); \
					\
                    snprintf( RunLogger::_sURLBuffer, RunLogger::URLBufferSize, "%s", map.GetLogString().c_str() ); \
                    RunLogger::Get()->Log( RunLogger::_sURLBuffer ); \
};

#define RLOG_SS( msg, vs1, vs2 ) { \
                    RunMap map; \
                    map.SetMessage( msg ); \
                    map.SetValueString1( vs1 ); \
                    map.SetValueString2( vs2 ); \
                    \
                    memset( RunLogger::_sURLBuffer, 0, sizeof( char ) * RunLogger::URLBufferSize ); \
					\
                    snprintf( RunLogger::_sURLBuffer, RunLogger::URLBufferSize, "%s", map.GetLogString().c_str() ); \
                    RunLogger::Get()->Log( RunLogger::_sURLBuffer ); \
};

#define RLOG_SSS( msg, vs1, vs2, vs3 ) { \
                    RunMap map; \
                    map.SetMessage( msg ); \
                    map.SetValueString1( vs1 ); \
                    map.SetValueString2( vs2 ); \
                    map.SetValueString3( vs3 ); \
                    \
                    memset( RunLogger::_sURLBuffer, 0, sizeof( char ) * RunLogger::URLBufferSize ); \
					\
                    snprintf( RunLogger::_sURLBuffer, RunLogger::URLBufferSize, "%s", map.GetLogString().c_str() ); \
                    RunLogger::Get()->Log( RunLogger::_sURLBuffer ); \
};

#define RLOG_I( msg, vi1 ) { \
                    RunMap map; \
                    map.SetMessage( msg ); \
                    map.SetValueInt1( vi1 ); \
                    \
                    memset( RunLogger::_sURLBuffer, 0, sizeof( char ) * RunLogger::URLBufferSize ); \
					\
                    snprintf( RunLogger::_sURLBuffer, RunLogger::URLBufferSize, "%s", map.GetLogString().c_str() ); \
                    RunLogger::Get()->Log( RunLogger::_sURLBuffer ); \
};

#define RLOG_II( msg, vi1, vi2 ) { \
                    RunMap map; \
                    map.SetMessage( msg ); \
                    map.SetValueInt1( vi1 ); \
                    map.SetValueInt2( vi2 ); \
                    \
                    memset( RunLogger::_sURLBuffer, 0, sizeof( char ) * RunLogger::URLBufferSize ); \
					\
                    snprintf( RunLogger::_sURLBuffer, RunLogger::URLBufferSize, "%s", map.GetLogString().c_str() ); \
                    RunLogger::Get()->Log( RunLogger::_sURLBuffer ); \
};

#define RLOG_III( msg, vi1, vi2, vi3 ) { \
                    RunMap map; \
                    map.SetMessage( msg ); \
                    map.SetValueInt1( vi1 ); \
                    map.SetValueInt2( vi2 ); \
                    map.SetValueInt3( vi3 ); \
                    \
                    memset( RunLogger::_sURLBuffer, 0, sizeof( char ) * RunLogger::URLBufferSize ); \
					\
                    snprintf( RunLogger::_sURLBuffer, RunLogger::URLBufferSize, "%s", map.GetLogString().c_str() ); \
                    RunLogger::Get()->Log( RunLogger::_sURLBuffer ); \
};

#define RLOG_SII( msg, vs1, vi1, vi2 ) { \
                    RunMap map; \
                    map.SetMessage( msg ); \
                    map.SetValueString1( vs1 ); \
                    map.SetValueInt1( vi1 ); \
                    map.SetValueInt2( vi2 ); \
                    \
                    memset( RunLogger::_sURLBuffer, 0, sizeof( char ) * RunLogger::URLBufferSize ); \
					\
                    snprintf( RunLogger::_sURLBuffer, RunLogger::URLBufferSize, "%s", map.GetLogString().c_str() ); \
                    RunLogger::Get()->Log( RunLogger::_sURLBuffer ); \
};

#define RLOG_SIII( msg, vs1, vi1, vi2, vi3 ) { \
                    RunMap map; \
                    map.SetMessage( msg ); \
                    map.SetValueString1( vs1 ); \
                    map.SetValueInt1( vi1 ); \
                    map.SetValueInt2( vi2 ); \
                    map.SetValueInt3( vi3 ); \
                    \
                    memset( RunLogger::_sURLBuffer, 0, sizeof( char ) * RunLogger::URLBufferSize ); \
					\
                    snprintf( RunLogger::_sURLBuffer, RunLogger::URLBufferSize, "%s", map.GetLogString().c_str() ); \
                    RunLogger::Get()->Log( RunLogger::_sURLBuffer ); \
};

#define RLOG_SSIII( msg, vs1, vs2, vi1, vi2, vi3 ) { \
                    RunMap map; \
                    map.SetMessage( msg ); \
                    map.SetValueString1( vs1 ); \
                    map.SetValueString2( vs2 ); \
                    map.SetValueInt1( vi1 ); \
                    map.SetValueInt2( vi2 ); \
                    map.SetValueInt3( vi3 ); \
                    \
                    memset( RunLogger::_sURLBuffer, 0, sizeof( char ) * RunLogger::URLBufferSize ); \
					\
                    snprintf( RunLogger::_sURLBuffer, RunLogger::URLBufferSize, "%s", map.GetLogString().c_str() ); \
                    RunLogger::Get()->Log( RunLogger::_sURLBuffer ); \
};

#define RLOG_SSSIII( msg, vs1, vs2, vs3, vi1, vi2, vi3 ) { \
                    RunMap map; \
                    map.SetMessage( msg ); \
                    map.SetValueString1( vs1 ); \
                    map.SetValueString2( vs2 ); \
                    map.SetValueString3( vs3 ); \
                    map.SetValueInt1( vi1 ); \
                    map.SetValueInt2( vi2 ); \
                    map.SetValueInt3( vi3 ); \
                    \
                    memset( RunLogger::_sURLBuffer, 0, sizeof( char ) * RunLogger::URLBufferSize ); \
					\
                    snprintf( RunLogger::_sURLBuffer, RunLogger::URLBufferSize, "%s", map.GetLogString().c_str() ); \
                    RunLogger::Get()->Log( RunLogger::_sURLBuffer ); \
};

#define RLOGLANG( lang ) { RunLogger::Get()->LogLang( lang ); }

#define RLOG(...)	{ \
						memset( RunLogger::_sURLBuffer, 0, sizeof( char ) * RunLogger::URLBufferSize ); \
						\
						snprintf( RunLogger::_sURLBuffer, RunLogger::URLBufferSize, __VA_ARGS__ ); \
						RunLogger::Get()->Log( RunLogger::_sURLBuffer ); \
                    };
#define RLOG_DESTROY    { RunLogger::Destroy(); }
#define RLOG_INIT       { RunLogger::Get(); }

#define RLOGI1(value,...)  { \
						memset( RunLogger::_sURLBuffer, 0, sizeof( char ) * RunLogger::URLBufferSize ); \
						\
						snprintf( RunLogger::_sURLBuffer, RunLogger::URLBufferSize,  __VA_ARGS__ ); \
						RunLogger::Get()->LogIValue1( value, RunLogger::_sURLBuffer ); \
                    };

#define RLOGS1(value,...)  { \
						memset( RunLogger::_sURLBuffer, 0, sizeof( char ) * RunLogger::URLBufferSize ); \
						\
						snprintf( RunLogger::_sURLBuffer, RunLogger::URLBufferSize,  __VA_ARGS__ ); \
						RunLogger::Get()->LogSValue1( value, RunLogger::_sURLBuffer ); \
                    };


#else

#define RLOG(...)
#define RLOGI1(value1,...)
#define RLOG_DESTROY
#define RLOG_INIT
#define RLOG_I(...)
#define RLOG_II(...)
#define RLOG_III(...)
#define RLOG_S(...)
#define RLOG_SS(...)
#define RLOG_SSS(...)
#define RLOG_SII(...)
#define RLOG_SIII(...)
#define RLOG_SSIII(...)
#define RLOG_SSSIII(...)
#define RLOGS1(...)
#define RLOGLANG(...)

#endif

#include <map>
#include <string>
#include "GameTypes.h"
#include <cocos2d.h>
#include "RunMap.h"

//--------------------------------------------------------------------
using namespace std;
using namespace GameTypes;
USING_NS_CC;
//--------------------------------------------------------------------
class RunLogger
{
public:
    static RunLogger* Get();
    static void Destroy();

    ~RunLogger();
    
    void Log( const char* msg );
    void LogIValue1( int value1, const char* msg );
    void LogSValue1( const char *value2, const char* msg );
    void LogLang( LanguageType lang );
    void Response( void *data, unsigned int len );
    
private:
    RunLogger();
    void Init();
    
public:
    typedef enum {
        URLBufferSize = 256,
        MacHashSize = 41,
    } SizeType;

    static char    _sURLBuffer[URLBufferSize];
    static char    _sMacHash[MacHashSize];
    
private:
    bool            _initDone;
    unsigned long   _sessionId;
    
    static RunLogger* _sInstance;    
};
//--------------------------------------------------------------------

#endif
