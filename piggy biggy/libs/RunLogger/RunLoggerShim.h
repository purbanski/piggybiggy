#ifndef piggy_biggy_RunLogger_shim_h
#define piggy_biggy_RunLogger_shim_h

class RunLoggerShim
{
public:
    static void Init();
    static void Destroy();
    static void DoHttpGet( const char *msg );
    
};


#endif
