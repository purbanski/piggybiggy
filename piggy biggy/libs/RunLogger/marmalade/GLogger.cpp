#include <sstream>
#include "GLogger.h"
#include <stddef.h>
#include "GameConfig.h"
#include "Platform/ToolBox.h"

#ifdef REMOTE_LOG
//------------------------------------------------------------------

GLogger* GLogger::_sInstance = NULL;
char GLogger::_sLogBuffer[ GLogger::_sLogBufferSize ];

//------------------------------------------------------------------
GLogger* GLogger::Get()
{
	if ( ! _sInstance )
	{
		_sInstance = new GLogger();
		_sInstance->Init();
	}

	return _sInstance;
}
//------------------------------------------------------------------
void GLogger::Destroy()
{
	if ( _sInstance )
		delete _sInstance;

	_sInstance = NULL; 
}
//------------------------------------------------------------------
GLogger::GLogger()
{
}
//------------------------------------------------------------------
GLogger::~GLogger()
{
}
//------------------------------------------------------------------
void GLogger::Init()
{
	//------------------------------
	// Google analytics
}
//------------------------------------------------------------------
void GLogger::LogEvent( const char *category, const char* action, const char *label, int value )
{
}
//------------------------------------------------------------------
void GLogger::SendView( const char *view )
{
}
//------------------------------------------------------------------
void GLogger::PurchaseCompleted()
{
}
//------------------------------------------------------------------
void GLogger::LevelComplitionTime( int levelId, int levelEnum, int time )
{
}
//------------------------------------------------------------------

#endif
