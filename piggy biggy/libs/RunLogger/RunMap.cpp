#include "RunMap.h"
#include "Debug/MyDebug.h"
#include <sstream>

//---------------------------------------------------------------
RunMap::RunMap()
{
    operator[]( "vs1" ) = "";
    operator[]( "vs2" ) = "";
    operator[]( "vs3" ) = "";
    operator[]( "vi1" ) = "";
    operator[]( "vi2" ) = "";
    operator[]( "vi3" ) = "";
    
}
//---------------------------------------------------------------
void RunMap::SetValueInt1( int i )
{
    stringstream ss;
    ss << i;
    
    operator[]( "vi1" ) = ss.str();
}
//---------------------------------------------------------------
void RunMap::SetValueInt2( int i )
{
    stringstream ss;
    ss << i;
    
    operator[]( "vi2" ) = ss.str();
}
//---------------------------------------------------------------
void RunMap::SetValueInt3( int i )
{
    stringstream ss;
    ss << i;
    
    operator[]( "vi3" ) = ss.str();
}
//---------------------------------------------------------------
void RunMap::SetValueString1( const char *str )
{
    operator[]( "vs1" ) = str;
}
//---------------------------------------------------------------
void RunMap::SetValueString2( const char *str )
{
    operator[]( "vs2" ) = str;
}
//---------------------------------------------------------------
void RunMap::SetValueString3( const char *str )
{
    operator[]( "vs3" ) = str;
}
//---------------------------------------------------------------
void RunMap::SetValueString1( int i )
{
    stringstream ss;
    ss << i;
    
    operator[]( "vs1" ) = ss.str();
}
//---------------------------------------------------------------
void RunMap::SetValueString2( int i )
{
    stringstream ss;
    ss << i;
    
    operator[]( "vs2" ) = ss.str();
}
//---------------------------------------------------------------
void RunMap::SetValueString3( int i )
{
    stringstream ss;
    ss << i;
    
    operator[]( "vs3" ) = ss.str();
}
//---------------------------------------------------------------
void RunMap::SetMessage( const char *msg )
{
    _message = msg;
}
//---------------------------------------------------------------
string RunMap::GetLogString()
{
    stringstream ss;
    ss << _message << "&";
    
    for ( iterator it = begin(); it != end(); it++ )
    {
        ss << it->first << '=' << it->second << "&";
    }

    string ret;
    ret = ss.str().substr( 0, ss.str().length() - 1 );
    
    D_LOG("%s", ret.c_str() );
    return ret;
}

