#include "RunLogger.h"
#include "Platform/ToolBox.h"
#include "GameConfig.h"
#include "Debug/MyDebug.h"
#include "Game.h"
#include "GameConfig.h"
#include "RunLoggerShim.h"
#include "RunLogger/GLogger.h"

#include <sstream>

RunLogger*  RunLogger::_sInstance = NULL;

char        RunLogger::_sURLBuffer[RunLogger::URLBufferSize];
char        RunLogger::_sMacHash[RunLogger::MacHashSize];

//--------------------------------------------------------------------
RunLogger* RunLogger::Get()
{
    if ( ! _sInstance )
    {
        _sInstance = new RunLogger();
        _sInstance->Init();
    }
    
    return _sInstance;
}
//--------------------------------------------------------------------
void RunLogger::Destroy()
{
    if ( _sInstance )
    {
        delete _sInstance;
        _sInstance = NULL;
    }
}
//--------------------------------------------------------------------
RunLogger::RunLogger()
{
    _initDone = false;
    RunLoggerShim::Init();
    strncpy( _sMacHash, ToolBox::GetMacHash().c_str(), MacHashSize );
    _sMacHash[MacHashSize-1] = NULL;
}
//--------------------------------------------------------------------
RunLogger::~RunLogger()
{
    RunLoggerShim::Destroy();
}
//--------------------------------------------------------------------
void RunLogger::Log( const char* msg )
{
    if ( ! _initDone )
        return;
    
    std::stringstream ss;
    ss << Config::StatsUrl << "log.php?";
    ss << "s=" << _sessionId << "&";
    ss << "m=" << msg;
    D_LOG( "http: %s", ss.str().c_str() );
    
    RunLoggerShim::DoHttpGet( ss.str().c_str() );
}
//--------------------------------------------------------------------
void RunLogger::LogLang( LanguageType lang )
{
    switch ( lang )
    {
        case kLanguageEnglish:
            RLOG_S("LANG", "ENGLISH" );
            GLogEvent("Lang", "English");
        break;
        
        case kLanguageChineseT:
            RLOG_S("LANG", "CHINESE_T" );
            GLogEvent("Lang", "Chinese T");
        break;

        case kLanguageChineseS:
            RLOG_S("LANG", "CHINESE_S" );
            GLogEvent("Lang", "Chinese S");
        break;

        case kLanguageFrench:
            RLOG_S("LANG", "FRENCH" );
            GLogEvent("Lang", "French");
        break;

        case kLanguageItalian:
            RLOG_S("LANG", "ITALIAN" );
            GLogEvent("Lang", "Italian");
        break;

        case kLanguageGerman:
            RLOG_S("LANG", "GERMAN" );
            GLogEvent("Lang", "German");
        break;

        case kLanguageSpanish:
            RLOG_S("LANG", "SPANISH" );
            GLogEvent("Lang", "Spanish");
        break;

        case kLanguageRussian:
            RLOG_S("LANG", "RUSSIAN" );
            GLogEvent("Lang", "Russian");
        break;

        case kLanguagePolish:
            RLOG_S("LANG", "POLISH" );
            GLogEvent("Lang", "Polish");
        break;

        case kLanguageJapanese:
            RLOG_S("LANG", "JAPANESE" );
            GLogEvent("Lang", "Japanese");
        break;

        default:
            RLOG_S("LANG", "UNKNOWN" );
            GLogEvent("Lang", "Unknown");
            break;
    }
}
//--------------------------------------------------------------------
void RunLogger::LogIValue1( int value1, const char* msg )
{
    if ( ! _initDone )
        return;
    
    std::stringstream ss;
    ss << Config::StatsUrl << "log.php?";
    ss << "s=" << _sessionId << "&";
    ss << "m=" << msg << "&";
    ss << "vi1=" << value1;
    
    D_LOG( "http: %s", ss.str().c_str() );
    
    RunLoggerShim::DoHttpGet( ss.str().c_str() );
}
//--------------------------------------------------------------------
void RunLogger::LogSValue1( const char *svalue1, const char* msg )
{
    if ( ! _initDone )
        return;
    
    std::stringstream ss;
    ss << Config::StatsUrl << "log.php?";
    ss << "s=" << _sessionId << "&";
    ss << "m=" << msg << "&";
    ss << "vs1=" << svalue1;
    
    D_LOG( "http: %s", ss.str().c_str() );
    
    RunLoggerShim::DoHttpGet( ss.str().c_str() );
}
//--------------------------------------------------------------------
void RunLogger::Init()
{
    std::stringstream ss;
    ss << Config::StatsUrl << "initSession.php?";
    ss << "u=" << _sMacHash << "&" ;
    ss << "d=" << ToolBox::GetDeviceType() << "&";
    ss << "a=" << Config::AppVersion << "&";
    ss << "lang=" << Game::Get()->GetGameStatus()->GetLang();
    
    D_LOG( "remote log init: %s", ss.str().c_str() );
    RunLoggerShim::DoHttpGet( ss.str().c_str() );
}
//--------------------------------------------------------------------
void RunLogger::Response( void *data, unsigned int len )
{
    if ( _initDone )
        return;
    
    _initDone = true;
 
    stringstream ss;
    ss << (char*)data;
    
    int result;          //number which will contain the result

    istringstream convert( ss.str() ); // stringstream used for the conversion constructed with the contents of 'Text'
                             // ie: the stream will start containing the characters of 'Text'

    if ( !( convert >> result) ) //give the value to 'Result' using the characters in the stream
    result = 0;             //if that fails set 'Result' to 0

    _sessionId = result;

    if ( Game::Get()->GetGameStatus()->GetStatus()->_isMusicMute )
    {
        RLOG_SS("SOUND","MUSIC","OFF" );
    }
    else
    {
        RLOG_SS("SOUND","MUSIC","ON");
    }
    
    
    if ( Game::Get()->GetGameStatus()->GetStatus()->_isEffectsMute )
    {
        RLOG_SS("SOUND","FX","OFF") ;
    }
    else
    {
        RLOG_SS("SOUND","FX","ON");
    }
    
    RLOGLANG( Game::Get()->GetGameStatus()->GetStatus()->_language );
    Game::Get()->GetGameStatus()->LogStats();
}
//--------------------------------------------------------------------------------------------