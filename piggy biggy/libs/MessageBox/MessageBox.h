#ifndef __MESSAGEBOX_H__
#define __MESSAGEBOX_H__

//--------------------------------------------------------------
class MessageBox
{
public:
    static void CreateInfoBox( const char *title, const char *text );
    static bool CreateYesNoBox( const char *title, const char *text );
};
//--------------------------------------------------------------


#endif
