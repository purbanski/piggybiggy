#include "MessageBox.h"
#import "MessageBoxImpl.h"
//---------------------------------------------------------------------------------------------
void MessageBox::CreateInfoBox( const char *atitle, const char *atext )
{
    [ MessageBoxImpl CreateInfoBox:atitle text:atext ];
}
//---------------------------------------------------------------------------------------------
bool MessageBox::CreateYesNoBox( const char *atitle, const char *atext )
{
    return [ MessageBoxImpl CreateYesNoBox:atitle text:atext ];
}
//---------------------------------------------------------------------------------------------

