#include <UIKit/UIKit.h>

//--------------------------------------------------------------
//
//--------------------------------------------------------------
@interface MessageBoxRecieverImpl : NSObject<UIAlertViewDelegate>
{
int m_buttonIndex;
}
- (id) init;
- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex;
- (int) getButtonIndex;
@end


//--------------------------------------------------------------
//
//--------------------------------------------------------------
@interface MessageBoxImpl : NSObject
{
}
+ (void) CreateInfoBox:(const char *)title text:(const char *)text;
+ (bool) CreateYesNoBox:(const char *)title text:(const char *)text;
@end


