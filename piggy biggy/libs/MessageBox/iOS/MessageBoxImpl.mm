#import "MessageBoxImpl.h"
#include "cocos2d.h"
#include "SoundEngine.h"
#include "Game.h"
#include "Platform/Lang.h"

//---------------------------------------------------------------------------------------------
// Message Box Reciever Impl
//---------------------------------------------------------------------------------------------
@implementation MessageBoxRecieverImpl
{
}
- (id) init
{
    [super init];
    m_buttonIndex = -1;

   return self;
}
//------------------------
- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    m_buttonIndex = buttonIndex;
}
//------------------------
- (int) getButtonIndex
{
    return m_buttonIndex;
}
//------------------------
@end


//---------------------------------------------------------------------------------------------
// Message Box Impl
//---------------------------------------------------------------------------------------------
@implementation MessageBoxImpl
//------------------------
+ (void) CreateInfoBox:(const char *)title text:(const char *)text
{
    UIAlertView* alert = [[UIAlertView alloc] init];

    [alert setTitle: [NSString stringWithUTF8String:title]];
    [alert setMessage: [NSString stringWithUTF8String:text]];
    [alert addButtonWithTitle: NSLocalizedString( @"OK", @"" <#comment#>) ];
    [alert show];
    
//    cocos2d::CCDirector::sharedDirector()->pause();
//    SoundEngine::Get()->PauseBackgroundMusic();
//
//    while ( [alert isVisible] )
//    {
//        [[NSRunLoop currentRunLoop ] runUntilDate:( [ NSDate dateWithTimeIntervalSinceNow:(0.05) ] )];
//    }
//    cocos2d::CCDirector::sharedDirector()->resume();
//    SoundEngine::Get()->ResumeBackgroundMusic();
}
//------------------------
+ (bool) CreateYesNoBox:(const char *)title text:(const char *)text
{
    UIAlertView* alert = [[UIAlertView alloc] init];
    MessageBoxRecieverImpl *reciever = [[[ MessageBoxRecieverImpl alloc] init] autorelease ];
    
    [alert setTitle: [NSString stringWithUTF8String:title]];
    [alert setMessage: [NSString stringWithUTF8String:text]];
    [alert addButtonWithTitle: [NSString stringWithUTF8String:LocalString("YesAnswer")]];
    [alert addButtonWithTitle: [NSString stringWithUTF8String:LocalString("NoAnswer")]];
    [alert setDelegate: reciever ];
    [alert show];
    
    //-----------------
    // Pause all
    cocos2d::CCDirector::sharedDirector()->pause();
    
    if ( ! Game::Get()->GetGameStatus()->GetMusicMute() )
        SoundEngine::Get()->PauseBackgroundMusic();

    //-----------------
    // Create msg box
    while ( [reciever getButtonIndex] == -1 )
    {
        [[NSRunLoop currentRunLoop ] runUntilDate:( [ NSDate dateWithTimeIntervalSinceNow:(0.05) ] )];
    }

    //-----------------
    // Resume all
    cocos2d::CCDirector::sharedDirector()->resume();
    SoundEngine::Get()->ResumeBackgroundMusic();

    
    if ( [ reciever getButtonIndex ] == 0 )
        return true;
    
    return false;
}
@end

//