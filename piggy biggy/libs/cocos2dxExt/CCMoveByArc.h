#ifndef __piggy_biggy__CCMoveByArc__
#define __piggy_biggy__CCMoveByArc__
//------------------------------------------------------------------------------------------
#include "cocos2d.h"
USING_NS_CC;
//------------------------------------------------------------------------------------------
class CCMoveByArc : public CCBezierBy
{
public:
    static CCBezierBy* actionWithDuration(ccTime time,
                                          const CCPoint& startPos,
                                          const CCPoint& endPos,
                                          float radius );
    
};
//------------------------------------------------------------------------------------------
class CCMoveByCircle : public CCBezierBy
{
public:
    static CCMoveByCircle* actionWithDuration(ccTime time, const CCPoint& center,float angleDelta );
    bool initWithDuration(ccTime time, const CCPoint& centerPoint, float angleDelta );
    
    void startWithTarget(CCNode *pTarget);
    void update(ccTime time);

private:
//    CCPoint _startPos;
    CCPoint _centerPoint;
    float   _angleDelta;
};
//------------------------------------------------------------------------------------------
#endif

