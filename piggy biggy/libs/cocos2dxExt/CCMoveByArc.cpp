#include "CCMoveByArc.h"
#include "Tools.h"
#include "Debug/MyDebug.h"
//------------------------------------------------------------------------------------
CCBezierBy* CCMoveByArc::actionWithDuration(ccTime time, const CCPoint& startPos, const CCPoint& endPos, float radius )
{
    float xs;
    float ys;
    float r;
        
    xs = 0.0f;
    ys = 0.0f;
    
    ccBezierConfig bezier;
    bezier.controlPoint_1 = ccp(xs, ys+radius);
    bezier.controlPoint_2 = ccp(xs+radius, ys+radius);
    bezier.endPosition = ccp( xs+radius,ys);
        
        
    return CCBezierBy::actionWithDuration( time, bezier );
}







//------------------------------------------------------------------------------------
CCMoveByCircle* CCMoveByCircle::actionWithDuration(ccTime time, const CCPoint& center, float angleDelta )
{
	CCMoveByCircle *moveBy = new CCMoveByCircle();
	moveBy->initWithDuration(time, center, angleDelta);
	moveBy->autorelease();

	return moveBy;
}
//------------------------------------------------------------------------------------
bool CCMoveByCircle::initWithDuration(ccTime time,
                                      const CCPoint& center,
                                      float angleDelta )
{
	if (CCActionInterval::initWithDuration(time))
	{
        _centerPoint = center;
        _angleDelta = angleDelta;
        
        return true;
	}

	return false;
}
//------------------------------------------------------------------------------------
void CCMoveByCircle::startWithTarget(CCNode *pTarget)
{
	CCActionInterval::startWithTarget(pTarget);
//	_startPos = pTarget->getPosition();
}
//------------------------------------------------------------------------------------
void CCMoveByCircle::update(ccTime time)
{
	if (m_pTarget)
	{
        CCPoint pos;

        pos = m_pTarget->getPosition();
        D_POINT(pos);
        
        pos = Tools::RotatePointAroundPoint( pos, _centerPoint, _angleDelta);
        D_POINT(pos);
		m_pTarget->setPosition(pos);
	}
}