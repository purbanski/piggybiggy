#ifndef __SIMPLEAUDIOLISTENER_H__
#define __SIMPLEAUDIOLISTENER_H__

class SimpleAudioListener
{
public:
    virtual void EffectFinished( unsigned int soundId) = 0;
};

#endif
