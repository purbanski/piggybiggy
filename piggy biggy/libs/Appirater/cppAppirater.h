#ifndef __piggy_biggy__cppAppirater__
#define __piggy_biggy__cppAppirater__
//----------------------------------------------------------------
class ccpAppirater
{
public:
    static void UserDidSignificantEvent( bool show = true );
    static void RateApp();
    static void RestartTracking();
    static int GetUsesCount();
    static int GetUserSignalCount();
};
//----------------------------------------------------------------
#endif