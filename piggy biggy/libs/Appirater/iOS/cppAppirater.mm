#include "cppAppirater.h"
#include "Appirater.h"
//----------------------------------------------------------------
void ccpAppirater::UserDidSignificantEvent( bool show )
{
    [ Appirater userDidSignificantEvent:show ];
}
//----------------------------------------------------------------
void ccpAppirater::RateApp()
{
    [ Appirater rateApp ];
}
//----------------------------------------------------------------
int ccpAppirater::GetUsesCount()
{
    return [ Appirater getUsesCount ];
}
//----------------------------------------------------------------
void ccpAppirater::RestartTracking()
{
    [ Appirater restartTracking ];
}
//----------------------------------------------------------------
int ccpAppirater::GetUserSignalCount()
{
    return [ Appirater getUserSignalCount ];
}
//----------------------------------------------------------------