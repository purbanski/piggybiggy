#ifndef __REPORT_H__
#define __REPORT_H__

#include "GameTypes.h"

using namespace GameTypes;

#ifdef BUILD_TESTFLIGHT

#define TFREPORT(...)	{ Report::Get()->Check( __VA_ARGS__ ); }

//------------------------------------------------------------
class Report
{
public:
    static Report* Get();
    static void Destroy();

    ~Report();

    void Check( const char* msg, ... );

private:
    static Report* _sInstance;
    
    static const char* Rep_LevelStarted;
    static const char* Rep_LevelCompleted;
    static const char* Rep_LevelFailed;
    
private:
    Report();
};
//------------------------------------------------------------

#else

#define TFREPORT(...)

#endif

#endif