#include "Report.h"
#include <stddef.h>
#include <sstream>
//-------------------------------------------------------------
Report* Report::_sInstance = NULL;

const char* Report::Rep_LevelStarted    = "Level Started";
const char* Report::Rep_LevelFailed     = "Level Failed";
const char* Report::Rep_LevelCompleted  = "Level Completed";
//-------------------------------------------------------------
Report* Report::Get()
{
    if ( ! _sInstance )
        _sInstance = new  Report();

    return _sInstance;
}
//-------------------------------------------------------------
void Report::Destroy()
{
    if ( _sInstance )
        delete _sInstance;
    
    _sInstance = NULL;
}
//-------------------------------------------------------------
Report::Report()
{

}
//-------------------------------------------------------------
Report::~Report()
{
    
}
//-------------------------------------------------------------
void Report::Check( const char *aMsg, ... )
{
    char msg[1024];
	memset( msg, 0, sizeof( char ) * 1024 );

	va_list args;
	va_start( args,  aMsg );
	vsnprintf( msg, 1024, aMsg, args );
	va_end(args);

    NSString *checkPoint;
    checkPoint = [ NSString stringWithUTF8String: msg ];
    
    [TestFlight passCheckpoint:checkPoint ];

}