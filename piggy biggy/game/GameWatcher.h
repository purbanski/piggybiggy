#ifndef __PU_GAMEWATCHER_H__
#define __PU_GAMEWATCHER_H__
//------------------------------------------------------------------------
#include <map>
#include "GameTypes.h"
#include "Promotion/FBPoster.h"
//------------------------------------------------------------------------
using namespace std;
using namespace GameTypes;

class GameWatcher
{
public:
    static GameWatcher* Get();
    static void Destroy();
    
    ~GameWatcher();
    void RecordEvent( GameWatcherEvents event, void *data );
    void Dump();
    
private:
    GameWatcher();
    void Process();
    void SetActivateLimits();
    
    struct Activate
    {
        static void LevelCompleted();
        static void LevelSkipped();
        static void FBScoreUpdate();
    };
    
    
private:
    static GameWatcher* _sInstance;

    typedef map<LevelTag, FBPoster::FBPostEnum> EventToFacebookMap;
    typedef map<GameWatcherEvents, RangeInt>  EventRangeMap;
    typedef map<GameWatcherEvents, int> EventMap;
    
	typedef void ( *FnPtr )();
	typedef map<GameWatcherEvents, FnPtr > ActivateFnMap;
    
    EventMap        _eventActivateCountMap;
    EventRangeMap   _eventRandomLimitsMap;
    ActivateFnMap   _activateFnMap;
    EventToFacebookMap  _tagToPostMap;
    
};
//------------------------------------------------------------------------
#endif
