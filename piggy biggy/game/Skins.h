#ifndef __SKINS_H__
#define __SKINS_H__

#include <string>
#include "GameTypes.h"

using namespace std;

//----------------------------------------------------------------------
class Skins
{
public:
	static const char* GetSkinName( const char *file );
	static void Destroy();
	static void SetSkinName( const char *skinName );
    static void SetSkinName( GameTypes::PocketType pocket );
	
	~Skins();

private:
	Skins();
	void ClearFullpath();
	void ClearFilename();

private:
	static Skins		*_sInstance;
	static const int	SKIN_FULLPATH_SIZE	= 128;
	static const int	SKIN_FILENAME_SIZE	= 32;
	
	char	_skinFullpath[SKIN_FULLPATH_SIZE];
	char	_skinSkinName[SKIN_FILENAME_SIZE];
};
//----------------------------------------------------------------------
#endif
