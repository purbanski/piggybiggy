#ifndef RENDER_H
#define RENDER_H

#include "Box2D/Box2D.h"

struct b2AABB;

class GLESDebugDraw : public b2DebugDraw
{
public:
	static GLESDebugDraw* Get();

	virtual void DrawPolygon(const b2Vec2* vertices, int vertexCount, const b2Color& color);
	virtual void DrawSolidBox(const b2Vec2* vertices, const b2Color& color, float alpha ); 
	virtual void DrawSolidPolygon(const b2Vec2* vertices, int vertexCount, const b2Color& color, float alpha ); 
	virtual void DrawSolidPolygon(const b2Vec2* vertices, int vertexCount, const b2Color& color );
	virtual void DrawCircle(const b2Vec2& center, float32 radius, const b2Color& color);
	virtual void DrawSolidCircle(const b2Vec2& center, float32 radius, const b2Vec2& axis, const b2Color& color);
	virtual void DrawSegment(const b2Vec2& p1, const b2Vec2& p2, const b2Color& color);
	virtual void DrawTransform(const b2Transform& xf);
    virtual void DrawPoint(const b2Vec2& p, float32 size, const b2Color& color);
    virtual void DrawString(int x, int y, const char* string, ...); 
    virtual void DrawAABB(b2AABB* aabb, const b2Color& color);

	void LockGL();
	void UnlockGL();

	void SetRatio( float ratio ) { mRatio = ratio; }

private:
	GLESDebugDraw();
	GLESDebugDraw( float32 ratio );

	static GLESDebugDraw* _sInstance;
	
	float32 &mRatio;

	
	float	_glLineWidth;
	bool	_glBlending;
	int		_glBledingFunction;
};


#endif
