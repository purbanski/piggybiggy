#ifdef BUILD_TEST

#include <time.h>
#include "unDebug.h"
#include "MyDebug.h"
#include "DebugTimer.h"
#include "DebugLogger.h"
#include "Tools.h"
#include <sstream>

//--------------------------------------------------------------------------
DebugTimer* DebugTimer::_sInstance = NULL;
//--------------------------------------------------------------------------
DebugTimer* DebugTimer::Get()
{
	if ( ! _sInstance )
	{
		_sInstance = new DebugTimer();
	}
	return _sInstance;
}
//--------------------------------------------------------------------------
void DebugTimer::Destroy()
{
	if ( _sInstance	)
		delete _sInstance;
	
	_sInstance = NULL;
}
//--------------------------------------------------------------------------
DebugTimer::DebugTimer()
{

}
//--------------------------------------------------------------------------
void DebugTimer::Start( IdType id )
{
#ifdef CC_UNDER_IOS
	time_t		nowTime;
	timespec	nowTimeSpec;

//	clock_gettime( CLOCK_REALTIME, &nowTimeSpec );
	time( &nowTime );

	if ( _timeMap.count( id ) > 0 )
	{
		unAssertMsg( DebugTimer, false, ("Timer problem. Id %l already exist", id ));
		DebugLogger::Get( DebugLogger::LogTimers )->Write( "Id %i exists" , id  );
		return;
	}
	//-------------------------------
	tm			*nowFormated;
	TimeTotal	ttime;

	nowFormated = localtime( &nowTime );
 
	stringstream ss;
	ss << nowTimeSpec.tv_sec << ":" ;
	ss << nowTimeSpec.tv_nsec << "] [Start:" << id << "] " ;

	ttime = TimeTotal( nowTime, nowTimeSpec );

	_timeMap[ id ] = ttime;
#endif
}
//--------------------------------------------------------------------------
void DebugTimer::Stop( IdType id )
{
	time_t		nowTime;
	timespec	nowTimeSpec;

	//clock_gettime( CLOCK_REALTIME, &nowTimeSpec );
	time( &nowTime );
	//---------------------------------------
	if ( _timeMap.count( id ) != 1  )
	{
		unAssertMsg( DebugTimer, false, ( "Timer problem. Wrong Id count: %d", _timeMap.count( id ) ));
		DebugLogger::Get( DebugLogger::LogTimers )->Write( "Id problem. Wrong count %d", _timeMap.count(id)  );
		return;
	}

	//---------------------------------------
	tm			*nowFormated;
	TimeTotal	ttime;
	DebugTimer::TimeMap::iterator it;

	ttime = _timeMap[ id ];
	
	it = _timeMap.find( id );
	if ( it != _timeMap.end() )
		_timeMap.erase( it );


	nowFormated = localtime( &nowTime );

	stringstream ss;
	//ss << "[" << id << "]";
	//ss << nowTimeSpec.tv_sec << ":" ;
	//ss << nowTimeSpec.tv_nsec << "]\t";
	ss << " " << Tools::GetTimeDiffrence( nowTimeSpec, ttime._timeSpec );
	ss << " ";
	if ( _timerLabels.count(id) > 0 )
		ss << _timerLabels[id];
	else
		ss<< "noname" ;

	ss << " " ;
	
	_messages.push_back( ss.str());
}
//--------------------------------------------------------------------------
void DebugTimer::InsertEmptyLine()
{
	_messages.push_front( string(""));
}
//--------------------------------------------------------------------------
void DebugTimer::Flush()
{
	if ( _messages.size() > 0 )
	{
		for ( Strings::iterator it = _messages.begin(); it != _messages.end(); it++ )
		{
			D_CSTRING( it->c_str() );
			DebugLogger::Get( DebugLogger::LogTimers )->Write( (*it).c_str() );
		}
		_messages.clear();
	}
}
//--------------------------------------------------------------------------
void DebugTimer::SetLabel( IdType id, const char *label )
{
	_timerLabels[id] = string( label );
}
//--------------------------------------------------------------------------



//--------------------------------------------------------------------------
// TimeTotal
//--------------------------------------------------------------------------
TimeTotal::TimeTotal()
{
	_timeSpec.tv_nsec = 0;
	_timeSpec.tv_sec = 0;
	_time = 0;
}
//--------------------------------------------------------------------------
TimeTotal::TimeTotal( time_t time, timespec timeSpec )
{
	_time = time;
	_timeSpec = timeSpec;
}
//--------------------------------------------------------------------------


#endif
