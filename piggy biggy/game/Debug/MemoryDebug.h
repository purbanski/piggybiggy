#ifndef __MEMORYDEBUG_H__
#define __MEMORYDEBUG_H__

#include "cocos2d.h"

USING_NS_CC;

class MemoryDebug : public CCNode
{
public:
	static MemoryDebug* Create();
	void Step( ccTime dTime );

private:
	MemoryDebug();
	void UpdateDisplay();

private:
	void Init();

private:
	CCLabelBMFont	*_label1;
	CCLabelBMFont	*_label2;
	float			_time;
};
#endif
