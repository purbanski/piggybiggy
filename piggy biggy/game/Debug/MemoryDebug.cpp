#include <sstream>
#include "MemoryDebug.h"
#include "GameConfig.h"
#include "Tools.h"

//---------------------------------------------------------------------
MemoryDebug* MemoryDebug::Create()
{
	MemoryDebug* node;
	node = new MemoryDebug();
	if ( node )
	{
		node->autorelease();
		node->Init();
		return node;
	}
	CC_SAFE_DELETE(node);
	return NULL;
}
//---------------------------------------------------------------------
MemoryDebug::MemoryDebug()
{
}
//---------------------------------------------------------------------
void MemoryDebug::Init()
{
	_label1 = CCLabelBMFont::labelWithString("", Config::MemoryViewFont );
	_label2 = CCLabelBMFont::labelWithString("", Config::MemoryViewFont );

	float y = 10.0f;
	_time = 0.0f;

	_label1->setPosition( 0.0f, y );
	_label2->setPosition( 0.0f, -y );

	addChild( _label1 );
	addChild( _label2 );

	UpdateDisplay();
	schedule( schedule_selector(MemoryDebug::Step ));
}
//---------------------------------------------------------------------
void MemoryDebug::Step( ccTime dTime )
{
	_time += dTime;
	if ( _time < 1.0f )
		return;

	_time = 0.0f;
	UpdateDisplay();
	
}
//---------------------------------------------------------------------
void MemoryDebug::UpdateDisplay()
{
	stringstream ss;
	int freeMem;
	int lfbMem;
	int usedMem;
	int sizeMem;

	//freeMem		= s3eMemoryGetInt( S3E_MEMORY_FREE );
	//lfbMem		= s3eMemoryGetInt( S3E_MEMORY_LFB );
	//usedMem		= s3eMemoryGetInt( S3E_MEMORY_USED );
	//sizeMem		= s3eMemoryGetInt( S3E_MEMORY_SIZE );

//	freeMem		= IwMemBucketGetFree( GameTypes::MEMBUCKET_FIXED );
//	lfbMem		= IwMemBucketGetLargestFreeBlock( GameTypes::MEMBUCKET_FIXED );
//	usedMem		= IwMemBucketGetUsed( GameTypes::MEMBUCKET_FIXED );
//	//sizeMem		= IwMemBucketGetUsed( GameTypes::MEMBUCKET_FIXED );
//	sizeMem		= 0;

    freeMem		= 0;
	lfbMem		= 0;
	usedMem		= 0;
    sizeMem		= 0;

	string strFree	= Tools::IntToDottedString( freeMem );
	string strLbf	= Tools::IntToDottedString( lfbMem );
	string strUsed	= Tools::IntToDottedString( usedMem );
	string strSize	= Tools::IntToDottedString( sizeMem );

	string msg;
	msg.append("LBF: ");
	msg.append( strLbf );
	msg.append("   Free: ");
	msg.append( strFree );
	msg.append("   Used: ");
	msg.append( strUsed );
	msg.append("   Size: ");
	msg.append( strSize );

	_label1->setString( msg.c_str() );
}

//---------------------------------------------------------------------
