#ifndef __FILELOGGER_H__
#define __FILELOGGER_H__
//------------------------------------------------------
#include <string>
#include "unFile.h"
#include <vector>
#include <map>
//------------------------------------------------------
using namespace std;
//------------------------------------------------------
#ifndef BUILD_TEST

#define D_WRITE 
#define D_WRITE_MEMINFO
#define D_WRITE_RESET( filename ) 

#else

#define D_WRITE( x ) { \
	char buf[512]; \
	/* sprintf( buf, "[0x%p] [%s:%i]\t[%s] : %s",  this, Tools::StripTillBackslash( __FILE__).c_str(), __LINE__, __FUNCTION__ , x  ); */ \
	sprintf( buf, " %s", x  ); \
	DebugLogger::Get()->Write( buf ); \
	} 

#define D_WRITECUSTOM( filename, x ) { \
	char buf[512]; \
	sprintf( buf, " %s", x  ); \
	DebugLogger::Get( filename )->Write( buf ); \
} 

#define D_WRITE_MEMINFO { DebugLogger::Get()->WriteMemInfo(); }
#define D_WRITE_RESET( filename )  { DebugLogger::Get( filename )->Reset(); }

//------------------------------------------------------
class DebugLogger
{
public:
	static const char *LogTestRun;
	static const char *LogTimers;
	static const char *LogMemory;
	static const char *LogNotifications;
	static const char *LogTestDuration;

	static DebugLogger* Get();
	static DebugLogger* Get( const char *filename );
	static void Destroy();
	
	~DebugLogger();
	void Reset();
	void Write( const char* msg, ... );
	void WriteMemInfo();

private:
	DebugLogger( const char* filename );

	void FlushMessages();
	void Truncate();

private:
	typedef map<string,DebugLogger*> DebugLoggersContainer;
	static DebugLoggersContainer _sLoggers;

private:
	typedef vector<string> Strings;
	string _filename;
	Strings _msgs;
};
//------------------------------------------------------
#endif

#endif
