#include "DebugNode.h"
#include "GameConfig.h"
#include "Tools.h"

//--------------------------------------------------------------------------------------------------

DebugNode* DebugNode::_sInstance = NULL;

//--------------------------------------------------------------------------------------------------
DebugNode* DebugNode::Get()
{
	if ( ! _sInstance  )
	{
		_sInstance = new DebugNode();

	}
	return _sInstance;
}
//--------------------------------------------------------------------------------------------------
DebugNode::DebugNode()
{
	_label = CCLabelBMFont::labelWithString( "", Config::DebugNodeFont );
	_label->setPosition( Tools::GetScreenMiddle() );
	addChild( _label );

	Print("Debug node");
	setPosition( ccp( 0.0f, 0.0f ));
}
//--------------------------------------------------------------------------------------------------
void DebugNode::Print( const char *aMsg, ... )
{
	char msg[1024];
	memset( msg, 0, sizeof( char ) * 1024 );

	va_list args;
	va_start( args,  aMsg );
	vsnprintf( msg, 1024, aMsg, args );
	va_end(args);

	_label->setString( msg );
}
//--------------------------------------------------------------------------------------------------
void DebugNode::PrintAdd( char *aMsg, ... )
{
	char msg[1024];
	memset( msg, 0, sizeof( char ) * 1024 );

	va_list args;
	va_start( args,  aMsg );
	vsprintf( msg, aMsg, args );
	va_end(args);

	string oldstr;
	oldstr.append( _label->getString());
	oldstr.append("\n");
	oldstr.append( msg );

	_label->setString( oldstr.c_str() );
}
//--------------------------------------------------------------------------------------------------


