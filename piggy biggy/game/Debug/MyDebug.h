#ifndef __PUDEBUG_H__
#define __PUDEBUG_H__

//#include <s3eDebug.h>
#include <string>
#include <set>
#include "unDebug.h"

using namespace std;

#ifdef PU_DEBUG
//---------------------------------------------------------------------------------------------------
class Debug
{
public:
	static int ConstructorCount;
	static int DestructorCount;
	
    static const int _sDebugTextBufferSize        = 512;
    static const int _sDebugHeaderAndTextSize     = 540;
    
	static char _sDebugText[_sDebugTextBufferSize];
	static char _sDebugHeaderAndText[_sDebugHeaderAndTextSize];

	static void Save( const char* buff );
	static void Save( const char* buff, const char* filename );

	static void Init();
	static void Init( const char* filename );

	static const char* DebugFile;
	static const char* MissingSpritesFile;

	static set<string> _missingSprites;
};
//---------------------------------------------------------------------------------------------------
#define D_FLUSH		Debug::_missingSprites.clear();
#define D_INIT		Debug::Init();
//---------------------------------------------------------------------------------------------------
#define  D_HIT	{ \
					memset( Debug::_sDebugHeaderAndText, 0, sizeof( char ) * 540 );	 \
					snprintf( Debug::_sDebugHeaderAndText,  Debug::_sDebugHeaderAndTextSize, "DEBUG[%s] : --- HIT ---\n", __FUNCTION__ ); \
					unDebugOutputString( Debug::_sDebugHeaderAndText ); \
				}
//------------------------------------------------------------------------------------------------------------
#define D_LOG(...)	{ \
						memset( Debug::_sDebugText, 0, sizeof( char ) * 512 ); \
						memset( Debug::_sDebugHeaderAndText, 0, sizeof( char ) * 540 );	 \
						\
						snprintf( Debug::_sDebugText,           Debug::_sDebugTextBufferSize, __VA_ARGS__ ); \
						snprintf( Debug::_sDebugHeaderAndText,  Debug::_sDebugHeaderAndTextSize, "DEBUG[%s] : %s\n", __FUNCTION__, Debug::_sDebugText ); \
						unDebugOutputString( Debug::_sDebugHeaderAndText ); \
					}
//------------------------------------------------------------------------------------------------------------
#define D_QLOG( param )	{ \
					memset( Debug::_sDebugText, 0, sizeof( char ) * 512 ); \
					memset( Debug::_sDebugHeaderAndText, 0, sizeof( char ) * 540 );	 \
					\
					snprintf( Debug::_sDebugText,           Debug::_sDebugTextBufferSize,  #param ); \
					snprintf( Debug::_sDebugHeaderAndText, Debug::_sDebugHeaderAndTextSize, "DEBUG[%s] : %s\n", __FUNCTION__, Debug::_sDebugText ); \
					unDebugOutputString( Debug::_sDebugHeaderAndText ); \
				}
//------------------------------------------------------------------------------------------------------------
#define DS_LOG(...)	{ \
						memset( Debug::_sDebugText, 0, sizeof( char ) * 512 ); \
						memset( Debug::_sDebugHeaderAndText, 0, sizeof( char ) * 540 );	 \
						\
						snprintf( Debug::_sDebugText,           Debug::_sDebugTextBufferSize, __VA_ARGS__ ); \
						snprintf( Debug::_sDebugHeaderAndText,  Debug::_sDebugHeaderAndTextSize, "DEBUG[%s] : %s\n", __FUNCTION__, Debug::_sDebugText ); \
						Debug::Save( Debug::_sDebugHeaderAndText ); \
					}
//------------------------------------------------------------------------------------------------------------
#define DS_LOG_MISSING_PNG(...)	{ \
						memset( Debug::_sDebugText, 0, sizeof( char ) * 512 ); \
						memset( Debug::_sDebugHeaderAndText, 0, sizeof( char ) * 540 );	 \
						\
						snprintf( Debug::_sDebugText,           Debug::_sDebugTextBufferSize, __VA_ARGS__ ); \
						snprintf( Debug::_sDebugHeaderAndText,  Debug::_sDebugHeaderAndTextSize, "%s\n", Debug::_sDebugText ); \
						if ( ! Debug::_missingSprites.count( string( Debug::_sDebugText ))) \
						{ \
								Debug::Save( Debug::_sDebugHeaderAndText, Debug::MissingSpritesFile ); \
								Debug::_missingSprites.insert( string( Debug::_sDebugText )); \
						} \
						}
//------------------------------------------------------------------------------------------------------------
#define DS_LOG_MISSING_PNG_RESET	Debug::_missingSprites.clear();
//------------------------------------------------------------------------------------------------------------
#define DS_LOG_MISSING_PNG_PLAIN(...)	{ \
						memset( Debug::_sDebugText, 0, sizeof( char ) * 512 ); \
						memset( Debug::_sDebugHeaderAndText, 0, sizeof( char ) * 540 );	 \
						\
						snprintf( Debug::_sDebugText,           Debug::_sDebugTextBufferSize, __VA_ARGS__ ); \
						snprintf( Debug::_sDebugHeaderAndText,  Debug::_sDebugHeaderAndTextSize, "%s\n", Debug::_sDebugText ); \
						Debug::Save( Debug::_sDebugHeaderAndText, Debug::MissingSpritesFile ); \
					}
//------------------------------------------------------------------------------------------------------------
#define DS_LOG_PLAIN(...)	{ \
						memset( Debug::_sDebugText, 0, sizeof( char ) * 512 ); \
						memset( Debug::_sDebugHeaderAndText, 0, sizeof( char ) * 540 );	 \
						\
						snprintf( Debug::_sDebugText,           Debug::_sDebugTextBufferSize, __VA_ARGS__ ); \
						snprintf( Debug::_sDebugHeaderAndText,  Debug::_sDebugHeaderAndTextSize, "%s\n", Debug::_sDebugText ); \
						Debug::Save( Debug::_sDebugHeaderAndText ); \
					}
//------------------------------------------------------------------------------------------------------------
#define D_SEPERATOR { \
						memset( Debug::_sDebugText, 0, sizeof( char ) * 512 ); \
						memset( Debug::_sDebugHeaderAndText, 0, sizeof( char ) * 540 );	 \
						\
						snprintf( Debug::_sDebugText,           Debug::_sDebugTextBufferSize, "------------------------------------------------------------------" ); \
						snprintf( Debug::_sDebugHeaderAndText,  Debug::_sDebugHeaderAndTextSize, "DEBUG[%s] : %s\n", __FUNCTION__, Debug::_sDebugText ); \
						unDebugOutputString( Debug::_sDebugHeaderAndText ); \
					}
//------------------------------------------------------------------------------------------------------------
#define DS_SEPERATOR { \
						memset( Debug::_sDebugText, 0, sizeof( char ) * 512 ); \
						memset( Debug::_sDebugHeaderAndText, 0, sizeof( char ) * 540 );	 \
						\
						snprintf( Debug::_sDebugText,           Debug::_sDebugTextBufferSize, "------------------------------------------------------------------" ); \
						snprintf( Debug::_sDebugHeaderAndText,  Debug::_sDebugHeaderAndTextSize, "DEBUG[%s] : %s\n", __FUNCTION__, Debug::_sDebugText ); \
						Debug::Save( Debug::_sDebugHeaderAndText ); \
					}
//------------------------------------------------------------------------------------------------------------
#define D_STRING( param )	{ \
							memset( Debug::_sDebugText, 0, sizeof( char ) * 512 ); \
							memset( Debug::_sDebugHeaderAndText, 0, sizeof( char ) * 540 );	 \
							\
							snprintf( Debug::_sDebugText,           Debug::_sDebugTextBufferSize, "%s()=%s", #param, param.c_str() ); \
							snprintf( Debug::_sDebugHeaderAndText,  Debug::_sDebugHeaderAndTextSize, "DEBUG[%s] : %s\n", __FUNCTION__, Debug::_sDebugText ); \
							unDebugOutputString( Debug::_sDebugHeaderAndText ); \
						}
//------------------------------------------------------------------------------------------------------------
#define DS_STRING( param )	{ \
							memset( Debug::_sDebugText, 0, sizeof( char ) * 512 ); \
							memset( Debug::_sDebugHeaderAndText, 0, sizeof( char ) * 540 );	 \
							\
							snprintf( Debug::_sDebugText,           Debug::_sDebugTextBufferSize, "%s()=%s", #param, param.c_str() ); \
							snprintf( Debug::_sDebugHeaderAndText,  Debug::_sDebugHeaderAndTextSize, "DEBUG[%s] : %s\n", __FUNCTION__, Debug::_sDebugText ); \
							Debug::Save( Debug::_sDebugHeaderAndText ); \
						}
//------------------------------------------------------------------------------------------------------------
#define D_CSTRING( param )	{ \
							memset( Debug::_sDebugText, 0, sizeof( char ) * 512 ); \
							memset( Debug::_sDebugHeaderAndText, 0, sizeof( char ) * 540 );	 \
							\
							snprintf( Debug::_sDebugText,           Debug::_sDebugTextBufferSize, "%s()=%s", #param, param ); \
							snprintf( Debug::_sDebugHeaderAndText,  Debug::_sDebugHeaderAndTextSize, "DEBUG[%s] : %s\n", __FUNCTION__, Debug::_sDebugText ); \
							unDebugOutputString( Debug::_sDebugHeaderAndText ); \
						}
//------------------------------------------------------------------------------------------------------------
#define D_SIZE( param )	{ \
						memset( Debug::_sDebugText, 0, sizeof( char ) * 512 ); \
						memset( Debug::_sDebugHeaderAndText, 0, sizeof( char ) * 540 );	 \
						\
						snprintf( Debug::_sDebugText,           Debug::_sDebugTextBufferSize, "%s.size()=%d", #param, (int)(param).size() ); \
						snprintf( Debug::_sDebugHeaderAndText,  Debug::_sDebugHeaderAndTextSize, "DEBUG[%s] : %s\n", __FUNCTION__, Debug::_sDebugText ); \
						unDebugOutputString( Debug::_sDebugHeaderAndText ); \
				}
//------------------------------------------------------------------------------------------------------------
#define D_FLOAT( param ) { \
						memset( Debug::_sDebugText, 0, sizeof( char ) * 512 ); \
						memset( Debug::_sDebugHeaderAndText, 0, sizeof( char ) * 540 );	 \
						\
						snprintf( Debug::_sDebugText,           Debug::_sDebugTextBufferSize, "%s=%f", #param, ( param )); \
						snprintf( Debug::_sDebugHeaderAndText,  Debug::_sDebugHeaderAndTextSize, "DEBUG[%s] : %s\n", __FUNCTION__, Debug::_sDebugText ); \
						unDebugOutputString( Debug::_sDebugHeaderAndText ); \
					}
//------------------------------------------------------------------------------------------------------------
#define DS_FLOAT( param ) { \
						memset( Debug::_sDebugText, 0, sizeof( char ) * 512 ); \
						memset( Debug::_sDebugHeaderAndText, 0, sizeof( char ) * 540 );	 \
						\
						snprintf( Debug::_sDebugText,           Debug::_sDebugTextBufferSize, "%s=%f", #param, ( param )); \
						snprintf( Debug::_sDebugHeaderAndText,  Debug::_sDebugHeaderAndTextSize, "DEBUG[%s] : %s\n", __FUNCTION__, Debug::_sDebugText ); \
						Debug::Save( Debug::_sDebugHeaderAndText ); \
					}
//------------------------------------------------------------------------------------------------------------
#define D_INT( param ) { \
						memset( Debug::_sDebugText, 0, sizeof( char ) * 512 ); \
						memset( Debug::_sDebugHeaderAndText, 0, sizeof( char ) * 540 );	 \
						\
						snprintf( Debug::_sDebugText,           Debug::_sDebugTextBufferSize, "%s=%d", #param, ( param )); \
						snprintf( Debug::_sDebugHeaderAndText,  Debug::_sDebugHeaderAndTextSize, "DEBUG[%s] : %s\n", __FUNCTION__, Debug::_sDebugText ); \
						unDebugOutputString( Debug::_sDebugHeaderAndText ); \
					}
//------------------------------------------------------------------------------------------------------------
#define DS_INT( param ) { \
						memset( Debug::_sDebugText, 0, sizeof( char ) * 512 ); \
						memset( Debug::_sDebugHeaderAndText, 0, sizeof( char ) * 540 );	 \
						\
						snprintf( Debug::_sDebugText,           Debug::_sDebugTextBufferSize, "%s=%d", #param, ( param )); \
						snprintf( Debug::_sDebugHeaderAndText,  Debug::_sDebugHeaderAndTextSize, "DEBUG[%s] : %s\n", __FUNCTION__, Debug::_sDebugText ); \
						Debug::Save( Debug::_sDebugHeaderAndText ); \
						}
//------------------------------------------------------------------------------------------------------------
#define D_POINT( param ) { \
						memset( Debug::_sDebugText, 0, sizeof( char ) * 512 ); \
						memset( Debug::_sDebugHeaderAndText, 0, sizeof( char ) * 540 );	 \
						\
						snprintf( Debug::_sDebugText,           Debug::_sDebugTextBufferSize, "%s(x=%f y=%f)", #param, (param).x, (param).y ); \
						snprintf( Debug::_sDebugHeaderAndText,  Debug::_sDebugHeaderAndTextSize, "DEBUG[%s] : %s\n", __FUNCTION__, Debug::_sDebugText ); \
						unDebugOutputString( Debug::_sDebugHeaderAndText ); \
					}
//------------------------------------------------------------------------------------------------------------
#define DS_POINT( param ) { \
						memset( Debug::_sDebugText, 0, sizeof( char ) * 512 ); \
						memset( Debug::_sDebugHeaderAndText, 0, sizeof( char ) * 540 );	 \
						\
						snprintf( Debug::_sDebugText,           Debug::_sDebugTextBufferSize, "%s(x=%f y=%f)", #param, (param).x, (param).y ); \
						snprintf( Debug::_sDebugHeaderAndText,  Debug::_sDebugHeaderAndTextSize, "DEBUG[%s] : %s\n", __FUNCTION__, Debug::_sDebugText ); \
						Debug::Save( Debug::_sDebugHeaderAndText ); \
					}
//------------------------------------------------------------------------------------------------------------
#define D_PTR( param ) { \
						memset( Debug::_sDebugText, 0, sizeof( char ) * 512 ); \
						memset( Debug::_sDebugHeaderAndText, 0, sizeof( char ) * 540 );	 \
						\
						snprintf( Debug::_sDebugText,           Debug::_sDebugTextBufferSize, "%s(%p)", #param, (param) ); \
						snprintf( Debug::_sDebugHeaderAndText,  Debug::_sDebugHeaderAndTextSize, "DEBUG[%s] : %s", __FUNCTION__, Debug::_sDebugText ); \
						unDebugOutputString( Debug::_sDebugHeaderAndText ); \
}
//------------------------------------------------------------------------------------------------------------
#define D_FUNCTION	{ D_LOG( ("%s", __FUNCTION__ )); }
//------------------------------------------------------------------------------------------------------------
#define D_STATS		{ D_LOG( "C: %d  D: %d   C-D: %d\n", Debug::ConstructorCount, Debug::DestructorCount, Debug::ConstructorCount - Debug::DestructorCount ); }
//------------------------------------------------------------------------------------------------------------
#define D_CONSTRUCT	{ \
							Debug::ConstructorCount++; \
							D_FUNCTION; \
							D_STATS \
					}
//------------------------------------------------------------------------------------------------------------
#define D_DESTRUCT	{ \
							Debug::DestructorCount++; \
							D_FUNCTION; \
							D_STATS \
					}
//------------------------------------------------------------------------------------------------------------
#else

#define D_FLUSH
#define D_INIT

#define D_LOG(...)
#define D_QLOG( param )
#define D_STRING( param )
#define D_CSTRING( param )	
#define D_SIZE( param )
#define D_FLOAT( param )
#define D_INT( param )
#define D_POINT( param )
#define D_PTR( param )
#define D_HIT
#define D_FUNCTION
#define D_STATS
#define D_CONSTRUCT
#define D_DESTRUCT
#define D_SEPERATOR

#define DS_LOG( ... )
#define DS_LOG_PLAIN( ... )
#define DS_LOG_MISSING_PNG( ... )
#define DS_LOG_MISSING_PNG_PLAIN( ... )
#define DS_LOG_MISSING_PNG_RESET
#define DS_FLOAT( param )
#define DS_INT( param )
#define DS_POINT( param )
#define DS_SEPERATOR

#endif


	
#endif
