#include "Debug/MyDebug.h"
#include "unFile.h"

#ifdef PU_DEBUG

//------------------------------------------------------------------------------------------------------------------

int Debug::ConstructorCount = 0;
int Debug::DestructorCount = 0;

char Debug::_sDebugText[512];
char Debug::_sDebugHeaderAndText[540];
const char* Debug::DebugFile = "debug_log.txt";
const char* Debug::MissingSpritesFile = "missing_sprites.txt";
set<string> Debug::_missingSprites;

//------------------------------------------------------------------------------------------------------------------
void Debug::Save( const char* buff )
{
	Save( buff, DebugFile );
}
//------------------------------------------------------------------------------------------------------------------
void Debug::Save( const char* buff, const char* filename )
{
	unFile *file = unDocumentFileOpen( filename, "a+b");
	unFileWrite( buff, sizeof( char ), strlen( buff ), file );
	unFileClose( file );
}
//------------------------------------------------------------------------------------------------------------------
void Debug::Init()
{
	Init( DebugFile );
}
//------------------------------------------------------------------------------------------------------------------
void Debug::Init( const char* filename )
{
	unFile *file = unDocumentFileOpen( filename, "wb");
	unFileClose( file );
}
//------------------------------------------------------------------------------------------------------------------

#endif

