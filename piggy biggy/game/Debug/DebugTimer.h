#ifndef __DEBUGTIMER_H__
#define __DEBUGTIMER_H__

#ifdef BUILD_TEST
//--------------------------------------------------------------------------
#include <list>
#include <string>
#include <map>
//--------------------------------------------------------------------------
using namespace std;
//--------------------------------------------------------------------------
class TimeTotal 
{
public:
	TimeTotal();
	TimeTotal( time_t time, timespec timeSpec );

public:
	time_t		_time;
	timespec	_timeSpec;
};
//--------------------------------------------------------------------------
class DebugTimer
{
public:
	typedef unsigned int IdType;
	static DebugTimer* Get();
	static void Destroy();
	
	void Start( IdType id );
	void Stop( IdType id );
	void InsertEmptyLine();
	void Flush();
	void SetLabel( IdType id, const char * );

private:
	DebugTimer();

private:
	static DebugTimer	*_sInstance;
	
	typedef list<string> Strings;
	typedef map<IdType, TimeTotal>	TimeMap;
	typedef map<IdType, string>		TimerLabels;

	TimerLabels	_timerLabels;
	Strings		_messages;
	TimeMap		_timeMap;
};
//--------------------------------------------------------------------------
#endif

#endif
