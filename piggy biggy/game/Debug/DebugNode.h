#ifndef __DEBUGNODE_H__
#define __DEBUGNODE_H__

#include "cocos2d.h"

USING_NS_CC;

class DebugNode : public CCNode
{
public:
	static DebugNode* Get();
	
	void Print( const char *msg, ... );
	void PrintAdd( char *msg, ... );

private:
	DebugNode();

private:
	static DebugNode	*_sInstance;
	CCLabelBMFont		*_label;
};
#endif
