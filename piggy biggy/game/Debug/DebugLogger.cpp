#ifdef BUILD_TEST

#include "MyDebug.h"
#include "DebugLogger.h"
#include "unDebug.h"
#include <sstream>
#include <time.h>
//#include <s3eMemory.h>
#include "Tools.h"
#include "unFile.h"
//------------------------------------------------------
DebugLogger::DebugLoggersContainer DebugLogger::_sLoggers;
//------------------------------------------------------
const char *DebugLogger::LogTestRun			= "testRun.log";
const char *DebugLogger::LogTimers			= "createTime.log";
const char *DebugLogger::LogMemory			= "memory.log";
const char *DebugLogger::LogNotifications	= "notification.log";
const char *DebugLogger::LogTestDuration	= "duration.log";
//------------------------------------------------------


DebugLogger* DebugLogger::Get()
{
#ifndef BUILD_TEST
    return -1;
#endif
	
	return Get( DebugLogger::LogTestRun );
}
//------------------------------------------------------
DebugLogger* DebugLogger::Get( const char *filename )
{
#ifndef BUILD_TEST
	return NULL;
#endif
	string file;
	file.append( filename );

	if ( _sLoggers[ file ] == NULL )
		_sLoggers[ file ] = new DebugLogger( file.c_str() );
	
	return _sLoggers[ file ];
}
//------------------------------------------------------
void DebugLogger::Destroy()
{
	DebugLogger *debugger;

	if ( _sLoggers.size() > 0 )
	{
		for ( DebugLoggersContainer::iterator it = _sLoggers.begin(); it != _sLoggers.end(); it++ )
		{
			debugger = it->second;
			if ( debugger )
				delete debugger;

			it->second = NULL;
		}
	}
}
//------------------------------------------------------
DebugLogger::DebugLogger( const char *filename )
{
	_filename.append( filename );
	//Reset();
}
//------------------------------------------------------
void DebugLogger::Write( const char* msg, ... )
{
	const int size = 1024;
	char	buffer[size];
	memset( buffer, 0, sizeof( char ) * size );

	va_list params;
	va_start(params, msg );
	vsprintf( buffer, msg, params );
	va_end( params );

	Truncate();
	string formatedMsg;

	formatedMsg.append( Tools::GetTimeStamp() );
	formatedMsg.append( buffer );
	formatedMsg.append( "\n" );

	_msgs.push_back( string( formatedMsg ));
	
	//if ( _msgs.size() < 10 )
//		return;

	FlushMessages();
	return;

}
//------------------------------------------------------
void DebugLogger::FlushMessages()
{
	unFile *file;
	file = unDocumentFileOpen( _filename.c_str(), "a+" );

	if ( ! file )
	{
		unAssertMsg( DebugLogger, false, ("File can't be opened: %s", _filename.c_str() ));
		return;
	}

	for ( Strings::iterator it = _msgs.begin(); it != _msgs.end(); it++ )
	{
		unFileWrite( it->c_str(), it->length(), 1, file );
	}
	
	unFileClose( file );
	_msgs.clear();
}
//------------------------------------------------------
DebugLogger::~DebugLogger()
{
	FlushMessages();
}
//------------------------------------------------------
void DebugLogger::WriteMemInfo()
{
	stringstream ss;
	int freeMem;
	int lfbMem;
	int usedMem;
	int sizeMem;

//	freeMem		= IwMemBucketGetFree( GameTypes::MEMBUCKET_FIXED );
//	//lfbMem		= IwMemBucketGetLargestFreeBlock( GameTypes::MEMBUCKET_FIXED );
//	usedMem		= IwMemBucketGetUsed( GameTypes::MEMBUCKET_FIXED );
//	sizeMem		= s3eMemoryGetInt( S3E_MEMORY_SIZE );

    freeMem		= 0;
	lfbMem		= 0;
	usedMem		= 0;
	sizeMem		= 0;

	string strFree	= Tools::IntToDottedString( freeMem );
	string strLbf	= Tools::IntToDottedString( lfbMem );
	string strUsed	= Tools::IntToDottedString( usedMem );
	string strSize	= Tools::IntToDottedString( sizeMem );

	string msg;
	msg.append("LBF: ");
	msg.append( strLbf );
	msg.append(",  Free: ");
	msg.append( strFree );
	msg.append(",  Used: ");
	msg.append( strUsed );
	msg.append(",  Size: ");
	msg.append( strSize );

	Write( msg.c_str() );
}
//------------------------------------------------------
void DebugLogger::Reset()
{
	unFile *file;
	file = unDocumentFileOpen( _filename.c_str(), "w" );
	if ( file )
		unFileClose( file );
}
//------------------------------------------------------
void DebugLogger::Truncate()
{
	unFile *file;
	file = unDocumentFileOpen( _filename.c_str(), "r" );
	if ( !file )
		return;

	int32 size;
	size = unFileGetSize( file );
	unFileClose( file );

	D_INT( size );
	if ( size > 5 * 1024 * 1024 )
		Reset();
}
//------------------------------------------------------
#endif
