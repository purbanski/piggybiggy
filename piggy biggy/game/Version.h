#ifndef __VERSION_H__
#define __VERSION_H__

#include <string>

using namespace std;

class Version
{
public:
	static string Game;
	static string GameEditor;
};

#endif
