#ifndef __GAMESTATUS_H__
#define __GAMESTATUS_H__

#include <vector>
#include "LevelFactory.h"
#include "GameTypes.h"
#include "Platform/GameCenter.h"

using namespace std;

//--------------------------------------------------------------------------------------//
// Game Level Sequence
//--------------------------------------------------------------------------------------//
class GameLevelSequence : public vector<GameTypes::LevelEnum>
{
public:
	GameLevelSequence();
	void Dump();
	int GetLevelIndex( GameTypes::LevelEnum level );
};



//--------------------------------------------------------------------------------------//
// Game status
//--------------------------------------------------------------------------------------//
class GameStatus 
{
public:
//--------------------------------------------------------------------------------------//
	typedef enum 
	{
		eLevelStatus_LastOpen = 1,
		eLevelStatus_Done,
		eLevelStatus_Locked,
		eLevelStatus_Skipped
	} LevelStatus;

//--------------------------------------------------------------------------------------//
	typedef struct 
	{
		unsigned int _clueProcessed;
		int _timeout;
		bool _running;
	} LevelClueStatus;
//--------------------------------------------------------------------------------------//
	typedef struct 
	{
		int                         _version;
		GameTypes::LevelEnum		_currentLevel;
		bool						_levelNameShowed[ GameTypes::eLevel_Finish ];
		LevelStatus					_statusTable[ GameTypes::eLevel_Finish ];

		LevelClueStatus				_clueStatusTable[ GameTypes::eLevel_Finish ];
		GameTypes::SecondsHolder	_scoreTable[ GameTypes::eLevel_Finish ];
		bool						_isMusicMute;
		bool						_isEffectsMute;
		//GameTypes::PocketType		_currentPocket;
		GameTypes::PocketStatusType	_pocketStatusTable[ 10 ];
        bool                        _pocketOpenAnimPlayed[ GameTypes::ePocketScience + 1 ];
		bool						_boughtCheck;
		unsigned int				_oneTimeMessageStatus[ GameTypes::eOTMsg_Last ]; // 0 - not played, 1 - played
	} StatusV1;
//--------------------------------------------------------------------------------------//
	typedef struct 
	{
		int                         _version;
		GameTypes::LevelEnum		_currentLevel;
		bool						_levelNameShowed[ GameTypes::eLevel_Finish ];
		LevelStatus					_statusTable[ GameTypes::eLevel_Finish ];

		LevelClueStatus				_clueStatusTable[ GameTypes::eLevel_Finish ];
		GameTypes::SecondsHolder	_scoreTable[ GameTypes::eLevel_Finish ];
		bool						_isMusicMute;
		bool						_isEffectsMute;
		//GameTypes::PocketType		_currentPocket;
		GameTypes::PocketStatusType	_pocketStatusTable[ 10 ];
        bool                        _pocketOpenAnimPlayed[ GameTypes::ePocketScience + 1 ];
		bool						_boughtCheck;
		unsigned int				_oneTimeMessageStatus[ GameTypes::eOTMsg_Last ]; // 0 - not played, 1 - played
        
        //----------------------
        // v1 finish / v2 start
        //----------------------
        bool                        _fbLogged;
        int                         _appRunTimes;
        
        unsigned int                _coinCollected;
        unsigned int                _frigleDestroyed;
        unsigned int                _fliesCut;
        unsigned int                _bombExploded;
        
        LanguageType                _language; // if it equals -1 - then autoprobe
        unsigned int				_levelRestartCount[ GameTypes::eLevel_Finish ];
		unsigned int				_oneTimeMessageRequests[ GameTypes::eOTMsg_Last ]; // 0 - not played, 1 - played
        bool                        _skipUsed;
//        unsigned int				_levelRestartCount[ GameTypes::eLevel_Finish ];
	} StatusV2;
//--------------------------------------------------------------------------------------//
public:
	GameStatus();
	~GameStatus();
	void Save();
	void ResetGameStatus();
	void RandomStatus();
	void UnlockLevels();
	unsigned int GetCoinCount( GameTypes::LevelEnum level );
    unsigned int GetLevelScore( GameTypes::LevelEnum level );
    unsigned int GetLevelTime( GameTypes::LevelEnum level );
    unsigned int GetTotalScore();
    
    void LogStats();
    
	void LevelNext();
	StatusV2* GetStatus() { return &_status; }
	
	//-- Previously Played Level getters/setters
	GameTypes::LevelEnum GetPreviouslyPlayedLevel(){ return _previousPlayedLevel; }
	void NutralizePreviouslyPlayedLevel();
	void ResetPreviouslyPlayedLevel();

	//-- Level name showed
	bool GetLevelNameShowed( GameTypes::LevelEnum level );
	void SetLevelNameShowed( GameTypes::LevelEnum level, bool showed );

	//-- Current Level getters/setters
	GameTypes::LevelEnum GetCurrentLevel(){ return _status._currentLevel; }
	void SetCurrentLevel( GameTypes::LevelEnum level );
	void UpdateCurrentLevel();
	GameTypes::LevelEnum GetNextUndoneLevel();


	//-- Level Status getters/setters
	LevelStatus GetLevelStatus( GameTypes::LevelEnum level );
	LevelStatus GetCurrentLevelStatus();
	void SetLevelStatus( GameTypes::LevelEnum level, LevelStatus status );
	void SetCurrentLevelStatus( LevelStatus status );
	GameLevelSequence& GetLevelSequence(){ return _levelSeq; }
    int GetLevelIndex( GameTypes::LevelEnum level );
    int GetCurrentLevelIndex();
    
	//-- Level Clue Status getters/setters
	LevelClueStatus GetLevelClueStatus( GameTypes::LevelEnum level );
	LevelClueStatus GetCurrentLevelClueStatus();
	void SetLevelClueStatus( GameTypes::LevelEnum level, GameStatus::LevelClueStatus clueStatus );
	void SetCurrentLevelClueStatus( GameStatus::LevelClueStatus clueStatus );
	
	
	//-- Level Pocket Status getters/setters
	void SetCurrentPocket( GameTypes::PocketType pocketEnum );
	GameTypes::PocketType GetCurrentPocket();
	void SetPocketStatus( GameTypes::PocketType pocket, GameTypes::PocketStatusType status );
	GameTypes::PocketStatusType GetPocketStatus( GameTypes::PocketType pocket );
    int GetLockedLevelCount( GameTypes::PocketType pocket );

   	bool GetPocketOpenAnimPlayed( GameTypes::PocketType pocket );
	void SetPocketOpenAnimPlayed( GameTypes::PocketType pocket, bool played );

	//-- One Time Messages 
	bool GetMsgPlayed( GameTypes::OneTimeMsg msg );
	void SetMsgPlayed( GameTypes::OneTimeMsg msg );
	bool GetMsgPlayed( GameTypes::OneTimeMsgRequests msg );
	void SetMsgPlayed( GameTypes::OneTimeMsgRequests msg );

    
	//-- Music & Sound getters/setters
	void SetMusicMute( bool mute );
	void SetEffectsMute( bool mute );
	bool GetMusicMute();
	bool GetEffectsMute();
		
    //-------------------------
    // Language
    LanguageType GetLang();
    void SetLang( LanguageType type );

	//---------------------------
	int GetSkipsLeft() { return _skipsLeft; }
    bool GetSkipUsed();
    void SetSkipUsed();
    
	bool NoMoreSkips();
	void CountSkippedLevels();

	void SetToFirstLevel();
	bool HasGameFinished();

	void GameHasBeenBought();
	void ToggleBilling();
	bool HasBeenBought();

    bool IsFbLogged();
    void SetFbLogged( bool logged );
    
    string GetLevelTimeString( LevelEnum level );

    void IncrementAppRunTime();
    int GetAppRunTime();
    bool ShouldIntroStart();
    
    void IncCoinCollectedCount();
    void IncBombDestroyedCount();
    void IncFrigleDestroyedCount();
    void IncFliesCutCount();
    void IncLevelRestart( LevelEnum level );
    
    unsigned int GetLevelRestartCount( LevelEnum level );
    
private:
	void InitStatusFile();
	void LoadStatusFile();
	bool CheckIfFileExists();
	void ResetStatus();
    void UpdateStatusFileToV2();
    void UpdateLanguage();
	

private:
	int                     _skipsLeft;
	StatusV2                _status;
	GameLevelSequence       _levelSeq;
	GameTypes::LevelEnum	_previousPlayedLevel; // for preloader
    string                  _encryptionKey;
    
};


#endif
