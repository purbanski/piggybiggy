#ifndef __DEBUGDRAW_H__
#define __DEBUGDRAW_H__

#include "cocos2d.h"
#include "Levels/Level.h"
#include "GameConfig.h"

USING_NS_CC;

class DebugDraw : public CCNode
{
public:
	DebugDraw( Level *level )
	{
		_level = level;
		_enable = true;
		//setPosition( ccp( EDITOR_DELTA_X, EDITOR_DELTA_Y ));
	};

	void ToggleDraw()
	{
		_enable = ( _enable ? false : true );
	}

	void EnableDraw( bool enable )
	{
		_enable = enable;
	}

	bool GetEnable()
	{
		return _enable;
	}

	void draw()
	{
		if ( _enable )
			_level->DrawDebugData();
	};

private:
	Level				*_level;
	bool				_enable;
	static	DebugDraw* _instance;

};

#endif
