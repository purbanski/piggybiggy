#include <string>
#include <sstream>
#include "GameGlobals.h"
#include "GameTypes.h"
#include "cocos2d.h"
#include "Debug/MyDebug.h"
#include "Platform/Lang.h"

USING_NS_CC;

using namespace  GameTypes;
//--------------------------------------------------
//Globals
//--------------------------------------------------
unsigned int gAppEventMemoryWarningCount    = 0;
unsigned int gAppEventActiveCount           = 0;
unsigned int gAppEventDeactiveCount         = 0;
unsigned int gAppEventEnterBackgroundCount  = 0;
unsigned int gAppEventnterForegroundCount   = 0;
bool         gGLBufferSwapEnabled           = true;

DisplayRotateType gAppDisplayRotateMode     = eDisplay_LandscapeRotating;
DeviceType gDevice                          = eDevice_unknown;

//--------------------------------------------------

GameGlobals * GameGlobals::_sInstance = NULL;
//--------------------------------------------------
GameGlobals* GameGlobals::Get()
{
    if ( ! _sInstance )
		_sInstance = new GameGlobals();
    
    return _sInstance;
}
//--------------------------------------------------
void GameGlobals::Destroy()
{
	if ( _sInstance )
		delete _sInstance;
	_sInstance = NULL;
}
//--------------------------------------------------
GameGlobals::GameGlobals()
{
	//--------------------------------------------------
	// Stepable blocks
	//--------------------------------------------------
	//_stepableBlockName.insert( eBlockAirBubble			);
	_stepableBlockName.insert( eBlockBankSaveWheel		);
	_stepableBlockName.insert( eBlockBomb				);
	_stepableBlockName.insert( eBlockBombStatic			);
	_stepableBlockName.insert( eBlockChain				);
	_stepableBlockName.insert( eBlockCoinSilver			);
	//_stepableBlockName.insert( eBlockCoinSilverStatic	);
	_stepableBlockName.insert( eBlockBullet				);
	//_stepableBlockName.insert( eBlockMagnet				);
	//_stepableBlockName.insert( eBlockMagnetMinus		);
	//_stepableBlockName.insert( eBlockMagnetPlus			);
	_stepableBlockName.insert( eBlockMagnetBomb			);
	_stepableBlockName.insert( eBlockMagnetBombMinus	);
	_stepableBlockName.insert( eBlockMagnetBombPlus		);


	//--------------------------------------------------
	// Exitable blocks
	//--------------------------------------------------
	_exitableBlockName.insert( eBlockChain	);

    
    //--------------------------------------------------
	// Quitable blocks
	//--------------------------------------------------
   	_quitableBlockName.insert( eBlockChain	);
	_quitableBlockName.insert( eBlockBomb	);
    _quitableBlockName.insert( eBlockBombStatic	);
    _quitableBlockName.insert( eBlockMagnetBomb );
    _quitableBlockName.insert( eBlockMagnetBombPlus );
    _quitableBlockName.insert( eBlockMagnetBombMinus );


	//--------------------------------------------------
	// Finishable blocks
	//--------------------------------------------------
	_finishableBlockName.insert( eBlockVehicalButtonLeft	);
	_finishableBlockName.insert( eBlockVehicalButtonRight	);
	

	//--------------------------------------------------
	// Animable blocks
	//--------------------------------------------------
	_rolledAnimableBlockName.insert( eBlockPiggyBankRolled	);
	_rolledAnimableBlockName.insert( eBlockThiefRolled		);


	//--------------------------------------------------
	// Animable blocks
	//--------------------------------------------------
	_fullAnimableBlockName.insert( eBlockPiggyBank			);
	_fullAnimableBlockName.insert( eBlockPiggyBankStatic	);
	_fullAnimableBlockName.insert( eBlockCop				);
	//_fullAnimableBlockName.insert( eBlockCopStatic			);
	_fullAnimableBlockName.insert( eBlockThief				);
	_fullAnimableBlockName.insert( eBlockThiefStatic		);
	//_animableBlockName.insert("CashCow");
	//_animableBlockName.insert("CashCowStatic");


	//--------------------------------------------------
	// No shadow blocks
	//--------------------------------------------------
	_noShadowBlockName.insert( eBlockBoxWall			);
	_noShadowBlockName.insert( eBlockCircleWall			);
	_noShadowBlockName.insert( eBlockGunSensor			);
	//_noShadowBlockName.insert( eBlockVehicalControls");
	_noShadowBlockName.insert( eBlockVehicalButtonRight	);
	_noShadowBlockName.insert( eBlockVehicalButtonLeft	);
	_noShadowBlockName.insert( eBlockBankLockCogWheel	);
	_noShadowBlockName.insert( eBlockBankBorderWall		);
	_noShadowBlockName.insert( eBlockBankLockBars		);
	_noShadowBlockName.insert( eBlockBankLock			);
	_noShadowBlockName.insert( eBlockChainBit			);
	_noShadowBlockName.insert( eBlockChain				);


	//--------------------------------------------------
	// Level names
	//--------------------------------------------------
	
	//Reset
	stringstream ss;

#ifdef BUILD_EDITOR
	for ( int i = (int) GameTypes::eLevel_Start; i != (int) GameTypes::eLevel_Finish; i++ )
	{
		ss.str("");
		ss << i;
		_levelNames[ (GameTypes::LevelEnum) i ] = ss.str();
	}
#endif
    
    SetLevelNames();
}
//--------------------------------------------------
GameGlobals::~GameGlobals()
{
	_fullAnimableBlockName.clear();
	_rolledAnimableBlockName.clear();
	_noShadowBlockName.clear();
	_stepableBlockName.clear();
	_quitableBlockName.clear();
	_finishableBlockName.clear();
    _exitableBlockName.clear();
    
	_levelNames.clear();
}
//--------------------------------------------------
BlockEnumContainer& GameGlobals::FullAnimableBlockNames()
{
	return _fullAnimableBlockName;
}
//--------------------------------------------------
BlockEnumContainer& GameGlobals::RolledAnimableBlockNames()
{
	return _rolledAnimableBlockName;
}
//--------------------------------------------------
LevelNamesContainer& GameGlobals::LevelNames()
{
	return _levelNames;
}
//--------------------------------------------------
BlockEnumContainer& GameGlobals::NoShadowBlockNames()
{
	return _noShadowBlockName;
}
//--------------------------------------------------
BlockEnumContainer& GameGlobals::StepableBlockNames()
{
	return _stepableBlockName;
}
//--------------------------------------------------
BlockEnumContainer& GameGlobals::QuitableBlockNames()
{
	return _quitableBlockName;
}
//--------------------------------------------------
BlockEnumContainer& GameGlobals::ExitableBlockNames()
{
	return _exitableBlockName;
}
//--------------------------------------------------
BlockEnumContainer& GameGlobals::FinishableBlockNames()
{
	return _finishableBlockName;
}
//--------------------------------------------------
void GameGlobals::SetLevelNames()
{
    _levelNames.clear();
    
    D_LOG("%s",LocalLevel("eLevelIntro_DestroyBlock") );
    
	_levelNames[ GameTypes::eLevelIntro_DestroyBlock ]	= string( LocalLevel("eLevelIntro_DestroyBlock"));
	_levelNames[ GameTypes::eLevelConstruction_1a ]     = string( LocalLevel("eLevelConstruction_1a"));
	_levelNames[ GameTypes::eLevelBogusia_GoUnder ]     = string( LocalLevel("eLevelBogusia_GoUnder"));
	_levelNames[ GameTypes::eLevelIntro_Gun ]			= string( LocalLevel("eLevelIntro_Gun"));
	_levelNames[ GameTypes::eLevelRotate_1 ]			= string( LocalLevel("eLevelRotate_1"));
	_levelNames[ GameTypes::eLevelLukasz_1a ]			= string( LocalLevel("eLevelLukasz_1a"));
	_levelNames[ GameTypes::eLevelExplosivePath ]		= string( LocalLevel("eLevelExplosivePath"));
	_levelNames[ GameTypes::eLevelReel_1 ]				= string( LocalLevel("eLevelReel_1"));
	_levelNames[ GameTypes::eLevelTraps_1 ]             = string( LocalLevel("eLevelTraps_1"));
	_levelNames[ GameTypes::eLevelRotateL ]             = string( LocalLevel("eLevelRotateL"));
	_levelNames[ GameTypes::eLevelIntro_Rope ]			= string( LocalLevel("eLevelIntro_Rope"));
	_levelNames[ GameTypes::eLevelChain_7a ]			= string( LocalLevel("eLevelChain_7a"));
	_levelNames[ GameTypes::eLevelBomb_3 ]				= string( LocalLevel("eLevelBomb_3"));
	_levelNames[ GameTypes::eLevelGun_3 ]				= string( LocalLevel("eLevelGun_3"));
	_levelNames[ GameTypes::eLevelChain_2a ]			= string( LocalLevel("eLevelChain_2a"));
	_levelNames[ GameTypes::eLevelScale_2 ]             = string( LocalLevel("eLevelScale_2"));
	_levelNames[ GameTypes::eLevelRotate_4 ]			= string( LocalLevel("eLevelRotate_4"));
	_levelNames[ GameTypes::eLevelRoll_1 ]				= string( LocalLevel("eLevelRoll_1"));
	_levelNames[ GameTypes::eLevelPawel_1 ]             = string( LocalLevel("eLevelPawel_1"));
	_levelNames[ GameTypes::eLevelVehical_2 ]			= string( LocalLevel("eLevelVehical_2"));
	_levelNames[ GameTypes::eLevelBomb_2 ]				= string( LocalLevel("eLevelBomb_2"));
	_levelNames[ GameTypes::eLevelBomb_1b ]             = string( LocalLevel("eLevelBomb_1b"));
	_levelNames[ GameTypes::eLevelOthers_1 ]			= string( LocalLevel("eLevelOthers_1"));
	_levelNames[ GameTypes::eLevelBank_1 ]				= string( LocalLevel("eLevelBank_1"));
    
	
	 _levelNames[ GameTypes::eLevelConstruction_3a ]	= string( LocalLevel("eLevelConstruction_3a"));
	 _levelNames[ GameTypes::eLevelBogusia_2 ]			= string( LocalLevel("eLevelBogusia_2"));
	 _levelNames[ GameTypes::eLevelRotate_2 ]			= string( LocalLevel("eLevelRotate_2"));
	 _levelNames[ GameTypes::eLevelChain_3 ]			= string( LocalLevel("eLevelChain_3"));
	 _levelNames[ GameTypes::eLevelGun_2 ]				= string( LocalLevel("eLevelGun_2"));
	 _levelNames[ GameTypes::eLevelScale_1 ]			= string( LocalLevel("eLevelScale_1"));
	 _levelNames[ GameTypes::eLevelPawel_12 ]			= string( LocalLevel("eLevelPawel_12"));
	 _levelNames[ GameTypes::eLevelOthers_2 ]			= string( LocalLevel("eLevelOthers_2"));
	 _levelNames[ GameTypes::eLevelOthers_6 ]			= string( LocalLevel("eLevelOthers_6"));
	 _levelNames[ GameTypes::eLevelTomek_1 ]			= string( LocalLevel("eLevelTomek_1"));
	 _levelNames[ GameTypes::eLevelBank_2 ]				= string( LocalLevel("eLevelBank_2"));
	 _levelNames[ GameTypes::eLevelBogusia_4 ]			= string( LocalLevel("eLevelBogusia_4"));
	 _levelNames[ GameTypes::eLevelSwing_1 ]			= string( LocalLevel("eLevelSwing_1"));
	 _levelNames[ GameTypes::eLevelSwing_2 ]			= string( LocalLevel("eLevelSwing_2"));
	 _levelNames[ GameTypes::eLevelTetris_2 ]			= string( LocalLevel("eLevelTetris_2"));
	 _levelNames[ GameTypes::eLevelVehical_3 ]			= string( LocalLevel("eLevelVehical_3"));
	 _levelNames[ GameTypes::eLevelPawel_15 ]			= string( LocalLevel("eLevelPawel_15"));
	 _levelNames[ GameTypes::eLevelRotate_7 ]			= string( LocalLevel("eLevelRotate_7"));


	 _levelNames[ GameTypes::eLevelSyncronize_1 ]		= string( LocalLevel("eLevelSyncronize_1"));
	 _levelNames[ GameTypes::eLevelScale_3 ]			= string( LocalLevel("eLevelScale_3"));
	 _levelNames[ GameTypes::eLevelMagnet_1 ]			= string( LocalLevel("eLevelMagnet_1"));
	 _levelNames[ GameTypes::eLevelPawel_3 ]			= string( LocalLevel("eLevelPawel_3"));
	 _levelNames[ GameTypes::eLevelIntro_MoneyMakeMoney ]= string( LocalLevel("eLevelIntro_MoneyMakeMoney"));
	 _levelNames[ GameTypes::eLevelLukasz_2 ]			= string( LocalLevel("eLevelLukasz_2"));
	 _levelNames[ GameTypes::eLevelRotate_5 ]			= string( LocalLevel("eLevelRotate_5"));
	 _levelNames[ GameTypes::eLevelTraps_2 ]			= string( LocalLevel("eLevelTraps_2"));
	 _levelNames[ GameTypes::eLevelChain_5 ]			= string( LocalLevel("eLevelChain_5"));
	 _levelNames[ GameTypes::eLevelConstruction_1d ]	= string( LocalLevel("eLevelConstruction_1d"));
	 _levelNames[ GameTypes::eLevelOthers_5 ]			= string( LocalLevel("eLevelOthers_5"));
	 _levelNames[ GameTypes::eLevelReel_2 ]				= string( LocalLevel("eLevelReel_2"));
	 _levelNames[ GameTypes::eLevelTomek_CalmWood ]		= string( LocalLevel("eLevelTomek_CalmWood"));
	 _levelNames[ GameTypes::eLevelDomino_1 ]			= string( LocalLevel("eLevelDomino_1"));
	 _levelNames[ GameTypes::eLevelChain_2b ]			= string( LocalLevel("eLevelChain_2b"));
	 _levelNames[ GameTypes::eLevelBombAndRoll_1 ]		= string( LocalLevel("eLevelBombAndRoll_1"));
	 _levelNames[ GameTypes::eLevelPawel_10 ]			= string( LocalLevel("eLevelPawel_10"));
	 _levelNames[ GameTypes::eLevelBank_3 ]				= string( LocalLevel("eLevelBank_3"));
	

	 _levelNames[GameTypes::eLevelPawel_8 ]				= string( LocalLevel("eLevelPawel_8"));
	 _levelNames[GameTypes::eLevelChain_7b ] 			= string( LocalLevel("eLevelChain_7b"));
	 _levelNames[GameTypes::eLevelSyncronize_2 ] 		= string( LocalLevel("eLevelSyncronize_2"));
	 _levelNames[GameTypes::eLevelTraps_3 ] 			= string( LocalLevel("eLevelTraps_3"));
	 _levelNames[GameTypes::eLevelBomb_5 ] 				= string( LocalLevel("eLevelBomb_5"));
	 _levelNames[GameTypes::eLevelVehical_1 ]			= string( LocalLevel("eLevelVehical_1"));
	 _levelNames[GameTypes::eLevelBomb_8 ] 				= string( LocalLevel("eLevelBomb_8"));
	 _levelNames[GameTypes::eLevelConstruction_1b ]		= string( LocalLevel("eLevelConstruction_1b"));
	 _levelNames[GameTypes::eLevelConstruction_4c ]		= string( LocalLevel("eLevelConstruction_4c"));
	 _levelNames[GameTypes::eLevelBomb_6 ] 				= string( LocalLevel("eLevelBomb_6"));
	 _levelNames[GameTypes::eLevelRotate_3 ] 			= string( LocalLevel("eLevelRotate_3"));
	 _levelNames[GameTypes::eLevelMagnet_2 ] 			= string( LocalLevel("eLevelMagnet_2"));
	 _levelNames[GameTypes::eLevelPawel_5 ] 			= string( LocalLevel("eLevelPawel_5"));
	 _levelNames[GameTypes::eLevelTomek_SelfMadeCannon ] = string( LocalLevel("eLevelTomek_SelfMadeCannon"));
	 _levelNames[GameTypes::eLevelGun_1 ] 				= string( LocalLevel("eLevelGun_1"));
	 _levelNames[GameTypes::eLevelPawel_7 ] 			= string( LocalLevel("eLevelPawel_7"));
	 _levelNames[GameTypes::eLevelConstruction_4b ]		= string( LocalLevel("eLevelConstruction_4b"));
	 _levelNames[GameTypes::eLevelChain_4 ]				= string( LocalLevel("eLevelChain_4"));
	 _levelNames[GameTypes::eLevelAdamS_PlainLogic ]	= string( LocalLevel("eLevelAdamS_PlainLogic"));
	 _levelNames[GameTypes::eLevelPawel_13 ] 			= string( LocalLevel("eLevelPawel_13"));
	 _levelNames[GameTypes::eLevelRotate_Circle ] 		= string( LocalLevel("eLevelRotate_Circle"));
	 _levelNames[GameTypes::eLevelChain_1 ] 			= string( LocalLevel("eLevelChain_1"));
	 _levelNames[GameTypes::eLevelPawel_2 ] 			= string( LocalLevel("eLevelPawel_2"));
	 _levelNames[GameTypes::eLevelPawel_11 ]			= string( LocalLevel("eLevelPawel_11"));

    
	 _levelNames[GameTypes::eLevelLukasz_1b ] 			= string( LocalLevel("eLevelLukasz_1b"));
	 _levelNames[GameTypes::eLevelConstruction_2a ] 	= string( LocalLevel("eLevelConstruction_2a"));
	 _levelNames[GameTypes::eLevelSyncronize_3 ] 		= string( LocalLevel("eLevelSyncronize_3"));
	 _levelNames[GameTypes::eLevelBomb_4 ] 				= string( LocalLevel("eLevelBomb_4"));
	 _levelNames[GameTypes::eLevelBomb_7 ]				= string( LocalLevel("eLevelBomb_7"));
	 _levelNames[GameTypes::eLevelOthers_7 ] 			= string( LocalLevel("eLevelOthers_7"));
	 _levelNames[GameTypes::eLevelRotate_6 ] 			= string( LocalLevel("eLevelRotate_6"));
	 _levelNames[GameTypes::eLevelChain_6 ] 			= string( LocalLevel("eLevelChain_6"));
	 _levelNames[GameTypes::eLevelSwing_Impossible ] 	= string( LocalLevel("eLevelSwing_Impossible"));
	 _levelNames[GameTypes::eLevelReel_3 ] 				= string( LocalLevel("eLevelReel_3"));
	 _levelNames[GameTypes::eLevelChain_2c ] 			= string( LocalLevel("eLevelChain_2c"));
	 _levelNames[GameTypes::eLevelConstruction_4a ] 	= string( LocalLevel("eLevelConstruction_4a"));
	 _levelNames[GameTypes::eLevelPathfinder_1 ] 		= string( LocalLevel("eLevelPathfinder_1"));
	 _levelNames[GameTypes::eLevelGun_4 ] 				= string( LocalLevel("eLevelGun_4"));
	 _levelNames[GameTypes::eLevelTetris_1 ] 			= string( LocalLevel("eLevelTetris_1"));
	 _levelNames[GameTypes::eLevelConstruction_1c ] 	= string( LocalLevel("eLevelConstruction_1c"));
	 _levelNames[GameTypes::eLevelLabyrinth_Imposible ] = string( LocalLevel("eLevelLabyrinth_Imposible"));
	 _levelNames[GameTypes::eLevelBank_Impossible ] 	= string( LocalLevel("eLevelBank_Impossible"));
}
//--------------------------------------------------
