#include "FBPoster.h"
#include "Facebook/FBTool.h"
#include "Game.h"
#include "RunLogger/RunLogger.h"
#include "GLogger.h"
#include "Platform/Lang.h"
#include "Tools.h"

//------------------------------------------------------------------------
FBPoster* FBPoster::_sInstance = NULL;
//------------------------------------------------------------------------
FBPoster* FBPoster::Get()
{
	if ( ! _sInstance )
	{
		_sInstance= new FBPoster();

	}
	return _sInstance;
}
//------------------------------------------------------------------------
void FBPoster::Destroy()
{
	if ( _sInstance	)
		delete _sInstance;

	_sInstance = NULL;
}
//------------------------------------------------------------------------
FBPoster::FBPoster()
{
    _postFnMap[ eFBPost_LevelAnyCompleted ]         = &Posts::LevelAnyCompleted;
    _postFnMap[ eFBPost_LevelBankCompleted ]        = &Posts::LevelBankCompleted;
    _postFnMap[ eFBPost_LevelCarCompleted ]         = &Posts::LevelCarCompleted;
    _postFnMap[ eFBPost_LevelHackerCompleted ]      = &Posts::LevelHackerCompleted;
    _postFnMap[ eFBPost_LevelSpitterCompleted ]     = &Posts::LevelSpitterCompleted;
    _postFnMap[ eFBPost_LevelBombsCompleted ]       = &Posts::LevelBombsCompleted;
    _postFnMap[ eFBPost_LevelReelCompleted ]        = &Posts::LevelReelCompleted;
    _postFnMap[ eFBPost_LevelBalancerCompleted ]    = &Posts::LevelBalancerCompleted;
    _postFnMap[ eFBPost_LevelFliesCompleted ]       = &Posts::LevelFliesCompleted;
    _postFnMap[ eFBPost_LevelTetrisCompleted ]      = &Posts::LevelTetrisCompleted;
    _postFnMap[ eFBPost_LevelCopCompleted ]         = &Posts::LevelCopCompleted;
    _postFnMap[ eFBPost_LevelFrigleCompleted ]      = &Posts::LevelFrigleCompleted;
    _postFnMap[ eFBPost_LevelThiefCompleted ]       = &Posts::LevelThiefCompleted;
    _postFnMap[ eFBPost_LevelBoxingCompleted ]      = &Posts::LevelBoxingCompleted;
    _postFnMap[ eFBPost_LevelButtonCompleted ]      = &Posts::LevelButtonCompleted;
    _postFnMap[ eFBPost_LevelMagnetBombCompleted ]  = &Posts::LevelMagnetBombCompleted;
    
    
    _levelTagsToPostMap[ eLevelTag_Balancer ] = eFBPost_LevelBalancerCompleted;
    _levelTagsToPostMap[ eLevelTag_Flies    ] = eFBPost_LevelFliesCompleted;
    _levelTagsToPostMap[ eLevelTag_Bank     ] = eFBPost_LevelBankCompleted;
    _levelTagsToPostMap[ eLevelTag_Bombs    ] = eFBPost_LevelBombsCompleted;
    _levelTagsToPostMap[ eLevelTag_Car      ] = eFBPost_LevelCarCompleted;
    _levelTagsToPostMap[ eLevelTag_Hacker   ] = eFBPost_LevelHackerCompleted;
    _levelTagsToPostMap[ eLevelTag_Reel     ] = eFBPost_LevelReelCompleted;
    _levelTagsToPostMap[ eLevelTag_Spitter  ] = eFBPost_LevelSpitterCompleted;
    _levelTagsToPostMap[ eLevelTag_Tetris   ] = eFBPost_LevelTetrisCompleted;
    _levelTagsToPostMap[ eLevelTag_Cop      ] = eFBPost_LevelCopCompleted;
    _levelTagsToPostMap[ eLevelTag_Frigle   ] = eFBPost_LevelFrigleCompleted;
    _levelTagsToPostMap[ eLevelTag_Thief    ] = eFBPost_LevelThiefCompleted;
    _levelTagsToPostMap[ eLevelTag_Boxing   ] = eFBPost_LevelBoxingCompleted;
    _levelTagsToPostMap[ eLevelTag_Button   ] = eFBPost_LevelButtonCompleted;
    
    _levelTagsToPostMap[ eLevelTag_MagnetBomb ] = eFBPost_LevelMagnetBombCompleted;
    
    _levelTagsToPostMap[ eLevelTag_Unknown  ] = eFBPost_LevelAnyCompleted;
}
//------------------------------------------------------------------------
FBPoster::~FBPoster()
{
    _levelTagsToPostMap.clear();
    _postFnMap.clear();
}
//------------------------------------------------------------------------
void FBPoster::RandomPost()
{
    int randomNr;

    randomNr = Tools::Rand(0, 7);
    switch ( randomNr )
    {
        case 0 :
        {
            FBPost_LevelCompleted post;
            FBTool::Get()->PostCustomPost( post );
        }
        break;
            
        case 1 :
        {
            FBPost_LevelCarCompleted post;
            FBTool::Get()->PostCustomPost( post );
        }
        break;

        case 2 :
        {
            FBPost_LevelCarCompleted post;
            FBTool::Get()->PostCustomPost( post );
        }
        break;
            
        case 3 :
        {
            FBPost_LevelBankCompleted post;
            FBTool::Get()->PostCustomPost( post );
        }
        break;

        case 4 :
        {
            FBPost_LevelBombsCompleted post;
            FBTool::Get()->PostCustomPost( post );
        }
        break;

        case 5 :
        {
            FBPost_LevelHackerCompleted post;
            FBTool::Get()->PostCustomPost( post );
        }
        break;
        
        case 6 :
        {
            FBPost_LevelReelCompleted post;
            FBTool::Get()->PostCustomPost( post );
        }
        break;

        case 7 :
        {
            FBPost_LevelSpitterCompleted post;
            FBTool::Get()->PostCustomPost( post );
        }
        break;

        default:
        {
            
        }
            ;
    }
}

//------------------------------------------------------------------------
// wall post
void FBPoster::Post_Wall( FBPostEnum post )
{
    if ( ! FBTool::Get()->IsLogged() )
        return;
    
    FnPtr fn;
    fn = _postFnMap[ post ];
    
    if ( fn )
    {
        FBPost post;
        post = (*fn)();
        FBTool::Get()->PostCustomPost( post );
    }
}
//------------------------------------------------------------------------
FBPost FBPoster::Post_LevelBase( LevelEnum levelEnum, LevelTag levelTag )
{
    FBPost post;
    
    FnPtr fn;
    FBPostEnum postEnum;
    
    postEnum = _levelTagsToPostMap[levelTag];
    fn = _postFnMap[ postEnum ];
    
    if ( fn )
    {
        post = (*fn)();
    }
    return post;
}
//------------------------------------------------------------------------
// level completed
void FBPoster::Post_LevelCompleted( LevelEnum levelEnum, LevelTag levelTag )
{
    if ( ! FBTool::Get()->IsLogged() )
        return;

    FBTool::Get()->Post_LevelCompleted( levelEnum,
                                       Post_LevelBase( levelEnum, levelTag ).GetImageURL().c_str() );
}

//------------------------------------------------------------------------
// level skipped
void FBPoster::Post_LevelSkipped( LevelEnum levelEnum, LevelTag levelTag )
{
    if ( ! FBTool::Get()->IsLogged() )
        return;

    string imgUrl;
    imgUrl.append( Config::FBCustomPostImagesURL );
    imgUrl.append( "fbSkippedLevel.png" );
    
    FBTool::Get()->Post_LevelSkipped( levelEnum, imgUrl.c_str() );
}

//------------------------------------------------------------------------



//------------------------------------------------------------------------
// Posts
//------------------------------------------------------------------------
FBPost FBPoster::Posts::LevelAnyCompleted()
{
    FBPost_LevelCompleted post;
    return post;
}
//----------------------------------------------------
FBPost FBPoster::Posts::LevelCarCompleted()
{
    FBPost_LevelCarCompleted post;
    return post;
}
//----------------------------------------------------
FBPost FBPoster::Posts::LevelBankCompleted()
{
    FBPost_LevelBankCompleted post;
    return post;
}
//----------------------------------------------------
FBPost FBPoster::Posts::LevelHackerCompleted()
{
    FBPost_LevelHackerCompleted post;
    return post;
}
//----------------------------------------------------
FBPost FBPoster::Posts::LevelSpitterCompleted()
{
    FBPost_LevelSpitterCompleted post;
    return post;
}
//----------------------------------------------------
FBPost FBPoster::Posts::LevelBombsCompleted()
{
    FBPost_LevelBombsCompleted post;
    return post;
}
//----------------------------------------------------
FBPost FBPoster::Posts::LevelReelCompleted()
{
    FBPost_LevelReelCompleted post;
    return post;
}
//----------------------------------------------------
FBPost FBPoster::Posts::LevelBalancerCompleted()
{
    FBPost_LevelBalancerCompleted post;
    return post;
}
//----------------------------------------------------
FBPost FBPoster::Posts::LevelFliesCompleted()
{
    FBPost_LevelFliesCompleted post;
    return post;
}
//----------------------------------------------------
FBPost FBPoster::Posts::LevelTetrisCompleted()
{
    FBPost_LevelTetrisCompleted post;
    return post;
}
//----------------------------------------------------
FBPost FBPoster::Posts::LevelCopCompleted()
{
    FBPost_LevelCopCompleted post;
    return post;
}
//----------------------------------------------------
FBPost FBPoster::Posts::LevelFrigleCompleted()
{
    FBPost_LevelFrigleCompleted post;
    return post;
}
//----------------------------------------------------
FBPost FBPoster::Posts::LevelThiefCompleted()
{
    FBPost_LevelThiefCompleted post;
    return post;
}
//----------------------------------------------------
FBPost FBPoster::Posts::LevelBoxingCompleted()
{
    FBPost_LevelBoxingCompleted post;
    return post;
}
//----------------------------------------------------
FBPost FBPoster::Posts::LevelMagnetBombCompleted()
{
    FBPost_LevelMagnetBombCompleted post;
    return post;
}
//----------------------------------------------------
FBPost FBPoster::Posts::LevelButtonCompleted()
{
    FBPost_LevelButtonCompleted post;
    return post;
}
//----------------------------------------------------
