#ifndef __piggy_biggy__FBRequestsMap__
#define __piggy_biggy__FBRequestsMap__
//-----------------------------------------------------------------------------
#include <map>
#include <string>
#include "GameTypes.h"
//-----------------------------------------------------------------------------
using namespace std;
using namespace GameTypes;
//-----------------------------------------------------------------------------
class FBRequestData
{
public:
    FBRequestData();
    FBRequestData(
                  const char *fromUsername,
                  const char *fromUserFbId,
                  const char *createdTime,
                  const char *createdDate,
                  const char *message
                );
    
    FBRequestData(
                  const char *fromUsername,
                  const char *fromUserFbId,
                  const char *createdTime,
                  const char *createdDate,
                  const char *message,
                  const char *requestData );
private:
    void Construct(
                  const char *fromUsername,
                  const char *fromUserFbId,
                  const char *createdTime,
                  const char *createdDate,
                  const char *message,
                  const char *requestData );
    
public:
    string _fromUsername;
    string _fromUserFbId;
    string _createdTime;
    string _createdDate;
    string _message;
    string _requestData;
    
    GameTypes::FBRequestType _requestType;
    GameTypes::LevelEnum     _levelEnum;
    unsigned int             _levelCompletedSeconds;
};
//-----------------------------------------------------------------------------
// map< fb id, fb rquest data>
class FBRequestsMap : public map<string, FBRequestData>
{
public:
    void Add(
             const char *objectId,
             const char *fromUsername,
             const char *fromUserFbId,
             const char *createDate,
             const char *message,
             const char *requestData = NULL );
    void Dump();

private:
    void CopyCreatedTime( const char *src, string &dest );
    void CopyCreatedDate( const char *src, string &dest );
};
//-----------------------------------------------------------------------------

#endif

