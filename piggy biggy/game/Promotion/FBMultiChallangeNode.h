#ifndef __piggy_biggy__FBMultiChallanageNode__
#define __piggy_biggy__FBMultiChallanageNode__

#include "Facebook/FBTool.h"
#include "Platform/WWWTool.h"
#include "FBChallangeMap.h"
#include <cocos2d.h>
#include "pu/ui/SlidingNode2.h"
#include "Components/CustomSprites.h"

USING_NS_CC;


//------------------------------------------------------------------------------
// FB Score Map
//------------------------------------------------------------------------------
typedef std::pair<unsigned int /*seconds*/, PocketType> LevelScoreData;
class FBScoreMap : public map<LevelEnum, LevelScoreData>
{
public:
    void Add( LevelEnum, unsigned int seconds );
    void Dump();
};


//------------------------------------------------------------------------------
// FB Multi Challange Node
//------------------------------------------------------------------------------
class FBRequestMenu;
class FBPocketToggleButton;

class FBMultiChallangeNode : public CCNode, public WWWToolListener
{
public:
    static FBMultiChallangeNode* Create( FBRequestMenu *parent );
    static FBMultiChallangeNode* Create( FBRequestMenu *parent, LevelEnum levelEnum );
    ~FBMultiChallangeNode();
    
    void Exiting();
    void SetPocket( PocketType pocket );
    
private:
    FBMultiChallangeNode( FBRequestMenu *parent );
    
    void Init();
  
    virtual void Http_FailedWithError( void *connection, int error );
    virtual void Http_FinishedLoading( void *connection, const void *data, int len );

    void Setup0();
    void Setup1();
    void Setup2();
    void Setup3();
  
    void AddPiggySprite( int count );
    CCMenuItem* GetMenuItemView( LevelEnum levelEnum );
    CCMenuItem* GetMenuItemAccept( LevelEnum levelEnum );

    void Menu_View(CCObject *sender );
    void Menu_Accept(CCObject *sender );
    
    void ShowNoInternetSprite();

//    void CleanUp();
    
protected:
    LoadingAnim     *_loading;

    SlidingNodeMenu *_menuAccept;
    SlidingNodeMenu *_menuView;
    FadeInSprite    *_piggySprite;
    
    typedef enum
    {
        eState_Idle = 0,
        eState_HTTPScoreGet,
        eState_Exiting
    } State;
    
    State   _state;

    FBScoreMap            _scoreMap;
    SlidingNode2         *_slidingNode;
    FBRequestMenu        *_parent;
    FBPocketToggleButton *_pocketButton;
    PocketType          _pocket;
    LevelEnum           _levelEnum;
    
    bool                    _internetAccess;
    FadeInSprite            *_noInternetAccess;
};


//------------------------------------------------------------------------------
// FB Multi Challange Node Record
//------------------------------------------------------------------------------
class FBMultiChallangeNodeRecord : public CCNode
{
public:
    static FBMultiChallangeNodeRecord* Create( LevelEnum level, unsigned int seconds );
    ~FBMultiChallangeNodeRecord();
    
private:
    FBMultiChallangeNodeRecord( LevelEnum level, unsigned int seconds );
    void Init();
    FadeInSprite* GetCoinSprite( unsigned int seconds );
    
private:
    LevelEnum _level;
    unsigned int _seconds;
};

//------------------------------------------------------------------------------
// FB Pocket Toggle Button
//------------------------------------------------------------------------------
class FBPocketToggleButton : public CCNode
{
public:
    static FBPocketToggleButton* Create( FBMultiChallangeNode* parent );
    ~FBPocketToggleButton();
    CCMenu* GetMenu();
    
private:
    FBPocketToggleButton( FBMultiChallangeNode* parent );
    void Init();
    CCMenuItemToggle* GetToggleButton();
    void Menu_SelectPocket( CCObject *sender );

private:
    CCMenu  *_menu;
    FBMultiChallangeNode* _parent;
};




#endif