#ifndef __piggy_biggy__FBViewLevelNode__
#define __piggy_biggy__FBViewLevelNode__
//-------------------------------------------------------------------
#include <cocos2d.h>
#include "Levels/Level.h"
//-------------------------------------------------------------------
USING_NS_CC;
//-------------------------------------------------------------------
class FBViewLevelNode : public CCLayer
{
public:
    static FBViewLevelNode* Create( Level* level );
    ~FBViewLevelNode();
    
    virtual void registerWithTouchDispatcher(void);
    virtual bool ccTouchBegan(CCTouch *pTouch, CCEvent *pEvent);
    virtual void ccTouchMoved(CCTouch *pTouch, CCEvent *pEvent) {}
    virtual void ccTouchEnded(CCTouch *pTouch, CCEvent *pEvent) {}
    
private:
    FBViewLevelNode( Level *level );
    bool MyInit();
    void RemoveMe();
    void RemoveLevel();
    void Show();
    void ShowLevel();
    void Menu_Close(CCObject *sender);
    
private:
    Level       *_level;
    CCSprite    *_flash;
    CCSprite    *_previewOnly;
    bool        _exiting;
};

//-------------------------------------------------------------------
#endif

