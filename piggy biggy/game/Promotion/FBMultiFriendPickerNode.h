#ifndef __piggy_biggy__FBMultiFriendPickerNode__
#define __piggy_biggy__FBMultiFriendPickerNode__

#include <cocos2d.h>
#include "Facebook/FBTool.h"
#include "Platform/WWWTool.h"
#include "pu/ui/SlidingNode2.h"
#include "Components/CustomSprites.h"
#include "GameTypes.h"
#include "Facebook/Menus/FBScoreMenu.h"
#include "Facebook/FBGameUsers.h"
#include <vector>
#include <set>

USING_NS_CC;
using namespace GameTypes;
using namespace std;

//------------------------------------------------------------------------------
// FB Multi Friend Picker Node
//------------------------------------------------------------------------------
class FBRequestMenu;
class FBMultiFriendPickerNode : public CCNode, public FBToolListener, public WWWToolListener
{
public:
    static FBMultiFriendPickerNode* Create( FBRequestMenu *parent, LevelEnum level, unsigned int seconds );
    ~FBMultiFriendPickerNode();
    
    void Exiting();
    
private:
    FBMultiFriendPickerNode( FBRequestMenu *parent, LevelEnum level, unsigned int seconds );
    void Init();
  
    virtual void Http_FailedWithError( void *connection, int error );
    virtual void Http_FinishedLoading( void *connection, const void *data, int len );

    virtual void FBRequestCompleted( FBTool::Request requestType, void *data );
    virtual void FBRequestError( int error );
    void Menu_SelectUser( CCObject *sender );
    
//    CCMenuItem *GetMenuItem( const LevelUserData &userData );
    
    void HTTPGetMultiFriends();

    void Setup0();
    void Setup1();
    void Setup2();
    void Setup3();
    
//    void CleanUp();
    
protected:
    LoadingAnim     *_loading;

    typedef enum
    {
        eState_Idle = 0,
        eState_FBGettingFriends,
        eState_HTTPRandomPeopleGet,
        eState_Exiting
    } State;
    
    State           _state;
    SlidingNode2    *_slidingNode;
    unsigned int    _seconds;
    LevelEnum       _level;
    FBRequestMenu   *_parent;
    
    FBFriendsMap        _friendsMap;
    FBGameUsersVector   _gameUsers;
    
    SlidingNodeMenu     *_usersMenu;
};

#endif

