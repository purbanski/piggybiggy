#include "FBPosts.h"
#include "GameConfig.h"
#include "Platform/Lang.h"
#include "Debug/MyDebug.h"
#include "Facebook/FBTool.h"
#include "GameGlobals.h"
#include "Game.h"
//----------------------------------------------------------------------------------
FBPost::FBPost()
{
    
}
//----------------------------------------------------------------------------------
FBPost::FBPost( const char *link,
               const char *imageURL,
               const char *name,
               const char *caption,
               const char *description )
{
    D_CSTRING(name);
    D_CSTRING(caption);
    
    char nameBuffer[100];
    char captionBuffer[256];
    char imageBuffer[128];
    
    string levelName;
    levelName = GameGlobals::Get()->LevelNames()[ Game::Get()->GetRuningLevel()->GetLevelEnum() ];

    snprintf( captionBuffer, 256, caption, FBTool::Get()->GetUsername().c_str(), levelName.c_str() );
    snprintf( imageBuffer, 128, "%s%s%s", Config::FBCustomPostImagesURL, imageURL, ".png" );
    
    
    D_CSTRING(nameBuffer);
    D_CSTRING(captionBuffer);
    
    _postMap["link"]      = string(link);
    _postMap["imageURL"]  = string(imageBuffer);
//    _postMap["name"]      = string(nameBufer);
    _postMap["name"]      = string(name);
    _postMap["caption"]   = string(captionBuffer);
    _postMap["description"]  = string(description);
}
//----------------------------------------------------------------------------------
FBPost::~FBPost()
{
    _postMap.clear();
}
//----------------------------------------------------------------------------------
string FBPost::GetLink()
{
    return _postMap["link"];
}
//----------------------------------------------------------------------------------
string FBPost::GetImageURL()
{
    return _postMap["imageURL"];
}
//----------------------------------------------------------------------------------
string FBPost::GetName()
{
    return _postMap["name"];
}
//----------------------------------------------------------------------------------
string FBPost::GetCaption()
{
    return _postMap["caption"];
}
//----------------------------------------------------------------------------------
string FBPost::GetDescription()
{
    return _postMap["description"];
}

//----------------------------------------------------------------------------------




//----------------------------------------------------------------------------------
FBPost_LevelCompleted::FBPost_LevelCompleted() : FBPost(
                                                        Config::URLAppStore,
                                                        "fbCoins",
                                                        LocalString("FBPostName"),
                                                        LocalString("FBPostCaption"),
                                                        LocalString("FBPostDescription"))
{
}
//----------------------------------------------------------------------------------
FBPost_LevelHackerCompleted::FBPost_LevelHackerCompleted() : FBPost(
                                                        Config::URLAppStore,
                                                        "fbHacker",
                                                        LocalString("FBPostName_Hacker"),
                                                        LocalString("FBPostCaption_Hacker"),
                                                        LocalString("FBPostDescription_Hacker"))
{
}
//----------------------------------------------------------------------------------
FBPost_LevelBankCompleted::FBPost_LevelBankCompleted() : FBPost(
                                                        Config::URLAppStore,
                                                        "fbBank",
                                                        LocalString("FBPostName_Bank"),
                                                        LocalString("FBPostCaption_Bank"),
                                                        LocalString("FBPostDescription_Bank"))
{
}
//----------------------------------------------------------------------------------
FBPost_LevelSpitterCompleted::FBPost_LevelSpitterCompleted() : FBPost(
                                                        Config::URLAppStore,
                                                        "fbSpitter",
                                                        LocalString("FBPostName_Spitter"),
                                                        LocalString("FBPostCaption_Spitter"),
                                                        LocalString("FBPostDescription_Spitter"))
{
}
//----------------------------------------------------------------------------------
FBPost_LevelBombsCompleted::FBPost_LevelBombsCompleted() : FBPost(
                                                        Config::URLAppStore,
                                                        "fbBombs",
                                                        LocalString("FBPostName_Bombs"),
                                                        LocalString("FBPostCaption_Bombs"),
                                                        LocalString("FBPostDescription_Bombs"))
{
}
//----------------------------------------------------------------------------------
FBPost_LevelReelCompleted::FBPost_LevelReelCompleted() : FBPost(
                                                        Config::URLAppStore,
                                                        "fbReel",
                                                        LocalString("FBPostName"),
                                                        LocalString("FBPostCaption"),
                                                        LocalString("FBPostDescription"))
{
}
//----------------------------------------------------------------------------------
FBPost_LevelBalancerCompleted::FBPost_LevelBalancerCompleted() : FBPost(
                                                        Config::URLAppStore,
                                                        "fbBalancer",
                                                        LocalString("FBPostName"),
                                                        LocalString("FBPostCaption"),
                                                        LocalString("FBPostDescription"))
{
}
//----------------------------------------------------------------------------------
FBPost_LevelFliesCompleted::FBPost_LevelFliesCompleted() : FBPost(
                                                        Config::URLAppStore,
                                                        "fbFlies",
                                                        LocalString("FBPostName"),
                                                        LocalString("FBPostCaption"),
                                                        LocalString("FBPostDescription"))
{
}
//----------------------------------------------------------------------------------
FBPost_LevelCarCompleted::FBPost_LevelCarCompleted() : FBPost(
                                                        Config::URLAppStore,
                                                        "fbCar",
                                                        LocalString("FBPostName"),
                                                        LocalString("FBPostCaption"),
                                                        LocalString("FBPostDescription"))
{
}
//----------------------------------------------------------------------------------
FBPost_LevelTetrisCompleted::FBPost_LevelTetrisCompleted() : FBPost(
                                                        Config::URLAppStore,
                                                        "fbBlocks",
                                                        LocalString("FBPostName"),
                                                        LocalString("FBPostCaption"),
                                                        LocalString("FBPostDescription"))
{
}
//----------------------------------------------------------------------------------
FBPost_LevelCopCompleted::FBPost_LevelCopCompleted() : FBPost(
                                                        Config::URLAppStore,
                                                        "fbCop",
                                                        LocalString("FBPostName"),
                                                        LocalString("FBPostCaption"),
                                                        LocalString("FBPostDescription"))
{
}
//----------------------------------------------------------------------------------
FBPost_LevelFrigleCompleted::FBPost_LevelFrigleCompleted() : FBPost(
                                                        Config::URLAppStore,
                                                        "fbFrigle",
                                                        LocalString("FBPostName"),
                                                        LocalString("FBPostCaption"),
                                                        LocalString("FBPostDescription"))
{
}
//----------------------------------------------------------------------------------
FBPost_LevelThiefCompleted::FBPost_LevelThiefCompleted() : FBPost(
                                                        Config::URLAppStore,
                                                        "fbThief",
                                                        LocalString("FBPostName"),
                                                        LocalString("FBPostCaption"),
                                                        LocalString("FBPostDescription"))
{
}
//----------------------------------------------------------------------------------
FBPost_LevelBoxingCompleted::FBPost_LevelBoxingCompleted() : FBPost(
                                                        Config::URLAppStore,
                                                        "fbBoxing",
                                                        LocalString("FBPostName"),
                                                        LocalString("FBPostCaption"),
                                                        LocalString("FBPostDescription"))
{
}
//----------------------------------------------------------------------------------
FBPost_LevelMagnetBombCompleted::FBPost_LevelMagnetBombCompleted() : FBPost(
                                                        Config::URLAppStore,
                                                        "fbMagnetBomb",
                                                        LocalString("FBPostName"),
                                                        LocalString("FBPostCaption"),
                                                        LocalString("FBPostDescription"))
{
}
//----------------------------------------------------------------------------------
FBPost_LevelButtonCompleted::FBPost_LevelButtonCompleted() : FBPost(
                                                        Config::URLAppStore,
                                                        "fbButton",
                                                        LocalString("FBPostName"),
                                                        LocalString("FBPostCaption"),
                                                        LocalString("FBPostDescription"))
{
}
//----------------------------------------------------------------------------------

