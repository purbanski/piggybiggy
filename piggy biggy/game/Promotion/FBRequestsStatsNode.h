#ifndef __piggy_biggy__FBRequestsStatsNode__
#define __piggy_biggy__FBRequestsStatsNode__
//------------------------------------------------------------------------------
#include <cocos2d.h>
#include "Platform/WWWTool.h"
#include "FBRequestMailbox.h"
//------------------------------------------------------------------------------
USING_NS_CC;

//------------------------------------------------------------------------------
// FB Request Menu Stats Node
//------------------------------------------------------------------------------
class FBRequestMenuStatsNode : public CCNode
{
public:
    static FBRequestMenuStatsNode* Create( FBRequestMenu *parent, FBChallangeMap challanges, const char *fbId );
    static FBChallangeStatus GetChallangeStatus( const char* status );
    
    ~FBRequestMenuStatsNode();
    
    void Exiting();
    
private:
    FBRequestMenuStatsNode( FBRequestMenu *parent, const char *fbId );
    void Init( FBChallangeMap challanges );
  
    void Setup1();
    void Setup2();
    void Setup3();
    void DeleteChallangeNotify();
    
    CCMenuItem* GetMenuItemView( const FBChallangeMapRecord &record );
    void Menu_Play(CCObject *sender );
    void Menu_View(CCObject *sender );
    
    void ChallangeAccept( const char *fromUser, GameTypes::LevelEnum levelEnum, unsigned int seconds );
    
    void CleanUp();
    void AddPiggySprite();
    
protected:
    SlidingNodeMenu *_menuView;
    LoadingAnim     *_loading;

    typedef enum
    {
        eState_Idle = 0,
        eState_Processing,
        eState_Exiting
    } State;
    
    FBChallangeMap  _challangeMap;
    SlidingNode2    *_slidingNode;
    FBRequestMenu   *_parent;
    string          _fbIdOpponent;
};



//------------------------------------------------------------------------------
// FB Request Menu Stats Record Node
//------------------------------------------------------------------------------
class FadeInSprite;
class FBRequestMenuStatsRecordNode : public CCNode
{
public:
    static FBRequestMenuStatsRecordNode* Create( const FBChallangeMapRecord &record );
    ~FBRequestMenuStatsRecordNode();
    
private:
    FBRequestMenuStatsRecordNode( const FBChallangeMapRecord &record );
    void Init();
    
    void Setup1();
    void Setup2();
    void Setup2a();
    void Setup3();
    void Setup4();
    void Setup5();

    FadeInSprite *GetCoinImage( int count );
    
private:
    FBChallangeMapRecord _challange;
    
public:
    static float _sWidth;
};


#endif 
