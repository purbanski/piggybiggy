#ifndef __FBNEWREQUESTNOTIFY_H__
#define __FBNEWREQUESTNOTIFY_H__

//--------------------------------------------------------------------------
#include <cocos2d.h>
#include "FBRequestsMap.h"
#include "Components/CustomNodes.h"
//--------------------------------------------------------------------------
USING_NS_CC;
//--------------------------------------------------------------------------
class FBNewRequestNotify : public CCNode
{
public:
    static FBNewRequestNotify* Create( FBRequestsMap *requestsMap );
    ~FBNewRequestNotify();
    
private:
    FBNewRequestNotify();
    void Init( FBRequestsMap *requestMap );
    
private:
    FBRequestsMap _requestsMap;
    void Menu_ShowMore( CCObject *sender );
    void Finish();
};
//--------------------------------------------------------------------------
#endif
