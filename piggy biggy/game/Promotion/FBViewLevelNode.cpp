#include "FBViewLevelNode.h"
#include "Components/FXMenuItem.h"
#include "Tools.h"
#include "Platform/Lang.h"
//-------------------------------------------------------------------
FBViewLevelNode* FBViewLevelNode::Create(Level *level)
{
    FBViewLevelNode *node;
    node = new FBViewLevelNode( level );
    if ( node && node->MyInit() )
    {
        node->autorelease();
        return node;
    }
    return NULL;
}
//-------------------------------------------------------------------
FBViewLevelNode::FBViewLevelNode( Level *level )
{
    _level = level;
    _exiting = false;
    _previewOnly = NULL;
}
//-------------------------------------------------------------------
FBViewLevelNode::~FBViewLevelNode()
{
    
}
//-------------------------------------------------------------------
bool FBViewLevelNode::MyInit()
{
    if ( ! CCLayer::init() )
        return false;
    
    setIsTouchEnabled( true );

    _flash = CCSprite::spriteWithFile( "Images/Other/ScreenshotFlash.png");
    _flash->setOpacity(0);
    _flash->setPosition( Tools::GetScreenMiddle()  );
    addChild( _flash, 100 );
    
    float fadeDur;
    fadeDur = 0.5f;
    
    _flash->runAction( CCSequence::actions(
                                          CCFadeTo::actionWithDuration(fadeDur, 255),
                                          CCCallFunc::actionWithTarget(this, callfunc_selector(FBViewLevelNode::ShowLevel )),
                                          CCFadeTo::actionWithDuration(fadeDur, 0),
                                          NULL
                                          ));
    
    _previewOnly = CCSprite::spriteWithFile( Lang::Get()->GetLang("Images/Facebook/Requests/previewOnly.png"));
    _previewOnly->setOpacity(0);
    
    if ( _level->GetOrientation() == eLevel_Horizontal )
    {
        _previewOnly->setPosition(ccp( Tools::GetScreenMiddle().x - 0.0f, Tools::GetScreenMiddle().y + 230.0f ));
    }
    else
    {
        _previewOnly->setRotation( -90.0f );
        _previewOnly->setPosition(ccp( Tools::GetScreenMiddle().x - 390.0f, Tools::GetScreenMiddle().y ));
    }
        
    addChild(_previewOnly, 200 );
    
    _previewOnly->runAction( CCSequence::actions(
                                                CCDelayTime::actionWithDuration( 0.5f ),
                                                CCFadeTo::actionWithDuration( 0.5f, 255 ),
                                                NULL
                                          ));
    return true;
}
//-------------------------------------------------------------------
void FBViewLevelNode::ShowLevel()
{
    CCMenu *menu;
    TrippleSprite sprite;
    FXMenuItemSpriteWithAnim *menuItem;
    
    sprite = TrippleSprite( "Images/Facebook/Requests/backBt.png" );
    menuItem = FXMenuItemSpriteWithAnim::Create( &sprite, this, menu_selector( FBViewLevelNode::Menu_Close  ));
    menu = CCMenu::menuWithItems( Config::eMousePriority_FBMenuLayerSuperVIPAboveX2, menuItem, NULL );
    
    if ( _level->GetOrientation() == eLevel_Horizontal )
    {
        menu->setPosition(ccp( 390.0f, -245.0f ));
    }
    else
    {
        menu->setAnchorPoint( CCPointZero );
        menu->setRotation( -90.0f );
        menu->setPosition(ccp( 390.0f, 245.0f ));
    }

    
    _level->addChild( menu, 255 );
    addChild( _level, 40 );
}
//-------------------------------------------------------------------
void FBViewLevelNode::registerWithTouchDispatcher(void)
{
	CCTouchDispatcher::sharedDispatcher()->addTargetedDelegate( this, Config::eMousePriority_FBMenuLayerSuperVIPAbove, true );
}
//-------------------------------------------------------------------
bool FBViewLevelNode::ccTouchBegan(CCTouch *pTouch, CCEvent *pEvent)
{
    return true;
}
//-------------------------------------------------------------------
void FBViewLevelNode::Show()
{
//    float dur;
//    dur = 0.75f;
//    _level->runAction( CCSequence::actions(
//                                          CCScaleTo::actionWithDuration( dur, 1.1f),
//                                          CCScaleTo::actionWithDuration( 0.2f, 0.9f ),
//                                          CCScaleTo::actionWithDuration( 0.2f, 1.0f ),
//                                          NULL
//                                          ));
 
}
//-------------------------------------------------------------------
void FBViewLevelNode::Menu_Close( CCObject* sender )
{
    if ( _exiting )
        return;
    
    _exiting = true;
    
    float dur;
    dur = 0.5f;

    _flash->stopAllActions();
    _flash->runAction(
                      CCSequence::actions(
                                          CCFadeTo::actionWithDuration(dur, 255),
                                          CCCallFunc::actionWithTarget(this, callfunc_selector(FBViewLevelNode::RemoveLevel)),
                                          CCFadeTo::actionWithDuration(dur, 0),
                                          CCCallFunc::actionWithTarget(this, callfunc_selector(FBViewLevelNode::RemoveMe)),
                                          NULL
                                          )
                      );

    _previewOnly->stopAllActions();
    _previewOnly->runAction( CCFadeTo::actionWithDuration( dur - 0.2f, 0 ));
}
//-------------------------------------------------------------------
void FBViewLevelNode::RemoveLevel()
{
    _level->removeFromParentAndCleanup( true );
}
//-------------------------------------------------------------------
void FBViewLevelNode::RemoveMe()
{
    removeFromParentAndCleanup( true );
}

