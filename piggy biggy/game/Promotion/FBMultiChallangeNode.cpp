#include "FBMultiChallangeNode.h"
#include "Tools.h"
#include <sstream>
#include "Game.h"
#include "Preloader.h"
#include "Levels/Pockets.h"
#include "FBRequestMenu.h"
#include "Debug/MyDebug.h"
#include "Components/CustomNodes.h"
#include "Components/CustomSprites.h"
#include "Components/TrippleSprite.h"
#include "Components/FXMenuItem.h"
#include "Platform/Lang.h"
#include "Platform/ScoreTool.h"
#include "FBMultiFriendPickerNode.h"
#include "RunLogger/RunLogger.h"

//------------------------------------------------------------------------------
// FB Request Menu Mailbox Node
//------------------------------------------------------------------------------
FBMultiChallangeNode* FBMultiChallangeNode::Create( FBRequestMenu *parent )
{
    FBMultiChallangeNode* node;
    node = new FBMultiChallangeNode( parent );
    
    if ( node )
    {
        node->autorelease();
        node->Init();
        return  node;
    }
    return NULL;
}
//------------------------------------------------------------------------------
FBMultiChallangeNode* FBMultiChallangeNode::Create( FBRequestMenu *parent, LevelEnum levelEnum )
{
    FBMultiChallangeNode* node;
    node = new FBMultiChallangeNode( parent );
    node->_levelEnum = levelEnum;
    
    if ( node )
    {
        node->autorelease();
        node->Init();
        return  node;
    }
    return NULL;
}
//------------------------------------------------------------------------------
FBMultiChallangeNode::FBMultiChallangeNode( FBRequestMenu *parent )
{
    _parent = parent;
    _state = eState_Idle;
    _loading = NULL;
    _pocket = ePocketBlue;
    _levelEnum = (LevelEnum) 0;
    _piggySprite = NULL;
    _noInternetAccess = NULL;
    _internetAccess = true;
}
//------------------------------------------------------------------------------
FBMultiChallangeNode::~FBMultiChallangeNode()
{
    if ( _state == eState_HTTPScoreGet )
        WWWTool::Get()->CancelHttpGet( this );
}
//------------------------------------------------------------------------------
void FBMultiChallangeNode::Init()
{  
    _slidingNode = SlidingNode2::node( Config::GameSize, Config::eMousePriority_FBMenuLayer );
    _slidingNode->setIsTouchEnabled( true );
    _slidingNode->setPosition( CCPoint( -90.0f, 100.0f ));
    addChild( _slidingNode, 10 );

    _pocketButton = FBPocketToggleButton::Create( this );
    _pocketButton->setPosition( CCPoint( Tools::GetScreenMiddleX() + 390.0f, Tools::GetScreenMiddleY() + 100.0f ));
    _pocketButton->GetMenu()->setOpacity( 0 );
    addChild( _pocketButton, 50 );
    
    stringstream url;
    url << Config::FBScoreURL << "getMyScore.php?fbId=" << FBTool::Get()->GetFBUserId();
    
    WWWTool::Get()->HttpGet( url.str().c_str(), this );
    if ( ! _loading )
    {
        _loading = LoadingAnim::Create();
        _loading->SetPositionForFBRequestMenu();
        addChild( _loading, 50 );
    }
    _state = eState_HTTPScoreGet;
    
    float yd;
    yd = 130.0f;
    
//    FadeInSprite *challangeLabelBg;
//    challangeLabelBg = FadeInSprite::Create( 0.5f, "Images/Facebook/Requests/challangeLabelBg.png" );
//    challangeLabelBg->setPosition( CCPoint( Tools::GetScreenMiddleX() , Tools::GetScreenMiddleY() + yd ));
//    addChild( challangeLabelBg, 15 );
//
//    FadeInLabel *challangeLabel;
//    challangeLabel = FadeInLabel::Create( 0.5f, LocalString("ChallangeChooseLevelMsg"), Config::FBHighScoreFont, 40);
//    challangeLabel->setPosition( CCPoint( Tools::GetScreenMiddleX(), Tools::GetScreenMiddleY() + yd ));
//    addChild( challangeLabel, 25 );
}
//------------------------------------------------------------------------------
void FBMultiChallangeNode::Setup0()
{
    GameStatus *status;
    status = Game::Get()->GetGameStatus();
    
    if ( ! status->GetMsgPlayed( eOTMsgRequest_ChooseLevel ))
    {
        status->SetMsgPlayed( eOTMsgRequest_ChooseLevel );
        _parent->ShowHelpMsg( LocalString("ChooseLevelMsg"));
    }
    
    stopAllActions();
    _slidingNode->GetContentNode()->RemoveAllNodes();
    _slidingNode->GetContentNode()->removeAllChildrenWithCleanup( true );
    
    float dur;
    dur = 0.25f;
    runAction( CCSequence::actions(
                                    CCCallFunc::actionWithTarget(this, callfunc_selector(FBMultiChallangeNode::Setup1 )),
//                                    CCDelayTime::actionWithDuration( dur ),
                                    CCCallFunc::actionWithTarget(this, callfunc_selector(FBMultiChallangeNode::Setup2 )),
                                    CCDelayTime::actionWithDuration( dur ),
                                    CCCallFunc::actionWithTarget(this, callfunc_selector(FBMultiChallangeNode::Setup3 )),
                                    NULL));

}
//------------------------------------------------------------------------
//void FBMultiChallangeNode::CleanUp()
//{
//    _slidingNode->onExit();
//    _parent->CleanUp();
//    
//    if ( _state == eState_HTTPScoreGet )
//        WWWTool::Get()->CancelHttpGet();
//}
//------------------------------------------------------------------------
void FBMultiChallangeNode::Http_FailedWithError( void *connection, int error )
{
    _state = eState_Idle;
    
    if ( _loading )
    {
        _loading->Stop();
        _loading = NULL;
    }
    
    ShowNoInternetSprite();
}
//------------------------------------------------------------------------
void FBMultiChallangeNode::Http_FinishedLoading( void *connection, const void *data, int len )
{
    if ( _state == eState_Exiting )
        return;
    
    _state = eState_Idle;
    
    string temp;
    string strData;
    
    temp.append( (const char*)data );
    strData = string( temp, 0, len );
    
    D_STRING( temp );
    D_STRING( strData );
    
    typedef vector<string> Vec;
    Vec userScores;
    Vec scoreRecords;
  
//    // example
//    // 1,7:40,19:114,25:2,224:71,242:86,748:89,215:53,299
      
    Tools::StringSplit( strData.c_str(), ':', userScores );
    for ( Vec::iterator score = userScores.begin(); score != userScores.end(); score++ )
    {
        D_LOG("%s-", score->c_str() );
        
        scoreRecords.clear();
        Tools::StringSplit( score->c_str(), ',', scoreRecords );
        
        D_SIZE( scoreRecords );
        if ( scoreRecords.size() != 2 )
            continue;

        int levelEnum;
        unsigned int seconds;
        
        stringstream(scoreRecords[0]) >> levelEnum;
        stringstream(scoreRecords[1]) >> seconds;

        
        _scoreMap.Add((LevelEnum) levelEnum, seconds );
    }

#ifdef DEBUG
    _scoreMap.Dump();
#endif
    
    if ( _levelEnum )
    {
        unsigned int seconds;
        seconds = _scoreMap[ _levelEnum ].first;
        _parent->Setup_FriendPickerView( _levelEnum, seconds );
        return;
    }
    
    RLOG_SS("FACEBOOK_CHALLENGE", "LEVELS_OPEN_TOTAL", _scoreMap.size());
    Setup0();
}
//------------------------------------------------------------------------------
void FBMultiChallangeNode::ShowNoInternetSprite()
{
    _internetAccess = false;
    
    if ( _noInternetAccess )
        _noInternetAccess->Show();
    
    if ( ! _noInternetAccess )
    {
        _noInternetAccess = FadeInSprite::Create( 0.5, "Images/Facebook/Requests/noInternet.png" );
        _noInternetAccess->setPosition( ccp( Tools::GetScreenMiddleX(), Tools::GetScreenMiddleY() - 64.0f ));
        addChild(_noInternetAccess);
    }
}
//------------------------------------------------------------------------
void FBMultiChallangeNode::Setup2()
{
    //-------------------
    // Sliding menu setup    
    GameStatus *status;
    status = Game::Get()->GetGameStatus();
    GameLevelSequence &levelSeq = status->GetLevelSequence();
    
    FBMultiChallangeNodeRecord *record;
    
    _menuAccept = SlidingNodeMenu::menuWithItems( Config::eMousePriority_FBMenuLayer, _slidingNode, NULL, NULL );
    _menuView = SlidingNodeMenu::menuWithItems( Config::eMousePriority_FBMenuLayer, _slidingNode, NULL, NULL );
    _slidingNode->GetContentNode()->addChild( _menuView, 10 );
    _slidingNode->GetContentNode()->addChild( _menuAccept, 10 );
    
    int count;
    FBScoreMap::iterator sit;
    
    count = 0;
    for ( GameLevelSequence::iterator level = levelSeq.begin(); level != levelSeq.end(); level++ )
    {
        sit = _scoreMap.find( *level );
        if ( sit != _scoreMap.end() )
        {
            if ( sit->second.second != _pocket )
                continue;
            
            count++;
            record = FBMultiChallangeNodeRecord::Create( *level, _scoreMap.find( *level )->second.first );
            record->setPosition(CCPoint( -15.0f, 0.0f ));
            _slidingNode->AddToContent( record );
            
            _menuAccept->addChild( GetMenuItemAccept( *level ), 10 );
            _menuView->addChild( GetMenuItemView( *level ), 10 );
        }
    }
    
    
    //--------------
    // padding  & postition
    float padding;
    float deltaX;
    
    deltaX = 130.0f;
    padding = 145.0f;

    CCPoint startingPos;
    startingPos = CCPoint( 0.0f, 20.5f );
   
    _slidingNode->SetClickableArea( CCPoint( 0.0f, 0.0f ), CCPoint( 960.0f, 640.0f));
    _slidingNode->GetContentNode()->AlignVertical( padding );
    _slidingNode->GetContentNode()->setPosition( CCPoint( Tools::GetScreenMiddleX() - deltaX, Tools::GetScreenMiddleY() + 0.0f ));
    _slidingNode->GetContentNode()->setContentSize( CCSize( 100.0f, ( count + 1 ) * padding ));
    _slidingNode->ShowItem( 1 );
	_slidingNode->SetInitPosition( startingPos );
    
    _menuView->alignItemsVerticallyWithPadding( padding - 116.0f );
    _menuAccept->alignItemsVerticallyWithPadding( padding - 116.0f );
    
    CCPoint menuPos;
    menuPos.x = 425.0f ;
    menuPos.y =  -padding * ( count ) / 2.0f + 30.0f;

    _menuView->setPosition( menuPos );
    _menuAccept->setPosition( CCPoint( menuPos.x - 145.0f, menuPos.y ));
    
    _menuView->setOpacity( 0 );
    _menuAccept->setOpacity( 0 );
    
    AddPiggySprite( count );
}
//------------------------------------------------------------------------------
void FBMultiChallangeNode::AddPiggySprite( int count )
{
    RLOG_SS("FACEBOOK_CHALLENGE", "LEVELS_OPEN_POCKET", count);
    
    int finalOpacity;

    if ( ! count )
        finalOpacity = 255;
    else
        finalOpacity = Config::FBRequestPiggyBgOpacity;

    if ( _piggySprite && _piggySprite->getParent() )
    {
        _piggySprite->removeFromParentAndCleanup( true );
    }
    
    _piggySprite = FadeInSprite::Create( 1.0f, "Images/Facebook/Requests/noLevelsPiggy.png", 0.0f, finalOpacity );
    _piggySprite->setPosition( CCPoint( Tools::GetScreenMiddleX() - 85.0f, Tools::GetScreenMiddleY() - 100.0f ));
    addChild( _piggySprite, 2 );
}
//------------------------------------------------------------------------------
void FBMultiChallangeNode::Setup3()
{
    _menuAccept->runAction( CCFadeTo::actionWithDuration( 0.5f, 255));
    _menuView->runAction( CCFadeTo::actionWithDuration( 0.5f, 255));
    
    if ( _loading )
    {
        _loading->Stop();
        _loading = NULL;
    }
}
//------------------------------------------------------------------------------
void FBMultiChallangeNode::Setup1()
{
    _pocketButton->GetMenu()->runAction( CCFadeTo::actionWithDuration( 0.5f, 255 ));
}
//------------------------------------------------------------------------------
CCMenuItem* FBMultiChallangeNode::GetMenuItemAccept( LevelEnum level )
{
    TrippleSprite triSprite;
    FXMenuItemSpriteWithAnim *item;
    
    triSprite = TrippleSprite("Images/Facebook/Requests/acceptBt.png");
    item = FXMenuItemSpriteWithAnim::Create( &triSprite, this, menu_selector( FBMultiChallangeNode::Menu_Accept ));
    item->setUserData( (void*)level );
    
    return item;
}
//------------------------------------------------------------------------
CCMenuItem* FBMultiChallangeNode::GetMenuItemView( LevelEnum level )
{
    TrippleSprite triSprite;
    FXMenuItemSpriteWithAnim *item;
    
    triSprite = TrippleSprite("Images/Facebook/Requests/viewLevelBt.png");
    item = FXMenuItemSpriteWithAnim::Create( &triSprite, this, menu_selector( FBMultiChallangeNode::Menu_View ));
    item->setUserData( (void*)level );
    
    return item;
}
//------------------------------------------------------------------------
void FBMultiChallangeNode::Menu_View( CCObject* sender )
{
    FXMenuItemSpriteWithAnim *item;
    LevelEnum level;
    
    item = (FXMenuItemSpriteWithAnim*)sender;
    level = (LevelEnum)(long) item->getUserData();
    
    //---------------------
    // execute request
   	Preloader::Get()->Reset();
	Preloader::Get()->FreeResources();

    _parent->ShowLevel( level );
 
    RLOG_SS("FACEBOOK_CHALLENGE","VIEW_LEVEL", level );
}
//------------------------------------------------------------------------
void FBMultiChallangeNode::Menu_Accept( CCObject* sender )
{
    LevelEnum level;
    unsigned int seconds;
    FXMenuItemSpriteWithAnim *item;
    
    item = (FXMenuItemSpriteWithAnim*) sender;
    level = (LevelEnum) (long) item->getUserData();
    seconds = _scoreMap[ level ].first;

    RLOG_SS("FACEBOOK_CHALLENGE","ACCEPT_LEVEL", level );
    _parent->Setup_FriendPickerView( level, seconds );
}
//------------------------------------------------------------------------------
void FBMultiChallangeNode::Exiting()
{
    if ( _loading )
        _loading->stopAllActions();
    
    if ( _state == eState_HTTPScoreGet )
        WWWTool::Get()->CancelHttpGet( this );
    
    _state = eState_Exiting;
    stopAllActions();
}
//------------------------------------------------------------------------------
void FBMultiChallangeNode::SetPocket( PocketType pocket )
{
    RLOG_SS("FACEBOOK_CHALLENGE", "SET_POCKET", pocket );
    _pocket = pocket;
    Setup0();
}



//------------------------------------------------------------------------------
// FB Multi Challange Node Record
//------------------------------------------------------------------------------
FBMultiChallangeNodeRecord* FBMultiChallangeNodeRecord::Create( LevelEnum level, unsigned int seconds )
{
    FBMultiChallangeNodeRecord *node;
    node = new FBMultiChallangeNodeRecord( level, seconds );
    
    if ( node )
    {
        node->autorelease();
        node->Init();
        return node;
    }
    return NULL;
}
//------------------------------------------------------------------------------
FBMultiChallangeNodeRecord::FBMultiChallangeNodeRecord( LevelEnum level, unsigned int seconds )
{
    _level = level;
    _seconds = seconds;
}
//------------------------------------------------------------------------------
FBMultiChallangeNodeRecord::~FBMultiChallangeNodeRecord()
{
}
//------------------------------------------------------------------------------
void FBMultiChallangeNodeRecord::Init()
{
    string levelStr;
    stringstream levelTimeStr;
    
    FadeInLabel *levelName;
    FadeInLabel *levelTime;
    FadeInSprite *coinSprite;
    
    levelTimeStr << Tools::ConvertSecToTime( _seconds );
    levelStr = GameGlobals::Get()->LevelNames()[ _level ];
    std::replace( levelStr.begin(), levelStr.end(), '\n', ' ' );
    levelName = FadeInLabel::Create( 0.5f, levelStr.c_str(), Config::FBHighScoreFont, 32 );
    levelTime = FadeInLabel::Create( 0.5f, levelTimeStr.str().c_str(), Config::FBHighScoreFontPlain, 28, 0.5f );
    coinSprite = GetCoinSprite( _seconds );
    
    levelTime->setPosition( CCPoint( 0.0f, -33.0f ));
    levelName->setPosition( CCPoint( 0.0f, 0.0f ));
    coinSprite->setPosition( CCPoint( 0.0f, -66.0f ));
    
    levelTime->SetColor( Config::FBRequestLabelsColor );
    levelName->SetColor( Config::FBRequestLabelsColor );
    
    addChild( levelName, 10 );
    addChild( levelTime, 10 );
    addChild( coinSprite, 10 );
}
//------------------------------------------------------------------------------
FadeInSprite* FBMultiChallangeNodeRecord::GetCoinSprite( unsigned int seconds )
{
    stringstream coinStr;
    FadeInSprite *coins;
    int coinCount;
    
    coinCount = ScoreTool::Get()->GetCoinCount( seconds );
    coinStr << "Images/Facebook/Highscore/Coin" << coinCount << ".png";
    
    coins = FadeInSprite::Create( 0.5f, coinStr.str().c_str(), 1.0f );
    return coins;
}



//------------------------------------------------------------------------------
// FB Pocket Toggle Button
//------------------------------------------------------------------------------
FBPocketToggleButton* FBPocketToggleButton::Create( FBMultiChallangeNode* parent )
{
    FBPocketToggleButton* node;
    node = new FBPocketToggleButton( parent );
    
    if ( node )
    {
        node->autorelease();
        node->Init();
        return node;
    }
    return  NULL;
}
//------------------------------------------------------------------------------
FBPocketToggleButton::FBPocketToggleButton( FBMultiChallangeNode *parent )
{
    _parent = parent;
}
//------------------------------------------------------------------------------
FBPocketToggleButton::~FBPocketToggleButton()
{
    
}
//------------------------------------------------------------------------------
void FBPocketToggleButton::Init()
{
    _menu = CCMenu::menuWithItems( Config::eMousePriority_FBMenuLayerVIP, GetToggleButton(), NULL );
    _menu->setPosition( CCPointZero );
    addChild( _menu, 10 );
}
//------------------------------------------------------------------------------
CCMenuItemToggle* FBPocketToggleButton::GetToggleButton()
{
    FXMenuItemToggle *toggleButton;
    toggleButton = NULL;
    
    typedef vector<int> Vec;
    Vec vec;
    vec.push_back( 1 );
    vec.push_back( 2 );
    vec.push_back( 3 );
    vec.push_back( 4 );
    vec.push_back( 5 );
    
    for ( Vec::iterator it = vec.begin(); it != vec.end(); it++ )
    {
        //----------------
        stringstream pocketFile;
        TrippleSprite pocketSprite;
        CCMenuItemSprite *menuItem;
        
        //-----------------
        // Main menu button
        pocketFile << "Images/Facebook/Requests/PocketButton/" << *it << ".png";
        pocketSprite = TrippleSprite(pocketFile.str().c_str());

        menuItem = FXMenuItemSpriteWithAnim::Create(
                                                    pocketSprite,
                                                    this,
                                                    menu_selector( FBPocketToggleButton::Menu_SelectPocket ));
        menuItem->setUserData((void*) *it );
//        D_INT( (long)menuItem->getUserData() )
        
        if ( ! toggleButton )
            toggleButton = FXMenuItemToggle::itemWithTarget(this,
                                                            menu_selector( FBPocketToggleButton::Menu_SelectPocket ),
                                                            menuItem,
                                                            NULL );
        else
            toggleButton->addSubItem( menuItem );
    }

    return toggleButton;
}
//---------------------------------------------------------------
void FBPocketToggleButton::Menu_SelectPocket( CCObject *sender )
{
    int pocketNr;
    FXMenuItemToggle *item;
    
    item = (FXMenuItemToggle*) sender;
    pocketNr = item->getSelectedIndex() + 1;
    
    _parent->SetPocket( (PocketType) pocketNr );
    D_INT(pocketNr);
}
//------------------------------------------------------------------------------
CCMenu* FBPocketToggleButton::GetMenu()
{
    return _menu;
}



//------------------------------------------------------------------------------





//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void FBScoreMap::Dump()
{
    for ( iterator it = begin(); it != end(); it++ )
    {
        D_LOG("level: %d\t seconds:%d \tpocket:%d", it->first, it->second.first, it->second.second );
    }
}
//------------------------------------------------------------------------------
void FBScoreMap::Add( LevelEnum level, unsigned int seconds )
{
    PocketType pocket;
    
    pocket = PocketManager::Get()->LevelInWhichPocket( level );
    operator[]( level ) = LevelScoreData( seconds, pocket );
}