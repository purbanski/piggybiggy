#include "FBMultiFriendPickerNode.h"

#include "Game.h"
#include "GameStatus.h"
#include "Tools.h"
#include "FBRequestMenu.h"
#include "Components/CustomNodes.h"
#include "Components/CustomSprites.h"
#include "Components/FXMenuItem.h"
#include "Components/TrippleSprite.h"
#include "Platform/Lang.h"
#include "Debug/MyDebug.h"
#include "Facebook/Menus/FBScoreMenu.h"
#include <sstream>
#include "RunLogger/RunLogger.h"

//------------------------------------------------------------------------------
// FB Request Menu Mailbox Node
//------------------------------------------------------------------------------
FBMultiFriendPickerNode* FBMultiFriendPickerNode::Create( FBRequestMenu *parent, LevelEnum level, unsigned int seconds )
{
    FBMultiFriendPickerNode* node;
    node = new FBMultiFriendPickerNode( parent, level, seconds );
    
    if ( node )
    {
        node->autorelease();
        node->Init();
        return  node;
    }
    return NULL;
}
//------------------------------------------------------------------------------
FBMultiFriendPickerNode::FBMultiFriendPickerNode( FBRequestMenu *parent, LevelEnum level, unsigned int seconds )
{
    _state = eState_Idle;
    _loading = NULL;
    
    _level = level;
    _seconds = seconds;
    _parent = parent;
}
//------------------------------------------------------------------------------
FBMultiFriendPickerNode::~FBMultiFriendPickerNode()
{
//    if ( _state == eState_HTTPScoreGet )
//        WWWTool::Get()->CancelHttpGet( this );
}
//------------------------------------------------------------------------------
void FBMultiFriendPickerNode::Init()
{
    //--------------------------
    // Sliding node construction
    _slidingNode = SlidingNode2::node( Config::GameSize, Config::eMousePriority_FBMenuLayer );
    _slidingNode->setIsTouchEnabled( true );
    _slidingNode->setPosition( CCPoint( -120.0f, 0.0f ));
    addChild( _slidingNode, 10 );

    if ( ! _loading )
    {
        _loading = LoadingAnim::Create();
        _loading->SetPositionForFBRequestMenu();
        addChild( _loading, 50 );
    }
    
    float yd;
    yd = 125.5f;

    FadeInSprite *challangeLabelBg;
    challangeLabelBg = FadeInSprite::Create( 0.5f, "Images/Facebook/Requests/challangeLabelBg.png" );
    challangeLabelBg->setPosition( CCPoint( Tools::GetScreenMiddleX() , Tools::GetScreenMiddleY() + yd ));
    addChild( challangeLabelBg, 15 );

    string levelName;
    levelName = GameGlobals::Get()->LevelNames()[_level];
    std::replace( levelName.begin(), levelName.end(), '\n', ' ' );
    
    FadeInLabel *challangeLabel;
    challangeLabel = FadeInLabel::Create( 0.5f, levelName.c_str(), Config::FBHighScoreFont, 40);
    challangeLabel->SetColor( Config::FBRequestLabelsColor );
    challangeLabel->setPosition( CCPoint( Tools::GetScreenMiddleX(), Tools::GetScreenMiddleY() + yd + 7.5f ));
    addChild( challangeLabel, 25 );

    
    FBTool::Get()->Read_FriendsList( this );
    _state = eState_FBGettingFriends;
}
//------------------------------------------------------------------------
//void FBMultiFriendPickerNode::CleanUp()
//{
//    _slidingNode->onExit();
//    _parent->CleanUp();
//    
//    if ( _state == eState_HTTPScoreGet )
//        WWWTool::Get()->CancelHttpGet();
//}
//------------------------------------------------------------------------
void FBMultiFriendPickerNode::FBRequestCompleted( FBTool::Request requestType, void *data )
{
    FBFriendsMap *friendsMap;
    friendsMap = (FBFriendsMap*) data;
    _friendsMap = *friendsMap;
    HTTPGetMultiFriends();
}
//------------------------------------------------------------------------
void FBMultiFriendPickerNode::FBRequestError( int error )
{
    if ( _loading )
    {
        _loading->Stop();
        _loading = NULL;
    }
}
//------------------------------------------------------------------------
void FBMultiFriendPickerNode::Http_FinishedLoading( void *connection, const void *data, int len )
{
    if ( _state == eState_Exiting )
        return;
    
    _state = eState_Idle;

    string temp;
    string strData;
    
    temp.append( (const char*)data );
    strData = string( temp, 0, len );
    
    D_STRING( temp );
    D_STRING( strData );
    
    _gameUsers = FBGameUsersVector::Create( _friendsMap, strData.c_str() );

#ifdef DEBUG
    _gameUsers.Dump();
#endif
    Setup0();
    
    GameStatus *status;
    status = Game::Get()->GetGameStatus();
    if ( ! status->GetMsgPlayed( eOTMsgRequest_ChooseFriend ))
    {
        status->SetMsgPlayed( eOTMsgRequest_ChooseFriend );
        _parent->ShowHelpMsg( LocalString("ChooseFriendMsg"));
    }
}
//------------------------------------------------------------------------
void FBMultiFriendPickerNode::Http_FailedWithError( void *connection, int error )
{
    _state = eState_Idle;
    
    if ( _loading )
    {
        _loading->Stop();
        _loading = NULL;
    }    
}
//------------------------------------------------------------------------------
void FBMultiFriendPickerNode::HTTPGetMultiFriends()
{
    stringstream iUrl;
    stringstream iEnabledFriends;
    stringstream iAllFriends;
    
    iUrl << Config::FBRequestsURL << "challangeMultiFriends.php?";
    iUrl << "level=" << _level << "&";
    iUrl << "fbId=" << FBTool::Get()->GetFBUserId() << "&";
    iUrl << "friends=" << _friendsMap.GetUsersString().c_str();
    
    WWWTool::Get()->HttpGet( iUrl.str().c_str(), this );
}
//------------------------------------------------------------------------------
void FBMultiFriendPickerNode::Setup0()
{
    _slidingNode->GetContentNode()->RemoveAllNodes();
    _slidingNode->GetContentNode()->removeAllChildrenWithCleanup( true );

    if ( _loading )
    {
        _loading->Stop();
        _loading = NULL;
    }
    
    float dur;
    dur = 1.5f;
    runAction( CCSequence::actions(
                                    CCCallFunc::actionWithTarget(this, callfunc_selector(FBMultiFriendPickerNode::Setup1 )),
                                    CCDelayTime::actionWithDuration( dur ),
                                    CCCallFunc::actionWithTarget(this, callfunc_selector(FBMultiFriendPickerNode::Setup2 )),
                                    CCDelayTime::actionWithDuration( dur ),
                                    CCCallFunc::actionWithTarget(this, callfunc_selector(FBMultiFriendPickerNode::Setup3 )),
                                    NULL));

}
//------------------------------------------------------------------------------
void FBMultiFriendPickerNode::Setup1()
{
    float x, y;
    float xd, yd;
    int userCount;
    
    const int colsBreak = 5;
    int rowsUsers;
    
    rowsUsers = 0;
    userCount = 0;
    
    x = -120.0f;
    y = 0.0f;
    xd = 170.0f;
    yd = 170.0f;
    
    
    _usersMenu = SlidingNodeMenu::menuWithItems( Config::eMousePriority_FBMenuLayerVIP, _slidingNode, NULL, NULL );
    _usersMenu->setPosition( CCPoint( 0.0f, 0.0f ));
    _slidingNode->GetContentNode()->AddNode( _usersMenu );

    int countFriendsGameEnabled = 0;
    int countFriendsGameDisable = 0;
    int countUnknownGameEnabled = 0;
    
    for ( FBGameUsersVector::iterator it = _gameUsers.begin(); it != _gameUsers.end(); it++ )
    {
        CCMenuItem *profile;

        profile = it->GetMenuItem( this, menu_selector(FBMultiFriendPickerNode::Menu_SelectUser));
        profile->setPosition( CCPoint( x + xd * userCount++, y - yd * rowsUsers ));

        _usersMenu->addChild( profile, 10 );
    
        if ( userCount == colsBreak )
        {
            userCount = 0;
            rowsUsers++;
        }
        
        switch (it->_levelUserType)
        {
            case GameTypes::eGameUser_FriendGameNotInstalled :
                countFriendsGameDisable++;
                break;
                
            case GameTypes::eGameUser_FriendLevelPassed :
            case GameTypes::eGameUser_FriendLevelNotPassed :
                countFriendsGameEnabled++;
                break;
                
            case GameTypes::eGameUser_UnknownPlayerLevelNotPassed :
                countUnknownGameEnabled++;
                break;

            case eGameUser_Unknown :
            default:
                break;
        }
    }
    RLOG_SSSIII("FACEBOOK_CHALLENGE","FRIEND_PICKER", "COUNT", _gameUsers.size(),
                countFriendsGameEnabled,
                countFriendsGameDisable,
                countUnknownGameEnabled );

    
    rowsUsers++;
    
    //--------------
    // padding  & postition
    float padding;
    float deltaX;
    float contentHeight;
    
    deltaX = 112.5f;
    padding = yd;
    contentHeight = ((float)( rowsUsers ) + 1.25f ) * padding;
    
    yd = 0.0f;
    xd = 0.0f;
    
    CCPoint startingPos;
    startingPos = CCPoint( 0.0f, 0.0f );
   
    _slidingNode->SetClickableArea( CCPoint( 0.0f, 0.0f ), CCPoint( 960.0f, 640.0f));
    _slidingNode->GetContentNode()->setPosition( CCPoint( Tools::GetScreenMiddleX() - deltaX, Tools::GetScreenMiddleY() - 30.0f ));
    _slidingNode->SetUpperLimitDelta( -60.0f );
    
    _slidingNode->GetContentNode()->setContentSize( CCSize( 100.0f, contentHeight ));
    _slidingNode->ShowItem( 1 );
	_slidingNode->SetInitPosition( startingPos );
}
//------------------------------------------------------------------------------
void FBMultiFriendPickerNode::Setup2()
{
}
//------------------------------------------------------------------------------
void FBMultiFriendPickerNode::Setup3()
{
}
//------------------------------------------------------------------------------
void FBMultiFriendPickerNode::Exiting()
{
    if ( _loading )
        _loading->stopAllActions();
    
//    if ( _state == eState_HTTPScoreGet )
//        WWWTool::Get()->CancelHttpGet( this );
    
    _state = eState_Exiting;
    stopAllActions();
}
//------------------------------------------------------------------------------
void FBMultiFriendPickerNode::Menu_SelectUser( CCObject *sender )
{
    CCMenuItem *item;
    LevelUserData *userData;
    
    item = (CCMenuItem*) sender;
    userData = (LevelUserData *) item->getUserData();
    
    D_CSTRING( userData->_fbId.c_str() );
    RLOG_SS("FACEBOOK_CHALLENGE", "USER_CHOOSEN", userData->_levelUserType );
    
    FBTool::Get()->Request_LevelChallange( userData->_fbId.c_str(), _level, _seconds );
}
//------------------------------------------------------------------------------

