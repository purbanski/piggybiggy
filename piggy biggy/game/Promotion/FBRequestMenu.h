#ifndef __piggy_biggy__FBRequestMenu__
#define __piggy_biggy__FBRequestMenu__

#include <cocos2d.h>
#include <string>
#include "FBRequestsMap.h"
//#include "pu/ui/SlidingNode2.h"
//#include "Components/CustomSprites.h"
#include "Platform/WWWTool.h"
#include "Facebook/FBTool.h"
#include "FBChallangeMap.h"
#include "FBMultiChallangeNode.h"
#include "FBMultiFriendPickerNode.h"
#include "FBRequestStatsTotalsNode.h"

//------------------------------------------------------------------------------
USING_NS_CC;
//------------------------------------------------------------------------------
// FB Request Menu
//------------------------------------------------------------------------------
class FBRequestMenuStatsNode;
class FBRequestMenuMailboxNode;
class ClickCancelLayer;

class FBRequestMenu : public CCLayer
{
public:
    static FBRequestMenu* Create();
    static FBRequestMenu* CreateAndViewStats();
    static FBRequestMenu* Create( LevelEnum levelEnum );
    
    ~FBRequestMenu();
    
    virtual void onExit();

    virtual bool ccTouchBegan(CCTouch *pTouch, CCEvent *pEvent);
    virtual void ccTouchMoved(CCTouch *pTouch, CCEvent *pEvent) {}
    virtual void ccTouchEnded(CCTouch *pTouch, CCEvent *pEvent) {}
    virtual void registerWithTouchDispatcher(void);
    
    void CleanUp();
  
    void Menu_ShowStats( CCObject *sender );
    void Menu_ShowMailbox( CCObject *sender );
    void Menu_ShowMultiChallange( CCObject *sender );
    
    void ShowHelpMsg( const char *msg );
    void ShowLevel( LevelEnum levelenum );
    
    void Setup_StatsView( FBChallangeMap challanges, const char *fbId );
    void Setup_StatsView( const char *fbId );
    void Setup_MailboxView();
    void Setup_MailboxView( FBRequestsMap map );
    void Setup_FriendPickerView(LevelEnum level, unsigned int seconds );
    
protected:
    FBRequestMenu();
    bool init();
    void MyInit();
    
    virtual void ParentCleanUp();

private:
    void Setup_Main();
    void Setup_StatsTotalsView();
    void Setup_ChallangeView();
    void SetupFades();
    
protected:
    void ContentNodesCleanup();
    virtual void Menu_Back(CCObject *sender );
    
protected:
    typedef enum
    {
        eStart_Default = 0,
        eStart_LevelChallange,
        eStart_Stats
    } StartType;
    
    StartType       _startType;
    bool            _cleaned;
    
private:
    CCMenu          *_menuTop;
    CCNode          *_contentNode;
    FBRequestsMap   _fbRequestMap;
    LevelEnum       _levelEnum;

    FBRequestMenuMailboxNode        *_mailboxNode;
    FBRequestMenuStatsNode          *_statsNode;
    FBRequestMenuStatsTotalsNode    *_statsTotalNode;
    FBMultiChallangeNode            *_multiChallangeNode;
    FBMultiFriendPickerNode         *_multiFriendPicker;
    
    RequestNotifySprite *_requestNotifySprite;
    RequestNotifySprite *_challangesNotifySprite;
};




//------------------------------------------------------------------------------
// FB Request Menu Main Menu
//------------------------------------------------------------------------------
class FBRequestMenu_MainMenu : public FBRequestMenu
{
public:
    static FBRequestMenu_MainMenu* Create();

protected:
    virtual void ParentCleanUp();

private:
    virtual void Menu_Back(CCObject *sender );
};


//------------------------------------------------------------------------------
// FB Request Menu Level Menu
//------------------------------------------------------------------------------
class FBRequestMenu_LevelMenu : public FBRequestMenu
{
public:
    static FBRequestMenu_LevelMenu* Create();
    virtual void Menu_Back( CCObject* sender );

protected:
    FBRequestMenu_LevelMenu();
    virtual void ParentCleanUp();
    
private:
    bool    _backPressed;
};


//------------------------------------------------------------------------------
// FB Request Menu Item
//------------------------------------------------------------------------------
class FBRequestMenuItem : public CCNode
{
public:
    static FBRequestMenuItem *Create( const FBRequestData& requestData  );
    ~FBRequestMenuItem();
    
private:
    FBRequestMenuItem( const FBRequestData& requestData );
    void Init();
    void AddDateLabel();
    
private:
    FBRequestData   _request;
};


//------------------------------------------------------------------------------
// Click Cancel Layer
//------------------------------------------------------------------------------
class ClickCancelLayer : public CCLayer
{
public:
    static ClickCancelLayer* Create( unsigned int mousePiority );
    bool init();

    ~ClickCancelLayer();
    
    virtual void onExit();

    virtual bool ccTouchBegan(CCTouch *pTouch, CCEvent *pEvent);
    virtual void ccTouchMoved(CCTouch *pTouch, CCEvent *pEvent) {}
    virtual void ccTouchEnded(CCTouch *pTouch, CCEvent *pEvent) {}
    virtual void registerWithTouchDispatcher(void);

    
protected:
    ClickCancelLayer( unsigned int mousePiority );
    
protected:
    unsigned int _mousePiority;
};




#endif /* defined(__piggy_biggy__FBRequestMenu__) */
