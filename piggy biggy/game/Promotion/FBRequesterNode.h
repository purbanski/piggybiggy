#ifndef __piggy_biggy__FBRequesterNode__
#define __piggy_biggy__FBRequesterNode__
//--------------------------------------------------------------------------
#include <cocos2d.h>
#include "FBRequestsMap.h"
#include "Components/CustomNodes.h"
//--------------------------------------------------------------------------
USING_NS_CC;
//--------------------------------------------------------------------------
class FBRequesterNode : public CCNode
{
public:
    static FBRequesterNode* Create( FBRequestsMap *requestsMap );
    ~FBRequesterNode();
    
private:
    FBRequesterNode();
    void Init( FBRequestsMap *requestMap );
    CCNode* GetNodeItem( FBRequestsMap::iterator* it );
    
private:
    FBRequestsMap _requestsMap;
    NodeAlign     *_contentNode;
};
//--------------------------------------------------------------------------
#endif

