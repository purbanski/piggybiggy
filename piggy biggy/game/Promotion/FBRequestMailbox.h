#ifndef __FBREQUESTMENU_H__
#define __FBREQUESTMENU_H__
//------------------------------------------------------------------------------
#include <cocos2d.h>
#include <string>
#include "FBRequestsMap.h"
#include "pu/ui/SlidingNode2.h"
#include "Components/CustomSprites.h"
#include "Platform/WWWTool.h"
#include "Facebook/FBTool.h"
#include "FBChallangeMap.h"
#include "FBMultiChallangeNode.h"
#include "FBMultiFriendPickerNode.h"
#include "FBRequestStatsTotalsNode.h"
//------------------------------------------------------------------------------
USING_NS_CC;
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// FB Request Menu Mailbox Node
//------------------------------------------------------------------------------
class FBRequestMenuMailboxNode : public CCNode, public FBToolListener, public WWWToolListener
{
public:
    static FBRequestMenuMailboxNode* Create( FBRequestMenu *parent, FBRequestsMap *requestMap = NULL );
    ~FBRequestMenuMailboxNode();

    virtual void FBRequestCompleted( FBTool::Request requestType, void *data );
    virtual void FBRequestError( int error );
    
    virtual void Http_FailedWithError( void *connection, int error );
    virtual void Http_FinishedLoading( void *connection, const void *data, int len );
    
    void Exiting();
    
private:
    FBRequestMenuMailboxNode( FBRequestMenu *parent );
    void Init( FBRequestsMap *map );

    void Setup0();
    void Setup1();
    void Setup2();
    void Setup3();
   
    CCMenuItem* GetMenuItemAccept( const char *requestId );
    CCMenuItem* GetMenuItemRemove( const char *requestId );

    void Menu_Accept(CCObject *sender );
    void Menu_Remove(CCObject *sender );

    void ChallangeAccept(
                         const char *fromUser,
                         GameTypes::LevelEnum levelEnum,
                         unsigned int seconds,
                         bool managed );
    void CleanUp();
    void AddPiggySprite();
    void ShowNoInternetAccess();
    
protected:
    bool                    _internetAccess;
    FadeInSprite            *_noInternetAccess;

    CCMenu *_menuAccept;
    CCMenu *_menuRemove;
    LoadingAnim *_loading;

    SlidingNode2    *_slidingNode;
    FBRequestMenu   *_parent;
    FBRequestsMap   _requestMap;
    string          _requestedUser;
    
    typedef enum
    {
        eState_Idle = 0,
        eState_FBProcessing,
        eState_HTTPGet,
        eState_Exiting
    } State;
    
    State   _state;
};


#endif

