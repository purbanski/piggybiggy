#include "FBRequestsMap.h"
#include "Debug/MyDebug.h"
#include "Tools.h"
#include <sstream>
//--------------------------------------------------------------------------
// FBRequestsMap
//--------------------------------------------------------------------------
void FBRequestsMap::Add(
                        const char *objectId,
                        const char *fromUsername,
                        const char *fromUserFbId,
                        const char *createdDate,
                        const char *message,
                        const char *requestData )
{
 
    string createdTimeStr;
    string createdDateStr;
    
    CopyCreatedTime( createdDate, createdTimeStr );
    CopyCreatedDate( createdDate, createdDateStr );
    
    if ( ! requestData )
        operator[]( string(objectId) ) = FBRequestData(
                                                       fromUsername,
                                                       fromUserFbId,
                                                       createdTimeStr.c_str(),
                                                       createdDateStr.c_str(),
                                                       message  );
    else
        operator[]( string(objectId) ) = FBRequestData(
                                                       fromUsername,
                                                       fromUserFbId,
                                                       createdTimeStr.c_str(),
                                                       createdDateStr.c_str(),
                                                       message,
                                                       requestData );
}
//--------------------------------------------------------------------------
void FBRequestsMap::Dump()
{
    for ( iterator it = begin(); it != end(); it++ )
    {
        D_LOG("(%s):\n\t%s\n\t%s\n\t%s\n\t%s\n\t%s\n\t%s",
              it->first.c_str(),
              it->second._fromUserFbId.c_str(),
              it->second._fromUsername.c_str(),
              it->second._createdTime.c_str(),
              it->second._createdDate.c_str(),
              it->second._message.c_str(),
              it->second._requestData.c_str() );
    }
}
//--------------------------------------------------------------------------
void FBRequestsMap::CopyCreatedDate( const char *src, string &dest )
{
    char date[11];
    
    date[10] = 0;
    memcpy( date, src, 10 );
    
    dest.append( date );
}
//--------------------------------------------------------------------------
void FBRequestsMap::CopyCreatedTime( const char *src, string &dest )
{
    char time[9];
    
    time[8] = 0;
    memcpy( time, (src+11), 8 );
    
    dest.append( time );
}





//--------------------------------------------------------------------------
// FB Request Data
//--------------------------------------------------------------------------
FBRequestData::FBRequestData()
{
    
}
//--------------------------------------------------------------------------
FBRequestData::FBRequestData(
                             const char *fromUsername,
                             const char *fromUserFbId,
                             const char *createdTime,
                             const char *createdDate,
                             const char *message
                            )
{    
    Construct( fromUsername, fromUserFbId, createdTime, createdDate, message, "" );
}
//--------------------------------------------------------------------------
FBRequestData::FBRequestData(
                             const char *fromUsername,
                             const char *fromUserFbId,
                             const char *createdTime,
                             const char *createdDate,
                             const char *message,
                             const char *requestData
                            )
{
    Construct( fromUsername, fromUserFbId, createdTime, createdDate, message, requestData );
}
//--------------------------------------------------------------------------
void FBRequestData::Construct(
                  const char *fromUsername,
                  const char *fromUserFbId,
                  const char *createdTime,
                  const char *createdDate,
                  const char *message,
                  const char *requestData )
{
    _fromUsername.append( fromUsername );
    _fromUserFbId.append( fromUserFbId );
    _createdTime.append( createdTime );
    _createdDate.append( createdDate );
    _message.append( message );
    _requestData.append( requestData );
    
    typedef vector<string> Strings;
    Strings data;
    
    Tools::StringSplit( _requestData, ':', data );
    D_SIZE(data);
    
    if ( data.size() < 1 )
        return;
//
//    {
//        int temp;
//        stringstream convert( data[0].c_str() );
//        convert >> temp;
//    
//        _requestType = (FBRequestType) temp;
//    }

    
    //-------------------------------
    // if request is level challange

    _requestType = eFBRequest_LevelChallange;
    if ( _requestType == eFBRequest_LevelChallange )
    {
        {
            int temp;
            stringstream convert( data[0].c_str() );
            convert >> temp;
    
            _levelEnum = (LevelEnum) temp;
        }
        
        {
            int temp;
            stringstream convert( data[1].c_str() );
            convert >> temp;
    
            _levelCompletedSeconds = temp;
        }
        
    }
}
//--------------------------------------------------------------------------

