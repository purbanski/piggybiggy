#include <sstream>
#include "FBRequestMenu.h"
#include "Platform/Lang.h"
#include "RunLogger/RunLogger.h"
#include "RunLogger/GLogger.h"
#include "Tools.h"
#include "Game.h"
#include "Components/CustomSprites.h"
#include "Debug/MyDebug.h"
#include "ScaleScreenLayer.h"
#include "Components/TrippleSprite.h"
#include "Components/FXMenuItem.h"
#include "FBRequestMailbox.h"
#include "FBRequestsStatsNode.h"
#include "FBRequestStatsTotalsNode.h"
#include "FBMultiChallangeNode.h"
#include "FBMultiFriendPickerNode.h"
#include "FBViewLevelNode.h"
#include "FBRequester.h"
#include "LevelFactory.h"
#include "Scenes/MainMenuScene.h"
//------------------------------------------------------------------------
// FB Request Menu Item
//------------------------------------------------------------------------
FBRequestMenuItem *FBRequestMenuItem::Create( const FBRequestData& requestData  )
{
    FBRequestMenuItem *item;
    item = new FBRequestMenuItem( requestData );
    if ( item )
    {
        item->autorelease();
        item->Init();
        return item;
    }
    return NULL;
}
//------------------------------------------------------------------------
FBRequestMenuItem::FBRequestMenuItem( const FBRequestData &requestData )
{
    _request = requestData;
}
//------------------------------------------------------------------------
FBRequestMenuItem::~FBRequestMenuItem()
{
    
}
//------------------------------------------------------------------------
void FBRequestMenuItem::Init()
{
    FBProfileBigSprite *fbProfile;
    FBUserFirstNameNode *nameNode;

    fbProfile = FBProfileBigSprite::Create( _request._fromUserFbId.c_str() );
    nameNode = FBUserFirstNameNode::CreateCenter( _request._fromUserFbId.c_str() );
    
    fbProfile->setPosition( CCPoint( -30.0f, 0.0f ));
    nameNode->setPosition( CCPoint( -20.00f, -65.0f ));
    nameNode->SetColor( Config::FBRequestLabelsColor );
    
    addChild( fbProfile, 10 );
    addChild( nameNode, 10 );

    CCLabelTTF *message;
    stringstream msg;
    
    int maxLen;
    maxLen = 15;
    
    msg << LocalString("ChallangeStr1") << endl;
    msg << LocalString("Level") << ": " << GameGlobals::Get()->LevelNames()[ _request._levelEnum ];
    
    message = CCLabelTTF::labelWithString( msg.str().c_str(), Config::FBHighScoreFontPlain,  32 );
    message->setOpacity( 0 );
    message->setPosition( CCPoint( 265.0f, 15.0f ));
    message->setColor(Config::FBRequestLabelsColor);
    addChild( message );    
    
    message->runAction(
                       CCSequence::actions(
                                           CCFadeTo::actionWithDuration( 0.35f, 255 ),
                                           CCCallFunc::actionWithTarget( this, callfunc_selector( FBRequestMenuItem::AddDateLabel )),
                                           NULL ));
    
    FadeInSprite *line;
    line = FadeInSprite::Create( 0.5f, "Images/Facebook/Requests/line.png", 0.25f );
    line->setPosition( CCPoint( 265.0f, -60.0f ));
    addChild( line, 10 );    
}
//------------------------------------------------------------------------
void FBRequestMenuItem::AddDateLabel()
{
//    FadeInLabel *timeLabel;
    FadeInLabel *dataLabel;
    
//    timeLabel = FadeInLabel::Create( 0.5f, _request._createdTime.c_str(), Config::FBHighScoreFontPlain, 20 );
    dataLabel = FadeInLabel::Create( 0.5f, _request._createdDate.c_str(), Config::FBHighScoreFontPlain, 20, 0.15f );
    
//    addChild( timeLabel, 10 );
    addChild( dataLabel, 10 );
    
//    timeLabel->setPosition( CCPoint( 265.0f, -75.0f ));
    dataLabel->setPosition( CCPoint( 410.0f, -77.5f ));

//    timeLabel->GetLabel()->setColor( Config::FBRequestLabelsColor );
    dataLabel->GetLabel()->setColor( Config::FBRequestLabelsColor );
}





//------------------------------------------------------------------------
// FB Request Menu
//------------------------------------------------------------------------
FBRequestMenu* FBRequestMenu::Create()
{
    FBRequestMenu *node;
    node = new FBRequestMenu();
    
    if ( node && node->init() )
    {
        node->autorelease();
        node->MyInit();
        return node;
    }
    return NULL;
}
//------------------------------------------------------------------------
FBRequestMenu* FBRequestMenu::CreateAndViewStats()
{
    FBRequestMenu *node;
    node = new FBRequestMenu();
    
    if ( node )
        node->_startType = eStart_Stats;
    
    if ( node && node->init() )
    {
        node->autorelease();
        node->MyInit();
        return node;
    }
    return NULL;
}
//------------------------------------------------------------------------
FBRequestMenu* FBRequestMenu::Create( LevelEnum levelEnum )
{
    FBRequestMenu *node;
    node = new FBRequestMenu();
    node->_levelEnum = levelEnum;
    node->_startType = eStart_LevelChallange;
    
    if ( node && node->init() )
    {
        node->autorelease();
        node->MyInit();
        return node;
    }
    return NULL;
}
//------------------------------------------------------------------------
FBRequestMenu::FBRequestMenu()
{
    _cleaned = false;
    _levelEnum = (LevelEnum) 0;
    _statsNode = NULL;
    _statsTotalNode = NULL;
    _mailboxNode = NULL;
    _multiFriendPicker = NULL;
    _requestNotifySprite = NULL;
    _challangesNotifySprite = NULL;
    _multiChallangeNode = NULL;
    _startType = eStart_Default;
    
    RLOG("FB_SHOW_STATS");
    GSendView("FB Request Menu");
}
//------------------------------------------------------------------------
FBRequestMenu::~FBRequestMenu()
{
    CleanUp();
}
//------------------------------------------------------------------------
bool FBRequestMenu::init()
{
    if ( ! CCLayer::init() )
        return false;
    
    setIsTouchEnabled( true );
    return true;
}
//------------------------------------------------------------------------
void FBRequestMenu::registerWithTouchDispatcher(void)
{
	CCTouchDispatcher::sharedDispatcher()->addTargetedDelegate( this, Config::eMousePriority_FBMenuLayerVIPAbove, true );
}
//------------------------------------------------------------------------
bool FBRequestMenu::ccTouchBegan( CCTouch *pTouch, CCEvent *pEvent)
{
    b2Vec2 pos;
    pos = Tools::TouchAdjustToGame( pTouch );
    D_POINT(pos);
    if ( pos.y > 490.0f || pos.y < 50.0f)
        return true;
    
    return false;
}
//------------------------------------------------------------------------
void FBRequestMenu::MyInit()
{
    //----------------------
    // scale layer 
    if ( Tools::GetScreenSize().width != Config::GameSize.width  || Tools::GetScreenSize().height != Config::GameSize.height )
	{
		CCLayer* scaleScreenLayer = ScaleScreenLayer::node();
		scaleScreenLayer->setPosition( Tools::GetScreenMiddle() );
		addChild( scaleScreenLayer, 250 );
	}
    
    //-------------------
    // Click cancel layer
    ClickCancelLayer *clickCancel;
    clickCancel = ClickCancelLayer::Create( Config::eMousePriority_FBIgnoreLayer );
    addChild( clickCancel );
    
    //-------------------
    // Content Node
    _contentNode = CCNode::node();
    _contentNode->setPosition( CCPoint( 0.0f, 0.0f ));
    addChild( _contentNode, 100 );
    
    
    //-------------------
    // Notification Nodes
    stringstream urlFbRequests;
    stringstream urlChallangesUpdates;
    
    urlFbRequests << Config::FBRequestsURL << "checkRequestNotify.php?fbId=" << FBTool::Get()->GetFBUserId().c_str();
    urlChallangesUpdates << Config::FBRequestsURL << "challanageUpdateNotifyCheck.php?fbId=" << FBTool::Get()->GetFBUserId().c_str();
  
    _requestNotifySprite = RequestNotifySprite::Create( urlFbRequests.str().c_str() );
    _challangesNotifySprite = RequestNotifySprite::Create( urlChallangesUpdates.str().c_str() );

    _requestNotifySprite->setPosition( CCPoint( Tools::GetScreenMiddleX() - 155.0f, Tools::GetScreenMiddleY() + 285.0f ));
    _challangesNotifySprite->setPosition( CCPoint( Tools::GetScreenMiddleX() + 20.0f, Tools::GetScreenMiddleY() + 285.0f ));
    
    addChild( _requestNotifySprite, 240 );
    addChild( _challangesNotifySprite, 240 );

    
    //-------------------
    // Background
    CCSprite *background;
    background = CCSprite::spriteWithFile("Images/Facebook/Requests/background.jpg");
    background->setOpacity( 0 );
    background->setPosition( CCPoint( Tools::GetScreenMiddle() ));
    addChild( background, 1 );

    CCFiniteTimeAction *runSeq;
    CCFiniteTimeAction *mainSeq;
    
    mainSeq = CCSequence::actions(
                                  CCFadeTo::actionWithDuration( 0.25f, 255 ),
                                  CCCallFunc::actionWithTarget(this, callfunc_selector(FBRequestMenu::SetupFades )),
                                  CCDelayTime::actionWithDuration( 0.2f ),
                                  CCCallFunc::actionWithTarget(this, callfunc_selector(FBRequestMenu::Setup_Main )),
                                  CCDelayTime::actionWithDuration( 0.25f ),
                                  NULL
                                  );
    
    switch ( _startType )
    {
        case eStart_Stats :
            runSeq = CCSequence::actions( mainSeq,
                                         CCCallFunc::actionWithTarget(this, callfunc_selector(FBRequestMenu::Setup_StatsTotalsView )),
                                         NULL );
            break;
            
        default:
            runSeq = CCSequence::actions( mainSeq,
                                         CCCallFunc::actionWithTarget(this, callfunc_selector(FBRequestMenu::Setup_ChallangeView )),
                                         NULL );
            break;
    }
    background->runAction( runSeq );
 }
//------------------------------------------------------------------------
void FBRequestMenu::Setup_Main()
{
    //-------------
    // Back menu setup
    CCMenu *backMenu;
    CCMenuItem *item;
    TrippleSprite triSprite;
    
    triSprite = TrippleSprite( "Images/Facebook/Requests/backBt.png");
    
    item = FXMenuItemSpriteWithAnim::Create( &triSprite, this, menu_selector( FBRequestMenu::Menu_Back ) );
    backMenu = CCMenu::menuWithItems( Config::eMousePriority_FBMenuLayerVIPAboveX2, item, NULL );
    backMenu->setPosition( CCPoint( Tools::GetScreenMiddleX() + 390.0f, Tools::GetScreenMiddleY() + 245.0f ));
    backMenu->setOpacity( 0 );
    backMenu->runAction( CCFadeTo::actionWithDuration( 0.5f, 245 ));
    addChild( backMenu, 240 );

    //---------
    // Top Menu
    TrippleSprite mailboxSprite;
    TrippleSprite statsSprite;
    TrippleSprite multiChallangeSprite;
    
    mailboxSprite = TrippleSprite("Images/Facebook/Requests/mailboxBt.png");
    statsSprite = TrippleSprite("Images/Facebook/Requests/statsBt.png");
    multiChallangeSprite = TrippleSprite("Images/Facebook/Requests/challangeBt.png");
    
    CCMenuItem *statsItem;
    CCMenuItem *mailboxItem;
    CCMenuItem *multiChallangeItem;
    
    statsItem = FXMenuItemSpriteWithAnim::Create( &statsSprite, this, menu_selector( FBRequestMenu::Menu_ShowStats ));
    mailboxItem = FXMenuItemSpriteWithAnim::Create( &mailboxSprite, this, menu_selector(FBRequestMenu::Menu_ShowMailbox ));
    multiChallangeItem = FXMenuItemSpriteWithAnim::Create( &multiChallangeSprite, this, menu_selector(FBRequestMenu::Menu_ShowMultiChallange ));

    _menuTop = CCMenu::menuWithItems( Config::eMousePriority_FBMenuLayerVIPAboveX2, multiChallangeItem, mailboxItem, statsItem, NULL );
    _menuTop->setOpacity( 0 );
    _menuTop->setPosition( ccp(Tools::GetScreenMiddleX()- 200.0f, Tools::GetScreenMiddleY()+Config::GameSize.height / 2.0f - 75.0f ));
    _menuTop->alignItemsHorizontallyWithPadding( 35.0f );
    addChild( _menuTop, 110 );

    _menuTop->runAction( CCFadeTo::actionWithDuration( 0.5f, 245 ));
}
//------------------------------------------------------------------------
void FBRequestMenu::ContentNodesCleanup()
{
    if ( _mailboxNode )
    {
        _mailboxNode->Exiting();
        _mailboxNode = NULL;
    }
    
    if ( _statsTotalNode )
    {
        _statsTotalNode->Exiting();
        _statsTotalNode = NULL;
    }
    
    if ( _statsNode )
    {
        _statsNode->Exiting();
        _statsNode = NULL;
    }
    
    if ( _multiChallangeNode )
    {
        _multiChallangeNode->Exiting();
        _multiChallangeNode = NULL;
    }
    
    if ( _multiFriendPicker )
    {
        _multiFriendPicker->Exiting();
        _multiFriendPicker = NULL;
    }
}
//------------------------------------------------------------------------
void FBRequestMenu::Setup_MailboxView( FBRequestsMap map )
{
    RLOG_S("FACEBOOK_MAILBOX","VIEW_MAILBOX");
    
    stringstream url;
    url << Config::FBRequestsURL << "deleteRequestNotify.php?fbId=" << FBTool::Get()->GetFBUserId().c_str();
    WWWTool::Get()->HttpGet( url.str().c_str() );
    
//    if ( _requestNotifySprite )
//        _requestNotifySprite->Hide();
    
    ContentNodesCleanup();
    _contentNode->removeAllChildrenWithCleanup( true );
    
    _mailboxNode = FBRequestMenuMailboxNode::Create( this, &map );
    _mailboxNode->setPosition(ccp(0.0f, 0.0f));
    _contentNode->addChild( _mailboxNode, 10 );
}
//------------------------------------------------------------------------
void FBRequestMenu::Setup_MailboxView()
{
    RLOG_S("FACEBOOK_MAILBOX","VIEW_MAILBOX");
    
    stringstream url;
    url << Config::FBRequestsURL << "deleteRequestNotify.php?fbId=" << FBTool::Get()->GetFBUserId().c_str();
    WWWTool::Get()->HttpGet( url.str().c_str() );
    
//    if ( _requestNotifySprite )
//        _requestNotifySprite->Hide();
    
    ContentNodesCleanup();
    _contentNode->removeAllChildrenWithCleanup( true );
    
    _mailboxNode = FBRequestMenuMailboxNode::Create( this );
    _mailboxNode->setPosition(ccp(0.0f, 0.0f));
    _contentNode->addChild( _mailboxNode, 10 );
}
//------------------------------------------------------------------------
void FBRequestMenu::Setup_StatsView( FBChallangeMap challanges, const char *fbId )
{
    RLOG_S("FACEBOOK_STATS","VIEW_STATS_USER" );
    
    string fbIdTemp;
    fbIdTemp = fbId;

    ContentNodesCleanup();
    _contentNode->removeAllChildrenWithCleanup( true );
    
    _statsNode = FBRequestMenuStatsNode::Create( this, challanges, fbIdTemp.c_str() );
    _statsNode->setPosition(ccp( 0.0f, 0.0f));
    _contentNode->addChild( _statsNode, 10 );
}
//------------------------------------------------------------------------
void FBRequestMenu::Setup_StatsView( const char *fbId )
{
    RLOG_S("FACEBOOK_STATS","VIEW_STATS_TOTAL" );

    string fbIdTemp;
    fbIdTemp = fbId;
    
    //---- this can destron fbId
    // that's why it saved before
    ContentNodesCleanup();
    _contentNode->removeAllChildrenWithCleanup( true );
    
    _statsTotalNode = FBRequestMenuStatsTotalsNode::Create( this, fbIdTemp.c_str() );
    _statsTotalNode->setPosition(ccp( 0.0f, 0.0f));
    _contentNode->addChild( _statsTotalNode, 10 );
}
//------------------------------------------------------------------------
void FBRequestMenu::Setup_StatsTotalsView()
{    
    ContentNodesCleanup();
    _contentNode->removeAllChildrenWithCleanup( true );
    
    _statsTotalNode = FBRequestMenuStatsTotalsNode::Create( this );
    _statsTotalNode->setPosition(ccp( 0.0f, 0.0f));
    _contentNode->addChild( _statsTotalNode, 10 );
}
//------------------------------------------------------------------------
void FBRequestMenu::Setup_ChallangeView()
{
    RLOG_S("FACEBOOK_CHALLENGE","VIEW_CHALLANGE_VIEW");
    
    ContentNodesCleanup();
    _contentNode->removeAllChildrenWithCleanup( true );
    
    if ( _levelEnum )
        _multiChallangeNode = FBMultiChallangeNode::Create( this, _levelEnum );
    else
        _multiChallangeNode = FBMultiChallangeNode::Create( this );
    
    _multiChallangeNode->setPosition(ccp( 0.0f, 0.0f));
    _contentNode->addChild( _multiChallangeNode, 10 );
}
//------------------------------------------------------------------------
void FBRequestMenu::Setup_FriendPickerView(LevelEnum level, unsigned int seconds )
{
    RLOG_SSS("FACEBOOK_CHALLENGE","FRIEND_PICKER", "LEVEL", level );
    
    ContentNodesCleanup();
    _contentNode->removeAllChildrenWithCleanup( true );
    
    _multiFriendPicker = FBMultiFriendPickerNode::Create( this, level, seconds );
    _multiFriendPicker->setPosition(ccp( 0.0f, 0.0f));
    _contentNode->addChild( _multiFriendPicker, 10 );
}
//------------------------------------------------------------------------
void FBRequestMenu::Menu_ShowMailbox( CCObject *sender )
{
    Setup_MailboxView();
}
//------------------------------------------------------------------------
void FBRequestMenu::Menu_ShowStats( CCObject *sender )
{
    Setup_StatsTotalsView();
}
//------------------------------------------------------------------------
void FBRequestMenu::Menu_ShowMultiChallange( CCObject *sender )
{
    Setup_ChallangeView();
}
//------------------------------------------------------------------------
void FBRequestMenu::Menu_Back( CCObject* sender )
{
    RLOG_S("FACEBOOK_MENU", "EXIT_MENU");
    CleanUp();
}
//------------------------------------------------------------------------
void FBRequestMenu::onExit()
{
    CleanUp();
}
//------------------------------------------------------------------------
void FBRequestMenu::CleanUp()
{
    if ( ! _cleaned )
    {
        _cleaned = true;
        
        ContentNodesCleanup();
        removeAllChildrenWithCleanup( true );
        ParentCleanUp();
        
        FBRequester::Get()->Restart();
        CCTouchDispatcher::sharedDispatcher()->removeDelegate( this );

        if ( getParent() )
            removeFromParentAndCleanup( true );
    }
}
//------------------------------------------------------------------------
void FBRequestMenu::ParentCleanUp()
{
}
//------------------------------------------------------------------------
void FBRequestMenu::SetupFades()
{
    CCSprite *topFade;
    CCSprite *bottomFade;
    
    topFade = CCSprite::spriteWithFile( "Images/Facebook/Requests/topFade.png");
    bottomFade =CCSprite::spriteWithFile( "Images/Facebook/Requests/bottomFade.png");
    
    addChild( topFade, 100 );
    addChild( bottomFade, 100 );
    
    CCSize sizeTop;
    CCSize sizeBottom;
    
    sizeTop = topFade->getContentSize();
    sizeBottom = bottomFade->getContentSize();
    
    topFade->setPosition( CCPoint(
                                  Tools::GetScreenMiddleX(),
                                  Tools::GetScreenMiddleY() + (Config::GameSize.height-sizeTop.height) / 2.0f  ));
    
    bottomFade->setPosition( CCPoint(
                                     Tools::GetScreenMiddleX(),
                                     Tools::GetScreenMiddleY() - (Config::GameSize.height-sizeBottom.height) / 2.0f - 1.0f ));
    
    topFade->setOpacity(0);
    bottomFade->setOpacity(0);
    
    topFade->runAction(CCFadeTo::actionWithDuration(0.25f, 255));
    bottomFade->runAction(CCFadeTo::actionWithDuration(0.25f, 255));
}
//------------------------------------------------------------------------
void FBRequestMenu::ShowHelpMsg( const char *msg )
{
    FadeInFadeOutLabel *msgLabel;
    msgLabel = FadeInFadeOutLabel::Create( 0.5f, 0.5f, 3.0f, 0.5f,
                                         msg, Config::FBHighScoreFont, 55 );
    msgLabel->SetColor( Config::FBRequestHelpMsgColor );
//    msgLabel->setPosition( ccp( Tools::GetScreenMiddle().x, Tools::GetScreenMiddle().y - 235.0f ));
    msgLabel->setPosition( ccp( Tools::GetScreenMiddle().x, Tools::GetScreenMiddle().y - 207.0f ));
    addChild( msgLabel, 230 );
}
//------------------------------------------------------------------------
void FBRequestMenu::ShowLevel(LevelEnum levelEnum )
{
    LevelFactory levelFactory;
    Level *level;
    
    level = levelFactory.CreateViewLevel( levelEnum );
    level->setContentSize( CCSize( 1.0f, 1.0f ));
    level->setContentSizeInPixels( CCSize( 1.0f, 1.0f ));
    level->SetViewMode( true );
    
    FBViewLevelNode *node;
    node = FBViewLevelNode::Create( level );
    node->setPosition( CCPoint( 0.0f, 0.0f ));
    addChild( node, 250 );
}



//------------------------------------------------------------------------
// FB Request Menu Ext
//------------------------------------------------------------------------
FBRequestMenu_MainMenu* FBRequestMenu_MainMenu::Create()
{
    FBRequestMenu_MainMenu *node;
    node = new FBRequestMenu_MainMenu();
    if ( node && node->init() )
    {
        node->autorelease();
        node->MyInit();
        return node;
    }
    return NULL;
}
//------------------------------------------------------------------------
void FBRequestMenu_MainMenu::ParentCleanUp()
{
    MainMenuScene *scene;
    if ( getParent() )
    {
        scene = (MainMenuScene*) getParent();
        scene->ExitClenaUp();
    }
}
//------------------------------------------------------------------------
void FBRequestMenu_MainMenu::Menu_Back( CCObject* sender )
{
    RLOG_S("FACEBOOK_MENU", "EXIT_MENU");
    
    _cleaned = true;
        
    ContentNodesCleanup();
    removeAllChildrenWithCleanup( true );
    
    FBRequester::Get()->Restart();
    CCTouchDispatcher::sharedDispatcher()->removeDelegate( this );

    if ( getParent() )
        removeFromParentAndCleanup( true );
}



//------------------------------------------------------------------------
// FB Request Menu Level
//------------------------------------------------------------------------
FBRequestMenu_LevelMenu* FBRequestMenu_LevelMenu::Create()
{
    FBRequestMenu_LevelMenu *node;
    node = new FBRequestMenu_LevelMenu();
    
    if ( node )
        node->_startType = eStart_Stats;
    
    if ( node && node->init() )
    {
        
        node->autorelease();
        node->MyInit();
        return node;
    }
    return NULL;
}
//------------------------------------------------------------------------
FBRequestMenu_LevelMenu::FBRequestMenu_LevelMenu()
{
    _backPressed = false;
}
//------------------------------------------------------------------------
void FBRequestMenu_LevelMenu::ParentCleanUp()
{
    if ( _backPressed )
        return;
    
    Level *level;
    level = Game::Get()->GetRuningLevel();
    
    level->Quiting();
}
//------------------------------------------------------------------------
void FBRequestMenu_LevelMenu::Menu_Back( CCObject *sender )
{
    _backPressed = true;
    FBRequestMenu::Menu_Back( sender );
}


//------------------------------------------------------------------------------
// Click Cancel Layer
//------------------------------------------------------------------------------
ClickCancelLayer* ClickCancelLayer::Create( unsigned int mousePiority )
{
    ClickCancelLayer *clickCancel;
    clickCancel = new ClickCancelLayer( mousePiority );
    
    if ( clickCancel && clickCancel->init())
    {
        clickCancel->autorelease();
        return clickCancel;
    }
    
    return NULL;
}
//---------------------------------------------------------------
ClickCancelLayer::ClickCancelLayer( unsigned int mousePiority )
{
    _mousePiority = mousePiority;
}
//---------------------------------------------------------------
ClickCancelLayer::~ClickCancelLayer()
{
    
}
//---------------------------------------------------------------
bool ClickCancelLayer::init()
{
    if ( ! CCLayer::init() )
        return false;
    
    setIsTouchEnabled( true );
    return true;
}
//---------------------------------------------------------------
void ClickCancelLayer::onExit()
{
    CCTouchDispatcher::sharedDispatcher()->removeDelegate( this );
}
//---------------------------------------------------------------
bool ClickCancelLayer::ccTouchBegan(CCTouch *pTouch, CCEvent *pEvent)
{
    return true;
}
//---------------------------------------------------------------
void ClickCancelLayer::registerWithTouchDispatcher(void)
{
   	CCTouchDispatcher::sharedDispatcher()->addTargetedDelegate( this, _mousePiority, true );
}
//---------------------------------------------------------------
