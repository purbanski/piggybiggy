#ifndef __FBPOSTER_H__
#define __FBPOSTER_H__
//---------------------------------------------------------------------------------
#include <map>
#include "FBPosts.h"
#include "GameTypes.h"
//---------------------------------------------------------------------------------
using namespace std;
using namespace GameTypes;
//---------------------------------------------------------------------------------
class FBPoster
{
public:
    typedef enum {
        eFBPost_LevelAnyCompleted = 1,
        eFBPost_LevelCarCompleted,
        eFBPost_LevelBankCompleted,
        eFBPost_LevelHackerCompleted,
        eFBPost_LevelSpitterCompleted,
        eFBPost_LevelBombsCompleted,
        eFBPost_LevelReelCompleted,
        eFBPost_LevelBalancerCompleted,
        eFBPost_LevelFliesCompleted,
        eFBPost_LevelTetrisCompleted,
        eFBPost_LevelCopCompleted,
        eFBPost_LevelFrigleCompleted,
        eFBPost_LevelThiefCompleted,
        eFBPost_LevelBoxingCompleted,
        eFBPost_LevelMagnetBombCompleted,
        eFBPost_LevelButtonCompleted
    } FBPostEnum;
    
public:
	static FBPoster* Get();
	static void Destroy();

	~FBPoster();
    
    void Post_Wall( FBPostEnum post );
    void Post_LevelCompleted( LevelEnum levelEnum, LevelTag levelTag );
    void Post_LevelSkipped( LevelEnum levelEnum, LevelTag levelTag );
    
    void Achievement_1();
    
    void RandomPost();
    
private:
    FBPoster();
    FBPost Post_LevelBase( LevelEnum levelEnum, LevelTag levelTag );
    
    struct Posts
    {
        static FBPost LevelAnyCompleted();
        static FBPost LevelCarCompleted();
        static FBPost LevelBankCompleted();
        static FBPost LevelHackerCompleted();
        static FBPost LevelSpitterCompleted();
        static FBPost LevelBombsCompleted();
        static FBPost LevelReelCompleted();
        static FBPost LevelBalancerCompleted();
        static FBPost LevelFliesCompleted();
        static FBPost LevelTetrisCompleted();
        static FBPost LevelCopCompleted();
        static FBPost LevelFrigleCompleted();
        static FBPost LevelThiefCompleted();
        static FBPost LevelBoxingCompleted();
        static FBPost LevelMagnetBombCompleted();
        static FBPost LevelButtonCompleted();
    };

private:
    typedef FBPost (*FnPtr)();
    typedef map<FBPostEnum, FnPtr> PostFnMap;
    typedef map<LevelTag, FBPostEnum> LevelTagToPostMap;
    
    LevelTagToPostMap   _levelTagsToPostMap;
    PostFnMap           _postFnMap;
    
	static FBPoster *_sInstance;
};
//---------------------------------------------------------------------------------
#endif
