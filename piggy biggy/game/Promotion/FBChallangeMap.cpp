#include "FBChallangeMap.h"
#include "Debug/MyDebug.h"
//------------------------------------------------------------------
FBChallangeMapRecord::FBChallangeMapRecord()
{
    _opponentSeconds = 0;
    _mySeconds = 0;
    _levelEnum = (LevelEnum)0;
    _challangeStatus = eFBChallangeStatus_Unknow;
    _notifySet = 0;
}
//------------------------------------------------------------------
FBChallangeMapRecord::FBChallangeMapRecord(
                         const char *opponentFbId,
                         unsigned int opponentSeconds,
                         unsigned int mySeconds,
                         LevelEnum level,
                         FBChallangeStatus challangeStatus )
{
    _opponentFbId.append( opponentFbId );
    _opponentSeconds = opponentSeconds;
    _mySeconds = mySeconds;
    _levelEnum = level;
    _challangeStatus = challangeStatus;
    _notifySet = 0;
}
//------------------------------------------------------------------
void FBChallangeMapRecord::Dump()
{
    D_LOG("opponent:%s oTime:%d myTime:%d levelEnum:%d status:%d notify:%d",
          _opponentFbId.c_str(),
          _opponentSeconds,
          _mySeconds,
          _levelEnum,
          _challangeStatus,
          _notifySet );
}



//------------------------------------------------------------------
// FB Challange Map
//------------------------------------------------------------------
void FBChallangeMap::Dump()
{
    for ( iterator it = begin(); it != end(); it++ )
        it->Dump();
}
//------------------------------------------------------------------
FBChallangeMap FBChallangeMap::GetUserChallangeMap( const char *fbId )
{
    FBChallangeMap ret;
    for ( iterator it = begin(); it != end(); it++ )
    {
        if ( !strncmp( fbId, it->_opponentFbId.c_str(), strlen(fbId )))
        {
            ret.push_back( *it );
        }
    }
    return ret;
}
//------------------------------------------------------------------
FBChallangeTotalsMap FBChallangeMap::GetTotalsMap()
{
    FBChallangeTotalsMap totalsMap;
    
    for ( iterator it = begin(); it != end(); it++ )
    {
        switch ( it->_challangeStatus )
        {
            case eFBChallangeStatus_InProgress :
                totalsMap.IncInProgress( it->_opponentFbId.c_str() );
                break;
                
            case eFBChallangeStatus_Lost :
                totalsMap.IncLoses( it->_opponentFbId.c_str() );
                break;
                
            case eFBChallangeStatus_Won :
                totalsMap.IncWins( it->_opponentFbId.c_str() );
                break;
                
            case eFBChallangeStatus_Equal :
                totalsMap.IncEquals( it->_opponentFbId.c_str() );
                break;
                
            default :
                break;
        }

        totalsMap.SetNotify( it->_opponentFbId.c_str(), it->_notifySet );
    }
    
    return totalsMap;    
}



//------------------------------------------------------------------------------------
// User Challange Totals Record
//------------------------------------------------------------------------------------
FBChallangeTotalsRecord::FBChallangeTotalsRecord()
{
    _fbId = "";
    _wins = 0;
    _loses = 0;
    _equals = 0;
    _inProgress = 0;
}
//------------------------------------------------------------------------------------
FBChallangeTotalsRecord::FBChallangeTotalsRecord( const char *fbId )
{
    _fbId = fbId;
    _wins = 0;
    _loses = 0;
    _equals = 0;
    _inProgress = 0;
    _notify = 0;
}
//------------------------------------------------------------------------------------
void FBChallangeTotalsRecord::Dump()
{
    D_LOG("in progress:%i\twins:%i\tloses:%d\tequals:%d\tnotify:%d", _inProgress, _wins, _loses, _equals, _notify );
}
//------------------------------------------------------------------------------------
void FBChallangeTotalsRecord::IncWins()
{
    _wins++;
}
//------------------------------------------------------------------------------------
void FBChallangeTotalsRecord::IncLoses()
{
    _loses++;
}
//------------------------------------------------------------------------------------
void FBChallangeTotalsRecord::IncEquals()
{
    _equals++;
}
//------------------------------------------------------------------------------------
void FBChallangeTotalsRecord::IncInProgress()
{
    _inProgress++;
}
//------------------------------------------------------------------------------------
void FBChallangeTotalsRecord::SetNotify( unsigned int notify )
{
    if ( notify )
        _notify = 1;
}



//------------------------------------------------------------------------------------
// User Challange Totals Map
//------------------------------------------------------------------------------------
void FBChallangeTotalsMap::Dump()
{
#ifdef DEBUG
    for ( iterator it = begin(); it != end(); it++ )
    {
        D_LOG("fbId:%s", it->first.c_str() );
        it->second.Dump();
    }
#endif
}
//------------------------------------------------------------------------------------
void FBChallangeTotalsMap::IncWins( const char* fbUserId )
{
    InitRecord( fbUserId );
    operator[]( fbUserId ).IncWins();
}
//------------------------------------------------------------------------------------
void FBChallangeTotalsMap::IncLoses( const char *fbUserId )
{
    InitRecord( fbUserId );
    operator[]( fbUserId ).IncLoses();
}
//------------------------------------------------------------------------------------
void FBChallangeTotalsMap::IncEquals( const char *fbUserId )
{
    InitRecord( fbUserId );
    operator[]( fbUserId ).IncEquals();
}
//------------------------------------------------------------------------------------
void FBChallangeTotalsMap::IncInProgress( const char *fbUserId )
{
    InitRecord( fbUserId );
    operator[]( fbUserId ).IncInProgress();
}
//------------------------------------------------------------------------------------
void FBChallangeTotalsMap::InitRecord( const char *fbUserId )
{
    iterator  ret;
    FBChallangeTotalsRecord record(fbUserId);
    
    ret = find( fbUserId );
    if ( ret != end() )
        return;
    
    
    operator[]( fbUserId ) =  FBChallangeTotalsRecord(fbUserId);
}
//------------------------------------------------------------------------------------
void FBChallangeTotalsMap::SetNotify( const char *fbUserId, unsigned int notify )
{
    operator[]( fbUserId ).SetNotify( notify );
}
