#ifndef piggy_biggy_FBChallangeMap_h
#define piggy_biggy_FBChallangeMap_h
//------------------------------------------------------------------------------------
#include <map>
#include <string>
#include <vector>
#include "GameTypes.h"
//------------------------------------------------------------------------------------
using namespace std;
using namespace GameTypes;
//------------------------------------------------------------------------------------
// Challanage Map Record
//------------------------------------------------------------------------------------
class FBChallangeMapRecord
{
public:
    FBChallangeMapRecord();
    FBChallangeMapRecord(
                         const char *opponentFbId,
                         unsigned int opponentSeconds,
                         unsigned int mySeconds,
                         LevelEnum level,
                         FBChallangeStatus challangeStatus );
    void Dump();
    
public:
    string              _opponentFbId;
    unsigned int        _opponentSeconds;
    unsigned int        _mySeconds;
    LevelEnum           _levelEnum;
    FBChallangeStatus   _challangeStatus;
    unsigned int        _notifySet;
};



//------------------------------------------------------------------------------------
// User Challange Totals Record
//------------------------------------------------------------------------------------
class FBChallangeTotalsRecord
{
public:
    FBChallangeTotalsRecord();
    FBChallangeTotalsRecord( const char *fbId );
    void Dump();

public:
    void IncWins();
    void IncLoses();
    void IncEquals();
    void IncInProgress();
    void SetNotify( unsigned int notify );
    
    string       _fbId;
    unsigned int _notify;
    unsigned int _wins;
    unsigned int _loses;
    unsigned int _equals;
    unsigned int _inProgress;
};



//------------------------------------------------------------------------------------
// User Challange Totals Map
//------------------------------------------------------------------------------------
class FBChallangeTotalsMap : public map<string /* fb user id */, FBChallangeTotalsRecord>
{
public:
    void Dump();
    
public:
    void IncWins( const char *fbUserId );
    void IncLoses( const char *fbUserId );
    void IncEquals( const char *fbUserId );
    void IncInProgress( const char *fbUserId );
    void SetNotify( const char *fbUserId, unsigned int notify );
    
private:
    void InitRecord( const char *fbUserId );
};



//------------------------------------------------------------------------------------
// Challange Map
//------------------------------------------------------------------------------------
class FBChallangeMap : public vector<FBChallangeMapRecord>
{
public:
    void Dump();
    FBChallangeMap GetUserChallangeMap( const char *fbId );
    FBChallangeTotalsMap GetTotalsMap();
};

#endif
