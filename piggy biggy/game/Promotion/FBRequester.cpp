#include "FBRequester.h"
#include "Debug/MyDebug.h"
#include "GameConfig.h"
#include "Facebook/FBTool.h"
#include "Promotion/FBRequestsMap.h"
#include "FBRequesterNode.h"
#include "FBNewRequestNotify.h"
#include <sys/time.h>
#include <sstream>
//---------------------------------------------------------------------------------
FBRequester* FBRequester::_sInstance = NULL;
//---------------------------------------------------------------------------------
FBRequester* FBRequester::Get()
{
    if ( ! _sInstance )
    {
        _sInstance = new FBRequester();
    }
    return _sInstance;
}
//---------------------------------------------------------------------------------
void FBRequester::Destroy()
{
    if ( _sInstance )
    {
        delete _sInstance;
        _sInstance = NULL;
    }
}
//---------------------------------------------------------------------------------
FBRequester::FBRequester()
{
    gettimeofday( &_lastStep, NULL );
    _processing = false;
}
//---------------------------------------------------------------------------------
FBRequester::~FBRequester()
{
}
//---------------------------------------------------------------------------------
void FBRequester::Step()
{
    return;
    
    if ( _processing || ! FBTool::Get()->IsLogged() )
        return;
    
    struct timeval now;
    gettimeofday( &now, NULL );

    if ( now.tv_sec - _lastStep.tv_sec < Config::FBRequesterCheckEverySec )
        return;

    stringstream url;
    
    url << Config::FBRequestsURL << "checkRequestNotify.php?";
    url << "fbId=" << FBTool::Get()->GetFBUserId();
    
    WWWTool::Get()->HttpGet( url.str().c_str(), this );
    
    _processing = true;
}
//---------------------------------------------------------------------------------
void FBRequester::Http_FailedWithError( void *connection, int error )
{
    
}
//---------------------------------------------------------------------------------
void FBRequester::Http_FinishedLoading( void *connection, const void *data, int len )
{
    //-------------------------------------
    // is there new notification to process?
    if ( strncmp( "1", (const char*) data , 1 ))
    {
        Restart();
        return;
    }
    
    FBTool::Get()->Request_ReadAll( this );
}
//---------------------------------------------------------------------------------
void FBRequester::FBRequestCompleted( FBTool::Request requestType, void *data )
{
    FBNewRequestNotify::Create( (FBRequestsMap*)data );
    // shoulb restarted but not too soon
}
//---------------------------------------------------------------------------------
void FBRequester::FBRequestError( int error )
{
    Restart();
}
//---------------------------------------------------------------------------------
void FBRequester::CheckForNewRequests()
{
}
//---------------------------------------------------------------------------------
void FBRequester::Restart()
{
    _processing = false;
    gettimeofday( &_lastStep, NULL );    
}
//---------------------------------------------------------------------------------