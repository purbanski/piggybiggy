#include "FBRequestsStatsNode.h"
#include "FBRequestMenu.h"
#include "Tools.h"
#include "Platform/Lang.h"
#include <sstream>
#include "Debug/MyDebug.h"
#include "Preloader.h"
#include "Game.h"
#include "Platform/ScoreTool.h"
#include "RunLogger/RunLogger.h"

//------------------------------------------------------------------------------
// FB Request Menu Stats Node
//------------------------------------------------------------------------------
FBRequestMenuStatsNode* FBRequestMenuStatsNode::Create( FBRequestMenu *parent, FBChallangeMap challanges, const char *fbId )
{
    FBRequestMenuStatsNode* node;
    node = new FBRequestMenuStatsNode( parent, fbId );
    
    if ( node )
    {
        node->autorelease();
        node->Init( challanges );
        return  node;
    }
    return NULL;
}
//------------------------------------------------------------------------------
FBRequestMenuStatsNode::FBRequestMenuStatsNode( FBRequestMenu *parent, const char *fbId )
{
    _parent = parent;
    _loading = NULL;
    _fbIdOpponent = fbId;
}
//------------------------------------------------------------------------------
FBRequestMenuStatsNode::~FBRequestMenuStatsNode()
{
}
//------------------------------------------------------------------------------
void FBRequestMenuStatsNode::Init( FBChallangeMap challanges )
{
    _challangeMap = challanges;
    _slidingNode = SlidingNode2::node( Config::GameSize, Config::eMousePriority_FBMenuLayer );
    _slidingNode->setIsTouchEnabled( true );
    _slidingNode->setPosition( CCPoint( -90.0f, 0.0f ));
    addChild( _slidingNode, 10 );
    
    
    // --- this is probably not needed
    GameStatus *status;
    status = Game::Get()->GetGameStatus();
    if ( ! status->GetMsgPlayed( eOTMsgRequest_ChallangeStats ))
    {
        status->SetMsgPlayed( eOTMsgRequest_ChallangeStats );
        _parent->ShowHelpMsg( LocalString("ViewChallangeStatsMsg"));
    }

    AddPiggySprite();
    Setup1();
}
//------------------------------------------------------------------------------
void FBRequestMenuStatsNode::AddPiggySprite()
{
    RLOG_SS("FACEBOOK_STATS", "USER_CHALLENGES_COUNT", _challangeMap.size() );
    
    int finalOpacity;
    if ( ! _challangeMap.size() )
    {
        finalOpacity = 255;
    }
    else
    {
        finalOpacity = Config::FBRequestPiggyBgOpacity;
    }
    
    FadeInSprite *sprite;
    sprite = FadeInSprite::Create( 1.0f, "Images/Facebook/Requests/noStatsPiggy.png", 0.0f, finalOpacity );
    sprite->setPosition( CCPoint( Tools::GetScreenMiddleX() + 160.0f, Tools::GetScreenMiddleY() - 135.0f ));
        
    addChild( sprite, -10 );
}
//------------------------------------------------------------------------
void FBRequestMenuStatsNode::CleanUp()
{
    _slidingNode->onExit();
    _parent->CleanUp();
}
//------------------------------------------------------------------------
FBChallangeStatus FBRequestMenuStatsNode::GetChallangeStatus( const char* status )
{
    FBChallangeStatus ret;
    
    if ( !strncmp("in-progress", status, strlen("in-progress")))
    {
        ret = eFBChallangeStatus_InProgress;
    }
    else if ( !strncmp("won", status, strlen("won")))
    {
        ret = eFBChallangeStatus_Won;
    }
    else if ( !strncmp("lost", status, strlen("lost")))
    {
        ret = eFBChallangeStatus_Lost;
    }
    else if ( !strncmp("equal", status, strlen("equal")))
    {
        ret = eFBChallangeStatus_Equal;
    }
    else
        ret = eFBChallangeStatus_Unknow;
    
    return ret;
}
//------------------------------------------------------------------------------
void FBRequestMenuStatsNode::Setup1()
{
    //-------------------
    // Sliding menu setup
    CCNode *node;

	_menuView = SlidingNodeMenu::menuWithItems( Config::eMousePriority_FBMenuLayerVIP, _slidingNode, NULL, NULL );
    _slidingNode->GetContentNode()->addChild( _menuView, 10 );
    
    for ( FBChallangeMap::iterator it = _challangeMap.begin(); it != _challangeMap.end(); it++ )
    {
        node = FBRequestMenuStatsRecordNode::Create( *it );
        node->setPosition( CCPointZero );
        _slidingNode->AddToContent( node );
        
        _menuView->addChild( GetMenuItemView( *it ), 10 );
    }
    
    
    float padding;
    float deltaY;
    
    deltaY = 260.0f;
    padding = 200.0f;

    CCPoint startingPos;
    startingPos = CCPoint( 0.0f, -15.0f );
   
    _slidingNode->SetClickableArea( CCPoint( 0.0f, 0.0f ), CCPoint( 960.0f, 640.0f));
    _slidingNode->GetContentNode()->AlignVertical( padding );
    _slidingNode->GetContentNode()->setPosition( CCPoint( Tools::GetScreenMiddleX() - deltaY, Tools::GetScreenMiddleY() + 60.0f ));
    _slidingNode->GetContentNode()->setContentSize( CCSize( 100.0f, ( _challangeMap.size() + 1 ) * padding ));
    _slidingNode->ShowItem( 1 );
	_slidingNode->SetInitPosition( startingPos );
    _slidingNode->SetUpperLimitDelta( 15.0f );
    
    _menuView->alignItemsVerticallyWithPadding( padding - 120.0f );

    CCPoint menuPos;
    menuPos.x = FBRequestMenuStatsRecordNode::_sWidth / 2.0f;
    menuPos.y =  - padding * (_challangeMap.size() ) / 2.0f + 85.0f;

    _menuView->setPosition( menuPos );
    _menuView->setOpacity( 0 );

     runAction( CCSequence::actions(
                                    CCDelayTime::actionWithDuration( 1.7f ),
                                    CCCallFunc::actionWithTarget(this, callfunc_selector(FBRequestMenuStatsNode::Setup2 )),
                                    CCDelayTime::actionWithDuration( 0.7f ),
                                    CCCallFunc::actionWithTarget(this, callfunc_selector(FBRequestMenuStatsNode::Setup3 )),
                                    CCDelayTime::actionWithDuration( 1.5f ),
                                    CCCallFunc::actionWithTarget(this, callfunc_selector(FBRequestMenuStatsNode::DeleteChallangeNotify )),
                                    NULL));
}
//------------------------------------------------------------------------------
void FBRequestMenuStatsNode::Setup2()
{
    _menuView->runAction( CCFadeTo::actionWithDuration( 0.5f, 255 ));
    if ( _loading )
    {
        _loading->Stop();
        _loading = NULL;
    }
}
//------------------------------------------------------------------------------
void FBRequestMenuStatsNode::Setup3()
{
    NodeAlign *notificationNode;
    notificationNode = NodeAlign::Create();
    
    _slidingNode->GetContentNode()->addChild( notificationNode, 15 );
    
    for ( FBChallangeMap::iterator it = _challangeMap.begin(); it != _challangeMap.end(); it++ )
    {
        if ( it->_notifySet )
            notificationNode->AddNode( NotificationSprite::Create() );
        else
            notificationNode->AddNode( CCNode::node() );
    }
    
    
    float padding;
    float deltaY;
    
    deltaY = 260.0f;
    padding = 200.0f;

    notificationNode->setPosition( CCPoint( 400.0f, 22.5f ));
    notificationNode->AlignVertical( padding );   
}
//------------------------------------------------------------------------------
void FBRequestMenuStatsNode::DeleteChallangeNotify()
{
    stringstream url;
    url << Config::FBRequestsURL << "challangeDeleteNotify.php?";
    url << "fbId=" << FBTool::Get()->GetFBUserId() << "&";
    url << "fbIdOpponent=" << _fbIdOpponent;
    
    WWWTool::Get()->HttpGet( url.str().c_str() );
}
//------------------------------------------------------------------------------
void FBRequestMenuStatsNode::Menu_View( CCObject* sender )
{
    CCMenuItem *item;
    FBChallangeMapRecord *record;

    item = (CCMenuItem*)sender;
    record = (FBChallangeMapRecord*) item->getUserData();

    RLOG_SS("FACEBOOK_STATS", "VIEW_LEVEL", record->_levelEnum );
    _parent->ShowLevel( record->_levelEnum );
}
//------------------------------------------------------------------------------
void FBRequestMenuStatsNode::Menu_Play( CCObject* sender )
{
    CCMenuItem *item;
    FBChallangeMapRecord *record;

    item = (CCMenuItem*)sender;
    record = (FBChallangeMapRecord*) item->getUserData();

    RLOG_SS("FACEBOOK_STATS", "PLAY_LEVEL", record->_levelEnum );
    
    //---------------------
    // execute request
   	Preloader::Get()->Reset();
	Preloader::Get()->FreeResources();

    Game::Get()->GetGameStatus()->ResetPreviouslyPlayedLevel();
    Game::Get()->CreateAndPlayLevel( record->_levelEnum, Game::eLevelToLevel );

    CleanUp();
}
//------------------------------------------------------------------------------
CCMenuItem* FBRequestMenuStatsNode::GetMenuItemView(const FBChallangeMapRecord &record )
{
    TrippleSprite triSprite;
    FXMenuItemSpriteWithAnim *item;
    
    string file;
    file.append("Images/Facebook/Requests/Stats/Challange");

    if (
        record._challangeStatus == GameTypes::eFBChallangeStatus_InProgress &&
        Game::Get()->GetGameStatus()->GetLevelStatus( record._levelEnum ) != GameStatus::eLevelStatus_Done
        )
    {
        file.append("Play.png");
        triSprite = TrippleSprite(file.c_str());
        item = FXMenuItemSpriteWithAnim::Create( &triSprite, this, menu_selector( FBRequestMenuStatsNode::Menu_Play ));
    }
    else
    {
        file.append("View.png");
        triSprite = TrippleSprite(file.c_str());
        item = FXMenuItemSpriteWithAnim::Create( &triSprite, this, menu_selector( FBRequestMenuStatsNode::Menu_View ));
    }
    
    item->setUserData( (void*) &record );
    return item;
}
//------------------------------------------------------------------------------
void FBRequestMenuStatsNode::Exiting()
{
//    if ( _loading )
//        _loading->stopAllActions();
//    
//    if ( _state == eState_Processing )
//        WWWTool::Get()->CancelHttpGet( this );
//    
//    _state = eState_Exiting;
    
    stopAllActions();
}
//------------------------------------------------------------------------------



//------------------------------------------------------------------------------
// FB Request Menu Stats Record Node
//------------------------------------------------------------------------------
float FBRequestMenuStatsRecordNode::_sWidth = 700.0f;
FBRequestMenuStatsRecordNode* FBRequestMenuStatsRecordNode::Create( const FBChallangeMapRecord &record )
{
    FBRequestMenuStatsRecordNode* node = new FBRequestMenuStatsRecordNode( record );
    if ( node )
    {
        node->autorelease();
        node->Init();
        return node;
    }
    return NULL;
}
//------------------------------------------------------------------------------
FBRequestMenuStatsRecordNode::FBRequestMenuStatsRecordNode( const FBChallangeMapRecord &record )
{
    _challange = record;
}
//------------------------------------------------------------------------------
FBRequestMenuStatsRecordNode::~FBRequestMenuStatsRecordNode()
{
}
//------------------------------------------------------------------------------
void FBRequestMenuStatsRecordNode::Init()
{
    float breakDur;
    breakDur = 0.25f;
    
    runAction( CCSequence::actions(
                                   CCCallFunc::actionWithTarget(this, callfunc_selector(FBRequestMenuStatsRecordNode::Setup1 )),
                                   CCDelayTime::actionWithDuration( breakDur * 2 ),
                                   CCCallFunc::actionWithTarget(this, callfunc_selector(FBRequestMenuStatsRecordNode::Setup2 )),
                                   CCDelayTime::actionWithDuration( breakDur * 2 ),
                                   CCCallFunc::actionWithTarget(this, callfunc_selector(FBRequestMenuStatsRecordNode::Setup2a )),
                                   CCDelayTime::actionWithDuration( breakDur * 2 ),
                                   CCCallFunc::actionWithTarget(this, callfunc_selector(FBRequestMenuStatsRecordNode::Setup3 )),
                                   CCDelayTime::actionWithDuration( breakDur * 6 ),
                                   CCCallFunc::actionWithTarget(this, callfunc_selector(FBRequestMenuStatsRecordNode::Setup4 )),
                                   CCDelayTime::actionWithDuration( breakDur * 2 ),
                                   CCCallFunc::actionWithTarget(this, callfunc_selector(FBRequestMenuStatsRecordNode::Setup5 )),
                                            NULL));

    
}    
//------------------------------------------------------------------------------
void FBRequestMenuStatsRecordNode::Setup1()
{
    FBUserFirstNameNode *labelOpp;
    FBUserFirstNameNode *labelMy;
    
    labelOpp = FBUserFirstNameNode::CreateCenter( _challange._opponentFbId.c_str() );
    labelMy = FBUserFirstNameNode::CreateCenter( FBTool::Get()->GetFBUserId().c_str() );
    
    labelOpp->setPosition( CCPoint( _sWidth - 10.0f, -65.0f ));
    labelMy->setPosition( CCPoint( 10.0f, -65.0f ));
    
    labelMy->SetColor( Config::FBRequestLabelsColor );
    labelOpp->SetColor( Config::FBRequestLabelsColor );
    
    addChild( labelOpp, 10 );
    addChild( labelMy, 10 );
}
//------------------------------------------------------------------------------
void FBRequestMenuStatsRecordNode::Setup2()
{
    FBProfileBigSprite *profileSpriteMe;
    FBProfileBigSprite *profileSprite;

    profileSprite = FBProfileBigSprite::Create( _challange._opponentFbId.c_str());
    profileSpriteMe = FBProfileBigSprite::Create( FBTool::Get()->GetFBUserId().c_str() );
    
    profileSprite->setPosition( CCPoint( _sWidth - 20.0f, 0.0f ));
    profileSpriteMe->setPosition( CCPoint( 0.0f, 0.0f ));
    
    addChild( profileSprite, 10 );
    addChild( profileSpriteMe, 10 );
}
//------------------------------------------------------------------------------
void FBRequestMenuStatsRecordNode::Setup2a()
{
    CCSprite *mySprite;
    CCSprite *opponentSprite;
   
    switch (_challange._challangeStatus)
    {
        case GameTypes::eFBChallangeStatus_Lost :            
            mySprite        = CCSprite::spriteWithFile("Images/Facebook/Requests/Stats/LostMark.png");
            opponentSprite  = CCSprite::spriteWithFile("Images/Facebook/Requests/Stats/WonMark.png");
            break;

        case GameTypes::eFBChallangeStatus_Won :
            mySprite        = CCSprite::spriteWithFile("Images/Facebook/Requests/Stats/WonMark.png");
            opponentSprite  = CCSprite::spriteWithFile("Images/Facebook/Requests/Stats/LostMark.png");
            break;

        case GameTypes::eFBChallangeStatus_Equal :
            mySprite        = CCSprite::spriteWithFile("Images/Facebook/Requests/Stats/EqualMark.png");
            opponentSprite  = CCSprite::spriteWithFile("Images/Facebook/Requests/Stats/EqualMark.png");
            break;

        case GameTypes::eFBChallangeStatus_InProgress :
            mySprite        = CCSprite::spriteWithFile("Images/Facebook/Requests/Stats/InProgressMark.png");
            opponentSprite  = CCSprite::spriteWithFile("Images/Facebook/Requests/Stats/InProgressMark.png");
            break;
            
        case GameTypes::eFBChallangeStatus_Unknow :
        default:
            mySprite        = CCSprite::spriteWithFile("Images/Other/none.png");
            opponentSprite  = CCSprite::spriteWithFile("Images/Other/none.png");
            break;
    }
    
    opponentSprite->setFlipX( true );
    
    mySprite->setOpacity(0);
    opponentSprite->setOpacity(0);

    addChild( mySprite, 20 );
    addChild( opponentSprite, 20 );

    //----------------------------
    // Setting position
    float x, y;
    float xd;
    
    x = _sWidth / 2.0f;
    y = 65.0f;
    xd = 395.0f;
    
    opponentSprite->setPosition( CCPoint( x + xd, y ));
    mySprite->setPosition( CCPoint( x - xd, y ));
    
    //-------------------------
    // Run action fade to
    mySprite->runAction( CCFadeTo::actionWithDuration( 0.5f, 255 ));
    opponentSprite->runAction( CCFadeTo::actionWithDuration( 0.5f, 255 ));
}
//------------------------------------------------------------------------------
void FBRequestMenuStatsRecordNode::Setup3()
{
    string levelName;
    FadeInLabel *levelNameLabel;
    
    levelName = GameGlobals::Get()->LevelNames()[ _challange._levelEnum];
    std::replace( levelName.begin(), levelName.end(), '\n', ' ' );
    
    levelNameLabel = FadeInLabel::Create( 0.5f, levelName.c_str(), Config::FBHighScoreFont, 34);
    levelNameLabel->setPosition( CCPoint( _sWidth / 2.0f, 70.0f ));
    
    levelNameLabel->SetColor( Config::FBRequestLabelsColor );  
    addChild( levelNameLabel, 10 );
}
//------------------------------------------------------------------------------
void FBRequestMenuStatsRecordNode::Setup4()
{
    stringstream timeOpp;
    stringstream timeMy;
    
    FadeInLabel *timeOppLabel;
    FadeInLabel *timeMyLabel;
    
    if ( _challange._mySeconds == 0 )
        timeMy << "";
    else
        timeMy << Tools::ConvertSecToTime( _challange._mySeconds );

    
    if ( _challange._opponentSeconds == 0 )
        timeOpp << "";
    else
        timeOpp << Tools::ConvertSecToTime( _challange._opponentSeconds );

    
    timeMyLabel = FadeInLabel::Create( 0.5f, timeMy.str().c_str(), Config::FBHighScoreFont, 29 );
    timeOppLabel = FadeInLabel::Create( 0.5f, timeOpp.str().c_str(), Config::FBHighScoreFont, 29 );
    
    timeOppLabel->setPosition( CCPoint( _sWidth - 140.0f, 20.0f ));
    timeMyLabel->setPosition( CCPoint( 140.0f, 20.0f ));
    
    timeMyLabel->SetColor( Config::FBRequestLabelsColor );
    timeOppLabel->SetColor( Config::FBRequestLabelsColor );
    
    addChild( timeMyLabel, 10 );
    addChild( timeOppLabel, 10 );
}
//------------------------------------------------------------------------------
void FBRequestMenuStatsRecordNode::Setup5()
{
    FadeInSprite *coinMy;
    FadeInSprite *coinOpp;
    if ( _challange._mySeconds != 0 )
    {
        coinMy = GetCoinImage( ScoreTool::Get()->GetCoinCount( _challange._mySeconds ));
        coinMy->setPosition( CCPoint( 140.0f, -20.0f ));
        addChild( coinMy, 10 );
    }
    
    if ( _challange._opponentSeconds != 0 )
    {
        coinOpp = GetCoinImage( ScoreTool::Get()->GetCoinCount( _challange._opponentSeconds ));
        coinOpp->setPosition( CCPoint( _sWidth - 140.0f, -20.0f ));
        addChild( coinOpp, 10 );
    }
}
//------------------------------------------------------------------------------
FadeInSprite* FBRequestMenuStatsRecordNode::GetCoinImage( int count )
{
    stringstream file;
    file << "Images/Facebook/Highscore/Coin";
    
    switch ( count )
    {
        case 0 :
        case 1 :
        case 2 :
        case 3 :
        case 4 :
        case 5 :
            file << count << ".png";
            break;

        default:
            file << "0.png";
            break;
    }
    
    FadeInSprite *sprite;
    sprite = FadeInSprite::Create( 0.5, file.str().c_str() );
    return sprite;
}


