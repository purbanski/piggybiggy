#ifndef __piggy_biggy__FBRequester__
#define __piggy_biggy__FBRequester__
//---------------------------------------------------------------------------------
#include <cocos2d.h>
#include "Facebook/FBTool.h"
#include "FBRequestsMap.h"
#include "Platform/WWWTool.h"
//---------------------------------------------------------------------------------
class FBRequester : public FBToolListener, public WWWToolListener
{
public:
    static FBRequester* Get();
    static void Destroy();
    
    ~FBRequester();
    void Step();
    
    virtual void FBRequestCompleted( FBTool::Request requestType, void *data );
    virtual void FBRequestError( int error );
    
    virtual void Http_FailedWithError( void *connection, int error );
    virtual void Http_FinishedLoading( void *connection, const void *data, int len );
    
    void Restart();

private:
    FBRequester();
    void CheckForNewRequests();
    
private:
    static FBRequester* _sInstance;
    timeval         _lastStep;
    bool            _processing;
//    FBRequestsMap   _requestsMap;
//    FBRequestsMap::iterator _requestMapIndex;
    
};
//---------------------------------------------------------------------------------

#endif
