#include "FBNewRequestNotify.h"
#include "GameConfig.h"
#include "Components/TrippleSprite.h"
#include "Components/FXMenuItem.h"
#include "Facebook/FBTool.h"
#include "FBRequester.h"
#include "FBRequestMenu.h"
#include "Tools.h"

//-------------------------------------------------------------------------------------
FBNewRequestNotify* FBNewRequestNotify::Create( FBRequestsMap *map )
{
    FBNewRequestNotify* node;
    node = new FBNewRequestNotify();
    
    if ( node )
    {
        node->autorelease();
        node->Init( map );
        return node;
    }
    
    return NULL;
}
//-------------------------------------------------------------------------------------
FBNewRequestNotify::FBNewRequestNotify()
{
}
//-------------------------------------------------------------------------------------
FBNewRequestNotify::~FBNewRequestNotify()
{
    FBRequester::Get()->Restart();
}
//-------------------------------------------------------------------------------------
void FBNewRequestNotify::Init( FBRequestsMap *map )
{
    TrippleSprite tSprite;
    FXMenuItemSpriteWithAnim *item;
    CCMenu *menu;
    _requestsMap = *map;
    
    tSprite = TrippleSprite( "Images/Facebook/Requests/newMailBt.png" );
    item = FXMenuItemSpriteWithAnim::Create( &tSprite, this, menu_selector( FBNewRequestNotify::Menu_ShowMore ));
    
    CCPoint pos;
    pos = Tools::GetScreenMiddle();
    pos.x -= ( Config::GameSize.width / 2.0f - 90.0f );
    pos.y += ( Config::GameSize.height / 2.0f - 90.0f );
    
    menu = CCMenu::menuWithItem( item );
    menu->setPosition( pos );
    
    addChild( menu );
    setPosition( CCPointZero );
    
    CCDirector::sharedDirector()->getRunningScene()->addChild( this, 255 );
    
    float shortDur;
    float longDur;
    
    shortDur = 0.35f;
    longDur = 1.0f;
    
    menu->setAnchorPoint( CCPointZero );
    
    CCFiniteTimeAction *rSeq = (CCFiniteTimeAction*) CCSequence::actions(
                                                     CCScaleTo::actionWithDuration(shortDur, 1.2f ),
                                                     CCScaleTo::actionWithDuration(shortDur, 1.0f ),
                                                     NULL
                                                     );
    
    CCAction *seq = (CCAction*) CCSequence::actions(
                                                    CCFadeTo::actionWithDuration( longDur, 255),
                                                    CCRepeat::actionWithAction( rSeq, 6 ),
                                                    CCFadeTo::actionWithDuration( longDur, 0),
                                                    CCCallFunc::actionWithTarget( this, callfunc_selector( FBNewRequestNotify::Finish)),
                                                    NULL );
                                                    
    menu->setOpacity(0);
    menu->runAction( seq );
    //)
}
//-------------------------------------------------------------------------------------
void FBNewRequestNotify::Menu_ShowMore( CCObject *sender )
{
    CCNode *requestMenu;
    requestMenu = FBRequestMenu::Create();
    requestMenu->setPosition( CCPoint( Tools::GetScreenMiddleX() - 1300.0f, Tools::GetScreenMiddleY() - 300.0f ));
    
    CCDirector::sharedDirector()->getRunningScene()->addChild( requestMenu, 200 );
}
//-------------------------------------------------------------------------------------
void FBNewRequestNotify::Finish()
{
    FBTool::Get()->Request_DeleteNotify( FBTool::Get()->GetFBUserId().c_str() );
    FBRequester::Get()->Restart();
}
//-------------------------------------------------------------------------------------
