#include "FBRequestStatsTotalsNode.h"
#include "Tools.h"
#include "Platform/Lang.h"
#include <sstream>
#include "Debug/MyDebug.h"
#include "Preloader.h"
#include "Game.h"
#include "Platform/ScoreTool.h"
#include "FBRequestMenu.h"
#include "GameConfig.h"
#include "RunLogger/RunLogger.h"

//------------------------------------------------------------------------------
// FB Request Menu Stats Totals Node
//------------------------------------------------------------------------------
FBRequestMenuStatsTotalsNode* FBRequestMenuStatsTotalsNode::Create( FBRequestMenu *parent, const char *fbIdOpponent )
{
    RLOG_S("FACEBOOK_STATS","VIEW_STATS_TOTAL");
    
    FBRequestMenuStatsTotalsNode* node;
    node = new FBRequestMenuStatsTotalsNode( parent, fbIdOpponent );
    
    if ( node )
    {
        node->autorelease();
        node->Init();
        return  node;
    }
    return NULL;
}
//------------------------------------------------------------------------------
FBRequestMenuStatsTotalsNode::FBRequestMenuStatsTotalsNode( FBRequestMenu *parent, const char *fbIdOpponent )
{
    _parent = parent;
    _state = eState_Idle;
    
    _noInternetAccess = NULL;
    _internetAccess = true;
    
    if ( fbIdOpponent )
        _fbIdOpponent = fbIdOpponent;
}
//------------------------------------------------------------------------------
FBRequestMenuStatsTotalsNode::~FBRequestMenuStatsTotalsNode()
{
    if ( _state == eState_Processing )
        WWWTool::Get()->CancelHttpGet( this );
}
//------------------------------------------------------------------------------
void FBRequestMenuStatsTotalsNode::Init()
{  
    _slidingNode = SlidingNode2::node( Config::GameSize, Config::eMousePriority_FBMenuLayer );
    _slidingNode->setIsTouchEnabled( true );
    _slidingNode->setPosition( CCPoint( -90.0f, 0.0f ));
    addChild( _slidingNode, 10 );

    ChallangeGetMy();
    
    _loading = LoadingAnim::Create();
    _loading->SetPositionForFBRequestMenu();
    addChild( _loading, 50 );
    
    GameStatus *status;
    status = Game::Get()->GetGameStatus();
    if ( ! status->GetMsgPlayed( eOTMsgRequest_ChallangeStats ))
    {
        status->SetMsgPlayed( eOTMsgRequest_ChallangeStats );
        _parent->ShowHelpMsg( LocalString("ViewChallangeStatsMsg"));
    }
}
//------------------------------------------------------------------------------
void FBRequestMenuStatsTotalsNode::ChallangeGetMy()
{
    stringstream iUrl;
    
    iUrl << Config::FBRequestsURL << "challangeGetMy.php?";
    iUrl << "fbId=" << FBTool::Get()->GetFBUserId();
    
    WWWTool::Get()->HttpGet( iUrl.str().c_str(), this );
    _state = eState_Processing;
}
//------------------------------------------------------------------------
void FBRequestMenuStatsTotalsNode::CleanUp()
{
    _slidingNode->onExit();
    _parent->CleanUp();
    
    if ( _state == eState_Processing )
        WWWTool::Get()->CancelHttpGet();
}
//------------------------------------------------------------------------
void FBRequestMenuStatsTotalsNode::ShowNoInternetSprite()
{
    _internetAccess = false;
    
    if ( _noInternetAccess )
        _noInternetAccess->Show();
    
    if ( ! _noInternetAccess )
    {
        _noInternetAccess = FadeInSprite::Create( 0.5, "Images/Facebook/Requests/noInternet.png" );
        _noInternetAccess->setPosition( ccp( Tools::GetScreenMiddleX(), Tools::GetScreenMiddleY() - 64.0f ));
        addChild(_noInternetAccess);
    }
}
//------------------------------------------------------------------------
void FBRequestMenuStatsTotalsNode::Http_FailedWithError( void *connection, int error )
{
    _state = eState_Idle;
    
    if ( _loading )
    {
        _loading->Stop();
        _loading = NULL;
    }
    
    ShowNoInternetSprite();
}
//------------------------------------------------------------------------
void FBRequestMenuStatsTotalsNode::Http_FinishedLoading( void *connection, const void *data, int len )
{
    if ( _state == eState_Exiting )
        return;
        
    string temp;
    string strData;
    
    _challangeMap.clear();
    
    temp.append( (const char*)data );
    strData = string( temp, 0, len );
    
    D_STRING( temp );
    D_STRING( strData );
    
    typedef vector<string> Vec;
    Vec usersChallanges;
    Vec chRecord;
  
//    // example
//    // 522550325,,187771356,65668200,in-progress,1:100005625662657,,513,115,in-progress,0:100005625662657,,599,66,in-progress,1
      
    Tools::StringSplit( strData.c_str(), ':', usersChallanges );
    for ( Vec::iterator challange = usersChallanges.begin(); challange != usersChallanges.end(); challange++ )
    {
        D_LOG("%s-", challange->c_str() );
        
        chRecord.clear();
        Tools::StringSplit( challange->c_str(), ',', chRecord );
        
        D_SIZE( chRecord );
        if ( chRecord.size() != 6 )
            continue;
        
        FBChallangeMapRecord rec;
        int levelEnum;
        int notify;

        rec._opponentFbId = chRecord[0];
        stringstream(chRecord[1]) >> rec._opponentSeconds;
        stringstream(chRecord[2]) >> rec._mySeconds;

        stringstream(chRecord[3]) >> levelEnum;
        rec._levelEnum = (LevelEnum) levelEnum;
        
        rec._challangeStatus = GetChallangeStatus(chRecord[4].c_str());
        
        stringstream(chRecord[5]) >> notify;
        rec._notifySet = notify;
        
        _challangeMap.push_back( rec );
    }

#ifdef DEBUG
    _challangeMap.Dump();
#endif
    
    if ( _challangeMap.size() )
    {
        //------------
        // Show top fade
        FadeInSprite *topFade;
        topFade = FadeInSprite::Create( 0.5f, "Images/Facebook/Requests/Stats/TopFade.png" );
        topFade->setPosition( ccp( Tools::GetScreenMiddleX() , Tools::GetScreenMiddleY() + 150.0f ));
        addChild( topFade, 100 );
    }
    
    
    if ( _challangeMap.size() && _fbIdOpponent.length() )
    {
        runAction( CCSequence::actions(
                                       CCDelayTime::actionWithDuration(0.5f),
                                       CCCallFunc::actionWithTarget( this, callfunc_selector( FBRequestMenuStatsTotalsNode::ShowDefaultStats )),
                                       NULL ));
        return;
    }

    
    int finalOpacity;
    if ( ! _challangeMap.size() )
    {
        finalOpacity = 255;
        _loading->Stop();
        _challangeTotalsMap.clear();
    }
    else
    {
        finalOpacity = Config::FBRequestPiggyBgOpacity;
        _challangeTotalsMap =  _challangeMap.GetTotalsMap();
        _challangeTotalsMap.Dump();
        Setup1();
    }
    
    RLOG_SS("FACEBOOK_STATS", "USERS_COUNT", _challangeTotalsMap.size() );
    
    FadeInSprite *sprite;
    sprite = FadeInSprite::Create( 1.0f, "Images/Facebook/Requests/noChallangesPiggy.png", 0.0f, finalOpacity );
    sprite->setPosition( CCPoint( Tools::GetScreenMiddleX() + 180.0f, Tools::GetScreenMiddleY() - 120.0f ));
        
    addChild( sprite, -10 );
}
//------------------------------------------------------------------------
void FBRequestMenuStatsTotalsNode::ShowStatsForUser( const char *fbIdOpponent )
{
    D_CSTRING(fbIdOpponent);
    _parent->Setup_StatsView( _challangeMap.GetUserChallangeMap( fbIdOpponent ), fbIdOpponent );
}
//------------------------------------------------------------------------
void FBRequestMenuStatsTotalsNode::ShowDefaultStats()
{
    D_STRING(_fbIdOpponent);
    ShowStatsForUser( _fbIdOpponent.c_str() );
}
//------------------------------------------------------------------------
FBChallangeStatus FBRequestMenuStatsTotalsNode::GetChallangeStatus( const char* status )
{
    FBChallangeStatus ret;
    
    if ( !strncmp("in-progress", status, strlen("in-progress")))
    {
        ret = eFBChallangeStatus_InProgress;
    }
    else if ( !strncmp("won", status, strlen("won")))
    {
        ret = eFBChallangeStatus_Won;
    }
    else if ( !strncmp("lost", status, strlen("lost")))
    {
        ret = eFBChallangeStatus_Lost;
    }
    else if ( !strncmp("equal", status, strlen("equal")))
    {
        ret = eFBChallangeStatus_Equal;
    }
    else
        ret = eFBChallangeStatus_Unknow;
    
    return ret;
}
//------------------------------------------------------------------------------
void FBRequestMenuStatsTotalsNode::Setup1()
{
    //-------------------
    // Sliding menu setup
    CCNode *node;
	_menuView = SlidingNodeMenu::menuWithItems( Config::eMousePriority_FBMenuLayerVIP, _slidingNode, NULL, NULL );

    //	menu->setPosition( CCPointZero );
    
    _slidingNode->GetContentNode()->addChild(_menuView, 10);
    
    FadeInSprite *winIcon;
    FadeInSprite *losesIcon;
    FadeInSprite *eualsIcon;
    FadeInSprite *inProgressIcon;
    
    FadeInSprite *separator1;
    FadeInSprite *separator2;
    FadeInSprite *separator3;
    // FadeInSprite *separator4;
    
    
    float xMove;
    float xd;
    float yd;
    
    float xdSep;
    float ydSep;
    
    xMove = FBRequestMenuStatsTotalsRecordNode::_sXMove;
    xMove = 180.0f;
    xd = -195.0f;
    yd = 100.0f;
 
    //---------------------
    // Top icons
    winIcon = FadeInSprite::Create( 0.5f, "Images/Facebook/Requests/Stats/WinsIcon.png" );
    losesIcon = FadeInSprite::Create( 0.5f, "Images/Facebook/Requests/Stats/LosesIcon.png", 0.5f );
    eualsIcon = FadeInSprite::Create( 0.5f, "Images/Facebook/Requests/Stats/EqualIcon.png", 1.0f );
    inProgressIcon = FadeInSprite::Create( 0.5f, "Images/Facebook/Requests/Stats/InProgressIcon.png", 1.5f );
    
    winIcon->setPosition( CCPoint( 20.0f + xd + Tools::GetScreenMiddleX(), Tools::GetScreenMiddleY() + yd ));
    losesIcon->setPosition( CCPoint( xd + Tools::GetScreenMiddleX() + xMove, Tools::GetScreenMiddleY() + yd ));
    eualsIcon->setPosition( CCPoint( xd + Tools::GetScreenMiddleX() + 2.0f * xMove, Tools::GetScreenMiddleY() + yd ));
    inProgressIcon->setPosition( CCPoint( xd + Tools::GetScreenMiddleX() + 3.0f * xMove, Tools::GetScreenMiddleY() + yd ));

    addChild( winIcon, 150 );
    addChild( losesIcon, 150 );
    addChild( eualsIcon, 150 );
    addChild( inProgressIcon, 150 );

    
    //---------------------
    // Separators
    xdSep = 80.0f;
    ydSep = -105.0f;
    
    separator1 = FadeInSprite::Create( 0.5f, "Images/Facebook/Requests/Stats/VerticalSeparator.png" );
    separator2 = FadeInSprite::Create( 0.5f, "Images/Facebook/Requests/Stats/VerticalSeparator.png", 0.5f );
    separator3 = FadeInSprite::Create( 0.5f, "Images/Facebook/Requests/Stats/VerticalSeparator.png", 1.0f );
//    separator4 = FadeInSprite::Create( 0.5f, "Images/Facebook/Requests/Stats/VerticalSeparator.png", 1.5f );
    
    separator1->setPosition( CCPoint( xdSep + 20.0f + xd + Tools::GetScreenMiddleX(), Tools::GetScreenMiddleY() + ydSep ));
    separator2->setPosition( CCPoint( xdSep + xd + Tools::GetScreenMiddleX() + xMove, Tools::GetScreenMiddleY() + ydSep ));
    separator3->setPosition( CCPoint( xdSep + 20.0f + xd + Tools::GetScreenMiddleX() + 2.0f * xMove, Tools::GetScreenMiddleY() + ydSep ));
//    separator4->setPosition( CCPoint( xdSep + xd + Tools::GetScreenMiddleX() + 3.0f * xMove, Tools::GetScreenMiddleY() + ydSep ));

    addChild( separator1, 15 );
    addChild( separator2, 15 );
    addChild( separator3, 15 );
//    addChild( separator4, 15 );
    
    
    //---------------------
    // Notification and Node
    NodeAlign *notificationNode;
    NodeAlign *nameNode;
    
    notificationNode = NodeAlign::Create();
    nameNode = NodeAlign::Create();

    _slidingNode->GetContentNode()->addChild( notificationNode, 200 );
    _slidingNode->GetContentNode()->addChild( nameNode, 200 );
    _slidingNode->SetUpperLimitDelta( 50.0f );
    
    for ( FBChallangeTotalsMap::iterator it = _challangeTotalsMap.begin(); it != _challangeTotalsMap.end(); it++ )
    {
        node = FBRequestMenuStatsTotalsRecordNode::Create( it->second );
        _slidingNode->AddToContent( node );

        if ( it->second._notify )
            notificationNode->AddNode( NotificationSprite::Create() );
        else
            notificationNode->AddNode( CCNode::node() );
        
        nameNode->AddNode( GetUserNameNode( it->second._fbId.c_str() ));
    
        _menuView->addChild( GetMenuItemView( it->second ), 10 );
    }

    
    float padding;
    float deltaY;
    
    deltaY = 60.0f;
    padding = 200.0f;

    CCPoint startingPos;
    startingPos = CCPoint( 0.0f, -15.0f );
   
    _slidingNode->SetClickableArea( CCPoint( 0.0f, 0.0f ), CCPoint( 960.0f, 640.0f));
    _slidingNode->GetContentNode()->AlignVertical( padding );
    _slidingNode->GetContentNode()->setPosition( CCPoint( Tools::GetScreenMiddleX() - deltaY, Tools::GetScreenMiddleY() + 30.0f ));
    _slidingNode->GetContentNode()->setContentSize( CCSize( 100.0f, ( _challangeTotalsMap.size() + 1 ) * padding ));
    _slidingNode->ShowItem( 1 );
	_slidingNode->SetInitPosition( startingPos );
    
    _menuView->alignItemsVerticallyWithPadding( padding - 95.0f );

    notificationNode->AlignVertical( padding );
    nameNode->AlignVertical( padding );
    
    notificationNode->setPosition( -155.0f, 55.0f );
    nameNode->setPosition( -210.0f, -70.0f );
    
    CCPoint menuPos;
    menuPos.x = -220.0f;
    menuPos.y =  - padding * (_challangeTotalsMap.size() ) / 2.0f + 95.0f;

    _menuView->setPosition( menuPos );
    _menuView->setOpacity( 0 );

     runAction( CCSequence::actions(
                                    CCDelayTime::actionWithDuration( 1.7f ),
                                    CCCallFunc::actionWithTarget(this, callfunc_selector(FBRequestMenuStatsTotalsNode::Setup2 )),
                                    NULL));
}
//------------------------------------------------------------------------------
void FBRequestMenuStatsTotalsNode::Setup2()
{
    _menuView->runAction( CCFadeTo::actionWithDuration( 0.5f, 255 ));
    if ( _loading )
    {
        _loading->Stop();
        _loading = NULL;
    }
}
//------------------------------------------------------------------------------
void FBRequestMenuStatsTotalsNode::Setup3()
{
//    _menuRemove->runAction( CCFadeTo::actionWithDuration( 0.5f, 255 ));
}
//------------------------------------------------------------------------------
void FBRequestMenuStatsTotalsNode::Menu_View( CCObject* sender )
{
    FXMenuItemSpriteWithAnim *menuItem;
    FBChallangeTotalsRecord *record;
    
    menuItem = (FXMenuItemSpriteWithAnim*) sender;
    record = (FBChallangeTotalsRecord *) menuItem->getUserData();
    D_CSTRING( record->_fbId.c_str() );
    
    ShowStatsForUser( record->_fbId.c_str() );
}
//------------------------------------------------------------------------------
CCNode* FBRequestMenuStatsTotalsNode::GetUserNameNode( const char* fbId )
{
    FBUserFirstNameNode *nameNode;
    nameNode = FBUserFirstNameNode::CreateCenter( fbId );
    
    nameNode->setPosition( CCPoint( 0.0f, 0.0f ));
    nameNode->SetColor( Config::FBRequestLabelsColor );
    
    return nameNode;
}
//------------------------------------------------------------------------------
CCMenuItem* FBRequestMenuStatsTotalsNode::GetMenuItemView( const FBChallangeTotalsRecord &record )
{
    FXMenuItemSpriteWithAnim *menuItem;
    TrippleSprite sprite;
    
    FBProfileBigSprite *profileSprite;
    CCSprite *selectedSprite;
    
    profileSprite = FBProfileBigSprite::Create( record._fbId.c_str(), "Images/Facebook/Frames/frameButton.png" );
    selectedSprite = CCSprite::spriteWithFile("Images/Facebook/Requests/userClick.png");
    
    sprite._normal = profileSprite;
    sprite._selected = selectedSprite;
    sprite._disable = CCSprite::spriteWithFile( "Images/Other/none.png" );
    
    menuItem = FXMenuItemSpriteWithAnim::Create( &sprite, this, menu_selector( FBRequestMenuStatsTotalsNode::Menu_View ));
    menuItem->setUserData( (void *) &record );

    return menuItem;
}
//------------------------------------------------------------------------------
void FBRequestMenuStatsTotalsNode::Exiting()
{
    if ( _loading )
        _loading->stopAllActions();
    
    if ( _state == eState_Processing )
        WWWTool::Get()->CancelHttpGet( this );
    
    _state = eState_Exiting;
    
    stopAllActions();
}
//------------------------------------------------------------------------------



//------------------------------------------------------------------------------
// FB Request Menu Mailbox Node
//------------------------------------------------------------------------------
float FBRequestMenuStatsTotalsRecordNode::_sXMove = 180.0f;
float FBRequestMenuStatsTotalsRecordNode::_sXStart = -40.0f;

FBRequestMenuStatsTotalsRecordNode* FBRequestMenuStatsTotalsRecordNode::Create( const FBChallangeTotalsRecord &record )
{
    FBRequestMenuStatsTotalsRecordNode* node = new FBRequestMenuStatsTotalsRecordNode( record );
    if ( node )
    {
        node->autorelease();
        node->Init();
        return node;
    }
    return NULL;
}
//------------------------------------------------------------------------------
FBRequestMenuStatsTotalsRecordNode::FBRequestMenuStatsTotalsRecordNode( const FBChallangeTotalsRecord &record )
{
    _challangesTotal = record;
}
//------------------------------------------------------------------------------
FBRequestMenuStatsTotalsRecordNode::~FBRequestMenuStatsTotalsRecordNode()
{
}
//------------------------------------------------------------------------------
void FBRequestMenuStatsTotalsRecordNode::Init()
{
    float breakDur;    
    breakDur = 0.25f;
    
    runAction( CCSequence::actions(
                                   CCCallFunc::actionWithTarget(this, callfunc_selector(FBRequestMenuStatsTotalsRecordNode::Setup1 )),
                                   CCDelayTime::actionWithDuration( breakDur * 2 ),
                                   CCCallFunc::actionWithTarget(this, callfunc_selector(FBRequestMenuStatsTotalsRecordNode::Setup2 )),
                                   CCDelayTime::actionWithDuration( breakDur * 2 ),
                                   CCCallFunc::actionWithTarget(this, callfunc_selector(FBRequestMenuStatsTotalsRecordNode::Setup3 )),
                                   CCDelayTime::actionWithDuration( breakDur * 2 ),
                                   CCCallFunc::actionWithTarget(this, callfunc_selector(FBRequestMenuStatsTotalsRecordNode::Setup4 )),
                                   NULL));    
}    
//------------------------------------------------------------------------------
void FBRequestMenuStatsTotalsRecordNode::Setup2()
{
    stringstream value;
    FadeInLabelBMP *label;
    
    value << _challangesTotal._loses;
    label = FadeInLabelBMP::Create( 0.5f, value.str().c_str(), Config::FBHighScorePositionFont );
    label->setPosition( CCPoint( _sXStart + _sXMove, 0.0f ));
    label->SetColor( Config::FBRequestLabelsColor );

    addChild( label, 10 );
}
//------------------------------------------------------------------------------
void FBRequestMenuStatsTotalsRecordNode::Setup1()
{
    stringstream wins;
    FadeInLabelBMP *labelWins;
    
    wins << _challangesTotal._wins;
    labelWins = FadeInLabelBMP::Create( 0.5f, wins.str().c_str(), Config::FBHighScorePositionFont );
    labelWins->setPosition( CCPoint( _sXStart, 0.0f ));
    labelWins->SetColor( Config::FBRequestLabelsColor );

    addChild( labelWins, 10 );
}
//------------------------------------------------------------------------------
void FBRequestMenuStatsTotalsRecordNode::Setup4()
{
    stringstream value;
    FadeInLabelBMP *label;
    
    value << _challangesTotal._inProgress;
    label = FadeInLabelBMP::Create( 0.5f, value.str().c_str(), Config::FBHighScorePositionFont );
    label->setPosition( CCPoint( _sXStart + 3.0f * _sXMove, 0.0f ));
    label->SetColor( Config::FBRequestLabelsColor );

    addChild( label, 10 );
}
//------------------------------------------------------------------------------
void FBRequestMenuStatsTotalsRecordNode::Setup3()
{
    stringstream value;
    FadeInLabelBMP *label;
    
    value << _challangesTotal._equals;
    label = FadeInLabelBMP::Create( 0.5f, value.str().c_str(), Config::FBHighScorePositionFont );
    label->setPosition( CCPoint( _sXStart + 2.0f * _sXMove, 0.0f ));
    label->SetColor( Config::FBRequestLabelsColor );

    addChild( label, 10 );
}
//------------------------------------------------------------------------------
