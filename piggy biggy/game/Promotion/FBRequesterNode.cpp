#include "FBRequesterNode.h"
#include "GameConfig.h"
//-------------------------------------------------------------------------------------
FBRequesterNode* FBRequesterNode::Create( FBRequestsMap *map )
{
    FBRequesterNode* node;
    node = new FBRequesterNode();
    
    if ( node )
    {
        node->autorelease();
        node->Init( map );
        return node;
    }
    
    return NULL;
}
//-------------------------------------------------------------------------------------
FBRequesterNode::FBRequesterNode()
{
}
//-------------------------------------------------------------------------------------
FBRequesterNode::~FBRequesterNode()
{
}
//-------------------------------------------------------------------------------------
void FBRequesterNode::Init( FBRequestsMap *map )
{
    _requestsMap = *map;
    
    _contentNode = NodeAlign::Create();
    
    for ( FBRequestsMap::iterator it = _requestsMap.begin(); it != _requestsMap.end(); it++ )
    {
        _contentNode->AddNode( GetNodeItem( &it ) );
    }
    _contentNode->AlignVertical( 130.0f );
    _contentNode->setPosition(CCPoint( 0.0f, 0.0f ));
    
    addChild( _contentNode );
    setPosition( CCPointZero );
    
    CCDirector::sharedDirector()->getRunningScene()->addChild( this, 255 );
}
//-------------------------------------------------------------------------------------
CCNode* FBRequesterNode::GetNodeItem(FBRequestsMap::iterator* it )
{
    CCLabelTTF *fromLabel;
    CCLabelTTF *msgLabel;
    CCNode *ret;
    
    fromLabel = CCLabelTTF::labelWithString(
                                            (*it)->second._fromUsername.c_str(),
                                            Config::FBHighScoreFont,
                                            28 );
 
    msgLabel = CCLabelTTF::labelWithString(
                                           (*it)->second._message.c_str(),
                                           Config::FBHighScoreFont,
                                           28 );
    
    fromLabel->setPosition( CCPoint( 0.0f, 0.0f ));
    msgLabel->setPosition( CCPoint( 0.0f, -40.0f ));
    
    ret = CCNode::node();
    ret->addChild( fromLabel, 10 );
    ret->addChild( msgLabel, 10 );
    
    return ret;
}