#ifndef __FBPOSTS_H__
#define __FBPOSTS_H__

#include <map>
#include <string>
//------------------------------------------------------------------------------------
using namespace std;
//------------------------------------------------------------------------------------
typedef map<string,string> FBPostMap;

//------------------------------------------------------------------------------------
class FBPost
{
public:
    FBPost();
    FBPost( const char *link, const char *imageURL, const char *name, const char *caption, const char *description );
    ~FBPost();
    
    string GetLink();
    string GetImageURL();
    string GetName();
    string GetCaption();
    string GetDescription();

protected:
    FBPostMap _postMap;
};

//------------------------------------------------------------------------------------
// Level Completed
//------------------------------------------------------------------------------------
class FBPost_LevelCompleted : public FBPost
{
public:
    FBPost_LevelCompleted();
};

//------------------------------------------------------------------------------------
// Level Car Completed
//------------------------------------------------------------------------------------
class FBPost_LevelCarCompleted : public FBPost
{
public:
    FBPost_LevelCarCompleted();
};

//------------------------------------------------------------------------------------
// Level Hacker Completed
//------------------------------------------------------------------------------------
class FBPost_LevelHackerCompleted : public FBPost
{
public:
    FBPost_LevelHackerCompleted();
};

//------------------------------------------------------------------------------------
// Level Bank Completed
//------------------------------------------------------------------------------------
class FBPost_LevelBankCompleted : public FBPost
{
public:
    FBPost_LevelBankCompleted();
};

//------------------------------------------------------------------------------------
// Level Spitter Completed
//------------------------------------------------------------------------------------
class FBPost_LevelSpitterCompleted : public FBPost
{
public:
    FBPost_LevelSpitterCompleted();
};

//------------------------------------------------------------------------------------
// Level Bombs Completed
//------------------------------------------------------------------------------------
class FBPost_LevelBombsCompleted : public FBPost
{
public:
    FBPost_LevelBombsCompleted();
};

//------------------------------------------------------------------------------------
// Level Reel Completed
//------------------------------------------------------------------------------------
class FBPost_LevelReelCompleted : public FBPost
{
public:
    FBPost_LevelReelCompleted();
};

//------------------------------------------------------------------------------------
// Level Balancer Completed
//------------------------------------------------------------------------------------
class FBPost_LevelBalancerCompleted : public FBPost
{
public:
    FBPost_LevelBalancerCompleted();
};

//------------------------------------------------------------------------------------
// Level Flies Completed
//------------------------------------------------------------------------------------
class FBPost_LevelFliesCompleted : public FBPost
{
public:
    FBPost_LevelFliesCompleted();
};


//------------------------------------------------------------------------------------
// Level Blocks/tetris Completed
//------------------------------------------------------------------------------------
class FBPost_LevelTetrisCompleted : public FBPost
{
public:
    FBPost_LevelTetrisCompleted();
};

//------------------------------------------------------------------------------------
// Level Cop Completed
//------------------------------------------------------------------------------------
class FBPost_LevelCopCompleted : public FBPost
{
public:
    FBPost_LevelCopCompleted();
};

//------------------------------------------------------------------------------------
// Level Frigle Completed
//------------------------------------------------------------------------------------
class FBPost_LevelFrigleCompleted : public FBPost
{
public:
    FBPost_LevelFrigleCompleted();
};

//------------------------------------------------------------------------------------
// Level Thief Completed
//------------------------------------------------------------------------------------
class FBPost_LevelThiefCompleted : public FBPost
{
public:
    FBPost_LevelThiefCompleted();
};

//------------------------------------------------------------------------------------
// Level Boxing Completed
//------------------------------------------------------------------------------------
class FBPost_LevelBoxingCompleted : public FBPost
{
public:
    FBPost_LevelBoxingCompleted();
};

//------------------------------------------------------------------------------------
// Level Magnet Bomb Completed
//------------------------------------------------------------------------------------
class FBPost_LevelMagnetBombCompleted : public FBPost
{
public:
    FBPost_LevelMagnetBombCompleted();
};


//------------------------------------------------------------------------------------
// Level Button Completed
//------------------------------------------------------------------------------------
class FBPost_LevelButtonCompleted : public FBPost
{
public:
    FBPost_LevelButtonCompleted();
};




#endif


