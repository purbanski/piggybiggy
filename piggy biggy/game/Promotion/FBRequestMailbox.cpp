#include "FBRequestMailbox.h"
#include "FBRequestMenu.h"
#include "FBRequestsStatsNode.h"
#include "FBRequester.h"
#include "Platform/WWWTool.h"
#include "Platform/Lang.h"
#include "Platform/ScoreTool.h"
#include "Components/CustomSprites.h"
#include "Components/CustomNodes.h"
#include "Components/TrippleSprite.h"
#include "Components/FXMenuItem.h"
#include "Debug/MyDebug.h"
#include "Tools.h"
#include "Game.h"
#include <sstream>
#include "Scenes/MainMenuScene.h"
#include "FBChallangeMap.h"
#include "FBViewLevelNode.h"
#include "RunLogger/RunLogger.h"

//------------------------------------------------------------------------------
// FB Request Menu Mailbox Node
//------------------------------------------------------------------------------
FBRequestMenuMailboxNode* FBRequestMenuMailboxNode::Create( FBRequestMenu *parent, FBRequestsMap *map )
{
    FBRequestMenuMailboxNode* node;
    node = new FBRequestMenuMailboxNode( parent );
    
    if ( node )
    {
        node->autorelease();
        node->Init( map );
        return  node;
    }
    return NULL;
}
//------------------------------------------------------------------------------
FBRequestMenuMailboxNode::FBRequestMenuMailboxNode( FBRequestMenu *parent )
{
    _parent = parent;
    _state = eState_Idle;
    _loading = NULL;
    _internetAccess = true;
    _noInternetAccess = NULL;
}
//------------------------------------------------------------------------------
FBRequestMenuMailboxNode::~FBRequestMenuMailboxNode()
{
    if ( _state == eState_FBProcessing )
        FBTool::Get()->CancelRequestForListener( this );
    
    if ( _loading )
        _loading->stopAllActions();
}
//------------------------------------------------------------------------------
void FBRequestMenuMailboxNode::Init( FBRequestsMap *map )
{  
    _slidingNode = SlidingNode2::node( Config::GameSize, Config::eMousePriority_FBMenuLayer );
    _slidingNode->setIsTouchEnabled( true );
    _slidingNode->setPosition( CCPoint( -90.0f, 0.0f ));
    addChild( _slidingNode, 10 );
    
    GameStatus *status;
    status = Game::Get()->GetGameStatus();
    if ( ! status->GetMsgPlayed( eOTMsgRequest_CheckChallangeRequests ))
    {
        status->SetMsgPlayed( eOTMsgRequest_CheckChallangeRequests );
        _parent->ShowHelpMsg( LocalString("CheckChallangeRequestsMsg"));
    }
    
    if ( map )
    {
        _requestMap = *map;
        AddPiggySprite();
    }
    else
    {
        _loading = LoadingAnim::Create();
        _loading->SetPositionForFBRequestMenu();
        
        addChild( _loading, 200 );
    
        FBTool::Get()->Request_ReadAll( this );
        _state = eState_FBProcessing;
    }    
}
//------------------------------------------------------------------------------
void FBRequestMenuMailboxNode::FBRequestCompleted( FBTool::Request requestType, void *data )
{
     if ( _state == eState_Exiting )
        return;
    
    _requestMap = *((FBRequestsMap*)data);
    _state = eState_Idle;
    AddPiggySprite();
}
//-------------------------------------------------------------------------------
void FBRequestMenuMailboxNode::AddPiggySprite()
{
    RLOG_SS("FACEBOOK_MAILBOX", "REQUESTS_COUNT", _requestMap.size() );
    
    int finalOpacity;
    if ( ! _requestMap.size() )
    {
        finalOpacity = 255;
        if ( _loading )
        {
            _loading->Stop();
            _loading = NULL;
        }
    }
    else
    {
        finalOpacity = Config::FBRequestPiggyBgOpacity;
        Setup0();
    }
    
    FadeInSprite *sprite;
    sprite = FadeInSprite::Create( 1.0f, "Images/Facebook/Requests/noRequestsPiggy.png", 0.0f, finalOpacity );
    sprite->setPosition( CCPoint( Tools::GetScreenMiddleX() + 180.0f, Tools::GetScreenMiddleY() - 150.0f ));
        
    addChild( sprite, -10 );
}
//------------------------------------------------------------------------------
void FBRequestMenuMailboxNode::Exiting()
{
    _loading->stopAllActions();
    
    if ( _state == eState_FBProcessing )
        FBTool::Get()->CancelRequestForListener( this );
    
    _state = eState_Exiting;
}
//------------------------------------------------------------------------------
void FBRequestMenuMailboxNode::FBRequestError( int error )
{
    _state = eState_Idle;
    if ( _loading )
    {
        _loading->Stop();
        _loading = NULL;
    }
    
    ShowNoInternetAccess();
}
//------------------------------------------------------------------------------
void FBRequestMenuMailboxNode::Http_FailedWithError( void *connection, int error )
{
    if ( _state == eState_Exiting )
        return;

    _state = eState_Idle;
    if ( _loading )
    {
        _loading->Stop();
        _loading = NULL;
    }
    
    ShowNoInternetAccess();
}
//------------------------------------------------------------------------------
void FBRequestMenuMailboxNode::Http_FinishedLoading( void *connection, const void *data, int len )
{
    if ( _state == eState_Exiting )
        return;
    
    _state = eState_Idle;
    if ( _loading )
    {
        _loading->Stop();
        _loading = NULL;
    }
    
    _parent->Setup_StatsView( _requestedUser.c_str() );
}
//------------------------------------------------------------------------------
void FBRequestMenuMailboxNode::ShowNoInternetAccess()
{
    _internetAccess = false;
    
    if ( _noInternetAccess )
        _noInternetAccess->Show();
    
    if ( ! _noInternetAccess )
    {
        _noInternetAccess = FadeInSprite::Create( 0.5, "Images/Facebook/Requests/noInternet.png" );
        _noInternetAccess->setPosition( ccp( Tools::GetScreenMiddleX(), Tools::GetScreenMiddleY() - 64.0f ));
        addChild(_noInternetAccess);
    }
}
//------------------------------------------------------------------------------
void FBRequestMenuMailboxNode::Setup0()
{
   runAction(
             CCSequence::actions(
                                 CCCallFunc::actionWithTarget(this, callfunc_selector(FBRequestMenuMailboxNode::Setup1 )),
                                 CCDelayTime::actionWithDuration( 0.25f ),
                                 CCCallFunc::actionWithTarget(this, callfunc_selector(FBRequestMenuMailboxNode::Setup2 )),
                                 CCDelayTime::actionWithDuration( 0.25f ),
                                 CCCallFunc::actionWithTarget(this, callfunc_selector(FBRequestMenuMailboxNode::Setup3 )),
                                 NULL));
}
//------------------------------------------------------------------------------
void FBRequestMenuMailboxNode::Setup1()
{
    if ( _loading )
        _loading->Stop();
    
    //-------------------
    // Sliding menu setup
    CCNode *node;
    
    _menuAccept = CCMenu::menuWithItems( Config::eMousePriority_FBMenuLayer, NULL, NULL );
    _menuRemove = CCMenu::menuWithItems( Config::eMousePriority_FBMenuLayer, NULL, NULL );
    
    _slidingNode->GetContentNode()->addChild(_menuAccept, 10);
    _slidingNode->GetContentNode()->addChild(_menuRemove, 10);
    
    for ( FBRequestsMap::iterator it = _requestMap.begin(); it != _requestMap.end(); it++ )
    {
//        if ( it->second._requestData.length() )
//        {
//            D_STRING( it->second._requestData );
//            D_INT( it->second._levelEnum );
//            D_INT( it->second._levelCompletedSeconds );
//        }
      
        node = FBRequestMenuItem::Create( it->second );
        node->setPosition( ccp( 0.0f, 0.0f ));
        _slidingNode->AddToContent( node );
        
        _menuAccept->addChild( GetMenuItemAccept( it->first.c_str() ), 10 );
        _menuRemove->addChild( GetMenuItemRemove( it->first.c_str() ), 10 );
    }
    
    
    float padding;
    float deltaX;
    
    deltaX = 240.0f;
    padding = 210.0f;

    CCPoint startingPos;
    startingPos = CCPoint( -10.0f, -40.0f );
   
    _slidingNode->SetClickableArea( CCPoint( 0.0f, 0.0f ), CCPoint( 960.0f, 640.0f));
    _slidingNode->GetContentNode()->AlignVertical( padding );
    _slidingNode->GetContentNode()->setPosition( CCPoint( Tools::GetScreenMiddleX() - deltaX, Tools::GetScreenMiddleY() + 100.0f ));
    _slidingNode->GetContentNode()->setContentSize( CCSize( 100.0f, (_requestMap.size() + 1 ) * padding ));
    _slidingNode->ShowItem( 1 );
	_slidingNode->SetInitPosition( startingPos );
    
    _menuAccept->alignItemsVerticallyWithPadding( padding - 116.0f );
    _menuRemove->alignItemsVerticallyWithPadding( padding - 116.0f ); // 100.0f = button size
    
    CCPoint menuPos;
    menuPos.x = + 560.0f;
    menuPos.y = - padding * (_requestMap.size() ) / 2.0f + 105.0f;
    
    _menuAccept->setPosition( menuPos );
    _menuRemove->setPosition( CCPoint( menuPos.x + 135.0f, menuPos.y ) );

    _menuAccept->setOpacity( 0 );
    _menuRemove->setOpacity( 0 );
}
//------------------------------------------------------------------------------
void FBRequestMenuMailboxNode::Setup2()
{
    _menuAccept->runAction( CCFadeTo::actionWithDuration( 0.5f, 255 ));
}
//------------------------------------------------------------------------------
void FBRequestMenuMailboxNode::Setup3()
{
    _menuRemove->runAction( CCFadeTo::actionWithDuration( 0.5f, 255 ));
}
//------------------------------------------------------------------------
CCMenuItem* FBRequestMenuMailboxNode::GetMenuItemAccept( const char *requestId )
{
    TrippleSprite triSprite;
    FXMenuItemSpriteWithAnim *item;
    
    triSprite = TrippleSprite("Images/Facebook/Requests/acceptBt.png");
    item = FXMenuItemSpriteWithAnim::Create( &triSprite, this, menu_selector( FBRequestMenuMailboxNode::Menu_Accept ));
    item->setUserData( (void*)requestId );
    
    return item;
}
//------------------------------------------------------------------------
CCMenuItem* FBRequestMenuMailboxNode::GetMenuItemRemove( const char *requestId )
{
    TrippleSprite triSprite;
    FXMenuItemSpriteWithAnim *item;
    
    triSprite = TrippleSprite("Images/Facebook/Requests/removeBt.png");
    item = FXMenuItemSpriteWithAnim::Create( &triSprite, this, menu_selector( FBRequestMenuMailboxNode::Menu_Remove ));
    item->setUserData( (void*)requestId );
    
    return item;
}
//------------------------------------------------------------------------
void FBRequestMenuMailboxNode::Menu_Accept( CCObject* sender )
{
    //----------
    // Get item level data
    CCMenuItem *item;
    const char *requestId;
    
    item = (CCMenuItem*) sender;
    requestId = (const char *) item->getUserData();

    D_LOG("fb request id: %s", requestId );
    RLOG_SS("FACEBOOK_MAILBOX","ACCEPT_CHALLENGE", _requestMap[requestId]._levelEnum );

    //-----------------
    // WWW challanges update
    stringstream fromUser;
    GameTypes::LevelEnum levelEnum;
    unsigned int seconds;
    
    fromUser << _requestMap[requestId]._fromUserFbId.c_str();
    levelEnum = (LevelEnum) _requestMap[requestId]._levelEnum;
    seconds = _requestMap[requestId]._levelCompletedSeconds;
    
    FBTool::Get()->Request_DeleteRequest( requestId, NULL );
   
    if ( Game::Get()->GetGameStatus()->GetLevelStatus( levelEnum ) != GameStatus::eLevelStatus_Done )
    {
        ChallangeAccept( fromUser.str().c_str(), levelEnum, seconds, false );

        //---------------------
        // execute request
        Preloader::Get()->Reset();
        Preloader::Get()->FreeResources();

        Game::Get()->GetGameStatus()->ResetPreviouslyPlayedLevel();
        Game::Get()->CreateAndPlayLevel( (GameTypes::LevelEnum) levelEnum, Game::eLevelToLevel );

        CleanUp();
    }
    else
    {
        ChallangeAccept( fromUser.str().c_str(), levelEnum, seconds, true );
        _requestedUser = fromUser.str();
    }
}
//------------------------------------------------------------------------
void FBRequestMenuMailboxNode::ChallangeAccept( const char *fromUser,
                                               GameTypes::LevelEnum levelEnum,
                                               unsigned int seconds,
                                               bool managed )
{
    stringstream url;
    
    url << Config::FBRequestsURL << "challangeAccept.php?";
    url << "fbId=" << FBTool::Get()->GetFBUserId() << "&";
    url << "fbIdOpponent=" << fromUser << "&";
    url << "levelEnum=" << levelEnum;
    
    D_STRING(url.str());

    if ( managed )
    {
        WWWTool::Get()->HttpGet( url.str().c_str(), this );
        _state = eState_FBProcessing;
        
        _loading = LoadingAnim::Create();
        _loading->SetPositionForFBRequestMenu();
        addChild( _loading, 200 );
    }
    else
        WWWTool::Get()->HttpGet( url.str().c_str() );
}
//------------------------------------------------------------------------
void FBRequestMenuMailboxNode::Menu_Remove( CCObject* sender )
{
    CCMenuItem *item;
    const char *requestId;
    
    item = (CCMenuItem*) sender;
    requestId = (const char *) item->getUserData();
 
    RLOG_SS("FACEBOOK_MAILBOX","REMOVE_CHALLENGE", _requestMap[requestId]._levelEnum );

    D_LOG("fb request id: %s", requestId );
    FBTool::Get()->Request_DeleteRequest( requestId );
    
    FBRequestsMap::iterator it;
    it = _requestMap.find( requestId );
    if ( it != _requestMap.end() )
    {
        _requestMap.erase( it );
        _parent->Setup_MailboxView( _requestMap );
    }
}
//------------------------------------------------------------------------
void FBRequestMenuMailboxNode::CleanUp()
{
    _slidingNode->onExit();
    _parent->CleanUp();
    
    if ( _state == eState_FBProcessing )
        FBTool::Get()->CancelRequestForListener( this );
    else if ( _state == eState_HTTPGet )
        WWWTool::Get()->CancelHttpGet( this );
}
