#ifndef __piggy_biggy__FBRequestStatsTotalsNode__
#define __piggy_biggy__FBRequestStatsTotalsNode__

//------------------------------------------------------------------------------
#include <cocos2d.h>
#include "Platform/WWWTool.h"
#include "Facebook/Menus/FBChallangeMenu.h"
#include "FBChallangeMap.h"
//------------------------------------------------------------------------------
USING_NS_CC;

//------------------------------------------------------------------------------
// FB Request Menu Stats Totals Node
//------------------------------------------------------------------------------
class FBRequestMenu;
class FBRequestMenuStatsTotalsNode : public CCNode, public WWWToolListener
{
public:
    static FBRequestMenuStatsTotalsNode* Create( FBRequestMenu *parent, const char *fbIdOpponent = NULL );
    static FBChallangeStatus GetChallangeStatus( const char* status );
    
    ~FBRequestMenuStatsTotalsNode();
    
    void Exiting();
    
private:
    FBRequestMenuStatsTotalsNode( FBRequestMenu *parent, const char *fbIdOpponent );
    void Init();
  
    virtual void Http_FailedWithError( void *connection, int error );
    virtual void Http_FinishedLoading( void *connection, const void *data, int len );

    void Setup1();
    void Setup2();
    void Setup3();
    
    CCNode* GetUserNameNode( const char *fbId );
    CCMenuItem* GetMenuItemView( const FBChallangeTotalsRecord &record );
    void Menu_View(CCObject *sender );
   
    void ShowStatsForUser( const char *fbIdOpponent );
    void ShowDefaultStats();
    void ChallangeGetMy();
    void ChallangeAccept( const char *fromUser, GameTypes::LevelEnum levelEnum, unsigned int seconds );
    
    void CleanUp();
    void ShowNoInternetSprite();
    
protected:
    SlidingNodeMenu *_menuView;
    LoadingAnim     *_loading;

    typedef enum
    {
        eState_Idle = 0,
        eState_Processing,
        eState_Exiting
    } State;
    
    State   _state;

    FBChallangeMap          _challangeMap;
    FBChallangeTotalsMap    _challangeTotalsMap;
    SlidingNode2            *_slidingNode;
    FBRequestMenu           *_parent;
    string                  _fbIdOpponent;
    
    bool                    _internetAccess;
    FadeInSprite            *_noInternetAccess;
};


//------------------------------------------------------------------------------
// FB Request Menu Stats Totals Node
//------------------------------------------------------------------------------
class FBRequestMenuStatsTotalsRecordNode : public CCNode
{
public:
    static float _sXMove;
    static float _sXStart;

public:
    static FBRequestMenuStatsTotalsRecordNode* Create( const FBChallangeTotalsRecord &record );
    ~FBRequestMenuStatsTotalsRecordNode();
    
private:
    FBRequestMenuStatsTotalsRecordNode( const FBChallangeTotalsRecord &record );
    void Init();
    
    void Setup1();
    void Setup2();
    void Setup3();
    void Setup4();
    
private:
    FBChallangeTotalsRecord _challangesTotal;
};



#endif 
