#ifndef __GLOBALS_H__
#define __GLOBALS_H__

#include <set>
#include <map>
#include <string>
#include "GameTypes.h"

using namespace std;

typedef set<GameTypes::BlockEnum> BlockEnumContainer;
typedef map<GameTypes::LevelEnum, string> LevelNamesContainer;

typedef enum
{
    eDisplay_LandscapeRotating = 1,
    eDisplay_LandscapeFixed,
    eDisplay_PortraitFixed
} DisplayRotateType;

typedef enum
{
    eDevice_unknown = 0,
    eDevice_simulator,
    
    eDevice_iPod1_1,    // ipod //2
    eDevice_iPod2_1,    // ipod second gen //3
    eDevice_iPod3_1,    // ipod third gen
    eDevice_iPod4_1,    // ipod fourth gen

    eDevice_iPhone1_1,  // iphone   // 6
    eDevice_iPhone1_2,  // 3g       // 7
    eDevice_iPhone2_1,  // 3gs      // 8
    
    eDevice_iPhone3_1,  // 4        // 9
    eDevice_iPhone4_1,  // 4s       // 10
    eDevice_iPhone5_1,  // 5        // 11
    eDevice_iPhone5_2,  // 5        // 12

    eDevice_iPad1_1,    // ipad 1   // 13
    eDevice_iPad2_1,    // ipad 2   // 14
    eDevice_iPad3_1,    // ipad 3   // 15
    eDevice_iPad_Mini   // ipad mini //16
    
    
} DeviceType;

extern unsigned int gAppEventMemoryWarningCount;
extern unsigned int gAppEventActiveCount;
extern unsigned int gAppEventDeactiveCount;
extern unsigned int gAppEventEnterBackgroundCount;
extern unsigned int gAppEventnterForegroundCount;
extern bool         gGLBufferSwapEnabled;

extern DisplayRotateType gAppDisplayRotateMode;
extern DeviceType gDevice;

//-----------------------------------------------------------------------
// Global
//-----------------------------------------------------------------------
class GameGlobals
{
public:
    static GameGlobals* Get();
	static void Destroy();

	~GameGlobals();
    
	BlockEnumContainer& FullAnimableBlockNames();
	BlockEnumContainer& RolledAnimableBlockNames();
	BlockEnumContainer& NoShadowBlockNames();
	BlockEnumContainer& StepableBlockNames();
	BlockEnumContainer& QuitableBlockNames();
   	BlockEnumContainer& ExitableBlockNames();
	BlockEnumContainer& FinishableBlockNames();
    
	LevelNamesContainer& LevelNames();
    void SetLevelNames();

private:
	GameGlobals();

    
private:
	static GameGlobals *_sInstance;

	BlockEnumContainer		_fullAnimableBlockName;
	BlockEnumContainer		_rolledAnimableBlockName;
	BlockEnumContainer		_noShadowBlockName;
	BlockEnumContainer		_stepableBlockName;
	BlockEnumContainer		_quitableBlockName;
  	BlockEnumContainer		_exitableBlockName;
	BlockEnumContainer		_finishableBlockName;

	LevelNamesContainer		_levelNames;
};
//-----------------------------------------------------------------------

#endif
