#include "cocos2d.h"
#include "GameConfig.h"
#include "Game.h"
#include "DebugDraw.h"
#include "Scenes/MainMenuScene.h"
#include "Scenes/Transitions.h"
#include "Scenes/WellWellDoneScene.h"
#include "Scenes/NextLevelScene.h"
#include "Scenes/UnlockPocketScene.h"
#include "SoundEngine.h"
#include "Levels/AllLevels.h"
#include "Tools.h"
#include "Debug/DebugNode.h"
#include "Debug/MyDebug.h"
#include "LoadingBar.h"
#include "Levels/Pockets.h"
#include "Animation/AnimManager.h"
#include "Animation/AnimTools.h"
#include "Preloader.h"
#include "Billing/Billing.h"
#include "Debug/DebugLogger.h"
#include "Debug/DebugTimer.h"
#include "Blocks/BlockNames.h"
#include "Platform/GameCenter.h"
#include "Facebook/FBTool.h"
#include "TestFlight/Report.h"
#include "RunLogger/RunLogger.h"
#include "GLogger.h"
#include "Platform/Lang.h"
#include "Promotion/FBPoster.h"
#include "GameWatcher.h"
#include "Platform/WWWTool.h"
#include "Platform/ScoreTool.h"

#ifdef BUILD_EDITOR
#include "Editor/GameEditor.h"
#endif


USING_NS_CC;

Game* Game::_sInstance = NULL;


//----------------------------------------------------------------------------------------
Game* Game::Get()
{
	if ( _sInstance == NULL )
	{
		_sInstance = new Game();
		_sInstance->Init();
	}

	return _sInstance;
}
//----------------------------------------------------------------------------------------
void Game::Destroy()
{
	if ( _sInstance )
	{
		delete _sInstance;
		_sInstance = NULL;
	}
}
//----------------------------------------------------------------------------------------
Game::Game()
{
	_level = NULL;

#ifdef BUILD_EDITOR
	_isSchedule = false;
#else
	_isSchedule = true;
#endif

	AdjustScale();   
}
//-------------------------------------------------------------------------------------------
void Game::Init()
{
    _lastPlayedLevelOrient = eLevel_Horizontal;
    
    GameWatcher::Get(); // just to preload
    FBPoster::Get();
    FBTool::Get();
    WWWTool::Get();
    ScoreTool::Get();
    
	//-------------------
	// Preloader
	Preloader::Get()->AddSound( SoundEngine::eSoundChangeScene );
	Preloader::Get()->AddSound( SoundEngine::eSoundButtonClick );

	//Preloader::Get()->AddPiggyAnimSet( 60.0f );
	Preloader::Get()->AddImageDir( "Images/Menus/LevelMenu/" );
	Preloader::Get()->AddImageDir( "Images/Menus/SoundMenu/" );
	Preloader::Get()->AddImageDir( "Images/OtherPreload/" );
	
	Preloader::Get()->Preload();
	
#ifdef BUILD_TEST
	DebugTimer::Get()->SetLabel( 1, "Level create total");
	DebugTimer::Get()->SetLabel( 2, "Level preload");
	//NotifyResponder::Get()->TestAll();
#endif
}
//-------------------------------------------------------------------------------------------
Game::~Game()
{
    GameCenter::Destroy();
	Billing::Destroy();
	Preloader::Destroy();
	SoundEngine::Destroy();
	AnimManager::Destroy();
	AnimerSimpleEffect::Destroy();
	AnimTools::Destroy();
	PocketManager::Get()->release();
	Skins::Destroy();
	BlockNames::Destroy();
	GameGlobals::Destroy();
    Lang::Destroy();
    FBTool::Destroy();
    FBPoster::Destroy();
    GameWatcher::Destroy();
    ScoreTool::Destroy();
    WWWTool::Destroy();

    
#ifdef BUILD_TEST
	DebugTimer::Destroy();
#endif

#ifdef BUILD_TESTFLIGHT
    Report::Destroy();
#endif

}
//-------------------------------------------------------------------------------------
Level* Game::CreateLevel( GameTypes::LevelEnum levelEnum )
{
#ifdef BUILD_EDITOR
	if ( _level && _level->getParent() )
	{
		_level->removeFromParentAndCleanup( true );
	}
#endif

#ifdef BUILD_TEST
	DebugTimer::Get()->Start( 1 );
#endif
    
	_gameStatus.SetCurrentLevel( levelEnum );
	_level = _levelFactory.CreateLevel( levelEnum );
	_level->Center();

#ifdef BUILD_EDITOR
	_level->LevelPause();
#endif
	return _level;
}
//-------------------------------------------------------------------------------------
void Game::CreateAndPlayLevel( GameTypes::LevelEnum levelEnum, LevelTransitionType transType, bool asnycLoad )
{
	_levelToCreate = levelEnum;
	_transitionType = transType;

#ifdef BUILD_EDITOR
	DoCreateLevel( NULL );
#else
   if ( ! _buyChecker.BuyIfNeeded( levelEnum ))
   {
      if ( asnycLoad )
         LoadingBar::GetAdjusted()->AddToScene( &Game::DoCreateLevel, NULL );
      else
         DoCreateLevel( NULL );
   }
#endif
}
//-------------------------------------------------------------------------------------
void Game::DoCreateLevel( void *data )
{
	CCScene *scene;
 	Level *level;
	
	scene = CCScene::node();
	level = _sInstance->CreateLevel( _sInstance->_levelToCreate );

	scene->setPosition( ccp( 0.0f, 0.0f ));
	scene->setUserData( (void*) _sInstance->_level );
	
#ifndef BUILD_EDITOR
	scene->addChild( _sInstance->_level );
	//if ( DebugNode::Get()->getParent() )
	//	DebugNode::Get()->removeFromParentAndCleanup( false ) ;
	//scene->addChild( DebugNode::Get() );
#else
	if ( GameEditor::Get()->getParent() )
		GameEditor::Get()->removeFromParentAndCleanup( false );
		
	GameEditor::Get()->addChild( level );
	scene->addChild( GameEditor::Get() );
	
	if ( DebugNode::Get()->getParent() )
		 DebugNode::Get()->removeFromParentAndCleanup( false ) ;
	scene->addChild( DebugNode::Get() );

	CCDirector::sharedDirector()->replaceScene( scene );
	return;
#endif

	CCScene* pScene;
	pScene = NULL;

	switch ( _sInstance->_transitionType )
	{
	case eLevelToLevel :
		pScene = LevelToLevelTransition::transitionWithDuration( 1.0f, scene );
		break;

	case eLevelRestart :
		pScene = LevelRestartTransition::transitionWithDuration( 1.0f, scene, ccWHITE );
		break;

	default:
		unAssertMsg( Game, false, ("Unknow transition type %d", _sInstance->_transitionType ));
	}

#ifdef BUILD_TEST
	DebugTimer::Get()->Stop( 1 );
	DebugTimer::Get()->Flush();
#endif

	if ( pScene )
		CCDirector::sharedDirector()->replaceScene( pScene );
}
//-------------------------------------------------------------------------------------------
void Game::ToggleDebugDraw()
{
	_level->ToggleDebugDraw();
}
//--------------------------------------------------------------------------------------------------
void Game::SkipLevel()
{
    RLOG_II( "LEVEL_SKIP", _gameStatus.GetCurrentLevel(), _gameStatus.GetCurrentLevelIndex() );
    GLogLevelEvent( _gameStatus.GetCurrentLevelIndex(), "Skip" );

    Game::Get()->GetGameStatus()->SetSkipUsed();
    GameWatcher::Get()->RecordEvent( GameTypes::eGameEvent_FBLevelSkipped,(void*)(int)_gameStatus.GetCurrentLevel() );
	SoundEngine::Get()->PlayEffect( SoundEngine::eSoundLevelSkipped );

	if ( _gameStatus.GetCurrentLevelStatus() == GameStatus::eLevelStatus_LastOpen )
	{
		_gameStatus.SetCurrentLevelStatus( GameStatus::eLevelStatus_Skipped );
		_gameStatus.SetLevelStatus( _gameStatus.GetNextUndoneLevel(), GameStatus::eLevelStatus_LastOpen );
		_gameStatus.Save();
	}

	PlayNextLevel();
}
//--------------------------------------------------------------------------------------------------
void Game::LevelFinished()
{
	if ( _gameStatus.GetCurrentLevelStatus() != GameStatus::eLevelStatus_Done )
	{
        _gameStatus.SetCurrentLevelStatus( GameStatus::eLevelStatus_Done );
        
        GameWatcher::Get()->RecordEvent( GameTypes::eGameEvent_FBScoreUpdate, NULL );
        GameCenterUpdate();

		if ( ! _gameStatus.HasGameFinished() )
		{
			_gameStatus.SetLevelStatus( _gameStatus.GetNextUndoneLevel(), GameStatus::eLevelStatus_LastOpen );
			_gameStatus.Save();
		}
		else
		{
			_gameStatus.SetToFirstLevel();
			_gameStatus.Save();
			if ( _level ) _level->Quiting();
			WellWellDoneScene::ShowScene();		
		}
	}
}
//--------------------------------------------------------------------------------------------------
void Game::GameCenterUpdate()
{
    GameCenter::Get()->PushScoresToServer();
}
//--------------------------------------------------------------------------------------------------
void Game::RestartLevel()
{
    int secondsCount;
    secondsCount = Game::Get()->GetGameStatus()->GetLevelTime( _level->GetLevelEnum() );
    
    RLOG_III("LEVEL_RESTART", (int)_gameStatus.GetCurrentLevel(), _gameStatus.GetCurrentLevelIndex(), secondsCount );
//    GLogLevelEvent( _gameStatus.GetCurrentLevelIndex(), "Restart" );
    
	SoundEngine::Get()->PlayEffect( SoundEngine::eSoundLevelRestart );
    
    if ( CheckForSkipLevelIntro() )
    {
       _level->ShowSkipLevelIntro();
       return;
    }

    _gameStatus.IncLevelRestart( _gameStatus.GetCurrentLevel() );
	_gameStatus.NutralizePreviouslyPlayedLevel();

	if ( _level ) _level->Quiting();
	CreateAndPlayLevel( _gameStatus.GetCurrentLevel(), eLevelRestart, false );
}
//----------------------------------------------------------------------------------------
bool Game::CheckForSkipLevelIntro()
{
    bool skipIntroPlayed;
    unsigned int levelRestartCount;
    GameStatus::LevelStatus levelStatus;
    
    levelStatus         = _gameStatus.GetLevelStatus(_level->GetLevelEnum());
    skipIntroPlayed     = _gameStatus.GetMsgPlayed( eOTMsg_SkipLevelIntro );
    levelRestartCount   = _gameStatus.GetLevelRestartCount(_level->GetLevelEnum());

    if (
        levelRestartCount + 1 < Config::ShowSkipIntro_LevelRestarCount ||
        levelStatus != GameStatus::eLevelStatus_LastOpen ||
        _level->GetOrientation() == eLevel_Vertical ||
        skipIntroPlayed ||
        Game::Get()->GetGameStatus()->GetSkipUsed() )
    {
        return false;
    }
    
    return true;
    
}
//----------------------------------------------------------------------------------------
GameStatus::LevelStatus Game::GetRuningLevelStatus()
{
	return ( _gameStatus.GetLevelStatus( _gameStatus.GetCurrentLevel()));
}
//----------------------------------------------------------------------------------------
void Game::PlayNextLevel()
{
    if ( _level ) _level->Quiting();
    
    _gameStatus.LevelNext();
    
    //check if open pocket anim should be played
    bool isLevelFistInPocket;
    isLevelFistInPocket =  PocketManager::Get()->IsLevelFirstInPocket( _gameStatus.GetCurrentLevel(), _gameStatus.GetCurrentPocket() );
    
    // D_INT( _gameStatus.GetCurrentPocket() );
    
    if ( isLevelFistInPocket && ! _gameStatus.GetPocketOpenAnimPlayed( _gameStatus.GetCurrentPocket()) )
    {
        _gameStatus.SetPocketOpenAnimPlayed( _gameStatus.GetCurrentPocket(), true );
        UnlockPocketNode::ShowScene( _gameStatus.GetCurrentPocket() );
    }
    else
        NextLevelScene::DoShowScene( NULL );
}
//----------------------------------------------------------------------------------------
void Game::AdjustScale()
{
	CCSize size;
	float scaleX;
	float scaleY;

	size = CCDirector::sharedDirector()->getWinSize();

	scaleX = size.width / Config::GameSize.width;
	scaleY = size.height / Config::GameSize.height;

	_scale = std::min<float>( scaleX, scaleY );
	//_scale = 0.5f;
#ifndef ANDROID_BUILD
	//_scale = 0.5;
#endif
}
//----------------------------------------------------------------------------------------
void Game::DumpMissingSprites()
{
	Level *level;
	GameLevelSequence levelSeq;
	int count;

	// this loging should not be done throw debug system
	// but should be independendt

#ifdef _DEBUG
	Debug::Init( Debug::MissingSpritesFile );
#endif
	count = 1;
	//_levelFactory.CreateLevel( GameTypes::eLevelIntro_Welcome );

	for ( GameLevelSequence::iterator it = levelSeq.begin(); it != levelSeq.end(); it++ )
	{
		DS_LOG_MISSING_PNG_RESET
		DS_LOG_MISSING_PNG_PLAIN( "-------------------------------------------------------");
		DS_LOG_MISSING_PNG_PLAIN( "%d) Level %s", count, GameGlobals::Get()->LevelNames()[ *it ].c_str() );
		DS_LOG_MISSING_PNG_PLAIN( "    enum: %d", (int)( *it ));
		DS_LOG_MISSING_PNG_PLAIN( "-------")
		
		level = _levelFactory.CreateLevel( *it, false );
		delete level;

		count++;
		DS_LOG_MISSING_PNG_PLAIN( "\n")
	}
}
//----------------------------------------------------------------------------------------
void Game::PauseGame()
{ 
    if ( _level )
        _level->LevelPause();
}
//----------------------------------------------------------------------------------------
void Game::ResumeGame()
{
    if ( _level )
        _level->LevelRun();
}
//----------------------------------------------------------------------------------------
void Game::ResetRuningLevel()
{
    if ( _level )
        _lastPlayedLevelOrient = _level->GetOrientation();
    
    _level = NULL;
}
//----------------------------------------------------------------------------------------

