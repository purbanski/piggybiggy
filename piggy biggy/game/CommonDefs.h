#ifndef __COMMONDEFS_H__
#define __COMMONDEFS_H__

#ifdef BUILD_EDITOR
#include "Editor/puBlockDef.h"
#endif

#include <string>
#include <map>
#include <set>
#include <list>
#include "Blocks/puBlock.h"

using namespace std;

// Base maps & sets
struct StrCmpHash
{
	bool operator()(const char* s1, const char* s2) const
	{ return strcmp( s1, s2) < 0; }
};

typedef map<const char*, const char *, StrCmpHash> MapStringString;
typedef map<const char*, string, StrCmpHash> MapPCharString;
typedef map<string, int> MapStringInt;
typedef map<int, string> MapIntString;
typedef set<string> SetString;
typedef set<int> SetInt;


// Function map with callback class member function
template <class C>
struct ClassFunctionMap
{
	typedef void* ( C::*FuncPtrType )( void * );
	typedef map<const char*, FuncPtrType, StrCmpHash>Type;
};


// Block and similar containers
typedef	std::vector<puBlockDef>		BlockDefsContainer;
//typedef	std::vector<puJointDef>		JointDefsContainer;
typedef	std::vector<b2Fixture *>	FixtureContainer;
typedef	std::vector<puBlock *>		BlockContainer;
typedef	std::set<puBlock *>			BlockSet;
//typedef std::list<int> PointerList;
// two below should be one list<b2Joint *>

typedef	std::set<b2Joint *> JointContainer;
typedef	std::vector<b2Joint *> JointVector;

typedef std::set<string> Filenames;

#ifdef BUILD_EDITOR

typedef std::set<puJointDef, bool(*)( puJointDef defA, puJointDef defB )> JointDefSet;
typedef set<string> Names;

#endif


#endif


