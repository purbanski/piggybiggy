#ifndef __BILLING_H__
#define __BILLING_H__
//---------------------------------------------------------------------------------
#include "GameTypes.h"
#include "PriceGetter.h"
#include "PriceGetterListener.h"

//---------------------------------------------------------------------------------
using namespace GameTypes;

typedef void( *BillingCallback )( void *data );

//---------------------------------------------------------------------------------
class Billing 
{
public:
	static Billing* Get();
	static void Destroy();

	~Billing();

    void Cancel();
    void SetTransactionState( BillingTransactionState state );
	void BuyExtensionPack( BillingCallback cbFn, void *data );
    bool IsPurchasedAllowed();
    void GetExtensionPackPrice( PriceGetterListener *listener );
    void CancelGetPrice();
    
private:
	Billing();
	void Init();
    void RestoreExtensionPack();
	void PurchasedCompleted();
	void CallCallbackFunction();

    void TransactionFinished();
    void TransactionCanceled();
    void TransactionUnknownError();
    void TransactionProductNotFound();
    
private:
    typedef enum
    {
        eAppGetNone = 0,
        eAppGetBuy,
        eAppGetRestore
    } AppGetType;

	static Billing *_sInstance;
	bool            _initiazlied;
	bool            _processing;
    AppGetType      _appGetType;
    BillingTransactionState  _transactionState;
    PriceGetter     _priceGetter;
    

	BillingCallback	_callBackFunction;
	void*			_callBackData;
};
//---------------------------------------------------------------------------------
#endif
