#ifndef piggy_biggy_PriceGetterListener_h
#define piggy_biggy_PriceGetterListener_h
//----------------------------------------------------------------------------------------
class PriceGetterListener
{
public:
    virtual void PriceRequestCompleted( void *data, unsigned int len ) = 0;
    virtual void PriceRequestError() = 0;
};
//----------------------------------------------------------------------------------------
#endif
