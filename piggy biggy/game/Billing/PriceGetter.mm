#include "PriceGetter.h"
#include "iOS/PriceGetter_iOS.h"
//------------------------------------------------------------------------------------
PriceGetter::PriceGetter()
{
    _priceGetter = [[PriceGetter_iOS alloc] init];
}
//------------------------------------------------------------------------------------
PriceGetter::~PriceGetter()
{
    [(PriceGetter_iOS*)_priceGetter cleanUp];
}
//------------------------------------------------------------------------------------
void PriceGetter::GetExtensionPackPrice(PriceGetterListener *listener)
{
    [(PriceGetter_iOS*)_priceGetter getExtensionPackPrice:listener];
}
//------------------------------------------------------------------------------------
void PriceGetter::Cancel()
{
    [(PriceGetter_iOS*)_priceGetter cancel];
}
//------------------------------------------------------------------------------------
