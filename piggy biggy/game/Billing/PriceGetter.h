#ifndef __piggy_biggy__PriceGetter__
#define __piggy_biggy__PriceGetter__
//------------------------------------------------------------------------------------
#include "PriceGetterListener.h"
//------------------------------------------------------------------------------------
class PriceGetter
{
public:
    PriceGetter();
    ~PriceGetter();

    void GetExtensionPackPrice(PriceGetterListener *listener);
    void Cancel();

private:
    void *_priceGetter;
};
//------------------------------------------------------------------------------------
#endif

