#include "Billing.h"
#include "Billing_iOS.h"
#include "PriceGetter_iOS.h"
#include "Game.h"
#include "MessageBox.h"
#include "Scenes/NextLevelScene.h"
#include "RunLogger/RunLogger.h"
#include "GLogger.h"
#include "Lang.h"

//------------------------------------------------------------------------
Billing * Billing::_sInstance = NULL;
//------------------------------------------------------------------------
Billing* Billing::Get()
{
	if ( ! _sInstance )
	{
		_sInstance= new Billing();

	}
	return _sInstance;
}
//------------------------------------------------------------------------
void Billing::Destroy()
{
	if ( _sInstance	)
		delete _sInstance;

	_sInstance = NULL;
}
//------------------------------------------------------------------------
Billing::Billing()
{
    _appGetType = eAppGetNone;
    _transactionState = eBillingTransactionBlank;
    
	_callBackData = NULL;
	_callBackFunction = NULL;
	_processing = false;
	_initiazlied = false;
}
//------------------------------------------------------------------------
Billing::~Billing()
{
    [ BillingIOS destroy ];
}
//------------------------------------------------------------------------
void Billing::RestoreExtensionPack()
{
    if ( _transactionState != eBillingTransactionBlank )
        return;
    
    // set by buy
    //_callBackFunction = cbFn;
    //_callBackData = data;
   
    if ( ! [ BillingIOS isPurchaseAllowed ] )
        return;
    
    _appGetType = eAppGetRestore;

    [[ BillingIOS sharedInstance ] buyExtensionPack: true ];
}
//------------------------------------------------------------------------
void Billing::BuyExtensionPack( BillingCallback cbFn, void *data )
{
    if ( _transactionState != eBillingTransactionBlank )
        return;
    
    _callBackFunction = cbFn;
    _callBackData = data;
   
    if ( ! [ BillingIOS isPurchaseAllowed ] )
        return;

    _appGetType = eAppGetBuy;
    [[ BillingIOS sharedInstance ] buyExtensionPack: false ];
}
//------------------------------------------------------------------------
void Billing::GetExtensionPackPrice(PriceGetterListener *listener)
{
    _priceGetter.GetExtensionPackPrice(listener);
}
//------------------------------------------------------------------------
void Billing::CancelGetPrice()
{
    _priceGetter.Cancel();
}
//------------------------------------------------------------------------
bool Billing::IsPurchasedAllowed()
{
    if ( ! [ BillingIOS isPurchaseAllowed ] )
	{
        MessageBox::CreateInfoBox( LocalString( "InAppDisabledMsg" ), LocalString( "InAppUpdateSettings" ));
        CallCallbackFunction();
	}

    return [ BillingIOS isPurchaseAllowed ];
}
//------------------------------------------------------------------------
void  Billing::SetTransactionState( BillingTransactionState state )
{
    switch (state)
    {
          case GameTypes::eBillingTransactionFinished :
            TransactionFinished();
            break;
            
        case GameTypes::eBillingTransactionErrorUnknown :
            TransactionUnknownError();
            break;
            
        case GameTypes::eBillingTransactionProductNotFound :
            TransactionProductNotFound();
            break;
        
        case GameTypes::eBillingTransactionCanceled :
//            if ( _appGetType == eAppGetBuy )
//                RestoreExtensionPack();
//            else
            TransactionCanceled();
            
            break;
            
        default:
            ;
    }
}
//----------------------------------------------------------------------
void Billing::TransactionProductNotFound()
{
    GLogEvent("Bill", "Product not found");
    RLOG_S("BILL","PROD_NOT_FOUND");
    
    _appGetType = eAppGetNone;
    
    MessageBox::CreateInfoBox( LocalString( "InAppTransFailed"), LocalString("InAppProdNotFound"));
    _transactionState = eBillingTransactionBlank;
    CallCallbackFunction();
}
//----------------------------------------------------------------------
void Billing::TransactionFinished()
{
    GLogEvent("Bill", "Transaction finished");
    RLOG_S("BILL","TRANS_FINISHED");

    _appGetType = eAppGetNone;
    
    PurchasedCompleted();
    CallCallbackFunction();
}
//----------------------------------------------------------------------
void Billing::TransactionUnknownError()
{
    GLogEvent("Bill", "Error unknown");
    RLOG_S("BILL","ERROR_UNKNOWN");

    _appGetType = eAppGetNone;
    
    MessageBox::CreateInfoBox( LocalString( "InAppTransFailed"), LocalString("InAppErrUnknown"));
    _transactionState = eBillingTransactionBlank;
    CallCallbackFunction();
}
//----------------------------------------------------------------------
void Billing::TransactionCanceled()
{
    GLogEvent("Bill", "Transaction Canceled");
    RLOG_S("BILL","TRANS_CANCELED");

    //MessageBox::CreateInfoBox( "Purchase canceled" , "");
    _transactionState = eBillingTransactionBlank;
    CallCallbackFunction();
}
//----------------------------------------------------------------------
void Billing::PurchasedCompleted()
{
	Game *game;
	GameStatus *gameStatus;

	game = Game::Get();
	
	gameStatus = game->GetGameStatus();
	gameStatus->GameHasBeenBought();

    GLogPurchased;
    
	Game::Get()->GetGameStatus()->ResetPreviouslyPlayedLevel();
    NextLevelScene::DoShowScene( NULL );
}
//------------------------------------------------------------------------
void Billing::CallCallbackFunction()
{
	if ( _callBackFunction )
		( *_callBackFunction )( _callBackData );
}
//------------------------------------------------------------------------
void Billing::Cancel()
{
    [[ BillingIOS sharedInstance ] cancel ];
}
//------------------------------------------------------------------------

