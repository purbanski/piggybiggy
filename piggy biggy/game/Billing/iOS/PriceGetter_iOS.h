#ifndef __PRICEGETTER_H__
#define __PRICEGETTER_H__

#import <Foundation/Foundation.h>
#import <StoreKit/StoreKit.h>
#include "../PriceGetterListener.h"

//------------------------------------------------------------------------
@interface PriceGetter_iOS : NSObject<SKProductsRequestDelegate>
{
    PriceGetterListener *_listener;
    SKProductsRequest   *_productRequest;
}
-(void) getExtensionPackPrice:(PriceGetterListener *) listener;
-(void) cancel;
-(void) cleanUp;
//------------------------------------------------------------------------

@end

#endif
