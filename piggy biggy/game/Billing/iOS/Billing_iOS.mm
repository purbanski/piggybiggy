#import "Billing_iOS.h"
#include "Billing.h"
#include "GameTypes.h"
#include "GameConfig.h"
#include "Debug/MyDebug.h"
//#import "GameConfig.h"

static BillingIOS *gBillingIOS = NULL;


@interface TransObserver ( Private )
- (void) transactionCleanUp: (SKPaymentTransaction *)transaction;
@end

//------------------------------------------------------------------------
//
//------------------------------------------------------------------------
@implementation TransObserver
{
}
//------------------------------------------------------------------------
- (id) init
{
    [super init];
    return self;
}
//------------------------------------------------------------------------
- (void) cancel
{
    //[super cancel];
}
//------------------------------------------------------------------------
- (void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *)transactions
{
    for (SKPaymentTransaction *transaction in transactions)
    {
        NSLog( @"Transaction state %d", transaction.transactionState );
        switch (transaction.transactionState)
        {
            case SKPaymentTransactionStatePurchased:
                [ self completeTransaction:transaction];
                break;
                
            case SKPaymentTransactionStateFailed:
                [self failedTransaction:transaction];
                break;
                
            case SKPaymentTransactionStateRestored:
                [self restoreTransaction:transaction];
                
            case SKPaymentTransactionStatePurchasing:
                NSLog( @"Purchasing" );
                break;
                
            default:
                break;
        }
    }
}
//------------------------------------------------------------------------
- (void) completeTransaction: (SKPaymentTransaction *)transaction
{
    [ self transactionCleanUp: transaction ];
    Billing::Get()->SetTransactionState( eBillingTransactionFinished );
}
//------------------------------------------------------------------------
- (void) restoreTransaction: (SKPaymentTransaction *)transaction
{
    [ self transactionCleanUp: transaction ];
    Billing::Get()->SetTransactionState( eBillingTransactionFinished );
}
//------------------------------------------------------------------------
- (void) failedTransaction: (SKPaymentTransaction *)transaction
{
    [ self transactionCleanUp: transaction ];
 
	NSLog(@"transaction error: %d", transaction.error.code );
   
    if (transaction.error.code != SKErrorPaymentCancelled)
        Billing::Get()->SetTransactionState( eBillingTransactionErrorUnknown );
    else
        Billing::Get()->SetTransactionState( eBillingTransactionCanceled );
}
//------------------------------------------------------------------------
- (void) transactionCleanUp: (SKPaymentTransaction *)transaction
{
    [[ SKPaymentQueue defaultQueue ] finishTransaction: transaction ];
    [[ SKPaymentQueue defaultQueue ] removeTransactionObserver:self ];
}
//------------------------------------------------------------------------
@end




//------------------------------------------------------------------------
//
//------------------------------------------------------------------------
@implementation BillingIOS
{
}
//------------------------------------------------------------------------
+ (BillingIOS *)sharedInstance
{
    if ( ! gBillingIOS )
    {
        gBillingIOS = [[ BillingIOS alloc] init ];
    }
    
    return gBillingIOS;
}
//------------------------------------------------------------------------
+ (void) destroy
{
    if ( gBillingIOS )
        [ gBillingIOS dealloc ];
    
    gBillingIOS = NULL;
}
//------------------------------------------------------------------------
+ (bool) isPurchaseAllowed
{
    return [SKPaymentQueue canMakePayments];
}
//------------------------------------------------------------------------
- (id) init
{
    [ super init ];
    
    m_transObserver = [[ TransObserver alloc] init ];
    m_restore = false;
    m_productRequest = NULL;
    
    return self;
}
//------------------------------------------------------------------------
- (void) dealloc
{
    [ super dealloc ];
    [ m_transObserver release ];
}
//------------------------------------------------------------------------
- (void) buyExtensionPack: (bool) restore
{
    m_restore = restore;
    
    //-------------------
    // requestProductData
    NSSet *productIdentifiers =[ NSSet setWithObjects:
                                [ NSString stringWithUTF8String: Config::AppStoreProductName ],
                                nil];
    
    m_productRequest = [[ SKProductsRequest alloc ]
                        initWithProductIdentifiers: productIdentifiers ];

    m_productRequest.delegate = self;
    [m_productRequest start];

}
//------------------------------------------------------------------------
- (void) productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response
{
    NSArray *myProducts = response.products;

    if ( myProducts.count != 1 )
    {
        Billing::Get()->SetTransactionState( GameTypes::eBillingTransactionProductNotFound );
        return;
    }

    //------------------------------------
    // place observer
    [[ SKPaymentQueue defaultQueue ] addTransactionObserver: m_transObserver ];
    

    //------------------------------------
    // requestProductData
    SKProduct *productId;
    productId = [ myProducts objectAtIndex: 0 ];
    
    if ( m_restore )
    {
        [[SKPaymentQueue defaultQueue] restoreCompletedTransactions];
    }
    else
    {
        SKPayment *payment = [SKPayment paymentWithProduct:productId];
        [[SKPaymentQueue defaultQueue] addPayment:payment];
    }
}
//------------------------------------------------------------------------
- (void) cancel
{
    if ( m_productRequest )
    {
        [ m_productRequest cancel ];
        m_productRequest = NULL;
    }
    
       [m_transObserver cancel ];
    
    [[ SKPaymentQueue defaultQueue ] removeTransactionObserver:m_transObserver ];
}
//------------------------------------------------------------------------
@end
