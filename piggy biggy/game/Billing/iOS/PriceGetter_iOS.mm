#import "PriceGetter_iOS.h"
#include "GameConfig.h"

@implementation PriceGetter_iOS
//------------------------------------------------------------------------------
- (void)getExtensionPackPrice:(PriceGetterListener*)listener
{
    _listener = listener;

    //-------------------
    // requestProductData
    NSSet *productIdentifiers =[ NSSet setWithObjects:
                                [ NSString stringWithUTF8String: Config::AppStoreProductName ],
                                nil];

    _productRequest = [[ SKProductsRequest alloc ]
                        initWithProductIdentifiers: productIdentifiers ];

    _productRequest.delegate = self;
    [_productRequest start];
}
//------------------------------------------------------------------------------
- (void) productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response
{
    NSArray *myProducts = response.products;

    if ( myProducts.count != 1 )
    {
        [_productRequest release];
        _productRequest = NULL;

        if ( _listener )
            _listener->PriceRequestError();
        return;
    }
    
    SKProduct *extPack;
    extPack = [myProducts firstObject];
    
#ifdef DEBUG
    if (extPack)
    {
        NSLog(@"Product title: %@" , extPack.localizedTitle);
        NSLog(@"Product description: %@" , extPack.localizedDescription);
        NSLog(@"Product price: %@" , extPack.price);
        NSLog(@"Product id: %@" , extPack.productIdentifier);
    }
#endif
    
    if ( _listener )
    {
        NSNumberFormatter *fmtr = [[[NSNumberFormatter alloc] init] autorelease];
        [fmtr setNumberStyle:NSNumberFormatterCurrencyStyle];
        [fmtr setLocale:[extPack priceLocale]];
        
        NSString *price;
        price = [ NSString stringWithFormat:@"%@ %@", extPack.price, fmtr.currencySymbol ];

        _listener->PriceRequestCompleted( (void*) [price UTF8String], [price length]);
    }
    
    [_productRequest release];
    _productRequest = NULL;
}
//------------------------------------------------------------------------------
-(void)cancel
{
    if ( _productRequest )
    {
        [_productRequest cancel];
        [_productRequest release];
        _productRequest = NULL;
    }
    _listener = NULL;
}
//------------------------------------------------------------------------------
-(void)cleanUp
{
    _listener = NULL;
    
    if ( _productRequest )
    {
        [_productRequest release];
        _productRequest = NULL;
    }
    
    [self release];
}
@end
