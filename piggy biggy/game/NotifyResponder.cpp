#include <sstream>
#include "NotifyResponder.h"
//#include "SysNotifications.h"
#include "Preloader.h"
#include "Debug/DebugLogger.h"
#include "Game.h"
//---------------------------------------------------------------------
NotifyResponder *NotifyResponder::_sInstance	= NULL;
int NotifyResponder::_countMemWarn				= 0;
int NotifyResponder::_countActive				= 0;
int NotifyResponder::_countLostActive			= 0;
int NotifyResponder::_countEnterBackground		= 0;
int NotifyResponder::_countEnterForeground		= 0;
//---------------------------------------------------------------------
NotifyResponder *NotifyResponder::Get()
{
	if ( ! _sInstance )
		_sInstance = new NotifyResponder();

	return _sInstance;
}
//---------------------------------------------------------------------
void NotifyResponder::Destroy()
{
	if ( _sInstance )
		delete _sInstance;

	_sInstance = NULL;
}
//---------------------------------------------------------------------
NotifyResponder::NotifyResponder()
{
//	s3eDeviceRegister( S3E_DEVICE_NOTIFY_PAUSE, &NotifyLostActive, NULL );
//	s3eDeviceRegister( S3E_DEVICE_NOTIFY_UNPAUSE, &NotifyBecomeActive, NULL );
//
//	SysNotificationRegister( S3E_SYSNOTIFICATIONS_MEMWARNING,		&NotifyMemoryWarning );
//    
//    
	//SysNotificationRegister( S3E_SYSNOTIFICATIONS_BECOME_ACTIVE,		&NotifyBecomeActive );
	//SysNotificationRegister( S3E_SYSNOTIFICATIONS_LOST_ACTIVE,		&NotifyLostActive );
	//SysNotificationRegister( S3E_SYSNOTIFICATIONS_ENTER_BACKGROUND,	&NotifyEnterBackground );
	//SysNotificationRegister( S3E_SYSNOTIFICATIONS_ENTER_FOREGROUND,	&NotifyEnterForeground );
	//SysNotificationRegister( S3E_SYSNOTIFICATIONS_TERMINATE,			&NotifyTerminate );
}
//---------------------------------------------------------------------
NotifyResponder::~NotifyResponder()
{
//	s3eDeviceUnRegister( S3E_DEVICE_NOTIFY_PAUSE, &NotifyLostActive );
//	s3eDeviceUnRegister( S3E_DEVICE_NOTIFY_UNPAUSE, &NotifyBecomeActive );
//
//	SysNotificationUnRegister( S3E_SYSNOTIFICATIONS_MEMWARNING,			&NotifyMemoryWarning );
//    
//    
	//SysNotificationUnRegister( S3E_SYSNOTIFICATIONS_BECOME_ACTIVE,	&NotifyBecomeActive );
	//SysNotificationUnRegister( S3E_SYSNOTIFICATIONS_LOST_ACTIVE,		&NotifyLostActive );
	//SysNotificationUnRegister( S3E_SYSNOTIFICATIONS_ENTER_BACKGROUND,	&NotifyEnterBackground );
	//SysNotificationUnRegister( S3E_SYSNOTIFICATIONS_ENTER_FOREGROUND,	&NotifyEnterForeground );
	//SysNotificationUnRegister( S3E_SYSNOTIFICATIONS_TERMINATE,		&NotifyTerminate );
}
//---------------------------------------------------------------------
int32 NotifyResponder::NotifyMemoryWarning( void* systemData, void* userData )
{
	Preloader::Get()->FreeResources();
	_countMemWarn++;

#ifdef BUILD_TEST
	stringstream ss;
	ss << "Warning received: " << _countMemWarn;
//	DebugLogger::Get( DebugLogger::LogNotifications )->Write( ss.str().c_str() );
#endif

	return 0;
}
//---------------------------------------------------------------------
int32 NotifyResponder::NotifyBecomeActive( void* systemData, void* userData )
{
	Level *level;
	_countActive++;

	CCDirector::sharedDirector()->resume();
	SoundEngine::Get()->ResumeBackgroundMusic();

	level = Game::Get()->GetRuningLevel();
	if ( level )
		level->LevelRun();

#ifdef BUILD_TEST
	stringstream ss;
	ss << "Active received: " << _countActive;
//	DebugLogger::Get( DebugLogger::LogNotifications )->Write( ss.str().c_str() );
#endif

	return 0;
}
//---------------------------------------------------------------------
int32 NotifyResponder::NotifyLostActive( void* systemData, void* userData )
{
	Level *level;
	_countLostActive++;
	
	level = Game::Get()->GetRuningLevel();
	if ( level )
		level->LevelPause();

	SoundEngine::Get()->PauseBackgroundMusic();
	CCDirector::sharedDirector()->pause();

#ifdef BUILD_TEST
	stringstream ss;
	ss << "Lost active received: " << _countLostActive;
	//DebugLogger::Get( DebugLogger::LogNotifications )->Write( ss.str().c_str() );
#endif

	return 0;
}
//---------------------------------------------------------------------
int32 NotifyResponder::NotifyEnterBackground( void* systemData, void* userData )
{
	_countEnterBackground++;

#ifdef BUILD_TEST
	stringstream ss;
	ss << "Enter background received: " << _countEnterBackground;
	//DebugLogger::Get( DebugLogger::LogNotifications )->Write( ss.str().c_str() );
#endif

	return 0;
}
//---------------------------------------------------------------------
int32 NotifyResponder::NotifyEnterForeground( void* systemData, void* userData )
{
	_countEnterForeground++;

#ifdef BUILD_TEST
	stringstream ss;
	ss << "Enter foreground received: " << _countEnterForeground;
	//DebugLogger::Get( DebugLogger::LogNotifications )->Write( ss.str().c_str() );
#endif

	return 0;
}
//---------------------------------------------------------------------
int32 NotifyResponder::NotifyTerminate( void* systemData, void* userData )
{
#ifdef BUILD_TEST
	//DebugLogger::Get( DebugLogger::LogNotifications )->Write( "Terminate received" );
#endif

	return 0;
}
//---------------------------------------------------------------------
void NotifyResponder::TestAll()
{
//	SysNotificationTest( S3E_SYSNOTIFICATIONS_MEMWARNING );
//	SysNotificationTest( S3E_SYSNOTIFICATIONS_BECOME_ACTIVE );
//	SysNotificationTest( S3E_SYSNOTIFICATIONS_LOST_ACTIVE );
//	SysNotificationTest( S3E_SYSNOTIFICATIONS_ENTER_BACKGROUND );
//	SysNotificationTest( S3E_SYSNOTIFICATIONS_ENTER_FOREGROUND );
//	SysNotificationTest( S3E_SYSNOTIFICATIONS_TERMINATE );
}



