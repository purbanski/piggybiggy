#ifndef __PUSWING_H__
#define __PUSWING_H__

#include "puBlock.h"
#include "puBox.h"
#include "puCircle.h"
#include "puEmptyBlock.h"

//---------------------------------------------------------------------------------------
class puSwing : public puWallBox< 320, 20 > 
{
public:
	puSwing();
	virtual ~puSwing();
	puBlock* GetFragileBox() { return &_fragile; }
	virtual void CreateJoints();
	virtual void DestroyJoints();

private:
	puCircleWall< 48 >		_swingHead;
	puFragileBox<96,96>		_fragile;
	puWoodBox<160,20>		_groundArm;
	puWoodBox<160,20>		_swingArm;
	puEmptyBlock			_swingNail;

	b2RevoluteJoint			*_jointSwingArmSwingHead;
	b2RevoluteJoint			*_jointSwing;
};
//---------------------------------------------------------------------------------------
class puSwingNoFloor : public puScrew< 10 > 
{
public:
	puSwingNoFloor();
	virtual ~puSwingNoFloor();
	virtual void CreateJoints();
	virtual void DestroyJoints();

private:
	puCircleWall< 48 >		_swingHead;
	puWoodBox<160,20>		_swingArm;

	b2RevoluteJoint			*_jointSwingArmSwingHead;
	b2RevoluteJoint			*_jointSwing;
};
//---------------------------------------------------------------------------------------
#endif
