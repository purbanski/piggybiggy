#include "puLiftDoor.h"
#include <Box2D/Box2D.h>
#include "Levels/World.h"
#include "GameConfig.h"
#include "Levels/Level.h"

puLiftDoor::puLiftDoor( float xpos, float ypos)
	: puBlock()
{
	//Construct( b2Vec2( 2.4f, 2.4f ), 3.0f );
	Construct( xpos, ypos );
}
//-----------------------------------------------------------------------------------------------------
void puLiftDoor::Construct( float x, float y )
{
	//_sprite = CCSprite::spriteWithFile("Blocks/Slider48Platform.png");

	b2Filter filter;
	filter.groupIndex = -1;

	b2PolygonShape box;
	box.SetAsBox( 2.4f, 2.4f );

	b2FixtureDef fixtureDef;
	fixtureDef.shape = &box;
	fixtureDef.density = 0.0f;
	fixtureDef.friction = 0.5f; // tarcie 0 - nie ma 1- max
	fixtureDef.restitution = 0.1f; // bouncy 0 - nie ma 1 max;
	fixtureDef.filter = filter;

	_body->CreateFixture( &fixtureDef );
	_body->SetType( b2_staticBody );
	SetPosition( x, y );
	
	
	// Arm - connected to ground
	//_armGround.SetSprite( CCSprite::spriteWithFile("Blocks/Slider48ArmGround.png" ), Level::eSpritesAbove );
	_armGround.SetPosition( x, y + 3.5f );
	_armGround.GetBody()->SetType( b2_dynamicBody );
	_armGround.GetBody()->GetFixtureList()->SetFilterData( filter );

	// Arm - ground joint
	{
		b2RevoluteJointDef rjd;
		rjd.Initialize( _body, _armGround.GetBody(), b2Vec2( x + 0.0f, y + 1.5f ));
		rjd.motorSpeed = -1.0f * b2_pi;
		rjd.maxMotorTorque = 100000.0f;
		rjd.enableMotor = true;

		rjd.lowerAngle =  0.5f * b2_pi; 
		rjd.upperAngle = -0.5f * b2_pi; 
		rjd.enableLimit = true;

		_motorJoint = (b2RevoluteJoint*)_world->CreateJoint(&rjd);
	}
	
	
	// Arm connected to pusher
	_armPusher.GetBody()->GetFixtureList()->SetFilterData( filter );
	_armPusher.GetBody()->SetType( b2_dynamicBody );
	//_armPusher.SetSprite( CCSprite::spriteWithFile("Blocks/Slider48ArmPusher.png" ), Level::eSpritesAbove );
	_armPusher.SetPosition( x, y + 9.5f );

	// Arm - arm joint
	{
		b2RevoluteJointDef rjd;
		rjd.Initialize( _armGround.GetBody(), _armPusher.GetBody(), b2Vec2( x + 0.0f, y + 5.5f ));
		rjd.enableMotor = false;
		_world->CreateJoint(&rjd);
	}

	//// Pusher
	_pusher.SetPosition( x, y + 13.5f );
	_pusher.GetBody()->SetType( b2_dynamicBody );
	_pusher.SetSprite( CCSprite::spriteWithFile( Skins::GetSkinName( "Images/Blocks/LiftDoor.png" )  ), GameTypes::eSpritesAboveX3);

	// Pusher - ground joint
	{
		b2RevoluteJointDef rjd;
		rjd.Initialize( _armPusher.GetBody(), _pusher.GetBody(), b2Vec2(x+0.0f, y+13.5f));
		_world->CreateJoint(&rjd);

		b2PrismaticJointDef pjd;
		pjd.Initialize( _body, _pusher.GetBody(), b2Vec2( x + 0.0f, y + 13.5f ), b2Vec2( 0.0f, 1 ));

		pjd.maxMotorForce = 1000.0f;
		pjd.enableMotor = true;

        (void)_world->CreateJoint(&pjd);
	}
}
//-----------------------------------------------------------------------------------------------------
puLiftDoor::~puLiftDoor()
{
}
//------------------------------------------------------------------------------------------------------
void puLiftDoor::Activate( void *ptr)
{
	_motorJoint->SetMotorSpeed ( -1 * _motorJoint->GetMotorSpeed() );
}
//------------------------------------------------------------------------------------------------------
void puLiftDoor::Deactivate( void *ptr )
{
	_motorJoint->SetMotorSpeed ( -1 * _motorJoint->GetMotorSpeed() );
}
