#ifndef __PUMAGNET_H__
#define __PUMAGNET_H__

#include "puBlockWithRange.h"
#include "puBlockExtras.h"

//---------------------------------------------------------------------------------------------------
// Magnet
//---------------------------------------------------------------------------------------------------
class puMagnet_ : public puBlockWithRange_
{
public:
	puMagnet_() : puBlockWithRange_( 4.4f ) { Construct(); }
	puMagnet_( float radius ) : puBlockWithRange_( radius ) { Construct(); }

	virtual ~puMagnet_(){};
	virtual void Step(); 

	void	SetMagnetImpulse( float value ) { _magnetImpulse = value; }
	float	GetMagnetImpulse(){ return _magnetImpulse; }

protected:
	void Construct();
	virtual	void Magnetize(); 

	b2Vec2 GetImpulseStrength( puBlock *block );

protected:
	float _magnetImpulse;
};
//---------------------------------------------------------------------------------------------------
template <int TRadius = 44, BlockScaleType T = eBlockNormal>
class puMagnet : public puMagnet_
{
public:
	puMagnet() : puMagnet_( (float) TRadius / 10.0f / float( T ) ) {}
};
//---------------------------------------------------------------------------------------------------


//---------------------------------------------------------------------------------------------------
// Magnet Minus
//---------------------------------------------------------------------------------------------------
class puMagnetMinus_ : public puMagnet_
{
public:
	puMagnetMinus_() : puMagnet_( 4.4f ) { Construct(); }
	puMagnetMinus_( float radius ) : puMagnet_( radius ) { Construct(); }

	virtual ~puMagnetMinus_(){};

protected:
	void Construct();
	virtual void Magnetize(); 
};
//---------------------------------------------------------------------------------------------------
template <int TRadius = 44, BlockScaleType T = eBlockNormal>
class puMagnetMinus : public puMagnetMinus_
{
public:
	puMagnetMinus() : puMagnetMinus_( (float) TRadius / 10.0f / float( T ) ) {}
};
//---------------------------------------------------------------------------------------------------


//---------------------------------------------------------------------------------------------------
// Magnet Plus
//---------------------------------------------------------------------------------------------------
class puMagnetPlus_ : public puMagnet_
{
public:
	puMagnetPlus_() : puMagnet_( 4.4f ) { Construct(); }
	puMagnetPlus_( float radius ) : puMagnet_( radius ) { Construct(); }

	virtual ~puMagnetPlus_(){};

protected:
	void Construct();
	virtual void Magnetize(); 
};
//---------------------------------------------------------------------------------------------------
template <int TRadius = 44, BlockScaleType T = eBlockNormal>
class puMagnetPlus : public puMagnetPlus_
{
public:
	puMagnetPlus() : puMagnetPlus_( (float) TRadius / 10.0f / float( T ) ) {}
};
//---------------------------------------------------------------------------------------------------


#endif
