#ifndef __PUBANK_H__
#define __PUBANK_H__

#include "CommonDefs.h"
#include "puBlock.h"
#include "Blocks/puBox.h"
#include "Blocks/puCircle.h"

class puBankBorderWall : public puEmptyBlock
{
public:
	puBankBorderWall();

private:
};
//----------------------------------------------------------------------------------------
class puBank : public puWallBox<475, 20, eBlockScaled >
{
public:
	const static int COINS_COUNT = 16;
	static void OpenBankFinish( void *data );

	puBank();
	~puBank();
	void OpenBankInit();

private:
	bool _bankOpen;

	puBankBorderWall				_borderWall;
	puWallBox<20,280,eBlockScaled>	_wall1;
	puWallBox<20,280,eBlockScaled>	_wall2;
	puWallBox<20,280,eBlockScaled>	_wall3;
	puWallBox<255, 254,eBlockScaled>	_bankSave1;

	puPiggyBank< 50 >			_piggyBank;
	BlockContainer				_coins;
};
//----------------------------------------------------------------------------------------
#endif
