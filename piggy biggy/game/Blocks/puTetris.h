#ifndef __PUTETRISSQUARE_H__
#define __PUTETRISSQUARE_H__

#include <Box2D/Box2D.h>
#include "Blocks/puBlock.h"
#include "Blocks/puBox.h"
#include "GameTypes.h"

using namespace GameTypes;

//-----------------------------------------------------------------------------------------
class puTetrisBase : public puBlock
{
public:
	const static float _sBlockSpace;
	const static float _sBlockHalfSpace;
	const static float _sBlockSize;
	const static float _sBlockHalfSize;
	const static float _sBlockSizeTotal;

	void StartFalling();
	void SetGround( puBlock *block ) { _ground = block; }
	void SetRotation( TetrisBlockRotation rotation );
	TetrisBlockRotation GetTetrisRotation();

	void SetStartPos( float x, float y ){ _startX = x; _startY = y; }
	void CleanUpShadow();
	void DestroyFragile();
	

protected:
	puTetrisBase();
	void SetDefaults();
	void SetPrisJoint();
	void CreateCube( float xpos, float ypos );
	void CleanUpBlock();
	virtual void DestroyAnimPreload();

	virtual void Construct0(){};
	virtual void Construct90(){ Construct0(); };
	virtual void Construct180(){ Construct0(); };
	virtual void Construct270(){ Construct0(); };

	puBlock		*_ground;
	float		_startX;
	float		_startY;
	TetrisBlockRotation	_tetrisBlockRotation;
};
//---------------------------------------------------------------------------------
class puTetrisSquare : public puTetrisBase
{
public:
	puTetrisSquare();
	virtual void SetRotation( float angle ){};

protected:
	virtual void Construct0();
};
//---------------------------------------------------------------------------------
class puTetrisS : public puTetrisBase
{
public:
	puTetrisS();

protected:
	virtual void Construct0();
	virtual void Construct90();
	virtual void Construct180(){ Construct0(); };
	virtual void Construct270(){ Construct90(); };
};
//---------------------------------------------------------------------------------
// S mirror
class puTetrisSM : public puTetrisBase
{
public:
	puTetrisSM();

protected:
	virtual void Construct0();
	virtual void Construct90();
	virtual void Construct180(){ Construct0(); };
	virtual void Construct270(){ Construct90(); };
};
//---------------------------------------------------------------------------------
class puTetrisT :public puTetrisBase
{
public:
	puTetrisT();
	
protected:
	virtual void Construct0();
	virtual void Construct90();
	virtual void Construct180();
	virtual void Construct270();
};
//---------------------------------------------------------------------------------
class puTetrisL : public puTetrisBase
{
public:
	puTetrisL();
	
protected:
	virtual void Construct0();
	virtual void Construct90();
	virtual void Construct180();
	virtual void Construct270();
};
//---------------------------------------------------------------------------------
class puTetrisLM : public puTetrisBase
{
public:
	puTetrisLM();
	
protected:
	virtual void Construct0();
	virtual void Construct90();
	virtual void Construct180();
	virtual void Construct270();
};
//---------------------------------------------------------------------------------
class puTetrisI : public puTetrisBase
{
public:
	puTetrisI();

protected:
	virtual void Construct0();
	virtual void Construct90();
	virtual void Construct180(){ Construct0(); };
	virtual void Construct270(){ Construct90(); };

};
//---------------------------------------------------------------------------------

#endif
