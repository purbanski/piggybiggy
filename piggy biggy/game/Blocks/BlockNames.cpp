#include "unDebug.h"
#include "BlockNames.h"
//-------------------------------------------------------------------------

//-------------------------------------------------------------------------
BlockName::BlockName()
{
	memset( _name, 0, NameSize );
}
//-------------------------------------------------------------------------
BlockName::BlockName( const char* name )
{
	if ( strlen(name) <= NameSize )
		strcpy( _name, name );
	else
	{
		unAssertMsg( BlockName, false,("Buffer overflow %s", name ));
	}
}
//-------------------------------------------------------------------------
const char * BlockName::GetName() const
{
	return _name;
}


//-------------------------------------------------------------------------
BlockNames* BlockNames::_sInstance = NULL;
//-------------------------------------------------------------------------
BlockNames* BlockNames::Get()
{
	if ( ! _sInstance )
	{
		_sInstance	= new BlockNames();
	}
	return _sInstance;
}
//-------------------------------------------------------------------------
void BlockNames::Destroy()
{
	if ( _sInstance )
		delete _sInstance;

	_sInstance = NULL;
}
//-------------------------------------------------------------------------

//-------------------------------------------------------------------------
BlockNames::BlockNames()
{
	_blockNames[ eBlockCoin ]				= BlockName("Coin");
	_blockNames[ eBlockCoinSilver ]			= BlockName("CoinSilver");
	_blockNames[ eBlockCoinStatic ]			= BlockName("CoinStatic");
	_blockNames[ eBlockPiggyBank ]			= BlockName("PiggyBank");
	_blockNames[ eBlockPiggyBankRolled ]	= BlockName("PiggyBankRolled");
	_blockNames[ eBlockPiggyBankStatic ]	= BlockName("PiggyBankStatic");
	_blockNames[ eBlockThief ]				= BlockName("Thief");
	_blockNames[ eBlockThiefRolled ]		= BlockName("ThiefRolled");
	_blockNames[ eBlockThiefStatic ]		= BlockName("ThiefStatic");
	_blockNames[ eBlockCop ]				= BlockName("Cop");
	_blockNames[ eBlockSpitter ]			= BlockName("Spitter");
	_blockNames[ eBlockSpitterExplosive ]	= BlockName("SpitterExplosive");
	_blockNames[ eBlockButton ]				= BlockName("Button");
	
	_blockNames[ eBlockCircleWall ]			= BlockName("CircleWall");
	_blockNames[ eBlockCircleFragile ]		= BlockName("CircleFragile");
	_blockNames[ eBlockCircleFragileStatic ]= BlockName("CircleFragileStatic");
	_blockNames[ eBlockBoxerGlove ]			= BlockName("BoxerGlove");
	_blockNames[ eBlockTyre ]				= BlockName("TyreA");

	_blockNames[ eBlockBoxWall ]			= BlockName("WallBox");
	_blockNames[ eBlockBoxFragile ]			= BlockName("FragileBox");
	_blockNames[ eBlockBoxFragileStatic ]	= BlockName("FragileBoxStatic");
	_blockNames[ eBlockBoxWood ]			= BlockName("WoodBox");
	_blockNames[ eBlockBoxWood2 ]			= BlockName("Wood2Box");
	_blockNames[ eBlockBoxLiftUp ]			= BlockName("LiftPlatform");
	_blockNames[ eBlockBoxBounce ]			= BlockName("BouncerBox");
	_blockNames[ eBlockBoxIron ]			= BlockName("IronBox");

	_blockNames[ eBlockPolygonFragileCustom1	] = BlockName("PolygonFragile_Custom1");
	_blockNames[ eBlockPolygonFragileCustom2	] = BlockName("PolygonFragile_Custom2");
	_blockNames[ eBlockPolygonCoinCustom1		] = BlockName("PolygonCoin_Custom1");

	_blockNames[ eBlockBoxIron					]	= BlockName("IronBox");
	_blockNames[ eBlockBoxIron					]	= BlockName("IronBox");
	
	_blockNames[ eBlockScrew					]	= BlockName("Screw");

	_blockNames[ eBlockCashCow					]	= BlockName("CashCow");
	_blockNames[ eBlockCashCowStatic			]	= BlockName("CashCowStatic");
	_blockNames[ eBlockPrison					]	= BlockName("Prison");
	_blockNames[ eBlockMotherPiggy				]	= BlockName("MotherPiggy");
	_blockNames[ eBlockMotherPiggyStatic		]	= BlockName("MotherPiggyStatic");

	_blockNames[ eBlockBomb						]	= BlockName("Bomb");
	_blockNames[ eBlockBombStatic				]	= BlockName("BombStatic");
	_blockNames[ eBlockMagnetBomb				]	= BlockName("MagnetBomb");
	_blockNames[ eBlockMagnetBombPlus			]	= BlockName("MagnetBombPlus");
	_blockNames[ eBlockMagnetBombMinus			]	= BlockName("MagnetBombMinus");

	_blockNames[ eBlockPolygonFragileCustom1	]	= BlockName("PolygonFragile_Custom1");
	_blockNames[ eBlockPolygonFragileCustom2	]	= BlockName("PolygonFragile_Custom2");
	_blockNames[ eBlockPolygonCoinCustom1		]	= BlockName("PolygonCoin_Custom1");
	_blockNames[ eBlockGunSensor				]	= BlockName("GunSensor");
	_blockNames[ eBlockBullet					]	= BlockName("Bullet");
	_blockNames[ eBlockReel						]	= BlockName("Reel");
	_blockNames[ eBlockChainBit					]	= BlockName("ChainBit");
	_blockNames[ eBlockVehical					]	= BlockName("Vehical");
	_blockNames[ eBlockBankLockBars				]	= BlockName("BankLockBars");
	_blockNames[ eBlockBankSaveWheel			]	= BlockName("BankSaveWheel");
	_blockNames[ eBlockVehicalControls			]	= BlockName("VehicalControls");
	_blockNames[ eBlockBoxerHand				]	= BlockName("BoxerHand");

	_blockNames[ eBlockTetrisS					]	= BlockName("TetrisS");
	_blockNames[ eBlockTetrisSM					]	= BlockName("TetrisSM");
	_blockNames[ eBlockTetrisT					]	= BlockName("TetrisT");

	_blockNames[ eBlockTetrisL					]	= BlockName("TetrisL");
	_blockNames[ eBlockTetrisLM					]	= BlockName("TetrisLM");
	_blockNames[ eBlockTetrisI					]	= BlockName("TetrisI");
	_blockNames[ eBlockTetrisSquare				]	= BlockName("TetrisSquare");
	_blockNames[ eBlockPolygonWall_Custom1		]	= BlockName("PolygonWall_Custom1");
	_blockNames[ eBlockPolygonWall_Custom2		]	= BlockName("PolygonWall_Custom2");

	_blockNames[ eBlockBombRange				]	= BlockName("none");
	_blockNames[ eBlockBorder					]	= BlockName("none");
	
	//_blockNames[ eBlockNew4						]	= BlockName("PolygonWall_Custom1");
	

}
//-------------------------------------------------------------------------
BlockNames::~BlockNames()
{
	_blockNames.clear();
}
//-------------------------------------------------------------------------
const char* BlockNames::GetName( BlockEnum blockEnum )
{
	BlockNameMap::const_iterator it;
	it = _blockNames.find( blockEnum );
	
	if ( it != _blockNames.end() )
		return (*it).second.GetName();

	unAssertMsg( BlockName, false, ( "Name of block: %d. Returning Bomb", blockEnum ));
	return("Bomb");

}
//-------------------------------------------------------------------------
