#ifndef __PUPOLYGON_H__
#define __PUPOLYGON_H__

#include "GameConfig.h"
#include "puBlock.h"


//-----------------------------------------------------------------------------------------
// Polygon
//-----------------------------------------------------------------------------------------
class puPolygon : public puBlock
{
public:
	void Construct( const b2Vec2* vertices, int count );
	void Construct( const b2Vec2* vertex, ... );

	void CenterPolygon();
	void LoadDefaultSprite();

	b2Vec2 GetVertex( int index ) { return _vertices[ index ] ; } // only for editor
	int GetVerticesCount() { return _verticesCount; }	//only for editor

protected:
	void ConstructCommon();
	virtual void doConstruct() = 0;

	puPolygon();
	void CopyVertices( const b2Vec2 *vertices );	// only for editor
	
	b2Vec2	_vertices[ Config::PolygonMaxVertex ]; // this is really only for editor
	int		_verticesCount;
};


//-----------------------------------------------------------------------------------------
// Polygon Fragile
//-----------------------------------------------------------------------------------------
class puPolygonFragile : public puPolygon
{
public:
	puPolygonFragile(){};
	virtual void doConstruct();

protected:
	void PreloadAnimAndSound();
};


//-----------------------------------------------------------------------------------------
// Polygon Fragile Static
//-----------------------------------------------------------------------------------------
class puPolygonFragileStatic : public puPolygon
{
public:
	puPolygonFragileStatic(){};
	virtual void doConstruct();
};


//-----------------------------------------------------------------------------------------
// Polygon Wall
//-----------------------------------------------------------------------------------------
class puPolygonWall : public puPolygon
{
public:
	puPolygonWall(){};
	virtual void doConstruct();
};


//-----------------------------------------------------------------------------------------
// Polygon Iron
//-----------------------------------------------------------------------------------------
class puPolygonIron : public puPolygon
{
public:
	puPolygonIron(){};
	virtual void doConstruct();
};


//-----------------------------------------------------------------------------------------
// Polygon Stone
//-----------------------------------------------------------------------------------------
class puPolygonStone : public puPolygon
{
public:
	puPolygonStone(){};
	virtual void doConstruct();
};


//-----------------------------------------------------------------------------------------
// Polygon Wood
//-----------------------------------------------------------------------------------------
class puPolygonWood : public puPolygon
{
public:
	puPolygonWood(){};
	virtual void doConstruct();
};


//-----------------------------------------------------------------------------------------
// Polygon Bouncer
//-----------------------------------------------------------------------------------------
class puPolygonBouncer : public puPolygon
{
public:
	puPolygonBouncer(){};
	virtual void doConstruct();
};


//-----------------------------------------------------------------------------------------
// Polygon Ice
//-----------------------------------------------------------------------------------------
class puPolygonIce : public puPolygon
{
public:
	puPolygonIce(){};
	virtual void doConstruct();
};


#endif
