#ifndef __PUBLOCKEXTRAS_H__
#define __PUBLOCKEXTRAS_H__

#include "puCircle.h"

//---------------------------------------------------------------------------------------------------
// Block Touch
//---------------------------------------------------------------------------------------------------
class puBlockTouch : public  puCircleBase
{
public:
	puBlockTouch();
	virtual void Activate( void *ptr ); 
	virtual void Destroy();
};

#endif
