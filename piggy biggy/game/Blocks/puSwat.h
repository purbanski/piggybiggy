#ifndef __PUSWAT_H__
#define __PUSWAT_H__

#include <Box2D/Box2D.h>
#include "puBlock.h"

class puSwat : public puBlock
{
public:
	puSwat();
	virtual ~puSwat();

private:
	void Construct();
};

#endif
