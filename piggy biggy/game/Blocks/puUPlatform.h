#ifndef __PUUPLATFORM_H__
#define __PUUPLATFORM_H__

#include <Box2D/Box2D.h>
#include "puBlock.h"
#include "puBox.h"

class puUPlatform : public puWallBox<8,1>
{
public:
	puUPlatform();
	~puUPlatform();

private:
	puWallBox<5,1>	_boxLeft;
	puWallBox<5,1>	_boxRight;
};


#endif
