#ifndef __PUL_H__
#define __PUL_H__

#include "puBlock.h"
#include "puBox.h"
#include "puCircle.h"

//---------------------------------------------------------------------------------------------
// class puL_
//---------------------------------------------------------------------------------------------
class puL_ : public puWoodBox_
{
public:
	puL_( float width, float height );

	virtual void CreateJoints();
	virtual void DestroyJoints();

protected:
	puWoodBox_		_wood1;
	b2RevoluteJoint	*_joint1;
};

//---------------------------------------------------------------------------------------------
// class puLScrewed_
//---------------------------------------------------------------------------------------------
class puLScrewed_ : public puL_
{
public:
	puLScrewed_( float width, float height );

	virtual void CreateJoints();
	virtual void DestroyJoints();

protected:
	puScrew<>	_screw1;
	b2RevoluteJoint	*_joint2;
};

//---------------------------------------------------------------------------------------------
// tempalte puL
//---------------------------------------------------------------------------------------------
template<int TWidth, int THeight, BlockScaleType TScale = eBlockNormal>
class puL : public puWoodBox<TWidth,THeight,TScale>
{
public:
	puL();

	virtual void CreateJoints();
	virtual void DestroyJoints();

protected:
	puWoodBox<TWidth,THeight,TScale> _wood1;
	b2RevoluteJoint	*_joint1;
};
//---------------------------------------------------------------------------------------------
template<int TWidth, int THeight, BlockScaleType TScale>
puL<TWidth,THeight,TScale>::puL()
{
	// _wood1
	_wood1.GetBody()->GetFixtureList()->SetDensity( 15.0f );
	_wood1.GetBody()->GetFixtureList()->SetFriction( 0.5f );
	_wood1.GetBody()->GetFixtureList()->SetRestitution( 0.1f );
	_wood1.SetBodyType( b2_dynamicBody );

	// SetNodeChain
	puWoodBox<TWidth,THeight,TScale>::SetNodeChain( &_wood1, NULL );

	// SetDeltaXY
	_wood1.SetDeltaXY(( TWidth - THeight ) / 2.0f / TScale, ( -TWidth + THeight ) / 2.0f / TScale);

	// Main block (Wall)
	this->GetBody()->GetFixtureList()->SetDensity( 15.0f );
	this->GetBody()->GetFixtureList()->SetFriction( 0.5f );
	this->GetBody()->GetFixtureList()->SetRestitution( 0.1f );
	this->SetBodyType( b2_dynamicBody );
	this->SetDeltaRotation( -90 );

	this->SetPosition( 0, 0 );
	CreateJoints();
}
//---------------------------------------------------------------------------------------------
template<int TWidth, int THeight, BlockScaleType TScale>
void puL<TWidth, THeight, TScale>::CreateJoints()
{
	float anchorX;
	float anchorY;

	anchorX = this->GetPosition().x;
	anchorY = this->GetPosition().y + this->_flipY * ( this->_height - this->_width ) / 2 ;

	b2RevoluteJointDef jointDef1;
	jointDef1.Initialize( this->GetBody(), this->_wood1.GetBody(), b2Vec2( anchorX, anchorY ));
	_joint1 = (b2RevoluteJoint *) this->_world->CreateJoint( &jointDef1 );

	_joint1->SetLimits( 0.0f, 0.0f );
	_joint1->EnableLimit( true );
	_joint1->EnableMotor( false );
	_joint1->SetMotorSpeed( 0.0f );
	_joint1->SetMaxMotorTorque( 0.0f );

}
//---------------------------------------------------------------------------------------------
template<int TWidth, int THeight, BlockScaleType TScale>
void puL<TWidth, THeight, TScale>::DestroyJoints()
{
	this->_world->DestroyJoint( _joint1 );
	_joint1 = NULL;
}


//---------------------------------------------------------------------------------------------
// tempalte puLScrewed
//---------------------------------------------------------------------------------------------
template<int TWidth, int THeight, BlockScaleType TScale = eBlockNormal>
class puLScrewed : public puL<TWidth,THeight,TScale>
{
public:
	puLScrewed();

	virtual void CreateJoints();
	virtual void DestroyJoints();

protected:
	puScrew<>			_screw1;
	b2RevoluteJoint	*_joint2;

};
//---------------------------------------------------------------------------------------------
template<int TWidth, int THeight, BlockScaleType TScale>
puLScrewed<TWidth,THeight,TScale>::puLScrewed()
{
	// SetNodeChain
	this->SetNodeChain( &_screw1, NULL );

	// SetDeltaXY
	_screw1.SetDeltaXY( 0, ( TWidth - THeight ) / 2.0f / TScale );

	this->SetPosition( 0, 0 );

	// we need to destroy puL joints
	// as they got created as a part of puL constructor
	puL<TWidth,THeight,TScale>::DestroyJoints();
	CreateJoints();
}
//---------------------------------------------------------------------------------------------
template<int TWidth, int THeight, BlockScaleType TScale>
void puLScrewed<TWidth, THeight, TScale>::CreateJoints()
{
	puL<TWidth,THeight,TScale>::CreateJoints();
	
	float anchorX;
	float anchorY;

	anchorX = _screw1.GetPosition().x ;
	anchorY = _screw1.GetPosition().y ;

	b2RevoluteJointDef jointDef1;
	jointDef1.Initialize( this->GetBody(), _screw1.GetBody(), b2Vec2( anchorX, anchorY ));
	_joint2 = (b2RevoluteJoint *) this->_world->CreateJoint( &jointDef1 );

	_joint2->SetLimits( 0.0f, 0.0f );
	_joint2->EnableLimit( false );
	_joint2->EnableMotor( false );
	_joint2->SetMotorSpeed( 0.0f );
	_joint2->SetMaxMotorTorque( 0.0f );
}
//---------------------------------------------------------------------------------------------
template<int TWidth, int THeight, BlockScaleType TScale>
void puLScrewed<TWidth, THeight, TScale>::DestroyJoints()
{
	puL<TWidth,THeight,TScale>::DestroyJoints();

	this->_world->DestroyJoint( _joint2 );
	_joint2 = NULL;
}
//---------------------------------------------------------------------------------------------



#endif
