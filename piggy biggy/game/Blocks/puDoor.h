#ifndef __PUDOOR_H__
#define __PUDOOR_H__

#include "AllBlocks.h"

//--------------------------------------------------------------------------
// puDoor_1
//--------------------------------------------------------------------------
class puDoor_1 : public puBlock
{
public:
	puDoor_1();
	~puDoor_1();

	virtual void CreateJoints();
	virtual void DestroyJoints();

	virtual void Activate( void *ptr = NULL );
	virtual void Deactivate( void *ptr = NULL );

private:
	puWallBox<20,20,eBlockScaled>	_wallBox1;
	b2PrismaticJoint				*_joint1;
	LiftDoorSprite*					_liftDoorSprite;
};


#endif
