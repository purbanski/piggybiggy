#ifndef __PUAIRBUBBLE_H__
#define __PUAIRBUBBLE_H__

#include "puCircle.h"

using namespace std;


//---------------------------------------------------------------------------------------------------
// Air Bubble
//---------------------------------------------------------------------------------------------------
class puAirBubble_ : public puCircleBase
{
public:
	puAirBubble_() : puCircleBase( 4.4f ) { Construct( 4.4f ); }
	puAirBubble_( float radius ) : puCircleBase( radius ) { Construct( radius ); }

	void Colide( puBlock *block );
	void PutInBubble( puBlock *block );
	bool IsBusy();

	virtual void Destroy();

	virtual ~puAirBubble_(){};
	virtual void Step();

protected:
	typedef enum
	{
		eActionScale = 1
	} ActionTag;

	virtual void Construct( float radius );
	
	void CreateFixture( float radius );
	void CleanFixture();
	void UpdateSprite();
	void PlayFlatingAnim();

	void ColideWithBlock( puBlock *block );
	void ColideWithBubble( puBlock *block );

	float	_baseScaleX;
	float	_baseScaleY;

	float	_lastScaleX;
	float	_lastScaleY;

	puBlock	*_insideBlock;
};
//---------------------------------------------------------------------------------------------------
template <int TRadius = 44, BlockScaleType T = eBlockNormal>
class puAirBubble : public puAirBubble_
{
public:
	puAirBubble() : puAirBubble_( (float) TRadius / 10.0f / float( T ) ) {}
};
//---------------------------------------------------------------------------------------------------

#endif
