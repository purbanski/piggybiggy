#ifndef __PUBLOCKSSETTINGS_H__
#define __PUBLOCKSSETTINGS_H__

namespace pu 
{
	namespace blocks
	{
		namespace settings
		{

			static const float IronDensity = 10.0f;
			static const float IronRestitution = 0.2f;
			static const float IronFriction = 0.6f;


			static const float IronStaticDensity = IronDensity;
			static const float IronStaticRestitution = IronRestitution;
			static const float IronStaticFriction = IronFriction;


			static const float WoodDensity = 5.5f;
			static const float WoodRestitution = 0.2f;
			static const float WoodFriction = 0.5f;

			static const float ButtonDensity = 5.5f;
			static const float ButtonRestitution = 0.2f;
			static const float ButtonFriction = 0.5f;

			static const float LiftPlatformDensity = 5.5f;
			static const float LiftPlatformRestitution = 0.2f;
			static const float LiftPlatformFriction = 0.7f;


			static const float WallDensity = 8.0f;
			static const float WallRestitution = 0.1f;
			static const float WallFriction = 0.8f;


			static const float StoneDensity = 9.0f;
			static const float StoneRestitution = 0.1f;
			static const float StoneFriction = 0.8f;


			static const float FragileDensity = 9.0f;
			static const float FragileRestitution = 0.1f;
			static const float FragileFriction = 0.7f;


			static const float DominoDensity = 2.0f;
			static const float DominoRestitution = 0.1f;
			static const float DominoFriction = 0.3f;


			static const float ReelDensity = 5.0f;
			static const float ReelRestitution = 0.5f;
			static const float ReelFriction = 0.2f;


			static const float BouncerDensity = 5.0f;
			static const float BouncerRestitution = 0.8f;
			static const float BouncerFriction = 0.4f;


			static const float IceDensity = 5.0f;
			static const float IceRestitution = 0.2f;
			static const float IceFriction = 0.1f;


			static const float OtherDensity = 5.0f;
			static const float OtherRestitution = 0.2f;
			static const float OtherFriction = 0.1f;


			static const float ThiefDensity = 6.5f;
			static const float ThiefRestitution = 0.1f;
			static const float ThiefFriction = 0.5f;


			static const float CoinDensity = 3.5f;
			static const float CoinRestitution = 0.2f;
			static const float CoinFriction = 0.6f;


			static const float CopDensity = 3.5f;
			static const float CopRestitution = 0.35f;
			static const float CopFriction = 0.4f;

			static const float GumDensity = 2.5f;
			static const float GumRestitution = 0.80f;
			static const float GumFriction = 0.5f;


			static const float PiggyBankDensity = 7.5f;
			static const float PiggyBankRestitution = 0.2f;
			static const float PiggyBankFriction = 0.4f;


			static const float AirBubbleDensity = 0.1f;
			static const float AirBubbleRestitution = 0.1f;
			static const float AirBubbleFriction = 0.1f;


			static const float TyreDensity = 8.0f;
			static const float TyreRestitution = 0.2f;
			static const float TyreFriction = 9.0f;


			static const float BombDensity = 6.5f;
			static const float BombRestitution = 0.1f;
			static const float BombFriction = 0.5f;


			static const float GlassDensity = 3.0f;
			static const float GlassRestitution = 0.1f;
			static const float GlassFriction = 0.2f;
			static const float GlassBreakSpeed = 15.0f;


			static const float HooliganDensity = 8.0f;
			static const float HooliganRestitution = 0.1f;
			static const float HooliganFriction = 0.8f;

		}

	}
};

#endif

