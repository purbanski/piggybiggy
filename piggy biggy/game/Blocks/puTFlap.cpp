#include "puTFlap.h"
#include "Levels/Level.h"

//--------------------------------------------------------------------------------------
puTFlap::puTFlap()
{
	b2Filter filter;

	// _woodBox1
	_woodBox1.SetRotation( 90.0000f );
	_woodBox1.SetDeltaRotation( 90.0000f );
	_woodBox1.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox1.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_woodBox1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_woodBox1.SetBodyType( b2_dynamicBody );

	// _screw1
	_screw1.GetBody()->GetFixtureList()->SetDensity( 0.1000f );
	_screw1.GetBody()->GetFixtureList()->SetFriction( 0.1000f );
	_screw1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	filter.groupIndex = -1;
	filter.categoryBits = 0;
	filter.maskBits = 65535;
	_screw1.GetBody()->GetFixtureList()->SetFilterData( filter );
	_screw1.SetBodyType( b2_staticBody );

	// _woodBox2
	_woodBox2.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox2.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_woodBox2.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	filter.groupIndex = -1;
	filter.categoryBits = 0;
	filter.maskBits = 65535;
	_woodBox2.GetBody()->GetFixtureList()->SetFilterData( filter );
	_woodBox2.SetBodyType( b2_dynamicBody );
	_woodBox2.SetSprite( CCSprite::spriteWithFile( Skins::GetSkinName( "Images/Blocks/none.png" ) ));

	// _woodBox3
	_woodBox3.SetRotation( 7.0000f );
	_woodBox3.SetDeltaRotation( 7.0000f );
	_woodBox3.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox3.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_woodBox3.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_woodBox3.SetBodyType( b2_dynamicBody );


	// SetDeltaXY
	_woodBox1.SetDeltaXY( 2.0f * -1.8000f, 0.0000f );
	_screw1.SetDeltaXY( 2.0f * -1.8000f, 0.0000f );
	_woodBox2.SetDeltaXY( 2.0f * -1.8000f, 2.0f * 3.6000f );
	_woodBox3.SetDeltaXY( 2.0f * -3.6500f, 2.0f * -3.8000f );

	// SetNodeChain
	SetNodeChain( &_woodBox1, &_screw1, &_woodBox2, &_woodBox3, NULL );

	// Main block (WoodBox)
	GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	SetBodyType( b2_dynamicBody );


	SetPosition( 0.0f, 0.0f );

	_jointRev1 = NULL;
	_jointRev2 = NULL;
	_jointRev3 = NULL;
	_jointRev4 = NULL;
	CreateJoints();
}
//-----------------------------------------------------------------------------------------------------
void puTFlap::CreateJoints()
{
	b2RevoluteJointDef jointRevDef1;
	jointRevDef1.collideConnected = false;
	jointRevDef1.Initialize( _screw1.GetBody(), _woodBox1.GetBody(), _screw1.GetPosition() );
	_jointRev1 = (b2RevoluteJoint *) _level->GetWorld()->CreateJoint( &jointRevDef1 );

	float l;
	l = 3.1f / 2.0f;

	if ( _flipX == eAxisNormal )
		_jointRev1->SetLimits( -l, 0.0f );
	else
		_jointRev1->SetLimits( 0.0f, l );
		

	_jointRev1->EnableLimit( true );
	_jointRev1->EnableMotor( true );
	_jointRev1->SetMotorSpeed( 0.0000f );
	_jointRev1->SetMaxMotorTorque( 400.0000f );

	b2RevoluteJointDef jointRevDef2;
	jointRevDef2.collideConnected = false;
	jointRevDef2.Initialize( _woodBox2.GetBody(), _woodBox1.GetBody(), _woodBox2.GetPosition() );
	_jointRev2 = (b2RevoluteJoint *) _level->GetWorld()->CreateJoint( &jointRevDef2 );

	_jointRev2->SetLimits( 0.0000f, 0.0000f );
	_jointRev2->EnableLimit( true );
	_jointRev2->EnableMotor( true );
	_jointRev2->SetMotorSpeed( 0.0000f );
	_jointRev2->SetMaxMotorTorque( 0.0000f );

	b2RevoluteJointDef jointRevDef3;
	jointRevDef3.collideConnected = false;
	jointRevDef3.Initialize( _woodBox1.GetBody(), _woodBox3.GetBody(), _woodBox3.GetPosition() );
	_jointRev3 = (b2RevoluteJoint *) _level->GetWorld()->CreateJoint( &jointRevDef3 );

	_jointRev3->SetLimits( 0.0000f, 0.0000f );
	_jointRev3->EnableLimit( true );
	_jointRev3->EnableMotor( true );
	_jointRev3->SetMotorSpeed( 0.0000f );
	_jointRev3->SetMaxMotorTorque( 0.0000f );

	b2RevoluteJointDef jointRevDef4;
	jointRevDef4.collideConnected = false;
	jointRevDef4.Initialize( _woodBox1.GetBody(), GetBody(), _woodBox1.GetPosition() );
	_jointRev4 = (b2RevoluteJoint *) _level->GetWorld()->CreateJoint( &jointRevDef4 );

	_jointRev4->SetLimits( 0.0000f, 0.0000f );
	_jointRev4->EnableLimit( true );
	_jointRev4->EnableMotor( true );
	_jointRev4->SetMotorSpeed( 0.0000f );
	_jointRev4->SetMaxMotorTorque( 0.0000f );
}
//-----------------------------------------------------------------------------------------------------
void puTFlap::DestroyJoints()
{
	if ( _jointRev1 ) _level->GetWorld()->DestroyJoint( _jointRev1 );
	if ( _jointRev2 ) _level->GetWorld()->DestroyJoint( _jointRev2 );
	if ( _jointRev3 ) _level->GetWorld()->DestroyJoint( _jointRev3 );
	if ( _jointRev4 ) _level->GetWorld()->DestroyJoint( _jointRev4 );

	_jointRev1 = NULL;
	_jointRev2 = NULL;
	_jointRev3 = NULL;
	_jointRev4 = NULL;
}
//-----------------------------------------------------------------------------------------------------
void puTFlap::SetJointLimits( float lower, float upper )
{
	_jointRev1->SetLimits( lower, upper );
}

//-----------------------------------------------------------------------------------------------------
