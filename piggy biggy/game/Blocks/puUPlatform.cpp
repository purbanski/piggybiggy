#include <Box2D/Box2D.h>

#include "Levels/World.h"
#include "Levels/Level.h"
#include "puUPlatform.h"
#include "puBlock.h"

//-----------------------------------------------------------------------------------------------------
puUPlatform::puUPlatform()
{
	_boxLeft.SetDeltaRotation( 90 );
	_boxRight.SetDeltaRotation( 90 );

	SetNext( &_boxLeft );
	_boxLeft.SetNext( &_boxRight );

	_boxLeft.SetDeltaXY( -4.5f, 2.0f );
	_boxRight.SetDeltaXY( 4.5f, 2.0f );

	SetPosition( 0, 0 );
}
//-----------------------------------------------------------------------------------------------------
puUPlatform::~puUPlatform()
{
}
