#include <sstream>
#include "unFile.h"
#include "Debug/MyDebug.h"
#include "puTriangle.h"
#include "puBlocksSettings.h"
#include "GameConfig.h"
#include "Skins.h"

using namespace pu::blocks::settings;
//----------------------------------------------------------------------------------------------------------
puTriangleBase::puTriangleBase( const b2Vec2& t1, const b2Vec2& t2, const b2Vec2& t3 ) 
{
	_tv[0] = t1;
	_tv[1] = t2;
	_tv[2] = t3;

	b2PolygonShape triangle;
	triangle.Set( _tv, 3 );

	_body->CreateFixture( &triangle, 2.0f );
}
//----------------------------------------------------------------------------------------------------------
void puTriangleBase::LoadDefaultSprite()
{
	string filename;
	stringstream ss;

	ss << ( int )( _tv[0].x * 10.0f ) << "x" << ( int )( _tv[0].y * 10.0f )  << "-";
	ss << ( int )( _tv[1].x * 10.0f ) << "x" << ( int )( _tv[1].y * 10.0f )  << "-";
	ss << ( int )( _tv[2].x * 10.0f ) << "x" << ( int )( _tv[2].y * 10.0f )  << ".png";

	filename.append( Skins::GetSkinName( "Images/Blocks/" ) );
	filename.append( GetClassName2() );
	filename.append( ss.str() );

	D_LOG( "Loading sprite %s", filename.c_str())

	if ( unResourceFileCheckExists( filename.c_str() ) )
		_sprite = CCSprite::spriteWithFile( filename.c_str() );
}

//----------------------------------------------------------------------------------------------------------
puTriangle_::puTriangle_( const b2Vec2& t1, const b2Vec2& t2, const b2Vec2& t3 ) : puTriangleBase( t1, t2, t3 )
{

}
//----------------------------------------------------------------------------------------------------------
puTriangleFragile_::puTriangleFragile_( const b2Vec2& t1, const b2Vec2& t2, const b2Vec2& t3 ) : puTriangleBase( t1, t2, t3 )
{
	b2Fixture *fixture;

	fixture = _body->GetFixtureList();
	fixture->SetDensity( FragileDensity );
	fixture->SetFriction( FragileFriction );
	fixture->SetRestitution( FragileRestitution );

	SetBodyType( b2_dynamicBody );
	SetBlockType( GameTypes::eDestroyable );
	LoadDefaultSprite();
}
//----------------------------------------------------------------------------------------------------------
puTriangleWall_::puTriangleWall_( const b2Vec2& t1, const b2Vec2& t2, const b2Vec2& t3 ) : puTriangleBase( t1, t2, t3 )
{
	b2Fixture *fixture;

	fixture = _body->GetFixtureList();
	fixture->SetDensity( WallDensity );
	fixture->SetFriction( WallFriction );
	fixture->SetRestitution( WallRestitution );

	SetBodyType( b2_staticBody );
	SetBlockType( GameTypes::eOther );
	LoadDefaultSprite();
}
//----------------------------------------------------------------------------------------------------------
puTriangleWood_::puTriangleWood_( const b2Vec2& t1, const b2Vec2& t2, const b2Vec2& t3 ) : puTriangleBase( t1, t2, t3 )
{
	b2Fixture *fixture;

	fixture = _body->GetFixtureList();
	fixture->SetDensity( WoodDensity );
	fixture->SetFriction( WoodFriction );
	fixture->SetRestitution( WoodRestitution );

	SetBodyType( b2_dynamicBody );
	SetBlockType( GameTypes::eOther );
	LoadDefaultSprite();
}
//----------------------------------------------------------------------------------------------------------
puTriangleEqual_::puTriangleEqual_( float sideSize ) : puTriangleBase( b2Vec2( 0.0f, 0.0f ), b2Vec2( sideSize, 0.0f ), b2Vec2( sideSize / 2.0f, sqrt( 3.0f ) * sideSize / 2.0f ) )
{
	
}

puTriangleEqual_::puTriangleEqual_( const b2Vec2& t1, const b2Vec2& t2, const b2Vec2& t3 ) 
	: 
puTriangleBase( 
	b2Vec2( 0.0f, 0.0f ), 
	b2Vec2( t1.x, 0.0f ), 
	b2Vec2( t1.x / 2.0f, sqrt( 3.0f ) * t1.x / 2.0f ) )
{

}
