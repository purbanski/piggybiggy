#include "puRollingPlatform.h"
#include "Levels/Level.h"
#include <Box2D/Box2D.h>

puRollingPlatform::puRollingPlatform()
{
	b2CircleShape shape;
	shape.m_radius = 0.5f;
	
	b2FixtureDef fixtureDef;
	fixtureDef.shape = &shape;
	fixtureDef.density = 0.0f;
	fixtureDef.friction = 0.0f; // tarcie 0 - nie ma 1- max
	fixtureDef.restitution = 0.0f;

	_body->CreateFixture( &fixtureDef );
	_blockType = GameTypes::eOther;
	_body->SetType(b2_staticBody );


	float x = 7;
	float y = 7 ;
	
	SetPosition( x, y );
	
	_circle.SetPosition( x, y );
	_circle.SetBodyType( b2_dynamicBody );
	_circle.SetBlockType( GameTypes::eMoveable );

			
	//box.SetRotation(90);
	_platform.SetPosition( x, y + 10.0f );
	_platform.SetBodyType( b2_dynamicBody );
	_platform.SetBlockType( GameTypes::eMoveable );
	_platform.GetBody()->GetFixtureList()->SetFriction( 1 );

	_pin.SetPosition( x, y + 8.0f );

	b2RevoluteJointDef jointDef;
	jointDef.Initialize( _platform.GetBody(), _pin.GetBody(), _platform.GetPosition() );
	jointDef.maxMotorTorque = 100000000;
	jointDef.motorSpeed = 0.0f;
	jointDef.enableMotor = true;
	//_pinJoint = 
	(void) _world->CreateJoint(&jointDef);


	{
	b2PolygonShape box1;
	b2PolygonShape box2;
	box1.SetAsBox( 0.5f, 2.5f, b2Vec2( -4, 2 ),  b2_pi * 2.0f );
	box2.SetAsBox( 0.5f, 2.5f, b2Vec2( 4, 2 ),  b2_pi * 2.0f );
	
	b2FixtureDef fixtureDef2;
	fixtureDef2.shape = &box1;
	fixtureDef2.density = 5;
	fixtureDef2.friction = 0.5f; // tarcie 0 - nie ma 1- max
	fixtureDef2.restitution = 0.1f; // bouncy 0 - nie ma 1 max;
	
	_platform.GetBody()->CreateFixture( &fixtureDef2 );

	fixtureDef2.shape = &box2;
	_platform.GetBody()->CreateFixture( &fixtureDef2 );
	//_body->SetType( b2_staticBody );
	}

	_platformLeft.SetPosition( 100, 100);
	_platformRight.SetPosition( 100, 100);
	
	// joint ground - rotating wheel
	b2RevoluteJointDef jd1;
	jd1.bodyA = _body;
	jd1.bodyB = _circle.GetBody();
	jd1.localAnchorA = _body->GetLocalPoint( _circle.GetPosition() );
	jd1.localAnchorB = _circle.GetBody()->GetLocalPoint( _circle.GetPosition() ); 
	jd1.referenceAngle = _circle.GetBody()->GetAngle() - _body->GetAngle();
	
	jd1.maxMotorTorque = 12000.0f;
	jd1.motorSpeed = 0.0f;
	jd1.enableMotor = true;

	_jointGroundCircle = ( b2RevoluteJoint* ) _world->CreateJoint( &jd1 );
	


	// joint ground platform
	b2PrismaticJointDef jd3;
	jd3.Initialize( _body, _pin.GetBody(), _pin.GetPosition(), b2Vec2( -1.0f, 0.0f));
	//jd3.enableLimit = true;
	_jointGroundPlatform = (b2PrismaticJoint*) _world->CreateJoint( &jd3 );
	_jointGroundPlatform->SetLimits( -5.0f, 5.0f );


	// joint Rotating wheel -  pin 
	b2GearJointDef jd5;
	jd5.bodyA = _circle.GetBody();
	jd5.bodyB = _pin.GetBody();
	jd5.joint1 = _jointGroundCircle;
	jd5.joint2 = _jointGroundPlatform; //prismatic
	jd5.ratio = -1.0f / _circle.GetBody()->GetFixtureList()->GetShape()->m_radius;
	_jointGear = (b2GearJoint*) _world->CreateJoint(&jd5);

	//_sprite = CCSprite::spriteWithFile("Blocks/Coin.png");
	//Level::GLevel()->CoinAdded();
}
//-----------------------------------------------------------------------------------------------------
puRollingPlatform::~puRollingPlatform()
{
}
