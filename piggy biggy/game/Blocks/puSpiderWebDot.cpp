#include "puSpiderWebDot.h"
//
//puSpiderWebDot::puSpiderWebDot()
//{
//	b2CircleShape shape;
//	b2FixtureDef fixtureDef;
//
//	fixtureDef.shape = &shape;
//	fixtureDef.density = 0.5f;
//	fixtureDef.restitution = 0.1f;
//
//	shape.m_radius = 1.0f;
//	_body->CreateFixture( &fixtureDef );
//
//	b2CircleShape shape2;
//	b2FixtureDef fixtureDef2;
//
//	b2Filter filter;
//	filter.groupIndex = -1;
//
//	fixtureDef2.filter = filter;
//	fixtureDef2.shape = &shape2;
//	fixtureDef2.density = 0.0f;
//	fixtureDef2.restitution = 0.0f;
//
//	shape2.m_radius = 2.0f;
//
//	_body->CreateFixture( &fixtureDef2 );
//
//	//SetBodyType( b2_staticBody );
//	SetBlockType( eDestroyable );
//};
//

#include <Box2D/Box2D.h>

#include "Levels/World.h"
#include "Levels/Level.h"
//#include "punewblock.h"
//#include "puDUPA.h"

puSpiderWebDot::puSpiderWebDot() : puCircle_( 2.0f )/*, _circle1( 2.0f )*/
{
	// _circle1
	//b2Filter filter;
	//filter.groupIndex = -1 ;
	//_circle1.GetBody()->GetFixtureList()->SetFilterData( filter );
	//_circle1.GetBody()->GetFixtureList()->SetDensity( 1.0f );
	//_circle1.GetBody()->GetFixtureList()->SetFriction( 0.2f );
	//_circle1.GetBody()->GetFixtureList()->SetRestitution( 0.5f );
	//_circle1.SetBodyType( b2_dynamicBody );
	//_circle1.SetBlockType( eDestroyable );

	//// SetNodeChain
	//SetNodeChain( &_circle1, NULL );

	//// SetDeltaXY
	//_circle1.SetDeltaXY( 0.0f, 0.0f );

	// Main block (Circle)
	GetBody()->GetFixtureList()->SetDensity( 5.0f );
	GetBody()->GetFixtureList()->SetFriction( 0.2f );
	GetBody()->GetFixtureList()->SetRestitution( 0.5f );
	SetBodyType( b2_dynamicBody );
	SetBlockType( GameTypes::eDestroyable );

	SetPosition( 0, 0 );
	CreateJoints();
	LoadDefaultSprite();
}
//-----------------------------------------------------------------------------------------------------
void puSpiderWebDot::CreateJoints()
{
	//b2RevoluteJointDef jointDef1;
	//jointDef1.Initialize( GetBody(), _circle1.GetBody(), b2Vec2( _flipX * 0.1f, _flipY * 0.1f ));
	//_joint1 = (b2RevoluteJoint *) _world->CreateJoint( &jointDef1 );

	//_joint1->EnableMotor( true );
	//_joint1->SetMotorSpeed( 0.0f );
	//_joint1->SetMaxMotorTorque( -3.032e-013f );
}
//-----------------------------------------------------------------------------------------------------
void puSpiderWebDot::DestroyJoints()
{
//	_world->DestroyJoint( _joint1 );
//	_joint1 = NULL;
}
//-----------------------------------------------------------------------------------------------------
puSpiderWebDot::~puSpiderWebDot()
{}
