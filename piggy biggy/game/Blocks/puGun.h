#ifndef __PUGUN_H__
#define __PUGUN_H__

#include "GameConfig.h"
#include "puBlock.h"
#include "puCircle.h"
#include <vector>

using namespace std;
//--------------------------------------------------------------------------
class puBullet : public puBlock
{
public :
	puBullet();
	void Fired();
	virtual void Step();

	void SetExplosive( bool enable );

private :
	void FilterReset();

private :
	bool _fired;
	int  _filterResetDelay;
};
//--------------------------------------------------------------------------
class puGunSensor : public puCircleBase
{
public:
	puGunSensor();
	virtual void Activate( void *ptr);
	~puGunSensor(){};
};
//--------------------------------------------------------------------------
class puGunBase : public puBlock
{
public:
	puGunBase();
	puGunBase( b2Vec2 size );

	virtual ~puGunBase();
	virtual void Activate( void *ptr = NULL );

	void SetBulletCount( int shots );

protected:
	void StartSpitterAnim( const char *filename );
	virtual void DoAnimSpit();
	virtual void DoAnimSpitEmpty();

private:
	void Construct( b2Vec2 size );
	b2Vec2 GetShotForce();
	b2Vec2 GetBulletStartPostion();

protected:
	typedef puBullet GunClip[Config::GunClipSize];
	
	b2Vec2		_bulletDelta;
	puGunSensor _sensor;
	GunClip		_bullets;
	int			_shotCount;
};
//--------------------------------------------------------------------------
class puGun : public puGunBase
{
public:
	puGun();

protected:
	virtual void DoAnimSpit();
	virtual void DoAnimSpitEmpty();
};
//--------------------------------------------------------------------------
class puGunExplosive : public puGunBase
{
public:
	puGunExplosive();

protected:
	virtual void DoAnimSpit();
	virtual void DoAnimSpitEmpty();
};
//--------------------------------------------------------------------------

#endif

