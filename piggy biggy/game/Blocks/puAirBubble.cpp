#include "Levels/Level.h"
#include "puAirBubble.h"

//-----------------------------------------------------------------------------------------------------
// Air Bubble
//-----------------------------------------------------------------------------------------------------
void puAirBubble_::Construct( float radius )
{
	_shapeType = eShape_Circle;

	CreateFixture( radius );

	LoadDefaultSprite();
	_baseScaleX = _sprite->getScaleX();
	_baseScaleY = _sprite->getScaleY();


	SetBodyType( b2_dynamicBody );
	_blockType = GameTypes::eAirBubble;
	_bouancyRate = 1.2f;

	_insideBlock = NULL;
}
//-----------------------------------------------------------------------------------------------------
void puAirBubble_::Colide( puBlock *block)
{
	if ( block->GetBlockType() == GameTypes::eAirBubble )
		ColideWithBubble( block );
	else
		ColideWithBlock( block );
}
//-----------------------------------------------------------------------------------------------------
void puAirBubble_::ColideWithBlock( puBlock *block )
{
	D_HIT
		PutInBubble( block );
	//block->SetArea( block->GetArea() + GetArea() );
	//DestroyBody();
}
//-----------------------------------------------------------------------------------------------------
void puAirBubble_::ColideWithBubble( puBlock *block )
{
	if ( ! _level->IsDemoRunning() )
		SoundEngine::Get()->PlayEffect( SoundEngine::eSoundAirBubblePop );

	D_HIT
		float radius;
	b2Vec2 pos;

	radius = ( block->GetWidth() + GetWidth() )/ 2.0f;
	pos = 0.5f * ( block->GetReversedPosition() + GetReversedPosition() );


	block->Destroy();
	SetPosition( pos );
	//block->GetSprite()->removeFromParentAndCleanup( true );
	//block->SetSprite( NULL );

	CleanFixture();
	CreateFixture( radius );
	UpdateSprite();
}
//-----------------------------------------------------------------------------------------------------
void puAirBubble_::Step()
{
	_sprite->setRotation( 0.0f );

	if ( GetSprite() )
	{
		if ( GetSprite()->getActionByTag( eActionScale ) )
		{
			//DS_LOG("Akcja found")
			return;
		}
		//else
		//DS_LOG("Akcja not found")
	}

	float deltaX;
	float deltaY;
	float ratio;
	float scaleX;
	float scaleY;
	float scaleLimit;
	float scaleAnimDeltaLimit;

	//--------------------------------------------
	// Airbuble x, y scale won't be scaled more 
	// than that
	//--------------------------------------------
	scaleLimit = 0.3f;

	//--------------------------------------------
	// Once reached we need to play scale anim
	//--------------------------------------------
	scaleAnimDeltaLimit = 0.2f;

	//--------------------------------------------
	// Ratio between flatting air bubble and it's speed
	// the higher ratio, air bubble flatten slower
	//--------------------------------------------
	ratio = 50.0f;


	deltaX = abs( _body->GetLinearVelocity().y  ) / ratio;
	deltaY = abs( _body->GetLinearVelocity().x  ) / ratio;

	scaleX = _baseScaleX - deltaY + deltaX;
	scaleY = _baseScaleY + deltaY - deltaX;

	if ( scaleX < scaleLimit ) scaleX = scaleLimit;
	if ( scaleY < scaleLimit ) scaleY = scaleLimit;

	DS_SEPERATOR
		DS_FLOAT( (_lastScaleX - scaleX))
		DS_FLOAT( (_lastScaleY - scaleY))

		//	if ( abs( _lastScaleX - scaleX  ) > scaleAnimDeltaLimit || abs( _lastScaleY - scaleY  ) > scaleAnimDeltaLimit ) 
	{
		CCScaleTo *scale = CCScaleTo::actionWithDuration( 0.5f, scaleX, scaleY );

		scale->setTag( eActionScale );
		_sprite->runAction( scale );
		PlayFlatingAnim();

		_lastScaleX = scaleX;
		_lastScaleY = scaleY;

		return;
	}

	_sprite->setScaleX( scaleX );
	_sprite->setScaleY( scaleY );

	_lastScaleX = scaleX;
	_lastScaleY = scaleY;
}
//-----------------------------------------------------------------------------------------------------
void puAirBubble_::PlayFlatingAnim()
{

}
//-----------------------------------------------------------------------------------------------------
void puAirBubble_::CreateFixture( float radius )
{
	_width = 2.0f * radius;
	_height = 2.0f * radius;
	_radius = radius;

	b2CircleShape shape;
	shape.m_radius = radius;

	b2FixtureDef fixtureDef;
	fixtureDef.shape = &shape;
	fixtureDef.density = AirBubbleDensity;
	fixtureDef.restitution = AirBubbleRestitution;
	fixtureDef.friction = AirBubbleFriction;

	_body->CreateFixture( &fixtureDef );
	if ( _sprite ) 
	{
		//	_sprite->removeFromParentAndCleanup( true );
		//	_sprite = NULL;
	}

	CalculateArea();
}
//-----------------------------------------------------------------------------------------------------
void puAirBubble_::CleanFixture()
{
	if ( ! _body || !_body->GetFixtureList() )
		return;

	_body->DestroyFixture( _body->GetFixtureList() );
}
//-----------------------------------------------------------------------------------------------------
void puAirBubble_::UpdateSprite()
{
	CCAction *action;
	action = _sprite->getActionByTag( eActionScale );

	if ( action )
		action->stop();

	SetDefaultSpriteScale();
	_baseScaleX = _sprite->getScaleX();
	_baseScaleY = _sprite->getScaleY();
}
//-----------------------------------------------------------------------------------------------------
void puAirBubble_::Destroy()
{
	//_level->GetParticleEngine()->Effect_Burn( _level, GetPosition() );
	DestroyBody();

	if ( ! _sprite ) 
		return;

	CCFadeTo *fade;
	CCScaleTo *scale;

	scale = new CCScaleTo();
	scale->autorelease();

	fade = new CCFadeTo();
	fade->autorelease();

	fade->initWithDuration( 0.3f, 0 );
	scale->initWithDuration( 0.3f, .1f );

	_sprite->runAction( fade );
	_sprite->runAction( scale );

	if ( ! _level->IsDemoRunning() )
		SoundEngine::Get()->PlayEffect( SoundEngine::eSoundAirBubblePop );
}
//-----------------------------------------------------------------------------------------------------
void puAirBubble_::PutInBubble( puBlock *block )
{
	block->SetPosition( GetReversedPosition() );

	b2RevoluteJointDef rjDef;
	rjDef.collideConnected = false;
	rjDef.Initialize( _body, block->GetBody(), GetPosition() );


	_world->CreateJoint( &rjDef );
}
//-----------------------------------------------------------------------------------------------------
bool puAirBubble_::IsBusy()
{
	return ( _insideBlock != NULL );
}
//-----------------------------------------------------------------------------------------------------
void puGumBall_::Construct( float radius )
{
	_width = 2.0f * radius;
	_height = 2.0f * radius;

	b2CircleShape shape;
	shape.m_radius = radius;

	b2FixtureDef fixtureDef;
	fixtureDef.shape = &shape;
	fixtureDef.density = GumDensity;
	fixtureDef.restitution = GumRestitution;
	fixtureDef.friction = GumFriction;

	_body->CreateFixture( &fixtureDef );

	SetBodyType( b2_dynamicBody );
	_blockType = GameTypes::eOther;
	_shapeType = eShape_Circle;

	LoadDefaultSprite();
}
