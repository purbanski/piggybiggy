#include "puBlockExtras.h"
#include "Levels/Level.h"

//---------------------------------------------------------------------------------------------------
// Bomb Touch
//---------------------------------------------------------------------------------------------------
puBlockTouch::puBlockTouch() : puCircleBase( 2.4f )
{
	_width = 2.0f * _radius;
	_height = 2.0f * _radius;

	_sprite = CCSprite::spriteWithFile( Skins::GetSkinName( "Images/Blocks/none.png" ) );

	b2Filter filter;
	filter.groupIndex = -1;
	filter.categoryBits = 0;
	filter.maskBits = 0;

	b2CircleShape shape;

	b2FixtureDef fixtureDef;
	fixtureDef.shape = &shape;
	fixtureDef.density = 0.0f;
	fixtureDef.restitution = 0.0f;
	fixtureDef.friction = 0.0f;
	fixtureDef.filter = filter;

	shape.m_radius = _radius;
	_body->CreateFixture( &fixtureDef );

	SetBodyType( b2_dynamicBody );
	_blockType = GameTypes::eActivable;
}
//-------------------------------------------------------------------------------------
void puBlockTouch::Activate( void *ptr )
{
	GetRoot()->Activate();
}
//-----------------------------------------------------------------------------------------------------
void puBlockTouch::Destroy()
{
	if ( _sprite )
	{
		_sprite->removeFromParentAndCleanup( true );
		_sprite = NULL;
	}
	DestroyBody();
}
