#include "puEmptyBlock.h"
#include "Skins.h"

puEmptyBlock::puEmptyBlock()
{
	b2CircleShape circle;
	circle.m_radius = 2.0f;

	b2Filter filter;
	filter.categoryBits = 0;
	filter.maskBits = 0;
	filter.groupIndex = -1;

	b2FixtureDef fixtureDef;
	fixtureDef.shape = &circle;
	fixtureDef.density = 0.0f;
	fixtureDef.friction = 0.0f;
	fixtureDef.restitution = 0.0f;
	fixtureDef.filter = filter;

	SetBodyType( b2_staticBody );
	SetBlockType( GameTypes::eOther );
	_body->CreateFixture( &fixtureDef );
	_sprite = CCSprite::spriteWithFile( Skins::GetSkinName("Images/Blocks/none.png") );
}

