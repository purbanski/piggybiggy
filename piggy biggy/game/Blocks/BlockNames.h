#ifndef __BLOCKNAMES_H__
#define __BLOCKNAMES_H__
//------------------------------------------------------------------------------------
#include <map>
#include <string>
#include "GameTypes.h"
//------------------------------------------------------------------------------------
using namespace std;
using namespace GameTypes;
//------------------------------------------------------------------------------------
class BlockName
{
public:
	BlockName();
	BlockName( const char* name );
	const char *GetName() const;

private:
	const static unsigned int NameSize =32;
	char	_name[NameSize];
};
//------------------------------------------------------------------------------------
class BlockNames
{
public:
	static BlockNames* Get();
	static void Destroy();

	~BlockNames();
	const char* GetName( BlockEnum blockEnum );

private:
	BlockNames();

private:
	static BlockNames* _sInstance;
	//typedef char BlName[32];
	typedef map<BlockEnum, BlockName> BlockNameMap;

	BlockNameMap	_blockNames;
};
//------------------------------------------------------------------------------------

#endif
