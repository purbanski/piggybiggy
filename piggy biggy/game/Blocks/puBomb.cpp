#include "puBomb.h"
#include "Game.h"
//-----------------------------------------------------------------------------
void IncBombExploded()
{
    Game::Get()->GetGameStatus()->IncBombDestroyedCount();    
}
//-----------------------------------------------------------------------------
void puBomb_::Construct()
{
	_blockEnum = eBlockBomb;
	SetBodyType( b2_dynamicBody );
	LoadDefaultSprite();
}
//-----------------------------------------------------------------------------
void puBombStatic_::Construct()
{
	_blockEnum = eBlockBombStatic;
	SetBodyType( b2_staticBody );
	LoadDefaultSpriteStatic();
}
//-----------------------------------------------------------------------------
void puMagnetBombMinus_::Construct()
{
	_blockEnum = eBlockMagnetBombMinus;
	LoadDefaultSprite();
}
//-----------------------------------------------------------------------------
void puMagnetBombPlus_::Construct()
{
	_blockEnum = eBlockMagnetBombPlus;
	LoadDefaultSprite();
}
//-----------------------------------------------------------------------------
void puMagnetBomb_::Construct()
{
	_blockEnum = eBlockMagnetBomb;
	LoadDefaultSprite();
}
//-----------------------------------------------------------------------------
