#include <list>
#include <cmath>
#include "puChain.h"
#include "Debug/MyDebug.h"
#include "Levels/Level.h"
#include "Game.h"
#include "SoundEngine.h"
#include "Tools.h"
#include "GameConfig.h"

using namespace std;

const float puChainBit::_sWidth = 2.0f;
const float puChainBit::_sHeight = 0.4f;
const float puChainBase::_sScrewRadius = 0.8f;

//-----------------------------------------------------------------------------------------
puChainBit::puChainBit( puChainBase* chain )
{
    _chain = chain;
	_blockEnum = eBlockChainBit;

	_cutData = AnimerSimpleEffect::Get()->Anim_FlyFly( _level );
	_cutData.Retain();

	b2PolygonShape box;
	// this size is not important
	// as we draw rope base on object center point
	box.SetAsBox( _sWidth / 2.0f, _sHeight / 2.0f  );

	b2Fixture *fixture;

	b2FixtureDef fixtureDef;
	fixtureDef.density = 20.0f;
	fixtureDef.restitution = 0.1f;
	fixtureDef.friction = 0.1f;
	fixtureDef.shape = &box;
	fixture = _body->CreateFixture( &fixtureDef );

	b2Filter filter;
	filter.categoryBits = 1;
	filter.maskBits = 0;
	filter.groupIndex = -1;

	fixture->SetFilterData( filter );

	_body->SetType( b2_dynamicBody );
	_blockType = GameTypes::eCutable;

	_sprite = NULL;
	_spriteZOrder = GameTypes::eSpritesBeloweX3;
	LoadDefaultSprite();
}
//-----------------------------------------------------------------------------------------
void puChainBit::Cut()
{
	_chain->Cut();
}
//-----------------------------------------------------------------------------------------
puChainBit::~puChainBit()
{
}
//-----------------------------------------------------------------------------------------
void puChainBit::StopCutAnim()
{
	_cutData.StopActions();
    _cutData.Release();
}
//-----------------------------------------------------------------------------------------
void puChainBit::RunCutAnim()
{
	CCSize size;
	size = CCDirector::sharedDirector()->getWinSize();
	D_PTR( _cutData._sprite )

	_cutData._sprite->setRotation( - GetRotation() + 90.0f );
	_cutData._sprite->setPosition( 
		ccp( 
		GetPosition().x * RATIO - size.width / 2.0f,
		GetPosition().y * RATIO - size.height / 2.0f ));

	_cutData._sprite->runAction( _cutData._action );
	_cutData._sprite->runAction( _cutData._action2 );

	_level->addChild( _cutData._sprite, GameTypes::eSpriteAnimFlies );
    //    _cutData.Release();
	RemoveSprite( true, false );
}
//-----------------------------------------------------------------------------------------




//-----------------------------------------------------------------------------------------
// puChain
//-----------------------------------------------------------------------------------------
puChainBase::puChainBase()
{
    _quiting = false;
	_cut = false;
	_isHookedBlockFixed = false;
	_flySleepDelay = 50;
	_blockEnum = eBlockChain;

	_hookedBlock = NULL;
	_hookedJoint = NULL;

	b2Filter filter;
	filter.categoryBits = 1;
	filter.maskBits = 0;
	filter.groupIndex = -1;

	_body->GetFixtureList()->SetFilterData( filter );
	_body->SetType( b2_staticBody );
	_blockType = GameTypes::eOther;
	
	SetEmptySprite();
	
	Preloader::Get()->AddSound( SoundEngine::eSoundChainCut );
	Preloader::Get()->AddSound( SoundEngine::eSoundAnim_FlyFly1 );
	Preloader::Get()->AddSound( SoundEngine::eSoundAnim_FlyFly2 );
	Preloader::Get()->AddSound( SoundEngine::eSoundAnim_FlyFly3 );
	Preloader::Get()->AddAnim( "FlyFly" );
}
//-----------------------------------------------------------------------------------------
puChainBase::~puChainBase()
{
	ChainBits::iterator it;
	for ( it = _chainBits.begin(); it != _chainBits.end(); it++ )
		delete (*it);

	_chainBits.clear();
    _joints.clear();
}
	
//-----------------------------------------------------------------------------------------
void puChainBase::SetChain( const GameTypes::ChainConstInfo& info )
{
	switch( info._type )
	{
		case GameTypes::ChainConstInfo::eChainByPoints :
			SetChain( info._chainFixPoint, info._chainEndPoint );
			break;

		case GameTypes::ChainConstInfo::eChainByRadius :
			SetChainOShape( info._radius,  info._piece );
			break;

		case GameTypes::ChainConstInfo::eChainByRadiusAndLength :
			SetChainUShape( info._radius, info._lengthB, info._lengthC );
			break;

		case GameTypes::ChainConstInfo::eChainByCubicLengths :
			SetChainCubic( info._lengthA, info._lengthB, info._lengthC );
			break;

		case GameTypes::ChainConstInfo::eChainByLength :
			SetChain( info._lengthA );
			break;


		default:
			unAssertMsg(puChainCutable, false, ("Unknown chain build type: %d", info._type ));
	}
}
//-----------------------------------------------------------------------------------------
void puChainBase::SetChain( const b2Vec2& aFixPoint, const b2Vec2& aEndPoint )
{
	_constInfo = GameTypes::ChainConstInfo( aFixPoint, aEndPoint );

	//---------------------------
	// Normalize
	//---------------------------
	float nRotation;
	b2Vec2 nFixPoint;
	b2Vec2 nEndPoint;

	nRotation = 0.0f;
	nFixPoint.SetZero();
	nEndPoint.Set( aEndPoint.x - aFixPoint.x, aEndPoint.y - aFixPoint.y );


	if ( nFixPoint.y == nEndPoint.y )
	{
		if ( nFixPoint.x <= nEndPoint.x )
			nRotation = 0.0f;
		else 
			nRotation = 180.0f;
	}
	else
	{
		if ( nEndPoint.x > 0.0f )
		{
			if ( nEndPoint.y > nFixPoint.y )
			{
				nRotation = atan( nEndPoint.y / nEndPoint.x ) * 180.0f / b2_pi;
			}
			else if ( nEndPoint.y < nFixPoint.y )
			{
				nRotation = 360.0f + atan( nEndPoint.y / nEndPoint.x ) * 180.0f / b2_pi;
			}
		}
		else if ( nEndPoint.x < 0.0f )
		{
			nRotation = 180.0f + atan( nEndPoint.y / nEndPoint.x ) * 180.0f / b2_pi;
		}
		else if ( nEndPoint.x == 0.0f )
		{
			if ( nEndPoint.y > nFixPoint.y ) 
			{
				nRotation = 90.0f;
			}
			else if ( nEndPoint.y < nFixPoint.y ) 
			{
				nRotation = 270.0f;
			}
		}
		else
		{
			unAssertMsg(puChainCutable, false, ("Chain len would be zero. Skipping construction"));
			return;
		}
	}

	//D_LOG( "normalized rotation %f", nRotation );
	//D_LOG( "normalized end x:%f y:%f len:%f", nEndPoint.x, nEndPoint.y, nEndPoint.x*nEndPoint.x +nEndPoint.y * nEndPoint.y );

	b2Vec2 tempVec;
	tempVec = nEndPoint;

	nEndPoint.x = tempVec.x * cos( -nRotation * b2_pi / 180.0f ) - tempVec.y * sin( -nRotation * b2_pi / 180.0f );
	nEndPoint.y = tempVec.x * sin( -nRotation * b2_pi / 180.0f ) + tempVec.y * cos( -nRotation * b2_pi / 180.0f );

	//D_LOG( "normalized & rotated end x:%f y:%f len:%f", nEndPoint.x, nEndPoint.y, nEndPoint.x*nEndPoint.x +nEndPoint.y * nEndPoint.y );


	//---------------------------
	// Allocate chain
	//---------------------------
	float chainLen;
	float chainBitLen;

	chainLen = sqrt(( nEndPoint.x * nEndPoint.x ) + ( nEndPoint.y * nEndPoint.y ));
	chainBitLen = puChainBit::_sWidth;

	float chainBitsCount;
	chainBitsCount = chainLen / chainBitLen;

	//---------------------------
	// Allocate chain
	//---------------------------
	AllocateChain( (int) chainBitsCount );

	//---------------------------
	// Setting positions deltas
	// and rotation 
	//---------------------------
	int count;
	float deltaX;
	ChainBits::iterator it;

	count = 0;
	deltaX = nEndPoint.x / chainBitsCount;

	for ( it = _chainBits.begin(); it != _chainBits.end(); it++ )
	{
		( *it )->SetDeltaXY( _sScrewRadius + count * deltaX, 0 );
		( *it )->SetDeltaRotation(  180.0f );
		count++;
	}
	SetPosition( 0, 0 );
	SetChainBitsJoints();

	//unnormalize 
	SetRotation( nRotation );
}
//-----------------------------------------------------------------------------------------
void puChainBase::SetChainOShape( float radius, float piece )
{
	_constInfo = GameTypes::ChainConstInfo( radius, piece );

	//---------------------------
	// Calculate chain length
	//---------------------------
	float chainLen;
	float chainBitLen;

	chainLen = 2.0f * b2_pi * radius * piece;
	chainBitLen = puChainBit::_sWidth;

	float chainBitsCount;
	chainBitsCount = chainLen / chainBitLen;


	//---------------------------
	// Allocate chain
	//---------------------------
	AllocateChain((int) chainBitsCount );


	//---------------------------
	// Setting positions deltas
	// and rotation 
	//---------------------------
	int count;
	count = 0;

	float xRot, yRot;
	float xDelta, yDelta;
	float rotDelta;
	ChainBits::iterator it;

	xDelta = -radius * cos( 0.0f );
	yDelta = radius * sin( 0.0f );

	rotDelta = 360 * piece / _chainBits.size();

	for ( it = _chainBits.begin(); it != _chainBits.end(); it++ )
	{
		xRot = xDelta + radius * cos( 2 * b2_pi * count / _chainBits.size() * piece );
		yRot = yDelta + radius * sin( 2 * b2_pi * count / _chainBits.size() * piece );

		( *it )->SetDeltaXY( -xRot, -yRot );
		( *it )->SetDeltaRotation(  90 + rotDelta * count );

		count++;
	}
	SetPosition( 0, 0 );
	SetChainBitsJoints();
}
//-----------------------------------------------------------------------------------------
void puChainBase::SetChain( float length )
{
	SetChain( b2Vec2( 0.0f, length ), b2Vec2( 0.0f, 0.0f ));
	_constInfo = GameTypes::ChainConstInfo( length );
}
//-----------------------------------------------------------------------------------------
void puChainBase::SetChainUShape( float radius, float lengthA, float lengthB )
{
	if ( ! lengthA || ! lengthB || !radius)
	{
		unAssert(false,("Bad chain length"));
		return;
	}

	_constInfo = GameTypes::ChainConstInfo( GameTypes::ChainConstInfo::eChainByRadiusAndLength, radius, lengthA, lengthB );

	//---------------------------
	// Calculate chain length
	//---------------------------
	float chainLenCircle;
	float chainBitLen;

	chainLenCircle = b2_pi * radius;
	chainBitLen = puChainBit::_sWidth;

	float chainBitsCircleCount;
	float chainBitsChainACount;
	float chainBitsChainBCount;

	chainBitsCircleCount = (float)(int)( chainLenCircle / chainBitLen );
	chainBitsChainACount = (float)(int)( lengthA / chainBitLen );
	chainBitsChainBCount = (float)(int)( lengthB / chainBitLen );



	//---------------------------
	// Allocate chain
	//---------------------------
	AllocateChain((int)( chainBitsCircleCount + chainBitsChainACount + chainBitsChainBCount ));


	b2Vec2 deltaPos;
	deltaPos.SetZero();

	ChainBits::iterator it;
	//---------------------------
	// Setting positions deltas
	// and rotation - LengthA
	//---------------------------
	{
		float delta;
		delta = lengthA / chainBitsChainACount;

		it = _chainBits.begin(); 
		for ( int i = 0 ; i < chainBitsChainACount ; i++ )
		{
			( *it )->SetDeltaXY( 0.0f, -_sScrewRadius - i * delta );
			( *it )->SetDeltaRotation( 90.0f );

			deltaPos.y = ((*it))->GetDeltaY();
			it++;
		}
	}

	deltaPos.y -= chainBitLen;


	//---------------------------
	// Setting positions deltas
	// and rotation - Half Circle
	//---------------------------
	{
		float xRot, yRot;
		float deltaX, deltaY;
		float rotDelta;

		deltaX = -radius * cos( 0.0f );
		deltaY = radius * sin( 0.0f );

		rotDelta = 180.0f / chainBitsCircleCount;


		for ( int i = 0 ; i < chainBitsCircleCount ; i++ )
		{
			xRot = deltaX + radius * cos( b2_pi * i / chainBitsCircleCount ) + deltaPos.x;
			yRot = deltaY + radius * sin( b2_pi * i / chainBitsCircleCount ) - deltaPos.y;

			( *it )->SetDeltaXY( -xRot, -yRot );
			( *it )->SetDeltaRotation(  90 + rotDelta * i );

			it++;
		}
	}

	//deltaPos.y -=  chainBitAdjustedLen;

	//---------------------------
	// Setting positions deltas
	// and rotation - LengthB
	//---------------------------
	{
		float delta;
		delta = lengthB / chainBitsChainBCount;

		for ( int i = 0 ; i < chainBitsChainBCount ; i++ )
		{
			( *it )->SetDeltaXY( deltaPos.x + 2.0f * radius, deltaPos.y + i * delta );
			( *it )->SetDeltaRotation( 270.0f );
			it++;
		}
	}

	SetPosition( 0, 0 );
	SetChainBitsJoints();
}
//-----------------------------------------------------------------------------------------
void puChainBase::SetChainCubic( float lengthA, float lengthB, float lengthC )
{
	if ( ! lengthA || ! lengthB || ! lengthC )
	{
		unAssert(false,("Bad chain length"));
		return;
	}

	_constInfo = GameTypes::ChainConstInfo( GameTypes::ChainConstInfo::eChainByCubicLengths, lengthA, lengthB, lengthC );

	//---------------------------
	// Calculate chain length
	//---------------------------
	float chainBitLen;
	chainBitLen = puChainBit::_sWidth;

	float chainBitsChainACount;
	float chainBitsChainBCount;
	float chainBitsChainCCount;

	chainBitsChainACount = (float)(int)( lengthA / chainBitLen );
	chainBitsChainBCount = (float)(int)( lengthB / chainBitLen );
	chainBitsChainCCount = (float)(int)( lengthC / chainBitLen );


	//---------------------------
	// Allocate chain
	//---------------------------
	AllocateChain((int)( chainBitsChainACount + chainBitsChainBCount + chainBitsChainCCount ));

	b2Vec2 deltaPos;
	deltaPos.SetZero();

	ChainBits::iterator it;
	//---------------------------
	// Setting positions deltas
	// and rotation - LengthA
	//---------------------------
	{
		float delta;
		delta = lengthA / chainBitsChainACount;

		it = _chainBits.begin(); 
		for ( int i = 0 ; i < chainBitsChainACount ; i++ )
		{
			( *it )->SetDeltaXY( 0.0f, -_sScrewRadius - i * delta );
			( *it )->SetDeltaRotation( 90.0f );

			deltaPos.y = ((*it))->GetDeltaY();
			it++;
		}
	}


	
	deltaPos.y -= chainBitLen;
	//---------------------------
	// Setting positions deltas
	// and rotation - LengthB
	//---------------------------
	{
		ChainBits::iterator itBegin = it;
		float delta;

		delta = lengthB / chainBitsChainBCount;

		for ( int i = 0 ; i < chainBitsChainBCount ; i++ )
		{
			( *it )->SetDeltaXY( i * delta + deltaPos.x,  deltaPos.y );
			( *it )->SetDeltaRotation( 180.0f );

			deltaPos.y = ((*it))->GetDeltaY();
			it++;
		}

		( *itBegin )->SetDeltaRotation( 90.0f );
	}



	////deltaPos.y -=  chainBitAdjustedLen;

	//---------------------------
	// Setting positions deltas
	// and rotation - LengthC
	//---------------------------
	{
		ChainBits::iterator itBegin = it;
		float delta;

		delta = lengthC / chainBitsChainCCount;

		for ( int i = 0 ; i < chainBitsChainCCount ; i++ )
		{
			( *it )->SetDeltaXY( deltaPos.x + lengthB, deltaPos.y + i * delta );
			( *it )->SetDeltaRotation( 270.0f );
			it++;
		}

		( *itBegin )->SetDeltaRotation( 180.0f );
	}

	SetPosition( 0, 0 );
	SetChainBitsJoints();
}

//-----------------------------------------------------------------------------------------
void puChainBase::Cut()
{
	if ( _cut )
		return;

	_cut = true;
    Game::Get()->GetGameStatus()->IncFliesCutCount();

	if ( ! _level->IsDemoRunning() )
		SoundEngine::Get()->PlayEffect( SoundEngine::eSoundChainCut );

	JointContainer::iterator jointIt;
	for ( jointIt = _joints.begin(); jointIt != _joints.end(); jointIt++ )
	{
		Game::Get()->GetRuningLevel()->GetWorld()->DestroyJoint( *jointIt );
	}
	_joints.clear();
	UnHook();

	//_chainDraw->Cut();
	//_chainDraw->SetChainBits( NULL );
}
//-----------------------------------------------------------------------------------------
void puChainBase::HookOnChain( puBlock* block )
{
	if ( _hookedBlock )
	{
//		if (MessageBoxAvailable())
//			MessageBox("Info", "Something already hooked");
		return;
	}

	_hookedBlock = block;

	ChainBits::iterator it;
	puChainBit *lastChainBit;

	it = _chainBits.end();
	it--;
	lastChainBit = *it;

	b2RevoluteJointDef revJointDef;
	revJointDef.collideConnected = false;
	revJointDef.lowerAngle = -0.1f;
	revJointDef.upperAngle = 0.1f;
	revJointDef.Initialize( lastChainBit->GetBody(), block->GetBody(), lastChainBit->GetPosition() );

	_hookedJoint = (b2RevoluteJoint *) World::Get()->GetWorld()->CreateJoint( &revJointDef );
	_hookedJoint->EnableLimit( _isHookedBlockFixed );
}
//-----------------------------------------------------------------------------------------
void puChainBase::CenterHookOnChain( puBlock* block )
{
	if ( _hookedBlock )
	{
//		if (MessageBoxAvailable())
//			MessageBox("Info", "Something already hooked");
		return;
	}

	b2Vec2 pos;
	pos.x = GetChainEnd()->GetReversedPosition().x + GetChainEnd()->GetDeltaX();
	pos.y = GetChainEnd()->GetReversedPosition().y + GetChainEnd()->GetDeltaY();

	block->SetPosition( pos );
	HookOnChain( block );
}
//-----------------------------------------------------------------------------------------
puBlock* puChainBase::GetHookedBlock()
{
	return _hookedBlock;
}
//-----------------------------------------------------------------------------------------
void puChainBase::UnHook()
{
	if ( _hookedBlock && ! _hookedBlock->GetBody() )
	{
		_hookedBlock = NULL;
		_hookedJoint = NULL;
		return;
	}

	if ( ! _hookedJoint )
	{
#ifdef BUILD_EDITOR
		//if (MessageBoxAvailable())
			//MessageBox("Info", "Nothing to unhook");
#endif
		return;
	}

	Game::Get()->GetRuningLevel()->GetWorld()->DestroyJoint( _hookedJoint );

	_hookedJoint = NULL;
	_hookedBlock = NULL;
}
//-----------------------------------------------------------------------------------------
void puChainBase::AllocateChain( int count )
{
	//----------------------------
	// Allocate mem for chain bits
	//----------------------------
	puChainBit *chainBit;

	for ( int i = 0; i < (int) count; i++ )
	{
		chainBit = new puChainBit( this );
		_chainBits.push_back( chainBit );
	}


	//---------------------------
	// Set Chainbits node sequence
	//---------------------------
	puBlock *currentBlock;
	ChainBits::iterator it;
	currentBlock = this;
	for ( it = _chainBits.begin(); it != _chainBits.end(); it++ )
	{
		currentBlock->SetNext( *it );
		currentBlock =  *it;
	}

	//---------------------------
	// Set Drawing
	//---------------------------
	//_chainDraw->SetChainBits( &_chainBits );

	ChainAllocated();
}
//-----------------------------------------------------------------------------------------
void puChainBase::SetChainBitsJoints()
{
	b2Vec2 jointPosition;
	puBlock *currentBlock;
	b2RevoluteJointDef revJointDef;
	ChainBits::iterator it;

	revJointDef.collideConnected = false;
	
	currentBlock = *( _chainBits.begin());
	it = _chainBits.begin();
	it++;


	while( it != _chainBits.end() )
	{
		jointPosition = currentBlock->GetPosition();
		revJointDef.Initialize( currentBlock->GetBody(), ( *it )->GetBody(),  jointPosition );
		_joints.push_back( _world->CreateJoint( &revJointDef ));
		
		currentBlock =  *it;
		it++;
	}

	//-----------------------------
	// Set screw to first bit joint
	//-----------------------------
	revJointDef.Initialize( GetBody(), (*(_chainBits.begin()))->GetBody(), GetPosition() );
	_joints.push_back( _world->CreateJoint( &revJointDef ));
}
//-----------------------------------------------------------------------------------------
void puChainBase::CleanUp()
{
	ChainBits::iterator it;
	for ( it = _chainBits.begin(); it != _chainBits.end(); it++ )
		(*it)->DestroyBody();

	//_chainDraw->SetChainBits( NULL );
}

//-----------------------------------------------------------------------------------------
b2Vec2 puChainBase::GetRandImpulse()
{
	int impulseMax = 10;
	b2Vec2 impulse;

	impulse.x = -impulseMax + 2.0f * ( rand() % impulseMax );
	impulse.y = -impulseMax + 2.0f * ( rand() % impulseMax );

	return impulse;
}
//-----------------------------------------------------------------------------------------
puBlock* puChainBase::GetChainEnd()
{
	if (! ( _chainBits.size() > 0 ))
		return NULL;

	ChainBits::iterator it;
	it = _chainBits.end();
	it--;

	return ( *it );
}
//-----------------------------------------------------------------------------------------
void puChainBase::CenterToLastBit()
{
	puBlock *block;
	b2Vec2 pos;

	block = GetChainEnd();
	
	pos = -Tools::RotatePointAroundPoint( b2Vec2( block->GetDeltaX(), block->GetDeltaY()), b2Vec2( 0.0f, 0.0f ), GetRotation());
	pos += b2Vec2( 24.0f, 16.0f );

	SetPosition( pos );
}
//-----------------------------------------------------------------------------------------
void puChainBase::FixHook( bool fixed )
{
	_isHookedBlockFixed = fixed;

	if ( ! _hookedJoint ) 
		return;

	_hookedJoint->EnableLimit( _isHookedBlockFixed );
}
//-----------------------------------------------------------------------------------------
bool puChainBase::IsHookFixed()
{
	return _isHookedBlockFixed;
}
//-----------------------------------------------------------------------------------------
void puChainBase::Step()
{
	if ( ! _cut || ! _flySleepDelay || _quiting )
		return;

	_flySleepDelay--;

	if ( ! _flySleepDelay )
	{
		ChainBits::iterator it;
		for ( it = _chainBits.begin(); it != _chainBits.end(); it++ )
		{
			(*it)->RunCutAnim();
			(*it)->DestroyBody();

		}
		SoundEngine::Get()->PlayRandomEffect( 
			10.0f,
			SoundEngine::eSoundAnim_FlyFly1, 
			SoundEngine::eSoundAnim_FlyFly2, 
			SoundEngine::eSoundAnim_FlyFly3, 
			SoundEngine::eSoundNone );
	}

}
//-----------------------------------------------------------------------------------------
void puChainBase::LevelQuiting()
{
    _quiting = true;
}
//-----------------------------------------------------------------------------------------
void puChainBase::LevelExiting()
{
	//---------------------------
	// Stop if already started
	puChainBit *chainBit;
	if ( _chainBits.size() )
	{
		for ( ChainBits::iterator it = _chainBits.begin(); it != _chainBits.end(); it++ )
		{
			chainBit = *it;
			if ( chainBit )
				chainBit->StopCutAnim();
		}
	}

	//---------------------------
	// Prevent to further start
	_flySleepDelay = -1;
}
//-----------------------------------------------------------------------------------------





//-----------------------------------------------------------------------------------------
// Chain Draw
//-----------------------------------------------------------------------------------------
ChainDraw::ChainDraw( puChainBase *chain )
{
	_chain = chain;
	_chainBits = NULL;
	_drawMode = eDraw_Normal;
	_deltaColor = 0.0f;
	_drawColor.r = 1.0f;
	_drawColor.b = 0.0f;
}
//-----------------------------------------------------------------------------------------
void ChainDraw::draw()
{
	return;
	if ( ! _chainBits || ! _chain || ! _chainBits->size() )
		return;
	
	GLESDebugDraw::Get()->LockGL();
	switch ( _drawMode )
	{
	case eDraw_Normal :
		DrawNormal();
		break;
	
	case eDraw_Cut :
		DrawCut();
		break;

	default:
		unAssertMsg(ChainDraw, false, ("Problem with ChainDraw draw mode %d" ,  _drawMode ));
		;

	}
	GLESDebugDraw::Get()->UnlockGL();
}
//-----------------------------------------------------------------------------------------
void ChainDraw::SetChainBits( puChainCutable::ChainBits *chainBits )
{
	_chainBits = chainBits;
}
//-----------------------------------------------------------------------------------------
ChainDraw::~ChainDraw()
{
}
//-----------------------------------------------------------------------------------------
void ChainDraw::DrawNormal()
{
	glLineWidth( 3.0f );	

	b2Vec2 v[2];
	puChainCutable::ChainBits::iterator it;

	puBlock *prevBlock;
	puBlock *currentBlock;

	b2Vec2 deltaPos;
	CCSize size;
	size = CCDirector::sharedDirector()->getWinSize();

	deltaPos.x = -( size.width / RATIO -  Config::GameSize.width / RATIO ) / 2.0f;
	deltaPos.y = -( size.height / RATIO -  Config::GameSize.height / RATIO ) / 2.0f;

	prevBlock = _chain;
	for ( it =  _chainBits->begin(); it != _chainBits->end(); it++ )
	{
		currentBlock = ( *it );

		v[0] = deltaPos + currentBlock->GetPosition();
		v[1] = deltaPos + prevBlock->GetPosition();

		GLESDebugDraw::Get()->DrawSolidBox( v, _drawColor, 1 );
		prevBlock = currentBlock;
	}


	glBlendFunc(CC_BLEND_SRC,CC_BLEND_DST);

	deltaPos.x = Config::ShadowPosition.x / RATIO - ( size.width / RATIO  - Config::GameSize.width / RATIO ) / 2.0f;
	deltaPos.y = Config::ShadowPosition.y / RATIO - ( size.height / RATIO - Config::GameSize.height / RATIO ) / 2.0f;

	prevBlock = _chain;
	for ( it =  _chainBits->begin(); it != _chainBits->end(); it++ )
	{
		currentBlock = ( *it );

		v[0] = deltaPos + currentBlock->GetPosition();
		v[1] = deltaPos + prevBlock->GetPosition();

		GLESDebugDraw::Get()->DrawSolidBox( v, b2Color( 0.0f, 0.0f, 0.0f ), 0.018f );
		prevBlock = currentBlock;
	}
}
//-----------------------------------------------------------------------------------------
void ChainDraw::DrawCut()
{
	glEnable( GL_BLEND );
	glLineWidth( 0.05f );	

	puChainCutable::ChainBits::iterator it;

	puBlock *prevBlock;
	puBlock *currentBlock;

	b2Vec2 deltaPos;
	CCSize size;
	size = CCDirector::sharedDirector()->getWinSize();

	//iphone
	deltaPos.x = -( size.width / RATIO -  Config::GameSize.width / RATIO  ) / 2.0f;
	deltaPos.y = -( size.height / RATIO -  Config::GameSize.height / RATIO ) / 2.0f;

	prevBlock = _chain;

	for ( it =  _chainBits->begin(); it != _chainBits->end(); it++ )
	{

		currentBlock = ( *it );
		glBlendFunc (GL_DST_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		GLESDebugDraw::Get()->DrawCircle( deltaPos+ currentBlock->GetPosition(), 0.15f,  b2Color( _deltaColor, _deltaColor, _deltaColor ) );

		glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_COLOR);
		GLESDebugDraw::Get()->DrawCircle( deltaPos+ currentBlock->GetPosition(), 0.15f,  _drawColor );

		prevBlock = currentBlock;
	}

	if ( _deltaColor < 0.75f ) 
	{
		_deltaColor += 0.025f;
	}
	else
	{
		_chain->CleanUp();
	}
}
//-----------------------------------------------------------------------------------------
void ChainDraw::Cut()
{
	_drawMode = eDraw_Cut;
}
//-----------------------------------------------------------------------------------------
void ChainDraw::SetDrawColor( b2Color color )
{
	_drawColor = color;
}

//-----------------------------------------------------------------------------------------






//-----------------------------------------------------------------------------------------
void puChainCutable::ChainAllocated()
{
	ChainBits::iterator it;
	for (  it = _chainBits.begin(); it != _chainBits.end(); it++ )
	{
		(*it)->SetBlockType( GameTypes::eCutable );
	}
	//_chainDraw->SetDrawColor( Config::ChainCutableColor );
	SetBlockType( GameTypes::eCutable );
}
//-----------------------------------------------------------------------------------------
void puChainStrong::ChainAllocated()
{
	ChainBits::iterator it;
	for (  it = _chainBits.begin(); it != _chainBits.end(); it++ )
	{
		(*it)->SetBlockType( GameTypes::eOther );
	}
	//_chainDraw->SetDrawColor( Config::ChainStrongColor );
	SetBlockType( GameTypes::eOther );
}
