#ifndef __PUKEYLOCKMEC_H__
#define __PUKEYLOCKMEC_H__

#include "Blocks/puBlock.h"
#include "Blocks/puBox.h"
#include "Blocks/puBankSaveWheel.h"

//-------------------------------------------------------------------------
// SingleBankLock
//-------------------------------------------------------------------------
class puSingleBankLock : public puIronBox<120,60,eBlockScaled>
{
public:
	puSingleBankLock();
	~puSingleBankLock();

	void Tick();
	bool IsFitted();
	void SetLock( float x );
	void SetLockStart( float x );

private:
	CCSprite		*_lockFitSprite;
	CCSprite		*_lockUnfitSprite;
	
	b2Vec2		_startPos;
	bool		_startPosSet;
	float		_hitPointDeltaX;
	float		_skipValue;
};



//-------------------------------------------------------------------------
// Lock shutterSlider
//-------------------------------------------------------------------------
class puBankLockShutterSlider : public puIronBox<350,23,eBlockScaled>
{
public:
	puBankLockShutterSlider();
};



//-------------------------------------------------------------------------
// Lock shutter
//-------------------------------------------------------------------------
class puBankLockShutter : public puIronBox<120,190,eBlockScaled>
{
public:
	puBankLockShutter();
};


//-------------------------------------------------------------------------
// Cog Wheel
//-------------------------------------------------------------------------
class puBankLockCogWheel : public puEmptyBlock
{
public:
	puBankLockCogWheel();
};


//-------------------------------------------------------------------------
// Cog Wheels
//-------------------------------------------------------------------------
class puBankLockCogWheels : public puEmptyBlock
{
public:
	puBankLockCogWheels();
	void SetWheelsRotation( float rotation );

private:
	puBankLockCogWheel _wheelL;
	puBankLockCogWheel _wheelR;
	
};



//-------------------------------------------------------------------------
// Bank Lock Bars
//-------------------------------------------------------------------------
class puBankLockBars : public puEmptyBlock
{
public:
	puBankLockBars();
};



//-------------------------------------------------------------------------
// BankLock
//-------------------------------------------------------------------------
class BankLockCombination;

class puBankLock : public puIronBox<420,20,eBlockScaled>
{
private:
	typedef enum 
	{
		eLockLocked = 1,
		eLockOpening,
		eLockOpened
	} LockState;

public:
	puBankLock( BankLockCombination *lockComb );
	~puBankLock();
	
	virtual void CreateJoints();
	virtual void DestroyJoints();

	void SetStartLocks( float l1, float l2, float l3 );
	void Tick();
	void LockOpen();
	bool IsLockOpened();
	void HideLocks();

private:
	LockState	_lockState;
	BankLockCombination *_lockCombination;

	puBankLockBars		_bars;

	puSingleBankLock	_ironLock1;
	puSingleBankLock	_ironLock2;
	puSingleBankLock	_ironLock3;

	puBankLockCogWheels	_cogWheel1;
	puBankLockCogWheels	_cogWheel2;
	puBankLockCogWheels	_cogWheel3;

	puWallBox<300,20,eBlockScaled>	_ironHolder1;
	puWallBox<300,20,eBlockScaled>	_ironHolder2;
	
	puBankLockShutter	_shutter1;
	puBankLockShutter	_shutter2;

	b2PrismaticJoint	*_pjoint1;
	b2PrismaticJoint	*_pjoint2;

	puBankSaveWheel	_bankSaveWheel1;
	puBankSaveWheel	_bankSaveWheel2;
	puBankSaveWheel	_bankSaveWheel3;

	CCSprite		*_lockCover;
};


//-------------------------------------------------------------------------
// BankLockCombinations
//-------------------------------------------------------------------------
class BankLockCombination
{
public:
	virtual void SetLocks( puSingleBankLock *lock1, puSingleBankLock *lock2, puSingleBankLock *lock3, puBankSaveWheel *wheel1, puBankSaveWheel *wheel2, puBankSaveWheel *wheel3, puBankLockCogWheels *cogWheel1, puBankLockCogWheels *cogWheel2, puBankLockCogWheels *cogWheel3  ) = 0;
};
//-------------------------------------------------------------------------
class BankLockCombination_1 : public BankLockCombination
{
public:
	virtual void SetLocks( puSingleBankLock *lock1, puSingleBankLock *lock2, puSingleBankLock *lock3, puBankSaveWheel *wheel1, puBankSaveWheel *wheel2, puBankSaveWheel *wheel3, puBankLockCogWheels *cogWheel1, puBankLockCogWheels *cogWheel2, puBankLockCogWheels *cogWheel3  );
};
//-------------------------------------------------------------------------
class BankLockCombination_2 : public BankLockCombination
{
public:
	virtual void SetLocks( puSingleBankLock *lock1, puSingleBankLock *lock2, puSingleBankLock *lock3, puBankSaveWheel *wheel1, puBankSaveWheel *wheel2, puBankSaveWheel *wheel3, puBankLockCogWheels *cogWheel1, puBankLockCogWheels *cogWheel2, puBankLockCogWheels *cogWheel3  );
};
//-------------------------------------------------------------------------
class BankLockCombination_3 : public BankLockCombination
{
public:
	virtual void SetLocks( puSingleBankLock *lock1, puSingleBankLock *lock2, puSingleBankLock *lock3, puBankSaveWheel *wheel1, puBankSaveWheel *wheel2, puBankSaveWheel *wheel3, puBankLockCogWheels *cogWheel1, puBankLockCogWheels *cogWheel2, puBankLockCogWheels *cogWheel3  );
};
//-------------------------------------------------------------------------
class BankLockCombination_4 : public BankLockCombination
{
public:
	virtual void SetLocks( puSingleBankLock *lock1, puSingleBankLock *lock2, puSingleBankLock *lock3, puBankSaveWheel *wheel1, puBankSaveWheel *wheel2, puBankSaveWheel *wheel3, puBankLockCogWheels *cogWheel1, puBankLockCogWheels *cogWheel2, puBankLockCogWheels *cogWheel3  );
};
//-------------------------------------------------------------------------
class BankLockCombination_5 : public BankLockCombination
{
public:
	virtual void SetLocks( puSingleBankLock *lock1, puSingleBankLock *lock2, puSingleBankLock *lock3, puBankSaveWheel *wheel1, puBankSaveWheel *wheel2, puBankSaveWheel *wheel3, puBankLockCogWheels *cogWheel1, puBankLockCogWheels *cogWheel2, puBankLockCogWheels *cogWheel3  );
};
//-------------------------------------------------------------------------

#endif
