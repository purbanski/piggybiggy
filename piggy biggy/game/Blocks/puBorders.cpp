#include <Box2D/Box2D.h>
#include <list>
#include "Levels/World.h"
#include "Levels/Level.h"
#include "puBorders.h"

using namespace std;
//-----------------------------------------------------------------------------------------------------
puBorderAllClose::puBorderAllClose()
{
	_blockEnum = eBlockBorder;

	// _otherBox1
	_otherBox1.GetBody()->GetFixtureList()->SetDensity( 5.0f );
	_otherBox1.GetBody()->GetFixtureList()->SetFriction( 0.1f );
	_otherBox1.GetBody()->GetFixtureList()->SetRestitution( 0.2f );
	_otherBox1.SetBodyType( b2_staticBody );

	// _otherBox2
	_otherBox2.GetBody()->GetFixtureList()->SetDensity( 5.0f );
	_otherBox2.GetBody()->GetFixtureList()->SetFriction( 0.1f );
	_otherBox2.GetBody()->GetFixtureList()->SetRestitution( 0.2f );
	_otherBox2.SetBodyType( b2_staticBody );

	// _otherBox3
	_otherBox3.GetBody()->GetFixtureList()->SetDensity( 5.0f );
	_otherBox3.GetBody()->GetFixtureList()->SetFriction( 0.1f );
	_otherBox3.GetBody()->GetFixtureList()->SetRestitution( 0.2f );
	_otherBox3.SetBodyType( b2_staticBody );


	// SetDeltaXY
	_otherBox1.SetDeltaXY( 48.0f, -30.0f );
	_otherBox2.SetDeltaXY( 2.0f, -62.0f );
	_otherBox3.SetDeltaXY( -46.0f, -32.0f );

	// SetNodeChain
	SetNodeChain( &_otherBox1, &_otherBox2, &_otherBox3, NULL );

	// Main block (OtherBox)
	GetBody()->GetFixtureList()->SetDensity( 5.0f );
	GetBody()->GetFixtureList()->SetFriction( 0.1f );
	GetBody()->GetFixtureList()->SetRestitution( 0.2f );
	SetBodyType( b2_staticBody );

	SetPosition( Config::GameSize.width / RATIO / 2.0f - 1.0f, Config::GameSize.height / RATIO - 1.0f);
}

//-----------------------------------------------------------------------------------------------------
puBorderU::puBorderU()
{
	_blockEnum = eBlockBorder;

	// _otherBox1
	_otherBox1.GetBody()->GetFixtureList()->SetDensity( 5.0f );
	_otherBox1.GetBody()->GetFixtureList()->SetFriction( 0.1f );
	_otherBox1.GetBody()->GetFixtureList()->SetRestitution( 0.2f );
	_otherBox1.SetBodyType( b2_staticBody );

	// _otherBox2
	_otherBox2.GetBody()->GetFixtureList()->SetDensity( 5.0f );
	_otherBox2.GetBody()->GetFixtureList()->SetFriction( 0.1f );
	_otherBox2.GetBody()->GetFixtureList()->SetRestitution( 0.2f );
	_otherBox2.SetBodyType( b2_staticBody );


	// SetDeltaXY
	_otherBox1.SetDeltaXY( 2.0f * 23.5f, 32.0f );
	_otherBox2.SetDeltaXY( 2.0f * -23.5f, 32.0f );

	// SetNodeChain
	SetNodeChain( &_otherBox1, &_otherBox2, NULL );

	// Main block (OtherBox)
	GetBody()->GetFixtureList()->SetDensity( 5.0f );
	GetBody()->GetFixtureList()->SetFriction( 0.1f );
	GetBody()->GetFixtureList()->SetRestitution( 0.2f );
	SetBodyType( b2_staticBody );

	SetPosition( 48.0f, 1.0f );
	CreateJoints();
}
//-----------------------------------------------------------------------------------------------------
puBorder::puBorder() : puBlock()
{
	_blockEnum = eBlockBorder;
	SetBorderType( eBorder_Normal );
	_blockType = GameTypes::eBorder;
	_body->SetType( b2_staticBody );
}
//-----------------------------------------------------------------------------------------------------
void puBorder::SetFilter( b2Filter filter )
{
	b2Fixture *fixture;
	fixture = _body->GetFixtureList();

	while ( fixture ) 
	{
		fixture->SetFilterData( filter );
		fixture = fixture->GetNext();
	}
}
//-----------------------------------------------------------------------------------------------------
void puBorder::SetBorderType( BorderType border )
{
	float w;

	switch ( border )
		{
		case eBorder_Normal :
			w = 20.0f;
			break;

		case eBorder_Big :
			w = 30.0f;
			break;

		default:
			w = 20.0f;
			;
	}

	SetBorderSize( w );
}
//-----------------------------------------------------------------------------------------------------
void puBorder::DestroyCurrentBorder()
{
	b2Fixture *fixture;
	FixtureContainer fixtures;
	
	fixture = _body->GetFixtureList();
	while( fixture )
	{
		fixtures.push_back( fixture );
		fixture = fixture->GetNext();
	}

	for ( FixtureContainer::iterator it = fixtures.begin(); it != fixtures.end(); it++ )
		_body->DestroyFixture( *it );
}
//-----------------------------------------------------------------------------------------------------
void puBorder::SetBorderSize( float w )
{
	b2PolygonShape box;
	b2FixtureDef fixtureDef;

	fixtureDef.shape = &box;
	fixtureDef.density = 0.0f;
	fixtureDef.friction = 1.0f;
	fixtureDef.restitution = 0.0f;
	fixtureDef.isSensor = true;

	DestroyCurrentBorder();

	// Bottom edge
	box.SetAsEdge( b2Vec2 ( -w , -w ), b2Vec2 ( Config::GameSize.width / RATIO + w, -w ));
	_body->CreateFixture( &fixtureDef );

	// Left edge
	box.SetAsEdge( b2Vec2 ( -w, -w ), b2Vec2 ( -w, w + Config::GameSize.height / RATIO ));
	_body->CreateFixture( &fixtureDef );

	// Top edge
	box.SetAsEdge( b2Vec2 ( -w , w + Config::GameSize.height / RATIO ), b2Vec2 ( Config::GameSize.width / RATIO + w, w + Config::GameSize.height / RATIO ));
	_body->CreateFixture( &fixtureDef );

	// Right edge
	box.SetAsEdge( b2Vec2 ( Config::GameSize.width / RATIO + w, -w ), b2Vec2 ( Config::GameSize.width / RATIO + w , w + Config::GameSize.height / RATIO));
	_body->CreateFixture( &fixtureDef );
}



//-----------------------------------------------------------------------------------------------------
// BorderLab
//-----------------------------------------------------------------------------------------------------
puBorderLab::puBorderLab()
{
	_blockEnum = eBlockBorder;
	SetBorderSize( 0.0f );
	_blockType = GameTypes::eBorder;
	_body->SetType( b2_staticBody );
}
//-----------------------------------------------------------------------------------------------------
void puBorderLab::SetBorderSize( float w )
{
	b2PolygonShape box;
	b2FixtureDef fixtureDef;
	b2Filter filter;

	filter.groupIndex = 0;
	filter.categoryBits = Config::eFilterCategoryCoinBorder;
	filter.maskBits = ~Config::eFilterCategoryCoin & ~Config::eFilterCategoryPiggy & 0xffff;

	fixtureDef.shape = &box;
	fixtureDef.density = 0.0f;
	fixtureDef.friction = 1.0f;
	fixtureDef.restitution = 0.0f;
	fixtureDef.filter = filter;

//	float w = Config::GameSize.width / RATIO;
//	float h = Config::GameSize.height / RATIO;

	DestroyCurrentBorder();

	// Bottom edge
	box.SetAsEdge( b2Vec2 ( -w , -w ), b2Vec2 ( Config::GameSize.width / RATIO + w, -w ));
	_body->CreateFixture( &fixtureDef );

	// Left edge
	box.SetAsEdge( b2Vec2 ( -w, -w ), b2Vec2 ( -w, w + Config::GameSize.height / RATIO ));
	_body->CreateFixture( &fixtureDef );

	// Top edge
	box.SetAsEdge( b2Vec2 ( -w , w + Config::GameSize.height / RATIO ), b2Vec2 ( Config::GameSize.width / RATIO + w, w + Config::GameSize.height / RATIO ));
	_body->CreateFixture( &fixtureDef );

	// Right edge
	box.SetAsEdge( b2Vec2 ( Config::GameSize.width / RATIO + w, -w ), b2Vec2 ( Config::GameSize.width / RATIO + w , w + Config::GameSize.height / RATIO));
	_body->CreateFixture( &fixtureDef );
}
//-----------------------------------------------------------------------------------------------------
void puBorderLab::DestroyCurrentBorder()
{
	b2Fixture *fixture;
	FixtureContainer fixtures;

	fixture = _body->GetFixtureList();
	while( fixture )
	{
		fixtures.push_back( fixture );
		fixture = fixture->GetNext();
	}

	for ( FixtureContainer::iterator it = fixtures.begin(); it != fixtures.end(); it++ )
		_body->DestroyFixture( *it );
}
