#include "puCastel.h"

#include "puCircle.h"
#include "puBox.h"

puCastel::puCastel()
{
	const int cw = 15;
	const int ch = 5;

	typedef puWoodBox<ch, cw, eBlockScaled > Stick;

	
	puBlock *b;
		
	float len;
	float w;
	float h;

	w = cw;
	h = ch;
	len = ( w - h ) / 10.0f;

	float max = 12;
	float xm = 10;


	for ( float j = max; j > 0 ; j -= 2 )
	{
		for ( float i = 0; i < j; i++ )
		{
			b = new Stick();
			b->SetPosition(  xm + i * len + ( max - j * len / 2.0f ) , (( max + 1 ) * len ) - len * j );
		}

	}

	for ( float j = max / 2; j > 0; j-- )
		for ( float i = 0; i < j; i ++ )
		{
			b = new Stick();
			b->SetPosition(xm + i * len * 2.0f + ( max -j * len ) + len / 2.0f , len * 2.0f * ( max / 2 - j + 1 ));
			b->SetRotation( 90 );
		}
}

puCastel::~puCastel()
{
}
