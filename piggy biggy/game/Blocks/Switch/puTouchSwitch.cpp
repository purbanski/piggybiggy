#include "puTouchSwitch.h"
#include "Levels/World.h"
#include "Levels/Level.h"

puTouchSwitch::puTouchSwitch()
{
	_machine = NULL;
	
	b2PolygonShape box;
	box.SetAsBox( 2.4f, 2.4f );

	b2FixtureDef fixtureDef;
	fixtureDef.shape = &box;
	fixtureDef.density = 0.0f;
	fixtureDef.restitution = 0.0f;
	fixtureDef.isSensor = true;
	
	_body->CreateFixture( &fixtureDef );
	_body->SetType( b2_staticBody );
	_blockType = GameTypes::eTouchSwitch;

	_state = eOFF;

	_spriteON = CCSprite::spriteWithFile( Skins::GetSkinName( "Images/Blocks/SwitchBoxON.png" ) );
	_spriteOFF = CCSprite::spriteWithFile( Skins::GetSkinName( "Images/Blocks/SwitchBoxOFF.png" ) );
	
	_sprite = _spriteOFF; // this one gets added as part of construction
//	World::_GWorld->addChild( _spriteON, Level::eSprites );
	//QFIX
				
	_spriteON->setIsVisible( false );
}
//-----------------------------------------------------------------------------
puTouchSwitch::~puTouchSwitch()
{
}
//-----------------------------------------------------------------------------
void puTouchSwitch::SetState( SwitchState state )
{
	if ( ! _machine ) 
		return;

	if ( state == eON )
	{
		//_sprite = _spriteON;
		//_spriteOFF->setIsVisible( false );
		//_spriteON->setIsVisible( true );
		_machine->Activate();
	}
	else
	{
		//_sprite = _spriteOFF;
		//_spriteON->setIsVisible( false );
		//_spriteOFF->setIsVisible( true );
		_machine->Deactivate();
	}
}
