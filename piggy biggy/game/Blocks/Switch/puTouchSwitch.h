#ifndef __PU_TOUCHSWITCH_H__
#define __PU_TOUCHSWITCH_H__

#include "../puBlock.h"

class puTouchSwitch : public puBlock
{
public:
	typedef enum 
	{
		eON = 1,
		eOFF
	} SwitchState;
	
	puTouchSwitch();
	virtual ~puTouchSwitch();

	SwitchState	GetState()						{ return _state; }
	puBlock*	GetMachine()					{ return _machine; }
	void		SetMachine( puBlock *block )	{ _machine = block; }
	void		SetState( SwitchState state );

protected:
	int				_signalONCount;
	puBlock			*_machine;
	SwitchState		_state;

	CCSprite		*_spriteON;
	CCSprite		*_spriteOFF;
};

#endif
