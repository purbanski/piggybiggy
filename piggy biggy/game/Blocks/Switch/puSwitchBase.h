#ifndef __PU_SWITCHBASE_H__
#define __PU_SWITCHBASE_H__

#include "../puBlock.h"

class Level;

class puSwitchBase : public puBlock
{
public:
	typedef enum 
	{
		eON = 1,
		eOFF
	} SwitchState;
	
	puSwitchBase();
	virtual ~puSwitchBase();

	SwitchState	GetState()						{ return _state; }
	puBlock*	GetMachine()					{ return _target; }
	void		SetTarget( puBlock *block )	{ _target = block; }
	void		SetState( SwitchState state );

protected:
	int				_signalONCount;
	puBlock			*_target;
	SwitchState		_state;

	CCSprite		*_spriteON;
	CCSprite		*_spriteOFF;
};

#endif
