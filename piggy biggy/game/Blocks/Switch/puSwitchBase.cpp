#include "puSwitchBase.h"
#include "Levels/World.h"
#include "Levels/Level.h"

puSwitchBase::puSwitchBase()
{
	_target = NULL;
	_signalONCount = 0;
	
	b2CircleShape shape;
	shape.m_radius = 2.0f;

	b2FixtureDef fixtureDef;
	fixtureDef.shape = &shape;
	fixtureDef.density = 0.0f;
	fixtureDef.restitution = 0.0f;
	fixtureDef.isSensor = true;
	
	_body->CreateFixture( &fixtureDef );
	_body->SetType( b2_staticBody );
	_blockType = GameTypes::eSwitch;

	_state = eOFF;

	_spriteON = CCSprite::spriteWithFile( Skins::GetSkinName( "Images/Blocks/SwitchBoxON.png" ) );
	_spriteOFF = CCSprite::spriteWithFile( Skins::GetSkinName( "Images/Blocks/SwitchBoxOFF.png" ) );
	_spriteON->retain();
	_spriteOFF->retain();

	//_sprite = _spriteOFF; // this one gets added as part of construction
	//_sprite = NULL;
	_sprite = _spriteOFF;
	_spriteZOrder = GameTypes::eSpritesBeloweX2;
	_spriteON->setIsVisible( false );
}
//-----------------------------------------------------------------------------
puSwitchBase::~puSwitchBase()
{
	//_spriteON->removeFromParentAndCleanup( true );
	//_spriteOFF->removeFromParentAndCleanup( true );
	_spriteON->release();
	_spriteOFF->release();
}
//-----------------------------------------------------------------------------
void puSwitchBase::SetState( SwitchState state )
{
	if ( ! _target ) 
		return;

	if ( state == eON )
	{
		_signalONCount++;
	}
	else
	{
		_signalONCount--;
	}

	if ( _signalONCount == 0 )
	{
		_sprite = _spriteOFF;
		_spriteON->setIsVisible( false );
		_spriteOFF->setIsVisible( true );
		_target->Deactivate();
	}
	// so 0.1.2.3 ( && state= eON ) is for 3.2._1_.0
	else if ( _signalONCount == 1 && state == eON )
	{
		_sprite = _spriteON;
		_spriteOFF->setIsVisible( false );
		_spriteON->setIsVisible( true );
		_target->Activate();
	}
}
