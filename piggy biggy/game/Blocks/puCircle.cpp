#include <sstream>
#include <Box2D/Box2D.h>
#include "puCircle.h"
#include "puBlocksSettings.h"
#include "Levels/Level.h"
#include "Debug/MyDebug.h"
#include "GameConfig.h"
#include "Tools.h"
#include "Components/CustomSprites.h"
#include "Animation/AnimManager.h"
#include "Animation/AnimTools.h"
#include "Animation/AnimSpritesCreator.h"
#include "BlockNames.h"
#include "unFile.h"
#include "Platform/Lang.h"

using namespace pu::blocks::settings;

//fix me - leak memory leak
set<GameTypes::BlockEnum> CirclesClassNames()
{
	set<GameTypes::BlockEnum> names;

	names.insert( eBlockPiggyBank );
	names.insert( eBlockPiggyBankStatic );
	names.insert( eBlockCoin );
	names.insert( eBlockCoinSilver );
	names.insert( eBlockCoinStatic );
	names.insert( eBlockBomb );
	names.insert( eBlockBombStatic );
	names.insert( eBlockMagnetBomb );
	names.insert( eBlockCop);
	names.insert( eBlockThief );
	names.insert( eBlockThiefStatic );
	names.insert( eBlockButton );
	
	/*names.insert( "Circle" );
	names.insert( "CircleFragile" );
	names.insert( "CircleFragileStatic" );
	names.insert( "CircleStone" );
	names.insert( "CircleWall" );
	names.insert( "CircleWood" );
	names.insert( "Screw" );
	names.insert( "Reel" );
	names.insert( "RotationWheel" );
	names.insert( "MagnetBombPlus" );
	names.insert( "MagnetBombMinus" );
	names.insert( "Magnet" );
	names.insert( "MagnetPlus" );
	names.insert( "MagnetMinus" );
	names.insert( "TyreA" );
	names.insert( "PiggyFarm");
	names.insert( "TennisBall");
	names.insert( "Glass");*/

	return names;
}


//-----------------------------------------------------------------------------------------------------
// Circle Base
//-----------------------------------------------------------------------------------------------------
puCircleBase::puCircleBase( float radius ) : _radius( radius )
{

	_blockType = GameTypes::eOther;
	_shapeType = GameTypes::eShape_Circle;
}
//-----------------------------------------------------------------------------------------------------
void puCircleBase::LoadDefaultSprite()
{
	string filename;
	stringstream ss;

	float radius = ( _radius * RATIO );
	int iRadius = (int) radius;

	ss << "Images/Blocks/" << GetClassName2() ;
	ss << iRadius << ".png";


	filename.append( Skins::GetSkinName( ss.str().c_str() ) );
	//filename.append( ss.str() );
	D_LOG( "Looking for: %s", filename.c_str() )


		if ( unResourceFileCheckExists( filename.c_str() ) )
		{
//			DS_LOG_MISSING_PNG( "+ %s%d.png", GetClassName2().c_str(), (int) ( _radius * 10.0f ))

			_sprite = CCSprite::spriteWithFile( filename.c_str() );
			SetSpriteZOrder( GameTypes::eSpritesAbove );
		}
		else
		{
			D_LOG( "Missing: %s", filename.c_str() )
//			DS_LOG_MISSING_PNG( "- %s%d.png", GetClassName2().c_str(), (int) ( _radius * 10.0f ))

#ifdef DEBUG_MEGA
			unAssertMsg(CircleBase, false, ("Missing: %s", filename.c_str() ));
#endif
			if ( ! puBlock::LoadDefaultSprite() )
				return;

			SetDefaultSpriteScale();
		}
}
//-----------------------------------------------------------------------------------------------------
void puCircleBase::LoadDefaultSpriteStatic()
{

	LoadDefaultSprite();
	_glueSprite = GlueCircleSprite::Create( _radius );
}
//-----------------------------------------------------------------------------------------------------
void puCircleBase::SetDefaultSpriteScale()
{

	float scalex = ( 2.0f * _radius / _sprite->getContentSizeInPixels().width ) * RATIO;
	float scaley = ( 2.0f * _radius / _sprite->getContentSizeInPixels().height ) * RATIO;

	//DS_LOG( "Rescaling default scale x:%f , y:%f", scalex, scaley )
	_sprite->setScaleX( scalex );
	_sprite->setScaleY( scaley );
}
//-----------------------------------------------------------------------------------------------------



//-----------------------------------------------------------------------------------------------------
// Circle Static
//-----------------------------------------------------------------------------------------------------
void puCircle_::Construct( float radius )
{
	_width = 2.0f * radius;
	_height = 2.0f * radius;
	LoadDefaultSprite();

	b2CircleShape shape;
	shape.m_radius = radius;
	
	b2FixtureDef fixtureDef;
	fixtureDef.shape = &shape;
	fixtureDef.density = 4.0f;
	fixtureDef.restitution = 0.5f;

	_body->CreateFixture( &fixtureDef );
	_shapeType = eShape_Circle;
}



//-----------------------------------------------------------------------------------------------------
// CircleFragile
//-----------------------------------------------------------------------------------------------------
void puCircleFragile_::Construct( float radius )
{
	_width = 2.0f * radius;
	_height = 2.0f * radius;
	_blockEnum = eBlockCircleFragile;

	LoadDefaultSprite();

	b2CircleShape shape;
	shape.m_radius = radius;

	b2FixtureDef fixtureDef;
	fixtureDef.shape = &shape;
	fixtureDef.density = FragileDensity;
	fixtureDef.restitution = FragileRestitution;
	fixtureDef.friction = FragileFriction;

	_body->CreateFixture( &fixtureDef );
	SetBodyType( b2_dynamicBody );
	SetBlockType( GameTypes::eDestroyable );
	_shapeType = eShape_Circle;
	_spriteZOrder = GameTypes::eSprites;

	PreloadResources( radius );
}
//-----------------------------------------------------------------------------------------------------
void puCircleFragile_::DestroyFragile()
{
	AnimerSimpleEffect::Get()->Anim_FragileDestroy( this );
	if ( ! _level->IsDemoRunning() )
	{
		SoundEngine::Get()->PlayRandomEffect( 
			10.0f, 
			SoundEngine::eSoundFragileDestroyed1,
			SoundEngine::eSoundFragileDestroyed2,
			SoundEngine::eSoundFragileDestroyed3,
			SoundEngine::eSoundFragileDestroyed4,
			SoundEngine::eSoundNone );
	}
	DestroyBody();
}
//-----------------------------------------------------------------------------------------------------


//-----------------------------------------------------------------------------------------------------
// CircleFragileStatic
//-----------------------------------------------------------------------------------------------------
void puCircleFragileStatic_::Construct( float radius )
{
	_width = 2.0f * radius;
	_height = 2.0f * radius;
	_blockEnum = eBlockCircleFragileStatic;

	LoadDefaultSpriteStatic();

	b2CircleShape shape;
	shape.m_radius = radius;

	b2FixtureDef fixtureDef;
	fixtureDef.shape = &shape;
	// FIXME: fragile vs fragilestatic density etc
	fixtureDef.density = FragileDensity;
	fixtureDef.restitution = FragileRestitution;
	fixtureDef.friction = FragileFriction;

	_body->CreateFixture( &fixtureDef );
	SetBodyType( b2_staticBody );
	SetBlockType( GameTypes::eDestroyable );
	_shapeType = eShape_Circle;
	_spriteZOrder = GameTypes::eSprites;
	
	PreloadResources( radius );
}
//-----------------------------------------------------------------------------------------------------
void puCircleFragileStatic_::DestroyFragile()
{
	AnimerSimpleEffect::Get()->Anim_FragileDestroy( this );
	if ( ! _level->IsDemoRunning() )
		SoundEngine::Get()->PlayRandomEffect( 
		10.0f,
		SoundEngine::eSoundFragileDestroyed1,
		SoundEngine::eSoundFragileDestroyed2,
		SoundEngine::eSoundFragileDestroyed3,
		SoundEngine::eSoundFragileDestroyed4,
		SoundEngine::eSoundNone );

	DestroyBody();
	AnimerSimpleEffect::Get()->Anim_GlueDestroy( _glueSprite );
}
//-----------------------------------------------------------------------------------------------------


//-----------------------------------------------------------------------------------------------------
// Circle Stone
//-----------------------------------------------------------------------------------------------------
void puCircleStone_::Construct( float radius )
{
	_width = 2.0f * radius;
	_height = 2.0f * radius;
	LoadDefaultSprite();

	b2CircleShape shape;
	shape.m_radius = radius;

	b2FixtureDef fixtureDef;
	fixtureDef.shape = &shape;
	fixtureDef.density = StoneDensity;
	fixtureDef.restitution = StoneRestitution;
	fixtureDef.friction = StoneFriction;

	_body->CreateFixture( &fixtureDef );
	SetBodyType( b2_dynamicBody );
	_shapeType = eShape_Circle;
	_spriteZOrder = GameTypes::eSprites;
}
//-----------------------------------------------------------------------------------------------------



//-----------------------------------------------------------------------------------------------------
// Circle Wall
//-----------------------------------------------------------------------------------------------------
void puCircleWall_::Construct( float radius )
{
	_blockEnum = eBlockCircleWall;

	_width = 2.0f * radius;
	_height = 2.0f * radius;
	LoadDefaultSprite();

	b2CircleShape shape;
	shape.m_radius = radius;

	b2FixtureDef fixtureDef;
	fixtureDef.shape = &shape;
	fixtureDef.density = WallDensity;
	fixtureDef.restitution = WallRestitution;
	fixtureDef.friction = WallFriction;

	_body->CreateFixture( &fixtureDef );
	_spriteZOrder = GameTypes::eSpritesWall;
	SetBodyType( b2_staticBody );
	_shapeType = eShape_Circle;
}
//-----------------------------------------------------------------------------------------------------



//-----------------------------------------------------------------------------------------------------
// Circle Wood
//-----------------------------------------------------------------------------------------------------
void puCircleWood_::Construct( float radius )
{
	_width = 2.0f * radius;
	_height = 2.0f * radius;
	LoadDefaultSprite();

	b2CircleShape shape;
	shape.m_radius = radius;

	b2FixtureDef fixtureDef;
	fixtureDef.shape = &shape;
	fixtureDef.density = WoodDensity;
	fixtureDef.restitution = WoodRestitution;
	fixtureDef.friction = WoodFriction;

	_body->CreateFixture( &fixtureDef );
	_spriteZOrder = GameTypes::eSprites;
	SetBodyType( b2_dynamicBody );
	_shapeType = eShape_Circle;
}
//-----------------------------------------------------------------------------------------------------




//-----------------------------------------------------------------------------------------------------
// Circle Iron
//-----------------------------------------------------------------------------------------------------
void puCircleIron_::Construct( float radius )
{
	_width = 2.0f * radius;
	_height = 2.0f * radius;
	LoadDefaultSprite();

	b2CircleShape shape;
	shape.m_radius = radius;

	b2FixtureDef fixtureDef;
	fixtureDef.shape = &shape;
	fixtureDef.density = IronDensity;
	fixtureDef.restitution = IronRestitution;
	fixtureDef.friction = IronFriction;

	_body->CreateFixture( &fixtureDef );
	SetBodyType( b2_dynamicBody );
	SetBlockType( GameTypes::eIron );
	_spriteZOrder = GameTypes::eSprites;
	_shapeType = eShape_Circle;
}
//-----------------------------------------------------------------------------------------------------


//-----------------------------------------------------------------------------------------------------
// Thief Rolled
//-----------------------------------------------------------------------------------------------------
void puThiefRolled_::Construct( float radius )
{
	_blockEnum = eBlockThiefRolled;

	_width = 2.0f * radius;
	_height = 2.0f * radius;

	b2CircleShape shape;
	shape.m_radius = radius;

	b2FixtureDef fixtureDef;
	fixtureDef.shape = &shape;
	fixtureDef.density = ThiefDensity;
	fixtureDef.restitution = ThiefRestitution;
	fixtureDef.friction = ThiefFriction;

	_body->CreateFixture( &fixtureDef );
	SetBodyType( b2_dynamicBody );
	_blockType = GameTypes::eThief;
	_shapeType = eShape_Circle;

	CalculateArea();

	_scaleToDefault = radius / ( 60.0f / RATIO );
	_sprite = AnimManager::Get()->CreateRollSprite( this ); 
	_spriteShadow = AnimManager::Get()->CreateRollSprite( this ); 
	_spriteZOrder = GameTypes::eSpritesCharacter;
	
	//AnimManager::Get()->SetSpriteState( this, eSpriteState_Roll ); // we reset afterwards therefore make no sens
	Preloader::Get()->AddThiefRolledAnimSet( radius * RATIO );
}
//-----------------------------------------------------------------------------------------------------


//-----------------------------------------------------------------------------------------------------
// Thief
//-----------------------------------------------------------------------------------------------------
void puThief_::Construct( float radius )
{
	_blockEnum = eBlockThief;

	_width = 2.0f * radius;
	_height = 2.0f * radius;

	b2CircleShape shape;
	shape.m_radius = radius;

	b2FixtureDef fixtureDef;
	fixtureDef.shape = &shape;
	fixtureDef.density = ThiefDensity;
	fixtureDef.restitution = ThiefRestitution;
	fixtureDef.friction = ThiefFriction;

	_body->CreateFixture( &fixtureDef );
	SetBodyType( b2_dynamicBody );
	_blockType = GameTypes::eThief;
	_shapeType = eShape_Circle;

	CalculateArea();

	_scaleToDefault = radius / ( 60.0f / RATIO );
	_sprite = AnimManager::Get()->CreateSitSprite( this ); 
	_spriteShadow = AnimManager::Get()->CreateSitSprite( this ); 
	_spriteZOrder = GameTypes::eSpritesCharacter;
	

	Preloader::Get()->AddThiefAnimSet( radius * RATIO );
}
//-----------------------------------------------------------------------------------------------------



//-----------------------------------------------------------------------------------------------------
// Thief Static
//-----------------------------------------------------------------------------------------------------
void puThiefStatic_::Construct( float radius )
{
	_blockEnum = eBlockThiefStatic;

	_width = 2.0f * radius;
	_height = 2.0f * radius;

	b2CircleShape shape;
	shape.m_radius = radius;

	b2FixtureDef fixtureDef;
	fixtureDef.shape = &shape;
	fixtureDef.density = ThiefDensity;
	fixtureDef.restitution = ThiefRestitution;
	fixtureDef.friction = ThiefFriction;

	_body->CreateFixture( &fixtureDef );
	SetBodyType( b2_staticBody );
	_spriteZOrder = GameTypes::eSprites;
	_blockType = GameTypes::eThief;
	_shapeType = eShape_Circle;

	CalculateArea();

	_scaleToDefault = radius / ( 60.0f / RATIO );
	_sprite = AnimManager::Get()->CreateSitSprite( this ); 
	_spriteShadow = AnimManager::Get()->CreateSitSprite( this ); 
	_spriteZOrder = GameTypes::eSpritesCharacter;
	_glueSprite = GlueCircleSprite::Create( _radius );

	Preloader::Get()->AddThiefAnimSet( radius * RATIO );
}
//-----------------------------------------------------------------------------------------------------
void puThiefStatic_::Destroy()
{
	puBlock::Destroy();
	AnimerSimpleEffect::Get()->Anim_GlueDestroy( _glueSprite );
}

//-----------------------------------------------------------------------------------------------------



//-----------------------------------------------------------------------------------------------------
// Coin
//-----------------------------------------------------------------------------------------------------
puCoin_::~puCoin_()
{
	// fix me
	// shouldn't it be in ~puBlock() ?
	// it is for sure needed here otherwise it's not forcing
	// to remove flash and ray - this is really a hack
	CC_SAFE_RELEASE_NULL(_sprite);
}
//-----------------------------------------------------------------------------------------------------
void puCoin_::Construct( float radius )
{
	
	_blockEnum = eBlockCoin;
	_width = 2.0f * radius;
	_height = 2.0f * radius;

	b2CircleShape shape;
	shape.m_radius = radius;

	b2FixtureDef fixtureDef;
	fixtureDef.shape = &shape;
	fixtureDef.density = CoinDensity;
	fixtureDef.restitution = CoinRestitution;
	fixtureDef.friction = CoinFriction;

	_body->CreateFixture( &fixtureDef );

	_blockType = GameTypes::eCoin;
	_shapeType = eShape_Circle;
	_spriteZOrder = GameTypes::eSprites;
	SetBodyType( b2_dynamicBody );

	LoadDefaultSprite();
	_sprite = CoinSprite::Create( this );
	_spriteShadow = CCSprite::spriteWithFile( Skins::GetSkinName( "Images/Blocks/Coin70.png" ) );
	_spriteShadow->setScale( radius / 7.0f );
	
	CalculateArea();

	Preloader::Get()->AddSound( SoundEngine::eSoundCoinCollected );
	Preloader::Get()->AddSkinImage( "Images/Particles/Coin.png" );
}
//-----------------------------------------------------------------------------------------------------
void puCoin_::Destroy()
{
	_level->GetParticleEngine()->Effect_CoinCollected( _level, this );
	DestroyBody();

	if ( ! _sprite ) 
		return;

	CoinSprite* sprite;
	sprite = (CoinSprite *) _sprite;

	sprite->stopAllActions();
	sprite->runAction( CCFadeTo::actionWithDuration( 0.3f, 0 ));
	sprite->runAction( CCScaleTo::actionWithDuration( 0.3f, 0.1f ));

	_spriteShadow->stopAllActions();
	_spriteShadow->runAction( CCFadeTo::actionWithDuration( 0.3f, 0 ));
	_spriteShadow->runAction( CCScaleTo::actionWithDuration( 0.3f, 0.1f ));
}
//----------------------------------------------------------------------------



//-----------------------------------------------------------------------------------------------------
// Coin Silver
//-----------------------------------------------------------------------------------------------------
void puCoinSilver_::Construct( float radius )
{
	_blockEnum = eBlockCoinSilver;
	_moneyMade = false;
	_stopChecking = false;
	
	SetBodyType( b2_dynamicBody );
	_blockType = GameTypes::eCoinSilver;
	_shapeType = eShape_Circle;

	_coin1.GetBody()->SetType( b2_staticBody );
	_coin1.SetPosition( b2Vec2( 16.0f, -16.0f ));

	_coin2.GetBody()->SetType( b2_staticBody );
	_coin2.SetPosition( b2Vec2( 16.0f, -16.0f ));
	_spriteZOrder = GameTypes::eSprites;

	CalculateArea();

	LoadDefaultSprite();
	//_spriteShadow = _sprite;
	_sprite = CoinSprite::Create( this );

	Preloader::Get()->AddSound( SoundEngine::eSoundCoinCollected );
	Preloader::Get()->AddSound( SoundEngine::eSoundMoneyMakeMoney );
	Preloader::Get()->AddSkinImage( "Images/Particles/CoinSilver.png" );
	Preloader::Get()->AddFont( Config::LevelMsgFont );
}
//-----------------------------------------------------------------------------------------------------
void puCoinSilver_::MoneyMakeMoney()
{
	_moneyMade = true;
	_coin1.GetBody()->SetType( b2_dynamicBody );
	_coin2.GetBody()->SetType( b2_dynamicBody );
	AnimerSimpleEffect::Get()->ShowMsg( _level, LocalString("MoneyMakeMoneyMsg"), GameTypes::eLevelMsg, 2.0f, false );
}
//----------------------------------------------------------------------------
void puCoinSilver_::Step()
{
	if ( _stopChecking || ! _moneyMade )
		return;

	_coin1.SetPosition( GetReversedPosition() );
	_coin2.SetPosition( GetReversedPosition() );

	_stopChecking = true;
}
//----------------------------------------------------------------------------
void puCoinSilver_::Destroy()
{
	CoinSprite* sprite;
	sprite = (CoinSprite *) _sprite;

	if ( ! sprite ) 
		return;

	if ( ! _moneyMade )
	{
		_level->GetParticleEngine()->Effect_CoinCollected( _level, this );
	}
	else
	{
		_level->GetParticleEngine()->Effect_Stars( _level, GetPosition() );
		if ( ! _level->IsDemoRunning() )
			SoundEngine::Get()->PlayEffect( SoundEngine::eSoundMoneyMakeMoney );
	}
	
	DestroyBody();

	sprite->stopAllActions();
	sprite->runAction( CCFadeTo::actionWithDuration( 0.3f, 0 ));
	sprite->runAction( CCScaleTo::actionWithDuration( 0.3f, 0.1f ));

	if ( _spriteShadow ) 
	{
		_spriteShadow->stopAllActions();
		_spriteShadow->runAction( CCFadeTo::actionWithDuration( 0.3f, 0 ));
		_spriteShadow->runAction( CCScaleTo::actionWithDuration( 0.3f, 0.1f ));
	}
}
//----------------------------------------------------------------------------



//-----------------------------------------------------------------------------------------------------
// CoinStatic
//-----------------------------------------------------------------------------------------------------
void puCoinStatic_::Construct( float radius )
{
	_blockEnum = eBlockCoinStatic;

	_width = 2.0f * radius;
	_height = 2.0f * radius;

	b2CircleShape shape;
	shape.m_radius = radius;

	b2FixtureDef fixtureDef;
	fixtureDef.shape = &shape;
	fixtureDef.density = CoinDensity;
	fixtureDef.restitution = CoinRestitution;
	fixtureDef.friction = CoinFriction;

	_body->CreateFixture( &fixtureDef );
	SetBodyType( b2_staticBody );
	_blockType = GameTypes::eCoin;
	_shapeType = eShape_Circle;
	_spriteZOrder = GameTypes::eSprites;

	LoadDefaultSpriteStatic();
	//_spriteShadow = _sprite;
	_sprite = CoinSprite::Create( this );
	

	Preloader::Get()->AddSound( SoundEngine::eSoundCoinCollected );
	Preloader::Get()->AddSkinImage( "Images/Particles/CoinStatic.png" );
}
//-----------------------------------------------------------------------------------------------------
void puCoinStatic_::Destroy()
{
	puCoin_::Destroy();
	AnimerSimpleEffect::Get()->Anim_GlueDestroy( _glueSprite );
}
//-----------------------------------------------------------------------------------------------------



//-----------------------------------------------------------------------------------------------------
// Cop
//-----------------------------------------------------------------------------------------------------
void puCop_::Construct( float radius )
{
	_blockEnum = eBlockCop;

	_width = 2.0f * radius;
	_height = 2.0f * radius;

	b2CircleShape shape;
	shape.m_radius = radius;

	b2FixtureDef fixtureDef;
	fixtureDef.shape = &shape;
	fixtureDef.density = CopDensity;
	fixtureDef.restitution = CopRestitution;
	fixtureDef.friction = CopFriction;

	_body->CreateFixture( &fixtureDef );
	SetBodyType( b2_dynamicBody );
	_blockType = GameTypes::eCop;
	_shapeType = eShape_Circle;
	_spriteZOrder = GameTypes::eSprites;

	//LoadDefaultSprite();
	_scaleToDefault = radius / ( 60.0f / RATIO );
	_sprite = AnimManager::Get()->CreateSitSprite( this ); 
	_spriteShadow = AnimManager::Get()->CreateSitSprite( this ); 
	_spriteZOrder = GameTypes::eSpritesCharacter;

	Preloader::Get()->AddCopAnimSet( radius * RATIO );
}
//----------------------------------------------------------------------------




//-----------------------------------------------------------------------------------------------------
// PiggyBankRolled
//-----------------------------------------------------------------------------------------------------
void puPiggyBankRolled_::Construct( float radius )
{
	_blockEnum = eBlockPiggyBankRolled;

	_width = 2.0f * radius;
	_height = 2.0f * radius;

	b2CircleShape shape;
	shape.m_radius = radius;

	b2FixtureDef fixtureDef;
	fixtureDef.shape = &shape;
	fixtureDef.density = PiggyBankDensity;
	fixtureDef.restitution = PiggyBankRestitution;
	fixtureDef.friction = PiggyBankFriction;

	_body->CreateFixture( &fixtureDef );

	_blockType = GameTypes::ePiggyBank;
	_shapeType = eShape_Circle;
	SetBodyType( b2_dynamicBody );

	//LoadDefaultSprite();
	CalculateArea();

	_scaleToDefault = radius / ( 60.0f / RATIO );
	_sprite = AnimManager::Get()->CreateRollSprite( this ); 
	_spriteShadow = AnimManager::Get()->CreateRollSprite( this ); 
	_spriteZOrder = GameTypes::eSpritesPiggyBank;

	Preloader::Get()->AddPiggyRolledAnimSet( radius * RATIO );
}
//----------------------------------------------------------------------------




//-----------------------------------------------------------------------------------------------------
// PiggyBank
//-----------------------------------------------------------------------------------------------------
void puPiggyBank_::Construct( float radius )
{

	_blockEnum = eBlockPiggyBank;
	_width = 2.0f * radius;
	_height = 2.0f * radius;
	
	b2CircleShape shape;
	shape.m_radius = radius;

	b2FixtureDef fixtureDef;
	fixtureDef.shape = &shape;
	fixtureDef.density = PiggyBankDensity;
	fixtureDef.restitution = PiggyBankRestitution;
	fixtureDef.friction = PiggyBankFriction;

	_body->CreateFixture( &fixtureDef );
	
	_blockType = GameTypes::ePiggyBank;
	_shapeType = eShape_Circle;
	SetBodyType( b2_dynamicBody );

	//LoadDefaultSprite();
	CalculateArea();
	
	_scaleToDefault = radius / ( 60.0f / RATIO );
	_sprite = AnimManager::Get()->CreateSitSprite( this ); 
	_spriteShadow = AnimManager::Get()->CreateSitSprite( this ); 
	_spriteZOrder = GameTypes::eSpritesPiggyBank;
	
	Preloader::Get()->AddPiggyAnimSet( this );
}
//----------------------------------------------------------------------------



//-----------------------------------------------------------------------------------------------------
// PiggyBankStatic
//-----------------------------------------------------------------------------------------------------
void puPiggyBankStatic_::Construct( float radius )
{

	_blockEnum	= eBlockPiggyBankStatic;
	_width		= 2.0f * radius;
	_height		= 2.0f * radius;

	b2CircleShape shape;
	shape.m_radius = radius;

	b2FixtureDef fixtureDef;
	fixtureDef.shape = &shape;
	fixtureDef.density = PiggyBankDensity;
	fixtureDef.restitution = PiggyBankRestitution;
	fixtureDef.friction = PiggyBankFriction;

	_body->CreateFixture( &fixtureDef );
	SetBodyType( b2_staticBody );
	_blockType = GameTypes::ePiggyBank;
	_shapeType = eShape_Circle;
	
//	LoadDefaultSprite();
	CalculateArea();

	_scaleToDefault = radius / ( 60.0f / RATIO );
	_sprite = AnimManager::Get()->CreateSitSprite( this ); 
	_spriteShadow = AnimManager::Get()->CreateSitSprite( this ); 
	_spriteZOrder = GameTypes::eSpritesPiggyBank;
	_glueSprite = GlueCircleSprite::Create( _radius );

	Preloader::Get()->AddPiggyStaticAnimSet( radius * RATIO );
}
//----------------------------------------------------------------------------
void puPiggyBankStatic_::Destroy()
{
	puBlock::Destroy();
	AnimerSimpleEffect::Get()->Anim_GlueDestroy( _glueSprite );
}
//----------------------------------------------------------------------------



//-----------------------------------------------------------------------------------------------------
// Tyre1
//-----------------------------------------------------------------------------------------------------
void puTyreA_::Construct( float radius )
{
	_width		= 2.0f * radius;
	_height		= 2.0f * radius;
	_blockEnum	= eBlockTyre;

	b2CircleShape shape;
	shape.m_radius = radius;

	b2FixtureDef fixtureDef;
	fixtureDef.shape = &shape;
	fixtureDef.density = TyreDensity;
	fixtureDef.restitution = TyreRestitution;
	fixtureDef.friction = TyreFriction;

	_body->CreateFixture( &fixtureDef );
	
	SetBodyType( b2_dynamicBody );
	_blockType = GameTypes::eOther;
	_shapeType = eShape_Circle;
	
	LoadDefaultSprite();
	_spriteZOrder = GameTypes::eSpritesAboveX3;
}
//----------------------------------------------------------------------------



//-----------------------------------------------------------------------------------------------------
// Screw
//-----------------------------------------------------------------------------------------------------
void puScrew_::Construct( float radius )
{
	_width	= 2.0f * radius;
	_height = 2.0f * radius;
	_blockEnum = eBlockScrew;
	LoadDefaultSprite();

	b2CircleShape shape;
	shape.m_radius = radius;

	b2Filter filter;
	filter.categoryBits = 0;
	filter.groupIndex = -1;
	filter.maskBits = 0xffff;

	b2FixtureDef fixtureDef;
	fixtureDef.shape = &shape;
	fixtureDef.density = 0.1f;
	fixtureDef.friction = 0.1f;
	fixtureDef.restitution = 0.1f;
	fixtureDef.filter = filter;

	_body->CreateFixture( &fixtureDef );
	_body->SetType( b2_staticBody );
	_blockType = GameTypes::eOther;
	_shapeType = eShape_Circle;
	_spriteZOrder = GameTypes::eSpritesAbove;
}
//-----------------------------------------------------------------------------------------------------




//-----------------------------------------------------------------------------------------------------
// Circle Glass
//-----------------------------------------------------------------------------------------------------
void puCircleGlass_::Construct( float radius )
{
	_width = 2.0f * radius;
	_height = 2.0f * radius;
	LoadDefaultSprite();

	b2CircleShape shape;
	shape.m_radius = radius;

	b2FixtureDef fixtureDef;
	fixtureDef.shape = &shape;
	fixtureDef.density = GlassDensity;
	fixtureDef.friction = GlassFriction;
	fixtureDef.restitution = GlassRestitution;
	SetSpeedBreak( GlassBreakSpeed );

	_body->CreateFixture( &fixtureDef );
	_body->SetType( b2_dynamicBody );
	_blockType = GameTypes::eGlass;
	_shapeType = eShape_Circle;

	SetSpriteZOrder( GameTypes::eSprites );
}
//-----------------------------------------------------------------------------------------------------



//-----------------------------------------------------------------------------------------------------
// Reel
//-----------------------------------------------------------------------------------------------------
void puReel_::Construct( float radius )
{
	_blockEnum = eBlockReel;
	_width = radius * 2.0f;
	_height = radius * 2.0f;
	_spriteZOrder = GameTypes::eSpritesBelowe;

	char filename[64];
	snprintf( &filename[0], 64, Skins::GetSkinName( "Images/Blocks/%s.png" ) , GetClassName2() );

	_sprite = ReelSprite::Create( filename );

	// _screw1
	_screw1.GetBody()->GetFixtureList()->SetDensity( 1.0f );
	_screw1.GetBody()->GetFixtureList()->SetFriction( 0.7f );
	_screw1.GetBody()->GetFixtureList()->SetRestitution( 0.2f );
	SetBodyType( b2_staticBody );

	_screw1.GetSprite()->setOpacity( 0 );
	// SetNodeChain
	SetNodeChain( &_screw1, NULL );

	// SetDeltaXY
	_screw1.SetDeltaXY( 0, 0 );

	// Main block
	b2CircleShape shape;
	b2FixtureDef fixtureDef;

	shape.m_radius = radius;
	fixtureDef.shape = &shape;
	_body->CreateFixture( &fixtureDef );

	_body->GetFixtureList()->SetDensity( ReelDensity );
	_body->GetFixtureList()->SetFriction( ReelFriction );
	_body->GetFixtureList()->SetRestitution( ReelRestitution );

	SetBodyType( b2_dynamicBody );
	SetBlockType( GameTypes::eMoveable );

	SetPosition( Config::GameSize.width / RATIO / 2.0f, Config::GameSize.height / RATIO / 2.0f );
	CreateJoints();
}
//-----------------------------------------------------------------------------------------------------
void puReel_::CreateJoints()
{
	b2RevoluteJointDef jointDef1;
	jointDef1.Initialize( GetBody(), _screw1.GetBody(), GetPosition() );
	_joint1 = (b2RevoluteJoint *) _world->CreateJoint( &jointDef1 );

	_joint1->EnableMotor( true );
	_joint1->SetMotorSpeed( 0.0f );
	_joint1->SetMaxMotorTorque( 6000.0f );
}
//-----------------------------------------------------------------------------------------------------
void puReel_::DestroyJoints()
{
	_world->DestroyJoint( _joint1 );
	_joint1 = NULL;
}
//-----------------------------------------------------------------------------------------------------
puReel_::~puReel_()
{
	//if ( _sprite )
	//	_sprite->release();
}


//-----------------------------------------------------------------------------------------------------
// Button
//-----------------------------------------------------------------------------------------------------
void puButton_::Construct( float radius )
{
	_blockEnum = eBlockButton;

	LoadDefaultSprite();
	_sprite = SpriteWithRay::Create( _sprite, radius, "ButtonRay" );

	_width = 2.0f * radius;
	_height = 2.0f * radius;

	b2CircleShape shape;
	shape.m_radius = radius;

	b2FixtureDef fixtureDef;
	fixtureDef.shape		= &shape;
	fixtureDef.density		= ButtonDensity;
	fixtureDef.restitution	= ButtonRestitution;
	fixtureDef.friction		= ButtonFriction;

	_body->CreateFixture( &fixtureDef );

	_spriteZOrder	= GameTypes::eSprites;
	_blockType		= GameTypes::eOther;
	_shapeType		= eShape_Circle;

	SetBodyType( b2_dynamicBody );	
	SetPosition( Config::GameSize.width / RATIO / 2.0f, Config::GameSize.height / RATIO / 2.0f );
}




//-----------------------------------------------------------------------------------------------------
// Rotation Wheel
//-----------------------------------------------------------------------------------------------------
void puRotationWheel_::Construct( float radius )
{
	_width = radius * 2;
	_height = radius * 2;
	LoadDefaultSprite();

	// _screw1
	_screw1.GetBody()->GetFixtureList()->SetDensity( 1.0f );
	_screw1.GetBody()->GetFixtureList()->SetFriction( 0.5f );
	_screw1.GetBody()->GetFixtureList()->SetRestitution( 0.2f );
	SetBodyType( b2_staticBody );

	// SetDeltaXY
	_screw1.SetDeltaXY( 0, 0 );

	// SetNodeChain
	SetNodeChain( &_screw1, NULL );

	// Main block
	b2CircleShape shape;
	b2FixtureDef fixtureDef;

	shape.m_radius = radius;
	fixtureDef.shape = &shape;
	_body->CreateFixture( &fixtureDef );

	_body->GetFixtureList()->SetDensity( ReelDensity );
	_body->GetFixtureList()->SetFriction( ReelFriction );
	_body->GetFixtureList()->SetRestitution( ReelRestitution );

	SetBodyType( b2_dynamicBody );
	SetBlockType( GameTypes::eMoveable );

	SetPosition( 0.0f, 0.0f );
	CreateJoints();
}
//-----------------------------------------------------------------------------------------------------
void puRotationWheel_::CreateJoints()
{
	b2RevoluteJointDef jointDef1;
	jointDef1.Initialize( _screw1.GetBody(), GetBody(), GetPosition() );
	_joint1 = (b2RevoluteJoint *) _world->CreateJoint( &jointDef1 );

	_joint1->EnableMotor( true );
	_joint1->SetMotorSpeed( 0.0f );
	_joint1->SetMaxMotorTorque( 500000.0f );
}
//-----------------------------------------------------------------------------------------------------
void puRotationWheel_::DestroyJoints()
{
	_world->DestroyJoint( _joint1 );
	_joint1 = NULL;
}
//-----------------------------------------------------------------------------------------------------




//-----------------------------------------------------------------------------------------------------
// Piggy Bank Little
//-----------------------------------------------------------------------------------------------------
void puPiggyBankLittle_::Construct( float radius )
{
	_width = 2.0f * radius;
	_height = 2.0f * radius;

	b2CircleShape shape;
	shape.m_radius = radius;


	b2FixtureDef fixtureDef;
	fixtureDef.shape = &shape;
	fixtureDef.density = PiggyBankDensity;
	fixtureDef.restitution = PiggyBankRestitution;
	fixtureDef.friction = PiggyBankFriction;

	_body->CreateFixture( &fixtureDef );

	_blockType = GameTypes::ePiggyBank;
	_shapeType = eShape_Circle;
	SetBodyType( b2_dynamicBody );

	CalculateArea();

	_scaleToDefault = radius / ( 60.0f / RATIO );
	_sprite = AnimManager::Get()->CreateSitSprite( this ); 
	_spriteShadow = AnimManager::Get()->CreateSitSprite( this ); 
	_spriteZOrder = GameTypes::eSpritesPiggyBank;
}



//-----------------------------------------------------------------------------------------------------
// Circle Fragile Preloader
//-----------------------------------------------------------------------------------------------------
void CircleFragilePreloader::PreloadResources( float radius )
{
	if ( ! radius )
		return;

	stringstream filename;
	filename << "CircleFragileDestroy" << radius * RATIO;
	
	Preloader::Get()->AddAnim( filename.str().c_str() );
	Preloader::Get()->AddSound( SoundEngine::eSoundFragileDestroyed1 );
	Preloader::Get()->AddSound( SoundEngine::eSoundFragileDestroyed2 );
	Preloader::Get()->AddSound( SoundEngine::eSoundFragileDestroyed3 );
	Preloader::Get()->AddSound( SoundEngine::eSoundFragileDestroyed4 );
}
