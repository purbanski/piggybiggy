#include "CustomBlocks.h"
#include "GameTypes.h"
#include "Levels/Level.h"

//------------------------------------------------------------------------------
puPolygonFragile_Custom1::puPolygonFragile_Custom1()
{
	_blockEnum = eBlockPolygonFragileCustom1;
    b2Vec2 vec[6];
  
    vec[0] = b2Vec2( 6.10f, 3.5f );
	vec[1] = b2Vec2( -0.10f, 7.1f );
	vec[2] = b2Vec2( -6.10f, 3.5f );
	vec[3] = b2Vec2( -6.10f, -3.5f );
	vec[4] = b2Vec2( -0.10f, -7.1f ),
	vec[5] = b2Vec2( 6.10f, -3.5f );

	Construct(
        &vec[0],
        &vec[1],
        &vec[2],
        &vec[3],
        &vec[4],
        &vec[5],
        NULL );
	
	puBlock::LoadDefaultSprite();
	PreloadAnimAndSound();
}
//------------------------------------------------------------------------------
puPolygonFragile_Custom2::puPolygonFragile_Custom2()
{
	_blockEnum = eBlockPolygonFragileCustom2;
    b2Vec2 vec[3];

    vec[0] = b2Vec2( 5.6f, 6.7f );
    vec[1] = b2Vec2( -5.8f, -0.1f );
	vec[2] = b2Vec2( 5.8f, -6.7f );
    
	Construct( &vec[0],	&vec[1], &vec[2], NULL );

	puBlock::LoadDefaultSprite();
	PreloadAnimAndSound();
}
//------------------------------------------------------------------------------
puPolygonCoin_Custom1::puPolygonCoin_Custom1()
{
	_blockEnum = eBlockPolygonCoinCustom1;
    b2Vec2 vec[6];
    
	vec[0] = b2Vec2( 6.10f, 3.5f );
	vec[1] = b2Vec2( -0.10f, 7.1f );
	vec[2] = b2Vec2( -6.10f, 3.5f );
	vec[3] = b2Vec2( -6.10f, -3.5f );
	vec[4] = b2Vec2( -0.10f, -7.1f ),
	vec[5] = b2Vec2( 6.10f, -3.5f );

    Construct(
        &vec[0],
        &vec[1],
        &vec[2],
        &vec[3],
        &vec[4],
        &vec[5],
        NULL );

	_blockType = GameTypes::eCoin;
	puBlock::LoadDefaultSprite();

	Preloader::Get()->AddSound(  SoundEngine::eSoundCoinCollected );
}
//------------------------------------------------------------------------------
void puPolygonCoin_Custom1::Destroy()
{
	_level->GetParticleEngine()->Effect_CoinCollected( _level, this );
	DestroyBody();

	if ( ! _sprite ) 
		return;

	_sprite->stopAllActions();
	_sprite->runAction( CCFadeTo::actionWithDuration( 0.3f, 0 ));
	_sprite->runAction( CCScaleTo::actionWithDuration( 0.3f, 0.1f ));

	_spriteShadow->stopAllActions();
	_spriteShadow->runAction( CCFadeTo::actionWithDuration( 0.3f, 0 ));
	_spriteShadow->runAction( CCScaleTo::actionWithDuration( 0.3f, 0.1f ));
}
//------------------------------------------------------------------------------
puPolygonWall_Custom1::puPolygonWall_Custom1()
{
	_blockEnum = eBlockPolygonWall_Custom1;
    b2Vec2 vec[4];
    
    vec[0] = b2Vec2( 15.0f, 4.0f );
	vec[1] = b2Vec2( -5.0f, 4.0f );
	vec[2] = b2Vec2( -5.0f, -4.0f );
	vec[3] = b2Vec2( 25.0f, -4.0f );
    
    Construct( &vec[0], &vec[1], &vec[2], &vec[3], NULL );

	_blockType = GameTypes::eWall;

	puBlock::LoadDefaultSprite();
}
//------------------------------------------------------------------------------
puPolygonWall_Custom2::puPolygonWall_Custom2()
{
	_blockEnum = eBlockPolygonWall_Custom2;
    b2Vec2 vec[4];
    
    vec[0] = b2Vec2( 9.0f, 1.0f );
	vec[1] = b2Vec2( -9.0f, 1.0f );
	vec[2] = b2Vec2( -13.0f, -1.0f );
	vec[3] = b2Vec2( 13.0f, -1.0f );
    
	Construct( &vec[0], &vec[1], &vec[2], &vec[3], NULL );

	_blockType = GameTypes::eWall;

	puBlock::LoadDefaultSprite();
}
//------------------------------------------------------------------------------

