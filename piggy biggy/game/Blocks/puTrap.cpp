#include <Box2D/Box2D.h>

#include "Levels/World.h"
#include "Levels/Level.h"
#include "puTrap.h"

puTrap::puTrap()
{
	// Screws
	_screw1.ResetSpriteZOrder( GameTypes::eSpritesAboveX2 );
	_screw2.ResetSpriteZOrder( GameTypes::eSpritesAboveX2 );
	_screw2.SetBodyType( b2_dynamicBody );

	// _woodBox1
	_woodBox1.SetRotation( -59.0f );
	_woodBox1.SetDeltaRotation( -59.0f );
	_woodBox1.GetBody()->GetFixtureList()->SetDensity( 0.5f );
	_woodBox1.GetBody()->GetFixtureList()->SetFriction( 1.0f );
	_woodBox1.GetBody()->GetFixtureList()->SetRestitution( 0.2f );
	_woodBox1.SetBodyType( b2_dynamicBody );


	b2PolygonShape shape;
	b2FixtureDef fixtureDef;
	shape.SetAsEdge( b2Vec2( 0, 0.4f ), b2Vec2( -7.0f, 0.4f ));
	fixtureDef.shape = &shape;
	_woodBox1.GetBody()->CreateFixture( &fixtureDef );

	// SetNodeChain
	SetNodeChain( &_screw1, &_screw2, &_woodBox1, NULL );

	// SetDeltaXY
	_screw1.SetDeltaXY( 9.1f, -5.5f );
	_screw2.SetDeltaXY( 3.5f, 3.4f );
	_woodBox1.SetDeltaXY( 6.2f, -0.8f );

	// Main block (WoodBox)
	SetRotation( 50.0f );
	SetDeltaRotation( 50.0f );
	GetBody()->GetFixtureList()->SetDensity( 5.5f );
	GetBody()->GetFixtureList()->SetFriction( 0.5f );
	GetBody()->GetFixtureList()->SetRestitution( 0.2f );
	SetBodyType( b2_dynamicBody );

	SetPosition( 0.0f, 0.0f );
	CreateJoints();
}
//-----------------------------------------------------------------------------------------------------
void puTrap::CreateJoints()
{
	b2RevoluteJointDef jointDef1;
	jointDef1.Initialize( GetBody(), _woodBox1.GetBody(), b2Vec2( GetPosition().x + 3.6f, GetPosition().y + 3.6f ) );
	_joint1 = (b2RevoluteJoint *) _world->CreateJoint( &jointDef1 );

	_joint1->SetLimits( -1.2f, 0.0f );
	_joint1->EnableLimit( true );
	_joint1->EnableMotor( true );
	_joint1->SetMotorSpeed( -0.8f );
	_joint1->SetMaxMotorTorque( 8.0f * 150.0f );

	b2RevoluteJointDef jointDef2;
	jointDef2.Initialize( _screw1.GetBody(), _woodBox1.GetBody(), _screw1.GetPosition());
	_joint2 = (b2RevoluteJoint *) _world->CreateJoint( &jointDef2 );

	b2RevoluteJointDef jointDef3;
	jointDef3.Initialize( _woodBox1.GetBody(), _screw2.GetBody(), _screw2.GetPosition() );
	_joint3 = (b2RevoluteJoint *) _world->CreateJoint( &jointDef3 );

	_joint3->SetLimits( 0.0f, 0.0f );
	_joint3->EnableLimit( true );
	_joint3->EnableMotor( false );

}
//-----------------------------------------------------------------------------------------------------
void puTrap::DestroyJoints()
{
	_world->DestroyJoint( _joint1 );
	_world->DestroyJoint( _joint2 );
	_world->DestroyJoint( _joint3 );
	
	_joint1 = NULL;
	_joint2 = NULL;
	_joint3 = NULL;
}
//-----------------------------------------------------------------------------------------------------
