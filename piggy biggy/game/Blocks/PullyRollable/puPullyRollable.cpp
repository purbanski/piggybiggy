//#include "puPully.h"
//#include "Game.h"
//
////--------------------------------------------------------------------------------------------------------------
//puPully::puPully() : _block1( 5.0f ), _block2( 10.0f )
//{
//	Construct( b2Vec2( 10.0f, 10.0f ), b2Vec2( 20.0f, 10.0f ), 5.0f );
//}
////--------------------------------------------------------------------------------------------------------------
//puPully::puPully( b2Vec2 b1pos, float b1dens, b2Vec2 b2pos, float b2dens, float jointTopY )
//	: _block1( b1dens ), _block2( b2dens )
//{
//	Construct( b1pos, b2pos, jointTopY );
//}
////--------------------------------------------------------------------------------------------------------------
//puPully::puPully( 
//		b2Vec2 b1pos, float b1dens, b2Vec2 b1size,
//		b2Vec2 b2pos, float b2dens, b2Vec2 b2size, 
//		float jointTopY )
//	: _block1( b1dens, b1size ), _block2( b2dens, b2size )
//{
//	Construct( b1pos, b2pos, jointTopY );
//}
////--------------------------------------------------------------------------------------------------------------
//void puPully::Construct( b2Vec2 b1pos, b2Vec2 b2pos, float jointTopY )
//{
//	_block1.SetPosition( b1pos );
//	_block2.SetPosition( b2pos );
//
//	_block1.SetBlockType( GameTypes::eDynamic );
//	_block2.SetBlockType( GameTypes::eDynamic );
//
//	b2PulleyJointDef pulleyDef;
//	b2Vec2 anchor1( b1pos );
//	b2Vec2 anchor2( b2pos );
//	b2Vec2 groundAnchor1( b1pos.x, b1pos.y + jointTopY );
//	b2Vec2 groundAnchor2( b2pos.x, b1pos.y + jointTopY ); // Watch out
//	pulleyDef.Initialize( _block1.GetBody(), _block2.GetBody(), groundAnchor1, groundAnchor2, anchor1, anchor2, 1.0f );
//
//	b2World *world;
//	world = Game::Get()->GetWorld();
//
//	_joint = ( b2PulleyJoint* ) world->CreateJoint(&pulleyDef);
//}
////--------------------------------------------------------------------------------------------------------------
//
//puPully::~puPully()
//{
//	b2World *world;
//	world = Game::Get()->GetWorld();
//
//	world->DestroyJoint( _joint );
//}
