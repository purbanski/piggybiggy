#ifndef __PUSCALE_H__
#define __PUSCALE_H__

#include "puBlock.h"
#include "puBox.h"
#include "puCircle.h"

//---------------------------------------------------------------------------------------------
// class puScale_
//---------------------------------------------------------------------------------------------
class puScale_ : public puWoodBox_
{
public:
	puScale_( float width, float height );

	virtual void CreateJoints();
	virtual void DestroyJoints();

protected:
	puScrew<>		_screw1;
	b2RevoluteJoint	*_joint1;
};

//---------------------------------------------------------------------------------------------
// tempalte puScale
//---------------------------------------------------------------------------------------------
template<int TWidth, int THeight, BlockScaleType TScale = eBlockNormal>
class puScale : public puWoodBox<TWidth,THeight,TScale>
{
public:
	puScale();
	
	virtual void CreateJoints();
	virtual void DestroyJoints();

protected:
	puScrew<>		_screw1;
	b2RevoluteJoint	*_joint1;
};
//---------------------------------------------------------------------------------------------
template<int TWidth, int THeight, BlockScaleType TScale>
puScale<TWidth,THeight,TScale>::puScale()
{
	// SetNodeChain
	this->SetNodeChain( &_screw1, NULL );

	// SetDeltaXY
	_screw1.SetDeltaXY( 0.0f, 0.0f );

	// Main block (Wood)
	this->SetBodyType( b2_dynamicBody );
	this->SetPosition( 0.0f, 0.0f );

	CreateJoints();
}
//---------------------------------------------------------------------------------------------
template<int TWidth, int THeight, BlockScaleType TScale>
void puScale<TWidth, THeight, TScale>::CreateJoints()
{
	float anchorX;
	float anchorY;

	anchorX = _screw1.GetPosition().x;
	anchorY = _screw1.GetPosition().y;

	b2RevoluteJointDef jointDef1;
	jointDef1.Initialize( this->GetBody(), _screw1.GetBody(), b2Vec2( anchorX, anchorY ));
	_joint1 = (b2RevoluteJoint *) this->_world->CreateJoint( &jointDef1 );

	_joint1->SetLimits( 0.0f, 0.0f );
	_joint1->EnableLimit( false );
	_joint1->EnableMotor( false );
	_joint1->SetMotorSpeed( 0.0f );
	_joint1->SetMaxMotorTorque( 0.0f );
}
//---------------------------------------------------------------------------------------------
template<int TWidth, int THeight, BlockScaleType TScale>
void puScale<TWidth, THeight, TScale>::DestroyJoints()
{
	this->_world->DestroyJoint( _joint1 );
	_joint1 = NULL;
}
#endif
