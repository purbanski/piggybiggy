#include <sstream>
#include "unFile.h"
#include "Debug/MyDebug.h"
#include "puPolygon.h"
#include "puBlocksSettings.h"
#include "Skins.h"
#include "Preloader.h"

using namespace pu::blocks::settings;

puPolygon::puPolygon()
{
//	LoadDefaultSprite();
}
//---------------------------------------------------------------------------------------
void puPolygon::Construct( const b2Vec2* vertices, int count )
{
	// mostly for editor except cetner polygon
	_verticesCount = count;
	CopyVertices( vertices );
	
	ConstructCommon();
}
//---------------------------------------------------------------------------------------
void puPolygon::Construct( const b2Vec2* vertices, ... )
{
	int index = 0;
	_vertices[ index++ ] = *vertices;

	va_list args;
	va_start( args, vertices );

	b2Vec2 *vertex = va_arg( args, b2Vec2* );
	while ( vertex )
	{
		_vertices[ index ] = *vertex;
		vertex = va_arg( args, b2Vec2* );
		index++;
	}

	va_end(args);

	_verticesCount = index;
	ConstructCommon();
}
//---------------------------------------------------------------------------------------
void puPolygon::ConstructCommon()
{
	CenterPolygon();

	b2PolygonShape polygon;
	polygon.Set( _vertices, _verticesCount );

	b2FixtureDef fixtureDef;
	fixtureDef.shape = &polygon;
	fixtureDef.density = 3.5f;
	fixtureDef.friction = 0.6f;
	fixtureDef.restitution = 0.2f;

	_body->CreateFixture( &fixtureDef );

	doConstruct();
}
//---------------------------------------------------------------------------------------
void puPolygon::LoadDefaultSprite()
{
	string filename;
	stringstream ss;

	ss << GetClassName2();
	for ( int i = 0 ; i < _verticesCount - 1; i++ )
		ss <<  ( _vertices[i].x * 100.0f ) << "x" << ( _vertices[i].y * 100.0f )  << "_";
	ss << ( _vertices[ _verticesCount - 1 ].x * 100.0f ) << "x" << ( _vertices[ _verticesCount - 1 ].y * 100.0f )  << ".png";

	filename.append( "Images/Blocks/" );
	filename.append( ss.str() );

	filename = Skins::GetSkinName( filename.c_str() );

	D_LOG( "Loading sprite %s", filename.c_str() );

	if ( unResourceFileCheckExists( filename.c_str() ) )
		_sprite = CCSprite::spriteWithFile( filename.c_str() );
	
	UpdateSprite();
}
//---------------------------------------------------------------------------------------
void puPolygon::CopyVertices( const b2Vec2 *vertices )
{
	for ( int i=0 ; i < Config::PolygonMaxVertex; i++ )
		_vertices[i] = vertices[i];
}
//---------------------------------------------------------------------------------------
void puPolygon::CenterPolygon()
{
	//-------------------------------------
	// Find max and min vertex
	//-------------------------------------
	float minX = _vertices[0].x;
	float maxX = _vertices[0].x;
	
	float minY = _vertices[0].y;
	float maxY = _vertices[0].y;

	for ( int i = 1 ; i < _verticesCount; i++ )
	{
		minX = min( minX, _vertices[i].x );
		maxX = max( maxX, _vertices[i].x );

		minY = min( minY, _vertices[i].y );
		maxY = max( maxY, _vertices[i].y );
	}

#ifdef _DEBUG
	//for ( int i = 0 ; i < _verticesCount; i++ )
	//D_LOG( "Vertex[%d] (pre) x: %f y: %f", i, _vertices[i].x, _vertices[i].y );
	//D_LOG( "Max x: %f y: %f, Min x: %f, y: %f", maxX, maxY, minX, minY );
#endif


	//-------------------------------------
	// Adjust vertices
	//-------------------------------------
	float deltaX;
	float deltaY;

	deltaX = (( maxX - minX ) / 2.0f ) + minX;
	deltaY = (( maxY - minY ) / 2.0f ) + minY;
	
	b2Vec2 deltaPos( -deltaX, -deltaY );

	for ( int i = 0 ; i < _verticesCount; i++ )
	{
		_vertices[i] += deltaPos;
		//D_LOG( "Vertex[%d] (pos) x: %f y: %f", i, _vertices[i].x, _vertices[i].y );
	}
}


//---------------------------------------------------------------------------------------
// Fragile
//---------------------------------------------------------------------------------------
void puPolygonFragile::doConstruct()
{
	b2Fixture *fixture;
	fixture = _body->GetFixtureList();

	fixture->SetDensity( FragileDensity );
	fixture->SetRestitution( FragileRestitution );
	fixture->SetFriction( FragileFriction );

	SetBlockType( GameTypes::eDestroyable );
	SetBodyType( b2_dynamicBody );
	LoadDefaultSprite();
}
//---------------------------------------------------------------------------------------
void puPolygonFragile::PreloadAnimAndSound()
{
	stringstream filename;
	filename << GetClassName2() << "Destroy" ;

	Preloader::Get()->AddAnim( filename.str().c_str() );
	Preloader::Get()->AddSound( SoundEngine::eSoundFragileDestroyed1 );
	Preloader::Get()->AddSound( SoundEngine::eSoundFragileDestroyed2 );
	Preloader::Get()->AddSound( SoundEngine::eSoundFragileDestroyed3 );
	Preloader::Get()->AddSound( SoundEngine::eSoundFragileDestroyed4 );
}



//---------------------------------------------------------------------------------------
// Fragile Static
//---------------------------------------------------------------------------------------
void puPolygonFragileStatic::doConstruct()
{
	b2Fixture *fixture;
	fixture = _body->GetFixtureList();

	fixture->SetDensity( FragileDensity );
	fixture->SetRestitution( FragileRestitution );
	fixture->SetFriction( FragileFriction );

	SetBlockType( GameTypes::eDestroyable );
	SetBodyType( b2_staticBody );
	LoadDefaultSprite();
}



//---------------------------------------------------------------------------------------
// Wall
//---------------------------------------------------------------------------------------
void puPolygonWall::doConstruct()
{
	b2Fixture *fixture;
	fixture = _body->GetFixtureList();

	fixture->SetDensity( WallDensity );
	fixture->SetRestitution( WallRestitution );
	fixture->SetFriction( WallFriction );

	SetBlockType( GameTypes::eOther );
	SetBodyType( b2_staticBody );
//	LoadDefaultSprite();
}


//-----------------------------------------------------------------------------------------
// Polygon Iron
//-----------------------------------------------------------------------------------------
void puPolygonIron::doConstruct()
{
	b2Fixture *fixture;
	fixture = _body->GetFixtureList();

	fixture->SetDensity( IronDensity );
	fixture->SetRestitution( IronRestitution );
	fixture->SetFriction( IronFriction );

	SetBlockType( GameTypes::eOther );
	SetBodyType( b2_staticBody );
	LoadDefaultSprite();
}


//-----------------------------------------------------------------------------------------
// Polygon Stone
//-----------------------------------------------------------------------------------------
void puPolygonStone::doConstruct()
{
	b2Fixture *fixture;
	fixture = _body->GetFixtureList();

	fixture->SetDensity( StoneDensity );
	fixture->SetRestitution( StoneRestitution );
	fixture->SetFriction( StoneFriction );

	SetBlockType( GameTypes::eOther );
	SetBodyType( b2_staticBody );
	LoadDefaultSprite();
}


//-----------------------------------------------------------------------------------------
// Polygon Wood
//-----------------------------------------------------------------------------------------
void puPolygonWood::doConstruct()
{
	b2Fixture *fixture;
	fixture = _body->GetFixtureList();

	fixture->SetDensity( WoodDensity );
	fixture->SetRestitution( WoodRestitution );
	fixture->SetFriction( WoodFriction );

	SetBlockType( GameTypes::eOther );
	SetBodyType( b2_dynamicBody );
	LoadDefaultSprite();
}


//-----------------------------------------------------------------------------------------
// Polygon Bouncer
//-----------------------------------------------------------------------------------------
void puPolygonBouncer::doConstruct()
{
	b2Fixture *fixture;
	fixture = _body->GetFixtureList();

	fixture->SetDensity( BouncerDensity );
	fixture->SetRestitution( BouncerRestitution );
	fixture->SetFriction( BouncerFriction );

	SetBlockType( GameTypes::eOther );
	SetBodyType( b2_staticBody );
	LoadDefaultSprite();
}


//-----------------------------------------------------------------------------------------
// Polygon Ice
//-----------------------------------------------------------------------------------------
void puPolygonIce::doConstruct()
{
	b2Fixture *fixture;
	fixture = _body->GetFixtureList();

	fixture->SetDensity( IceDensity );
	fixture->SetRestitution( IceRestitution );
	fixture->SetFriction( IceFriction );

	SetBlockType( GameTypes::eOther );
	SetBodyType( b2_staticBody );
	LoadDefaultSprite();
}
