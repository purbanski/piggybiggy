#ifndef __PUBORDERS_H__
#define __PUBORDERS_H__

#include "AllBlocks.h"
#include "GameGlobals.h"
//--------------------------------------------------------------------------------------
class puBorder : public puBlock
{
public:
	typedef enum
	{
		eBorder_Normal = 1,
		eBorder_Big
	} BorderType;

	puBorder();

	void SetBorderSize( float size );
	void SetBorderType( BorderType border );
	void SetFilter( b2Filter filter );

private:
	void DestroyCurrentBorder();
};
//--------------------------------------------------------------------------------------
class puBorderLab : public puBlock
{
public:
	puBorderLab();
	void SetBorderSize( float size );

private:
	void DestroyCurrentBorder();
};

//--------------------------------------------------------------------------------------
class puBorderAllClose : public puWallBox<940,20,eBlockScaled>
{
public:
	puBorderAllClose();

private:
	puWallBox< 20, 620, eBlockScaled >	_otherBox1;
	puWallBox< 20, 620, eBlockScaled >	_otherBox3;
	puWallBox< 940, 20, eBlockScaled >	_otherBox2;
};

//--------------------------------------------------------------------------------------
class puBorderU : public puWallBox< 960, 20, eBlockScaled>
{
public:
	puBorderU();

private:
	puWallBox< 20, 620, eBlockScaled >	_otherBox1;
	puWallBox< 20, 620, eBlockScaled >	_otherBox2;
};


#endif
