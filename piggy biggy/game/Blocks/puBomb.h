#ifndef __PUBOMB_H__
#define __PUBOMB_H__

#include "puCircle.h"
#include "puBlockExtras.h"
#include "puMagnet.h"
#include "Levels/Level.h"
#include "SoundEngine.h"
#include "Animation/AnimerSimpleEffect.h"
#include "Preloader.h"
#include "Tools.h"

//---------------------------------------------------------------------------------------------------
void IncBombExploded();

//---------------------------------------------------------------------------------------------------
// Bomb
//---------------------------------------------------------------------------------------------------
template < class Magnet >
class puMagnetBombBase : public Magnet
{
public:
	//---------------------------------------------------------------------------------------------------
	puMagnetBombBase() : Magnet( 4.4f ) 
	{
		Construct();
	}
	//---------------------------------------------------------------------------------------------------
	puMagnetBombBase( float radius ) : Magnet( radius )
	{ 
		Construct(); 
	}
	//---------------------------------------------------------------------------------------------------
	virtual ~puMagnetBombBase()
	{
	}
	//---------------------------------------------------------------------------------------------------
	virtual void LoadDefaultSprite()
	{
		Magnet::LoadDefaultSprite();
		Tools::RunBombPulse( this->_sprite );
	}
	//---------------------------------------------------------------------------------------------------
	virtual void LoadDefaultSpriteStatic()
	{
		Magnet::LoadDefaultSpriteStatic();
		Tools::RunBombPulse( this->_sprite );
	}
	//---------------------------------------------------------------------------------------------------
	virtual void Destroy()
	{
		if ( ! Magnet::_body )
			return;

		AnimerSimpleEffect::Get()->Anim_BombExploded( this );
        IncBombExploded();
        
		Magnet::DestroyBody();
		Magnet::_blockRange.Destroy();
		_bombTouch.Destroy();
		
		if ( ! Magnet::_sprite ) 
			return;

		AnimerSimpleEffect::Get()->Anim_BombDestroy( Magnet::_sprite );

		if ( Magnet::_spriteShadow )
			AnimerSimpleEffect::Get()->Anim_BombDestroy( Magnet::_spriteShadow );

		if ( ! Magnet::_level->IsDemoRunning() )
			SoundEngine::Get()->PlayRandomEffect( 
			15.0f,
			SoundEngine::eSoundBombExploded1, 
			SoundEngine::eSoundBombExploded2, 
			SoundEngine::eSoundBombExploded3, 
			SoundEngine::eSoundNone );
	}
	//---------------------------------------------------------------------------------------------------
	virtual void DestroyStatic()
	{
		puMagnetBombBase::Destroy();
		AnimerSimpleEffect::Get()->Anim_GlueDestroy( Magnet::_glueSprite );
	}
	//---------------------------------------------------------------------------------------------------
	virtual void Activate( void *ptr )
	{
		if ( _activated )
			return;
		
		_activated = true;

		for ( puBlockWithRange_::BlocksSet::iterator it = Magnet::_blocksInRange.begin(); it != Magnet::_blocksInRange.end(); it++ )
		{
			//D_PTR( *it );
			ApplyExplosion( *it );
		}

		Destroy();
		Magnet::_blocksInRange.clear();
	} 
	//---------------------------------------------------------------------------------------------------
	void SetBombImpulse( float impusle ) { _bombImpulse = impusle; }
	float GetBombImpulse() { return _bombImpulse; }

	
protected:
	//---------------------------------------------------------------------------------------------------
	void Construct()
	{
		_activated = false;

		Preloader::Get()->AddAnim( "BombExplosion" );

		//Magnet::LoadDefaultSprite();
		Magnet::SetBodyType( b2_dynamicBody );
		Magnet::_blockType = GameTypes::eActivable;

		Magnet::SetRange( 16.0f );
		_bombImpulse = 6000;

		_bombTouch.SetDeltaXY( 0.0f, 0.0f );
		Magnet::_blockRange.SetDeltaXY( 0.0f, 0.0f );

		Magnet::SetNext( & this->_blockRange );
		Magnet::_blockRange.SetNext( &_bombTouch );

		//-------------
		// joints
		//-------------
		b2RevoluteJointDef revJointDef;

		revJointDef.collideConnected = false;
		revJointDef.Initialize( Magnet::_body, _bombTouch.GetBody(), Magnet::GetPosition() );

		Magnet::_level->GetWorld()->CreateJoint( &revJointDef );

		Preloader::Get()->AddSound( SoundEngine::eSoundBombExploded1 );
		Preloader::Get()->AddSound( SoundEngine::eSoundBombExploded2 );
		Preloader::Get()->AddSound( SoundEngine::eSoundBombExploded3 );
	}
	//---------------------------------------------------------------------------------------------------
	void ApplyExplosion( puBlock *block )
	{
		if ( ! block )
			return;

		if ( ! block->GetBody() || block->GetBody()->GetType() == b2_staticBody )
			return;

		//D_HIT
		//D_LOG("Explode %s " , block->GetClassName().c_str() )
		//D_FLOAT( _bombRange.GetRange() )
		//D_POINT( GetPosition() )
		//D_POINT( block->GetPosition() )

		b2Vec2 distance;
		b2Vec2 impulseStrenght;

		distance.x = ( block->GetPosition().x - Magnet::GetPosition().x );
		distance.y = ( block->GetPosition().y - Magnet::GetPosition().y );

		//D_POINT( distance )
		//D_FLOAT( distance.y / distance.x ) 

		float nRotation;
		// check for limits and distance.x = 0

		if ( distance.x == 0.0f )
			nRotation = 90.0f;
		else
			nRotation = atan( abs( distance.y )/  abs( distance.x )) * 180.0f / b2_pi;

		//	D_FLOAT( nRotation )	

		if ( distance.x >= 0 && distance.y >=0 )
		{
			nRotation += 0.0f;
		}
		else if ( distance.x < 0 && distance.y >= 0 ) 
		{
			nRotation = 180.0f - nRotation;
		}
		else if ( distance.x < 0 && distance.y < 0 )
		{
			nRotation = 180.0f + nRotation;
		}
		else if ( distance.x >= 0 && distance.y < 0 )
		{
			nRotation = 360.0f - nRotation;
		}

		//D_FLOAT( nRotation )
		if (( abs( distance.x ) > Magnet::_blockRange.GetRange() ) || ( abs( distance.y ) > Magnet::_blockRange.GetRange() ))
		{
			impulseStrenght.SetZero();
		}
		else
		{
			impulseStrenght.x = ( Magnet::_blockRange.GetRange() - abs( distance.x )) / Magnet::_blockRange.GetRange();
			impulseStrenght.y = ( Magnet::_blockRange.GetRange() - abs( distance.y )) / Magnet::_blockRange.GetRange();
		}

		//D_FLOAT( _maxImpulse )
		//D_POINT( impulseStrenght )

		impulseStrenght.x = impulseStrenght.x * cos( nRotation * b2_pi / 180.0f );
		impulseStrenght.y = impulseStrenght.y * sin( nRotation * b2_pi / 180.0f );

		//D_POINT( impulseStrenght )

		block->GetBody()->ApplyLinearImpulse( _bombImpulse * impulseStrenght, block->GetPosition() );
	}
	//---------------------------------------------------------------------------------------------------
protected:
	puBlockTouch	_bombTouch;
	float			_bombImpulse;
	bool			_activated;
};


//---------------------------------------------------------------------------------------------------
// Bomb
//---------------------------------------------------------------------------------------------------
class puBomb_ : public puMagnetBombBase< puMagnet_ >
{
public:
	puBomb_() : puMagnetBombBase< puMagnet_ >( 4.4f ) { Construct(); }
	puBomb_( float radius ) : puMagnetBombBase< puMagnet_ >( radius ) { Construct(); }

	virtual ~puBomb_(){};
	virtual void Step() {} // to get rid of magnetization

protected:
	virtual void Construct();
};
//---------------------------------------------------------------------------------------------------
template <int TRadius = 44, BlockScaleType T = eBlockNormal>
class puBomb : public puBomb_
{
public:
	puBomb() : puBomb_( (float) TRadius / 10.0f / float( T ) ) {}
};
//---------------------------------------------------------------------------------------------------



//---------------------------------------------------------------------------------------------------
// Bomb
//---------------------------------------------------------------------------------------------------
class puBombStatic_ : public puMagnetBombBase< puMagnet_ >
{
public:
	puBombStatic_() : puMagnetBombBase< puMagnet_ >( 4.4f ) { Construct(); }
	puBombStatic_( float radius ) : puMagnetBombBase< puMagnet_ >( radius ) { Construct(); }
	virtual void Destroy() { DestroyStatic(); }

	virtual ~puBombStatic_(){};
	virtual void Step() {} // to get rid of magnetization

protected:
	virtual void Construct();
};
//---------------------------------------------------------------------------------------------------
template <int TRadius = 44, BlockScaleType T = eBlockNormal>
class puBombStatic : public puBombStatic_
{
public:
	puBombStatic() : puBombStatic_( (float) TRadius / 10.0f / float( T ) ) {}
};
//---------------------------------------------------------------------------------------------------



//---------------------------------------------------------------------------------------------------
// Magnet Bomb
//---------------------------------------------------------------------------------------------------
class puMagnetBomb_ : public puMagnetBombBase< puMagnet_ >
{
public:
	puMagnetBomb_() : puMagnetBombBase< puMagnet_ >( 4.4f ) { Construct(); }
	puMagnetBomb_( float radius ) : puMagnetBombBase< puMagnet_ >( radius ) { Construct(); }

	virtual ~puMagnetBomb_(){};

protected:
	virtual void Construct();
};
//---------------------------------------------------------------------------------------------------
template <int TRadius = 44, BlockScaleType T = eBlockNormal>
class puMagnetBomb : public puMagnetBomb_
{
public:
	puMagnetBomb() : puMagnetBomb_( (float) TRadius / 10.0f / float( T ) ) {}
};
//---------------------------------------------------------------------------------------------------



//---------------------------------------------------------------------------------------------------
// Magnet Bomb Plus
//---------------------------------------------------------------------------------------------------
class puMagnetBombPlus_ : public puMagnetBombBase< puMagnetPlus_ >
{
public:
	puMagnetBombPlus_() : puMagnetBombBase< puMagnetPlus_ >( 4.4f ) { Construct(); }
	puMagnetBombPlus_( float radius ) : puMagnetBombBase< puMagnetPlus_ >( radius ) { Construct(); }

	virtual ~puMagnetBombPlus_(){};

protected:
	virtual void Construct();
};
//---------------------------------------------------------------------------------------------------
template <int TRadius = 44, BlockScaleType T = eBlockNormal>
class puMagnetBombPlus : public puMagnetBombPlus_
{
public:
	puMagnetBombPlus() : puMagnetBombPlus_( (float) TRadius / 10.0f / float( T ) ) {}
};
//---------------------------------------------------------------------------------------------------



//---------------------------------------------------------------------------------------------------
// Magnet Bomb Minus
//---------------------------------------------------------------------------------------------------
class puMagnetBombMinus_ : public puMagnetBombBase< puMagnetMinus_ >
{
public:
	puMagnetBombMinus_() : puMagnetBombBase< puMagnetMinus_ >( 4.4f ) { Construct(); }
	puMagnetBombMinus_( float radius ) : puMagnetBombBase< puMagnetMinus_ >( radius ) { Construct(); }

	virtual ~puMagnetBombMinus_(){};

protected:
	virtual void Construct();
};
//---------------------------------------------------------------------------------------------------
template <int TRadius = 44, BlockScaleType T = eBlockNormal>
class puMagnetBombMinus : public puMagnetBombMinus_
{
public:
	puMagnetBombMinus() : puMagnetBombMinus_( (float) TRadius / 10.0f / float( T ) ) {}
};
//---------------------------------------------------------------------------------------------------


#endif
