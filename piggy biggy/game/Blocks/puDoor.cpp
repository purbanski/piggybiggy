#include <Box2D/Box2D.h>

#include "SoundEngine.h"
#include "Levels/World.h"
#include "Levels/Level.h"
#include "puDoor.h"

//-----------------------------------------------------------------------------------------------------
// puDoor_1
//-----------------------------------------------------------------------------------------------------
puDoor_1::puDoor_1()
{
	b2Filter filter;
	filter.groupIndex = -1;

	b2PolygonShape box;
	box.SetAsBox( 2.0f, 6.0f );

	b2FixtureDef fixture;
	fixture.shape = &box;
	fixture.density = 0.9f;
	fixture.restitution = 0.1f;
	fixture.friction = 0.7f;

	_body->CreateFixture( &fixture );

	// _wallBox1
	_wallBox1.GetBody()->GetFixtureList()->SetDensity( 8.0f );
	_wallBox1.GetBody()->GetFixtureList()->SetFriction( 0.8f );
	_wallBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1f );
	_wallBox1.GetBody()->GetFixtureList()->SetFilterData( filter );
	_wallBox1.SetBodyType( b2_staticBody );
	_wallBox1.SetEmptySprite();

	// SetDeltaXY
	_wallBox1.SetDeltaXY( 0.0f, -14.8f );

	// door and door's sliding should not colide
	
	filter.groupIndex = 0;
	filter.categoryBits = Config::eFilterCategoryDoor;
	filter.maskBits = ~Config::eFilterCategoryDoor & 0xffff;

	_body->GetFixtureList()->SetFilterData( filter );

	filter.groupIndex = -1;
	filter.categoryBits = 0;
	filter.maskBits = 0xffff;
	
	// SetNodeChain
	SetNext( &_wallBox1 );

	// Main block (IronBox)
	GetBody()->GetFixtureList()->SetDensity( 10.0f );
	GetBody()->GetFixtureList()->SetFriction( 0.6f );
	GetBody()->GetFixtureList()->SetRestitution( 0.2f );
	SetBodyType( b2_dynamicBody );
	
	_liftDoorSprite = LiftDoorSprite::Create();
	_liftDoorSprite->SetRed();
	_sprite = _liftDoorSprite;
//	_sprite = CCSprite::spriteWithFile( Skins::GetSkinName( "Images/Blocks/LiftDoorRed.png" ) );
	SetSpriteZOrder( GameTypes::eSpritesAboveX3 );

	SetPosition( 0.0f, 0.0f );
	CreateJoints();

	Preloader::Get()->AddSound( SoundEngine::eSoundSlidingDoorOpen );
	Preloader::Get()->AddSound( SoundEngine::eSoundSlidingDoorClose );
}
//-----------------------------------------------------------------------------------------------------
void puDoor_1::CreateJoints()
{
	b2PrismaticJointDef jointDef1;
	jointDef1.collideConnected= false;
	jointDef1.Initialize( GetBody(), _wallBox1.GetBody(), _wallBox1.GetPosition(), b2Vec2( 0.0f, 1.0f ) );
	_joint1 = (b2PrismaticJoint *) _world->CreateJoint( &jointDef1 );

	_joint1->EnableMotor( true );
	_joint1->SetMotorSpeed( -6.0f );
	_joint1->SetMaxMotorForce( 15000.0f );
	_joint1->SetLimits( 0.0f, 14.0f );
	_joint1->EnableLimit( true );
}
//-----------------------------------------------------------------------------------------------------
void puDoor_1::DestroyJoints()
{
	if ( _joint1 )
		_world->DestroyJoint( _joint1 );
	_joint1 = NULL;
}
//-----------------------------------------------------------------------------------------------------
puDoor_1::~puDoor_1()
{
}
//-----------------------------------------------------------------------------------------------------
void puDoor_1::Activate( void *ptr /*= NULL */ )
{
	_liftDoorSprite->SetGreen();
	_joint1->SetMotorSpeed( +6.0f );
	
	if ( ! _level->IsDemoRunning() )
		SoundEngine::Get()->PlayEffect( SoundEngine::eSoundSlidingDoorOpen );
}
//-----------------------------------------------------------------------------------------------------
void puDoor_1::Deactivate( void *ptr /*= NULL */ )
{
	_liftDoorSprite->SetRed();
	_joint1->SetMotorSpeed( -6.0f );
	
	if ( ! _level->IsDemoRunning() )
		SoundEngine::Get()->PlayEffect( SoundEngine::eSoundSlidingDoorClose );
}
//-----------------------------------------------------------------------------------------------------


//
//
////-----------------------------------------------------------------------------------------------------
//// puDoorSwitch
////-----------------------------------------------------------------------------------------------------
//puDoorSwitch::puDoorSwitch()
//{
//	GetBody()->GetFixtureList()->SetSensor( true );
//	SetBodyType( b2_staticBody );
//	_target = NULL;
//}
////-----------------------------------------------------------------------------------------------------
//puDoorSwitch::~puDoorSwitch()
//{
//
//}
////-----------------------------------------------------------------------------------------------------
//void puDoorSwitch::Activate( void *ptr /*= NULL */ )
//{
//	if ( ! _target ) return;
//	_target->Activate();
//}
////-----------------------------------------------------------------------------------------------------
//void puDoorSwitch::Deactivate( void *ptr /*= NULL */ )
//{
//	if ( ! _target ) return;
//	_target->Deactivate();
//}
