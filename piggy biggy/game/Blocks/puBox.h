#ifndef __PUBOX_H__
#define __PUBOX_H__

#include "puBaseBox.h"
#include "puBlocksSettings.h"
#include "Animation/AnimerSimpleEffect.h"
#include "Animation/AnimManager.h"
#include "GameTypes.h"
#include "Preloader.h"
#include <set>
#include <string>

using namespace std;
using namespace pu::blocks::settings;
using namespace GameTypes;
//fix me memory leak
set<BlockEnum> GBoxNames();

typedef enum {
	eBoxOther = 1,
	eBoxWall,
	eBoxStone,
	eBoxIron,
	eBoxGlass,
	eBoxWood,
	eBoxDomino,
	eBoxFragile,
	eBoxFragileStatic,
	eBoxBouncer,
	eBoxL,
	eBoxLScrewed,
	eBoxScale,
	eBoxLiftPlatform
} PUBoxTypes;


//----------------------------------------------------------------------------------------
// Iron box
//----------------------------------------------------------------------------------------
class IronBoxPolicy
{
public:
	static BoxPolicy GetPolicy()
	{
		BoxPolicy policy;
		policy._fixtureDef.density = IronDensity;
		policy._fixtureDef.restitution = IronRestitution;
		policy._fixtureDef.friction = IronFriction;

		policy._bodyType	= b2_dynamicBody;
		policy._blockType	= GameTypes::eIron;
		policy._spriteLayer = GameTypes::eSprites;
		policy._blockEnum	= GameTypes::eBlockBoxIron;

		return policy;
	}
};

template<int W, int H, BlockScaleType T = eBlockScaled>
class puIronBox : public puBaseBoxTemplated<IronBoxPolicy, W, H, T>
{
public:
	puIronBox()
	{
	}
};

typedef puBaseBox_<IronBoxPolicy> puIronBox_;


//----------------------------------------------------------------------------------------
// Wall box
//----------------------------------------------------------------------------------------
class WallPolicy
{
public:
	static BoxPolicy GetPolicy()
	{
		BoxPolicy policy;
		policy._fixtureDef.density = WallDensity;
		policy._fixtureDef.restitution = WallRestitution;
		policy._fixtureDef.friction = WallFriction;

		policy._bodyType	= b2_staticBody;
		policy._blockType	= GameTypes::eWall;
		policy._spriteLayer = GameTypes::eSpritesWall;
		policy._blockEnum	= GameTypes::eBlockBoxWall;
		return policy;
	}
};

template<int W, int H, BlockScaleType T = eBlockScaled>
class puWallBox : public puBaseBoxTemplated<WallPolicy, W, H, T>
{
public:
	puWallBox()
	{
	}
};

typedef puBaseBox_<WallPolicy> puWallBox_;





//----------------------------------------------------------------------------------------
// Stone box
//----------------------------------------------------------------------------------------
class StoneBoxPolicy
{
public:
	static BoxPolicy GetPolicy()
	{
		BoxPolicy policy;
		policy._fixtureDef.density = StoneDensity;
		policy._fixtureDef.restitution = StoneRestitution;
		policy._fixtureDef.friction = StoneFriction;

		policy._bodyType	= b2_dynamicBody;
		policy._blockType	= GameTypes::eOther;
		policy._spriteLayer = GameTypes::eSprites;
		return policy;
	}
};

template<int W, int H, BlockScaleType T = eBlockScaled>
class puStoneBox : public puBaseBoxTemplated<StoneBoxPolicy, W, H, T>{};

typedef puBaseBox_<StoneBoxPolicy> puStoneBox_;


//----------------------------------------------------------------------------------------
// Other box
//----------------------------------------------------------------------------------------
class OtherBoxPolicy
{
public:
	static BoxPolicy GetPolicy()
	{
		BoxPolicy policy;
		policy._fixtureDef.density = OtherDensity;
		policy._fixtureDef.restitution = OtherRestitution;
		policy._fixtureDef.friction = OtherFriction;

		policy._bodyType	= b2_staticBody;
		policy._blockType	= GameTypes::eOther;
		policy._spriteLayer = GameTypes::eSprites;

		return policy;
	}
};

template<int W, int H, BlockScaleType T = eBlockScaled>
class puOtherBox : public puBaseBoxTemplated<OtherBoxPolicy, W, H, T>{};

typedef puBaseBox_<OtherBoxPolicy> puOtherBox_;


//----------------------------------------------------------------------------------------
// Wooden Box
//----------------------------------------------------------------------------------------
class WoodBoxPolicy
{
public:
	static BoxPolicy GetPolicy()
	{
		BoxPolicy policy;
		policy._fixtureDef.density = WoodDensity;
		policy._fixtureDef.restitution = WoodRestitution;
		policy._fixtureDef.friction = WoodFriction;

		policy._bodyType	= b2_dynamicBody;
		policy._blockType	= GameTypes::eOther;
		policy._spriteLayer = GameTypes::eSpritesWood;
		policy._blockEnum	= GameTypes::eBlockBoxWood;

		return policy;
	}
};

template<int W, int H, BlockScaleType T = eBlockScaled>
class puWoodBox : public puBaseBoxTemplated<WoodBoxPolicy, W, H, T>
{
public:
	puWoodBox()
	{
	}
};

typedef puBaseBox_<WoodBoxPolicy> puWoodBox_;


//----------------------------------------------------------------------------------------
// Wooden Box2
//----------------------------------------------------------------------------------------
class Wood2BoxPolicy
{
public:
	static BoxPolicy GetPolicy()
	{
		BoxPolicy policy;
		policy._fixtureDef.density = WoodDensity;
		policy._fixtureDef.restitution = WoodRestitution;
		policy._fixtureDef.friction = WoodFriction;

		policy._bodyType = b2_dynamicBody;
		policy._blockType = GameTypes::eOther;
		policy._spriteLayer = GameTypes::eSpritesWood;
		policy._blockEnum	= GameTypes::eBlockBoxWood2;

		return policy;
	}
};

template<int W, int H, BlockScaleType T = eBlockScaled>
class puWood2Box : public puBaseBoxTemplated<Wood2BoxPolicy, W, H, T>
{
public:
	puWood2Box()
	{
	}
};

typedef puBaseBox_<Wood2BoxPolicy> puWood2Box_;



//----------------------------------------------------------------------------------------
// Fragile Box Preloader
//----------------------------------------------------------------------------------------
class FragileBoxPreloader 
{
protected:
	FragileBoxPreloader( int w, int h );
};



//----------------------------------------------------------------------------------------
// Fragile Box
//----------------------------------------------------------------------------------------
class FragileBoxPolicy
{
public:

	static BoxPolicy GetPolicy()
	{
		BoxPolicy policy;
		policy._fixtureDef.density = FragileDensity;
		policy._fixtureDef.restitution = FragileRestitution;
		policy._fixtureDef.friction = FragileFriction;

		policy._bodyType	= b2_dynamicBody;
		policy._blockType	= GameTypes::eDestroyable;
		policy._spriteLayer = GameTypes::eSprites;
		policy._blockEnum	= GameTypes::eBlockBoxFragile;

		return policy;
	}
};

template<int W, int H, BlockScaleType T = eBlockScaled>
class puFragileBox : public puBaseBoxTemplated<FragileBoxPolicy, W, H, T>, public FragileBoxPreloader
{
public:
	puFragileBox() : FragileBoxPreloader( W, H )
	{
	} 
};

typedef puBaseBox_<FragileBoxPolicy> puFragileBox_;



//----------------------------------------------------------------------------------------
// Fragile Box Static
//----------------------------------------------------------------------------------------
class FragileBoxStaticPolicy
{
public:
	static BoxPolicy GetPolicy()
	{
		BoxPolicy policy;

		policy = FragileBoxPolicy::GetPolicy();
		policy._fixtureDef.density		= FragileDensity;
		policy._fixtureDef.restitution	= FragileRestitution;
		policy._fixtureDef.friction		= FragileFriction;
		
		policy._bodyType	= b2_staticBody;
		policy._blockType	= GameTypes::eDestroyable;
		policy._spriteLayer = GameTypes::eSprites;
		policy._blockEnum	= GameTypes::eBlockBoxFragileStatic;

		return policy;
	}
};

template<int W, int H, BlockScaleType T = eBlockScaled>
class puFragileBoxStatic : public puBaseBoxTemplated<FragileBoxStaticPolicy, W, H, T>, public FragileBoxPreloader
{
public:
	puFragileBoxStatic() : FragileBoxPreloader( W, H )
	{
		puBaseBoxTemplated<FragileBoxStaticPolicy, W, H, T>::MySetSpriteStatic();
	}

	virtual void DestroyFragile()
	{
		this->DoDestroyFragile();
		AnimerSimpleEffect::Get()->Anim_GlueDestroy( puFragileBoxStatic::_glueSprite );
	}
};


class puFragileBoxStatic_ : public puBaseBox_<FragileBoxStaticPolicy>, public FragileBoxPreloader
{
public:
	puFragileBoxStatic_( float w, float h ) : puBaseBox_<FragileBoxStaticPolicy>( w, h ), FragileBoxPreloader((int) w, (int) h )
	{
		puBaseBox_<FragileBoxStaticPolicy>::MySetSpriteStatic();
	}

	virtual void DestroyFragile()
	{
		this->DoDestroyFragile();
		AnimerSimpleEffect::Get()->Anim_GlueDestroy( puFragileBoxStatic_::_glueSprite );
	}

};

//----------------------------------------------------------------------------------------
// Bouncer Dynamic box
//----------------------------------------------------------------------------------------
class BouncerBoxPolicy
{
public:
	static BoxPolicy GetPolicy()
	{
		BoxPolicy policy;
		policy._fixtureDef.density = 0.3f;
		policy._fixtureDef.restitution = 1.0f;
		policy._fixtureDef.friction = 0.5f;
		
		policy._bodyType	= b2_dynamicBody;
		policy._blockType	= GameTypes::eOther;
		policy._spriteLayer = GameTypes::eSprites;
		policy._blockEnum	= GameTypes::eBlockBoxBounce;

		return policy;
	}
};
template<int W, int H, BlockScaleType T = eBlockScaled>
class puBouncerBox : public puBaseBoxTemplated<BouncerBoxPolicy, W, H, T>
{
public:
	puBouncerBox()
	{
		this->_sprite = AnimManager::Get()->CreatePlainSprite( this, 15.0f ); 
		this->_spriteShadow = AnimManager::Get()->CreatePlainSprite( this, 15.0f ); 
		this->_spriteZOrder = GameTypes::eSpritesCharacterBelowe;
	}

};

class puBouncerBox_ : public puBaseBox_<BouncerBoxPolicy>
{
public:
	puBouncerBox_( float w, float h ) : puBaseBox_<BouncerBoxPolicy>( w, h )
	{
		this->_sprite = AnimManager::Get()->CreatePlainSprite( this, 15.0f ); 
		this->_spriteShadow = AnimManager::Get()->CreatePlainSprite( this, 15.0f ); 
		this->_spriteZOrder = GameTypes::eSpritesCharacterBelowe;
	}
};

//----------------------------------------------------------------------------------------
// Glass Dynamic box
//----------------------------------------------------------------------------------------
class GlassBoxPolicy
{
public:
	static BoxPolicy GetPolicy()
	{
		BoxPolicy policy;
		policy._fixtureDef.density = GlassDensity;
		policy._fixtureDef.restitution = GlassRestitution;
		policy._fixtureDef.friction = GlassFriction;
		
		policy._bodyType	= b2_dynamicBody;
		policy._blockType	= GameTypes::eGlass;
		policy._spriteLayer = GameTypes::eSprites;

		return policy;
	}
};


template<int W, int H, BlockScaleType T = eBlockScaled>
class puGlassBox : public puBaseBoxTemplated< GlassBoxPolicy, W, H, T>
{
public:
	puGlassBox<W, H, T>() 
	{ 
		puGlassBox<W, H, T>::_speedBreak = GlassBreakSpeed; 
	}
};


class puGlassBox_ : public puBaseBox_<GlassBoxPolicy> 
{
public:
	puGlassBox_( float w, float h ) : puBaseBox_<GlassBoxPolicy>( w, h )
	{ 
		_speedBreak = GlassBreakSpeed;
	}
};


//----------------------------------------------------------------------------------------
// Domino Dynamic box
//----------------------------------------------------------------------------------------
class DominoBoxPolicy
{
public:
	static BoxPolicy GetPolicy()
	{
		BoxPolicy policy;
		policy._fixtureDef.density = DominoDensity;
		policy._fixtureDef.restitution = DominoRestitution;
		policy._fixtureDef.friction = DominoFriction;
		policy._bodyType	= b2_dynamicBody;
		policy._blockType	= GameTypes::eOther;
		policy._spriteLayer = GameTypes::eSprites;

		return policy;
	}
};
template<int W, int H, BlockScaleType T = eBlockScaled>
class puDominoBox : public puBaseBoxTemplated< DominoBoxPolicy, W, H, T>
{
};

typedef puBaseBox_<DominoBoxPolicy> puDominoBox_;


//----------------------------------------------------------------------------------------
// Lift Platform Box
//----------------------------------------------------------------------------------------
class LiftPlatformPolicy
{
public:
	static BoxPolicy GetPolicy()
	{
		BoxPolicy policy;
		policy._fixtureDef.density = LiftPlatformDensity;
		policy._fixtureDef.restitution = LiftPlatformRestitution;
		policy._fixtureDef.friction = LiftPlatformFriction;

		policy._bodyType	= b2_dynamicBody;
		policy._blockType	= GameTypes::eOther;
		policy._spriteLayer = GameTypes::eSprites;
		policy._blockEnum	= GameTypes::eBlockBoxLiftUp;

		return policy;
	}
};

template<int W, int H, BlockScaleType T = eBlockScaled>
class puLiftPlatform : public puBaseBoxTemplated<LiftPlatformPolicy, W, H, T>
{
public:
	puLiftPlatform()
	{
	} 
};


typedef puBaseBox_<LiftPlatformPolicy> puLiftPlatform_;
#endif
