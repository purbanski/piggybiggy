#ifndef __PUCIRCLE_H__
#define __PUCIRCLE_H__

#include <set>
#include <string>
#include <Box2D/Box2D.h>

#include "puBlock.h"

using namespace std;

set<GameTypes::BlockEnum> CirclesClassNames();

typedef enum {
	eCircle = 1,
	eCircle_Fragile,
	eCircle_FragileStatic,
	eCircle_Stone,
	eCircle_Wall,
	eCircle_Wood,
	eCircle_Screw,
	eCircle_Reel,
	eCircle_RotationWheel,
	eCircle_Cop,
	eCircle_Iron,
	eCircle_Thief,
	eCircle_ThiefStatic,
	eCircle_Button,
	eCircle_Bomb,
	eCircle_MagnetBomb,
	eCircle_MagnetBombPlus,
	eCircle_MagnetBombMinus,
	eCircle_Coin,
	eCircle_CoinSilver,
	eCircle_CoinStatic,
	eCircle_PiggyBank,
	eCircle_PiggyBankStatic,
	eCircle_PiggyBankIron,
	eCircle_PiggyMother,
	eCircle_PiggyMotherStatic,
	eCircle_Magnet,
	eCircle_MagnetMinus,
	eCircle_MagnetPlus,
	eCircle_PiggyFarm,
	eCircle_AirBubble,
	eCircle_GumBall,
	eCircle_Glass
} PUCircleTypes;


//---------------------------------------------------------------------------------------------------
// Circle base
//---------------------------------------------------------------------------------------------------
class puCircleBase : public puBlock
{
public:
	puCircleBase( float radius  );
	virtual ~puCircleBase(){}

protected:
	virtual void LoadDefaultSprite();
	virtual void LoadDefaultSpriteStatic();

	void SetDefaultSpriteScale();

protected:
	float			_radius;
};


//---------------------------------------------------------------------------------------------------
// Static circle
//---------------------------------------------------------------------------------------------------
class puCircle_ : public puCircleBase
{
public:
	puCircle_() : puCircleBase( 4.0f ) { Construct( 4.0f ); }
	puCircle_( float radius ) : puCircleBase( radius ) { Construct( radius ); }

	virtual ~puCircle_(){};

protected:
	virtual void Construct( float radius );
};
//---------------------------------------------------------------------------------------------------
template <int TRadius = 40, BlockScaleType T = eBlockNormal>
class puCircle : public puCircle_
{
public:
	puCircle() : puCircle_( (float) TRadius / 10.0f / float( T ) ) {}

};
//---------------------------------------------------------------------------------------------------



class CircleFragilePreloader
{
protected:
	CircleFragilePreloader() {};
	void PreloadResources( float radius );
};

//---------------------------------------------------------------------------------------------------
// CircleFragile
//---------------------------------------------------------------------------------------------------
class puCircleFragile_ : public puCircleBase, public CircleFragilePreloader
{
public:
	puCircleFragile_() : puCircleBase( 4.0f ) { Construct( 4.0f ); }
	puCircleFragile_( float radius ) : puCircleBase( radius ) { Construct( radius ); }
	
	virtual ~puCircleFragile_(){};
	virtual void DestroyFragile();

protected:
	virtual void Construct( float radius );
};
//---------------------------------------------------------------------------------------------------
template <int TRadius = 40, BlockScaleType T = eBlockNormal>
class puCircleFragile : public puCircleFragile_
{
public:
	puCircleFragile() : puCircleFragile_( (float) TRadius / 10.0f / float( T ) ) {}

};
//---------------------------------------------------------------------------------------------------


//---------------------------------------------------------------------------------------------------
// CircleFragile
//---------------------------------------------------------------------------------------------------
class puCircleFragileStatic_ : public puCircleBase, public CircleFragilePreloader
{
public:
	puCircleFragileStatic_() : puCircleBase( 4.0f ) { Construct( 4.0f ); }
	puCircleFragileStatic_( float radius ) : puCircleBase( radius ) { Construct( radius ); }

	virtual ~puCircleFragileStatic_(){};
	virtual void DestroyFragile();

protected:
	virtual void Construct( float radius );
};
//---------------------------------------------------------------------------------------------------
template <int TRadius = 40, BlockScaleType T = eBlockNormal>
class puCircleFragileStatic : public puCircleFragileStatic_
{
public:
	puCircleFragileStatic() : puCircleFragileStatic_( (float) TRadius / 10.0f / float( T ) ) {}

};
//---------------------------------------------------------------------------------------------------



//---------------------------------------------------------------------------------------------------
// Circle Stone
//---------------------------------------------------------------------------------------------------
class puCircleStone_ : public puCircleBase
{
public:
	puCircleStone_() : puCircleBase( 4.0f ) { Construct( 4.0f ); }
	puCircleStone_( float radius ) : puCircleBase( radius ) { Construct( radius ); }

	virtual ~puCircleStone_(){};

protected:
	virtual void Construct( float radius );
};
//---------------------------------------------------------------------------------------------------
template <int TRadius = 40, BlockScaleType T = eBlockNormal>
class puCircleStone : public puCircleStone_
{
public:
	puCircleStone() : puCircleStone_( (float) TRadius / 10.0f / float( T ) ) {}

};
//---------------------------------------------------------------------------------------------------



//---------------------------------------------------------------------------------------------------
// Circle Wall
//---------------------------------------------------------------------------------------------------
class puCircleWall_ : public puCircleBase
{
public:
	puCircleWall_() : puCircleBase( 4.0f ) { Construct( 4.0f ); }
	puCircleWall_( float radius ) : puCircleBase( radius ) { Construct( radius ); }

	virtual ~puCircleWall_(){};

protected:
	virtual void Construct( float radius );
};
//---------------------------------------------------------------------------------------------------
template <int TRadius = 40, BlockScaleType T = eBlockNormal>
class puCircleWall : public puCircleWall_
{
public:
	puCircleWall() : puCircleWall_( (float) TRadius / 10.0f / float( T ) ) {}

};
//---------------------------------------------------------------------------------------------------



//---------------------------------------------------------------------------------------------------
// Circle Wood
//---------------------------------------------------------------------------------------------------
class puCircleWood_ : public puCircleBase
{
public:
	puCircleWood_() : puCircleBase( 4.0f ) { Construct( 4.0f ); }
	puCircleWood_( float radius ) : puCircleBase( radius ) { Construct( radius ); }

	virtual ~puCircleWood_(){};

protected:
	virtual void Construct( float radius );
};
//---------------------------------------------------------------------------------------------------
template <int TRadius = 40, BlockScaleType T = eBlockNormal>
class puCircleWood : public puCircleWood_
{
public:
	puCircleWood() : puCircleWood_( (float) TRadius / 10.0f / float( T ) ) {}

};
//---------------------------------------------------------------------------------------------------





//---------------------------------------------------------------------------------------------------
// Circle Iron
//---------------------------------------------------------------------------------------------------
class puCircleIron_ : public puCircleBase
{
public:
	puCircleIron_() : puCircleBase( 4.0f ) { Construct( 4.0f ); }
	puCircleIron_( float radius ) : puCircleBase( radius ) { Construct( radius ); }

	virtual ~puCircleIron_(){};

protected:
	virtual void Construct( float radius );
};
//---------------------------------------------------------------------------------------------------
template <int TRadius = 40, BlockScaleType T = eBlockNormal>
class puCircleIron : public puCircleIron_
{
public:
	puCircleIron() : puCircleIron_( (float) TRadius / 10.0f / float( T ) ) {}
};
//---------------------------------------------------------------------------------------------------




//---------------------------------------------------------------------------------------------------
// Screw
//---------------------------------------------------------------------------------------------------
class puScrew_ : public puCircleBase
{
public:
	puScrew_ () : puCircleBase( 1.2f ) { Construct( 1.2f ); }
	puScrew_( float radius ) : puCircleBase( radius ) { Construct( radius ); }

	virtual ~puScrew_(){};

protected:
	virtual void Construct( float radius );
};
//---------------------------------------------------------------------------------------------------
template <int TRadius = 12, BlockScaleType T = eBlockNormal>
class puScrew : public puScrew_
{
public:
	puScrew() : puScrew_( (float) TRadius / 10.0f / float( T ) ) {}
};
//---------------------------------------------------------------------------------------------------



//---------------------------------------------------------------------------------------------------
// Circle Glass
//---------------------------------------------------------------------------------------------------
class puCircleGlass_ : public puCircleBase
{
public:
	puCircleGlass_ () : puCircleBase( 4.8f ) { Construct( 4.8f ); }
	puCircleGlass_( float radius ) : puCircleBase( radius ) { Construct( radius ); }

	virtual ~puCircleGlass_(){};

protected:
	virtual void Construct( float radius );
};
//---------------------------------------------------------------------------------------------------
template <int TRadius = 48, BlockScaleType T = eBlockNormal>
class puCircleGlass : public puCircleGlass_
{
public:
	puCircleGlass() : puCircleGlass_( (float) TRadius / 10.0f / float( T ) ) {}
};
//---------------------------------------------------------------------------------------------------



//---------------------------------------------------------------------------------------------------
// Reel
//---------------------------------------------------------------------------------------------------
class puReel_ : public puCircleBase
{
public:
	puReel_ () : puCircleBase( 15.0f ) { Construct( 15.0f ); }
	puReel_( float radius ) : puCircleBase( radius ) { Construct( radius ); }

	virtual ~puReel_();;

	virtual void CreateJoints();
	virtual void DestroyJoints();

protected:
	virtual void Construct( float radius );

private:
	puScrew_			_screw1;
	b2RevoluteJoint		*_joint1;
};
//---------------------------------------------------------------------------------------------------
template <int TRadius = 150, BlockScaleType T = eBlockNormal>
class puReel : public puReel_
{
public:
	puReel() : puReel_( (float) TRadius / 10.0f / float( T ) ) {}
};
//---------------------------------------------------------------------------------------------------




//---------------------------------------------------------------------------------------------------
// Button
//---------------------------------------------------------------------------------------------------
class puButton_ : public puCircleBase
{
public:
	puButton_ () : puCircleBase( 15.0f ) { Construct( 15.0f ); }
	puButton_( float radius ) : puCircleBase( radius ) { Construct( radius ); }

	virtual ~puButton_(){};

protected:
	virtual void Construct( float radius );
};
//---------------------------------------------------------------------------------------------------
template <int TRadius = 150, BlockScaleType T = eBlockNormal>
class puButton : public puButton_
{
public:
	puButton() : puButton_( (float) TRadius / 10.0f / float( T ) ) {}
};
//---------------------------------------------------------------------------------------------------


//---------------------------------------------------------------------------------------------------
// Rotation Wheel
//---------------------------------------------------------------------------------------------------
class puRotationWheel_ : public puBlock
{
public:
	puRotationWheel_() { Construct( 13.0f ); }
	puRotationWheel_( float radius ) { Construct( radius ); }

	virtual ~puRotationWheel_(){};

	virtual void CreateJoints();
	virtual void DestroyJoints();

protected:
	virtual void Construct( float radius );

protected:
	puScrew_			_screw1;
	b2RevoluteJoint		*_joint1;
};
//---------------------------------------------------------------------------------------------------
template <int TRadius = 130, BlockScaleType T = eBlockNormal>
class puRotationWheel : public puRotationWheel_
{
public:
	puRotationWheel() : puRotationWheel_( (float) TRadius / 10.0f / float( T ) ) {}
};
//---------------------------------------------------------------------------------------------------



//---------------------------------------------------------------------------------------------------
// Thief Rolled
//---------------------------------------------------------------------------------------------------
class puThiefRolled_ : public puCircleBase
{
public:
	puThiefRolled_() : puCircleBase( 4.4f ) { Construct( 4.4f ); }
	puThiefRolled_( float radius ) : puCircleBase( radius ) { Construct( radius ); }

	virtual ~puThiefRolled_(){};
	virtual void Construct( float radius );
};
//---------------------------------------------------------------------------------------------------
template <int TRadius = 44, BlockScaleType T = eBlockNormal>
class puThiefRolled : public puThiefRolled_
{
public:
	puThiefRolled() : puThiefRolled_( (float) TRadius / 10.0f / float( T ) ) {}
};
//---------------------------------------------------------------------------------------------------



//---------------------------------------------------------------------------------------------------
// Thief
//---------------------------------------------------------------------------------------------------
class puThief_ : public puCircleBase
{
public:
	puThief_() : puCircleBase( 4.4f ) { Construct( 4.4f ); }
	puThief_( float radius ) : puCircleBase( radius ) { Construct( radius ); }

	virtual ~puThief_(){};

protected:
	virtual void Construct( float radius );
};
//---------------------------------------------------------------------------------------------------
template <int TRadius = 44, BlockScaleType T = eBlockNormal>
class puThief : public puThief_
{
public:
	puThief() : puThief_( (float) TRadius / 10.0f / float( T ) ) {}
};
//---------------------------------------------------------------------------------------------------


//---------------------------------------------------------------------------------------------------
// ThiefStatic
//---------------------------------------------------------------------------------------------------
class puThiefStatic_ : public puCircleBase
{
public:
	puThiefStatic_() : puCircleBase( 4.4f ) { Construct( 4.4f ); }
	puThiefStatic_( float radius ) : puCircleBase( radius ) { Construct( radius ); }

	virtual ~puThiefStatic_(){};
	virtual void Destroy();

protected:
	virtual void Construct( float radius );
};
//---------------------------------------------------------------------------------------------------
template <int TRadius = 44, BlockScaleType T = eBlockNormal>
class puThiefStatic : public puThiefStatic_
{
public:
	puThiefStatic() : puThiefStatic_( (float) TRadius / 10.0f / float( T ) ) {}
};
//---------------------------------------------------------------------------------------------------


//---------------------------------------------------------------------------------------------------
// Coin
//---------------------------------------------------------------------------------------------------
class puCoin_ : public puCircleBase
{
public:
	puCoin_() : puCircleBase( 4.0f ) { Construct( 4.0f ); }
	puCoin_( float radius ) : puCircleBase( radius ) { Construct( radius ); }
	
	virtual ~puCoin_();
	virtual void Destroy();

protected:
	virtual void Construct( float radius );
};
//---------------------------------------------------------------------------------------------------
template <int TRadius = 40, BlockScaleType T = eBlockNormal>
class puCoin : public puCoin_
{
public:
	puCoin() : puCoin_( (float) TRadius / 10.0f / float( T ) ) {}
};
//---------------------------------------------------------------------------------------------------




//---------------------------------------------------------------------------------------------------
// Coin Silver
//---------------------------------------------------------------------------------------------------
class puCoinSilver_ : public puCoin_
{
public:
	puCoinSilver_() : puCoin_( 4.0f ), _coin1( 4.0f ), _coin2( 4.0f ) { Construct( 4.0f ); }
	puCoinSilver_( float radius ) : puCoin_( radius ), _coin1( radius ), _coin2( radius ) { Construct( radius ); }

	virtual ~puCoinSilver_(){};
	virtual void Step();
	virtual void Destroy();

	void MoneyMakeMoney();

protected:
	virtual void Construct( float radius );

protected:
	puCoin_		_coin1;
	puCoin_		_coin2;

	bool		_moneyMade;
	bool		_stopChecking;
};
//---------------------------------------------------------------------------------------------------
template <int TRadius = 40, BlockScaleType T = eBlockNormal>
class puCoinSilver : public puCoinSilver_
{
public:
	puCoinSilver() : puCoinSilver_( (float) TRadius / 10.0f / float( T ) ) {}
};
//---------------------------------------------------------------------------------------------------



//---------------------------------------------------------------------------------------------------
// CoinStatic
//---------------------------------------------------------------------------------------------------
class puCoinStatic_ : public puCoin_
{
public:
	puCoinStatic_() : puCoin_( 4.0f ) { Construct( 4.0f ); }
	puCoinStatic_( float radius ) : puCoin_( radius ) { Construct( radius ); }

	virtual ~puCoinStatic_(){};
	virtual void Destroy();

protected:
	virtual void Construct( float radius );
};
//---------------------------------------------------------------------------------------------------
template <int TRadius = 40, BlockScaleType T = eBlockNormal>
class puCoinStatic : public puCoinStatic_
{
public:
	puCoinStatic() : puCoinStatic_( (float) TRadius / 10.0f / float( T ) ) {}
};
//---------------------------------------------------------------------------------------------------



//---------------------------------------------------------------------------------------------------
// Cop
//---------------------------------------------------------------------------------------------------
class puCop_ : public puCircleBase
{
public:
	puCop_() : puCircleBase( 5.0f ) { Construct( 5.0f ); }
	puCop_( float radius ) : puCircleBase( radius ) { Construct( radius ); }

	virtual ~puCop_(){};

protected:
	virtual void Construct( float radius );
};
//---------------------------------------------------------------------------------------------------
template <int TRadius = 50, BlockScaleType T = eBlockNormal>
class puCop : public puCop_
{
public:
	puCop() : puCop_( (float) TRadius / 10.0f / float( T ) ) {}
};
//---------------------------------------------------------------------------------------------------



//---------------------------------------------------------------------------------------------------
// PiggyBankRolled
//---------------------------------------------------------------------------------------------------
class puPiggyBankRolled_ : public puCircleBase
{
public:
	puPiggyBankRolled_() : puCircleBase( 6.0f ) { Construct( 6.0f ); }
	puPiggyBankRolled_( float radius ) : puCircleBase( radius ) { Construct( radius ); }

	virtual ~puPiggyBankRolled_(){};

protected:
	virtual void Construct( float radius );
};
//---------------------------------------------------------------------------------------------------
template <int TRadius = 60, BlockScaleType T = eBlockNormal>
class puPiggyBankRolled : public puPiggyBankRolled_
{
public:
	puPiggyBankRolled() : puPiggyBankRolled_( (float) TRadius / 10.0f / float( T ) ) {}
};
//---------------------------------------------------------------------------------------------------



//---------------------------------------------------------------------------------------------------
// PiggyBank
//---------------------------------------------------------------------------------------------------
class puPiggyBank_ : public puCircleBase
{
public:
	puPiggyBank_() : puCircleBase( 6.0f ) { Construct( 6.0f ); }
	puPiggyBank_( float radius ) : puCircleBase( radius ) { Construct( radius ); }

	virtual ~puPiggyBank_(){};

protected:
	virtual void Construct( float radius );
};
//---------------------------------------------------------------------------------------------------
template <int TRadius = 60, BlockScaleType T = eBlockNormal>
class puPiggyBank : public puPiggyBank_
{
public:
	puPiggyBank() : puPiggyBank_( (float) TRadius / 10.0f / float( T ) ) {}
};
//---------------------------------------------------------------------------------------------------




//---------------------------------------------------------------------------------------------------
// PiggyBankLittle
//---------------------------------------------------------------------------------------------------
class puPiggyBankLittle_ : public puCircleBase
{
public:
	puPiggyBankLittle_() : puCircleBase( 1.8f ) { Construct( 1.8f ); }
	puPiggyBankLittle_( float radius ) : puCircleBase( radius ) { Construct( radius ); }

	virtual ~puPiggyBankLittle_(){};

protected:
	virtual void Construct( float radius );
};
//---------------------------------------------------------------------------------------------------
template <int TRadius = 18, BlockScaleType T = eBlockNormal>
class puPiggyBankLittle : public puPiggyBankLittle_
{
public:
	puPiggyBankLittle() : puPiggyBankLittle_( (float) TRadius / 10.0f / float( T ) ) {}
};
//---------------------------------------------------------------------------------------------------



//---------------------------------------------------------------------------------------------------
// PiggyBankStatic
//---------------------------------------------------------------------------------------------------
class puPiggyBankStatic_ : public puCircleBase
{
public:
	puPiggyBankStatic_() : puCircleBase( 6.0f ) { Construct( 6.0f ); }
	puPiggyBankStatic_( float radius ) : puCircleBase( radius ) { Construct( radius ); }

	virtual ~puPiggyBankStatic_(){};
	virtual void Destroy();

protected:
	virtual void Construct( float radius );
};
//---------------------------------------------------------------------------------------------------
template <int TRadius = 60, BlockScaleType T = eBlockNormal>
class puPiggyBankStatic : public puPiggyBankStatic_
{
public:
	puPiggyBankStatic() : puPiggyBankStatic_( (float) TRadius / 10.0f / float( T ) ) {}
};
//---------------------------------------------------------------------------------------------------



//---------------------------------------------------------------------------------------------------
// PiggyBankIron
//---------------------------------------------------------------------------------------------------
class puPiggyBankIron_ : public puPiggyBank_
{
public:
	puPiggyBankIron_() : puPiggyBank_( 6.0f ) { LoadDefaultSprite(); }
	puPiggyBankIron_( float radius ) : puPiggyBank_( radius ) { LoadDefaultSprite();}

	virtual ~puPiggyBankIron_(){};
};
//---------------------------------------------------------------------------------------------------
template <int TRadius = 60, BlockScaleType T = eBlockNormal>
class puPiggyBankIron : public puPiggyBankIron_
{
public:
	puPiggyBankIron() : puPiggyBankIron_( (float) TRadius / 10.0f / float( T ) ) {}
};
//---------------------------------------------------------------------------------------------------



//---------------------------------------------------------------------------------------------------
// TyreA
//---------------------------------------------------------------------------------------------------
class puTyreA_ : public puCircleBase
{
public:
	puTyreA_() : puCircleBase( 3.0f ) { Construct( 3.0f ); }
	puTyreA_( float radius ) : puCircleBase( radius ) { Construct( radius ); }

	virtual ~puTyreA_(){};

protected:
	virtual void Construct( float radius );
};
//---------------------------------------------------------------------------------------------------
template <int TRadius = 30>
class puTyreA : public puTyreA_
{
public:
	puTyreA() : puTyreA_( (float) TRadius / 10.0f ) {}

};
//---------------------------------------------------------------------------------------------------


//---------------------------------------------------------------------------------------------------
// GumBall
//---------------------------------------------------------------------------------------------------
class puGumBall_ : public puCircleBase
{
public:
	puGumBall_() : puCircleBase( 4.0f ) { Construct( 4.0f ); }
	puGumBall_( float radius ) : puCircleBase( radius ) { Construct( radius ); }

	virtual ~puGumBall_(){};

protected:
	virtual void Construct( float radius );
};
//---------------------------------------------------------------------------------------------------
template <int TRadius = 40, BlockScaleType T = eBlockNormal>
class puGumBall : public puGumBall_
{
public:
	puGumBall() : puGumBall_( (float) TRadius / 10.0f / float( T ) ) {}
};
//---------------------------------------------------------------------------------------------------


//-----------------------------------------------------------------------------------------------------
#endif
