#include "puL.h"
//---------------------------------------------------------------------------------------------
puL_::puL_( float width, float height ) : puWoodBox_( width, height ), _wood1( width, height )
{
	_width = width;
	_height = height;
	// _wood1
	_wood1.GetBody()->GetFixtureList()->SetDensity( 8.0f );
	_wood1.GetBody()->GetFixtureList()->SetFriction( 0.5f );
	_wood1.GetBody()->GetFixtureList()->SetRestitution( 0.1f );
	_wood1.SetBodyType( b2_dynamicBody );

	// SetNodeChain
	SetNodeChain( &_wood1, NULL );

	// SetDeltaXY
	_wood1.SetDeltaXY(( width - height ) / 2.0f, ( -width + height ) / 2.0f );

	// Main block (Wood)
	GetBody()->GetFixtureList()->SetDensity( 8.0f );
	GetBody()->GetFixtureList()->SetFriction( 0.5f );
	GetBody()->GetFixtureList()->SetRestitution( 0.1f );
	SetBodyType( b2_dynamicBody );
	SetDeltaRotation( -90 );

	SetPosition( 0, 0 );
	CreateJoints();
}
//---------------------------------------------------------------------------------------------
void puL_::CreateJoints()
{
	float anchorX;
	float anchorY;

	anchorX = GetPosition().x;
	anchorY = GetPosition().y + _flipY * ( _height - _width ) / 2 ;

	b2RevoluteJointDef jointDef1;
	jointDef1.Initialize( GetBody(), _wood1.GetBody(), b2Vec2( anchorX, anchorY ));
	_joint1 = (b2RevoluteJoint *) _world->CreateJoint( &jointDef1 );

	_joint1->SetLimits( 0.0f, 0.0f );
	_joint1->EnableLimit( true );
	_joint1->EnableMotor( false );
	_joint1->SetMotorSpeed( 0.0f );
	_joint1->SetMaxMotorTorque( 0.0f );

}
//---------------------------------------------------------------------------------------------
void puL_::DestroyJoints()
{
	_world->DestroyJoint( _joint1 );
	_joint1 = NULL;
}



//---------------------------------------------------------------------------------------------
// puLScrewed_
//---------------------------------------------------------------------------------------------
puLScrewed_::puLScrewed_( float width, float height ) : puL_( width, height )
{
	// SetNodeChain
	SetNodeChain( /*&_wood1, */&_screw1, NULL );

	// SetDeltaXY
	_screw1.SetDeltaXY( 0, ( width - height ) / 2.0f );

	SetPosition( 0, 0 );

	// we need to destroy puL joints
	// as they got created as a part of puL constructor
	puL_::DestroyJoints();
	CreateJoints();
}
//---------------------------------------------------------------------------------------------
void puLScrewed_::CreateJoints()
{
	puL_::CreateJoints();

	float anchorX;
	float anchorY;

	anchorX = _screw1.GetPosition().x;
	anchorY = _screw1.GetPosition().y ;

	b2RevoluteJointDef jointDef1;
	jointDef1.Initialize( GetBody(), _screw1.GetBody(), b2Vec2( anchorX, anchorY ));
	_joint2 = (b2RevoluteJoint *) _world->CreateJoint( &jointDef1 );

	_joint2->SetLimits( 0.0f, 0.0f );
	_joint2->EnableLimit( false );
	_joint2->EnableMotor( false );
	_joint2->SetMotorSpeed( 0.0f );
	_joint2->SetMaxMotorTorque( 0.0f );
}
//---------------------------------------------------------------------------------------------
void puLScrewed_::DestroyJoints()
{
	puL_::DestroyJoints();

	_world->DestroyJoint( _joint2 );
	_joint2 = NULL;
}
//---------------------------------------------------------------------------------------------
