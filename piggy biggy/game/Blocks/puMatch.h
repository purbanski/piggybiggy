#ifndef __PUMATCH_H__
#define	__PUMATCH_H__
 
#include <Box2D/Box2D.h>
#include "puBlock.h"

class puMatch : public puBlock
{
public:
	puMatch();
	puMatch( b2Vec2 size );
	puMatch( b2Vec2 size, float density );

	virtual ~puMatch();

private:
	void Construct( b2Vec2 size, float density );
};

#endif
