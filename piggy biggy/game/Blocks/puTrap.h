#ifndef __PUTRAP_H__
#define __PUTRAP_H__

#include "puBlock.h"
#include "Blocks/puBox.h"

//------------------------------------------------------------------------
class puTrap : public puWoodBox<90,5,eBlockScaled>
{
public:
	puTrap();
	
	virtual void CreateJoints();
	virtual void DestroyJoints();

private:
	puScrew<8>		_screw1;
	puScrew<8>		_screw2;
	puWoodBox<110,10,eBlockScaled>	_woodBox1;

	b2RevoluteJoint	*_joint1;
	b2RevoluteJoint	*_joint2;
	b2RevoluteJoint	*_joint3;
};
//------------------------------------------------------------------------
#endif
