#include "puMatch.h"
#include <Box2D/Box2D.h>
#include "Skins.h"

puMatch::puMatch() : puBlock()
{
	Construct( b2Vec2( 2.0f, 2.0f ), 3.0f );
}
//-----------------------------------------------------------------------------------------------------
puMatch::puMatch( b2Vec2 size ) : puBlock()
{
	Construct( size, 3.0f );
}
//-----------------------------------------------------------------------------------------------------
puMatch::puMatch( b2Vec2 size, float density )
{
	Construct( size, density );
}
//-----------------------------------------------------------------------------------------------------
void puMatch::Construct( b2Vec2 size, float density )
{
	b2PolygonShape box;
	box.SetAsBox( size.x, size.y );

	b2FixtureDef fixtureDef;
	fixtureDef.shape = &box;
	fixtureDef.density = density;
	fixtureDef.friction = 0.6f; // tarcie 0 - nie ma 1- max
	fixtureDef.restitution = 0.1f; // bouncy 0 - nie ma 1 max;
	
	_body->CreateFixture( &fixtureDef );
	_sprite = CCSprite::spriteWithFile( Skins::GetSkinName( "Images/Blocks/Match.png" ) );
}
//-----------------------------------------------------------------------------------------------------
puMatch::~puMatch()
{
}
