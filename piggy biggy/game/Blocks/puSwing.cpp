#include <Box2D/Box2D.h>

#include "Levels/World.h"
#include "Levels/Level.h"
#include "puSwing.h"

puSwing::puSwing()
{	
	b2Filter filter;
	filter.groupIndex = 0;
	filter.categoryBits = Config::eFilterCategorySwing;
	filter.maskBits = ~Config::eFilterCategorySwing & 0xffff;

	//---------------
	// platform
	_body->GetFixtureList()->SetFilterData( filter );
	//SetSprite( CCSprite::spriteWithFile( Skins::GetSkinName( "Images/Blocks/SwingPlatform.png" ) ));

	//---------------
	// swingnail
	_swingNail.GetBody()->SetType( b2_staticBody );
	_swingNail.SetSprite( CCSprite::spriteWithFile( Skins::GetSkinName( "Images/Blocks/SwingNail.png" ) ), GameTypes::eSpritesAboveX3 );

	//---------------
	// swinghead
	_swingHead.GetBody()->SetType( b2_dynamicBody );
	_swingHead.GetBody()->GetFixtureList()->SetFilterData( filter );
	_swingHead.GetBody()->GetFixtureList()->SetRestitution( 0.3f );
	_swingHead.GetBody()->GetFixtureList()->SetDensity( 9.0f );
	_swingHead.GetBody()->GetFixtureList()->SetFriction( 0.7f );
	_swingHead.SetSprite( CCSprite::spriteWithFile( Skins::GetSkinName( "Images/Blocks/SwingHead.png" ) ), GameTypes::eSpritesAbove );


	//---------------
	// ground arm
	b2Filter filter2;
	filter2.categoryBits = 0;
	filter2.maskBits = 0;
	filter2.groupIndex = -1;

	_groundArm.SetRotation( -90.0f );
	_groundArm.SetDeltaRotation( -90.0f );
	_groundArm.GetBody()->GetFixtureList()->SetFilterData( filter2 );
	_groundArm.SetSpriteZOrder( GameTypes::eSpritesBelowe );
	_groundArm.SetBodyType( b2_staticBody );


	//---------------
	// swing arm
	_swingArm.SetBodyType( b2_dynamicBody );
	_swingArm.GetBody()->GetFixtureList()->SetFilterData( filter2 );
	//_swingArm.GetBody()->GetFixtureList()->SetFilterData( filter ); //FIXME set proper filter
	_swingArm.SetSpriteZOrder( GameTypes::eSprites );


	//------------------
	// List
	SetNodeChain( &_groundArm, &_swingArm, &_swingHead, &_swingNail, &_fragile, NULL );

	_groundArm.SetDeltaXY( 0.0f, 9.0f );
	_swingArm.SetDeltaXY( 6.0f, 15.0f );
	_swingHead.SetDeltaXY( 12.0f, 15.0f );
	_fragile.SetDeltaXY( 12.0f, 5.0f );
	_swingNail.SetDeltaXY( 0.0f, 15.0f );

	SetPosition( Config::GameSize.width / 2.0f / RATIO, Config::GameSize.height / 2.0f / RATIO );
	CreateJoints();
}
//-----------------------------------------------------------------------------------------------------
void puSwing::CreateJoints()
{
	//----------------
	// Fixed Joint swing arm - swing head
	b2RevoluteJointDef jointDef;
	jointDef.Initialize( _swingArm.GetBody(), _swingHead.GetBody(), _swingHead.GetPosition() );
	jointDef.enableLimit = true;
	_jointSwingArmSwingHead = (b2RevoluteJoint *) _world->CreateJoint(&jointDef);


	//-----------------
	// joint Ground + swinghead
	b2RevoluteJointDef jd;
	jd.Initialize( _groundArm.GetBody(), _swingArm.GetBody(), _groundArm.GetPosition() + b2Vec2( 0.0f, _flipY * 6.0f ));
	_jointSwing = (b2RevoluteJoint *) _world->CreateJoint(&jd);
}
//-----------------------------------------------------------------------------------------------------
void puSwing::DestroyJoints()
{
	if ( _jointSwing )				_world->DestroyJoint( _jointSwing );
	if ( _jointSwingArmSwingHead )	_world->DestroyJoint( _jointSwingArmSwingHead );

	_jointSwing = NULL;
	_jointSwingArmSwingHead = NULL;
}
//-----------------------------------------------------------------------------------------------------
puSwing::~puSwing()
{
	//DestroyJoints();
	//FIXME // when deleting swing from editor it cause a problem
}





//-----------------------------------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------------------------------
puSwingNoFloor::puSwingNoFloor()
{	
	b2Filter filter;
	filter.groupIndex = 0;
	filter.categoryBits = Config::eFilterCategorySwing;
	filter.maskBits = ~Config::eFilterCategorySwing & 0xffff;

	ResetSpriteZOrder( GameTypes::eSpritesBeloweX2 );
	//---------------
	// platform
	//_body->GetFixtureList()->SetFilterData( filter );
	//SetSprite( CCSprite::spriteWithFile( Skins::GetSkinName( "Images/Blocks/SwingPlatform.png" ) ));

	//---------------
	// swinghead
	_swingHead.GetBody()->SetType( b2_dynamicBody );
	_swingHead.GetBody()->GetFixtureList()->SetFilterData( filter );
	_swingHead.GetBody()->GetFixtureList()->SetRestitution( 0.3f );
	_swingHead.GetBody()->GetFixtureList()->SetDensity( 9.0f );
	_swingHead.GetBody()->GetFixtureList()->SetFriction( 0.7f );
	_swingHead.SetSprite( CCSprite::spriteWithFile( Skins::GetSkinName( "Images/Blocks/SwingHead.png" ) ), GameTypes::eSpritesAbove );


	//---------------
	// ground arm
	b2Filter filter2;
	filter2.categoryBits = 0;
	filter2.maskBits = 0;
	filter2.groupIndex = -1;

	//---------------
	// swing arm
	_swingArm.SetBodyType( b2_dynamicBody );
	_swingArm.GetBody()->GetFixtureList()->SetFilterData( filter2 );
	//_swingArm.GetBody()->GetFixtureList()->SetFilterData( filter ); //FIXME set proper filter
	_swingArm.SetSpriteZOrder( GameTypes::eSpritesBeloweX3 );


	//------------------
	// List
	SetNodeChain( &_swingArm, &_swingHead, NULL );

	_swingArm.SetDeltaXY( 6.0f, 0.0f );
	_swingHead.SetDeltaXY( 12.0f, 0.0f );

	SetPosition( Config::GameSize.width / 2.0f / RATIO, Config::GameSize.height / 2.0f / RATIO );
	CreateJoints();
}
//-----------------------------------------------------------------------------------------------------
void puSwingNoFloor::CreateJoints()
{
	//----------------
	// Fixed Joint swing arm - swing head
	b2RevoluteJointDef jointDef;
	jointDef.Initialize( _swingArm.GetBody(), _swingHead.GetBody(), _swingHead.GetPosition() );
	jointDef.enableLimit = true;
	_jointSwingArmSwingHead = (b2RevoluteJoint *) _world->CreateJoint(&jointDef);


	//-----------------
	// joint Ground + swinghead
	b2RevoluteJointDef jd;
	jd.Initialize( GetBody(), _swingArm.GetBody(), GetPosition() );
	_jointSwing = (b2RevoluteJoint *) _world->CreateJoint(&jd);
}
//-----------------------------------------------------------------------------------------------------
void puSwingNoFloor::DestroyJoints()
{
	if ( _jointSwing )				_world->DestroyJoint( _jointSwing );
	if ( _jointSwingArmSwingHead )	_world->DestroyJoint( _jointSwingArmSwingHead );

	_jointSwing = NULL;
	_jointSwingArmSwingHead = NULL;
}
//-----------------------------------------------------------------------------------------------------
puSwingNoFloor::~puSwingNoFloor()
{
	//DestroyJoints();
	//FIXME // when deleting swing from editor it cause a problem
}
