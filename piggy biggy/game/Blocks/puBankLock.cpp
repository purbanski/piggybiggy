#include <Box2D/Box2D.h>
#include "SoundEngine.h"
#include "Levels/World.h"
#include "Levels/Level.h"
#include "puBankLock.h"
#include "Debug/MyDebug.h"
#include "GameConfig.h"
#include "Components/CustomSprites.h"
#include "Game.h"

//-----------------------------------------------------------------------------------------------------
// SingleBankLock
//-----------------------------------------------------------------------------------------------------
puSingleBankLock::puSingleBankLock()
{
	_lockFitSprite = CCSprite::spriteWithFile(  Skins::GetSkinName( "Images/Blocks/BankSaveLockFit.png" )  );
	_lockUnfitSprite = CCSprite::spriteWithFile(  Skins::GetSkinName( "Images/Blocks/BankSaveLockUnfit.png" ) );
	_sprite = _lockUnfitSprite;
	_spriteZOrder = GameTypes::eSpritesAboveX2;

	_lockFitSprite->retain();
	_lockUnfitSprite->retain();

	_startPos.Set( 0.0f, 0.0f );
	_startPosSet = false;
	_skipValue = 0.0f;
}
//-----------------------------------------------------------------------------------------------------
puSingleBankLock::~puSingleBankLock()
{
	_lockFitSprite->release();
	_lockUnfitSprite->release();
}
//-----------------------------------------------------------------------------------------------------
void puSingleBankLock::Tick()
{
	if ( IsFitted() )
	{
		if ( GetSprite() != _lockFitSprite )
		{
			//RemoveSprite( true, false );
			SetSprite( _lockFitSprite );
			UpdateSprite( false );

			SoundEngine::Get()->PlayEffect( SoundEngine::eSoundBankLockUnlocked );
		}
	}
	else if ( GetSprite() != _lockUnfitSprite )
	{	
		//RemoveSprite( true, false );
		SetSprite( _lockUnfitSprite );
		UpdateSprite( false );
		SoundEngine::Get()->PlayEffect( SoundEngine::eSoundBankLockLocked );
	}
}
//-----------------------------------------------------------------------------------------------------
bool puSingleBankLock::IsFitted()
{
	b2Vec2 pos;
	float hitRange = 0.1f;

	pos = _body->GetPosition();

	if ( _startPos.x - _hitPointDeltaX + hitRange  > pos.x && _startPos.x - _hitPointDeltaX - hitRange < pos.x )
		return true;
	else 
		return false;
}
//-----------------------------------------------------------------------------------------------------
void puSingleBankLock::SetLock( float x )
{
	float borderXDelta = 13.0f;
	x *= -1;
	x /= 3.0f;

	if ( ! _startPosSet )
	{
		_startPos = _body->GetPosition();
		_startPosSet = true;
	}

	// Check if not extend over border	
	if (  (( _startPos.x + x ) <= ( _startPos.x - _hitPointDeltaX + borderXDelta + _skipValue )) && 
		  (( _startPos.x + x ) >= ( _startPos.x - _hitPointDeltaX - borderXDelta + _skipValue )) 
		)
	{
		_body->SetTransform( b2Vec2( _startPos.x + x - _skipValue, _startPos.y ), _body->GetAngle() );
	}
	else
	{
		if ( ( _startPos.x + x ) > ( _startPos.x - _hitPointDeltaX + borderXDelta + _skipValue ) )
		{
			_body->SetTransform( b2Vec2( _startPos.x - _hitPointDeltaX + borderXDelta, _startPos.y ), _body->GetAngle() );
			_skipValue = x + _hitPointDeltaX - borderXDelta;
		}
		else
		{
			_body->SetTransform( b2Vec2( _startPos.x - _hitPointDeltaX - borderXDelta, _startPos.y ), _body->GetAngle() );
			_skipValue = x + _hitPointDeltaX + borderXDelta;
		}
	}
}
//-----------------------------------------------------------------------------------------------------
void puSingleBankLock::SetLockStart( float x )
{
	_hitPointDeltaX = x;
	SetDeltaX( GetDeltaX() + x );
}
//-----------------------------------------------------------------------------------------------------




//-----------------------------------------------------------------------------------------------------
// BankLock
//-----------------------------------------------------------------------------------------------------
puBankLock::puBankLock( BankLockCombination *lockCombination )
{
	_blockEnum = eBlockBankLock;
	_pjoint1 = NULL;
	_pjoint2 = NULL;

	_lockCombination = lockCombination;
	_lockState = eLockLocked;

	// Set body types
	_ironLock1.SetBodyType( b2_staticBody );
	_ironLock2.SetBodyType( b2_staticBody );
	_ironLock3.SetBodyType( b2_staticBody );

	_ironHolder1.SetBodyType( b2_staticBody );
	_ironHolder2.SetBodyType( b2_staticBody );
	_ironHolder1.SetDeltaRotation( 90.0f );
	_ironHolder2.SetDeltaRotation( 90.0f );
	_ironHolder1.RemoveSprite();
	_ironHolder2.RemoveSprite();
	
	_shutter1.SetBodyType( b2_staticBody );
	_shutter2.SetBodyType( b2_staticBody );

	// Set Deltas
	_ironLock1.SetDeltaXY( 0.0f, 2.0f * 2.1f );
	_ironLock2.SetDeltaXY( 0.0f, 2.0f * 5.2f );
	_ironLock3.SetDeltaXY( 0.0f, 2.0f * 8.3f );

	_ironHolder1.SetDeltaXY( 2.0f * 10.0f, 2.0f * 8.0f );
	_ironHolder2.SetDeltaXY( 2.0f * -10.0f, 2.0f * 8.0f );

	_shutter1.SetDeltaXY( 2.0f * -6.24f, 2.0f * 15.0f );
	_shutter2.SetDeltaXY( 2.0f *  6.24f, 2.0f * 15.0f );
	
	_bankSaveWheel1.SetDeltaXY(  2.0f * -3.0f - 0.80f, 2.0f * -9.5f, true );
	_bankSaveWheel2.SetDeltaXY(  2.0f * 13.0f - 0.80f, 2.0f * -9.5f, true );
	_bankSaveWheel3.SetDeltaXY(  2.0f * 29.0f - 0.80f, 2.0f * -9.5f, true );

	_cogWheel1.SetDeltaXY( 19.0f, 4.45f, true );
	_cogWheel2.SetDeltaXY( 19.0f, 10.65f, true );
	_cogWheel3.SetDeltaXY( 19.0f, 16.80f, true );
	_bars.SetDeltaXY( 0.0f, 10.5f );

	// SetNodeChain
	SetNodeChain( &_ironLock1, &_ironHolder1, &_ironLock2, &_ironLock3, &_shutter1, &_shutter2, &_ironHolder2, &_bankSaveWheel1, &_bankSaveWheel2, &_bankSaveWheel3, &_cogWheel1, &_cogWheel2, &_cogWheel3, &_bars, NULL );

	// Main block (IronBox)
	GetBody()->GetFixtureList()->SetDensity( 10.0f );
	GetBody()->GetFixtureList()->SetFriction( 0.6f );
	GetBody()->GetFixtureList()->SetRestitution( 0.2f );
	SetBodyType( b2_staticBody );

	_lockCover = CCSprite::spriteWithFile(  Skins::GetSkinName( "Images/Blocks/BankLockCover.png" ) );
	_lockCover->retain();

	SetPosition( 0.0f, 0.0f );

	SetSprite( MovedSprite::Create( 5.0f, 128.0f, 0.0f, "Images/Blocks/BankLockBorder.png" ), GameTypes::eSprites );
}
//-----------------------------------------------------------------------------------------------------
puBankLock::~puBankLock()
{
	_lockCover->release();
}
//-----------------------------------------------------------------------------------------------------
void puBankLock::Tick()
{
	if ( _lockState == eLockLocked )
	{
		_lockCombination->SetLocks( &_ironLock1, &_ironLock2, &_ironLock3, &_bankSaveWheel1, &_bankSaveWheel2, &_bankSaveWheel3, &_cogWheel1, &_cogWheel2, &_cogWheel3 );
		
		_ironLock1.Tick();
		_ironLock2.Tick();
		_ironLock3.Tick();

		if ( _ironLock1.IsFitted() && _ironLock2.IsFitted() && _ironLock3.IsFitted() )
		{
			_lockState = eLockOpening;
			LockOpen();
		}

	}
	else if ( _lockState == eLockOpening )
	{
		if ( IsLockOpened() )
			_lockState = eLockOpened;
	}
	else if ( _lockState == eLockOpened )
	{
	}
}
//-----------------------------------------------------------------------------------------------------
bool puBankLock::IsLockOpened()
{
	// this is so fragile
	// checking ironMoveLock y, and compare with my

	if ( _shutter1.GetPosition().y - GetPosition().y < 11.0f )
		return true;
	else
		return false;
}
//-----------------------------------------------------------------------------------------------------
void puBankLock::CreateJoints()
{
	_bankSaveWheel1.CreateJoints();
	_bankSaveWheel2.CreateJoints();
	_bankSaveWheel3.CreateJoints();

	b2PrismaticJointDef jointDef1;
	jointDef1.collideConnected = false;
	jointDef1.Initialize( GetBody(), _shutter1.GetBody(), b2Vec2( _shutter1.GetPosition().x, GetPosition().y ), b2Vec2( 0.0f, 1.0f ) );
	_pjoint1 = (b2PrismaticJoint *) _world->CreateJoint( &jointDef1 );

	_pjoint1->EnableMotor( true );
	_pjoint1->SetMotorSpeed( -4.0f );
	_pjoint1->SetMaxMotorForce( 5000000.0f );
	_pjoint1->EnableLimit( true );
	_pjoint1->SetLimits( -19.6f, 0.0f );

	b2PrismaticJointDef jointDef2;
	jointDef2.collideConnected = false;
	jointDef2.Initialize( GetBody(), _shutter2.GetBody(), b2Vec2( _shutter2.GetPosition().x, GetPosition().y ), b2Vec2( 0.0f, 1.0f ) );
	_pjoint2 = (b2PrismaticJoint *) _world->CreateJoint( &jointDef2 );

	_pjoint2->EnableMotor( true );
	_pjoint2->SetMotorSpeed( -4.0f );
	_pjoint2->SetMaxMotorForce( 5000000.0f );
	_pjoint2->EnableLimit( true );
	_pjoint2->SetLimits( -19.6f, 0.0f );

}
//-----------------------------------------------------------------------------------------------------
void puBankLock::DestroyJoints()
{
	_bankSaveWheel1.DestroyJoints();
	_bankSaveWheel2.DestroyJoints();
	_bankSaveWheel3.DestroyJoints();

	if ( _pjoint1 )
	{
		_world->DestroyJoint( _pjoint1 );
		_pjoint1 = NULL;
	}

	if ( _pjoint2 )
	{
		_world->DestroyJoint( _pjoint2 );
		_pjoint2 = NULL;
	}
}
//-----------------------------------------------------------------------------------------------------
void puBankLock::SetStartLocks( float l1, float l2, float l3 )
{
	_ironLock1.SetLockStart( l1 );
	_ironLock2.SetLockStart( l2 );
	_ironLock3.SetLockStart( l3 );
}
//-----------------------------------------------------------------------------------------------------
void puBankLock::LockOpen()
{
	CCFadeTo *fade = new CCFadeTo();
	fade->initWithDuration( 0.5f, 0 );
	_lockCover->runAction( fade );

	SoundEngine::Get()->PlayEffect( SoundEngine::eSoundBankLockBarsSlide );
	_shutter1.SetBodyType( b2_dynamicBody );
	_shutter2.SetBodyType( b2_dynamicBody );
}
//-----------------------------------------------------------------------------------------------------
void puBankLock::HideLocks()
{
	_lockCover->setPosition( ccp( -250.0f, 135.0f ));
	_level->addChild( _lockCover, GameTypes::eSpritesAboveX3 );
}


//-----------------------------------------------------------------------------------------------------
// BankLockCombinations
//-----------------------------------------------------------------------------------------------------
void BankLockCombination_1::SetLocks( puSingleBankLock *lock1, puSingleBankLock *lock2, puSingleBankLock *lock3, puBankSaveWheel *wheel1, puBankSaveWheel *wheel2, puBankSaveWheel *wheel3, puBankLockCogWheels *cogWheel1, puBankLockCogWheels *cogWheel2, puBankLockCogWheels *cogWheel3 )
{
	float rot1;
	float rot2;
	float rot3;

	rot1 = wheel3->GetRotation() / 67.0f									- wheel2->GetRotation() / 37.0f;
	rot2 =									  wheel1->GetRotation() / 33.0f ;
	rot3 =																	+ wheel2->GetRotation() / 71.0f ;

	cogWheel1->SetWheelsRotation( rot1 );
	cogWheel2->SetWheelsRotation( rot2 );
	cogWheel3->SetWheelsRotation( rot3 );

	lock1->SetLock( rot1 );
	lock2->SetLock( rot2 );
	lock3->SetLock( rot3 );
}
//-----------------------------------------------------------------------------------------------------
void BankLockCombination_2::SetLocks( puSingleBankLock *lock1, puSingleBankLock *lock2, puSingleBankLock *lock3, puBankSaveWheel *wheel1, puBankSaveWheel *wheel2, puBankSaveWheel *wheel3, puBankLockCogWheels *cogWheel1, puBankLockCogWheels *cogWheel2, puBankLockCogWheels *cogWheel3 )
{
	float rot1;
	float rot2;
	float rot3;

	rot1 = wheel1->GetRotation() / 41.0f	+ wheel2->GetRotation() / 59.0f + wheel3->GetRotation() / 92.0f;
	rot2 = -wheel1->GetRotation() / 33.0f									+ wheel3->GetRotation() / 92.0f;
	rot3 = wheel1->GetRotation() / 21.0f	- wheel2->GetRotation() / 79.0f	+ wheel3->GetRotation() / 92.0f ;

	cogWheel1->SetWheelsRotation( rot1 );
	cogWheel2->SetWheelsRotation( rot2 );
	cogWheel3->SetWheelsRotation( rot3 );

	lock1->SetLock( rot1 );
	lock2->SetLock( rot2 );
	lock3->SetLock( rot3 );
}
//-----------------------------------------------------------------------------------------------------
void BankLockCombination_3::SetLocks( puSingleBankLock *lock1, puSingleBankLock *lock2, puSingleBankLock *lock3, puBankSaveWheel *wheel1, puBankSaveWheel *wheel2, puBankSaveWheel *wheel3, puBankLockCogWheels *cogWheel1, puBankLockCogWheels *cogWheel2, puBankLockCogWheels *cogWheel3 )
{
	float rot1;
	float rot2;
	float rot3;

	rot1 = wheel1->GetRotation() / 120.0f;
	rot2 =									wheel2->GetRotation() / 75.0f	+ wheel3->GetRotation() / 23.0f;
	rot3 = wheel1->GetRotation() / 57.0f									- wheel3->GetRotation() / 45.0f;

	cogWheel1->SetWheelsRotation( rot1 );
	cogWheel2->SetWheelsRotation( rot2 );
	cogWheel3->SetWheelsRotation( rot3 );

	lock1->SetLock( rot1 );
	lock2->SetLock( rot2 );
	lock3->SetLock( rot3 );
}
//-----------------------------------------------------------------------------------------------------

void BankLockCombination_4::SetLocks( puSingleBankLock *lock1, puSingleBankLock *lock2, puSingleBankLock *lock3, puBankSaveWheel *wheel1, puBankSaveWheel *wheel2, puBankSaveWheel *wheel3, puBankLockCogWheels *cogWheel1, puBankLockCogWheels *cogWheel2, puBankLockCogWheels *cogWheel3 )
{
	float rot1;
	float rot2;
	float rot3;

	rot1 = wheel1->GetRotation() / 97.0f									+ wheel3->GetRotation() / 23.0f;
	rot2 = wheel1->GetRotation() / 39.0f	+ wheel2->GetRotation() / 57.0f	+ wheel3->GetRotation() / 113.0f;
	rot3 = 																	  wheel3->GetRotation() / 41.0f;

	cogWheel1->SetWheelsRotation( rot1 );
	cogWheel2->SetWheelsRotation( rot2 );
	cogWheel3->SetWheelsRotation( rot3 );

	lock1->SetLock( rot1 );
	lock2->SetLock( rot2 );
	lock3->SetLock(	rot3 );
}
//-----------------------------------------------------------------------------------------------------
void BankLockCombination_5::SetLocks( puSingleBankLock *lock1, puSingleBankLock *lock2, puSingleBankLock *lock3, puBankSaveWheel *wheel1, puBankSaveWheel *wheel2, puBankSaveWheel *wheel3, puBankLockCogWheels *cogWheel1, puBankLockCogWheels *cogWheel2, puBankLockCogWheels *cogWheel3 )
{
	float rot1;
	float rot2;
	float rot3;

	rot1 = wheel1->GetRotation() / 79.0f	+ wheel2->GetRotation() / 119.0f;
	rot2 =									+ wheel2->GetRotation() / 87.0f;
	rot3 =									+ wheel2->GetRotation() / 51.0f		+ wheel3->GetRotation() / 91.0f;

	cogWheel1->SetWheelsRotation( rot1 );
	cogWheel2->SetWheelsRotation( rot2 );
	cogWheel3->SetWheelsRotation( rot3 );

	lock1->SetLock( rot1 );
	lock2->SetLock(	rot2 );
	lock3->SetLock(	rot3 );
}



//-----------------------------------------------------------------------------------------------------
// BankLockShutterSlider
//-----------------------------------------------------------------------------------------------------
puBankLockShutterSlider::puBankLockShutterSlider()
{
	b2Filter filter;
	filter.groupIndex = -1 ;

	GetBody()->GetFixtureList()->SetFilterData( filter );

	_spriteZOrder = GameTypes::eSpritesBelowe;
	LoadDefaultSprite();
}



//-----------------------------------------------------------------------------------------------------
// BankLockShutter
//-----------------------------------------------------------------------------------------------------
puBankLockShutter::puBankLockShutter()
{
	_sprite = MovedSprite::Create( 0.0f, 108.0f, 0.0f, "Images/Blocks/BankLockShutter.png" );
	_spriteShadow = MovedSprite::Create( 0.0f, 108.0f, 0.0f, "Images/Blocks/BankLockShutter.png" );
	_spriteZOrder = GameTypes::eSpritesWallAboveX2;
}



//-----------------------------------------------------------------------------------------------------
// BankLockCogWheel
//-----------------------------------------------------------------------------------------------------
puBankLockCogWheel::puBankLockCogWheel()
{
	_blockEnum = eBlockBankLockCogWheel;

	b2Filter filter;
	filter.categoryBits = 0;
	filter.groupIndex = -1;
	filter.maskBits = 0xffff;

	_body->GetFixtureList()->SetFilterData( filter );
	SetBodyType( b2_staticBody );
	
	_spriteZOrder = GameTypes::eSpritesWallAbove;
	_sprite = LoadSkinedSprite( "BankLockCogWheel.png" );
}
//-----------------------------------------------------------------------------------------------------
puBankLockCogWheels::puBankLockCogWheels()
{
	_wheelL.SetDeltaXY( -38.0f, 0.0f );
	_wheelR.SetDeltaXY( 38.0f, 0.0f );

	SetNodeChain( &_wheelL, &_wheelR, NULL);
	SetPosition( 0.0f, 0.0f );
}
//-----------------------------------------------------------------------------------------------------
void puBankLockCogWheels::SetWheelsRotation( float rotation )
{
	_wheelL.SetRotationDirect( rotation * 10.0f );
	_wheelR.SetRotationDirect( rotation * 10.0f );
}



//-----------------------------------------------------------------------------------------------------
// BankLockBars
//-----------------------------------------------------------------------------------------------------
puBankLockBars::puBankLockBars()
{
	_blockEnum = eBlockBankLockBars;
	_spriteZOrder = GameTypes::eSpritesWall;
	LoadDefaultSprite();
}
