#ifndef __PUTRIANGLE_H__
#define __PUTRIANGLE_H__

#include "puBlock.h"

typedef enum {
	eTriangle = 1,
	eTriangleFragile,
	eTriangleStone,
	eTriangleWall,
	eTriangleWood,
	eTriangleEqual,
	eTriangleEqualWood
} PUTriangleTypes;

//---------------------------------------------------------------------------------------------------------
class puTriangleBase : public puBlock
{
public:
	puTriangleBase( const b2Vec2& t1, const b2Vec2& t2, const b2Vec2& t3 );
	void LoadDefaultSprite();

	b2Vec2 GetT1(){ return _tv[0]; }
	b2Vec2 GetT2(){ return _tv[1]; }
	b2Vec2 GetT3(){ return _tv[2]; }

protected:
	b2Vec2	_tv[3];
};
//---------------------------------------------------------------------------------------------------------
class puTriangle_ : public puTriangleBase
{
public:
	puTriangle_( const b2Vec2& t1, const b2Vec2& t2, const b2Vec2& t3 );
};
//---------------------------------------------------------------------------------------------------------
class puTriangleFragile_ : public puTriangleBase
{
public:
	puTriangleFragile_( const b2Vec2& t1, const b2Vec2& t2, const b2Vec2& t3 );
};
//---------------------------------------------------------------------------------------------------------
class puTriangleWall_ : public puTriangleBase
{
public:
	puTriangleWall_( const b2Vec2& t1, const b2Vec2& t2, const b2Vec2& t3 );
};
//---------------------------------------------------------------------------------------------------------
class puTriangleWood_ : public puTriangleBase
{
public:
	puTriangleWood_( const b2Vec2& t1, const b2Vec2& t2, const b2Vec2& t3 );
};
//---------------------------------------------------------------------------------------------------------
template < int Tx1, int Ty1, int Tx2, int Ty2, int Tx3, int Ty3 >
class puTriangleFragile : public puTriangleFragile_
{
public:
	puTriangleFragile() : puTriangleFragile_(
		b2Vec2( Tx1 / 10.0f, Ty1 / 10.0f ),
		b2Vec2( Tx2 / 10.0f, Ty2 / 10.0f ),
		b2Vec2( Tx3 / 10.0f, Ty3 / 10.0f ) ){}
};
//---------------------------------------------------------------------------------------------------------
template < int Tx1, int Ty1, int Tx2, int Ty2, int Tx3, int Ty3 >
class puTriangleWall : public puTriangleWall_
{
public:
	puTriangleWall() : puTriangleWall_(
		b2Vec2( Tx1 / 10.0f, Ty1 / 10.0f ),
		b2Vec2( Tx2 / 10.0f, Ty2 / 10.0f ),
		b2Vec2( Tx3 / 10.0f, Ty3 / 10.0f ) ){}
};
//---------------------------------------------------------------------------------------------------------
template < int Tx1, int Ty1, int Tx2, int Ty2, int Tx3, int Ty3 >
class puTriangleWood : public puTriangleWood_
{
public:
	puTriangleWood() : puTriangleWood_(
		b2Vec2( Tx1 / 10.0f, Ty1 / 10.0f ),
		b2Vec2( Tx2 / 10.0f, Ty2 / 10.0f ),
		b2Vec2( Tx3 / 10.0f, Ty3 / 10.0f ) ){}
};
//---------------------------------------------------------------------------------------------------------
class puTriangleEqual_ : public puTriangleBase
{
public:
	puTriangleEqual_( const b2Vec2& t1, const b2Vec2& t2, const b2Vec2& t3 );
	puTriangleEqual_( float sideSize );
};


#endif
