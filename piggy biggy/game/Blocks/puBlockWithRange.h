#ifndef __PUBLOCKWITHRANGE_H__
#define __PUBLOCKWITHRANGE_H__

#include "puBlock.h"
#include "puCircle.h"

//---------------------------------------------------------------------------------------------------
// Block Range
//---------------------------------------------------------------------------------------------------
class puBlockRange : public puCircleBase
{
public:
	puBlockRange();
	virtual void Destroy();

	void SetRange( float radius );
	float GetRange() { return _radius; }
};


//---------------------------------------------------------------------------------------------------
// Block with range
//---------------------------------------------------------------------------------------------------
class puBlockWithRange_ : public puCircleBase
{
public:
	puBlockWithRange_() : puCircleBase( 4.4f ) { Construct( 4.4f ); }
	puBlockWithRange_( float radius ) : puCircleBase( radius ) { Construct( radius ); }

	virtual ~puBlockWithRange_();

	virtual void BlockRangeIn( puBlock *block );
	virtual void BlockRangeOut( puBlock *block );
    virtual void LevelQuiting();
    
	void SetRange( float radius ) { _blockRange.SetRange( radius); }
	float GetRange() { return _blockRange.GetRange(); }

protected:
	virtual void Construct( float radius );

protected:
	typedef multiset<puBlock*>	BlocksMultiSet;
	typedef set<puBlock*>		BlocksSet;

	puBlockRange		_blockRange;
	BlocksSet			_blocksInRange;

private:
	BlocksMultiSet		_blocksInRangeMulti;
    bool                _quiting;
};
//---------------------------------------------------------------------------------------------------
template <int TRadius = 44, BlockScaleType T = eBlockNormal>
class puBlockWithRange : public puBlockWithRange_
{
public:
	puBlockWithRange() : puBlockWithRange_( (float) TRadius / 10.0f / float( T ) ) {}
};
//---------------------------------------------------------------------------------------------------


#endif
