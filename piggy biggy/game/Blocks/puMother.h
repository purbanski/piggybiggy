#ifndef __PUMOTHER_H__
#define __PUMOTHER_H__

#include <sstream>
#include <set>
#include "unDebug.h"
#include "puCircle.h"
#include "puPolygon.h"
#include "puBlocksSettings.h"
#include "SoundEngine.h"
#include "Components/CustomSprites.h"
#include "GameTypes.h"
#include "Tools.h" // remove me

using namespace std;
using namespace pu::blocks::settings;

//-------------------------------------------------------------------------------------------
// Mother Class Names
//-------------------------------------------------------------------------------------------
set<GameTypes::BlockEnum> MotherClassNames();

typedef enum {
	eMother_Piggy = 1,
	eMother_PiggyStatic,
	eMother_Coin,
	eMother_CoinStatic,
	eMother_Prison
} MotherTypes;

//---------------------------------------------------------------------------------------------------
// Mother
//---------------------------------------------------------------------------------------------------
template <class BlockTempl>
class puMother : public puCircleBase
{
public:
	int GetKidsCount()	{ return _kidsStartCount; }
	float GetKidsSize()	{ return _kidsSize; }
	//----------------------------------------------------------
	void SetKidsCount( int count )
	{
		if ( count > Config::MotherMaxKidCount )
		{
			unAssertMsg( "puMother_", false, ( "Too many kids (max: %d)", Config::MotherMaxKidCount ));
		}
		else
		{
			_kidsCount = count ;
			UpdateCounter();
		}
	}

	virtual ~puMother()
	{
		for ( int i = 0; i < _kidsStartCount; i++ )
		{
			delete  ( _belly[i]  );
		}
	}
	
	void SetKidsFlipX( AxisFlip flip )
	{
		for ( int i = 0; i < _kidsCount; i++ )
			if ( _belly[i]->GetFlipX() != flip ) 
				_belly[i]->FlipX();
	}

protected :
	//-------------------------------------------------------------------------------------
	puMother() : puCircleBase( 3.5f )	{ Construct( 3.5f, ( 3.5f / 2.0f ), Config::MotherMaxKidCount ); }
	puMother( float radius, float kidsSize, int kidsCount ) : puCircleBase( radius )	{ Construct( radius, kidsSize, kidsCount ); }
	
	//----------------------------------------------------------
	virtual void PlayNewBornEffect()
	{
		SoundEngine::Get()->PlayEffect( SoundEngine::eSoundPiggyNewBorn );
	}

	//----------------------------------------------------------
	void Construct( float radius, float kidsSize, int kidsCount )
	{
		_kidsSize = kidsSize;
		_kidsCount = kidsCount;
		_kidsStartCount = kidsCount;

		_width = 2.0f * radius;
		_height = 2.0f * radius;

		b2CircleShape shape;
		shape.m_radius = radius;

		b2FixtureDef fixtureDef;
		fixtureDef.shape = &shape;
		fixtureDef.density = PiggyBankDensity;
		fixtureDef.restitution = PiggyBankRestitution;
		fixtureDef.friction = PiggyBankFriction;

		_body->CreateFixture( &fixtureDef );
		_blockType = GameTypes::eActivable;


		//---------------
		// Kids
		//---------------
		for ( int i = 0; i < _kidsCount; i++ )
		{
			_belly[i] = new BlockTempl( _kidsSize );
			_belly[i]->GetBody()->SetActive( false );
			_belly[i]->SetPosition( b2Vec2( 0.0f, 0.0f ));
#ifdef BUILD_EDITOR
			_belly[i]->SetUID( eUidDontCopy );
#endif
			_belly[i]->GetSprite()->setIsVisible( false );
			_belly[i]->GetSpriteShadow()->setIsVisible( false );
		}
	}
	//-------------------------------------------------------------------------------------------
	void Activate( void *ptr )
	{
		if ( ! ( _kidsCount > 0 ) )
			return;

		puBlock *block;

		block = _belly[ _kidsCount - 1 ];
		block->SetPosition( GetReversedPosition() );
		block->GetBody()->SetActive( true );
		block->GetSprite()->setIsVisible( true );
		block->GetSpriteShadow()->setIsVisible( true );
		
		_kidsCount--;
		UpdateCounter();

		FadeCounterAnim();
		PlayNewBornEffect();
	}
	//-------------------------------------------------------------------------------------------
	void FadeCounterAnim()
	{
		float dur = 0.35f;
		int dest = 230;

		CCActionInterval* seq = ((CCActionInterval*)(CCSequence::actions( 
     		CCTintTo::actionWithDuration( dur, 0, dest, 0 ),
			CCTintTo::actionWithDuration( dur, dest, 0, 0 ),
			CCTintTo::actionWithDuration( dur, 255, 255, 255 ),
			CCFadeTo::actionWithDuration( 0.5f, Config::MotherCounterEndOpacity ),
			NULL)) );

		_counterLabel->runAction( seq );
	}
	//-------------------------------------------------------------------------------------------
	void UpdateCounter()
	{
		stringstream ss;
		ss << _kidsCount;

		_counterLabel->setString( ss.str().c_str() );
	}
	//-------------------------------------------------------------------------------------------
	void CounterConstruct()
	{
		_counterLabel = CCLabelBMFont::labelWithString( "", Config::MotherCounterFont );
		_counterLabel->setOpacity( Config::MotherCounterEndOpacity );
		_counterLabel->setPosition( GetCounterPos() );
	//	_counterLabel->setScale( 1.0f / _sprite->getScale() );
		
		UpdateCounter();
		_sprite->addChild( _counterLabel, 200 );
		
		FadeCounterAnim();
	}
	//-------------------------------------------------------------------------------------------
private:
	CCPoint GetCounterPos()
	{
		CCPoint pos;

		pos.x = _radius * RATIO * 2.6f;
		pos.y = _radius * RATIO * 0.8f;

		return pos;
	}

protected:
	typedef puBlock* Belly[Config::MotherMaxKidCount];

	int				_kidsCount;
	int				_kidsStartCount;
	float			_kidsSize;
	CCLabelBMFont	*_counterLabel;
	Belly			_belly;
};
//---------------------------------------------------------------------------------------------------

typedef puMother<puPiggyBank_> puMotherBase;


//---------------------------------------------------------------------------------------------------
// MotherPiggy
//---------------------------------------------------------------------------------------------------
class puMotherPiggy_ : public puMother< puPiggyBank_>
{
public:
	puMotherPiggy_() : puMother< puPiggyBank_ >( 7.5f, 6.0f, Config::MotherMaxKidCount ) { Construct(); }
	puMotherPiggy_( float radius, float kidsSize, int kidsCount ) : puMother< puPiggyBank_>( radius, kidsSize, kidsCount ) { Construct(); }

	virtual ~puMotherPiggy_(){};

protected:
	virtual void Construct();
};
//---------------------------------------------------------------------------------------------------
template <int TRadius = 75, int TKidsSize = 60, int TKidsCount = Config::MotherMaxKidCount, BlockScaleType T = eBlockScaled >
class puMotherPiggy : public puMotherPiggy_
{
public:
	puMotherPiggy() : puMotherPiggy_( (float) TRadius / float( T ), (float) TKidsSize / float( T ), TKidsCount ) {}
};
//---------------------------------------------------------------------------------------------------



//---------------------------------------------------------------------------------------------------
// MotherPiggyStatic
//---------------------------------------------------------------------------------------------------
class puMotherPiggyStatic_ : public puMother< puPiggyBank_>
{
public:
	puMotherPiggyStatic_() : puMother< puPiggyBank_>( 7.5f, 6.0f, Config::MotherMaxKidCount ) { Construct(); }
	puMotherPiggyStatic_( float radius, float kidSize, int kidsCount ) : puMother< puPiggyBank_>( radius, kidSize, kidsCount ) { Construct(); }

	virtual ~puMotherPiggyStatic_(){};

protected:
	virtual void Construct();
};
//---------------------------------------------------------------------------------------------------
template <int TRadius = 75, int TKidsSize = 60, int TKidsCount = Config::MotherMaxKidCount, BlockScaleType T = eBlockScaled >
class puMotherPiggyStatic : public puMotherPiggyStatic_
{
public:
	puMotherPiggyStatic() : puMotherPiggyStatic_( (float) TRadius / float( T ), (float) TKidsSize / float( T ), TKidsCount ) {}
};
//---------------------------------------------------------------------------------------------------




//---------------------------------------------------------------------------------------------------
// CashCow
//---------------------------------------------------------------------------------------------------
class puCashCow_ : public puMother< puCoin_>
{
public:
	puCashCow_() : puMother< puCoin_>( 6.5f, 4.0f, Config::MotherMaxKidCount ) { Construct(); }
	puCashCow_( float radius, float kidSize, int kidsCount ) : puMother< puCoin_>( radius, kidSize, kidsCount ) { Construct(); }

	virtual ~puCashCow_(){};
	virtual void PlayNewBornEffect();

protected:
	virtual void Construct();
};
//---------------------------------------------------------------------------------------------------
template <int TRadius = 65, int TKidsSize = 40, int TKidsCount = Config::MotherMaxKidCount, BlockScaleType T = eBlockScaled >
class puCashCow : public puCashCow_
{
public:
	puCashCow() : puCashCow_( (float) TRadius / float( T ), (float) TKidsSize / float( T ), TKidsCount ) {}
};
//---------------------------------------------------------------------------------------------------




//---------------------------------------------------------------------------------------------------
// CashCowStatic
//---------------------------------------------------------------------------------------------------
class puCashCowStatic_ : public puMother< puCoin_>
{
public:
	puCashCowStatic_() : puMother< puCoin_>( 6.5f, 4.0f, Config::MotherMaxKidCount ) { Construct(); }
	puCashCowStatic_( float radius, float kidSize, int kidsCount ) : puMother< puCoin_>( radius, kidSize, kidsCount ) { Construct(); }

	virtual ~puCashCowStatic_(){};
	virtual void PlayNewBornEffect();

protected:
	virtual void Construct();
};
//---------------------------------------------------------------------------------------------------
template <int TRadius = 65, int TKidsSize = 40, int TKidsCount = Config::MotherMaxKidCount, BlockScaleType T = eBlockScaled >
class puCashCowStatic : public puCashCowStatic_
{
public:
	puCashCowStatic() : puCashCowStatic_( (float) TRadius / float( T ), (float) TKidsSize / float( T ), TKidsCount ) {}
};
//---------------------------------------------------------------------------------------------------



//---------------------------------------------------------------------------------------------------
// Prison
//---------------------------------------------------------------------------------------------------
class puPrison_ : public puMother< puThief_ >
{
public:
	puPrison_() : puMother< puThief_ >( 6.5f, 4.0f, Config::MotherMaxKidCount ) { Construct(); }
	puPrison_( float radius, float kidSize, int kidsCount ) : puMother< puThief_ >( radius, kidSize, kidsCount ) { Construct(); }

	virtual ~puPrison_(){};
	virtual void PlayNewBornEffect() { SoundEngine::Get()->PlayEffect( SoundEngine::eSoundPrisonAlert ); }

protected:
	virtual void Construct();
};
//---------------------------------------------------------------------------------------------------
template <int TRadius = 65, int TKidsSize = 40, int TKidsCount = Config::MotherMaxKidCount, BlockScaleType T = eBlockScaled >
class puPrison : public puPrison_
{
public:
	puPrison() : puPrison_( (float) TRadius / float( T ), (float) TKidsSize / float( T ), TKidsCount ) {}
};
//---------------------------------------------------------------------------------------------------


// Farms
class puFarm : public puMother< puPiggyBank_ >
{
public:
	puFarm() { Construct( 6.0f, 4.0f ); }
	puFarm( float w, float h ) { Construct( 6.0f, 4.0f ); }


protected :
	virtual void LoadDefaultSprite();

	void Construct( float w, float h );


protected:
	puPolygonWall	_roof;
};

//---------------------------------------------------------------------------------------------------
// PiggyFarmBuilding
//---------------------------------------------------------------------------------------------------
class puPiggyFarm_ : public puMother< puPiggyBank_>
{
public:
	puPiggyFarm_() : puMother< puPiggyBank_>() { Construct(); }
	puPiggyFarm_( float radius, float kidsSize,  int kidsCount ) : puMother< puPiggyBank_>( radius, kidsSize,  kidsCount ) { Construct(); }

	virtual ~puPiggyFarm_(){};

protected:
	virtual void Construct();

};
//---------------------------------------------------------------------------------------------------
template <int TRadius = 35, int TKidsSize = 17, int TKidsCount = Config::MotherMaxKidCount, BlockScaleType T = eBlockScaled >
class puPiggyFarm : public puPiggyFarm_
{
public:
	puPiggyFarm() : puPiggyFarm_( (float) TRadius / float( T ), (float) TKidsSize / float( T ), TKidsCount ) {}
};
//---------------------------------------------------------------------------------------------------

#endif
