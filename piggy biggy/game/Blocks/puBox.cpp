#include "puBox.h"
#include "puL.h"
#include <sstream>
#include "Preloader.h"
//------------------------------------------------------------------------------------------
// fix me
//memory leak
set<BlockEnum> GBoxNames()
{
	set<BlockEnum> names;

	names.insert( eBlockBoxWall );
	//names.insert( StoneBoxPolicy::GetClassName() );
	names.insert( eBlockBoxWood );
	names.insert( eBlockBoxFragile );
	names.insert( eBlockBoxFragileStatic );
	names.insert( eBlockBoxBounce );
	names.insert( eBlockBoxIron );
	names.insert( eBlockBoxLiftUp );
	//names.insert( OtherBoxPolicy::GetClassName() );
	//names.insert( DominoBoxPolicy::GetClassName() );
	//names.insert( GlassBoxPolicy::GetClassName() );
	//names.insert( "L" );
	//names.insert( "LScrewed" );
	//names.insert( "WoodBoxScrewed" );
	//names.insert( "Scale" );

	return names;
}
//------------------------------------------------------------------------------------------
FragileBoxPreloader::FragileBoxPreloader( int w, int h )
{
	if ( ! w || ! h )
		return;

	stringstream filename;

	filename << "FragileDestroy" << w;
	filename << "x" << h;

	Preloader::Get()->AddSound(  SoundEngine::eSoundFragileDestroyed1 );
	Preloader::Get()->AddSound(  SoundEngine::eSoundFragileDestroyed2 );
	Preloader::Get()->AddSound(  SoundEngine::eSoundFragileDestroyed3 );
	Preloader::Get()->AddSound(  SoundEngine::eSoundFragileDestroyed4 );
	Preloader::Get()->AddAnim( filename.str().c_str() );
}
//------------------------------------------------------------------------------------------
