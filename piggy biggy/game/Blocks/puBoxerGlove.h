#ifndef __REPLEACEME_H__
#define __REPLEACEME_H__

#include "puBlock.h"
#include "puBox.h"
#include "puCircle.h"

//---------------------------------------------------------------

class puBoxerHand : public puBlock
{
public:
	puBoxerHand();
	~puBoxerHand();

	virtual void Activate( void *ptr);
	void SetGlove( puBlock *glove );

private:
	puBlock *_glove;
};
//---------------------------------------------------------------

class puBoxerGlove : public puCircleWall< 50 >
{
public:
	puBoxerGlove();
	~puBoxerGlove();

	virtual void Activate( void *ptr);

	virtual void CreateJoints();
	virtual void DestroyJoints();
	void SetBoxerForce( float force );
	float GetBoxerForce();

private:
	puBoxerHand			_boxerHand;
	bool				_activated;
	b2PrismaticJoint	*_joint1;
	float				_force;
};

//---------------------------------------------------------------

#endif
