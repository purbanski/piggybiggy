#include "puFloor1.h"
#include "Levels/Level.h"

puFloor1::puFloor1()
{
	_spriteZOrder = GameTypes::eSpritesWall;
	LoadDefaultSprite();
}

puFloor2::puFloor2()
{
	_spriteZOrder = GameTypes::eSpritesWall;
	LoadDefaultSprite();

	SetPosition( Config::GameSize.width / RATIO / 2.0f, 1.0f );
};

puBouncingFloor1::puBouncingFloor1()
{
	_body->GetFixtureList()->SetDensity( 8.0f );
	_body->GetFixtureList()->SetFriction( 0.1f );
	_body->GetFixtureList()->SetRestitution( 0.98f );


	LoadDefaultSprite();
	if ( _sprite )
	{
		float scale;
		float dur;

		dur = 0.4f;
		scale = 0.008f;

		CCFiniteTimeAction *sequence;
		sequence = CCSequence::actions
			( 
			CCScaleTo::actionWithDuration( dur, 1.0f + scale, 1.0f + scale ), 
			CCScaleTo::actionWithDuration( dur, 1.0f - scale, 1.0f - scale ), 
			//CCWaves::actionWithDuration( 1.0f), 
			//CCWaves3D::actionWithDuration( 1.0f), 
			//CCTwirl::actionWithDuration( 1.0f),
			//CCFadeTo::actionWithDuration( 2.0f, 0 ),
			//CCFadeTo::actionWithDuration( 2.0f, 255 ),
			NULL );


		CCRepeatForever *forever;
		forever = CCRepeatForever::actionWithAction( dynamic_cast<CCActionInterval*>( sequence ));
		_sprite->runAction( forever );
	}
}
