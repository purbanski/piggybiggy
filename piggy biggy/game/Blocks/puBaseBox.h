#ifndef __PUBASEBOX_H__
#define __PUBASEBOX_H__

#include <Box2D/Box2D.h>
#include "puBlock.h"
#include "Debug/MyDebug.h"
#include "GameConfig.h"
#include "GameTypes.h"
#include "Skins.h"
#include "SoundEngine.h"
#include "CommonDefs.h"
#include "Tools.h"
#include "Components/CustomSprites.h"
#include "Animation/AnimerSimpleEffect.h"
#include "unFile.h"

//-----------------------------------------------------------------------------------------
class BoxPolicy
{
public:
	b2FixtureDef			_fixtureDef;
	b2BodyType				_bodyType;
	GameTypes::BlockType	_blockType;
	GameTypes::SpriteLayers	_spriteLayer;
	GameTypes::BlockEnum	_blockEnum;
};

//-----------------------------------------------------------------------------------------
template <class CreatePolicy>
class puBaseBox : public puBlock
{
public:
	virtual ~puBaseBox(){};
	void MySetSprite();
	void MySetSpriteStatic();

protected:
	puBaseBox() 
	{
		_blockEnum = GameTypes::eBlockNone;
		_blockType = GameTypes::eOther;
		_shapeType = GameTypes::eShape_Box;
	}

	void Construct();

	void DoDestroyFragile()
	{
		AnimerSimpleEffect::Get()->Anim_FragileDestroy( this );

		// fix me - check on the game::get if demo is running
		//if ( ! puBlock::_level->IsDemoRunning() )
		SoundEngine::Get()->PlayRandomEffect( 
			10.0f,
			SoundEngine::eSoundFragileDestroyed1, 
			SoundEngine::eSoundFragileDestroyed2, 
			SoundEngine::eSoundFragileDestroyed3, 
			SoundEngine::eSoundFragileDestroyed4,
			SoundEngine::eSoundNone );
				
		DestroyBody();
	}

};
//-----------------------------------------------------------------------------------------
template <class CreatePolicy>
void puBaseBox<CreatePolicy>::Construct()
{
	b2PolygonShape box;
	box.SetAsBox( _width / 2.0f, _height / 2.0f  );
	
	b2FixtureDef fixtureDef;
	BoxPolicy policy;

	policy = CreatePolicy::GetPolicy();
	fixtureDef = policy._fixtureDef;
	fixtureDef.shape = &box;

	_body->CreateFixture( &fixtureDef );
	_body->SetType( policy._bodyType );
	_blockEnum = policy._blockEnum;

	SetBlockType( policy._blockType );

	_spriteZOrder = policy._spriteLayer;
	MySetSprite();
}
//-----------------------------------------------------------------------------------------
template <class CreatePolicy>
void puBaseBox<CreatePolicy>::MySetSprite()
{
	char filename[128];
	snprintf( &filename[0], 128, "Images/Blocks/%s%dx%d.png", GetClassName2(), (int) ( _width * RATIO ), (int)( _height * RATIO ));

	string file;
	file = Skins::GetSkinName( filename );

	
	if ( unResourceFileCheckExists( file.c_str() ))
	{
		DS_LOG_MISSING_PNG( "+ %s%dx%d.png", GetClassName2(), (int) ( _width * RATIO ), (int)( _height * RATIO ))
		_sprite = CCSprite::spriteWithFile( file.c_str() );
	}
	else
	{
		DS_LOG_MISSING_PNG( "- %s%dx%d.png", GetClassName2(), (int) ( _width * RATIO ), (int)( _height * RATIO ))

#ifdef DEBUG_MEGA
		unAssertMsg( BaseBox, false, ("Missing: %s", file.c_str() ));
#endif

		snprintf( &filename[0], 128, "Images/Blocks/%s.png", GetClassName2() );
		file = Skins::GetSkinName( filename );
		_sprite = CCSprite::spriteWithFile( file.c_str() );
		
		float scalex = _width / _sprite->getContentSize().width * RATIO;
		float scaley = _height / _sprite->getContentSize().height * RATIO;
		_sprite->setScaleX( scalex );
		_sprite->setScaleY( scaley );
	}
}
//-----------------------------------------------------------------------------------------
template <class CreatePolicy>
void puBaseBox<CreatePolicy>::MySetSpriteStatic()
{
	MySetSprite();
	_glueSprite = GlueBoxSprite::Create( _width, _height );
}


//-----------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------
template <class CreatePolicy, int W, int H, BlockScaleType T = eBlockNormal>
class puBaseBoxTemplated : public puBaseBox<CreatePolicy>
{
public:
	puBaseBoxTemplated();
};


template <class CreatePolicy, int W, int H, BlockScaleType T>
puBaseBoxTemplated<CreatePolicy,W,H,T>::puBaseBoxTemplated()
{
	if ( T == eBlockNormal ) 
	{
		puBaseBox<CreatePolicy>::_width = W;
		puBaseBox<CreatePolicy>::_height = H;
	}
	else
	{
		// HACK:
		// FIXME:
		// + 0.0001 because 36 / 10 = 3.599999
		float w = Tools::SafeDivide( W, 10.0f );

		//D_FLOAT(w)
		puBaseBox<CreatePolicy>::_width = w;
		puBaseBox<CreatePolicy>::_height = Tools::SafeDivide( H, 10.0f );
	}
	puBaseBox<CreatePolicy>::Construct();
}

//-----------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------
template <class CreatePolicy>
class puBaseBox_ : public puBaseBox<CreatePolicy>
{
public:
	puBaseBox_( float w, float h );
};
//-----------------------------------------------------------------------------------------
template <class CreatePolicy>
puBaseBox_<CreatePolicy>::puBaseBox_( float w, float h )
{
	puBaseBox<CreatePolicy>::_width = w;
	puBaseBox<CreatePolicy>::_height = h;
	
	puBaseBox<CreatePolicy>::Construct();
}
//-----------------------------------------------------------------------------------------



#endif
