#include <Box2D/Box2D.h>

#include "Levels/World.h"
#include "Levels/Level.h"
#include "puTetris.h"

const float puTetrisBase::_sBlockSpace		= 0.025f;
const float puTetrisBase::_sBlockSize		= 4.8f;
const float puTetrisBase::_sBlockSizeTotal	= puTetrisBase::_sBlockSize + puTetrisBase::_sBlockSpace;
const float puTetrisBase::_sBlockHalfSpace	= puTetrisBase::_sBlockSpace / 2;
const float puTetrisBase::_sBlockHalfSize	= puTetrisBase::_sBlockSize / 2;

//----------------------------------------------------------------------------
puTetrisBase::puTetrisBase()
{
	_startX = 0;
	_startY = 0;
	_tetrisBlockRotation = eRot0;

	Preloader::Get()->AddSound( SoundEngine::eSoundFragileDestroyed1 );
	Preloader::Get()->AddSound( SoundEngine::eSoundFragileDestroyed2 );
	Preloader::Get()->AddSound( SoundEngine::eSoundFragileDestroyed3 );
	Preloader::Get()->AddSound( SoundEngine::eSoundFragileDestroyed4 );

	SetBlockType( GameTypes::eDestroyable );
}
//----------------------------------------------------------------------------
void puTetrisBase::SetDefaults()
{
	b2Fixture *fixture;

	fixture = _body->GetFixtureList();
	while ( fixture != NULL )
	{
		fixture->SetDensity( 1.2f );
		fixture->SetFriction( 0.6f );
		fixture->SetRestitution( 0.1f );

		fixture = fixture->GetNext();
	}

	_body->SetType( b2_dynamicBody );
	SetPosition( 0, 0 );
}
//----------------------------------------------------------------------------
void puTetrisBase::StartFalling()
{
	SetBodyType( b2_dynamicBody );
	SetPosition( _startX, _startY );
	SetPrisJoint();
}
//----------------------------------------------------------------------------
void puTetrisBase::SetPrisJoint()
{
	if ( ! _body )
		return;

	b2PrismaticJointDef pjd;

	pjd.Initialize( _body, _ground->GetBody(),  b2Vec2( GetPosition().x, 0 ), b2Vec2( 0.0f, -1.0f ));
	pjd.collideConnected = true;
	pjd.maxMotorForce = 1.0f;
	pjd.motorSpeed = 0.0f;
	pjd.enableMotor = true;

	_world->CreateJoint( &pjd );
}
//----------------------------------------------------------------------------
void puTetrisBase::CreateCube( float xpos, float ypos )
{
	b2PolygonShape box;
	b2FixtureDef fixtureDef;
	
	box.SetAsBox( _sBlockHalfSize, _sBlockHalfSize, b2Vec2( xpos + _sBlockSpace, ypos ), 0 );
	fixtureDef.shape = &box;
	fixtureDef.restitution = 0.0f;
	fixtureDef.friction = 0.9f;
	fixtureDef.density = 1.0f;

	_body->CreateFixture( &fixtureDef );
}
//----------------------------------------------------------------------------
// ignore angle an make a cycle 0,90, 180, 270,...
void puTetrisBase::SetRotation( TetrisBlockRotation rotation )
{
	CleanUpBlock();

	switch ( rotation )
	{
		case eRot0 :
			_tetrisBlockRotation = eRot0;
			Construct0();
			break;

		case eRot90 :
			_tetrisBlockRotation = eRot90;
			Construct90();
			break;

		case eRot180 :
			_tetrisBlockRotation = eRot180;
			Construct180();
			break;

		case eRot270 :
			_tetrisBlockRotation = eRot270;
			Construct270();
			break;
	}
}
//----------------------------------------------------------------------------
void puTetrisBase::CleanUpBlock()
{
	b2Fixture *fixture;
	b2Fixture *destoryMe;

	fixture = _body->GetFixtureList();
	
	while( fixture )
	{
		destoryMe = fixture;
		fixture = fixture->GetNext();
		_body->DestroyFixture( destoryMe );
	}

	if ( _sprite )
	{
		if ( _sprite->getParent() )
			_sprite->removeFromParentAndCleanup( true );
	}
}
//----------------------------------------------------------------------------
void puTetrisBase::DestroyFragile()
{
	AnimerSimpleEffect::Get()->Anim_TetrisDestroy( this );
	// fix me - check on the game::get if demo is running
	//if ( ! puBlock::_level->IsDemoRunning() )
	SoundEngine::Get()->PlayRandomEffect( 
		10.0f,
		SoundEngine::eSoundFragileDestroyed1, 
		SoundEngine::eSoundFragileDestroyed2,
		SoundEngine::eSoundFragileDestroyed3,
		SoundEngine::eSoundFragileDestroyed4,
		SoundEngine::eSoundNone );

	DestroyBody();
}
//----------------------------------------------------------------------------
void puTetrisBase::CleanUpShadow()
{
	if ( _spriteShadow && _spriteShadow->getParent() )
	{
		_spriteShadow->removeFromParentAndCleanup( true );
		_spriteShadow = NULL;
	}
}
//----------------------------------------------------------------------------
GameTypes::TetrisBlockRotation puTetrisBase::GetTetrisRotation()
{
	return _tetrisBlockRotation;
}
//----------------------------------------------------------------------------
void puTetrisBase::DestroyAnimPreload()
{
	string filename;

	filename.append( GetClassName2() );
	filename.append( "Destroy" );

	Preloader::Get()->AddAnim( filename.c_str() );
}



//----------------------------------------------------------------------------
puTetrisSquare::puTetrisSquare()
{
	_blockEnum = eBlockTetrisSquare;
	DestroyAnimPreload();
	Construct0();
	SetDefaults();
}
//----------------------------------------------------------------------------
void puTetrisSquare::Construct0()
{
	CreateCube( 0, 0 );
	CreateCube( _sBlockSize + _sBlockSpace, 0 );
	CreateCube( 0, _sBlockSize );
	CreateCube( _sBlockSizeTotal, _sBlockSize );

	_sprite = CCSprite::spriteWithFile(  Skins::GetSkinName( "Images/Blocks/TetrisSquare.png" )  );
	CleanUpShadow();
	UpdateSprite( false );
}

//----------------------------------------------------------------------------
puTetrisS::puTetrisS()
{
	_blockEnum = eBlockTetrisS;
	DestroyAnimPreload();
	Construct0();
	SetDefaults();
}
//----------------------------------------------------------------------------
void puTetrisS::Construct0()
{
	_tetrisBlockRotation = eRot0;

	CreateCube( 0, 0 );
	CreateCube( _sBlockSizeTotal, 0 );
	CreateCube( _sBlockSizeTotal, _sBlockSize );
	CreateCube( 2 * _sBlockSizeTotal, _sBlockSize );
	
	_sprite = CCSprite::spriteWithFile(  Skins::GetSkinName( "Images/Blocks/TetrisS_0.png" )  );
	CleanUpShadow();
	UpdateSprite( false );
}
//----------------------------------------------------------------------------
void puTetrisS::Construct90()
{
	_tetrisBlockRotation = eRot90;

	CreateCube( 0, 0 );
	CreateCube( _sBlockSizeTotal, 0 );
	CreateCube( _sBlockSizeTotal, -_sBlockSize );
	CreateCube( 0, _sBlockSize );

	_sprite = CCSprite::spriteWithFile(  Skins::GetSkinName( "Images/Blocks/TetrisS_90.png" )  );
	CleanUpShadow();
	UpdateSprite( false );
}

//----------------------------------------------------------------------------
puTetrisSM::puTetrisSM()
{
	_blockEnum = eBlockTetrisSM;
	DestroyAnimPreload();
	Construct0();
	SetDefaults();
}
//----------------------------------------------------------------------------
void puTetrisSM::Construct0()
{
	CreateCube( 0, 0 );
	CreateCube( _sBlockSizeTotal, 0 );
	CreateCube( 0, _sBlockSize );
	CreateCube( -_sBlockSizeTotal, _sBlockSize );

	_sprite = CCSprite::spriteWithFile(  Skins::GetSkinName( "Images/Blocks/TetrisSM_0.png" )  );
	CleanUpShadow();
	UpdateSprite( false );
}
//----------------------------------------------------------------------------
void puTetrisSM::Construct90()
{
	CreateCube( 0, 0 );
	CreateCube( _sBlockSizeTotal, 0 );
	CreateCube( _sBlockSizeTotal, _sBlockSize );
	CreateCube( 0, -_sBlockSize );

	_sprite = CCSprite::spriteWithFile(  Skins::GetSkinName( "Images/Blocks/TetrisSM_90.png" )  );
	CleanUpShadow();
	UpdateSprite( false );
}
//----------------------------------------------------------------------------
puTetrisT::puTetrisT()
{
	_blockEnum = eBlockTetrisT;
	DestroyAnimPreload();
	Construct0();
	SetDefaults();
}
//----------------------------------------------------------------------------
void puTetrisT::Construct0()
{
	CreateCube( - _sBlockSizeTotal, 0 );
	CreateCube( 0, 0 );
	CreateCube( _sBlockSizeTotal, 0 );
	CreateCube( 0, _sBlockSize );

	_sprite = CCSprite::spriteWithFile(  Skins::GetSkinName( "Images/Blocks/TetrisT_0.png" )  );
	CleanUpShadow();
	UpdateSprite( false );
}
//----------------------------------------------------------------------------
void puTetrisT::Construct90()
{
	CreateCube( 0, _sBlockSize );
	CreateCube( 0, 0 );
	CreateCube( 0, -_sBlockSize );
	CreateCube( _sBlockSizeTotal, 0 );

	_sprite = CCSprite::spriteWithFile(  Skins::GetSkinName( "Images/Blocks/TetrisT_90.png" )  );
	CleanUpShadow();
	UpdateSprite( false );
}
//----------------------------------------------------------------------------
void puTetrisT::Construct180()
{
	CreateCube( - _sBlockSizeTotal, 0 );
	CreateCube( 0, 0 );
	CreateCube( _sBlockSizeTotal, 0 );
	CreateCube( 0, -_sBlockSize );

	_sprite = CCSprite::spriteWithFile(  Skins::GetSkinName( "Images/Blocks/TetrisT_180.png" )  );
	CleanUpShadow();
	UpdateSprite( false );
}
//----------------------------------------------------------------------------
void puTetrisT::Construct270()
{
	CreateCube( 0, _sBlockSize );
	CreateCube( 0, 0 );
	CreateCube( 0, -_sBlockSize );
	CreateCube( -_sBlockSizeTotal, 0 );

	_sprite = CCSprite::spriteWithFile(  Skins::GetSkinName( "Images/Blocks/TetrisT_270.png" )  );
	CleanUpShadow();
	UpdateSprite( false );
}

//----------------------------------------------------------------------------
puTetrisL::puTetrisL()
{
	_blockEnum = eBlockTetrisL;
	DestroyAnimPreload();
	Construct0();
	SetDefaults();
}
//----------------------------------------------------------------------------
void puTetrisL::Construct0()
{
	CreateCube( -_sBlockSizeTotal, 0 );
	CreateCube( 0, 0 );
	CreateCube( _sBlockSizeTotal, 0 );
	CreateCube( _sBlockSizeTotal, _sBlockSize );

	_sprite = CCSprite::spriteWithFile(  Skins::GetSkinName( "Images/Blocks/TetrisL_0.png" )  );
	CleanUpShadow();
	UpdateSprite( false );
}
//----------------------------------------------------------------------------
void puTetrisL::Construct90()
{
	CreateCube( 0, -_sBlockSize );
	CreateCube( 0, 0 );
	CreateCube( 0, _sBlockSize );
	CreateCube( _sBlockSizeTotal, -_sBlockSize );

	_sprite = CCSprite::spriteWithFile(  Skins::GetSkinName( "Images/Blocks/TetrisL_90.png" )  );
	CleanUpShadow();
	UpdateSprite( false );
}
//----------------------------------------------------------------------------
void puTetrisL::Construct180()
{
	CreateCube( -_sBlockSizeTotal, 0 );
	CreateCube( 0, 0 );
	CreateCube( _sBlockSizeTotal, 0 );
	CreateCube( -_sBlockSizeTotal, -_sBlockSize );

	_sprite = CCSprite::spriteWithFile(  Skins::GetSkinName( "Images/Blocks/TetrisL_180.png" )  );
	CleanUpShadow();
	UpdateSprite( false );
}
//----------------------------------------------------------------------------
void puTetrisL::Construct270()
{
	CreateCube( 0, -_sBlockSize );
	CreateCube( 0, 0 );
	CreateCube( 0, _sBlockSize );
	CreateCube( -_sBlockSizeTotal, _sBlockSize );

	_sprite = CCSprite::spriteWithFile(  Skins::GetSkinName( "Images/Blocks/TetrisL_270.png" )  );
	CleanUpShadow();
	UpdateSprite( false );
}
//----------------------------------------------------------------------------
puTetrisLM::puTetrisLM()
{
	_blockEnum = eBlockTetrisLM;
	DestroyAnimPreload();
	Construct0();
	SetDefaults();
}
//----------------------------------------------------------------------------
void puTetrisLM::Construct0()
{
	CreateCube( -_sBlockSizeTotal, 0 );
	CreateCube( 0, 0 );
	CreateCube( _sBlockSizeTotal, 0 );
	CreateCube( -_sBlockSizeTotal, _sBlockSize );

	_sprite = CCSprite::spriteWithFile(  Skins::GetSkinName( "Images/Blocks/TetrisLM_0.png" )  );
	CleanUpShadow();
	UpdateSprite( false );
}
//----------------------------------------------------------------------------
void puTetrisLM::Construct90()
{
	CreateCube( 0, _sBlockSize );
	CreateCube( 0, 0 );
	CreateCube( 0, -_sBlockSize );
	CreateCube( _sBlockSizeTotal, _sBlockSize );

	_sprite = CCSprite::spriteWithFile(  Skins::GetSkinName( "Images/Blocks/TetrisLM_90.png" )  );
	CleanUpShadow();
	UpdateSprite( false );
}
//----------------------------------------------------------------------------
void puTetrisLM::Construct180()
{
	CreateCube( -_sBlockSizeTotal, 0 );
	CreateCube( 0, 0 );
	CreateCube( _sBlockSizeTotal, 0 );
	CreateCube( _sBlockSizeTotal, -_sBlockSize );

	_sprite = CCSprite::spriteWithFile(  Skins::GetSkinName( "Images/Blocks/TetrisLM_180.png" )  );
	CleanUpShadow();
	UpdateSprite( false );
}
//----------------------------------------------------------------------------
void puTetrisLM::Construct270()
{
	CreateCube( 0, _sBlockSize );
	CreateCube( 0, 0 );
	CreateCube( 0, -_sBlockSize );
	CreateCube( -_sBlockSizeTotal, -_sBlockSize );

	_sprite = CCSprite::spriteWithFile(  Skins::GetSkinName( "Images/Blocks/TetrisLM_270.png" )  );
	CleanUpShadow();
	UpdateSprite( false );
}

//----------------------------------------------------------------------------
puTetrisI::puTetrisI()
{
	_blockEnum = eBlockTetrisI;
	DestroyAnimPreload();
	Construct0();
	SetDefaults();
}
//----------------------------------------------------------------------------
void puTetrisI::Construct0()
{
	CreateCube( - _sBlockSizeTotal, 0 );
	CreateCube( 0, 0 );
	CreateCube( _sBlockSizeTotal, 0 );
	CreateCube( 2 * _sBlockSizeTotal, 0 );

	_sprite = CCSprite::spriteWithFile(  Skins::GetSkinName( "Images/Blocks/TetrisI_0.png" )  );
	CleanUpShadow();
	UpdateSprite( false );
}
//----------------------------------------------------------------------------
void puTetrisI::Construct90()
{
	CreateCube( 0, 2 *_sBlockSize );
	CreateCube( 0, _sBlockSize );
	CreateCube( 0, 0 );
	CreateCube( 0, -_sBlockSize );

	_sprite = CCSprite::spriteWithFile(  Skins::GetSkinName( "Images/Blocks/TetrisI_90.png" )  );
	CleanUpShadow();
	UpdateSprite( false );
}
//----------------------------------------------------------------------------
