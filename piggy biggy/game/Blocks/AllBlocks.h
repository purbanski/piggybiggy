#ifndef __BLOCKSREADY_H__
#define __BLOCKSREADY_H__

#include "puSwing.h"
#include "puBankSaveWheel.h"
#include "puBankLock.h"
#include "puBomb.h"
#include "puBorders.h"
#include "puBox.h"
#include "puBoxerGlove.h"
#include "puCircle.h"
#include "puChain.h"
#include "puDoor.h"
#include "puEdges.h"
#include "puFloor1.h"
#include "puGun.h"
#include "puL.h"
#include "puMagnet.h"
#include "puMother.h"
#include "puPolygon.h"
#include "puScale.h"
#include "puSpiderWebDot.h"
#include "puSlider.h"
#include "puTetris.h"
#include "puTFlap.h"
#include "puTrap.h"
#include "puTriangle.h"
#include "puEmptyBlock.h"
#include "puWoodBoxScrewed.h"

#include "CustomBlocks.h"
#include "Vehicals/puMiniMe.h"

#endif

