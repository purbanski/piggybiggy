#include "puBankSaveWheel.h"
#include "Game.h"
#include "Components/CustomSprites.h"
#include "Preloader.h"
#include "SoundEngine.h"

//----------------------------------------------------------------------------------
puBankSaveWheel::puBankSaveWheel()
{
	_lastRotation	= 0.0f;
	_rotationDelta	= 0.0f;
	_blockEnum		= eBlockBankSaveWheel;

	char filename[64];
	snprintf( &filename[0], 64, Skins::GetSkinName( "Images/Blocks/%s.png" ) , GetClassName2() );

	_sprite = BankSaveWheelSprite::Create( filename );
	_spriteZOrder = GameTypes::eSprites;

	// SetDeltaXY
	_screw1.SetDeltaXY( 0.0f, 0.0f );
	_screw1.SetSprite( NULL );

	// SetNodeChain
	SetNodeChain( &_screw1, NULL );

	// Main block
	b2CircleShape shape;
	shape.m_radius = 15.0f;

	b2FixtureDef fixtureDef;
	fixtureDef.shape = &shape;
	fixtureDef.density = 6.0f;
	fixtureDef.restitution = 0.2f;

	_body->CreateFixture( &fixtureDef );
	SetBlockType( GameTypes::eMoveable );
	SetBodyType( b2_dynamicBody );

	SetPosition( 0.0f, 0.0f );
	CreateJoints();

	Preloader::Get()->AddSound( SoundEngine::eSoundBankSettingWheelPryk );
}
//----------------------------------------------------------------------------------
void puBankSaveWheel::CreateJoints()
{
	b2RevoluteJointDef jointDef1;
	jointDef1.Initialize( _screw1.GetBody(), GetBody(), GetPosition() );
	_joint1 = (b2RevoluteJoint *) _world->CreateJoint( &jointDef1 );

	_joint1->EnableMotor( true );
	_joint1->SetMotorSpeed( 0.0f );
	_joint1->SetMaxMotorTorque( 70000.0f );
}
//----------------------------------------------------------------------------------
void puBankSaveWheel::DestroyJoints()
{
	_world->DestroyJoint( _joint1 );
	_joint1 = NULL;
}
//----------------------------------------------------------------------------------
void puBankSaveWheel::Step()
{
	float rotmax = 20.0f;
	float speedFactor;

	_rotationDelta += ( GetRotation() - _lastRotation );

	speedFactor = sqrt( abs( _body->GetAngularVelocity()));
	if ( speedFactor < 1 )
		speedFactor = 1;

	if ( abs( _rotationDelta ) >  rotmax * speedFactor ) 
	{
		SoundEngine::Get()->PlayEffect( SoundEngine::eSoundBankSettingWheelPryk );
		_rotationDelta = 0.0f;
	}
	_lastRotation = GetRotation();
}
//----------------------------------------------------------------------------------
