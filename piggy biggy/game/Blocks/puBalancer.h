#ifndef __PUBALANCER_H__
#define __PUBALANCER_H__

#include <Box2D/Box2D.h>
#include "puBlock.h"
#include "puCircle.h"

class puBalancer : public puBlock
{
public:
	static const float sPinRadius;

public:
	puBalancer();
	virtual ~puBalancer();

	void PutOn( float deltaX, float deltaY, puBlock *b1, puBlock *b2 );

private:
	void Construct();

private:
	puCircle_	_pin;

};


#endif
