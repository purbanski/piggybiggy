#include "puBalancingScale.h"
#include <Box2D/Box2D.h>
#include "Levels/World.h"

puBalancingScale::puBalancingScale()
{
	Construct();
}
//-----------------------------------------------------------------------------------------------------
void puBalancingScale::Construct()
{
	float x, y;
	x=110;
	y=10;

	float halfStandHeight = 4.5f;

	b2PolygonShape box;
	box.SetAsBox( halfStandHeight, 0.5 );

	b2FixtureDef fixtureDef;
	fixtureDef.shape = &box;
	fixtureDef.density = 5;
	fixtureDef.friction = 1; // tarcie 0 - nie ma 1- max
	fixtureDef.restitution = 0; // bouncy 0 - nie ma 1 max;
	
	_body->CreateFixture( &fixtureDef );

	SetRotation( 90 );
	SetPosition( x, y );

	_platform.SetPosition( x, y + 4.5f );
	
	_leftArm.SetPosition( x - 1.7f, y + 2 );
	_leftArm.SetRotation( -55 );
	//{
	//	b2RevoluteJointDef jointDef;
	//	jointDef.collideConnected = false;
	//	jointDef.Initialize( _leftArm.GetBody(), _platform.GetBody(), GetPosition() + b2Vec2(  -2 * _radius, 0 )   );
	//	jointDef.maxMotorTorque = 1000.0f;
	//	jointDef.motorSpeed = .6 * b2_pi;
	//	jointDef.enableMotor = true;
	//	b2RevoluteJoint * joint = (b2RevoluteJoint *) Game::Get()->GetRuningLevel()->GetWorld()->CreateJoint(&jointDef);
	//}

	_rightArm.SetPosition( x + 1.7f, y + 2 );
	_rightArm.SetRotation( 55 );

	/*_tyre1.SetPosition( -2 * _radius, 0 );
	_tyre1.SetBodyType( b2_dynamicBody );
	_tyre1.GetBody()->GetFixtureList()->SetRestitution( 0 );
	_tyre1.GetBody()->GetFixtureList()->SetFriction( 0.7 );
	_tyre1.GetBody()->GetFixtureList()->SetDensity( 35 );

	_tyre2.SetPosition( 2 * _radius, 0 );
	_tyre2.SetBodyType( b2_dynamicBody );
	_tyre2.GetBody()->GetFixtureList()->SetRestitution( 0 );
	_tyre2.GetBody()->GetFixtureList()->SetFriction( 0.7 );
	_tyre2.GetBody()->GetFixtureList()->SetDensity( 35 );
	
	{
		b2RevoluteJointDef jointDef;
		jointDef.collideConnected = false;
		jointDef.Initialize( _tyre1.GetBody(), _body, GetPosition() + b2Vec2(  -2 * _radius, 0 )   );
		jointDef.maxMotorTorque = 1000.0f;
		jointDef.motorSpeed = .6 * b2_pi;
		jointDef.enableMotor = true;
		b2RevoluteJoint * joint = (b2RevoluteJoint *) Game::Get()->GetRuningLevel()->GetWorld()->CreateJoint(&jointDef);
	}

	{
		b2RevoluteJointDef jointDef;
		jointDef.collideConnected = false;
		jointDef.Initialize( _tyre2.GetBody(), _body, GetPosition() + b2Vec2(  2 * _radius, 0 )   );
		jointDef.maxMotorTorque = 1000.0f;
		jointDef.motorSpeed = .6 * b2_pi;
		jointDef.enableMotor = true;
		b2RevoluteJoint * joint = (b2RevoluteJoint *) Game::Get()->GetRuningLevel()->GetWorld()->CreateJoint(&jointDef);
	}
	
	*///_tyre2.SetPosition
	 
}
//-----------------------------------------------------------------------------------------------------
puBalancingScale::~puBalancingScale()
{
}
//-----------------------------------------------------------------------------------------------------
void puBalancingScale::SetPosition( float x, float y )
{
	puBlock::SetPosition( x, y );
	//_tyre1.SetPosition( x - 2 * _radius, y );
	//_//tyre2.SetPosition( x + 2 * _radius, y );

}

