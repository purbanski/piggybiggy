#include "puSlider.h"
#include <Box2D/Box2D.h>
#include "Levels/World.h"
#include "GameConfig.h"
#include "Levels/Level.h"

puSlider::puSlider( float xpos, float ypos)
	: puBlock()
{
	Construct( xpos, ypos );
}
//-----------------------------------------------------------------------------------------------------
void puSlider::Construct( float x, float y )
{
	_sprite = CCSprite::spriteWithFile( Skins::GetSkinName( "Images/Blocks/Slider48Platform.png" ) );

	b2PolygonShape box;
	box.SetAsBox( 2.4f, 2.4f );

	b2FixtureDef fixtureDef;
	fixtureDef.shape = &box;
	fixtureDef.density = 0.0f;
	fixtureDef.friction = 0.5f; // tarcie 0 - nie ma 1- max
	fixtureDef.restitution = 0.1f; // bouncy 0 - nie ma 1 max;
	
	_body->CreateFixture( &fixtureDef );
	_body->SetType( b2_staticBody );
	SetPosition( x, y );
	
	
	// Arm - connected to ground
	_armGround.SetSprite( CCSprite::spriteWithFile( Skins::GetSkinName( "Images/Blocks/Slider48ArmGround.png" )  ), GameTypes::eSpritesAbove );
	_armGround.SetPosition( x, y + 3.5f );
	_armGround.SetBodyType( b2_dynamicBody );
			
	// Arm - ground joint
	{
		b2RevoluteJointDef rjd;
		rjd.Initialize( _body, _armGround.GetBody(), b2Vec2( x + 0.0f, y + 1.5f ));
		rjd.motorSpeed = 1.0f * b2_pi;
		rjd.maxMotorTorque = 10000.0f;
		rjd.enableMotor = true;
		(void) _world->CreateJoint(&rjd);
	}
	
	
	// Arm connected to pusher
	b2Filter filter;
	filter.groupIndex = -1;
	_armPusher.GetBody()->GetFixtureList()->SetFilterData( filter );
	_armPusher.SetSprite( CCSprite::spriteWithFile( Skins::GetSkinName( "Images/Blocks/Slider48ArmPusher.png" )  ), GameTypes::eSpritesAbove );
	_armPusher.SetPosition( x, y + 9.5f );
	_armPusher.SetBodyType( b2_dynamicBody );
	// Arm - arm joint
	{
		b2RevoluteJointDef rjd;
		rjd.Initialize( _armGround.GetBody(), _armPusher.GetBody(), b2Vec2( x + 0.0f, y + 5.5f ));
		rjd.enableMotor = false;
		_world->CreateJoint(&rjd);
	}

	//// Pusher
	_pusher.SetPosition( x, y + 13.5f );
	_pusher.SetSprite( CCSprite::spriteWithFile( Skins::GetSkinName( "Images/Blocks/Slider48Pusher.png" )  ), GameTypes::eSpritesAboveX3);
	_pusher.SetBodyType( b2_dynamicBody );
	// Pusher - ground joint
	{
		b2RevoluteJointDef rjd;
		rjd.Initialize( _armPusher.GetBody(), _pusher.GetBody(), b2Vec2(x+0.0f, y+13.5f));
		_world->CreateJoint(&rjd);

		b2PrismaticJointDef pjd;
		pjd.Initialize( _body, _pusher.GetBody(), b2Vec2( x + 0.0f, y + 13.5f ), b2Vec2( 0.0f, 1 ));

		pjd.maxMotorForce = 1000.0f;
		pjd.enableMotor = true;

		(void) _world->CreateJoint(&pjd);
	}

	////
}
//-----------------------------------------------------------------------------------------------------
puSlider::~puSlider()
{
}
