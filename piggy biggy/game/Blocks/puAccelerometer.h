#ifndef __PUACCELEROMETER_H__
#define __PUACCELEROMETER_H__

#include "puCircle.h"
#include "puChain.h"
#include "puEmptyBlock.h"

//--------------------------------------------------------------------------
class puAccelBall : public puCircleWood<23>
{
public:
    puAccelBall();
};
//--------------------------------------------------------------------------
class puAccelWall : public puWallBox<10,15, eBlockScaled>
{
public:
    puAccelWall();
};
//--------------------------------------------------------------------------
class puAccelBg : public puCircleWall<20>
{
public:
    puAccelBg();
};
//--------------------------------------------------------------------------
class puAccelIcon : public puAccelWall
{
public:
	puAccelIcon();
	~puAccelIcon();

private:
    puAccelBall	_accelBall;
	puAccelWall	_wallBox10;
	puAccelWall	_wallBox11;
	puAccelWall	_wallBox12;
	puAccelWall	_wallBox13;
	puAccelWall	_wallBox14;
	puAccelWall	_wallBox15;
	puAccelWall	_wallBox16;
	puAccelWall	_wallBox17;
	puAccelWall	_wallBox18;
	puAccelWall	_wallBox19;
	puAccelWall	_wallBox1;
	puAccelWall	_wallBox2;
	puAccelWall	_wallBox3;
	puAccelWall	_wallBox4;
	puAccelWall	_wallBox5;
	puAccelWall	_wallBox6;
	puAccelWall	_wallBox7;
	puAccelWall	_wallBox8;
	puAccelWall	_wallBox9;
    puAccelBg   _bgCircle;

};


#endif
