#ifndef __PULIFTDOOR_H__
#define __PULIFTDOOR_H__

#include <Box2D/Box2D.h>
#include "puBlock.h"
#include "puBox.h"

class puLiftDoor : public puBlock
{
public:
	puLiftDoor( float xpos, float ypos );
	virtual ~puLiftDoor();

	virtual void Activate( void *ptr = NULL );
	virtual void Deactivate( void *ptr = NULL );

private:
	void Construct( float xpos, float ypos );

private:	
	puWallBox<1,4>	_armGround;
	puWallBox<1,8>	_armPusher;
	puWallBox<2,6>	_pusher;

	b2RevoluteJoint		*_motorJoint;
};

#endif
