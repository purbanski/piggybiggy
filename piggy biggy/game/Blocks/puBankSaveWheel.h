#ifndef __PUBANKSAVEWHEEL_H__
#define __PUBANKSAVEWHEEL_H__

#include "puBlock.h"
#include "puCircle.h"

//--------------------------------------------------------------------------------
class puBankSaveWheel : public puBlock
{
public:
	puBankSaveWheel();

	virtual void CreateJoints();
	virtual void DestroyJoints();
	virtual void Step();

private:
	b2RevoluteJoint		*_joint1;
	puScrew<>			_screw1;

	float _lastRotation;
	float _rotationDelta;
};
//--------------------------------------------------------------------------------
#endif
