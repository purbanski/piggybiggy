#ifndef __PUBLOCK_H__
#define __PUBLOCK_H__

#include "cocos2d.h"
#include <Box2D/Box2D.h>
#include "GameGlobals.h"
#include "SoundEngine.h"
// puBlockDef is just to satisfy compiler for no editor build
//#include "Editor/puBlockDef.h"
#include "GameTypes.h"

//-------------------------------------------------------------------------------------
USING_NS_CC;
using namespace GameTypes;
//-------------------------------------------------------------------------------------
typedef enum
{
	eBlockNormal = 1,
	eBlockScaled = 10
} BlockScaleType;


class Level;
//-----------------------------------------------------------------------------------------------------------
// puBlock
//-----------------------------------------------------------------------------------------------------------
class puBlock
{
public:
	typedef enum
	{
		eAxisFlipped = -1,
		eAxisNormal = 1
	} AxisFlip;

	puBlock();
	virtual ~puBlock();

public:
	virtual void Activate( void *ptr = NULL ); //this is only for eActivable really - should be in other class
	virtual void Deactivate( void *ptr = NULL ); //this is only for eActivable really - should be in other class
	virtual void Destroy();
	virtual void Cut(); //this is only for eDestroyabld really - should be in other class
	virtual void Step(); // for the moment only for magnet
	virtual void LevelQuiting(); // block with range
  	virtual void LevelExiting(); // chain
	virtual void LevelFinished(); // for the moment only for chain
	virtual void DestroyFragile();

	virtual float GetRotation();
	virtual void SetRotation( float angle );
	virtual void SetRotationDirect( float angle );
	virtual void SetPosition( const b2Vec2& position );
	virtual void SetPosition( float x, float y );
	virtual void SetPositionDirect( float x, float y );
	virtual void SetPositionDirect( b2Vec2 pos );
	
	virtual void FlipX();
	virtual void FlipY();
	
	virtual const char* GetClassName2();
	BlockEnum GetBlockEnum() { return _blockEnum; }
	virtual void CenterOnScreen();

	GameTypes::ShapeType GetShapeType(){ return _shapeType; }
	void RemoveSprite( bool cleanUp = true, bool recrusive = true );
	void UpdateSprite( bool recrusive = true);
	void SetEmptySprite();
	
// getters setters
	b2Vec2				GetPosition();
	b2Vec2				GetReversedPosition();
	b2Body*				GetBody()				{ return _body; }
	GameTypes::BlockType	GetBlockType()		{ return _blockType; }
	CCSprite*			GetSprite()				{ return _sprite; }
	CCSprite**			GetSpritePtr()			{ return &_sprite; }
	CCSprite*			GetSpriteShadow()		{ return _spriteShadow; }
	CCSprite**			GetSpriteShadowPtr()	{ return &_spriteShadow; }
	int					GetSpriteZOrder()		{ return _spriteZOrder; }
	CCSprite*			GetGlueSprite()			{ return _glueSprite; }
	float				GetDeltaX()				{ return _deltaX; }
	float				GetDeltaY()				{ return _deltaY; }
	float				GetDeltaRotation();
	AxisFlip			GetFlipX()				{ return _flipX; }
	AxisFlip			GetFlipY()				{ return _flipY; }
	int					GetUID()				{ return _uid; }
	float				GetWidth()				{ return _width; }
	float				GetHeight()				{ return _height; }
	float				GetSpriteDeltaRotation(){ return _spriteDeltaRotation; }
	float				GetScaleToDefault()		{ return _scaleToDefault; }
	Level*				GetLevel()				{ return _level; }

	void		SetBodyType (b2BodyType type );
	void		SetBlockType( GameTypes::BlockType type ) { _blockType = type; }
	void		SetSprite( CCSprite* sprite );
	void		SetSprite( CCSprite* sprite, int zOrder );
	void		SetSpriteZOrder( int zOrder ) { _spriteZOrder = zOrder; }
	void		ResetSpriteZOrder( int zOrder );
	void		SetDeltaX( float dx );
	void		SetDeltaY( float dy );
	void		SetDeltaXY( float dx, float dy, bool recrusive = false );
	void		SetDeltaRotation( float da ) { _deltaRotation = da;  SetRotation( da ); }
	void		SetUID( int uid ) { _uid = uid; }
	void		SetWidth( float width )	{ _width = width; }
	void		SetHeight( float height ) { _height = height; }
	void		SetSpriteDeltaRotation( float rot ) { _spriteDeltaRotation = rot; }
	void		DestroyBody();  //this is only for eDestroyabld really - should be in other class

	virtual void CreateJoints() {};
	virtual void DestroyJoints() {};

	//------------
	// Node stuff
	//------------
	void		SetNodeChain( puBlock* item, ... );
	void		SetNext( puBlock *next );
	void		SetPrev( puBlock* prev )  { _prev = prev; }
	puBlock*	GetNext() { return _next; }
	puBlock*	GetPrev() { return _prev; }
	
	puBlock*	GetRoot();
	bool		AmIRoot();

	
	//------------
	// Bouyancy stuff
	//------------
	void SetBouyancyRate( float bou )	{	_bouancyRate = bou; }
	float GetBouyancyRate()				{	return _bouancyRate;  }
	float GetBouyancy();
	void SetArea( float area )			{ _area = area; }
	float GetArea()						{ return _area; }

	//------------
	// Speed Break stuff
	//------------
	void SetSpeedBreak( float speed ) { _speedBreak = speed; }
	float GetSpeedBreak() { return _speedBreak; }
	bool ShouldSpeedBreak();
	CCSprite* GetSkinSprite( const char *filename );

protected:
	CCSprite* LoadSkinedSprite( const char *filenam );
	bool LoadDefaultSprite();
	bool LoadDefaultSpriteStatic();
	float CalculateArea();
	void UpdateChildrenTransf( float angle, b2Vec2 pos );
	void StartDayAndNight();

private:
	void AddShadow();

protected:
	b2Body		*_body;
	BlockEnum	_blockEnum;
	BlockType	_blockType;
	CCSprite	*_sprite;
	CCSprite	*_spriteShadow;
	CCSprite	*_glueSprite;

	int			_spriteZOrder;

	b2World		*_world;
	Level		*_level;

	AxisFlip	_flipX;
	AxisFlip	_flipY;
	float		_deltaX;
	float		_deltaY;

	float		_width;
	float		_height;

	float		_deltaRotation;			// for next blocks/nodes
	float		_spriteDeltaRotation;	// for sprite rotation
	int			_uid;

	float		_speedBreak;

	puBlock		*_next;
	puBlock		*_prev;

	GameTypes::ShapeType	_shapeType;

	float _bouancyRate;
	float _area;

	typedef set<string> Files;
	
	Files					_preloadSprites;
	Files					_preloadFonts;
	SoundEngine::SoundSet	_preloadSounds;
	float					_scaleToDefault;

#ifdef BUILD_EDITOR
public:
	puBlockDef GetBlockDef();
	void SetBlockDef( const puBlockDef& blockDef );
#endif
};

#endif
