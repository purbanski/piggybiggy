#include <math.h>

#include "AllBlocks.h"
#include "Game.h"
#include "GameConfig.h"
#include "Levels/Level.h"
#include "Levels/World.h"
#include "Debug/MyDebug.h"
#include "Tools.h"
#include "SoundEngine.h"
#include "Components/CustomSprites.h"
#include "BlockNames.h"

#ifdef BUILD_EDITOR
#include "Editor/Creators/BlockCreator.h"
#endif

USING_NS_CC;


//-----------------------------------------------------------------------------------------------------
puBlock::puBlock()
{
	// we have to keep track of this guy
	// as global is not enough
	// new level will change global world
	// before destruction of this object takes place
	// therefore we need to keep it to be able to destruct body 
	_world = World::Get()->GetWorld();
	_level = (Level *) World::Get();
	_blockEnum = eBlockNone;

	_bouancyRate = 0.0f;
	b2BodyDef bodyDef;
	bodyDef.type = b2_dynamicBody;
	
	bodyDef.position.Set( 0.0f, 0.0f );
	
	_body = _world->CreateBody( &bodyDef );
	_body->SetUserData( this );
	//_blockType = GameTypes::eOther;

	_flipX = puBlock::eAxisNormal;
	_flipY = puBlock::eAxisNormal;

	_deltaX = 0;
	_deltaY = 0;

	_deltaRotation = 0;

	_spriteZOrder = GameTypes::eSprites;
	_sprite = NULL;
	_spriteShadow = NULL;
	_spriteDeltaRotation = 0.0f;
	
	_scaleToDefault = 1.0f;
	
	_glueSprite = NULL;

	_uid = -1;
	_speedBreak = -1.0f;

	_width = 0;
	_height = 0;

	_shapeType = eShape_Unknown;
	_blockType = eOther;

	_area = 0.0f;

	_next = NULL;
	_prev = NULL;

}
//-----------------------------------------------------------------------------------------------------
puBlock::~puBlock()
{
	if ( _body )
		_world->DestroyBody( _body );


	_world = NULL;
	_body = NULL;
	_sprite = NULL;
}
//-----------------------------------------------------------------------------------------------------
void puBlock::SetRotation( float angleDegree )
{
	float angleRad;
	angleRad = angleDegree / 180.0f * b2_pi;

	_body->SetTransform( _body->GetPosition(), angleRad );

	if ( GetNext() != NULL )
		GetNext()->UpdateChildrenTransf( angleDegree -_deltaRotation, GetPosition() );

	UpdateSprite( true );
}
//-----------------------------------------------------------------------------------------------------
void puBlock::SetRotationDirect( float angleDegree )
{
	float angleRad;
	angleRad = angleDegree / 180.0f * b2_pi;

	_body->SetTransform( _body->GetPosition(), angleRad );

	UpdateSprite( false );
}
//-----------------------------------------------------------------------------------------------------
float puBlock::GetRotation()
{
	float rad = _body->GetAngle();
	float degree;

	degree = rad * 180.0f / b2_pi; 
	return degree;
}
//-----------------------------------------------------------------------------------------------------
b2Vec2 puBlock::GetPosition()
{
	return _body->GetPosition();
}
//-----------------------------------------------------------------------------------------------------
b2Vec2 puBlock::GetReversedPosition()
{
	if ( ! _body )
		return b2Vec2( -1.0f, -1.0f );

	b2Vec2 pos;
	b2Vec2 reversedPos;
	CCSize size;

	size = CCDirector::sharedDirector()->getWinSize();
	pos = _body->GetPosition();

	reversedPos.x = pos.x - size.width  / 2.0f / RATIO - ( _flipX * _deltaX ) + Config::GameSize.width / 2.0f / RATIO;
	reversedPos.y = pos.y - size.height / 2.0f / RATIO - ( _flipY * _deltaY ) + Config::GameSize.height / 2.0f / RATIO;

	return reversedPos;
}
//-----------------------------------------------------------------------------------------------------
void puBlock::SetPosition( const b2Vec2& position )
{
	SetPosition( position.x, position.y );
}
//-----------------------------------------------------------------------------------------------------
void puBlock::SetPosition( float x, float y )
{
	// possible bug if i dont start at root block
	// than first transformation will be done as for root but for child
	CCSize size;
	size = CCDirector::sharedDirector()->getWinSize();

	//iphone
	float nx = size.width / 2.0f / RATIO + x + ( _flipX * _deltaX ) - Config::GameSize.width / 2.0f / RATIO;
	float ny = size.height / 2.0f / RATIO + y + ( _flipY * _deltaY ) - Config::GameSize.height / 2.0f / RATIO;

	if ( _body )
		_body->SetTransform( b2Vec2( nx, ny ), _body->GetAngle() );
	
	if ( GetNext() != NULL )
		GetNext()->UpdateChildrenTransf( GetRotation() -_deltaRotation, b2Vec2( nx, ny ));

	UpdateSprite( true );
}
//-----------------------------------------------------------------------------------------------------
void puBlock::SetPositionDirect( float x, float y )
{
	CCSize size;
	size = CCDirector::sharedDirector()->getWinSize();

	float nx = size.width / 2.0f / RATIO + x + ( _flipX * _deltaX ) - Config::GameSize.width / 2.0f / RATIO;
	float ny = size.height / 2.0f / RATIO + y + ( _flipY * _deltaY ) - Config::GameSize.height / 2.0f / RATIO;

	if ( _body )
		_body->SetTransform( b2Vec2( nx, ny ), _body->GetAngle() );

}
//-----------------------------------------------------------------------------------------------------
void puBlock::SetPositionDirect( b2Vec2 pos )
{
	SetPositionDirect( pos.x, pos.y );
}

//-----------------------------------------------------------------------------------------------------
void puBlock::UpdateChildrenTransf( float angleDegree, b2Vec2 mainBlockPos )
{
	b2Vec2 newPos;
	
	newPos = Tools::RotatePointAroundPoint( b2Vec2( GetDeltaX(), GetDeltaY() ), b2Vec2( 0, 0 ), angleDegree );
	newPos.Set( newPos.x * _flipX , newPos.y * _flipY );
	newPos += mainBlockPos;

	float newAngleRad;
	newAngleRad = ( angleDegree + GetDeltaRotation()) / 180.0f * b2_pi;

	_body->SetTransform( newPos, newAngleRad );

	DestroyJoints();
	CreateJoints();

	if ( GetNext() != NULL )
		GetNext()->UpdateChildrenTransf( angleDegree, mainBlockPos );
}
//-----------------------------------------------------------------------------------------------------
void puBlock::UpdateSprite( bool recrusive )
{
	//---------------------------------
	// For example when body destroyed
	//---------------------------------
	if ( ! _body )
		return;

	if ( GetNext() != NULL && recrusive )
		GetNext()->UpdateSprite();

	if ( _sprite == NULL )
		return;
		
	// Check if sprite is oprhan
	bool isSpriteOrphan;
	isSpriteOrphan = _sprite->getParent() == NULL ? true : false;

	if ( isSpriteOrphan )
	{
		_level->addChild( _sprite, _spriteZOrder );
		//_sprite->puExt_SpriteAdded( _level );

		AddShadow();
		//D_LOG("Orphan sprite added");
	}

	//TODO why to call all the time?
	CCSize size;
	size = CCDirector::sharedDirector()->getWinSize();

	_sprite->setPosition( CCPoint(  _body->GetPosition().x * RATIO - size.width / 2.0f ,  _body->GetPosition().y * RATIO  - size.height / 2.0f )); 
	_sprite->setRotation( - ( _body->GetAngle() )  * 180.0f / b2_pi - _spriteDeltaRotation );
	
	if ( _spriteShadow && _spriteShadow->getParent() )
	{
		_spriteShadow->setPosition( 
			CCPoint(  
				Config::ShadowPosition.x + _body->GetPosition().x * RATIO - size.width / 2.0f ,  
				Config::ShadowPosition.y +  _body->GetPosition().y * RATIO  - size.height / 2.0f ));

		_spriteShadow->setRotation( _sprite->getRotation() );
	}

	if ( _glueSprite )
	{
		if ( ! _glueSprite->getParent() )
			_level->addChild( _glueSprite, GameTypes::eSpritesGlue );

		_glueSprite->setPosition( _sprite->getPosition() );
	}
}
//-----------------------------------------------------------------------------------------------------
void puBlock::AddShadow()
{
	if ( GameGlobals::Get()->NoShadowBlockNames().count( _blockEnum ) > 0 )
		return;
	
	// needed for bank save single lock shadow
	// otherwise it blinks when cross fit-unfit border
	if ( ! _spriteShadow ) 
	{
		_spriteShadow = CCSprite::spriteWithTexture( _sprite->getTexture() );
		_spriteShadow->setScaleX( _sprite->getScaleX() );
		_spriteShadow->setScaleY( _sprite->getScaleY() );
	}
	
	//if ( _spriteShadow && _spriteShadow->getParent() )
	//	_spriteShadow->removeFromParentAndCleanup( true );
	if ( ! _spriteShadow->getParent() )
	{
		_spriteShadow->setOpacity( Config::ShadowOpacity );
		_level->addChild( _spriteShadow, GameTypes::eSpritesShadows );
		
		ccColor3B shadow;
		shadow.r = 0;
		shadow.g = 0;
		shadow.b = 0;

		_spriteShadow->setColor( shadow );
	
		if ( _flipX == eAxisFlipped ) _spriteShadow->setFlipX( true );
		else _spriteShadow->setFlipX( false );

		if ( _flipY == eAxisFlipped ) _spriteShadow->setFlipY( true );
		else _spriteShadow->setFlipY( false );
	}
}
//-----------------------------------------------------------------------------------------------------
void puBlock::Activate( void *ptr )
{
	if ( GetNext() != NULL )
		GetNext()->Activate();
}
//-----------------------------------------------------------------------------------------------------
void puBlock::Deactivate( void *ptr )
{
	if ( GetNext() != NULL )
		GetNext()->Deactivate();
}
//------------------------------------------------------------------------------------------------------
void puBlock::Destroy()
{
	DestroyBody();

	if ( ! _sprite ) 
		return;

	AnimerSimpleEffect::Get()->Anim_FragileDestroy_Depricated( _sprite );
	AnimerSimpleEffect::Get()->Anim_FragileDestroy_Depricated( _spriteShadow );

	//if ( ! _level->IsDemoRunning() )
//		SoundEngine::Get()->PlayEffect( SoundEngine::eSoundFragileDestroyed );
}
//------------------------------------------------------------------------------------------------------
void puBlock::DestroyFragile()
{
	//_level->GetParticleEngine()->Effect_FragileDestroyed( _level, GetPosition() );

    Game::Get()->GetGameStatus()->IncFrigleDestroyedCount();
	if ( _sprite )
	{
		AnimerSimpleEffect::Get()->Anim_FragileDestroy( this );
	
		if ( ! _level->IsDemoRunning() )
			SoundEngine::Get()->PlayRandomEffect( 
				10.0f,
				SoundEngine::eSoundFragileDestroyed1,
				SoundEngine::eSoundFragileDestroyed2,
				SoundEngine::eSoundFragileDestroyed3,
				SoundEngine::eSoundFragileDestroyed4,
				SoundEngine::eSoundNone	);
	}

	DestroyBody();
}
//------------------------------------------------------------------------------------------------------
void puBlock::Cut()
{

}
//------------------------------------------------------------------------------------------------------
void puBlock::FlipX()
{
	b2Vec2 centerPoint;
	centerPoint = GetRoot()->GetPosition();

	_flipX = _flipX == puBlock::eAxisNormal ? puBlock::eAxisFlipped : puBlock::eAxisNormal ;
	bool flip  = _flipX == puBlock::eAxisNormal ? false : true ;

	D_FLOAT( Tools::RadToDeg( _body->GetAngle() ))
	// it is a hack below
	// because rotation just uwzglednia flipx 
	// wogle nie patrzy na flipy - hack to make tflap work
	_body->SetTransform( b2Vec2( centerPoint.x + ( _flipX * _deltaX ), centerPoint.y + ( _flipY * _deltaY )), 0.0f );
	_deltaRotation = -_deltaRotation;

	if ( _sprite != NULL ) 
		_sprite->setFlipX( flip );

	if ( _spriteShadow != NULL ) 
		_spriteShadow->setFlipX( flip );

	if ( GetNext() != NULL )
		GetNext()->FlipX();

	// CreateJoints has to be executed after all blocks
	// finished flipping.. otherwise recreating joints
	// with some flipped some no
	DestroyJoints();
	if ( AmIRoot() ) 
		SetPosition( GetReversedPosition() ); // to refresh
	CreateJoints();
}
//------------------------------------------------------------------------------------------------------
void puBlock::FlipY()
{
	b2Vec2 centerPoint = _body->GetPosition() - b2Vec2( _deltaX * _flipX, _deltaY * _flipY );

	_flipY = _flipY == puBlock::eAxisNormal ? puBlock::eAxisFlipped : puBlock::eAxisNormal ;
	bool flip  = _flipY == puBlock::eAxisNormal ? false : true ;

	_body->SetTransform( b2Vec2( centerPoint.x + ( _flipX * _deltaX ), centerPoint.y + ( _flipY * _deltaY )), _body->GetAngle() );

	if ( _sprite != NULL ) 
		_sprite->setFlipY( flip );

	if ( _spriteShadow != NULL ) 
		_spriteShadow->setFlipY( flip );

	if ( GetNext() != NULL )
		GetNext()->FlipY();

	DestroyJoints();
	CreateJoints();
}
//------------------------------------------------------------------------------------------------------
void puBlock::CenterOnScreen()
{
	SetPosition( Config::GameSize.width / RATIO / 2.0f, Config::GameSize.height / RATIO / 2.0f );
}
//------------------------------------------------------------------------------------------------------
void puBlock::RemoveSprite( bool cleanUp, bool recrusive )
{
	if ( _sprite && _sprite->getParent() )
	{
		_sprite->removeFromParentAndCleanup( cleanUp );
		_sprite = NULL;
	}

	if ( _spriteShadow && _spriteShadow->getParent() )
	{
		_spriteShadow->removeFromParentAndCleanup( cleanUp );
		_spriteShadow = NULL;
	}

	if ( GetNext() != NULL && recrusive )
		GetNext()->RemoveSprite( cleanUp );
}
//------------------------------------------------------------------------------------------------------
bool puBlock::LoadDefaultSprite()
{
	char filename[64];

    snprintf( &filename[0], 64, "Images/Blocks/%s.png", GetClassName2() );
    snprintf( &filename[0], 64, "%s", Skins::GetSkinName( filename ) );
    
	DS_LOG( "Looking for default: %s", filename )
	if ( unResourceFileCheckExists( &filename[0] ) )
	{
		DS_LOG( "Loading default: %s", filename )
		_sprite = CCSprite::spriteWithFile( filename );
		return true;
	}
	else
	{
		DS_LOG( "Missing default: %s", filename )
		unAssertMsg( puBlock, false, ("Missing png file :%s", filename ));
		return false;
	}
}//------------------------------------------------------------------------------------------------------
bool puBlock::LoadDefaultSpriteStatic()
{
	LoadDefaultSprite();
	_glueSprite = GlueCircleSprite::Create( _width );
	return true;
}
//------------------------------------------------------------------------------------------------------
CCSprite* puBlock::LoadSkinedSprite( const char *filenam )
{
	CCSprite* sprite;
	string file;

	file.append( "Images/Blocks/" );
	file.append( filenam );

	sprite = CCSprite::spriteWithFile( Skins::GetSkinName( file.c_str() ) );
	return sprite;
}
//------------------------------------------------------------------------------------------------------
void puBlock::SetDeltaXY( float dx, float dy, bool recrusive )
{
	if ( GetNext() != NULL && recrusive)
	{
		GetNext()->SetDeltaXY( dx, dy, recrusive );
		_deltaX += dx;
		_deltaY += dy;
	}
	else
	{
		_deltaX = dx;
		_deltaY = dy;
	}
}
//------------------------------------------------------------------------------------------------------
void puBlock::SetDeltaX( float dx )
{
	_deltaX = dx;
}
//------------------------------------------------------------------------------------------------------
void puBlock::SetDeltaY( float dy/*, bool recrusive */)
{
	_deltaY = dy;
}
//------------------------------------------------------------------------------------------------------
void puBlock::DestroyBody()
{
	if ( ! _body )
		return;

	_level->RemoveFromContainers( this );
	_body->SetUserData( NULL );
	_world->DestroyBody( _body );
 	_body = NULL;
}
//------------------------------------------------------------------------------------------------------
void puBlock::SetSprite( CCSprite* sprite )
{
	if ( _sprite && _sprite->getParent() )
		_sprite->removeFromParentAndCleanup( true );

	_sprite = sprite; 
}
//------------------------------------------------------------------------------------------------------
void puBlock::SetSprite( CCSprite* sprite, int zOrder )
{
	SetSprite( sprite );
	_spriteZOrder = zOrder;
}
//------------------------------------------------------------------------------------------------------
void puBlock::Step()
{
}
//------------------------------------------------------------------------------------------------------
void puBlock::LevelQuiting()
{
}
//------------------------------------------------------------------------------------------------------
void puBlock::LevelExiting()
{
}
//------------------------------------------------------------------------------------------------------
CCSprite* puBlock::GetSkinSprite( const char *filename )
{
	char file[64];
	snprintf( &file[0], 64, Skins::GetSkinName( "Images/Blocks/%s.png" ) , filename );

	//DS_LOG( "Looking for default: %s", filename )
	if ( unResourceFileCheckExists( &filename[0] ) )
		return CCSprite::spriteWithFile( filename );
	else
	{
		//DS_LOG( "Missing default: %s", filename )
		unAssertMsg( puBlock, false, ("Missing png file :%s", filename ));
		return NULL;
	}
}
//------------------------------------------------------------------------------------------------------
float puBlock::GetDeltaRotation()
{
	return _deltaRotation;
}
//------------------------------------------------------------------------------------------------------
float puBlock::GetBouyancy()
{
	if ( ! _area )
	{
		unAssertMsg(puBlock, false, ("Area should be set"));
		_area = CalculateArea();
	}

	//Fwyp = gestosc wody * 9,81 * objetosc - masa * 9,81

	float fwyp;
	float getWody;
	float grav;

	float objetosc;
	float masa;

	getWody = 1;
	grav = 9.81f;
	objetosc = _area;
	masa = _area * _body->GetFixtureList()->GetDensity();

	fwyp = getWody * grav * objetosc - masa * grav;
	return fwyp * _bouancyRate;
	return _area * _bouancyRate / _body->GetFixtureList()->GetDensity() / 1000.0f;
}
//------------------------------------------------------------------------------------------------------
float puBlock::CalculateArea()
{
	switch ( _shapeType )
	{
	case eShape_Box :
		_area = _width * _height;
		break;

	case eShape_Circle :
		_area = b2_pi * pow ((_width / 2.0f ), 2 );
		break;

	case eShape_Unknown :
	case eShape_Polygon :
	default: 
		unAssertMsg( puBlock, false, ("Don't know how calculate area for name:%s type:%d", GetClassName2(), _shapeType));
	}

	return _area;
}
//------------------------------------------------------------------------------------------------------
void puBlock::StartDayAndNight()
{
	return;
	if ( ! _sprite || ! _sprite->getParent() )
		return;

	float dayLong = Config::AnimDayLong;

	CCTintTo *sunset = CCTintTo::actionWithDuration( dayLong / 4.0f, Config::BlockTint, Config::BlockTint, Config::BlockTint );
	CCTintTo *sunrise = CCTintTo::actionWithDuration( dayLong / 4.0f, 255, 255, 255 );
	CCFadeTo *day = CCFadeTo::actionWithDuration( dayLong, _sprite->getOpacity() );
	CCFadeTo *night = CCFadeTo::actionWithDuration( dayLong, _sprite->getOpacity() );

	CCRepeatForever *repeat = CCRepeatForever::actionWithAction( dynamic_cast<CCActionInterval*>( CCSequence::actions( sunrise, day, sunset, night, NULL )));
	_sprite->runAction( repeat );
}
//------------------------------------------------------------------------------------------------------
void puBlock::LevelFinished()
{

}
//------------------------------------------------------------------------------------------------------



//------------------------------------------------------------------------------------------------------
// puBlock ( Node stuff )
//------------------------------------------------------------------------------------------------------
void puBlock::SetNodeChain( puBlock* item, ... )
{
	if ( ! item )
		return;

	puBlock* currentNode = this;

	while ( currentNode->GetNext() != NULL )
		currentNode = currentNode->GetNext();

	currentNode->SetNext( item );
	currentNode = currentNode->GetNext();


	va_list args;
	va_start( args, item );

	puBlock *i = va_arg( args, puBlock* );

	while ( i )
	{
		while ( currentNode->GetNext() != NULL )
			currentNode = currentNode->GetNext();

		currentNode->SetNext( i );
		currentNode = i;

		i = va_arg( args, puBlock* );
	}
	va_end(args);
}
//------------------------------------------------------------------------------------------------------
void puBlock::SetNext( puBlock *next )
{
	_next = next;

	if ( _next )
		_next->SetPrev( this );
}
//------------------------------------------------------------------------------------------------------
puBlock* puBlock::GetRoot()
{
	puBlock* node;
	node = this;

	while ( node->_prev != NULL )
		node = node->_prev;

	return node;
}
//------------------------------------------------------------------------------------------------------
bool puBlock::AmIRoot()
{
	return ( _prev == NULL ? true : false );
}
//------------------------------------------------------------------------------------------------------
bool puBlock::ShouldSpeedBreak()
{
	if ( _speedBreak <= 0.0f )
		return false;

	float speedx;
	float speedy;
	float speedAng;
	float speedTotal;

	speedx = _body->GetLinearVelocity().x;
	speedy = _body->GetLinearVelocity().y;
	speedAng = _body->GetAngularVelocity();
	
	speedTotal = sqrt( speedx * speedx + speedy * speedy );
	speedTotal += speedAng / 2.0f;

	if ( speedTotal > _speedBreak )
		return true;

	return false;
}
//------------------------------------------------------------------------------------------------------
void puBlock::SetEmptySprite()
{
	if ( _sprite && _sprite->getParent() )
		_sprite->removeFromParentAndCleanup( true );

	if ( _spriteShadow && _spriteShadow->getParent() )
		_spriteShadow->removeFromParentAndCleanup( true );

	_sprite =  CCSprite::spriteWithFile( Skins::GetSkinName( "Images/Blocks/none.png") );
	_spriteZOrder = GameTypes::eSpritesHidden;
}
//------------------------------------------------------------------------------------------------------
void puBlock::ResetSpriteZOrder( int zOrder )
{
	_spriteZOrder = zOrder;

	CCNode *parent;
	parent = _sprite->getParent();

	if ( parent )
	{
		_sprite->removeFromParentAndCleanup( false );
		parent->addChild( _sprite, _spriteZOrder );
	}
}
//------------------------------------------------------------------------------------------------------
void puBlock::SetBodyType( b2BodyType type )
{
	if ( _body )
		_body->SetType( type );
}
//------------------------------------------------------------------------------------------------------
const char* puBlock::GetClassName2()
{
	return BlockNames::Get()->GetName( _blockEnum );
}

//------------------------------------------------------------------------------------------------------
// puBlock ( Node stuff ) end
//------------------------------------------------------------------------------------------------------



//------------------------------------------------------------------------------------------------------
#ifdef BUILD_EDITOR
puBlockDef puBlock::GetBlockDef()
{
	puBlockDef def;
	strcpy( def._className, GetClassName2() );
	def._uid = GetUID();
	def._blockEnum = GetBlockEnum();
	def._bodyType = GetBody()->GetType();
	def._blockType = GetBlockType();
	def._posX = GetReversedPosition().x;
	def._posY = GetReversedPosition().y;
	def._flipX = ( puBlock::eAxisFlipped == GetFlipX() ? true : false );
	def._flipY = ( puBlock::eAxisFlipped == GetFlipY() ? true : false );
	def._rotation = GetRotation();
	def._resitution = GetBody()->GetFixtureList()->GetRestitution();
	def._friction = GetBody()->GetFixtureList()->GetFriction();
	def._density = GetBody()->GetFixtureList()->GetDensity();

	def._linearVelocity = GetBody()->GetLinearVelocity();
	def._linearDamping = GetBody()->GetLinearDamping();
	def._angularVelocity = GetBody()->GetAngularVelocity();
	def._angularDamping = GetBody()->GetAngularDamping();
	
	def._width = _width;
	def._height = _height;
	def._filter = GetBody()->GetFixtureList()->GetFilterData();
	def._bouyancyRate = GetBouyancyRate();
	def._speedBreakLimit = GetSpeedBreak();

	//---------------------------------------------
	// Circle
	//---------------------------------------------
	if ( CirclesClassNames().count( GetBlockEnum() ) > 0 )
	{
		puCircleBase *circle = (puCircleBase* ) this;
		def._radius = circle->GetBody()->GetFixtureList()->GetShape()->m_radius;
	}


	//---------------------------------------------
	// Polygon
	//---------------------------------------------
	if ( ! strncmp( GetClassName2(), "Polygon", strlen("Polygon")))
	{
		puPolygon *polygon;
		polygon = ( puPolygon * ) this;

		for ( int i=0; i < polygon->GetVerticesCount(); i++ )
		{
			def._vertices[i] = polygon->GetVertex( i );
		}
		def._verticesCount = polygon->GetVerticesCount();
	}

	//---------------------------------------------
	// Triangle
	//---------------------------------------------
	if ( BlockCreator::Get()->GetCreateTriangleMap().count( GetBlockEnum() ) != 0 )
	{
		puTriangleBase *triangle = (puTriangleBase* ) this;
		def._t1 = triangle->GetT1();
		def._t2 = triangle->GetT2();
		def._t3 = triangle->GetT3();
	}

	//---------------------------------------------
	// Chain
	//---------------------------------------------
	if ( BlockCreator::Get()->GetCreateChainMap().count( GetBlockEnum() ) != 0 )
	{
		puChainCutable *chain = (puChainCutable* ) this;
		def._chainInfo = chain->GetConstInfo();

		def._chainInfo._isHookedBlockFixed = chain->IsHookFixed();
		
		if ( chain->GetHookedBlock() )
			def._chainInfo._hookedBlockUID = chain->GetHookedBlock()->GetUID();
		else
			def._chainInfo._hookedBlockUID = eUidNoBlock;
	}


	//---------------------------------------------
	// Bomb & MagnetBomb
	//---------------------------------------------
	if ( ! strncmp( GetClassName2(), "Bomb", 4 ) || 
		! strncmp( GetClassName2(), "MagnetBomb", 10 ))
	{
		puBomb_ *bomb = (puBomb_* ) this;
		def._blockRange = bomb->GetRange();
		def._bombImpulse = bomb->GetBombImpulse();
	}


	//---------------------------------------------
	// Magnet & MagnetBomb
	//---------------------------------------------
	if ( ! strncmp( GetClassName2(), "Magnet", 6 )) //MagnetBomb as well works
	{
		puMagnet_ *magnet = (puMagnet_* ) this;
		def._blockRange = magnet->GetRange();
		def._magnetImpulse = magnet->GetMagnetImpulse();
	}



	//---------------------------------------------
	// BoxerGlove
	//---------------------------------------------
	if ( ! strncmp( GetClassName2(), "BoxerGlove", 10 ))
	{
		puBoxerGlove *boxerGlover = (puBoxerGlove* ) this;
		def._boxerGloveForce = boxerGlover->GetBoxerForce();
	}


	//---------------------------------------------
	// Mothers
	//---------------------------------------------
	if ( MotherClassNames().count( GetBlockEnum() )) 
	{
		puMotherBase *mother = (puMotherBase * ) this;
		def._motherKidsCount = mother->GetKidsCount();
		def._motherKidsSize = mother->GetKidsSize();
	}

	return def;
}

//------------------------------------------------------------------------------------------------------
void puBlock::SetBlockDef( const puBlockDef& def )
{
	SetUID( def._uid );
	SetBodyType( def._bodyType );
	SetBlockType( def._blockType );
	SetPosition( def._posX, def._posY );
	SetRotation( def._rotation );
	SetBouyancyRate( def._bouyancyRate );
	SetSpeedBreak( def._speedBreakLimit );

	_body->SetLinearVelocity( def._linearVelocity );
	_body->SetLinearDamping( def._linearDamping );
	_body->SetAngularVelocity( def._angularVelocity );
	_body->SetAngularDamping( def._angularDamping );

	if ( def._flipX ) FlipX();
	if ( def._flipY ) FlipY();

	_body->GetFixtureList()->SetDensity( def._density );
	_body->GetFixtureList()->SetFriction( def._friction );
	_body->GetFixtureList()->SetRestitution( def._resitution );
	_body->GetFixtureList()->SetFilterData( def._filter );


	//---------------------------------------------
	// Bomb & MagnetBomb
	//---------------------------------------------
	if ( ! strncmp( def._className, "Bomb", 4 ) || 
		! strncmp( def._className, "MagnetBomb", 10 ))
	{
		puBomb_ *bomb;
		bomb = (puBomb_ *)this;

		bomb->SetRange( def._blockRange );
		bomb->SetBombImpulse( def._bombImpulse );
	}


	//---------------------------------------------
	// Magnet & MagnetBomb
	//---------------------------------------------
	if ( ! strncmp( def._className, "Magnet", 6 )) // MagnetBomb
	{
		puMagnet_ *magnet;
		magnet = (puMagnet_ *)this;

		magnet->SetRange( def._blockRange );
		magnet->SetMagnetImpulse( def._magnetImpulse );
	}

	
	//---------------------------------------------
	// BoxerGlove
	//---------------------------------------------
	if ( ! strncmp( GetClassName2(), "BoxerGlove", 10 ))
	{
		puBoxerGlove *boxerGlover = (puBoxerGlove* ) this;
		boxerGlover->SetBoxerForce( def._boxerGloveForce );
	}

	
	//---------------------------------------------
	// Chain
	//---------------------------------------------
	if ( ! strncmp( def._className, "Chain", 5 ))
	{
		puChainBase *chain;
		chain = (puChainBase * )this;

		chain->FixHook( def._chainInfo._isHookedBlockFixed );
	}


	//---------------------------------------------
	// Mothers
	//---------------------------------------------
	if ( MotherClassNames().count( GetBlockEnum() )) 
	{
		puMotherBase *mother = (puMotherBase * ) this;
		mother->SetKidsCount( def._motherKidsCount );
		//mother->SetKidsS( def._motherKidsCount );
	}


	UpdateSprite();
}



#endif //BUILD_EDITOR

