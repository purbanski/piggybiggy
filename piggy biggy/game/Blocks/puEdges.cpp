#include "CCGL.h"
#include <Box2D/Box2D.h>
#include "GLES-Render.h"
#include "Game.h"
#include "Levels/Level.h"
#include "puEdges.h"
#include "Tools.h"

//-----------------------------------------------------------------------------------------------------
puEdges::puEdges()
{
	SetBodyType( b2_staticBody );

	_edgesDraw = new EdgesDraw();
	_edgesDraw->autorelease();
	_edgesDraw->setPosition( ccp( 0.0f, 0.0f ));
	
	_sprite = CCSprite::spriteWithFile( Skins::GetSkinName( "Images/Blocks/none.png" ) );
	_sprite->setContentSizeInPixels( Config::GameSize );
	_sprite->addChild( _edgesDraw );
	
	SetSpriteZOrder( GameTypes::eSpritesBelowe );

}
//-----------------------------------------------------------------------------------------------------
void puEdges::SetEdges( Edges *edges, float scale )
{
	_edges = edges;

	b2PolygonShape edgeShape;
	b2FixtureDef edgeDef;

	edgeDef.shape = &edgeShape;
	edgeDef.density = 0.0f;
	edgeDef.friction = 0.7f;
	edgeDef.restitution = 0.2f;


	Edges::iterator it;
	Edge edge;
	b2Vec2 edgeStart;
	b2Vec2 edgeStop;

	
	for ( it = _edges->begin(); it != _edges->end(); it++ )
	{
		edge = ( *it );
		edgeStart.Set( edge._xa, edge._ya );
		edgeStop.Set( edge._xb, edge._yb );
	
		edgeStart *= scale;
		edgeStop *= scale;

		edgeShape.SetAsEdge( edgeStart, edgeStop );
		_body->CreateFixture( &edgeDef );
	}
	
	_edgesDraw->SetEdges( _edges, scale );
}

//-----------------------------------------------------------------------------------------------------






//-----------------------------------------------------------------------------------------------------
// EdgesDraw
//-----------------------------------------------------------------------------------------------------
EdgesDraw::EdgesDraw()
{
	_edges = NULL;
}
//-----------------------------------------------------------------------------------------------------
void EdgesDraw::draw()
{
	if ( ! _edges )
		return;

	GLESDebugDraw::Get()->LockGL();
	
	puEdges::Edges::iterator it;
	puEdges::Edge edge;

	b2Vec2 edgeStart;
	b2Vec2 edgeStop;
	b2Vec2 delta;

	b2Vec2 v[2];

	delta.Set( Config::GameSize.width / 2.0f / RATIO / _scale , Config::GameSize.height / 2.0f / RATIO / _scale );
	
	for ( it = _edges->begin(); it != _edges->end(); it++ )
	{
		edge = ( *it );
		edgeStart.Set( edge._xa, edge._ya );
		edgeStop.Set( edge._xb, edge._yb );

		edgeStart += delta;
		edgeStop += delta;

		v[0] = _scale * edgeStart;
		v[1] = _scale * edgeStop;

		glLineWidth( 4.0f );
		GLESDebugDraw::Get()->DrawSolidBox( v, b2Color( 0.36f, 0.18f, 0.08f ), 1);

		glLineWidth( 1.5f );
		GLESDebugDraw::Get()->DrawSolidBox( v, b2Color( 1.0f, 0.77f, 0.56f ), 1 );

	}
	GLESDebugDraw::Get()->UnlockGL();
}
//-----------------------------------------------------------------------------------------------------
void EdgesDraw::SetEdges( puEdges::Edges *edges, float scale )
{
	_edges = edges;
	_scale = scale;
}
