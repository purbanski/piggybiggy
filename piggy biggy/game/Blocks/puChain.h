#ifndef __PUCHAIN_H__
#define __PUCHAIN_H__

#include <list>
#include "cocos2d.h"
#include "Blocks/puCircle.h"
#include "Blocks/puBox.h"
#include "GameTypes.h"
#include "Animation/AnimerSimpleEffect.h"

using namespace std;
USING_NS_CC;

class puChainBase;
class ChainDraw;

//------------------------------------------------------------------------------------------
class puChainBit : public puBlock
{
public:
	static const float _sWidth;
	static const float _sHeight;

public:
	puChainBit( puChainBase *chain );
	~puChainBit();

	void RunCutAnim();
	void StopCutAnim();

	virtual void Cut();

private:
	puChainBase       *_chain;
	SpriteAndAction	_cutData;
};
//------------------------------------------------------------------------------------------
class puChainBase : public puScrew<5>
{
public:
	static const float _sScrewRadius;

	typedef list<puChainBit*> ChainBits;
	typedef list<b2Joint*> JointContainer;

public:
	virtual ~puChainBase();

	virtual void Cut();
	virtual void Step();
   	virtual void LevelQuiting();
	virtual void LevelExiting();

	void CleanUp();

	void SetChain( const GameTypes::ChainConstInfo& info );
	void SetChain( const b2Vec2& fixPoint, const b2Vec2& endPoint );
	void SetChainOShape( float radius, float piece );
	void SetChainUShape( float radius, float lengthA, float lengthB );
	void SetChainCubic( float lengthA, float lengthB, float lengthC );
	void SetChain( float length );

	void HookOnChain( puBlock* block );
	void CenterHookOnChain( puBlock* block );
	void CenterToLastBit();
	void FixHook( bool fixed );
	bool IsHookFixed();

	puBlock* GetChainEnd();

protected:
	puChainBase();

private:
	void AllocateChain( int chainBitsCount );

	//--------------------------
	//
	//--------------------------
	virtual void ChainAllocated() = 0;

	void SetChainBitsJoints();
	b2Vec2 GetRandImpulse();

protected:	
	ChainBits		_chainBits;
	JointContainer	_joints;

	// for editor only can be taken away if needed
public:
	GameTypes::ChainConstInfo GetConstInfo() { return _constInfo; }

	puBlock* GetHookedBlock();
	void UnHook();	

private:
	GameTypes::ChainConstInfo _constInfo;

	bool			_cut;
	int				_flySleepDelay;
	bool			_isHookedBlockFixed;
	puBlock			*_hookedBlock;
	b2RevoluteJoint	*_hookedJoint;
    bool            _quiting;
};
//------------------------------------------------------------------------------------------
class puChainCutable : public puChainBase
{
private:
	virtual void ChainAllocated();
};
//------------------------------------------------------------------------------------------
class puChainStrong : public puChainBase
{
private:
	virtual void ChainAllocated();
};
//------------------------------------------------------------------------------------------
class ChainDraw : public CCNode
{
public:
	typedef enum
	{
		eDraw_Normal = 1,
		eDraw_Cut
	} DrawMode;

	ChainDraw( puChainBase *chain );
	~ChainDraw();

	virtual void draw();
	void SetChainBits( puChainBase::ChainBits *chainBits );
	void SetDrawColor( b2Color );
	void Cut();

private:
	void DrawNormal();
	void DrawCut();

private:
	puChainBase				*_chain;
	puChainBase::ChainBits	*_chainBits;
	DrawMode			_drawMode;
	b2Color				_drawColor;

	float				_deltaColor;

};
//------------------------------------------------------------------------------------------

#endif
