#include <Box2D/Box2D.h>
#include "Levels/World.h"
#include "Levels/Level.h"
#include "puBank.h"
#include "Components/CustomSprites.h"

//-----------------------------------------------------------------------------------------------------
puBank::puBank()
{
	// Create Coins
	puBlock *coin;
	puBlock *block;
	
	float x;
	float y;

	float xDelta = -0.0f;
	float yDelta = 5.0f;

	for ( int  i = 0 ; i < COINS_COUNT; i++ )
	{
		x = xDelta - 16.5f +( i * 6 - ( ((float)( i / 4 )) * 23.7f ));
		y = yDelta + (((float)( i / 4 )) * 6.0f);

		coin = new puCoin<27>();
		//coin->SetDeltaXY( x + (rand() % 2), y );
		coin->SetDeltaXY( x, y );

		if ( _coins.size() != 0 )
		{
			block = (_coins.at( _coins.size() - 1 ));
			block->SetNext( coin );
		}
		_coins.push_back( coin );
	}

	// SetDeltaXYS
	xDelta = 2.2f;

	_wall1.SetDeltaXY( -23.2f + xDelta, 2.0f * 7.5f );
	_wall2.SetDeltaXY( 2.75f + xDelta, 2.0f * 7.5f );
	_wall3.SetDeltaXY( 20.5f + xDelta, 2.0f * 7.5f );

	_bankSave1.SetDeltaXY( -9.5f + xDelta, 14.25f );
	_piggyBank.SetDeltaXY( 14.5f, 6.5f );
	_borderWall.SetDeltaXY( -18.3f, 4.9f );
	
	_piggyBank.GetBody()->SetAngularDamping( 0.05f );
	_piggyBank.GetBody()->SetLinearDamping( 0.05f );

	// SetNodeChain
	SetNodeChain( &_wall1, &_wall2, &_wall3, &_bankSave1, &_borderWall, &_piggyBank, NULL );
	_piggyBank.SetNext( _coins.at( 0 ) );

	_wall1.SetEmptySprite();
	_wall2.SetEmptySprite();
	_wall3.SetEmptySprite();
	SetEmptySprite();

	// Other
	b2Filter filter;
	filter.categoryBits = 0;
	filter.groupIndex = -1;
	filter.maskBits = 0xffff;

	
	_bankSave1.GetBody()->GetFixtureList()->SetFilterData( filter );
	_bankSave1.SetSprite( BankSaveSprite::Create(), GameTypes::eSpritesAboveX2 );
	//_bankSave1.GetSprite()->setOpacity( 10 );

	_bankOpen = false;

	Preloader::Get()->AddSound( SoundEngine::eSoundBankSaveBigWheel );
	Preloader::Get()->AddSound( SoundEngine::eSoundBankSaveSmallWheel );
	Preloader::Get()->AddSound( SoundEngine::eSoundBankSaveSlideUp );

	Preloader::Get()->AddSound( SoundEngine::eSoundBankSettingWheelPryk ); 

	Preloader::Get()->AddSound( SoundEngine::eSoundBankLockBarsSlide );
	Preloader::Get()->AddSound( SoundEngine::eSoundBankLockUnlocked );
	Preloader::Get()->AddSound( SoundEngine::eSoundBankLockLocked );

	SetPosition( 0.0f, 0.0f );
}
//-----------------------------------------------------------------------------------------------------
puBank::~puBank()
{
	BlockContainer::iterator it;

	for ( it = _coins.begin(); it != _coins.end(); it++ )
		delete (*it);
}
//-----------------------------------------------------------------------------------------------------
void puBank::OpenBankFinish( void *data )
{
	puBank *bank;
	bank = (puBank *) data;

	bank->_bankSave1.DestroyBody();
	bank->_wall2.DestroyBody();
}
//-----------------------------------------------------------------------------------------------------
void puBank::OpenBankInit()
{
	if ( _bankOpen ) 
		return;

	_bankOpen = true;
	
	BankSaveSprite *bankSprite;
	bankSprite = (BankSaveSprite *) _bankSave1.GetSprite();
	bankSprite->StartAnim( &puBank::OpenBankFinish, this );
}
//-----------------------------------------------------------------------------------------------------



//-----------------------------------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------------------------------
puBankBorderWall::puBankBorderWall()
{
	_blockEnum = eBlockBankBorderWall;
	_sprite = MovedSprite::Create( 183.0f, 80.0f, 0.0f, "Images/Blocks/BankBorderWall.png" );
	_spriteZOrder = GameTypes::eSpritesAboveX3;
}
//-----------------------------------------------------------------------------------------------------
