#ifndef __PUBALANCINGSCALE_H__
#define __PUBALANCINGSCALE_H__

#include <Box2D/Box2D.h>
#include "puBlock.h"
#include "puCircle.h"
#include "puBox.h"

class puBalancingScale : public puBlock
{
public:
	puBalancingScale(); 
	virtual ~puBalancingScale();

	virtual void SetPosition( float x, float y );

private:
	void Construct();

	puWallBox<8,1>	_platform;
	//puWall		_stand; // i'm a stand
	puWallBox<6,1>	_leftArm;
	puWallBox<6,1>	_rightArm;
	puCircle<>	_wieghtBlock;
};

#endif
