#include "puBoxerGlove.h"

//--------------------------------------------------------------------------
puBoxerGlove::puBoxerGlove()
{
	b2Filter filter;
	filter.groupIndex = -17;
	filter.categoryBits = 1;
	filter.maskBits = 65535;


	//----------------------------
	// Moi
	_blockEnum = eBlockBoxerGlove;
	SetBodyType( b2_dynamicBody );
	SetBlockType( GameTypes::eActivable );
	SetSpriteZOrder( GameTypes::eSpritesAbove );
	LoadDefaultSprite();
	_activated = false;
	_joint1 = NULL;
	_force = 20000.0f;

	//----------------------------
	// boxer hand
	_boxerHand.SetRotation( 0.0000f );
	_boxerHand.SetDeltaRotation( 0.0000f );
	_boxerHand.GetBody()->GetFixtureList()->SetFilterData( filter );

	_boxerHand.SetGlove( this );
	_boxerHand.SetDeltaXY( 1.5000f, -0.2500f );


	//----------------------------
	// SetNodeChain
	SetNext( &_boxerHand );
	
	SetPosition( Config::GameSize.width / 2.0f / RATIO, Config::GameSize.height / 2.0f / RATIO );
	CreateJoints();
}
//--------------------------------------------------------------------------
puBoxerGlove::~puBoxerGlove()
{

}
//--------------------------------------------------------------------------
void puBoxerGlove::CreateJoints()
{
	b2PrismaticJointDef jointDef1;

	jointDef1.Initialize( GetBody(), _boxerHand.GetBody(), GetPosition(), b2Vec2( _flipX * 1.0f, 0.0f ));
	jointDef1.enableMotor = true;
	jointDef1.maxMotorForce = 2000.0f;
	jointDef1.enableLimit = true;
	jointDef1.upperTranslation = 7.0f;
	jointDef1.lowerTranslation = 0.0f;

	_joint1 = (b2PrismaticJoint *) _world->CreateJoint( &jointDef1 );
}
//--------------------------------------------------------------------------
void puBoxerGlove::DestroyJoints()
{
	if ( _joint1 )
		_world->DestroyJoint( _joint1 );

	_joint1 = NULL;
}
//--------------------------------------------------------------------------
void puBoxerGlove::Activate( void *ptr )
{
	if ( ! _activated )
	{
		float forceApplied;
		forceApplied = _force;

		if ( _flipX == puBlock::eAxisNormal )
			forceApplied = -forceApplied;

		_body->ApplyLinearImpulse( b2Vec2( forceApplied, 0.0f ), GetPosition() );
		SoundEngine::Get()->PlayEffect( SoundEngine::eSoundBoxerGlovePunch );
		_activated = true;
	}
}
//--------------------------------------------------------------------------
void puBoxerGlove::SetBoxerForce( float force )
{
	_force = force;
}
//--------------------------------------------------------------------------
float puBoxerGlove::GetBoxerForce()
{
	return _force;
}


//--------------------------------------------------------------------------
// Boxer Glove
//--------------------------------------------------------------------------
puBoxerHand::puBoxerHand()
{
	_blockEnum = eBlockBoxerHand;
	_shapeType = eShape_Box;
	
	SetSpriteZOrder( GameTypes::eSpritesBelowe );
	LoadDefaultSprite();

	_width = 100.0f / RATIO / 2.0f;
	_height = 40.0f / RATIO / 2.0f;

	b2PolygonShape shape;
	shape.SetAsBox( _width, _height );

	b2FixtureDef fixtureDef;
	fixtureDef.shape		= &shape;
	fixtureDef.density		= WallDensity;
	fixtureDef.restitution	= WallRestitution;
	fixtureDef.friction		= WallFriction;

	_body->CreateFixture( &fixtureDef );

	//
	SetBlockType( GameTypes::eActivable );
	SetBodyType( b2_staticBody );
	
}
//--------------------------------------------------------------------------
puBoxerHand::~puBoxerHand()
{

}
//--------------------------------------------------------------------------
void puBoxerHand::Activate( void *ptr )
{
	if ( _glove )
		_glove->Activate( ptr );
}
//--------------------------------------------------------------------------
void puBoxerHand::SetGlove( puBlock *glove )
{
	_glove = glove;
}
//--------------------------------------------------------------------------
