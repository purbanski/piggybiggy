#include "puBalancer.h"
#include "Levels/Level.h"

const float puBalancer::sPinRadius = 0.5f;
//-----------------------------------------------------------------------------------------------------
puBalancer::puBalancer() : _pin( sPinRadius )
{
	Construct();
}
//-----------------------------------------------------------------------------------------------------
void puBalancer::Construct()
{
	// Platform
	b2PolygonShape box;
	box.SetAsBox( 11.0f , 0.5f );

	b2FixtureDef fixtureDef;
	fixtureDef.shape = &box;
	fixtureDef.density = 5.0f;
	fixtureDef.friction = 1.0f; // tarcie 0 - nie ma 1- max
	fixtureDef.restitution = 0.0f; // bouncy 0 - nie ma 1 max;
	
	_body->CreateFixture( &fixtureDef );
	_blockType = GameTypes::eMoveable;
	
	// Pin
	_pin.GetBody()->SetType( b2_staticBody );
	_pin.GetBody()->GetFixtureList()->SetRestitution( 0.2f );


	// Joint
	b2RevoluteJointDef jointDef;
	jointDef.Initialize( _body, _pin.GetBody(), _body->GetPosition() );
	jointDef.lowerAngle = -0.04f * b2_pi; // -90 degrees
	jointDef.upperAngle =  0.04f * b2_pi; // 45 degrees
	jointDef.enableLimit = true;
	
	jointDef.maxMotorTorque = 10.0f;
	jointDef.motorSpeed = 0.0f;
	jointDef.enableMotor = true;
	//jointDef.enableMotor = false;

    (void) _world->CreateJoint(&jointDef);

	_sprite = CCSprite::spriteWithFile( Skins::GetSkinName( "Images/Blocks/Balancer.png" ) );
	_pin.SetSprite( CCSprite::spriteWithFile( Skins::GetSkinName( "Images/Blocks/BalancerPin.png" ) ));
	_pin.SetSpriteZOrder( GameTypes::eSpritesAbove );

	SetNext( &_pin );
}
//-----------------------------------------------------------------------------------------------------
puBalancer::~puBalancer()
{
}
//-----------------------------------------------------------------------------------------------------
void puBalancer::PutOn( float deltaX, float deltaY, puBlock *b1, puBlock *b2 )
{
	float balancerX = _body->GetPosition().x;
	float balancerY = _body->GetPosition().y;
	
	float b1DistanceFromBalancer = deltaX ;
	float b2DistanceFromBalancer = 0 ; // to be calculated

	float b1Mass;
	float b2Mass;

	b1Mass = b1->GetBody()->GetMass();
	b2Mass = b2->GetBody()->GetMass();

	b2DistanceFromBalancer = b1Mass * b1DistanceFromBalancer / b2Mass;

	b1->SetPosition( balancerX + b1DistanceFromBalancer, balancerY + deltaY );
	b2->SetPosition( balancerX - b2DistanceFromBalancer, balancerY + deltaY );
}
