#ifndef __PUROLLINGPLATFORM_H__
#define __PUROLLINGPLATFORM_H__

#include <Box2D/Box2D.h>
#include "puBlock.h"
#include "puBox.h"
#include "puCircle.h"

class puRollingPlatform: public puBlock
{
public:
	puRollingPlatform();
	virtual ~puRollingPlatform();

private:
	puCircle<55>			_circle;
	puWallBox<8,1>			_platform;
	puWallBox<7,1>			_platformRight;
	puWallBox<7,1>			_platformLeft;

	puCircle<20>			_pin;

	b2RevoluteJoint		*_jointGroundCircle;
	b2PrismaticJoint	*_jointGroundPlatform;
	b2GearJoint			*_jointGear;
};

#endif
