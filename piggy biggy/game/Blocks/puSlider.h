#ifndef __PUSLIDER_H__
#define __PUSLIDER_H__

#include <Box2D/Box2D.h>
#include "puBlock.h"
#include "puBox.h"

class puSlider : public puBlock
{
public:
	puSlider( float xpos, float ypos );
	virtual ~puSlider();

private:
	void Construct( float xpos, float ypos );

	// Values maybe should be /2
	puWallBox<2,8>	_armGround;
	puWallBox<2,16>	_armPusher;
	puWallBox<9,9>	_pusher;
};

#endif
