#include "GameConfig.h"
#include "puMother.h"
#include "puBlocksSettings.h"
#include "Preloader.h"
#include "Animation/AnimManager.h"
#include "GameTypes.h"

using namespace GameTypes;
using namespace pu::blocks::settings;

//-------------------------------------------------------------------------------------------
// MotherClassNames
//-------------------------------------------------------------------------------------------
set<BlockEnum> MotherClassNames()
{
	set<BlockEnum> names;
	names.insert( eBlockMotherPiggy );
	names.insert( eBlockMotherPiggyStatic );
	names.insert( eBlockCashCow );
	names.insert( eBlockCashCowStatic );
	names.insert( eBlockPrison );
	//names.insert( "PoliceStation");
	//names.insert( "PoliceStationStatic");
	
	return names;
}


//-------------------------------------------------------------------------------------------
// PiggyFarm
//-------------------------------------------------------------------------------------------
void puMotherPiggy_::Construct()
{
	_blockEnum = eBlockMotherPiggy;
	_sprite = AnimManager::Get()->CreateSitSprite( this ); 
	_spriteShadow = AnimManager::Get()->CreateSitSprite( this ); 
	_spriteZOrder = GameTypes::eSpritesCharacter;
	CounterConstruct();

	SetBodyType( b2_dynamicBody );

	Preloader::Get()->AddSound( SoundEngine::eSoundPiggyNewBorn );
}


//-------------------------------------------------------------------------------------------
// MotherPiggyStatic
//-------------------------------------------------------------------------------------------
void puMotherPiggyStatic_::Construct()
{
	_blockEnum = eBlockMotherPiggyStatic;
	_sprite = AnimManager::Get()->CreateSitSprite( this ); 
	_spriteShadow = AnimManager::Get()->CreateSitSprite( this ); 
	_spriteZOrder = GameTypes::eSpritesCharacter;
	_glueSprite = GlueCircleSprite::Create( _radius );

	CounterConstruct();

	SetBodyType( b2_staticBody );
	Preloader::Get()->AddSound( SoundEngine::eSoundPiggyNewBorn );
}



//-------------------------------------------------------------------------------------------
// MotherPiggyStatic
//-------------------------------------------------------------------------------------------
void puPiggyFarm_::Construct()
{

}



//-------------------------------------------------------------------------------------------
// Cash Cow
//-------------------------------------------------------------------------------------------
void puCashCow_::Construct()
{
	_blockEnum = eBlockCashCow;
	_sprite = AnimManager::Get()->CreateSitSprite( this ); 
	_spriteShadow = AnimManager::Get()->CreateSitSprite( this ); 
	_spriteZOrder = GameTypes::eSpritesPiggyBankAbove;

	CounterConstruct();

	SetBodyType( b2_dynamicBody );
	Preloader::Get()->AddSound( SoundEngine::eSoundCoinNewBorn );
}
//-------------------------------------------------------------------------------------------
void puCashCow_::PlayNewBornEffect()
{
	SoundEngine::Get()->PlayEffect( SoundEngine::eSoundCoinNewBorn );
}



//-------------------------------------------------------------------------------------------
// Cash Cow Static
//-------------------------------------------------------------------------------------------
void puCashCowStatic_::Construct()
{
	_blockEnum = eBlockCashCowStatic;
	_sprite = AnimManager::Get()->CreateSitSprite( this ); 
	_spriteShadow = AnimManager::Get()->CreateSitSprite( this ); 
	_spriteZOrder = GameTypes::eSpritesPiggyBankAbove;
	_glueSprite = GlueCircleSprite::Create( _radius );

	CounterConstruct();

	SetBodyType( b2_staticBody );
	Preloader::Get()->AddSound( SoundEngine::eSoundCoinNewBorn );
}
//-------------------------------------------------------------------------------------------
void puCashCowStatic_::PlayNewBornEffect()
{
	SoundEngine::Get()->PlayEffect( SoundEngine::eSoundCoinNewBorn );
}


//-------------------------------------------------------------------------------------------
// Prison
//-------------------------------------------------------------------------------------------
void puPrison_::Construct()
{
	_blockEnum = eBlockPrison;
	_sprite = AnimManager::Get()->CreateSitSprite( this ); 
	_spriteShadow = AnimManager::Get()->CreateSitSprite( this ); 
	_spriteZOrder = GameTypes::eSpritesCharacter;
	_glueSprite = GlueCircleSprite::Create( _radius );

	CounterConstruct();

	SetBodyType( b2_staticBody );
	Preloader::Get()->AddSound( SoundEngine::eSoundPrisonAlert );
}



// farm
void puFarm::Construct( float w, float h )
{
	_body->DestroyFixture( _body->GetFixtureList() );
	
	b2PolygonShape box;
	box.SetAsBox( _width / 2.0f, _height / 2.0f  );

	b2FixtureDef fixtureDef;
	fixtureDef.density = WallDensity;
	fixtureDef.restitution = WallRestitution;
	fixtureDef.friction = WallFriction;
	fixtureDef.shape = &box;

	_body->CreateFixture( &fixtureDef );
	_body->SetType( b2_staticBody );

	float roofHeight;
	roofHeight = _width / 3.0f;

	float roofExt;
	roofExt = 2.0f;

    b2Vec2 dontRemember;
    b2Vec2 width;
    b2Vec2 height;
    
    dontRemember.Set( -_width / 2.0f - roofExt, 0.0f );
    width.Set( _width / 2.0f + roofExt, 0.0f );
    height.Set( 0.0f, roofHeight );
    
	_roof.Construct( &dontRemember, &width, &height, NULL );
	_roof.SetDeltaXY( 0.0f, ( _height + roofHeight ) / 2.0f );
	
	SetNext( &_roof );
	SetPosition( 0.0f, 0.0f );

	LoadDefaultSprite();
}

void puFarm::LoadDefaultSprite()
{
	puBlock::LoadDefaultSprite();

	_counterLabel = CCLabelBMFont::labelWithString( "", Config::MotherCounterFont );
	_counterLabel->setPosition( ccp( 30.0f, 10.0f ));
	UpdateCounter();

	_sprite->addChild( _counterLabel );
}
