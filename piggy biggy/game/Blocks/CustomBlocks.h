#ifndef __CUSTOMBLOCKS_H__
#define __CUSTOMBLOCKS_H__

#include "puBlock.h"
#include "puPolygon.h"

//-------------------------------------------------------------------
class puPolygonFragile_Custom1 : public puPolygonFragile
{
public:
	puPolygonFragile_Custom1();
};
//-------------------------------------------------------------------
class puPolygonCoin_Custom1 : public puPolygonFragile
{
public:
	puPolygonCoin_Custom1();
	virtual void Destroy();
};
//-------------------------------------------------------------------
class puPolygonFragile_Custom2 : public puPolygonFragile
{
public:
	puPolygonFragile_Custom2();
};
//-------------------------------------------------------------------
class puPolygonWall_Custom1 : public puPolygonWall
{
public:
	puPolygonWall_Custom1();
};
//-------------------------------------------------------------------
class puPolygonWall_Custom2 : public puPolygonWall
{
public:
	puPolygonWall_Custom2();
};
//-------------------------------------------------------------------
#endif
