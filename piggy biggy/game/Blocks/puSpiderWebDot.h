#ifndef __PUSPIDERWEBDOT_H__
#define __PUSPIDERWEBDOT_H__


#include "puBlock.h"
#include "Blocks/puCircle.h"

class puSpiderWebDot : public puCircle_
{
public:
	puSpiderWebDot();
	~puSpiderWebDot();

	virtual void CreateJoints();
	virtual void DestroyJoints();

private:
	puCircle_			_circle1;
	b2RevoluteJoint		*_joint1;
};

#endif
