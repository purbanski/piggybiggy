#include <Box2D/Box2D.h>
#include "Debug/MyDebug.h"
#include "SoundEngine.h"
#include "Levels/Level.h"
#include "puGun.h"
#include "Animation/AnimTools.h"
#include "Animation/AnimManager.h"

//-----------------------------------------------------------------------------------------------------
// Bullet
//-----------------------------------------------------------------------------------------------------
puBullet::puBullet()
{
	_fired = false;
	_filterResetDelay = 6;
	_blockEnum = eBlockBullet;

	SetBlockType( GameTypes::eBullet );

	_body->SetBullet( true );
	_body->SetActive( false );

	b2CircleShape box;
	box.m_radius = 0.6f;

	b2FixtureDef fixtureDef;
	fixtureDef.shape = &box;
	fixtureDef.density = 120.0f;
	fixtureDef.friction = 0.050f;
	fixtureDef.restitution = 0.0f;

	_body->CreateFixture( &fixtureDef );
	LoadDefaultSprite();
}
//-----------------------------------------------------------------------------------------------------
void puBullet::Fired()
{
	_fired = true;
}
//-----------------------------------------------------------------------------------------------------
void puBullet::Step()
{
	if ( ! _fired )
		return;

	if ( _fired && _filterResetDelay )
		_filterResetDelay--;

	if ( ! _filterResetDelay )
	{
		FilterReset();
		_fired = false;
	}
}
//-----------------------------------------------------------------------------------------------------
void puBullet::FilterReset()
{
	b2Filter filter;

	filter.maskBits = 0xffff;
	filter.categoryBits = 1;
	filter.groupIndex = 0;

	GetBody()->GetFixtureList()->SetFilterData( filter );
}
//-----------------------------------------------------------------------------------------------------
void puBullet::SetExplosive( bool enable )
{
	if ( enable )
	{
		SetBlockType( GameTypes::eBulletExplosive );
		SetSprite( CCSprite::spriteWithFile( Skins::GetSkinName( "Images/Blocks/BulletExplosive.png" ) ));
	}
	else
	{
		SetBlockType( GameTypes::eBullet );
		SetSprite( CCSprite::spriteWithFile( Skins::GetSkinName( "Images/Blocks/Bullet.png" ) ));
	}
}


//--------------------------------------------------------------------------------
// Gun Base
//-----------------------------------------------------------------------------------------------------
puGunBase::puGunBase()
{
	Construct( b2Vec2( 6.0f, 4.0f ));
}
//-----------------------------------------------------------------------------------------------------
puGunBase::puGunBase( b2Vec2 size )
{
	Construct( size );
}

//-----------------------------------------------------------------------------------------------------
void puGunBase::Construct( b2Vec2 size )
{
	_width	= 2.0f * 6.0f;
	_height	= 2.0f * 6.0f;

	_shotCount = Config::GunClipSize;
	_shapeType = GameTypes::eShape_Circle;

	_bulletDelta.Set( -1.5f, -1.5f );
	//_bulletDelta.SetZero();

	_body->SetType( b2_dynamicBody );

	_blockType = GameTypes::eActivable;
	_spriteZOrder = GameTypes::eSpritesAbove;

	// sensor
//	_sensor.SetDeltaXY( 0.0f, 0.0f );


	// set filter so bullets do not colide with gun
	// otherwise at shot time, bullet starts to colide with gun
	// resulting in bullet flying not nice :)

	b2Filter filter;
	filter.groupIndex = 0;
	filter.categoryBits = Config::eFilterCategoryGun;
	filter.maskBits = ~Config::eFilterCategoryGun & 0xffff;

	b2CircleShape circle;
	circle.m_radius = _width / 2.0f;

	b2FixtureDef fixtureDef;
	fixtureDef.shape = &circle;
	fixtureDef.density = 7.5f;
	fixtureDef.friction = 0.4f;
	fixtureDef.restitution = 0.2f;
	fixtureDef.filter = filter;

	_body->CreateFixture( &fixtureDef );

	for ( int i = 0; i < Config::GunClipSize; i++ )
	{ 
		_bullets[i].SetDeltaXY( _bulletDelta.x, _bulletDelta.y );
		_bullets[i].GetBody()->GetFixtureList()->SetFilterData( filter );

		if ( i < Config::GunClipSize - 1 )
			_bullets[i].SetNext( &_bullets[i+1] );
	}
	
	// it should be there
	//_bullets[ puGunBase::CLIP_SIZE - 1 ].SetPrev( &_bullets[ puGunBase::CLIP_SIZE - 2 ]);

	SetNext( &_sensor );
	_sensor.SetNext( &_bullets[0] );
	//_sensor.SetNext( NULL );
	SetPosition( 0.0f, 0.0f );

	//-------------
	// joints
	//-------------
	b2RevoluteJointDef revJointDef;
	
	revJointDef.collideConnected = false;
	revJointDef.Initialize( _body, _sensor.GetBody(), GetPosition() );
	_level->GetWorld()->CreateJoint( &revJointDef );

	Preloader::Get()->AddSound( SoundEngine::eSoundSpit );
	Preloader::Get()->AddSound( SoundEngine::eSoundSpitEmpty );
}
//-----------------------------------------------------------------------------------------------------
puGunBase::~puGunBase()
{
}		
//-----------------------------------------------------------------------------------------------------
void puGunBase::Activate( void *ptr )
{
	if ( _shotCount != 0 )
	{
		_bullets[ _shotCount - 1 ].SetPositionDirect( GetBulletStartPostion() );
		_bullets[ _shotCount - 1 ].GetBody()->SetActive( true );
		_bullets[ _shotCount - 1 ].GetBody()->SetLinearVelocity( GetShotForce() );
		_bullets[ _shotCount - 1 ].Fired();
		//_bullets[ _shotCount + 1 ].SetNext( NULL );

		_shotCount--;
		DoAnimSpit();
	}
	else
	{
		DoAnimSpitEmpty();
	}
}
//-----------------------------------------------------------------------------------------------------
void puGunBase::SetBulletCount( int shots )
{
	unAssertMsg( puGun, shots <= Config::GunClipSize, ( "Too many bullets, wont fit into gun clip" ));
	_shotCount = shots;
}
//-----------------------------------------------------------------------------------------------------
b2Vec2 puGunBase::GetShotForce()
{
	b2Vec2 force;
	b2Vec2 center;

	center.SetZero();

	force.Set( -_flipX * 480.0f, 40.0f );
	force = Tools::RotatePointAroundPoint( force, center, GetRotation() );

	return force;
}
//-----------------------------------------------------------------------------------------------------
b2Vec2 puGunBase::GetBulletStartPostion()
{
	b2Vec2 pos;
	b2Vec2 center;

	center.SetZero();

	pos.x = _bulletDelta.x;
	pos.y = _bulletDelta.y;

	D_POINT( pos );
	pos = Tools::RotatePointAroundPoint( pos, center, GetRotation() );
	D_POINT( pos );

	pos = pos + GetReversedPosition();
	
	D_POINT( pos );
	return pos;
}
//--------------------------------------------------------------------------------
void puGunBase::DoAnimSpit()
{
	unAssertMsg( Game, false, ("Implement me!") );
}
//--------------------------------------------------------------------------------
void puGunBase::DoAnimSpitEmpty()
{
	unAssertMsg( Game, false, ("Implement me!") );
}



//--------------------------------------------------------------------------------
// Gun
//--------------------------------------------------------------------------------
puGun::puGun()
{
	_blockEnum = eBlockSpitter;

	_scaleToDefault = 1.0f;
	_sprite = AnimManager::Get()->CreateSitSprite( this ); 
	_spriteShadow = AnimManager::Get()->CreateSitSprite( this ); 
	_spriteZOrder = GameTypes::eSpritesAbove;

	CenterOnScreen();
	
	Preloader::Get()->AddAnim( GameTypes::eAnim_SpitterSpit, this );
}
//--------------------------------------------------------------------------------
void puGun::DoAnimSpit()
{
	AnimManager::Get()->PlayAnim( GameTypes::eAnim_SpitterSpit, GameTypes::eAnimPiority_High, this );
}
//--------------------------------------------------------------------------------
void puGun::DoAnimSpitEmpty()
{
	AnimManager::Get()->PlayAnim( GameTypes::eAnim_SpitterSpitEmpty, GameTypes::eAnimPiority_High, this );
}



//--------------------------------------------------------------------------------
// Gun Explosive
//--------------------------------------------------------------------------------
puGunExplosive::puGunExplosive()
{
	_blockEnum = eBlockSpitterExplosive;

	for ( int i = 0; i < Config::GunClipSize; i++ )
		_bullets[i].SetExplosive( true );

	_scaleToDefault = 1.0f;
	_sprite = AnimManager::Get()->CreateSitSprite( this ); 
	_spriteShadow = AnimManager::Get()->CreateSitSprite( this ); 
	_spriteZOrder = GameTypes::eSpritesAbove;
	
	CenterOnScreen();
	
	Preloader::Get()->AddAnim( GameTypes::eAnim_SpitterExplosiveSpit, this );
	Preloader::Get()->AddSound( SoundEngine::eSoundFireworks1 );
	Preloader::Get()->AddSound( SoundEngine::eSoundFireworks2 );
	Preloader::Get()->AddSkinImage( "Images/Particles/Fireworks.png");
}
//--------------------------------------------------------------------------------
void puGunExplosive::DoAnimSpit()
{
	AnimManager::Get()->PlayAnim( GameTypes::eAnim_SpitterExplosiveSpit, GameTypes::eAnimPiority_High, this );
}
//--------------------------------------------------------------------------------
void puGunExplosive::DoAnimSpitEmpty()
{
	AnimManager::Get()->PlayAnim( GameTypes::eAnim_SpitterExplosiveSpitEmpty, GameTypes::eAnimPiority_High, this );
}

//--------------------------------------------------------------------------------
// Gun Sensor
//--------------------------------------------------------------------------------
puGunSensor::puGunSensor() : puCircleBase( 11.0f )
{
	_blockEnum = eBlockGunSensor;
	_shapeType = eShape_Circle;
	_spriteZOrder = GameTypes::eSpritesBeloweX3;

	SetBlockType( GameTypes::eActivable );

	_width = 2.0f * _radius;
	_height = 2.0f * _radius;

	b2CircleShape shape;
	shape.m_radius = _radius;

	b2Filter filter;
	filter.groupIndex	= 0;
	filter.categoryBits = Config::eFilterCategoryGun;
	filter.maskBits		= 0x0;

	b2FixtureDef fixtureDef;
	fixtureDef.shape		= &shape;
	fixtureDef.density		= 0.0f;
	fixtureDef.restitution	= 0.0f;
	fixtureDef.friction		= 0.0f;
	fixtureDef.filter		= filter;

	_body->CreateFixture( &fixtureDef );
	SetBodyType( b2_dynamicBody );

	_sprite = CCSprite::spriteWithFile( "Images/Other/none.png" );
	_body->SetType( b2_dynamicBody );
}
//--------------------------------------------------------------------------------
void puGunSensor::Activate( void *ptr /*= NULL */ )
{
	GetRoot()->Activate();
}
//--------------------------------------------------------------------------------
