#include "puMagnet.h"
#include "puBlocksSettings.h"
#include "Levels/Level.h"
#include "SoundEngine.h"
#include "Blocks/BlockNames.h"

//-----------------------------------------------------------------------------------------------------
// Magnet
//-----------------------------------------------------------------------------------------------------
void puMagnet_::Construct()
{
//	LoadDefaultSprite();
	SetRange( 14.0f );
	_magnetImpulse = 800;

	SetNext( &_blockRange );
	SetBodyType( b2_dynamicBody );

	_blockType = GameTypes::eOther;
   	_blockEnum	= eBlockMagnet;
}
//-------------------------------------------------------------------------------------
void puMagnet_::Step()
{
	if ( _blocksInRange.size() > 0 )
		Magnetize();
}
//-------------------------------------------------------------------------------------
void puMagnet_::Magnetize()
{
	b2Vec2 impulse;

	for ( BlocksSet::iterator it = _blocksInRange.begin(); it != _blocksInRange.end(); it++ )
	{
        CCLog("Block in range: %s (enum: %d)", BlockNames::Get()->GetName( (*it)->GetBlockEnum() ), (*it)->GetBlockEnum() );
        
		if (( 
			(*it)->GetBlockType() != GameTypes::eCoin && 
			(*it)->GetBlockType() != GameTypes::eCoinSilver && 
			(*it)->GetBlockType() != GameTypes::eIron &&
			(*it)->GetBlockEnum() != GameTypes::eBlockBomb &&
			(*it)->GetBlockEnum() != GameTypes::eBlockMagnetBomb &&
			(*it)->GetBlockEnum() != GameTypes::eBlockMagnetBombMinus &&
			(*it)->GetBlockEnum() != GameTypes::eBlockMagnetBombPlus
			)
			||
			( (*it)->GetBody() == NULL || (*it)->GetBody()->GetType() == b2_staticBody ))

			continue;

		impulse = GetImpulseStrength( *it );

		(*it)->GetBody()->ApplyLinearImpulse( -_magnetImpulse / 2 * impulse, (*it)->GetPosition() );
		GetBody()->ApplyLinearImpulse( _magnetImpulse / 2 * impulse, GetPosition() );
	}
}
//-------------------------------------------------------------------------------------
b2Vec2 puMagnet_::GetImpulseStrength( puBlock *block )
{
	b2Vec2 distance;
	b2Vec2 impulseStrenght;

	distance.x = ( block->GetPosition().x - GetPosition().x );
	distance.y = ( block->GetPosition().y - GetPosition().y );

	float nRotation;
	// check for limits and distance.x = 0

	if ( distance.x == 0.0f )
		nRotation = 90.0f;
	else
		nRotation = atan( abs( distance.y )/  abs( distance.x )) * 180.0f / b2_pi;

	if ( distance.x >= 0 && distance.y >=0 )
	{
		nRotation += 0.0f;
	}
	else if ( distance.x < 0 && distance.y >= 0 ) 
	{
		nRotation = 180.0f - nRotation;
	}
	else if ( distance.x < 0 && distance.y < 0 )
	{
		nRotation = 180.0f + nRotation;
	}
	else if ( distance.x >= 0 && distance.y < 0 )
	{
		nRotation = 360.0f - nRotation;
	}

	if (( abs( distance.x ) > _blockRange.GetRange() ) || ( abs( distance.y ) > _blockRange.GetRange() ))
	{
		impulseStrenght.SetZero();
	}
	else
	{
		impulseStrenght.x = ( _blockRange.GetRange() - abs( distance.x )) / _blockRange.GetRange();
		impulseStrenght.y = ( _blockRange.GetRange() - abs( distance.y )) / _blockRange.GetRange();
	}
	
	impulseStrenght.x = impulseStrenght.x * cos( nRotation * b2_pi / 180.0f );
	impulseStrenght.y = impulseStrenght.y * sin( nRotation * b2_pi / 180.0f );
	
	return impulseStrenght;
}






//-----------------------------------------------------------------------------------------------------
// Magnet Minus
//-----------------------------------------------------------------------------------------------------
void puMagnetMinus_::Construct()
{
	_blockEnum	= eBlockMagnetMinus;
	LoadDefaultSprite();
}
//-----------------------------------------------------------------------------------------------------
void puMagnetMinus_::Magnetize()
{
	b2Vec2 impulse;

	for ( BlocksSet::iterator it = _blocksInRange.begin(); it != _blocksInRange.end(); it++ )
	{
		if (( 
			(*it)->GetBlockType() != GameTypes::eCoin && 
			(*it)->GetBlockType() != GameTypes::eCoinSilver && 
			(*it)->GetBlockType() != GameTypes::eIron &&
			(*it)->GetBlockEnum() != GameTypes::eBlockBomb &&
			(*it)->GetBlockEnum() != GameTypes::eBlockMagnetBomb &&
			(*it)->GetBlockEnum() != GameTypes::eBlockMagnetBombPlus &&
			(*it)->GetBlockEnum() != GameTypes::eBlockMagnetBombMinus
			)
			||
			( (*it)->GetBody() == NULL || (*it)->GetBody()->GetType() == b2_staticBody ))

			continue;

		impulse = GetImpulseStrength( *it );

		if ( (*it)->GetBlockEnum() == eBlockMagnetBombMinus )
		{
			(*it)->GetBody()->ApplyLinearImpulse( _magnetImpulse / 2 * impulse, (*it)->GetPosition() );
			GetBody()->ApplyLinearImpulse( -_magnetImpulse / 2 * impulse, GetPosition() );
		}
		else
		{
			(*it)->GetBody()->ApplyLinearImpulse( -_magnetImpulse / 2 * impulse, (*it)->GetPosition() );
			GetBody()->ApplyLinearImpulse( _magnetImpulse / 2 * impulse, GetPosition() );
		}

	}
}




//-----------------------------------------------------------------------------------------------------
// Magnet Plus
//-----------------------------------------------------------------------------------------------------
void puMagnetPlus_::Construct()
{
	_blockEnum	= eBlockMagnetPlus;
	LoadDefaultSprite();
}
//-----------------------------------------------------------------------------------------------------
void puMagnetPlus_::Magnetize()
{
	b2Vec2 impulse;

	for ( BlocksSet::iterator it = _blocksInRange.begin(); it != _blocksInRange.end(); it++ )
	{
		if (( 
			(*it)->GetBlockType() != GameTypes::eCoin && 
			(*it)->GetBlockType() != GameTypes::eCoinSilver && 
			(*it)->GetBlockType() != GameTypes::eIron &&
			(*it)->GetBlockEnum() != GameTypes::eBlockBomb && 
			(*it)->GetBlockEnum() != GameTypes::eBlockMagnetBomb &&
			(*it)->GetBlockEnum() != GameTypes::eBlockMagnetBombPlus && 
			(*it)->GetBlockEnum() != GameTypes::eBlockMagnetBombMinus  
			)
			||
			( (*it)->GetBody() == NULL || (*it)->GetBody()->GetType() == b2_staticBody ))

			continue;

		impulse = GetImpulseStrength( *it );

		if ( (*it)->GetBlockEnum() == GameTypes::eBlockMagnetBombPlus )
		{
			(*it)->GetBody()->ApplyLinearImpulse( _magnetImpulse / 2 * impulse, (*it)->GetPosition() );
			GetBody()->ApplyLinearImpulse( -_magnetImpulse / 2 * impulse, GetPosition() );
		}
		else
		{
			(*it)->GetBody()->ApplyLinearImpulse( -_magnetImpulse / 2 * impulse, (*it)->GetPosition() );
			GetBody()->ApplyLinearImpulse( _magnetImpulse / 2 * impulse, GetPosition() );
		}

	}
}
