//#ifndef __PUQUADRANGLE_H__
//#define __PUQUADRANGLE_H__
//
//#include "puBlock.h"
//
//typedef enum {
//	eQuadrangle = 1,
//	eQuadrangleFragile,
//	eQuadrangleStone,
//	eQuadrangleWall,
//	eQuadrangleWood,
//	eQuadrangleEqual,
//	eQuadrangleEqualWood
//} PUQuadrangleTypes;
//
////---------------------------------------------------------------------------------------------------------
//class puQuadrangleBase : public puBlock
//{
//public:
//	puQuadrangleBase( const b2Vec2& t1, const b2Vec2& t2, const b2Vec2& t3 );
//	void LoadDefaultSprite();
//
//	b2Vec2 GetT1(){ return _tv[0]; }
//	b2Vec2 GetT2(){ return _tv[1]; }
//	b2Vec2 GetT3(){ return _tv[2]; }
//
//protected:
//	b2Vec2	_tv[3];
//};
////---------------------------------------------------------------------------------------------------------
//class puTriangle_ : public puTriangleBase
//{
//public:
//	puTriangle_( const b2Vec2& t1, const b2Vec2& t2, const b2Vec2& t3 );
//	virtual string GetClassName() { return string("Triangle"); }
//};
////---------------------------------------------------------------------------------------------------------
//class puTriangleFragile_ : public puTriangleBase
//{
//public:
//	puTriangleFragile_( const b2Vec2& t1, const b2Vec2& t2, const b2Vec2& t3 );
//	virtual string GetClassName() { return string("TriangleFragile"); }
//};
////---------------------------------------------------------------------------------------------------------
//class puTriangleWall_ : public puTriangleBase
//{
//public:
//	puTriangleWall_( const b2Vec2& t1, const b2Vec2& t2, const b2Vec2& t3 );
//	virtual string GetClassName() { return string("TriangleWall"); }
//};
////---------------------------------------------------------------------------------------------------------
//
//class puTriangleEqual_ : public puTriangleBase
//{
//public:
//	puTriangleEqual_( const b2Vec2& t1, const b2Vec2& t2, const b2Vec2& t3 );
//	puTriangleEqual_( float sideSize );
//	virtual string GetClassName() { return string("TriangleEqual"); }
//
//};
//
//#endif
