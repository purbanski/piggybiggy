#include "puScale.h"

//---------------------------------------------------------------------------------------------
puScale_::puScale_( float width, float height ) : puWoodBox_( width, height )
{
	// SetNodeChain
	SetNodeChain( &_screw1, NULL );

	// SetDeltaXY
	_screw1.SetDeltaXY( 0.0f, 0.0f );

	// Main block (Wood)
	SetBodyType( b2_dynamicBody );

	SetPosition( 0.0f, 0.0f );
	CreateJoints();
}
//---------------------------------------------------------------------------------------------
void puScale_::CreateJoints()
{
	float anchorX;
	float anchorY;

	anchorX = _screw1.GetPosition().x;
	anchorY = _screw1.GetPosition().y;

	b2RevoluteJointDef jointDef1;
	jointDef1.Initialize( GetBody(), _screw1.GetBody(), b2Vec2( anchorX, anchorY ));
	_joint1 = (b2RevoluteJoint *) _world->CreateJoint( &jointDef1 );

	_joint1->SetLimits( 0.0f, 0.0f );
	_joint1->EnableLimit( false );
	_joint1->EnableMotor( false );
	_joint1->SetMotorSpeed( 0.0f );
	_joint1->SetMaxMotorTorque( 0.0f );
}
//---------------------------------------------------------------------------------------------
void puScale_::DestroyJoints()
{
	_world->DestroyJoint( _joint1 );
	_joint1 = NULL;
}
//---------------------------------------------------------------------------------------------
