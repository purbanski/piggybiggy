#ifndef __PUEDGES_H__
#define __PUEDGES_H__

#include "puBlock.h"
#include <list>

using namespace std;

class EdgesDraw;

//--------------------------------------------------------------------------
class puEdges : public puBlock
{
public:
	struct Edge
	{
		Edge()
		{
			_xa = 0.0f;
			_ya = 0.0f;
			_xb = 0.0f;
			_yb = 0.0f;
		}

		Edge( float xa, float ya, float xb, float yb )
		{
			_xa = xa;
			_ya = ya;
			_xb = xb;
			_yb = yb;
		}
		float _xa, _ya;
		float _xb, _yb;
	};

	typedef list<Edge> Edges;

public:
	puEdges();

	void SetEdges( Edges *edges, float scale );

private:
	EdgesDraw	*_edgesDraw;
	Edges		*_edges;
};
//--------------------------------------------------------------------------
class EdgesDraw : public CCNode
{
public:
	EdgesDraw();

	virtual void draw();
	void SetEdges( puEdges::Edges *edges, float scale );

private:
	puEdges::Edges	*_edges;
	float			_scale;
};
//--------------------------------------------------------------------------
#endif

