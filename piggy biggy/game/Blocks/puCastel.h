#ifndef __PUCASTEL_H__
#define __PUCASTEL_H__

#include <Box2D/Box2D.h>
#include "puCircle.h"

//-----------------------------------------------------------------

class puCastel : public puCircle<2>
{
public:
	puCastel();
	~puCastel();

};

//-----------------------------------------------------------------

#endif
