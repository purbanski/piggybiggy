#include "puBlockWithRange.h"
#include "Levels/Level.h"

//---------------------------------------------------------------------------------------------------
// Block Range
//---------------------------------------------------------------------------------------------------
puBlockRange::puBlockRange() : puCircleBase( 5.0f )
{
	_width = 2.0f * _radius;
	_height = 2.0f * _radius;
	_blockType = GameTypes::eBlockRange;
	_blockEnum = GameTypes::eBlockBombRange;

#ifdef BUILD_EDITOR
	SetSpriteZOrder( GameTypes::eSpritesBlockRange );
	LoadDefaultSprite();
	_sprite->setOpacity( 40 );
#else
	SetEmptySprite();
#endif

	SetRange( _radius );

	SetBodyType( b2_dynamicBody );
	
}
//---------------------------------------------------------------------------------------------------
void puBlockRange::SetRange( float radius )
{
	b2Fixture *fixture;
	fixture = _body->GetFixtureList();

	if ( fixture )
	{
		_body->DestroyFixture( fixture );
	}

	_radius = radius;

	b2CircleShape shape;
	shape.m_radius = radius;

	b2FixtureDef fixtureDef;
	fixtureDef.shape = &shape;
	fixtureDef.density = 0.0f;
	fixtureDef.restitution = 0.0f;
	fixtureDef.friction = 0.0f;
	fixtureDef.isSensor = true;

	_body->CreateFixture( &fixtureDef );

	// rescale range image
	float scalex = ( 2.0f * _radius / 150.0f ) * RATIO ;
	float scaley = ( 2.0f * _radius / 150.0f ) * RATIO ; 

	D_FLOAT( _sprite->getScaleX() )
	D_FLOAT( _sprite->getScaleY() )

	_sprite->setScaleX( scalex );
	_sprite->setScaleY( scaley );
}
//-----------------------------------------------------------------------------------------------------
void puBlockRange::Destroy()
{
	CCFadeTo *fade;

	fade = new CCFadeTo();
	fade->autorelease();
	fade->initWithDuration( 0.2f, 0 );

	_sprite->stopAllActions();
	_sprite->runAction( fade );
	_sprite = NULL;
	DestroyBody();
}



//-----------------------------------------------------------------------------------------------------
// Block with range
//-----------------------------------------------------------------------------------------------------
void puBlockWithRange_::Construct( float radius )
{
    _quiting = false;
	_width = 2.0f * radius;
	_height = 2.0f * radius;

	SetSpriteZOrder( GameTypes::eSpritesBlockWithRange );

	b2Filter filter;
	filter.groupIndex = -1;
	filter.categoryBits = 0;
	filter.maskBits = 0;

	b2CircleShape shape;

	b2FixtureDef fixtureDef;
	fixtureDef.shape = &shape;
	fixtureDef.density = pu::blocks::settings::BombDensity;
	fixtureDef.restitution = pu::blocks::settings::BombRestitution;
	fixtureDef.friction = pu::blocks::settings::BombFriction;

	shape.m_radius = radius;
	_body->CreateFixture( &fixtureDef );

	SetRange( 11.0f );

	_blockRange.SetDeltaXY( 0.0f, 0.0f );
	SetNodeChain( &_blockRange, NULL );

	SetBodyType( b2_dynamicBody );
	_blockType = GameTypes::eOther;

	//-------------
	// joints
	//-------------
	b2RevoluteJointDef revJointDef;

	revJointDef.collideConnected = false;
	revJointDef.Initialize( _body, _blockRange.GetBody(), GetPosition() );

	_level->GetWorld()->CreateJoint( &revJointDef );
}
//-------------------------------------------------------------------------------------
puBlockWithRange_::~puBlockWithRange_()
{
    _blocksInRange.clear();
    _blocksInRangeMulti.clear();
}
//-------------------------------------------------------------------------------------
void puBlockWithRange_::BlockRangeIn( puBlock *block )
{
    if ( _quiting )
        return;
    
	_blocksInRangeMulti.insert( block );
	
	_blocksInRange.clear();
	for ( BlocksMultiSet::iterator it = _blocksInRangeMulti.begin(); it != _blocksInRangeMulti.end(); it++ )
		_blocksInRange.insert( *it );

}
//-------------------------------------------------------------------------------------
void puBlockWithRange_::BlockRangeOut( puBlock *block )
{
    if ( _quiting )
        return;
    
	if ( _blocksInRangeMulti.size() < 1 )
		return;

	BlocksMultiSet::iterator it;

	it = _blocksInRangeMulti.find( block );

	if ( it != _blocksInRangeMulti.end() )
		_blocksInRangeMulti.erase( it );

	_blocksInRange.clear();
	for ( BlocksMultiSet::iterator it = _blocksInRangeMulti.begin(); it != _blocksInRangeMulti.end(); it++ )
		_blocksInRange.insert( *it );

}
//-------------------------------------------------------------------------------------
void puBlockWithRange_::LevelQuiting()
{
    _quiting = true;
}
