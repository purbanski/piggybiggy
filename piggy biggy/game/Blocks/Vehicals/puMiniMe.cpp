#include <Box2D/Box2D.h>
#include "cocos2d.h"
#include <math.h>

#include "Levels/World.h"
#include "Levels/Level.h"
#include "puMiniMe.h"
#include "Tools.h"
#include "Debug/MyDebug.h"
#include "SoundEngine.h"

USING_NS_CC;

const float puVehical::FORCE_MAX = 60.0f * 15000.0f;
const float puVehical::FORCE_STEP = 1.0f * 1.0f;
const float puVehical::FORCE_INIT = 60.0f * 8.5f;
const float puVehical::UPTO_BREAKING_SPEED_INIT = 2.0f;
const float puVehical::OUTOF_BREAKING_SPEED = 1.0f;
const float puVehical::FORCE_FACTOR = 0.6f;

const int	puVehical::BREAKING_STEP_COUNT = 30;

//----------------------------------------------------------------------------------------------
// MiniMe
//----------------------------------------------------------------------------------------------
puVehical::puVehical()
{
	// Main block
	_blockEnum = eBlockVehical;

	b2PolygonShape shape;
	shape.SetAsBox( 3.8f, 0.8f );

	b2FixtureDef fixtureDef;
	fixtureDef.shape = &shape;
	fixtureDef.density = 5.5f;
	fixtureDef.restitution = 0.2f;
	fixtureDef.friction = 0.5f;

	_body->CreateFixture( &fixtureDef );
	SetBlockType( GameTypes::eOther );
	SetBodyType( b2_dynamicBody );

	LoadDefaultSprite();

	// _circleWood1
	_backWheel.GetBody()->GetFixtureList()->SetDensity( 5.5f );
	_backWheel.GetBody()->GetFixtureList()->SetFriction( 0.95f );
	_backWheel.GetBody()->GetFixtureList()->SetRestitution( 0.2f );
	_backWheel.GetBody()->SetAngularDamping( 0.5f );
	_backWheel.SetBodyType( b2_dynamicBody );

	// _circleWood2
	_frontWheel.GetBody()->GetFixtureList()->SetDensity( 5.5f );
	_frontWheel.GetBody()->GetFixtureList()->SetFriction( 0.95f );
	_frontWheel.GetBody()->GetFixtureList()->SetRestitution( 0.2f );
	_frontWheel.GetBody()->SetAngularDamping( 0.5f );
	_frontWheel.SetBodyType( b2_dynamicBody );

	// SetDeltaXY
	_backWheel.SetDeltaXY( -2.8f, 0.0f );
	_frontWheel.SetDeltaXY( 2.8f, 0.0f );
	//_controls.SetDeltaXY( 0.0f, 0.0f );

	// SetNodeChain
	SetNodeChain( &_controls, &_backWheel, &_frontWheel, NULL );
	//_frontWheel.SetNext( &_controls );

	// Main block (CircleWood)

	SetBlockType( GameTypes::eVehical );
	_frontWheel.SetBlockType( GameTypes::eVehical );
	_backWheel.SetBlockType( GameTypes::eVehical );

	_force = FORCE_INIT;
	_torque = 0.0f;
	_state = eStateNone;
	_stateBak = eStateNone;
	_stateChanged = false;
	_naturalRotation = 8.1f;

	_controls.SetVehicle( this );

	Preloader::Get()->AddSound( SoundEngine::eSoundCarBreaking1 );
	Preloader::Get()->AddSound( SoundEngine::eSoundCarBreaking2 );
	Preloader::Get()->AddSound( SoundEngine::eSoundCarBreaking3 );
	Preloader::Get()->AddSound( SoundEngine::eSoundCarStartAndRun );

	puVehical::SetPosition( Config::GameSize.width / 2.0f / RATIO, Config::GameSize.height / 2.0f / RATIO );
	CreateJoints();
}
//-----------------------------------------------------------------------------------------------------
void puVehical::CreateJoints()
{
	b2RevoluteJointDef jointDef1;
	jointDef1.Initialize( _backWheel.GetBody(), GetBody(), _backWheel.GetPosition() );
	_joint1 = (b2RevoluteJoint *) _world->CreateJoint( &jointDef1 );

	_joint1->EnableMotor( true );
	_joint1->SetMotorSpeed( 0.0f );
	_joint1->SetMaxMotorTorque( 10.0f );

	b2RevoluteJointDef jointDef2;
	jointDef2.Initialize( _frontWheel.GetBody(), GetBody(), _frontWheel.GetPosition() );
	_joint2 = (b2RevoluteJoint *) _world->CreateJoint( &jointDef2 );

	_joint2->EnableMotor( true );
	_joint2->SetMotorSpeed( 0.0f );
	_joint2->SetMaxMotorTorque( 10.0f );
}
//-----------------------------------------------------------------------------------------------------
void puVehical::DestroyJoints()
{
	_world->DestroyJoint( _joint1 );
	_world->DestroyJoint( _joint2 );
	
	_joint1 = NULL;
	_joint2 = NULL;
}
//-----------------------------------------------------------------------------------------------------
puVehical::~puVehical()
{
}
//-----------------------------------------------------------------------------------------------------
void puVehical::SetState( VehicalStates state )
{
	_state = state;
	_stateChanged =  true;
	_carStartSoundPlayed = false;
}
//-----------------------------------------------------------------------------------------------------
void puVehical::Update()
{
	switch (_state )
	{
	case eStateGoRight :
		GoRight();
		break;
		
	case eStateGoLeft :
		GoLeft();
		break;

	case eStateNone :
		_force = 0.0f;
		break;

	case eStateBreaking :
		Breaking();
		break;
	}
}
//-----------------------------------------------------------------------------------------------------
void puVehical::GoRight()
{
	if ( _stateChanged )
	{
		_stateChanged = false;
		if ( _backWheel.GetBody()->GetAngularVelocity() > UPTO_BREAKING_SPEED_INIT  )
		{
			Breaking();
			_force = -FORCE_INIT;
			return;
		}
	}
	//D_LOG("Go right" );
	if ( ! _carStartSoundPlayed )
	{
		SoundEngine::Get()->PlayEffect_CarStartAndRun();
		_carStartSoundPlayed = true;
	}

	//very needed here!
	if ( _force >= 0 ) _force = -FORCE_INIT;

	if ( _force > -FORCE_MAX )
	{
		_force -= FORCE_STEP;
	}
	Accelerate();
}
//-----------------------------------------------------------------------------------------------------
void puVehical::GoLeft()
{
	if ( _stateChanged )
	{
		_stateChanged = false;
		if ( _backWheel.GetBody()->GetAngularVelocity() < -UPTO_BREAKING_SPEED_INIT  )
		{
			Breaking();
			_force = -FORCE_INIT;
			return;
		}
	}
	//D_LOG("Go left" );
	if ( ! _carStartSoundPlayed )
	{
		SoundEngine::Get()->PlayEffect_CarStartAndRun();
		_carStartSoundPlayed = true;
	}


	//very needed here!
	if ( _force <= 0 ) _force = FORCE_INIT;

	if ( _force < FORCE_MAX )
	{
		_force += FORCE_STEP;
	}
	Accelerate();
}
//-----------------------------------------------------------------------------------------------------
void puVehical::Breaking()
{
	if ( _state == eStateGoLeft || _state == eStateGoRight )
	{
		_stateBak = _state;
		_state = eStateBreaking;
		_breakingCount = BREAKING_STEP_COUNT;

		SoundEngine::Get()->StopEffect_CarStartAndRun();
		SoundEngine::Get()->PlayRandomEffect( 
			10.0f,
			SoundEngine::eSoundCarBreaking1, 
			SoundEngine::eSoundCarBreaking2, 
			SoundEngine::eSoundCarBreaking3, 
			SoundEngine::eSoundNone );
	}

	//D_LOG("fabs: %f", fabs( _neutralWheel.GetBody()->GetAngularVelocity()));
	//if ( fabs( _frontWheel.GetBody()->GetAngularVelocity())  > UPTO_BREAKING_SPEED  )
	if ( _breakingCount-- && fabs( GetSpeed()) > OUTOF_BREAKING_SPEED )
	{
		D_LOG("Breaking (break count: %d, speed: %f )", _breakingCount, GetSpeed() );
		_backWheel.GetBody()->SetAngularVelocity( 0.0f );
		_frontWheel.GetBody()->SetAngularVelocity( 0.0f );
	}
	else
	{
		// breaking completed
		if ( _stateBak == eStateGoLeft || _stateBak == eStateGoRight )
			_state = _stateBak;
	}
}
//-----------------------------------------------------------------------------------------------------
float puVehical::GetSpeed()
{
	float speedx = GetBody()->GetLinearVelocity().x * cos(( GetRotation() + _naturalRotation ) * b2_pi / 180.0f );// + GetBody()->GetLinearVelocity().y * sin( GetRotation()) ;
	float speedy = GetBody()->GetLinearVelocity().y * sin(( GetRotation() + _naturalRotation ) * b2_pi / 180.0f );// + GetBody()->GetLinearVelocity().y * sin( GetRotation()) ;

	return speedx + speedy;
}
//-----------------------------------------------------------------------------------------------------
void puVehical::Accelerate()
{
	_backWheel.GetBody()->ApplyAngularImpulse( _force * FORCE_FACTOR );
	_frontWheel.GetBody()->ApplyAngularImpulse( _force * FORCE_FACTOR );

	float delta;
	delta = _frontWheel.GetBody()->GetAngularVelocity() + _backWheel.GetBody()->GetAngularVelocity();
	delta /= 2.0f;

	
	SoundEngine::Get()->AdjustEffect_CarStartAndRun( delta );
}
//-----------------------------------------------------------------------------------------------------
b2Joint* puVehical::GetWheelJoint( puBlock *wheel )
{
	if ( wheel == &_frontWheel )
		return _joint1;

	if ( wheel == &_backWheel )
		return _joint2;

	unAssertMsg(puVehical, false, ("This shouldn't happen"));
	return NULL;
}
//----------------------------------------------------------------------------------------------
void puVehical::SetPosition( float x, float y )
{
	_controls.UpdatePositionDelta( -x, -y );
	puBlock::SetPosition( x, y );
	//SetControlsPosition();
}
//----------------------------------------------------------------------------------------------
void puVehical::SetPosition( const b2Vec2& position )
{
	puVehical::SetPosition( position.x, position.y );
}
//----------------------------------------------------------------------------------------------
void puVehical::SetControlsPosition()
{
	
	_controls.SetPosition( Config::GameSize.width / RATIO / 2.0f, -puVehcialButtonBase::BUTTON_RADIUS / RATIO );
}
//-----------------------------------------------------------------------------------------------------





//----------------------------------------------------------------------------------------------
// MiniMeControls
//----------------------------------------------------------------------------------------------
puVehcialControls::puVehcialControls() : puCircleBase( 10 )
{
	_blockEnum = eBlockVehicalControls;
	_shapeType = eShape_Circle;
	_spriteZOrder = GameTypes::eSpritesBeloweX3;

	_width = 2.0f * _radius;
	_height = 2.0f * _radius;

	b2CircleShape shape;
	shape.m_radius = _radius;

	b2Filter filter;
	filter.groupIndex = 0;
	filter.categoryBits = 0;
	filter.maskBits = 0;

	b2FixtureDef fixtureDef;
	fixtureDef.shape		= &shape;
	fixtureDef.density		= CoinDensity;
	fixtureDef.restitution	= CoinRestitution;
	fixtureDef.friction		= CoinFriction;
	fixtureDef.filter		= filter;

	_body->CreateFixture( &fixtureDef );
	SetBodyType( b2_dynamicBody );

	GetBody()->GetFixtureList()->SetFilterData( filter );
	GetBody()->GetFixtureList()->SetDensity( 0.0f );
	GetBody()->GetFixtureList()->SetRestitution( 0.0f );
	GetBody()->GetFixtureList()->SetFriction( 0.0f );

	SetBodyType( b2_staticBody );
	SetBlockType( GameTypes::eOther );

	// SetDeltaXY
	UpdatePositionDelta( 0.0f, 0.0f );

	// SetNodeChain
	SetNodeChain( &_buttonL, &_buttonR, NULL );

	SetPosition( 24.0f, -12.0f );
	_spriteZOrder = GameTypes::eSpritesControls;
	SetEmptySprite();
}
//-----------------------------------------------------------------------------------------------------
puVehcialControls::~puVehcialControls()
{
}
//-----------------------------------------------------------------------------------------------------
void puVehcialControls::SetVehicle( puVehical *vehicle )
{
	_buttonL.SetVehicle( vehicle );
	_buttonR.SetVehicle( vehicle );
}
//-----------------------------------------------------------------------------------------------------
void puVehcialControls::UpdatePositionDelta( float x, float y )
{
	float moveY;
	moveY = -40.0f;

	_deltaX = x + Config::GameSize.width / 2.0f / RATIO;
	_deltaY = y + puVehcialButtonBase::BUTTON_RADIUS / RATIO + moveY;
	
	_buttonL.SetDeltaXY( _deltaX -Config::GameSize.width / 2.0f / RATIO + puVehcialButtonBase::BUTTON_RADIUS / 10.0f * 1.0f , _deltaY - moveY );
	_buttonR.SetDeltaXY( _deltaX + Config::GameSize.width / 2.0f / RATIO - ( puVehcialButtonBase::BUTTON_RADIUS / 10.0f * 1.0f ) , _deltaY - moveY );
}
//-----------------------------------------------------------------------------------------------------



//----------------------------------------------------------------------------------------------
// MiniMeButton
//----------------------------------------------------------------------------------------------
puVehcialButtonBase::puVehcialButtonBase()
{
	_activated = false;

	b2CircleShape circle;
	circle.m_radius = puVehcialButtonBase::BUTTON_RADIUS / 10.0f;

	b2Filter filter;
	filter.groupIndex = -1;
	filter.categoryBits = 0;

	b2FixtureDef fixtureDef;
	fixtureDef.shape = &circle;
	fixtureDef.density = 20.0f;
	fixtureDef.friction = 0.9f;
	fixtureDef.restitution = 0.1f;
	fixtureDef.filter = filter;

	_body->CreateFixture( &fixtureDef );

	SetBlockType( GameTypes::eActivable );
	SetBodyType( b2_staticBody );
	SetPosition( 0.0f, 0.0f );

	_spriteZOrder = GameTypes::eSpritesControls;
    
}
//----------------------------------------------------------------------------------------------
void puVehcialButtonBase::ConstFinal()
{
	_spriteZOrder = GameTypes::eSpritesControls;
	_spriteON->setIsVisible( false );

    //-------
    float fadeDur;
    CCAction *action;
    CCSprite *blinkSprite;
//    CCScene *scene;

    fadeDur = 0.35f;
//    scene = CCDirector::sharedDirector()->getRunningScene();
    
    blinkSprite = CCSprite::spriteWithTexture(_spriteON->getTexture());
    blinkSprite->setOpacity( 0 );
    action = CCSequence::actions(
                                 CCFadeTo::actionWithDuration(fadeDur, 255 ),
                                 CCFadeTo::actionWithDuration(fadeDur, 50 ),
                                 CCFadeTo::actionWithDuration(fadeDur, 255 ),
//                                CCCallFuncND::actionWithTarget( scene, callfuncND_selector( SoundEngine::PlayEffectCallback ), (void*)(int) SoundEngine::eSoundCarBreaking1 ),
                                 CCFadeTo::actionWithDuration(fadeDur, 50 ),
                                 CCFadeTo::actionWithDuration(fadeDur, 255 ),
//                                 CCCallFuncND::actionWithTarget( scene, callfuncND_selector( SoundEngine::PlayEffectCallback ), (void*)(int) SoundEngine::eSoundCarBreaking1 ),
                                 CCCallFuncND::actionWithTarget(blinkSprite,
                                                              callfuncND_selector( puVehcialButtonBase::BlinkFinish), (void*) this),
                                CCFadeTo::actionWithDuration(fadeDur, 0 ),
                                NULL );
    _sprite = blinkSprite;
    _sprite->runAction(action);
}
//----------------------------------------------------------------------------------------------
puVehcialButtonBase::~puVehcialButtonBase()
{	
	if ( _spriteON && _spriteON->getParent() ) 
		_spriteON->removeFromParentAndCleanup( true );
	
	if ( _spriteOFF && _spriteOFF->getParent() ) 
		_spriteOFF->removeFromParentAndCleanup( true );

	_spriteON->release();
	_spriteOFF->release();
}
//----------------------------------------------------------------------------------------------
void puVehcialButtonBase::Activate( void *ptr /*= NULL */ )
{
    _activated = true;
	_sprite = _spriteON;
	_spriteON->setIsVisible( true );
    _spriteON->setOpacity( 255 ); // just in case it did not finished fade in animation at BlinkFinish
	_spriteOFF->setIsVisible( false );
}
//----------------------------------------------------------------------------------------------
void puVehcialButtonBase::Deactivate( void *ptr /*= NULL */ )
{
	_vehicle->SetState( puVehical::eStateNone );
	_sprite = _spriteOFF;
	_spriteOFF->setIsVisible( true );
    _spriteOFF->setOpacity( 255 ); // just in case it did not finished fade in animation at BlinkFinish
	_spriteON->setIsVisible( false );
	SoundEngine::Get()->StopEffect_CarStartAndRun();
	_activated = false;
}
//----------------------------------------------------------------------------------------------
void puVehcialButtonBase::LevelFinished()
{
	if ( _activated )
		Deactivate( NULL );
}
//----------------------------------------------------------------------------------------------
void puVehcialButtonBase::BlinkFinish(CCNode *node, void *data)
{
    puVehcialButtonBase *button;
    button = (puVehcialButtonBase*) data;
    
    if ( button->_activated )
    {
        button->_sprite = button->_spriteON;
        button->_spriteOFF->setIsVisible( false );

        button->_spriteON->setIsVisible( true );
        button->_spriteON->setOpacity(0);
        button->_spriteON->runAction(CCFadeTo::actionWithDuration(0.35f, 255));
    }
    else
    {
        button->_sprite = button->_spriteOFF;
        button->_spriteON->setIsVisible( false );

        button->_spriteOFF->setIsVisible( true );
        button->_spriteOFF->setOpacity(0);
        button->_spriteOFF->runAction(CCFadeTo::actionWithDuration(0.35f, 255));
    }
}
//----------------------------------------------------------------------------------------------




//----------------------------------------------------------------------------------------------
// MiniMeButtonLeft
//----------------------------------------------------------------------------------------------
puVehicalButtonLeft::puVehicalButtonLeft()
{
	_blockEnum = eBlockVehicalButtonLeft;

	_spriteON = CCSprite::spriteWithFile( Skins::GetSkinName( "Images/Blocks/VehicalButtonLeftON.png" ) );
	_spriteOFF = CCSprite::spriteWithFile( Skins::GetSkinName( "Images/Blocks/VehicalButtonLeftOFF.png" ) );
	_spriteON->retain();
	_spriteOFF->retain();
    
    ConstFinal();
}
//----------------------------------------------------------------------------------------------
void puVehicalButtonLeft::Activate( void *ptr )
{
	_vehicle->SetState( puVehical::eStateGoLeft );
	puVehcialButtonBase::Activate( ptr );
}
//----------------------------------------------------------------------------------------------



//----------------------------------------------------------------------------------------------
// MiniMeButtonRight
//----------------------------------------------------------------------------------------------
puVehicalButtonRight::puVehicalButtonRight()
{
	_blockEnum = eBlockVehicalButtonRight;

	_spriteON = CCSprite::spriteWithFile( Skins::GetSkinName( "Images/Blocks/VehicalButtonRightON.png" ) );
	_spriteOFF = CCSprite::spriteWithFile( Skins::GetSkinName( "Images/Blocks/VehicalButtonRightOFF.png" ) );

	_spriteON->retain();
	_spriteOFF->retain();
    
    ConstFinal();
}
//----------------------------------------------------------------------------------------------
void puVehicalButtonRight::Activate( void *ptr )
{
	_vehicle->SetState( puVehical::eStateGoRight );
	puVehcialButtonBase::Activate( ptr );
}
//----------------------------------------------------------------------------------------------
