#ifndef __PUMINIME_H__
#define __PUMINIME_H__

#include "../AllBlocks.h"

class puVehical;
//--------------------------------------------------------------------------------------------------------
class puVehcialButtonBase : public puBlock
{
public:
	static const int BUTTON_RADIUS = 75;
	
	virtual ~puVehcialButtonBase();
	void SetVehicle( puVehical *vehicle ) { _vehicle = vehicle; }

	virtual void Activate( void *ptr = NULL );
	virtual void Deactivate( void *ptr = NULL );
	virtual void LevelFinished();

protected:
	puVehcialButtonBase();
    void ConstFinal();
    void BlinkFinish( CCNode *node, void *data );

protected:
	puVehical	*_vehicle;
	bool		_activated;
	CCSprite	*_spriteOFF;
	CCSprite	*_spriteON;
};
//--------------------------------------------------------------------------------------------------------
class puVehicalButtonLeft : public puVehcialButtonBase
{
public:
	puVehicalButtonLeft();
	virtual ~puVehicalButtonLeft(){}

	virtual void Activate( void *ptr = NULL );
};
//--------------------------------------------------------------------------------------------------------
class puVehicalButtonRight : public puVehcialButtonBase
{
public:
	puVehicalButtonRight();
	virtual ~puVehicalButtonRight(){}

	virtual void Activate( void *ptr = NULL );
};
//--------------------------------------------------------------------------------------------------------
class puVehcialControls : public puCircleBase
{
public:
	puVehcialControls();
	virtual ~puVehcialControls();

	void SetVehicle( puVehical *vehicle );

	void UpdatePositionDelta( float x, float y );

private:
	puVehicalButtonLeft		_buttonL;
	puVehicalButtonRight	_buttonR;
};
//--------------------------------------------------------------------------------------------------------
class puVehical : public puBlock
{
public:
	typedef enum
	{
		eStateGoRight = 1,
		eStateGoLeft,
		eStateBreaking,
		eStateNone
	} VehicalStates;

	puVehical();
	virtual ~puVehical();

	virtual void CreateJoints();
	virtual void DestroyJoints();

	void Update();
	void SetState( VehicalStates state );
	puBlock* GetFrontWheel(){ return &_frontWheel; }
	puBlock* GetBackWheel(){ return &_backWheel; }
	b2Joint* GetWheelJoint( puBlock *wheel );

	virtual void SetPosition( float x, float y );
	virtual void SetPosition( const b2Vec2& position );

private:
	void SetControlsPosition();
	void GoRight();
	void GoLeft();
	void Accelerate();
	void Breaking();

	float GetSpeed();

private:
	puTyreA<22>		_backWheel;
	puTyreA<22>		_frontWheel;
	puVehcialControls	_controls;

	b2RevoluteJoint	*_joint1;
	b2RevoluteJoint	*_joint2;

	static const float	FORCE_MAX;
	static const float	FORCE_INIT;
	static const float	FORCE_STEP;
	static const float	FORCE_FACTOR;
	static const float	UPTO_BREAKING_SPEED_INIT;
	static const float	OUTOF_BREAKING_SPEED;
	static const int	BREAKING_STEP_COUNT;

	float _force;
	float _torque;
	float _naturalRotation;

	VehicalStates	_state;
	VehicalStates	_stateBak;
	bool			_stateChanged;
	bool			_carStartSoundPlayed;
	int				_breakingCount;
};
//--------------------------------------------------------------------------------------------------------

#endif
