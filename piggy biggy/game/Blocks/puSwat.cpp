#include "puSwat.h"
#include "Game.h"
#include <Box2D/Box2D.h>

puSwat::puSwat()
{
	Construct();
}
//-----------------------------------------------------------------------------------------------------
void puSwat::Construct()
{
	b2CircleShape shape;
	shape.m_radius = 2.4f;
	shape.m_p = b2Vec2( 0, 0 );

	b2FixtureDef fixtureDef;
	fixtureDef.shape = &shape;
	fixtureDef.density = 3.0f;
	fixtureDef.friction = 0.5f; // tarcie 0 - nie ma 1- max
	fixtureDef.restitution = 0.6f; // bouncy

	_body->CreateFixture( &fixtureDef );
	
	_body->SetType( b2_dynamicBody );
	_blockType = GameTypes::eSwat;

	_sprite = CCSprite::spriteWithFile( Skins::GetSkinName( "Images/Blocks/Swat.png" ) );
}
//-----------------------------------------------------------------------------------------------------
puSwat::~puSwat()
{
}
