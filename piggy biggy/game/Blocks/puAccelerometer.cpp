#include "puAccelerometer.h"
//------------------------------------------------------------------------------
puAccelBall::puAccelBall()
{
    _sprite = CCSprite::spriteWithFile(Skins::GetSkinName("Images/Blocks/AccelBall.png"));
	_sprite = SpriteWithRay::Create( _sprite, 2, "AccelBallRay" );
    
    _body->GetFixtureList()->SetDensity( 9.0000f );
	_body->GetFixtureList()->SetFriction( 0.7000f );
	_body->GetFixtureList()->SetRestitution( 0.300f );
    
    b2Filter filter;
    filter.maskBits = 0;
    filter.categoryBits = Config::eFilterCategoryAccelIcon;
    filter.groupIndex = Config::eFilterCategoryGroupAccelIcon;
    
    _body->GetFixtureList()->SetFilterData(filter);
//    _body->SetAngularDamping( 0.3f );
    
	SetBodyType( b2_dynamicBody );
    _spriteZOrder = eSpritesWallAbove;
}
//------------------------------------------------------------------------------
puAccelWall::puAccelWall()
{
//    _sprite = CCSprite::spriteWithFile(Skins::GetSkinName("Images/Blocks/AccelWall.png"));
    SetEmptySprite();
    _body->GetFixtureList()->SetDensity( 8.0000f );
	_body->GetFixtureList()->SetFriction( 0.8000f );
	_body->GetFixtureList()->SetRestitution( 0.1000f );
	SetBodyType( b2_staticBody );
//    _spriteZOrder = eSpritesBeloweX2;

    b2Filter filter;
    filter.maskBits = 0;
    filter.categoryBits = Config::eFilterCategoryAccelIcon;
    filter.groupIndex = Config::eFilterCategoryGroupAccelIcon;

    _body->GetFixtureList()->SetFilterData(filter);
}
//------------------------------------------------------------------------------
puAccelBg::puAccelBg()
{
    _sprite = CCSprite::spriteWithFile(Skins::GetSkinName("Images/Blocks/AccelBg.png"));
	SetBodyType( b2_staticBody );
    _spriteZOrder = eSpritesWallAboveX2;

    b2Filter filter;
    filter.maskBits = 0;
    filter.categoryBits = 0;
    filter.groupIndex = 0;

    _body->GetFixtureList()->SetFilterData(filter);
}
//------------------------------------------------------------------------------
puAccelIcon::puAccelIcon()
{
	_wallBox1.SetDeltaRotation( 198.0000f );
	_wallBox2.SetDeltaRotation( 234.0000f );
	_wallBox3.SetDeltaRotation( 252.0000f );
	_wallBox4.SetDeltaRotation( 270.0000f );
	_wallBox5.SetDeltaRotation( 216.0000f );
	_wallBox6.SetDeltaRotation( 144.0000f );
	_wallBox7.SetDeltaRotation( 162.0000f );
	_wallBox8.SetDeltaRotation( 126.0000f );
	_wallBox9.SetDeltaRotation( 90.0000f );
	_wallBox10.SetDeltaRotation( 108.0000f );
	_wallBox11.SetDeltaRotation( 36.0000f );
	_wallBox12.SetDeltaRotation( 54.0000f );
	_wallBox13.SetDeltaRotation( 72.0000f );
	_wallBox14.SetDeltaRotation( 18.0000f );
	_wallBox15.SetDeltaRotation( 288.0000f );
	_wallBox16.SetDeltaRotation( 324.0000f );
	_wallBox17.SetDeltaRotation( 342.0000f );
	_wallBox19.SetDeltaRotation( 306.0000f );


	// SetDeltaXY
	_wallBox1.SetDeltaXY( -0.2692f, 1.6996f );
	_wallBox2.SetDeltaXY( -2.2672f, 4.4496f );
	_wallBox3.SetDeltaXY( -3.8004f, 5.2308f );
	_wallBox4.SetDeltaXY( -5.5000f, 5.5000f );
	_wallBox5.SetDeltaXY( -1.0504f, 3.2328f );
	_wallBox6.SetDeltaXY( -1.0504f, -3.2328f );
	_wallBox7.SetDeltaXY( -0.2692f, -1.6996f );
	_wallBox8.SetDeltaXY( -2.2672f, -4.4496f );
	_wallBox9.SetDeltaXY( -5.5000f, -5.5000f );
	_wallBox10.SetDeltaXY( -3.8004f, -5.2308f );
	_wallBox11.SetDeltaXY( -9.9496f, -3.2328f );
	_wallBox12.SetDeltaXY( -8.7328f, -4.4496f );
	_wallBox13.SetDeltaXY( -7.1996f, -5.2308f );
	_wallBox14.SetDeltaXY( -10.7308f, -1.6996f );
	_wallBox15.SetDeltaXY( -7.1996f, 5.2308f );
	_wallBox16.SetDeltaXY( -9.9496f, 3.2328f );
	_wallBox17.SetDeltaXY( -10.7308f, 1.6996f );
	_wallBox18.SetDeltaXY( -11.0000f, 0.0000f );
	_wallBox19.SetDeltaXY( -8.7328f, 4.4496f );
    _bgCircle.SetDeltaXY( -5.5000f, 0.0000f );
    _accelBall.SetDeltaXY( -6.0000f, 0.0000f );
    
	// SetNodeChain
	SetNodeChain( &_wallBox1, &_wallBox2, &_wallBox3, &_wallBox4, &_wallBox5, &_wallBox6, &_wallBox7, &_wallBox8, &_wallBox9, &_wallBox10, &_wallBox11, &_wallBox12, &_wallBox13, &_wallBox14, &_wallBox15, &_wallBox16, &_wallBox17, &_wallBox18, &_wallBox19, &_bgCircle, &_accelBall, NULL );

	// Main block (WallBox)
	SetRotation( 0.0000f );

	SetPosition( 0, 0 );
}
//-----------------------------------------------------------------------------------------------------
puAccelIcon::~puAccelIcon()
{
}
//-----------------------------------------------------------------------------------------------------

