#ifndef __PUPULLYBLOCK_H__
#define __PUPULLYBLOCK_H__

#include <Box2D/Box2D.h>
#include "Blocks/puBlock.h"
#include "puPullyBlock.h"


class puPullyBlock : public puBlock
{
public:
	puPullyBlock( float density );
	puPullyBlock( float density, b2Vec2 size );
	virtual ~puPullyBlock();

private:
	void Construct( float density, b2Vec2 size );
};


#endif
