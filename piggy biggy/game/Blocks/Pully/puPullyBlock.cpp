#include "puPullyBlock.h"
#include "Game.h"

//--------------------------------------------------------------------------------------------------------------
puPullyBlock::puPullyBlock( float density ) 
{
	Construct( density, b2Vec2( 1, 2 ));
}
//--------------------------------------------------------------------------------------------------------------
puPullyBlock::puPullyBlock( float density, b2Vec2 size ) 
{
	Construct( density, size );
}
//--------------------------------------------------------------------------------------------------------------
void puPullyBlock::Construct( float density, b2Vec2 size )
{
	b2PolygonShape shape;
	shape.SetAsBox( size.x, size.y );

	_body->CreateFixture( &shape, density );
}
//--------------------------------------------------------------------------------------------------------------
puPullyBlock::~puPullyBlock()
{
}
