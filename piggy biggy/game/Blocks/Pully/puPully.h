#ifndef __PUPULLY_H__
#define __PUPULLY_H__

#include <Box2D/Box2D.h>
#include "Blocks/puBlock.h"
#include "Blocks/pully/puPullyBlock.h"

class puPully
{
public:
	puPully();
	puPully( b2Vec2 b1pos, float b1dens, b2Vec2 b2pos, float b2dens, float jointTopY );
	puPully( b2Vec2 b1pos, float b1dens, b2Vec2 b1size, b2Vec2 b2pos, float b2dens, b2Vec2 b2size, float jointTopY );

	virtual ~puPully();

private:
	void Construct( b2Vec2 b1pos, b2Vec2 b2pos, float jointTopY );

private:
	puPullyBlock _block1;
	puPullyBlock _block2;
	b2PulleyJoint *_joint;
};

#endif
