#ifndef __PUTFLAP_H__
#define __PUTFLAP_H__

#include <Box2D/Box2D.h>
#include "puCircle.h"
#include "puBox.h"

//-------------------------------------------------------------------------
class puTFlap : public puWoodBox<90,15,eBlockScaled>
{
public:
	puTFlap();
	
	void SetJointLimits( float lower, float upper );
	virtual void CreateJoints();
	virtual void DestroyJoints();

private:
	puScrew< 8 >	_screw1;
	puWoodBox<90,15,eBlockScaled>	_woodBox2;
	puWoodBox<90,15,eBlockScaled>	_woodBox3;
	puWoodBox<160,15,eBlockScaled>	_woodBox1;

	b2RevoluteJoint	*_jointRev1;
	b2RevoluteJoint	*_jointRev2;
	b2RevoluteJoint	*_jointRev3;
	b2RevoluteJoint	*_jointRev4;
};
//-------------------------------------------------------------------------

#endif
