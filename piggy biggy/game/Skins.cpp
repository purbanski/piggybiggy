#include "unFile.h"
#include "Skins.h"
#include "Debug/MyDebug.h"


Skins* Skins::_sInstance = NULL;
//----------------------------------------------------------------------
const char* Skins::GetSkinName( const char *file )
{

	if ( ! _sInstance )
		_sInstance = new Skins();

	string defaultSkin;
	string skin;

	if ( ! strlen( _sInstance->_skinSkinName ))
	{
		_sInstance->ClearFilename();
		strcpy( _sInstance->_skinSkinName, "BluePocket" );
	}
	
	_sInstance->ClearFullpath();
	strcpy( _sInstance->_skinFullpath, "Skins/" ); 
	strcat( _sInstance->_skinFullpath, _sInstance->_skinSkinName );
	strcat( _sInstance->_skinFullpath, "/" ); 
	strcat( _sInstance->_skinFullpath, file ); 

	if ( ! unResourceFileCheckExists( _sInstance->_skinFullpath ))
	{
		_sInstance->ClearFullpath();
		strcpy( _sInstance->_skinFullpath, "Skins/BluePocket/" );
		strcat( _sInstance->_skinFullpath, file ); 
	}

//    D_LOG( "SKins %s", _sInstance->_skinFullpath );
	return _sInstance->_skinFullpath; 
}
//----------------------------------------------------------------------
void Skins::SetSkinName( GameTypes::PocketType pocket )
{
    switch ( pocket )
	{
	case GameTypes::ePocketMoro :
		SetSkinName( "MoroPocket" );
		break;

	case GameTypes::ePocketRasta :
		SetSkinName( "RastaPocket" );
		break;

	case GameTypes::ePocketJapan :
		SetSkinName( "JapanPocket" );
		break;

	case GameTypes::ePocketScience :
		SetSkinName( "SciencePocket" );
		break;
    
    case GameTypes::ePocketBlue :
        SetSkinName( "BluePocket" );
		break;
            
   	default:
#ifdef DEBUG_MEGA
		unAssertMsg( Level, false, ("Wrong packet enum %d", pocket ));
#endif
        SetSkinName( "BluePocket" );
    }
}
//----------------------------------------------------------------------
void Skins::SetSkinName( const char *skin )
{
	if ( ! _sInstance )
		_sInstance = new Skins();

	memset( _sInstance->_skinSkinName, 0, SKIN_FILENAME_SIZE );
	strcpy( _sInstance->_skinSkinName, skin );
}
//----------------------------------------------------------------------
Skins::Skins()
{
	ClearFilename();
	ClearFullpath();
}
//----------------------------------------------------------------------
Skins::~Skins()
{
}
//----------------------------------------------------------------------
void Skins::Destroy()
{
	if ( _sInstance )
		delete _sInstance;

	_sInstance = NULL;
}
//----------------------------------------------------------------------
void Skins::ClearFilename()
{
	memset( _skinSkinName, 0, SKIN_FILENAME_SIZE );
}
//----------------------------------------------------------------------
void Skins::ClearFullpath()
{
	memset( _skinFullpath, 0, SKIN_FULLPATH_SIZE );
}
//----------------------------------------------------------------------

