#ifndef __PRELOADER_H__
#define __PRELOADER_H__

#include "SoundEngine.h"
#include "GameTypes.h"
#include <set>
#include <map>

using namespace std;
using namespace GameTypes;

class puBlock;

//-------------------------------------------------------------------------------
typedef set<float>	RadiusSet;

class AnimSizeSet : public map<CharactersAnim, RadiusSet>
{
public:
	void Add( CharactersAnim, float radius );
	void Reset();
	void Dump();
};
//-------------------------------------------------------------------------------
class Preloader
{
public:
	static Preloader* Get();
	static void Destroy();
	static int32 MemoryWarningRecieved( void* systemData, void* userData );

	void Preload();

	void AddSound( SoundEngine::Sound sound );
	void AddImage( const char *filename );
	void AddImageDir( const char *filename );
	void AddSkinImage( const char *filename );
	void AddFont( const char *filename );
	void AddAnim( GameTypes::CharactersAnim, puBlock *block );
	void AddAnim( GameTypes::CharactersAnim, float radius );
	void AddAnim( const char *filename  );
	void PreloadGraphicsInDir( const char *dir, bool skinned = true );

	void AddPiggyAnimSet( puBlock *block );
	
	void AddPiggyAnimSet( float radius );
	void AddPiggyRolledAnimSet( float radius );
	void AddPiggyStaticAnimSet( float radius );
	void AddThiefAnimSet( float radius );
	void AddThiefRolledAnimSet( float radius );
	void AddCopAnimSet( float radius );

	Preloader(){};
	~Preloader();

	void Reset();
	void FreeResources();

private:

	typedef set<SoundEngine::Sound>		SoundSet;
	typedef set<string>					FilesSet;

private:
	static Preloader	*_sInstance;	
	
	AnimSizeSet	_contAnim;
	FilesSet	_contAnimNames;
	SoundSet	_contSounds;
	FilesSet	_contImage;
	FilesSet	_contDirImage;
	FilesSet	_contFont;
};
//-------------------------------------------------------------------------------
#endif
