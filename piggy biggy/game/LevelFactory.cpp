﻿#include "LevelFactory.h"
#include "Levels/AllLevels.h"

//-----------------------------------------------------------------------------------------
LevelFactory::LevelFactory()
{
#ifdef BUILD_EDITOR4ALL
	_levelMap[ GameTypes::eLevelEditorEmpty ]			= &LevelFactory::NewLevel<LevelTestEmpty>;
	return;
#endif

	_levelMap[ GameTypes::eLevelIntro_Welcome ]			= &LevelFactory::NewLevel<LevelIntro_Welcome>;
	_levelMap[ GameTypes::eLevelIntro_DestroyBlock ]	= &LevelFactory::NewLevel<LevelIntro_DestroyBlock>;
	_levelMap[ GameTypes::eLevelIntro_Gun ]				= &LevelFactory::NewLevel<LevelIntro_Gun>;
	_levelMap[ GameTypes::eLevelIntro_Rope ]			= &LevelFactory::NewLevel<LevelIntro_Rope>;
	_levelMap[ GameTypes::eLevelIntro_MoneyMakeMoney ]	= &LevelFactory::NewLevel<LevelIntro_MoneyMakeMoney>;
	//_levelMap[ GameTypes::eLevelIntro_3 ]			= &LevelFactory::NewLevel<LevelIntro_1>;

	_levelMap[ GameTypes::eLevelMagnet_1 ]			= &LevelFactory::NewLevel<LevelMagnet_1>;
	_levelMap[ GameTypes::eLevelMagnet_2 ]			= &LevelFactory::NewLevel<LevelMagnet_2>;

	_levelMap[ GameTypes::eLevelAdamS_PlainLogic ]	= &LevelFactory::NewLevel<LevelAdamS_PlainLogic>;

	_levelMap[ GameTypes::eLevelSyncronize_1 ]		= &LevelFactory::NewLevel<LevelSynchronize_1>;
	_levelMap[ GameTypes::eLevelSyncronize_2 ]		= &LevelFactory::NewLevel<LevelSynchronize_2>;
	_levelMap[ GameTypes::eLevelSyncronize_3 ]		= &LevelFactory::NewLevel<LevelSynchronize_3>;

	_levelMap[ GameTypes::eLevelBomb_1 ]			= &LevelFactory::NewLevel<LevelLetsBomb_1>;
	_levelMap[ GameTypes::eLevelBomb_1b ]			= &LevelFactory::NewLevel<LevelLetsBomb_1b>;
	_levelMap[ GameTypes::eLevelBomb_2 ]			= &LevelFactory::NewLevel<LevelLetsBomb_2>;
	_levelMap[ GameTypes::eLevelBomb_3 ]			= &LevelFactory::NewLevel<LevelLetsBomb_3>;
	_levelMap[ GameTypes::eLevelBomb_4 ]			= &LevelFactory::NewLevel<LevelLetsBomb_4>;
	_levelMap[ GameTypes::eLevelBomb_5 ]			= &LevelFactory::NewLevel<LevelLetsBomb_5>;
	_levelMap[ GameTypes::eLevelBomb_6 ]			= &LevelFactory::NewLevel<LevelLetsBomb_6>;
	_levelMap[ GameTypes::eLevelBomb_7 ]			= &LevelFactory::NewLevel<LevelLetsBomb_Pile>;
	_levelMap[ GameTypes::eLevelBomb_8 ]			= &LevelFactory::NewLevel<LevelLetsBomb_8>;
	
	_levelMap[ GameTypes::eLevelSwing_1 ]			= &LevelFactory::NewLevel<LevelLetsSwing_1>;
	_levelMap[ GameTypes::eLevelSwing_2 ]			= &LevelFactory::NewLevel<LevelLetsSwing_2>;
	_levelMap[ GameTypes::eLevelSwing_Impossible ]	= &LevelFactory::NewLevel<LevelLetsSwing_3>;

	_levelMap[ GameTypes::eLevelDemo_0 ]			= &LevelFactory::NewLevel<LevelDemo_0>;
	_levelMap[ GameTypes::eLevelDemo_1 ]			= &LevelFactory::NewLevel<LevelDemo_1>;
	_levelMap[ GameTypes::eLevelDemo_2 ]			= &LevelFactory::NewLevel<LevelDemo_2>;
	_levelMap[ GameTypes::eLevelDemo_3 ]			= &LevelFactory::NewLevel<LevelDemo_3>;

	_levelMap[ GameTypes::eLevelRoll_1 ]			= &LevelFactory::NewLevel<LevelLetsRoll_1>;
	_levelMap[ GameTypes::eLevelRoll_2 ]			= &LevelFactory::NewLevel<LevelLetsRoll_2>;
	//_levelMap[ GameTypes::eLevelRoll_3 ]			= &LevelFactory::NewLevel<LevelLets_Synchronize_1>;
	_levelMap[ GameTypes::eLevelRotateL ]			= &LevelFactory::NewLevel<LevelRotateL>;

	_levelMap[ GameTypes::eLevelTraps_1 ]			= &LevelFactory::NewLevel<LevelTraps_1>;
	_levelMap[ GameTypes::eLevelTraps_2 ]			= &LevelFactory::NewLevel<LevelTraps_2>;
	_levelMap[ GameTypes::eLevelTraps_3 ]			= &LevelFactory::NewLevel<LevelTraps_3>;
	
	_levelMap[ GameTypes::eLevelChain_1 ]			= &LevelFactory::NewLevel<LevelChain_1>;
	_levelMap[ GameTypes::eLevelChain_2a ]			= &LevelFactory::NewLevel<LevelChain_2a>;
	_levelMap[ GameTypes::eLevelChain_2b ]			= &LevelFactory::NewLevel<LevelChain_2b>;
	_levelMap[ GameTypes::eLevelChain_2c ]			= &LevelFactory::NewLevel<LevelChain_2c>;
	_levelMap[ GameTypes::eLevelChain_3 ]			= &LevelFactory::NewLevel<LevelChain_3>;
	_levelMap[ GameTypes::eLevelChain_4 ]			= &LevelFactory::NewLevel<LevelChain_4>;
	_levelMap[ GameTypes::eLevelChain_5 ]			= &LevelFactory::NewLevel<LevelChain_5>;
	_levelMap[ GameTypes::eLevelChain_6 ]			= &LevelFactory::NewLevel<LevelChain_6>;
	_levelMap[ GameTypes::eLevelChain_7a ]			= &LevelFactory::NewLevel<LevelChain_7a>;
	_levelMap[ GameTypes::eLevelChain_7b ]			= &LevelFactory::NewLevel<LevelChain_7b>;

	_levelMap[ GameTypes::eLevelTetris_1 ]			= &LevelFactory::NewLevel<LevelTetris_1>;
	_levelMap[ GameTypes::eLevelTetris_2 ]			= &LevelFactory::NewLevel<LevelTetris_2>;
	
	_levelMap[ GameTypes::eLevelDomino_1 ]			= &LevelFactory::NewLevel<LevelDomino_1>;

	_levelMap[ GameTypes::eLevelConstruction_1a ]	= &LevelFactory::NewLevel<LevelConstruction_1a>;
	_levelMap[ GameTypes::eLevelConstruction_1b ]	= &LevelFactory::NewLevel<LevelConstruction_1b>;
	_levelMap[ GameTypes::eLevelConstruction_1c ]	= &LevelFactory::NewLevel<LevelConstruction_1c>;
	_levelMap[ GameTypes::eLevelConstruction_1d ]	= &LevelFactory::NewLevel<LevelConstruction_1d>;

	_levelMap[ GameTypes::eLevelConstruction_2a ]	= &LevelFactory::NewLevel<LevelConstruction_2a>;
	
	_levelMap[ GameTypes::eLevelConstruction_3a ]	= &LevelFactory::NewLevel<LevelConstruction_3a>;
	
	_levelMap[ GameTypes::eLevelConstruction_4a ]	= &LevelFactory::NewLevel<LevelConstruction_4a>;
	_levelMap[ GameTypes::eLevelConstruction_4b ]	= &LevelFactory::NewLevel<LevelConstruction_4b>;
	_levelMap[ GameTypes::eLevelConstruction_4c ]	= &LevelFactory::NewLevel<LevelConstruction_4c>;

	_levelMap[ GameTypes::eLevelGun_1 ]				= &LevelFactory::NewLevel<LevelLetsShoot_1>;
	_levelMap[ GameTypes::eLevelGun_2 ]				= &LevelFactory::NewLevel<LevelLetsShoot_2>;
	_levelMap[ GameTypes::eLevelGun_3 ]				= &LevelFactory::NewLevel<LevelLetsShoot_3>;
	_levelMap[ GameTypes::eLevelGun_4 ]				= &LevelFactory::NewLevel<LevelLetsShoot_4>;

	_levelMap[ GameTypes::eLevelReel_1 ]			= &LevelFactory::NewLevel<LevelLetsRell_1>;
	_levelMap[ GameTypes::eLevelReel_2 ]			= &LevelFactory::NewLevel<LevelLetsRell_2>;
	_levelMap[ GameTypes::eLevelReel_3 ]			= &LevelFactory::NewLevel<LevelLetsRell_3>;

	_levelMap[ GameTypes::eLevelBank_1 ]			= &LevelFactory::NewLevel<LevelBank_1>;
	_levelMap[ GameTypes::eLevelBank_2 ]			= &LevelFactory::NewLevel<LevelBank_2>;
	_levelMap[ GameTypes::eLevelBank_3 ]			= &LevelFactory::NewLevel<LevelBank_3>;
	_levelMap[ GameTypes::eLevelBank_Impossible ]	= &LevelFactory::NewLevel<LevelBank_4>;
	_levelMap[ GameTypes::eLevelBank_Impossible_bak ]	= &LevelFactory::NewLevel<LevelBank_5>;

	_levelMap[ GameTypes::eLevelScale_1 ]			= &LevelFactory::NewLevel<LevelScale_1>;
	_levelMap[ GameTypes::eLevelOthers_1 ]			= &LevelFactory::NewLevel<LevelOthers_1>;
	_levelMap[ GameTypes::eLevelOthers_2 ]			= &LevelFactory::NewLevel<LevelOthers_2>;
	_levelMap[ GameTypes::eLevelOthers_3 ]			= &LevelFactory::NewLevel<LevelOthers_ControlWheel>;
	_levelMap[ GameTypes::eLevelOthers_4 ]			= &LevelFactory::NewLevel<LevelOthers_4>;
	_levelMap[ GameTypes::eLevelOthers_5 ]			= &LevelFactory::NewLevel<LevelSwingAndBomb>;
	_levelMap[ GameTypes::eLevelOthers_6 ]			= &LevelFactory::NewLevel<LevelOthers_6>;
	_levelMap[ GameTypes::eLevelOthers_7 ]			= &LevelFactory::NewLevel<LevelOthers_7>;
	
	_levelMap[ GameTypes::eLevelRotate_1 ]			= &LevelFactory::NewLevel<LevelLetsRotate_1>;
	_levelMap[ GameTypes::eLevelRotate_2 ]			= &LevelFactory::NewLevel<LevelLetsRotate_2>;
	_levelMap[ GameTypes::eLevelRotate_3 ]			= &LevelFactory::NewLevel<LevelLetsRotate_3>;
	_levelMap[ GameTypes::eLevelRotate_4 ]			= &LevelFactory::NewLevel<LevelLetsRotate_4>;
	_levelMap[ GameTypes::eLevelRotate_5 ]			= &LevelFactory::NewLevel<LevelLetsRotate_5>;
	_levelMap[ GameTypes::eLevelRotate_6 ]			= &LevelFactory::NewLevel<LevelLetsRotate_6>;
	_levelMap[ GameTypes::eLevelRotate_7 ]			= &LevelFactory::NewLevel<LevelLetsRotate_7>;
	_levelMap[ GameTypes::eLevelRotate_Circle ]		= &LevelFactory::NewLevel<LevelLetsRotate_Circle>;

	_levelMap[ GameTypes::eLevelLukasz_1a ]			= &LevelFactory::NewLevel<LevelLukasz_1a>;
	_levelMap[ GameTypes::eLevelLukasz_1b ]			= &LevelFactory::NewLevel<LevelLukasz_1b>;
	_levelMap[ GameTypes::eLevelLukasz_2 ]			= &LevelFactory::NewLevel<LevelLukasz_2>;

	_levelMap[ GameTypes::eLevelLabyrinth_1 ]			= &LevelFactory::NewLevel<LevelLabyrinth_1>;
	_levelMap[ GameTypes::eLevelLabyrinth_Imposible ]	= &LevelFactory::NewLevel<LevelLabyrinth_Imposible>;

	_levelMap[ GameTypes::eLevelVehical_1 ]			= &LevelFactory::NewLevel<LevelVehical_1>;
	_levelMap[ GameTypes::eLevelVehical_2 ]			= &LevelFactory::NewLevel<LevelVehical_2>;
	_levelMap[ GameTypes::eLevelVehical_3 ]			= &LevelFactory::NewLevel<LevelVehical_3>;
	_levelMap[ GameTypes::eLevelVehical_4 ]			= &LevelFactory::NewLevel<LevelVehical_4>;


	_levelMap[ GameTypes::eLevelTestSea ]				= &LevelFactory::NewLevel<LevelTestSea>;
	_levelMap[ GameTypes::eLevelTestAquarium ]			= &LevelFactory::NewLevel<LevelTestAquarium>;
	_levelMap[ GameTypes::eLevelTestAquariumBlank ]		= &LevelFactory::NewLevel<LevelTestAquariumBlank>;
	_levelMap[ GameTypes::eLevelTestDestroyBlocks ]		= &LevelFactory::NewLevel<LevelTestDestroyBlocks>;
	_levelMap[ GameTypes::eLevelTestAccelerometr ]		= &LevelFactory::NewLevel<LevelTestAccelerometr>;
	_levelMap[ GameTypes::eLevelTestVertical ]			= &LevelFactory::NewLevel<LevelTestVertical>;
	_levelMap[ GameTypes::eLevelTestSpace ]				= &LevelFactory::NewLevel<LevelTestSpace>;
	_levelMap[ GameTypes::eLevelTestUser ]				= &LevelFactory::NewLevel<LevelUserTest>;


	_levelMap[ GameTypes::eLevelEditorEmpty ]			= &LevelFactory::NewLevel<LevelTestEmpty>;
	_levelMap[ GameTypes::eLevelEditorWithSkin ]		= &LevelFactory::NewLevel<LevelTestEmptyWithSkin>;
	_levelMap[ GameTypes::eLevelEditorScreenshot ]		= &LevelFactory::NewLevel<LevelScreenshot>;
	
	//_levelMap[ GameTypes::eLevelEditorEmptySkin ]		= &LevelFactory::NewLevel<LevelTestEmptyWithSkin>;
	_levelMap[ GameTypes::eLevelEditorAquarium ]		= &LevelFactory::NewLevel<LevelEditorAquarium>;
	//_levelMap[ GameTypes::eLevelEditorSea ]				= &LevelFactory::NewLevel<LevelEditorSea>;

	_levelMap[ GameTypes::eLevelPathfinder_1 ]			= &LevelFactory::NewLevel<LevelPathFinder_1>;
	_levelMap[ GameTypes::eLevelScale_2 ]				= &LevelFactory::NewLevel<LevelScale_2>;
	_levelMap[ GameTypes::eLevelScale_3 ]				= &LevelFactory::NewLevel<LevelScale_3>;
	_levelMap[ GameTypes::eLevelBombAndRoll_1 ]			= &LevelFactory::NewLevel<LevelBombAndRoll_1>;
	_levelMap[ GameTypes::eLevelTunnel_1 ]				= &LevelFactory::NewLevel<LevelTunnel_1>;
	_levelMap[ GameTypes::eLevelExplosivePath ]			= &LevelFactory::NewLevel<LevelExplosivePath>;
	_levelMap[ GameTypes::eLevelBouncingCoin_1 ]		= &LevelFactory::NewLevel<LevelBouncingCoin_1>;
	_levelMap[ GameTypes::eLevelSpiderWeb ]			= &LevelFactory::NewLevel<LevelSpiderWeb>;

	_levelMap[ GameTypes::eLevelKonkurs ]				= &LevelFactory::NewLevel<LevelKonkurs>;

	//------------------------ 
	// Paweł Dąbrowski
	_levelMap[ GameTypes::eLevelPawel_1 ]				= &LevelFactory::NewLevel<LevelPawel_1>;
	_levelMap[ GameTypes::eLevelPawel_2 ]				= &LevelFactory::NewLevel<LevelPawel_2>;
	_levelMap[ GameTypes::eLevelPawel_3 ]				= &LevelFactory::NewLevel<LevelPawel_3>;
	_levelMap[ GameTypes::eLevelPawel_4 ]				= &LevelFactory::NewLevel<LevelPawel_4>;
	_levelMap[ GameTypes::eLevelPawel_5 ]				= &LevelFactory::NewLevel<LevelPawel_5>;
	_levelMap[ GameTypes::eLevelPawel_6 ]				= &LevelFactory::NewLevel<LevelPawel_6>;
	_levelMap[ GameTypes::eLevelPawel_7 ]				= &LevelFactory::NewLevel<LevelPawel_7>;
	_levelMap[ GameTypes::eLevelPawel_8 ]				= &LevelFactory::NewLevel<LevelPawel_8>;
	_levelMap[ GameTypes::eLevelPawel_9 ]				= &LevelFactory::NewLevel<LevelPawel_9>;
	_levelMap[ GameTypes::eLevelPawel_10 ]				= &LevelFactory::NewLevel<LevelPawel_10>;
	_levelMap[ GameTypes::eLevelPawel_11 ]				= &LevelFactory::NewLevel<LevelPawel_11>;
	_levelMap[ GameTypes::eLevelPawel_12 ]				= &LevelFactory::NewLevel<LevelPawel_12>;
	_levelMap[ GameTypes::eLevelPawel_13 ]				= &LevelFactory::NewLevel<LevelPawel_13>;
	//_levelMap[ GameTypes::eLevelPawel_14 ]				= &LevelFactory::NewLevel<LevelPawel_14>;
	_levelMap[ GameTypes::eLevelPawel_15 ]				= &LevelFactory::NewLevel<LevelPawel_15>;
	_levelMap[ GameTypes::eLevelPawel_16 ]				= &LevelFactory::NewLevel<LevelPawel_16>;


	//-------------------------------
	// Tomek Wierel
	_levelMap[ GameTypes::eLevelTomek_1 ]				= &LevelFactory::NewLevel<LevelTomekW_Mapa3>;
	_levelMap[ GameTypes::eLevelTomek_2 ]				= &LevelFactory::NewLevel<LevelTomekW_Prosty4>;
	_levelMap[ GameTypes::eLevelTomek_CalmWood ]		= &LevelFactory::NewLevel<LevelTomekW_CalmWood>;
	_levelMap[ GameTypes::eLevelTomek_SelfMadeCannon  ]	= &LevelFactory::NewLevel<LevelTomekW_SelfMadeCannon>;

	//-------------------------------
	// Bogusia
	_levelMap[ GameTypes::eLevelBogusia_GoUnder ]		= &LevelFactory::NewLevel<LevelBogusia_1>;
	_levelMap[ GameTypes::eLevelBogusia_2 ]				= &LevelFactory::NewLevel<LevelBogusia_Etap3a>;
	_levelMap[ GameTypes::eLevelBogusia_3 ]				= &LevelFactory::NewLevel<LevelBogusia_Etap3b>;
	_levelMap[ GameTypes::eLevelBogusia_4 ]				= &LevelFactory::NewLevel<LevelBogusia_Etap7>;

	//---------------------------------
	// Jacek Zielinski
	_levelMap[ GameTypes::eLevelJacekZ_1 ]				= &LevelFactory::NewLevel<LevelJacekZ_Fastfrog>;

	//---------------------------------
	// Marzena Kantorska 
	_levelMap[ GameTypes::eLevelMarzena_1 ]				= &LevelFactory::NewLevel<LevelMarzena_Etap5>;
}
//-----------------------------------------------------------------------------------------
LevelFactory::~LevelFactory()
{
	_levelMap.clear();
}
//-----------------------------------------------------------------------------------------
Level* LevelFactory::CreateLevel( GameTypes::LevelEnum levelEnum, bool managed )
{
	Level *ret;
	LevelFactory::FuncPtr func;

	func = _levelMap[ levelEnum ];
	if ( ! func )
	{
		unAssertMsg(LevelFactory, false, ("Level empty (not in the level map)???? "));
		func = _levelMap[ GameTypes::eLevelIntro_Welcome ];
	}

	ret = (this->*func)( levelEnum, managed, false );
	return ret;
}
//-----------------------------------------------------------------------------------------
Level* LevelFactory::CreateDemoLevel( GameTypes::LevelEnum levelEnum )
{
	Level *ret;
	LevelFactory::FuncPtr func;

	func = _levelMap[ levelEnum ];
	if ( ! func )
	{
		unAssertMsg(LevelFactory, false, ("Level empty (not in the level map)???? "));
		func = _levelMap[ GameTypes::eLevelIntro_Welcome ];
	}

	ret = (this->*func)( levelEnum, true, false );
	return ret;
}
//-----------------------------------------------------------------------------------------
Level* LevelFactory::CreateViewLevel( GameTypes::LevelEnum levelEnum )
{
	Level *ret;
	LevelFactory::FuncPtr func;

	func = _levelMap[ levelEnum ];
	if ( ! func )
	{
		unAssertMsg(LevelFactory, false, ("Level empty (not in the level map)???? "));
		func = _levelMap[ GameTypes::eLevelIntro_Welcome ];
	}

	ret = (this->*func)( levelEnum, true, true );
	return ret;
}

