#ifndef __LEVELMENU_H__
#define __LEVELMENU_H__

#include "cocos2d.h"
#include <pu/ui/menus//HidingMenu.h>
#include "Components/FXMenuItem.h"
#include "Levels/Level.h"
#include "GameConfig.h"

USING_NS_CC;
using namespace GameTypes;

//-----------------------------------------------------------------------------------------------------
class LevelMenuBase : public HidingMenu
{
public:
//	LAYER_NODE_FUNC( LevelMenuBase );
	bool init();
	void AdjustPosition();

	typedef void (LevelMenuBase::*FuncPtr)(CCObject*);

public:
	~LevelMenuBase();

	//Touch related	
	virtual bool ccTouchBegan(CCTouch *touch, CCEvent *pEvent) { return _enabled; }
	virtual void ccTouchMoved(CCTouch *touch, CCEvent *pEvent) {};
	virtual void ccTouchEnded(CCTouch *touch, CCEvent *pEvent) {};

	virtual void Menu_RestartLevel( CCObject* pSender );
	virtual void Menu_NextLevel( CCObject* pSender );
	virtual void Menu_SkipLevel( CCObject* pSender );
	virtual void Menu_SelectLevel( CCObject* pSender );
    virtual void Menu_ChallengeMenu( CCObject *pSender );
    
protected:
	LevelMenuBase();
    void SetDeltaMove( const CCPoint &deltaMove );

	CCMenuItem* GetMenuItem( const char *btName, FuncPtr, const char *disabledPostfix = NULL );
	CCMenuItem* GetSkipButton(const char *btName, FuncPtr fn, const char* fontFile , const char* disabledfontFile );

protected:
	CCMenuItem	*_skipLabelButton;
};
//-----------------------------------------------------------------------------
class SkipMenuItem : public FXMenuItemSprite
{
public:
	static SkipMenuItem* Create( CCNode *target, const char *btName, LevelMenuBase::FuncPtr fn, const char *fontFile, const char *disabledFontFile );
	void Init( const char *btName, const char *fontFile, const char *disabledFontFile );

	//virtual void activate();
	virtual ~SkipMenuItem();
};
//-----------------------------------------------------------------------------------------------------
class MusicMenu;
class LevelSubMenu : public LevelMenuBase
{
public:
	static LevelSubMenu* node( Level *level );
	bool init( Level *level );
	~LevelSubMenu();

	virtual void registerWithTouchDispatcher();

	virtual void Show();
	virtual void Hide();

	void SetParentMenu( CCMenu* menu );

	void Menu_BackToGame( CCObject* pSender );
    void SetEnabled( bool enabled);

private:
	LevelSubMenu();

	void Menu_MainMenu( CCObject* pSender );
	void Menu_SkipLevel( CCObject* pSender );

	void Menu_Debug( CCObject* pSender );

private:
	CCMenu		*_parentMenu;
    CCMenu      *_levelMenu;
	CCMenuItem	*_backToGameButton;
    MusicMenu   *_musicMenu;
};
//-----------------------------------------------------------------------------------------------------
class LevelFinishMenu : public LevelMenuBase
{
public:
    typedef enum
    {
        eLevelFinishMenu_LevelFinished = 0,
        eLevelFinishMenu_LevelFinishedFirstTime,
        eLevelFinishMenu_LevelFailed
    } LevelFinishMenuState;
    
	static LevelFinishMenu* node( LevelFinishMenuState state, GameTypes::LevelOrientation orient, const CCPoint& deltaMove  );
	bool Init( LevelFinishMenuState state, const CCPoint &deltaMove );
	~LevelFinishMenu();

	virtual void registerWithTouchDispatcher();
	virtual void Show();

private:
	LevelFinishMenu( LevelOrientation orient );
    void SetCompletedMenu( );
    void SetCompletedFirstTimeMenu();
    void SetFailedMenu();

private:
	CCMenu		*_menu;
	CCAction	*_showAction;
    GameTypes::LevelOrientation _levelOrient;
    
    static      CCPoint _sPiggySpirtePos;
};
//-----------------------------------------------------------------------------------------------------
class LevelFinishMenuNode : public CCNode
{
public:
	static LevelFinishMenuNode* node( Level *level );
	~LevelFinishMenuNode();

	void SetOrient( LevelOrientation orient );
	void Show( LevelState state );
	void ShowDelay( CCNode *node );

private:
	LevelFinishMenuNode();
	void Init( Level *level );

private:
	LevelFinishMenu		*_menuCompleted;
	LevelFinishMenu		*_menuFailed;
    CCAction            *_showSeq;
    LevelState          _menuState;
};
//-----------------------------------------------------------------------------------------------------
class MusicMenu : public CCNode
{
public:
	static MusicMenu* Create();

	~MusicMenu();

	void ToggleMusic( CCObject *sender );
	void ToggleEffects( CCObject *sender );
	void SetVisibleEffectsButton( bool visible );
    void SetEnable( bool enable );
    
private:
	MusicMenu();
	void Init();

	FXMenuItemToggle	*_effectsButton;
	FXMenuItemToggle	*_musicButton;
	CCMenu *_sndMenu;
};
//-----------------------------------------------------------------------------------------------------
class LevelTopRightMenu : public CCNode
{
public:
	static LevelTopRightMenu* Create( Level *level );
	~LevelTopRightMenu();

	void EnableRestartButton( bool enable );
	void EnableMenuButton( bool enable );
    void EnableSubMenu( bool enable );
    void EnableMenu( bool enable );
	CCNode* GetMenuButtons(){ return _menuButtons; }
	void SetOrientation( GameTypes::LevelOrientation orient );

    void HideSubMenu();
	void Menu_ShowSubMenu( CCObject* pSender );

private:
	LevelTopRightMenu();

	void Menu_Restart( CCObject* pSender );
	void Menu_Screenshot( CCObject* pSender );
    
	void Init( Level *level );

private:
	CCMenu				*_menu;
	CCNode				*_menuButtons;
	LevelSubMenu		*_subMenu;
	CCNode				*_subMenuNode;
	FXMenuItemSprite	*_restartButton;
	FXMenuItemSprite	*_menuButton;
   	FXMenuItemSprite	*_screenshotButton;

#ifdef BUILD_EDITOR
private:
	void Menu_EditorToggle( CCObject* pSender );
	void Menu_PlayGameToggle( CCObject* pSender );

private:
	MenuControls		*_subEditorMenu;
#endif

};

//-----------------------------------------------------------------------------------------------------

#endif
