#include "LevelWater.h"
//--------------------------------------------------------------------------
LevelSea::LevelSea( GameTypes::LevelEnum levelEnum, bool viewMode ) : LevelWater( levelEnum, viewMode )
{
	_waterBounancy = Config::WaterBouyancy;
	_wavingFrequency = Config::WaterWaveFrequency;
	_wavingForce = Config::WaterWaveForce;
	_wavingForce.Set( 1000.0f, 0.0f );
	
	// _wallBox1
	_wallBox1.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox1.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox1.SetBodyType( b2_staticBody );

	// _wallBox2
	_wallBox2.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox2.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox2.SetBodyType( b2_staticBody );

	// _wallBox3
	_wallBox3.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox3.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox3.SetBodyType( b2_staticBody );


	// _wallBox4
	_wallBox4.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox4.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox4.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox4.SetBodyType( b2_staticBody );


	//Set blocks positions
	_wallBox1.SetPosition( 24.0000f, 0.4f );
	_wallBox2.SetPosition( 0.0f, 16.8000f );
	_wallBox3.SetPosition( 48.0f, 16.0000f );
	_wallBox4.SetPosition( 24.0000f, 31.4f );

	if ( _wallBox4.GetSprite() )
	{
		_wallBox4.GetSprite()->removeFromParentAndCleanup( true );
		_wallBox4.SetSprite( CCSprite::spriteWithFile( Skins::GetSkinName("Images/Blocks/none.png") ));
	}
}
//--------------------------------------------------------------------------
LevelAquarium::LevelAquarium( GameTypes::LevelEnum levelEnum, bool viewMode ) : LevelWater( levelEnum, viewMode )
{
	_waterBounancy = Config::WaterBouyancy;
	_wavingFrequency = 0.0f;
	_wavingForce.SetZero();

	// _wallBox1
	_wallBox1.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox1.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox1.SetBodyType( b2_staticBody );

	// _wallBox2
	_wallBox2.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox2.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox2.SetBodyType( b2_staticBody );

	// _wallBox3
	_wallBox3.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox3.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox3.SetBodyType( b2_staticBody );


	// _wallBox4
	_wallBox4.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox4.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox4.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox4.SetBodyType( b2_staticBody );


	//Set blocks positions
	_wallBox1.SetPosition( 24.0000f, 0.4f );
	_wallBox2.SetPosition( 0.0f, 16.8000f );
	_wallBox3.SetPosition( 48.0f, 16.0000f );
	_wallBox4.SetPosition( 24.0000f, 32.4f );

	if ( _wallBox4.GetSprite() )
	{
		_wallBox4.GetSprite()->removeFromParentAndCleanup( true );
		_wallBox4.SetSprite( CCSprite::spriteWithFile( Skins::GetSkinName("Images/Blocks/none.png") ));
	}

	//CCWaves *waves = CCWaves::actionWithWaves( 3, 8, true, false, ccg(15,10), 15);
	//runAction(( CCRepeatForever::actionWithAction((CCActionInterval*) waves )));
}

//--------------------------------------------------------------------------
LevelWater::LevelWater( GameTypes::LevelEnum levelEnum, bool viewMode ) : Level( levelEnum, viewMode )
{
	_wavingCounter = 0.0f;
	_waterBounancy = Config::WaterBouyancy;
	_wavingFrequency = Config::WaterWaveFrequency;
	_wavingForce = Config::WaterWaveForce;
	_wavingForce.SetZero();
}
//--------------------------------------------------------------------------
void LevelWater::Step( cocos2d::ccTime dt )
{
	Level::Step( dt );

	b2Vec2 bouyancy;
	
	// erase me
	_waterBounancy = 1;

	bouyancy = -1.0f * _waterBounancy  * _world.GetGravity();

	for ( BlockSet::iterator it = _bouyentBlocks.begin(); it != _bouyentBlocks.end(); it++ )
	{
		if ( (*it)->GetBody() )
		{
			// Bouancy force
			(*it)->GetBody()->ApplyForce( (*it)->GetBouyancy() * bouyancy, (*it)->GetPosition() );

			// wave force
			(*it)->GetBody()->ApplyForce(  GetWaveForce(), (*it)->GetPosition() );
		}
	}
}
//--------------------------------------------------------------------------
b2Vec2 LevelWater::GetWaveForce()
{
	if ( _wavingCounter < 360.0f )
		_wavingCounter += _wavingFrequency;
	else 
		_wavingCounter = 0.0f;

	b2Vec2 ret;
	ret.SetZero();
	ret.x = sin ((float) _wavingCounter / 180.0f * b2_pi ) * _wavingForce.x ;
		
	return ret;
}
//--------------------------------------------------------------------------
