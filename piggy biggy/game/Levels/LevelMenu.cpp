#include <sstream>
#include "LevelMenu.h"
#include "SoundEngine.h"
#include "Game.h"
#include "Scenes/SelectLevelFromPocketScene.h"
#include "Scenes/MainMenuScene.h"
#include "Tools.h"
#include "SoundEngine.h"
#include "RunLogger/RunLogger.h"
#include "Facebook/FBTool.h"
#include "Promotion/FBRequestMenu.h"
#include "Platform/Lang.h"
#include "MessageBox.h"

#ifdef BUILD_EDITOR
#include "Editor/GameEditor.h"
#endif

using namespace std;

CCPoint LevelMenuPos(780.0f, 0.0f);
//CCPoint MenuOnRight_DeltaMove( 630.0f, 0.0f);
CCPoint MenuOnRight_DeltaMove( 780.0f, 0.0f);
CCPoint MenuOnCenter_DeltaMove( 780.0f, 0.0f);

CCPoint MenuOnRight_DeltaMove_Vertical( 630.0f, 0.0f);
CCPoint MenuOnCenter_DeltaMove_Vertical( 630.0f, 0.0f);


//----------------------------------------------------------------------------------------
// LevelMenuBase
//----------------------------------------------------------------------------------------
LevelMenuBase::LevelMenuBase()
{
}
//----------------------------------------------------------------------------------------
bool LevelMenuBase::init()
{
	if ( ! HidingMenu::init() )
		return false;

	//setPosition( CCPoint( 400.0f, 0.0f ) );

	return true;
}
//----------------------------------------------------------------------------------------
void LevelMenuBase::SetDeltaMove( const CCPoint &movingDelta )
{
    SetShowPosition( CCPoint( -movingDelta.x, movingDelta.y ));
	SetHidePosition( movingDelta );
}
//----------------------------------------------------------------------------------------
CCMenuItem* LevelMenuBase::GetMenuItem( const char *btName, FuncPtr fn, const char *disablePostfix ) 
{
	string file;
	string fileHold;
	string fileDisabled;

	file.append("Images/Menus/LevelMenu/bt" );
	file.append( btName );
	file.append( ".png" );

	file = Skins::GetSkinName( file.c_str() );

	fileDisabled.append("Images/Menus/LevelMenu/bt" );
	fileDisabled.append( btName );
	if ( disablePostfix )
	{
		fileDisabled.append( disablePostfix );
	}
	fileDisabled.append( ".png" );
	fileDisabled = Skins::GetSkinName( fileDisabled.c_str() );

	fileHold.append("Images/Menus/LevelMenu/bt" );
	fileHold.append( btName );
	fileHold.append( ".png" );
	fileHold = Skins::GetSkinName( fileHold.c_str() );
	
	CCSprite *btSprite = CCSprite::spriteWithFile( file.c_str() );
	CCSprite *btSpriteHold = CCSprite::spriteWithFile( fileHold.c_str() );
	CCSprite *btSpriteDisabled = CCSprite::spriteWithFile( fileDisabled.c_str() );

	AnimerSimpleEffect::Get()->Anim_ButtonPulse( btSpriteHold );
	FXMenuItemSprite *menuButton = FXMenuItemSprite::itemFromNormalSprite( btSprite, btSpriteHold, btSpriteDisabled, this, (SEL_MenuHandler) fn );

	return menuButton;
}
//----------------------------------------------------------------------------------------
CCMenuItem* LevelMenuBase::GetSkipButton( const char *btName, FuncPtr fn, const char *fontFile, const char *disabledFontFile )
{
	return SkipMenuItem::Create( this, btName, fn, fontFile, disabledFontFile );
}
//----------------------------------------------------------------------------------------
void LevelMenuBase::Menu_RestartLevel( CCObject* pSender )
{
	Game::Get()->RestartLevel();
}
//----------------------------------------------------------------------------------------
void LevelMenuBase::Menu_NextLevel( CCObject* pSender )
{
	Game::Get()->PlayNextLevel();
}
//----------------------------------------------------------------------------------------
void LevelMenuBase::Menu_SkipLevel( CCObject* pSender )
{
	Game::Get()->SkipLevel();
}
//----------------------------------------------------------------------------------------
void LevelMenuBase::Menu_SelectLevel( CCObject* pSender )
{
	Game::Get()->GetRuningLevel()->Quiting();
	Preloader::Get()->FreeResources();
	SelectLevelFromPocketScene::ShowScene( Game::Get()->GetGameStatus()->GetCurrentPocket() );
}
//----------------------------------------------------------------------------------------
void LevelMenuBase::Menu_ChallengeMenu( CCObject* pSender )
{
    if ( ! FBTool::Get()->IsLogged() )
    {
        RLOG_SS("MENU_LEVEL", "CHALLENGE", "FB_LOGIN_REQUEST" )
        if ( MessageBox::CreateYesNoBox( LocalString( "MsgBoxLoginToFacebookTitle" ), LocalString( "MsgBoxLoginToFacebook" )))
        {
            RLOG_SSS("MENU_LEVEL", "CHALLENGE", "FB_LOGIN_REQUEST", "YES" );
            FBTool::Get()->Login();
        }
        else
        {
            RLOG_SSS("MENU_LEVEL", "CHALLENGE", "FB_LOGIN_REQUEST", "NO" );
        }
    }
    else
    {
        FBRequestMenu_LevelMenu *menu;
        menu = FBRequestMenu_LevelMenu::Create();
        menu->setPosition( CCPointZero );
        CCDirector::sharedDirector()->getRunningScene()->addChild( menu, 250 );
    }
}
//----------------------------------------------------------------------------------------
LevelMenuBase::~LevelMenuBase()
{
	removeAllChildrenWithCleanup( true );
}



//----------------------------------------------------------------------------------------
// LevelMenu
//----------------------------------------------------------------------------------------
LevelTopRightMenu* LevelTopRightMenu::Create( Level *level )
{
	LevelTopRightMenu* ret;
	ret = new LevelTopRightMenu();
	if ( ret )
	{
		ret->autorelease();
		ret->Init( level );
		return ret;
	}
	CC_SAFE_DELETE(ret);
	return NULL;
}
//----------------------------------------------------------------------------------------
LevelTopRightMenu::LevelTopRightMenu()
{
}
//----------------------------------------------------------------------------------------
void LevelTopRightMenu::Init( Level *level )
{
	int opactiy;
	opactiy = 255;

	CCSprite *menuSprite = CCSprite::spriteWithFile( Skins::GetSkinName("Images/Menus/LevelMenu/MenuBt.png") );
	CCSprite *menuSpriteSelected = CCSprite::spriteWithFile( Skins::GetSkinName("Images/Menus/LevelMenu/MenuBt.png") );
	CCSprite *menuSpriteDisabled = CCSprite::spriteWithFile( Skins::GetSkinName("Images/Menus/LevelMenu/MenuBt.png") );
	_menuButton = FXMenuItemSprite::itemFromNormalSprite( menuSprite, menuSpriteSelected, menuSpriteDisabled,this, menu_selector( LevelTopRightMenu::Menu_ShowSubMenu ) );

	CCSprite *restartSprite = CCSprite::spriteWithFile( Skins::GetSkinName("Images/Menus/LevelMenu/RestartBt.png") );
	CCSprite *restartSpriteSelected = CCSprite::spriteWithFile( Skins::GetSkinName("Images/Menus/LevelMenu/RestartBt.png") );
	CCSprite *restartSpriteDisabled = CCSprite::spriteWithFile( Skins::GetSkinName("Images/Menus/LevelMenu/RestartBt.png") );
	_restartButton = FXMenuItemSprite::itemFromNormalSprite( restartSprite, restartSpriteSelected, restartSpriteDisabled,this, menu_selector( LevelTopRightMenu::Menu_Restart ) );

//    CCSprite *screenshotSprite = CCSprite::spriteWithFile( Skins::GetSkinName("Images/Menus/LevelMenu/ScreenshotBt.png") );
//	CCSprite *screenshotSpriteSelected = CCSprite::spriteWithFile( Skins::GetSkinName("Images/Menus/LevelMenu/ScreenshotBt.png") );
//	CCSprite *screenshotSpriteDisabled = CCSprite::spriteWithFile( Skins::GetSkinName("Images/Menus/LevelMenu/ScreenshotBt.png") );
//	_screenshotButton = FXMenuItemSprite::itemFromNormalSprite( screenshotSprite, screenshotSpriteSelected, screenshotSpriteDisabled,this, menu_selector( LevelTopRightMenu::Menu_Screenshot ) );

    
	menuSprite->setOpacity( opactiy );
	menuSpriteSelected->setOpacity( opactiy );
	menuSpriteDisabled->setOpacity( opactiy );

	restartSprite->setOpacity( opactiy );
	restartSpriteSelected->setOpacity( opactiy );
	restartSpriteDisabled->setOpacity( opactiy );

//    screenshotSprite->setOpacity( opactiy );
//	screenshotSpriteSelected->setOpacity( opactiy );
//	screenshotSpriteDisabled->setOpacity( opactiy );

	_menu = CCMenu::menuWithItems( _restartButton, _menuButton, NULL);
	_menu->alignItemsHorizontallyWithPadding( 15.0f );
    //_screenshotButton->setPosition( _screenshotButton->getPosition().x - 100.0f, _screenshotButton->getPosition().y );
    
	_menu->setPosition( ccp( 55.0f, -14.0f ) );

	_menuButtons = CCNode::node();
	_menuButtons->addChild(_menu );

    if ( FBTool::Get()->IsLogged() )
    {
//        CCMenu *screenshotMenu;
//        screenshotMenu = CCMenu::menuWithItem( _screenshotButton );
//        screenshotMenu->setPosition( ccp( -765.0f, -565.0f ));
    
//        _menuButtons->addChild( screenshotMenu );
    }
    
    _subMenu = LevelSubMenu::node( level );
    _subMenu->setIsVisible( true );
    _subMenu->SetParentMenu( _menu );
    _subMenu->setPosition( CCPointZero );

    _subMenuNode = CCNode::node();
    _subMenuNode->addChild( _subMenu );

    addChild( _subMenuNode );
}
//----------------------------------------------------------------------------------------
void LevelTopRightMenu::SetOrientation( GameTypes::LevelOrientation orient )
{
	CCPoint pos;
	switch( orient )
	{
	case GameTypes::eLevel_Horizontal :
		_menuButtons->setRotation( 0.0f );
		_menuButtons->setPosition( ccp( Config::GameSize.width / 2.0f - 150.0f, Config::GameSize.height / 2.0f - 30.0f));

		_subMenuNode->setPosition( LevelMenuPos );
		_subMenuNode->setRotation( 0.0f );
		break;

	case GameTypes::eLevel_Vertical :
		pos = ccp( -Config::GameSize.width / 2.0f / RATIO - 402.0f ,  Config::GameSize.height / 2.0f / RATIO + 142.0f );

		_menuButtons->setRotation( -90.0f );
		_menuButtons->setPosition( pos );

		_subMenuNode->setPosition( ccp( 0.0f, 630.0f ));
		_subMenuNode->setRotation( -90.0f );
		break;
	}
}
//----------------------------------------------------------------------------------------
LevelTopRightMenu::~LevelTopRightMenu()
{
	if ( _subMenu->getParent() )
		_subMenu->removeFromParentAndCleanup( true );

	removeAllChildrenWithCleanup( true );
}
//----------------------------------------------------------------------------------------
void LevelTopRightMenu::Menu_ShowSubMenu( CCObject* pSender )
{
    RLOG_S("MENU_LEVEL", "SHOW" );
 
    EnableMenu(false);
	_subMenu->Show();
	//Game::Get()->GetRuningLevel()->LevelPause();
}
//----------------------------------------------------------------------------------------
void LevelTopRightMenu::HideSubMenu()
{
    _subMenu->Menu_BackToGame(NULL);

}
//----------------------------------------------------------------------------------------
void LevelTopRightMenu::Menu_Screenshot( CCObject* pSender )
{
    CCDirector::sharedDirector()->makeScreenshot();
}
//----------------------------------------------------------------------------------------
void LevelTopRightMenu::Menu_Restart( CCObject* pSender )
{
	Game::Get()->RestartLevel();
}
//----------------------------------------------------------------------------------------
#ifdef BUILD_EDITOR
void LevelTopRightMenu::Menu_EditorToggle( CCObject* pSender )
{
//	if ( _listener != NULL )
	//	_listener->MenuEditorCallback();
}
//----------------------------------------------------------------------------------------
void LevelTopRightMenu::Menu_PlayGameToggle( CCObject* pSender )
{
	Level *level;
	
	level = Game::Get()->GetRuningLevel();
	if ( level->IsScheduled() )
	{
		level->LevelPause();
		GameEditor::Get()->SetEnabled( true );
	}
	else
	{
		GameEditor::Get()->SetEnabled( false );
		level->LevelRun();
	}
}
#endif
//----------------------------------------------------------------------------------------
void LevelTopRightMenu::EnableRestartButton( bool enable )
{
#ifdef BUILD_EDITOR
	return;
#endif

	_restartButton->setIsEnabled( enable );
}
//----------------------------------------------------------------------------------------
void LevelTopRightMenu::EnableMenuButton( bool enable )
{
#ifdef BUILD_EDITOR
	return;
#endif

	_menuButton->setIsEnabled( enable );
}
//----------------------------------------------------------------------------------------
void LevelTopRightMenu::EnableSubMenu(bool enable)
{
	_subMenu->SetEnabled(enable);
}
//----------------------------------------------------------------------------------------
void LevelTopRightMenu::EnableMenu(bool enable)
{
    _menu->setIsTouchEnabled(enable);
}




//----------------------------------------------------------------------------------------
// LevelSubMenu
//----------------------------------------------------------------------------------------
LevelSubMenu* LevelSubMenu::node( Level *level )
{

	LevelSubMenu *pRet = new LevelSubMenu(); 
	if ( pRet && pRet->init( level ) ) 
	{
		pRet->autorelease();
		return pRet;
	}
	else
	{
		delete pRet;
		pRet = NULL;
		return NULL;
	}
}
//----------------------------------------------------------------------------------------
LevelSubMenu::LevelSubMenu() 
{
}
//----------------------------------------------------------------------------------------
bool LevelSubMenu::init( Level *level )
{
	if ( ! LevelMenuBase::init() )
		return false;

	setIsTouchEnabled( true );

	CCMenu	*backMenu;

	//---------------------
	// Construct menu
	if 	(
		( Game::Get()->GetRuningLevelStatus() == GameStatus::eLevelStatus_Done ) ||
		( Game::Get()->GetRuningLevelStatus() == GameStatus::eLevelStatus_Skipped )
		)
		_levelMenu = CCMenu::menuWithItems
        (
         	GetMenuItem( "Menu", ( LevelMenuBase::FuncPtr ) &LevelSubMenu::Menu_MainMenu ),
//         GetMenuItem( "ChallengeMenu", ( LevelMenuBase::FuncPtr ) &LevelSubMenu::Menu_ChallengeMenu ),
         GetMenuItem( "SelectLevel", &LevelMenuBase::Menu_SelectLevel ),
         GetMenuItem( "NextLevel", &LevelMenuBase::Menu_NextLevel ),
         NULL
         );
	else
	{
		_levelMenu = CCMenu::menuWithItems
        (
         	GetMenuItem( "Menu", ( LevelMenuBase::FuncPtr ) &LevelSubMenu::Menu_MainMenu ),
//         GetMenuItem( "ChallengeMenu", ( LevelMenuBase::FuncPtr ) &LevelSubMenu::Menu_ChallengeMenu ),
         GetMenuItem( "SelectLevel", &LevelMenuBase::Menu_SelectLevel ),
         GetSkipButton( "SkipLevel", ( LevelMenuBase::FuncPtr ) &LevelSubMenu::Menu_SkipLevel, Config::LevelSkipCountVioletFont, Config::LevelSkipCountDisabledRedFont ),
         NULL
         );
	}
	addChild( _levelMenu , 10 );

    
  	//---------------------
	// Piggy sprite and bg
    CCSprite *piggySprite;
	CCSprite *bgSprite;
	
    bgSprite = CCSprite::spriteWithFile( Skins::GetSkinName( "Images/Menus/LevelMenu/menuBackground.png" ) );
	bgSprite->setPosition( ccp( 0.0f, 0.0f ));
	addChild( bgSprite, 5 );

    piggySprite = CCSprite::spriteWithFile( Skins::GetSkinName("Images/Menus/LevelMenu/piggyPaused.png") );
    piggySprite->setPosition( CCPoint( 0.0f, 105.0f ));
    addChild( piggySprite, 10 );
    
    
    //---------------------
	// Back and music menu
	AnyRequestNotify *notify;
    notify = AnyRequestNotify::Create();
    addChild( notify, 12 );
    
	//---------------------
	// Back and music menu
	_backToGameButton = GetMenuItem( "BackToGame", ( LevelMenuBase::FuncPtr ) &LevelSubMenu::Menu_BackToGame );
	backMenu = CCMenu::menuWithItem( _backToGameButton );
	addChild( backMenu, 10 );

	_musicMenu = MusicMenu::Create();
	addChild( _musicMenu, 20 );


	//---------------------
	// Set orientation
	if ( level->GetOrientation() == GameTypes::eLevel_Horizontal )
	{
        SetDeltaMove( MenuOnCenter_DeltaMove );
		
        _musicMenu->setPosition( ccp( -3.5f, -190.0f ));
        backMenu->setPosition( ccp( 275.0f, 115.0f ));

		_levelMenu->alignItemsHorizontallyWithPadding( 40.0f );
        _levelMenu->setPosition( ccp( 0.0f, -75.0f ));
	}
	else
	{
        SetDeltaMove( MenuOnCenter_DeltaMove_Vertical );

        _musicMenu->setPosition( ccp( -3.5f, -190.0f ));
      	backMenu->setPosition( ccp( 252.5f, 115.0f ));

		_levelMenu->alignItemsHorizontallyWithPadding( 45.0f );
        _levelMenu->setPosition( ccp( 0.0f, -75.0f ));
	}
    
   notify->setPosition( CCPoint(
                                 _levelMenu->getPosition().x - 125.0f,
                                 _levelMenu->getPosition().y + 32.0f ));
	return true;
}
//----------------------------------------------------------------------------------------
LevelSubMenu::~LevelSubMenu()
{
	removeAllChildrenWithCleanup( true );
	CCTouchDispatcher::sharedDispatcher()->removeDelegate( this );
}
//----------------------------------------------------------------------------------------
void LevelSubMenu::Menu_MainMenu( CCObject* pSender )
{
	Game::Get()->GetRuningLevel()->Quiting();
	Preloader::Get()->FreeResources();

	MainMenuScene::ShowScene();
}
//----------------------------------------------------------------------------------------
void LevelSubMenu::Menu_SkipLevel( CCObject* pSender )
{	
	Hide();
	LevelMenuBase::Menu_SkipLevel( pSender );
}
//----------------------------------------------------------------------------------------
void LevelSubMenu::Menu_Debug( CCObject* pSender )
{
	Game::Get()->ToggleDebugDraw();
}
//----------------------------------------------------------------------------------------
void LevelSubMenu::Menu_BackToGame( CCObject* pSender )
{
    RLOG_S("MENU_LEVEL", "HIDE" );
	Hide();

    _levelMenu->setIsTouchEnabled( false );
	_backToGameButton->setIsEnabled( false );
	_parentMenu->setIsTouchEnabled( true );
}
//----------------------------------------------------------------------------------------
void LevelSubMenu::registerWithTouchDispatcher()
{
	CCTouchDispatcher::sharedDispatcher()->addTargetedDelegate( this, Config::eMousePriority_LevelSubMenu, true );
}
//----------------------------------------------------------------------------------------
void LevelSubMenu::Show()
{
	_backToGameButton->setIsEnabled( true );
    _levelMenu->setIsTouchEnabled( true );

	Game::Get()->GetRuningLevel()->LevelPause();
    Game::Get()->GetRuningLevel()->SubMenuShow( true );
    HidingMenu::Show();
}
//----------------------------------------------------------------------------------------
void LevelSubMenu::Hide()
{
	HidingMenu::Hide();
    Game::Get()->GetRuningLevel()->SubMenuShow( false );
	Game::Get()->GetRuningLevel()->LevelRun();
}
//----------------------------------------------------------------------------------------
void LevelSubMenu::SetParentMenu( CCMenu* menu )
{
	_parentMenu = menu;
}

//----------------------------------------------------------------------------------------
void LevelSubMenu::SetEnabled(bool enable)
{
    if ( ! enable )
    {
        CCArray *buttons;
        buttons = _levelMenu->getChildren();
        
        CCObject *obj;
        CCMenuItemSprite *item;

        for ( unsigned int i = 0; i < buttons->count(); i++ )
        {
            obj = buttons->objectAtIndex( i );
            item =  (CCMenuItemSprite *) obj;
            if ( item )
            {
                CCSprite *sprite;
                CCSprite *copy;
                
                sprite = (CCSprite*) item->getNormalImage();
                copy = CCSprite::spriteWithTexture( sprite->getTexture() );
                item->setDisabledImage(copy);
                item->setIsEnabled(false);
            }
        }
        
        _musicMenu->SetEnable(false);
        _backToGameButton->setIsEnabled(false);
    }
}




//----------------------------------------------------------------------------------------
// LevelFinishMenu
//----------------------------------------------------------------------------------------
CCPoint LevelFinishMenu::_sPiggySpirtePos( 0.0f, 60.0f );
LevelFinishMenu* LevelFinishMenu::node( LevelFinishMenuState state, GameTypes::LevelOrientation orient, const CCPoint &moveDelta )
{
	LevelFinishMenu *pRet = new LevelFinishMenu( orient );
	if (pRet && pRet->Init(  state, moveDelta ))
	{ 
		pRet->autorelease();
		return pRet;
	}
	else
	{
		delete pRet;
		pRet = NULL;
		return NULL;
	}
}
//----------------------------------------------------------------------------------------
LevelFinishMenu::LevelFinishMenu( LevelOrientation orient )
{
	_showAction = NULL;
    _levelOrient = orient;
}
//----------------------------------------------------------------------------------------
LevelFinishMenu::~LevelFinishMenu()
{
	if ( _showAction )
		_showAction->release();

	removeAllChildrenWithCleanup( true );
}
//----------------------------------------------------------------------------------------
bool LevelFinishMenu::Init( LevelFinishMenuState state, const CCPoint &moveDelta )
{
    SetDeltaMove( moveDelta );
    
	if ( ! LevelMenuBase::init() )
		return false;
	
    switch( state )
    {
        case eLevelFinishMenu_LevelFailed:
            SetFailedMenu();
            break;

        case eLevelFinishMenu_LevelFinished :
            SetCompletedMenu();
            break;

        case eLevelFinishMenu_LevelFinishedFirstTime :
            SetCompletedFirstTimeMenu();
            break;
            
        default:
            ;
    }
    
	if ( _levelOrient == GameTypes::eLevel_Horizontal )
	{
		_menu->alignItemsHorizontallyWithPadding( 25.0f );
		_menu->setPosition( ccp( 0.0f, -160.0f ));
        
        if ( Game::Get()->GetGameStatus()->GetCurrentLevelStatus() == GameStatus::eLevelStatus_Done
            || state == eLevelFinishMenu_LevelFinishedFirstTime
            || state == eLevelFinishMenu_LevelFinished )
        {
            SetDeltaMove( MenuOnRight_DeltaMove );
        }
        else
        {
            SetDeltaMove( MenuOnCenter_DeltaMove );
        }
        
        if ( state == eLevelFinishMenu_LevelFinishedFirstTime )
            _menu->setPosition( ccp( 0.0f, -185.0f ));
	}
	else
	{
		_menu->alignItemsHorizontallyWithPadding( 25.0f );
        SetDeltaMove( MenuOnCenter_DeltaMove_Vertical );
        
        if ( state == eLevelFinishMenu_LevelFinishedFirstTime )
            _menu->setPosition( ccp( 0.0f, -175.0f ));
        else
            _menu->setPosition( ccp( 0.0f, -165.0f ));

	}


	addChild( _menu, 10 );
	setIsTouchEnabled( true );

	_showAction = CCMoveBy::actionWithDuration( 0.5f, _showPoint );
	_showAction->retain();

	return true;
}
//----------------------------------------------------------------------------------------
void LevelFinishMenu::SetCompletedFirstTimeMenu()
{
    //---------------------
    // Setting background
    CCSprite *background;
    CCSprite *coinsPlace;
    CCSprite *timePlace;
    
    coinsPlace = CCSprite::spriteWithFile( Skins::GetSkinName( "Images/Menus/Scores/coinsPlace.png" ) );
    timePlace  = CCSprite::spriteWithFile( Skins::GetSkinName( "Images/Menus/Scores/timePlace.png" ) );
    
    coinsPlace->setPosition( CCPoint( 0.0f, -15.0f + ScoreCounter::_yCoins ));
    timePlace->setPosition( CCPoint( 7.5f, 140.0f  - 30.0f ));
    
    addChild( coinsPlace, 10 );
    addChild( timePlace, 10 );
    
    background = CCSprite::spriteWithFile( Skins::GetSkinName( "Images/Menus/LevelMenu/menuBackground.png" ) );
  	background->setPosition( ccp( 0.0f, 0.0f ));
	addChild( background, 3 );

    //---------------------
    // Setting menu
    _menu = CCMenu::menuWithItems(
                                  GetMenuItem( "GreenSelectLevel", &LevelMenuBase::Menu_SelectLevel ),
                                  GetMenuItem( "GreenRestart", &LevelMenuBase::Menu_RestartLevel ),
                                  GetMenuItem( "GreenNextLevel", &LevelMenuBase::Menu_NextLevel ),
                                  NULL );
}
//----------------------------------------------------------------------------------------
void LevelFinishMenu::SetCompletedMenu()
{
    //---------------------
    // Setting piggy
    CCSprite *piggy;
    piggy = CCSprite::spriteWithFile( Skins::GetSkinName( "Images/Menus/LevelMenu/piggyCompleted.png" ) );
    piggy->setPosition( _sPiggySpirtePos );
    addChild( piggy, 10 );

    
    //---------------------
    // Setting background
    CCSprite *background;
    
    if ( _levelOrient == GameTypes::eLevel_Horizontal )
        background = CCSprite::spriteWithFile( Skins::GetSkinName( "Images/Menus/LevelMenu/menuBackground.png" ) );
    else
        background = CCSprite::spriteWithFile( Skins::GetSkinName( "Images/Menus/LevelMenu/menuBackground.png" ) );

  	background->setPosition( ccp( 0.0f, 0.0f ));
	addChild( background, 5 );

    //---------------------
    // Setting menu

	_menu = CCMenu::menuWithItems(
                                      GetMenuItem( "GreenSelectLevel", &LevelMenuBase::Menu_SelectLevel ),
                                      GetMenuItem( "GreenRestart", &LevelMenuBase::Menu_RestartLevel ),
                                      GetMenuItem( "GreenNextLevel", &LevelMenuBase::Menu_NextLevel ),
                                      NULL );
}
//----------------------------------------------------------------------------------------
void LevelFinishMenu::SetFailedMenu()
{
    //---------------------
    // Setting piggy
    CCSprite *piggy;
    piggy = CCSprite::spriteWithFile( Skins::GetSkinName( "Images/Menus/LevelMenu/piggyFailed.png" ) );
    piggy->setPosition( _sPiggySpirtePos );
    addChild( piggy, 10 );
    
    //---------------------
    // Setting background
    CCSprite *background;
    background = CCSprite::spriteWithFile( Skins::GetSkinName( "Images/Menus/LevelMenu/menuBackground.png" ) );
  	background->setPosition( ccp( 0.0f, 0.0f ));
	addChild( background, 5 );

    //---------------------
    // Setting background
    if (( Game::Get()->GetGameStatus()->GetCurrentLevelStatus() == GameStatus::eLevelStatus_Done ) ||
        ( Game::Get()->GetGameStatus()->GetCurrentLevelStatus() == GameStatus::eLevelStatus_Skipped ))
    {
        _menu = CCMenu::menuWithItems(
                                      GetMenuItem( "RedSelectLevel", &LevelMenuBase::Menu_SelectLevel ),
                                      GetMenuItem( "RedRestart", &LevelMenuBase::Menu_RestartLevel ),
                                      GetMenuItem( "RedNextLevel", &LevelMenuBase::Menu_NextLevel ),
                                      NULL );
    }
    else
    {
        _menu = CCMenu::menuWithItems(
                                      GetMenuItem( "RedSelectLevel", &LevelMenuBase::Menu_SelectLevel ),
                                      GetMenuItem( "RedRestart", &LevelMenuBase::Menu_RestartLevel ),
                                      GetSkipButton( "RedSkipLevel", &LevelMenuBase::Menu_SkipLevel, Config::LevelSkipCountRedFont, Config::LevelSkipCountDisabledRedFont ),
                                      NULL );
    }
}
//----------------------------------------------------------------------------------------
void LevelFinishMenu::registerWithTouchDispatcher()
{
	CCTouchDispatcher::sharedDispatcher()->addTargetedDelegate( this, Config::eMousePriority_LevelSubMenu, true );
}
//----------------------------------------------------------------------------------------
void LevelFinishMenu::Show()
{
	_enabled = true;
	if ( ! _showAction )
		return;

	runAction( _showAction );
	_showAction->release();
	_showAction = NULL;
}
//----------------------------------------------------------------------------------------




//----------------------------------------------------------------------------------------
// MusicMenu
//----------------------------------------------------------------------------------------
MusicMenu* MusicMenu::Create()
{
	MusicMenu *mm;
	mm = new MusicMenu();

	if ( mm )
	{
		mm->autorelease();
		mm->Init();
		return mm;
	}
	CC_SAFE_DELETE( mm );
	return NULL;
}
//----------------------------------------------------------------------------------------
MusicMenu::MusicMenu()
{
}
//----------------------------------------------------------------------------------------
void MusicMenu::Init()
{
	CCSprite *sndImgOn			= CCSprite::spriteWithFile( Skins::GetSkinName( "Images/Menus/SoundMenu/btSoundOn.png" ));
	CCSprite *sndImgOnPress		= CCSprite::spriteWithFile( Skins::GetSkinName( "Images/Menus/SoundMenu/btSoundOnPress.png" ));
	CCSprite *sndImgOff			= CCSprite::spriteWithFile( Skins::GetSkinName( "Images/Menus/SoundMenu/btSoundOff.png" ));
	CCSprite *sndImgOffPress	= CCSprite::spriteWithFile( Skins::GetSkinName( "Images/Menus/SoundMenu/btSoundOnPress.png" ));

	CCMenuItemSprite *sndOn		= CCMenuItemSprite::itemFromNormalSprite( sndImgOn, sndImgOnPress, this, menu_selector( MusicMenu::ToggleMusic ));
	CCMenuItemSprite *sndOff	= CCMenuItemSprite::itemFromNormalSprite( sndImgOff, sndImgOffPress, this, menu_selector( MusicMenu::ToggleMusic ));


	CCSprite *sndFxImgOn		= CCSprite::spriteWithFile( Skins::GetSkinName( "Images/Menus/SoundMenu/btSoundFxOn.png" ));
	CCSprite *sndFxImgOnPress	= CCSprite::spriteWithFile( Skins::GetSkinName( "Images/Menus/SoundMenu/btSoundFxOnPress.png" ));
	CCSprite *sndFxImgOff		= CCSprite::spriteWithFile( Skins::GetSkinName( "Images/Menus/SoundMenu/btSoundFxOff.png" ));
	CCSprite *sndFxImgOffPress	= CCSprite::spriteWithFile( Skins::GetSkinName( "Images/Menus/SoundMenu/btSoundFxOnPress.png" ));
	
	CCMenuItemSprite *sndFxOn	= CCMenuItemSprite::itemFromNormalSprite( sndFxImgOn, sndFxImgOnPress, this, menu_selector( MusicMenu::ToggleEffects ));
	CCMenuItemSprite *sndFxOff	= CCMenuItemSprite::itemFromNormalSprite( sndFxImgOff, sndFxImgOffPress, this, menu_selector( MusicMenu::ToggleEffects ));

	_musicButton	= FXMenuItemToggle::itemWithTarget( this, menu_selector( MusicMenu::ToggleMusic ), sndOn, sndOff, NULL );
	_effectsButton	= FXMenuItemToggle::itemWithTarget( this, menu_selector( MusicMenu::ToggleEffects ), sndFxOn, sndFxOff, NULL );

	GameStatus* gameStatus;
	gameStatus = Game::Get()->GetGameStatus();

	//--- Set music
	if ( gameStatus->GetMusicMute() ) 
		_musicButton->setSelectedIndex( 1 );
	else
		_musicButton->setSelectedIndex( 0 );


	//--- Set effects
	if ( gameStatus->GetEffectsMute() ) 
		_effectsButton->setSelectedIndex( 1 );
	else
		_effectsButton->setSelectedIndex( 0 );

	_sndMenu = CCMenu::menuWithItems( _musicButton, _effectsButton, NULL );
	_sndMenu->alignItemsHorizontallyWithPadding( 35.0f );
	_sndMenu->setPosition( ccp( 0.0f, 0.0f ));
	addChild( _sndMenu );
}
//----------------------------------------------------------------------------------------
void MusicMenu::ToggleMusic( CCObject *sender )
{
	if ( ! Game::Get()->GetGameStatus()->GetMusicMute() )
    {
		SoundEngine::Get()->GetBackgroundMusic().PauseMusic();
        RLOG_SS( "SOUND","MUSIC", "OFF" );
    }
    else
    {
        SoundEngine::Get()->GetBackgroundMusic().ResumeMusic();
        RLOG_SS( "SOUND","MUSIC", "ON" );
    }
}
//----------------------------------------------------------------------------------------
void MusicMenu::ToggleEffects( CCObject *sender )
{
    if( Game::Get()->GetGameStatus()->GetEffectsMute() )
    {
        RLOG_SS("SOUND","FX","ON");
        Game::Get()->GetGameStatus()->SetEffectsMute( false );
        SoundEngine::Get()->PlayEffect( SoundEngine::eSoundButtonClick );
    }
    else
    {
        RLOG_SS("SOUND","FX","OFF");
        Game::Get()->GetGameStatus()->SetEffectsMute( true );
    }
}
//----------------------------------------------------------------------------------------
void MusicMenu::SetEnable(bool enable)
{
    _effectsButton->setIsEnabled(enable);
    _musicButton->setIsEnabled(enable);
}
//----------------------------------------------------------------------------------------
void MusicMenu::SetVisibleEffectsButton( bool visible )
{

	_effectsButton->setIsVisible( visible );
	_effectsButton->setIsEnabled( visible );
}
//----------------------------------------------------------------------------------------
MusicMenu::~MusicMenu()
{
	_musicButton->removeAllChildrenWithCleanup( true );
	_effectsButton->removeAllChildrenWithCleanup( true );
	_sndMenu->removeAllChildrenWithCleanup( true );
	removeAllChildrenWithCleanup( true );
}





//----------------------------------------------------------------------------------------
// LevelFinishMenuNode
//----------------------------------------------------------------------------------------
LevelFinishMenuNode* LevelFinishMenuNode::node( Level* level )
{

	LevelFinishMenuNode *ret;
	ret = new LevelFinishMenuNode();

	if ( ret )
	{
		ret->autorelease();
		ret->Init( level );
		return ret;
	}

	CC_SAFE_DELETE(ret);
	return NULL;
}
//----------------------------------------------------------------------------------------
LevelFinishMenuNode::LevelFinishMenuNode()
{
    _showSeq = NULL;
}
//----------------------------------------------------------------------------------------
LevelFinishMenuNode::~LevelFinishMenuNode()
{
    if ( _showSeq )
        _showSeq->release();
}
//----------------------------------------------------------------------------------------
void LevelFinishMenuNode::Init( Level* level )
{
    GameStatus::LevelStatus levelStatus;
    levelStatus = Game::Get()->GetGameStatus()->GetLevelStatus( level->GetLevelEnum() );
    
    //------------
    // Menu completed
    if ( levelStatus != GameStatus::eLevelStatus_Done )
        _menuCompleted	= LevelFinishMenu::node( LevelFinishMenu::eLevelFinishMenu_LevelFinishedFirstTime, level->GetOrientation(), MenuOnRight_DeltaMove );
    else
        _menuCompleted	= LevelFinishMenu::node( LevelFinishMenu::eLevelFinishMenu_LevelFinished, level->GetOrientation(), MenuOnRight_DeltaMove );
    
    //------------
    // Menu failed
    if ( levelStatus != GameStatus::eLevelStatus_Done )
        _menuFailed	= LevelFinishMenu::node( LevelFinishMenu::eLevelFinishMenu_LevelFailed, level->GetOrientation(), MenuOnCenter_DeltaMove );
    else
        _menuFailed	= LevelFinishMenu::node( LevelFinishMenu::eLevelFinishMenu_LevelFailed, level->GetOrientation(), MenuOnRight_DeltaMove );

	
	_menuCompleted->setPosition( CCPointZero );
	_menuFailed->setPosition( CCPointZero );

	addChild( _menuCompleted );
	addChild( _menuFailed );
    
    
    _showSeq = (CCActionInterval*)(CCSequence::actions
                                   (
                                    CCDelayTime::actionWithDuration( 2.55f ),
                                    CCCallFunc::actionWithTarget(this, callfunc_selector( LevelFinishMenuNode::ShowDelay )),
                                    NULL ));

    _showSeq->retain();
}
//----------------------------------------------------------------------------------------
void LevelFinishMenuNode::SetOrient( GameTypes::LevelOrientation orient )
{
	switch( orient )
	{
	case GameTypes::eLevel_Horizontal :
		setPosition( LevelMenuPos );
		setRotation( 0.0f );
		break;

	case GameTypes::eLevel_Vertical :
		setPosition( ccp( 0.0f, 630.0f ));
		setRotation( -90.0f );
		break;
	}
}
//----------------------------------------------------------------------------------------
void LevelFinishMenuNode::Show( GameTypes::LevelState state )
{
    _menuState = state;
	runAction( _showSeq );

#ifndef INTRO_POINTER
	//ShowDelay( this, (void*) state );
#endif
}
//----------------------------------------------------------------------------------------
void LevelFinishMenuNode::ShowDelay( CCNode *node)
{
    if ( _showSeq )
    {
        _showSeq->release();
        _showSeq = NULL;
    }

	switch( _menuState )
	{
	case GameTypes::eLevelState_Finished :
		SoundEngine::Get()->PlayEffect( SoundEngine::eSoundLevelFinished );
		_menuCompleted->Show();
		break;

	case GameTypes::eLevelState_Failed :
		SoundEngine::Get()->PlayEffect( SoundEngine::eSoundLevelFailed );
		_menuFailed->Show();
		break;

	default:
		unAssertMsg(LevelFinishMenuNode, false, ("Unknow level state: %d", state ));
	}
}




//----------------------------------------------------------------------------------------
// Skip Menu Item
//----------------------------------------------------------------------------------------
SkipMenuItem* SkipMenuItem::Create( CCNode *target, const char *btName, LevelMenuBase::FuncPtr fn, const char *fontFile, const char *disabledFontFile )
{

	string file;
	string fileHold;
	string fileDisabled;

	file.append("Images/Menus/LevelMenu/bt" );
	file.append( btName );
	file.append( ".png" );

	file = Skins::GetSkinName( file.c_str() );

	fileDisabled.append("Images/Menus/LevelMenu/bt" );
	fileDisabled.append( btName );
	fileDisabled.append( "Disabled.png" );
	fileDisabled = Skins::GetSkinName( fileDisabled.c_str() );

	fileHold.append("Images/Menus/LevelMenu/bt" );
	fileHold.append( btName );
	fileHold.append( ".png" );
	fileHold = Skins::GetSkinName( fileHold.c_str() );

	CCSprite *btSprite = CCSprite::spriteWithFile( file.c_str() );
	CCSprite *btSpriteHold = CCSprite::spriteWithFile( fileHold.c_str() );
	CCSprite *btSpriteDisabled = CCSprite::spriteWithFile( fileDisabled.c_str() );
	AnimerSimpleEffect::Get()->Anim_ButtonPulse( btSpriteHold );

	//------------------------
	// Real construct
	SkipMenuItem *item = new SkipMenuItem();
	if ( item && item->initFromNormalSprite( btSprite, btSpriteHold, btSpriteDisabled, target, (SEL_MenuHandler) fn ))
	{
		item->autorelease();
		item->Init( btName, fontFile, disabledFontFile );
		return item;
	}

	CC_SAFE_DELETE(item);
	return NULL;
}
//----------------------------------------------------------------------------------------
SkipMenuItem::~SkipMenuItem()
{
	removeAllChildrenWithCleanup( true );
}
//----------------------------------------------------------------------------------------
void SkipMenuItem::Init( const char *btName, const char *fontFile, const char *disabledFontFile )
{

	CCLabelBMFont *label;
	CCSprite *spriteNap;
	CCPoint labelPos;

	labelPos.x = 132.5f;
	labelPos.y = 27.5f;

	//----------------------------------------
	// Construct skip button
	stringstream ss;
	ss << Game::Get()->GetGameStatus()->GetSkipsLeft();
	
	

	if ( !( Game::Get()->GetGameStatus()->GetSkipsLeft() > 0 ) ||  Game::Get()->GetGameStatus()->NoMoreSkips() )
	{
		label = CCLabelBMFont::labelWithString( ss.str().c_str(), disabledFontFile );
		setIsEnabled( false );
	}
	else
		// CCLabelBMFont is leaking memory leak fixme
		label = CCLabelBMFont::labelWithString( ss.str().c_str(), fontFile );

	label->setPosition( ccp( labelPos.x + 1.5f, labelPos.y + 1.5f ));

	string skipNapFile;
	skipNapFile.append( "Images/Menus/LevelMenu/bt" );
	skipNapFile.append( btName );
	skipNapFile.append( "Nap");
	if ( !( Game::Get()->GetGameStatus()->GetSkipsLeft() > 0 ) ||  Game::Get()->GetGameStatus()->NoMoreSkips() )
		skipNapFile.append( "Disabled");
	
	skipNapFile.append( ".png");
	skipNapFile = Skins::GetSkinName( skipNapFile.c_str() );

	spriteNap = CCSprite::spriteWithFile( skipNapFile.c_str() );
	spriteNap->setPosition( labelPos );

	addChild( spriteNap, 5 );
	addChild( label, 10 );
}
