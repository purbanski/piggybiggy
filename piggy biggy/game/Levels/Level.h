#ifndef __LEVEL_H__
#define __LEVEL_H__

#include <vector>
#include <list>
#include "cocos2d.h"
#include "ContactSolver.h"
#include "World.h"
#include "BlockFinder.h"
#include "Blocks/puBorders.h"
#include "ParticleEngine.h"
#include "GLES-Render.h"
#include "ScaleScreenLayer.h"
#include "LevelMessage.h"
#include "LevelAnimator.h"
#include "ScoreCounter.h"
#include "Scenes/UnlockPocketScene.h"
#include "BuyChecker.h"
#include "Facebook/FBPromoNode.h"

#ifdef BUILD_EDITOR
#include "Debug/DebugNode.h"
#endif

#ifdef INTRO_POINTER
#include "Components/IntroPointer.h"
#endif

USING_NS_CC;
using namespace std;


//----------------------------------------------------------------------------------------------
class DemoStep
{
public:
	typedef enum
	{
		eDestroyBlock = 1,
		eApplyForce
	} StepType;

	DemoStep( int time, StepType type, puBlock* block );
	DemoStep( int time, StepType type, puBlock* block, b2Vec2 force );

	void RunStep();
	int GetTime() { return _time; }

private:
	StepType _type;
	puBlock	*_block;
	b2Vec2	_force;
	int		_time;
};
//----------------------------------------------------------------------------------------------
class DebugDraw;
class LevelBlockOperator;
class LevelTopRightMenu;
class MenuControls;
class LevelFinishMenuNode;
class LevelOrientMsg;
//----------------------------------------------------------------------------------------------
class Level : public CCLayer, public World, public GameEventsListener
{
public:
	virtual ~Level();

	void ResetContainers();

	virtual bool init();
	virtual void Step( cocos2d::ccTime dt );

	virtual void LevelRun();
	virtual void LevelPause();
    
	// CCnode
	virtual void onEnter();
	virtual void onExit();
	virtual void onEnterTransitionDidFinish();

	void MsgTick();
	void UpdateSprites();
	void Center();
    void SubMenuShow( bool show );
	
	// Game events
	virtual void GameCoinCollected( puBlock *piggy );
	virtual void GameBlockExplode( b2Vec2 pos );
	virtual void GameMoneyExplode( b2Vec2 pos );
	virtual void GameThiefCaught( puBlock *policeman );
	virtual void GameCoinStolen( puBlock *thief );
	virtual void GamePiggyStolen( puBlock *piggyBank, puBlock *thief );

	virtual void GameCoinSilverCollected( b2Vec2 pos );
	virtual void GameCoinExplode();
	virtual void GameOutOfBorder();
	virtual void GameCustomAction_ContactBegin( puBlock *aBlock, puBlock *bBlock, b2Contact *contact );
	virtual void GameCustomAction_ContactEnd( puBlock *aBlock, puBlock *bBlock, b2Contact *contact );

	//Touch related	
	virtual void ccTouchesBegan(CCSet *pTouches, CCEvent *pEvent);
	virtual void ccTouchesMoved(CCSet *pTouches, CCEvent *pEvent);
	virtual void ccTouchesEnded(CCSet *pTouches, CCEvent *pEvent);
	virtual void ccTouchesCancelled(CCSet *pTouches, CCEvent *pEvent);

	virtual void registerWithTouchDispatcher();

	// Accelerometr - needed to set orientation
//	virtual void didAccelerate(CCAcceleration* pAccelerationValue);

	void ToggleDebugDraw();
	void EnableDebugDraw( bool enable );
	bool IsDebugEnabled();
	bool IsScheduled() { return _isScheduled; }
	bool IsEnding();
	bool IsDemoRunning() { return _demoRuning; }
	GameTypes::LevelEnum GetLevelEnum(){ return _levelEnum; }
	ParticleEngine*	GetParticleEngine() { return &_particleEngine; }
	GameTypes::LevelState GetLevelState() { return _currentState; }

	virtual void Quiting();

	void DisplayMsg( const char* str, const b2Vec2& pos );
	void DisplayMsg( const char* str );

	GameTypes::LevelOrientation GetOrientation();
	
	// animation
	BlockSet &GetAnimableBlocks() { return _animableBlocks; }
	BlockSet &GetRolledAnimableBlocks() { return _rolledAnimableBlocks; }

	void RemoveFromContainers( puBlock *block );
	void UpdateBlockContainers();
    
    void ScreenshotFlash();
    GameTypes::LevelTag GetLevelTag() { return _levelTag; }
    void SetViewMode( bool enabled );
    
    void ShowSkipLevelIntro();
    void ShowSubMenu(bool show);
    void SkipLevelIntroFinished();
    
protected:
	Level( GameTypes::LevelEnum levelEnum, bool viewMode, bool sleepAllowed = true );

	virtual void ConstFinal();
    void ConstFinalMinimal();
    
	void AddSprites();

	float GetScreenMiddleX();
	float GetScreenMiddleY();
	void DisplayLevelStartMsg();
	void DisplayLevelFinishedMsg();

	virtual void AdjustOldScreen();
	bool IsDone();

	void BlocksStep();
	void BlocksLevelQuit();
   	void BlocksLevelPlayFinished();
    void SlowDownPiggyBank();
    void BlocksLevelExit();
	void SetOrient();

private:
	void CheckDelayedStateChange();
	void LevelCompleted();
	void LevelCompletedPre();
	void LevelFailed();
	void LevelFailedPre( bool startPiggAnim = true );

	void SetDelayedStateChange( LevelState state, int delay );
	void SetPocketSkin();
	void SetUpScoreCounter();
	void DisplayLevelName();
    
    void ScoreShown();
	
protected:
    GameTypes::LevelType    _levelType;
	GameTypes::LevelEnum	_levelEnum;
    GameTypes::LevelTag     _levelTag;
    
    unsigned int            _levelIndex;
    
	string					_msgStart;
	string					_msgFinish;

	ScoreCounter			*_scoreCounter;

	ContactSolver			_contactSolver;
	CCSprite				*_background;

	LevelBlockOperator		*_blockOperator;
	int						_coinCount;
	puBorder				_border;
	bool					_isScheduled;
	bool                    _showingSkipIntro;
    
	//typedef list<LevelMsg> MessageList;
	LevelMsgManager			_msgManager;

	int						_demoCounter;
	int						_msgCounter;
	ParticleEngine			_particleEngine;
	bool					_styleSet;

	BlockSet				_animableBlocks;
	BlockSet				_rolledAnimableBlocks;
	BlockSet				_bouyentBlocks;
	BlockSet				_stepableBlocks;
	BlockSet				_quitableBlocks;
	BlockSet				_finishableBlocks;
  	BlockSet				_exitableBlocks;
    BlockSet                _piggyBankDynamicBlocks;


	bool					_demoRuning;
    bool                    _viewMode;
	LevelTopRightMenu		*_menu;
	LevelFinishMenuNode		*_levelFinishMenu;

	GameTypes::LevelOrientation	_levelOrient;
	LevelOrientMsg*				_levelOrientMsg;

//    FBPromoNode            *_fbPromoNode;
    
private:
	ScaleScreenLayer		*_scaleScreenLayer;

	LevelState				_currentState;
	int						_delayCounter;
	
	DebugDraw				*_debugDraw;
	
	LevelAnimatorIdleBlocks		_animatorIdleBlocks;
	LevelAnimatorActiveBlocks	_animatorActiveBlocks;
	LevelAnimatorEvents			_animatorEvents;

    CCSprite                    *_screenshotFlash;
    //	UnlockPocketNode		*_openCurrentPocketNode;
	//  UnlockPocketNode		*_openNextPocketNode;
	bool					_quiting;
    bool                    _subMenuShown;
    
	friend class			ScoreCounter;
#ifdef BUILD_EDITOR
public:
	b2Vec2 AdjustWithLevelPosition( const b2Vec2 &touch );

	CCSprite *GetBackground() { return _background; }
	void ToggleSize();
private: 
	void AddLevelEnum();
	friend class EditorBlockOperator;
#endif

#ifdef BUILD_TEST

public:
	void Unfailable() { _unfailable = true; }
	int GetCoinCount() { return _coinCount; }
private:
	bool	_unfailable;

#endif
    
#ifdef INTRO_POINTER
private:
    IntroPointer    *_introPointer;
#endif
    
};
//--------------------------------------------------------------------------
class LevelVertical : public Level
{
public:
	virtual ~LevelVertical(){};
	bool init();
	virtual void didAccelerate(CCAcceleration* pAccelerationValue);

protected:
	LevelVertical( GameTypes::LevelEnum levelEnum, bool viewMode );
};
//--------------------------------------------------------------------------
class LevelDemo : public Level
{
public:
	virtual ~LevelDemo(){};
	bool init();
	
	virtual void ConstFinal();
	virtual void Step( cocos2d::ccTime dt );
	void DemoTick();

protected:
	LevelDemo( GameTypes::LevelEnum levelEnum, bool viewMode );
	typedef list<DemoStep>	DemoStepList;
	DemoStepList			_demoList;
};

#endif

