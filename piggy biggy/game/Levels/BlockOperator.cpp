#include "BlockOperator.h"
#include "Game.h"
#include "Tools.h"

//-------------------------------------------------------------------------------------------
LevelBlockOperator::LevelBlockOperator()
{

}
//-------------------------------------------------------------------------------------------
LevelBlockOperator::~LevelBlockOperator()
{
	_touchedMap.clear();
	_activeBlockMap.clear();
}
//-------------------------------------------------------------------------------------------
void LevelBlockOperator::MouseDown( CCTouch *touch )
{
	//D_PTR( touch )

	if ( _touchedMap.count( touch ) ==  0 )
	{
		b2Vec2 position;
		puBlock *block;

		position = Tools::TouchAdjust( touch );
		block = BlockTouched( position );

		_touchedMap[touch] = TouchInfo();
		_touchedMap[touch]._touchPosition = position;

		if ( block )
		{
			_touchedMap[touch]._block		= block;
			OperateTouch( touch );
		}		
	}
	//DebugNode::Get()->PrintAdd( "Down %p / %d", touch, _touchedMap.size() );
}
//-------------------------------------------------------------------------------------------
void LevelBlockOperator::MouseMove( CCTouch *touch )
{
	//DebugNode::Get()->PrintAdd( "Move %p / %d", touch, _touchedMap.size() );

	//D_PTR( touch )

	MouseDown( touch );

	b2Vec2 position;
	position = Tools::TouchAdjust( touch );

	if ( _touchedMap[touch]._touchPosition == position )
	{
	// I didn't move
		return;
	}

	if ( _touchedMap[touch]._mouseJoint )
	{
		_touchedMap[touch]._mouseJoint->SetTarget( position );
		_touchedMap[touch]._touchPosition =  position;
		return;
	}

	if ( ! MouseLongMove( _touchedMap[touch]._touchPosition, position, touch ))
	{
		puBlock *block;
		block = BlockTouched( position );
				
		if ( block )
		{
			_touchedMap[touch]._block = block;
			
			if (( block->GetBlockType() == GameTypes::eCutable ) ||
				( block->GetBlockType() == GameTypes::eDestroyable ) )
				OperateMove( touch );
		}
		
		ProcessActiveBlocks( touch, block );
	}
	
	_touchedMap[touch]._touchPosition = position;
}
//-------------------------------------------------------------------------------------------
void LevelBlockOperator::MouseUp( CCTouch* touch )
{
	D_PTR( touch )

	// this is just for activalbe - and we do not check if it was touched
	// as it would failed if you touch it, move outside, and untouch it
	OperateFinish( touch ); 
	_touchedMap.erase( touch );
	//DebugNode::Get()->PrintAdd( "Up %p / (%f, %f) %d", touch,  touch->locationInView().x, touch->locationInView().y, _touchedMap.size() );
}
//-------------------------------------------------------------------------------------------
void LevelBlockOperator::OperateTouch( CCTouch *touch )
{

	TouchInfo info;
	info = _touchedMap[touch];

	switch ( info._block->GetBlockType() )
	{
	case GameTypes::eDestroyable :
		info._block->DestroyFragile();
		break;

	case GameTypes::eMoveable :
		SetUpMouseJoint( touch );
		break;

	case GameTypes::eCutable :
		info._block->Cut();
		break;

	case GameTypes::eActivable :
		if ( _activeBlockMap.count( touch ))
		{
			unAssertMsg(BlockOperator, false, ("Touch already connected with antother active block"));
		}

		info._block->Activate();
		_activeBlockMap[ touch ] = info._block;

		break;

	case GameTypes::eAirBubble :
		info._block->Destroy();
		break;


	case GameTypes::ePiggyBank :
		//SetUpMouseJoint( touch );
		//break;

	case GameTypes::eCoin :
	case GameTypes::eCoinSilver :
	case GameTypes::eCop :
	case GameTypes::eOther :
	case GameTypes::eIron :
	case GameTypes::eThief :
#ifdef BUILD_TEST
            SetUpMouseJoint( touch );
#endif
//#ifdef BUILD_EDITOR
//		SetUpMouseJoint( touch );
//#else
//#	if defined _DEBUG && !defined TEST_BUILD
//		SetUpMouseJoint( touch );
//#	endif
//#endif
		break;

	case GameTypes::eDynamic :
		break;

	default :
#ifdef BUILD_EDITOR
		SetUpMouseJoint( touch );
#endif
		;
	}
}
//-------------------------------------------------------------------------------------------
void LevelBlockOperator::OperateMove( CCTouch *touch )
{

	TouchInfo info;
	info = _touchedMap[touch];

	switch ( info._block->GetBlockType() )
	{
	case GameTypes::eDestroyable :
		info._block->DestroyFragile();
		break;

	case GameTypes::eMoveable :
		SetUpMouseJoint( touch );
		break;

	case GameTypes::eCutable :
		info._block->Cut();
		break;

	case GameTypes::eAirBubble :
		info._block->Destroy();
		break;


	default :
#ifdef BUILD_EDITOR
//		SetUpMouseJoint( touch );
#endif
		;
	}
}
//-------------------------------------------------------------------------------------------
void LevelBlockOperator::OperateFinish( CCTouch *touch )
{

	if ( _touchedMap[touch]._mouseJoint  )
	{
		Game::Get()->GetRuningLevel()->GetWorld()->DestroyJoint( _touchedMap[touch]._mouseJoint );
	}

	if ( _activeBlockMap.count( touch ))
	{
		puBlock *block;
		block = _touchedMap[touch]._block;
		if ( block )
			block->Deactivate();

		_activeBlockMap.erase( touch );
	}
}
//-------------------------------------------------------------------------------------------
void LevelBlockOperator::SetUpMouseJoint( CCTouch* touch )
{

	TouchInfo info;
	info = _touchedMap[touch];

	if ( ! info._block || ! info._block->GetBody() )
		return;

	if ( ! info._mouseJoint )
	{
		b2MouseJointDef jointDef;
		jointDef.bodyA = Game::Get()->GetRuningLevel()->GetGroundBody();
		jointDef.bodyB = info._block->GetBody();
		jointDef.target = info._touchPosition;
		jointDef.maxForce = 3000.0f * info._block->GetBody()->GetMass();

		_touchedMap[touch]._mouseJoint = ( b2MouseJoint*) Game::Get()->GetRuningLevel()->GetWorld()->CreateJoint( &jointDef );
		_touchedMap[touch]._block->GetBody()->SetAwake(true);
	}
}
//-------------------------------------------------------------------------------------------
void LevelBlockOperator::_doBlockTouch( puBlock *block, const b2Vec2 &pos, void *userData )
{

	CCTouch *touch;
	touch = (CCTouch *) userData;

	ProcessActiveBlocks( touch, block );

	_touchedMap[touch]._block = block;
	_touchedMap[touch]._touchPosition = pos;

	OperateMove( touch );
}
//-------------------------------------------------------------------------------------------
void LevelBlockOperator::_doEmptyTouch( const b2Vec2 &pos, void *userData )
{
	CCTouch *touch;
	touch = (CCTouch *) userData;

	ProcessActiveBlocks( touch, NULL );
}
//-------------------------------------------------------------------------------------------
void LevelBlockOperator::RelaseAllMouseJoints()
{
	TouchedBlockMap::iterator it;
	for ( it = _touchedMap.begin(); it != _touchedMap.end(); it++ )
	{
		if ( (*it).second._mouseJoint )
		{
			Game::Get()->GetRuningLevel()->GetWorld()->DestroyJoint( (*it).second._mouseJoint );
			(*it).second._mouseJoint = NULL;
		}
	}
}
//-------------------------------------------------------------------------------------------
void LevelBlockOperator::ProcessActiveBlocks( CCTouch * touch, puBlock * block )
{
	if ( _activeBlockMap.count( touch ) && _activeBlockMap[ touch ] != block )
	{
		puBlock *activeBlock;
		activeBlock = _activeBlockMap[ touch ];

		if ( activeBlock && activeBlock->GetBlockType() == GameTypes::eActivable )
			activeBlock->Deactivate( NULL );

		_activeBlockMap.erase( touch );
	}
}
//-------------------------------------------------------------------------------------------
