#include "SoundEngine.h"
#include "ScoreCounter.h"
#include "GameConfig.h"
#include "Game.h"
#include "Level.h"
#include "ParticleEngine.h"

//---------------------------------------------
// Score Counter
//---------------------------------------------
float ScoreCounter::_yCoins = -48.5f;

ScoreCounter* ScoreCounter::Create( Level *level )
{
	ScoreCounter *node;
	node = new ScoreCounter( level );
	if ( node )
	{
		node->autorelease();
		node->Init();
		return node;
	}
	CC_SAFE_DELETE(node);
	return NULL;
}
//---------------------------------------------
ScoreCounter::ScoreCounter(  Level *level )
{
#ifdef BUILD_EDITOR4ALL
	return;
#endif

	_stopCalled				= false;
	 _level					= level;
	_startTime				= time( NULL );
	_remainSmallCoinsNode	= NULL;
	_remainBigCoinsNode		= NULL;
//	_coinsRemainBg			= NULL;
//	_coinsRemainBgA1		= NULL;
//	_coinsRemainBgA2		= NULL;
	_finalScore				= NULL;
    
    _counterSpriteFadeOutAction     = NULL;
    _pointerFadeOutAction           = NULL;
    _pointerFastFadeOutAction       = NULL;
    _stopperMiddleDotFadeOutAction  = NULL;
    _runDisplayTimeAction           = NULL;
        
	_mute = false;
}
//---------------------------------------------
void ScoreCounter::Init()
{
#ifndef INTRO_POINTER
	if ( ! IsLevelDone() )
#endif
	{
		_counterSprite = CCSprite::spriteWithFile( Skins::GetSkinName( "Images/Stopper/StopperBg.png") );
		_counterSprite->setPosition( ccp( 0.0f, 0.0f ));
		addChild( _counterSprite, 15 );

		_finalScore = CCNode::node();
		addChild( _finalScore, 10 );

        //--------------
        // final time
		_finalTime = FinalTimeAnimation::Get( this );
		_finalScore->addChild( _finalTime, 30 );

		_stopperMiddleDot = CCSprite::spriteWithFile( Skins::GetSkinName( "Images/Stopper/StopperMiddleDot.png") );
		_pointer = CCSprite::spriteWithFile( Skins::GetSkinName("Images/Stopper/StopperPointer.png") );
		_pointerFast = CCSprite::spriteWithFile( Skins::GetSkinName("Images/Stopper/StopperPointerFast.png") );

		_counterSprite->addChild( _pointer, 10 );
		_counterSprite->addChild( _pointerFast, 10 );
		_counterSprite->addChild( _stopperMiddleDot, 15 );

		CCPoint pos;
		pos.x = 2.0f + _counterSprite->getContentSizeInPixels().width / 2.0f;
		pos.y = -1.5f + _counterSprite->getContentSizeInPixels().height / 2.0f;

		_pointer->setPosition( pos );
		_pointerFast->setPosition( pos );
		_stopperMiddleDot->setPosition( pos );

		_pointerAction = NULL;
		_pointerFastAction = NULL;
		ProcessTime();


		//----------------------
		// Coins remain big background
		{
			float dur;
			dur = 0.5f;

//			_coinsRemainBg = CCSprite::spriteWithFile( Skins::GetSkinName("Images/OtherPreload/ScoreBg.png") );
//
//			_coinsRemainBgA1 = CCSequence::actions(
//                                                   CCDelayTime::actionWithDuration(0.5f),
//                                                   CCFadeTo::actionWithDuration( dur, 255 ),
//                                                   NULL);
//
//			float sdur = 0.5f;
//			float sdurDelta = 0.1f;
//			float scaleDelta = 0.15f;
//
//			_coinsRemainBgA2 = CCSequence::actions(
//				CCScaleTo::actionWithDuration( sdur, 1.0f + scaleDelta  ),
//				CCScaleTo::actionWithDuration( sdurDelta, 1.0f - scaleDelta ),
//				CCScaleTo::actionWithDuration( sdurDelta, 1.0f ),
//				NULL);
//
//				_coinsRemainBg->setScale( 0.001f );
//				_coinsRemainBg->setPosition( ccp( 140.0f, -20.0f ));
//				_coinsRemainBg->setOpacity( 0 );
//				_finalScore->addChild( _coinsRemainBg, 5 );
			
//			_coinsRemainBgA1->retain();
//			_coinsRemainBgA2->retain();
		}
		// Coins remain big background
		//----------------------



		//----------------------
		// Final Big coins remain  
		{
			int cointCount;
			float x, y, xDelta;
			float dur;
			float delay;

            //-------------------
            // Score Coins placment
			cointCount = 5;
            if ( _level->GetOrientation() == eLevel_Horizontal )
            {
                x = 80.0f;
                y = 10.5f;
            }
            else
            {
                x = -70.0f;
                y = 1.5f;
            }
            
			
			xDelta = 50.0f;
			dur = 0.4f;
			delay = 0.4f;
			_remainBigCoinsNode = CCNode::node();
			
			CCSprite *coinSprite;

			for ( int i = 0; i < cointCount; i++ )
			{
				coinSprite = CCSprite::spriteWithFile( Skins::GetSkinName( "Images/OtherPreload/ScoreCoin.png") );
				coinSprite->setPosition( ccp( x, y ));
				coinSprite->setOpacity( 0 );
				coinSprite->setScale( 0.01f );
				_remainBigCoinsNode->addChild( coinSprite, i + 10 );

				CCFiniteTimeAction*  actionFade = CCSequence::actions(
					CCDelayTime::actionWithDuration( i * delay ),
                                                                      //        CCCallFuncND::actionWithTarget( this, callfuncND_selector( ScoreCounter::AddCoinEffect ), (void*) coinSprite ),
					CCFadeTo::actionWithDuration( dur, 255 ),
					NULL);

				CCFiniteTimeAction*  actionScale = CCSequence::actions(
					CCDelayTime::actionWithDuration( i * delay ),
                    CCScaleTo::actionWithDuration( 0.30f, 1.2f ),
					CCScaleTo::actionWithDuration( 0.05f, 0.9f ),
					CCScaleTo::actionWithDuration( 0.05f, 1.0f ),
					NULL);


				CCFiniteTimeAction*  actionSound = CCSequence::actions(
					CCDelayTime::actionWithDuration( i * delay * 0.99f ),
//                  CCDelayTime::actionWithDuration( 0.25f ),
   					CCCallFunc::actionWithTarget( this, callfunc_selector( ScoreCounter::PlayCoinScore )),
					NULL);

				_remainCoinsContainer.push_back( NodeAndAction( coinSprite, actionFade ));
				_remainCoinsContainer.push_back( NodeAndAction( coinSprite, actionScale ));
				_remainCoinsContainer.push_back( NodeAndAction( coinSprite, actionSound ));
				
				x += xDelta;
			}
			
			_finalScore->addChild( _remainBigCoinsNode, 20 );
		}
        
        //--------------------------
        // Fade out counter actions
        {
           	float dur = 0.6f;
            float durFadeOutStopper = 2.0f;
            
            _pointerFadeOutAction           = CCFadeTo::actionWithDuration( durFadeOutStopper, 0 );
            _pointerFastFadeOutAction       = CCFadeTo::actionWithDuration( durFadeOutStopper, 0 );
            _stopperMiddleDotFadeOutAction  = CCFadeTo::actionWithDuration( durFadeOutStopper, 0 );
            _counterSpriteFadeOutAction     = CCFadeTo::actionWithDuration( durFadeOutStopper, 0 );
            _runDisplayTimeAction           = CCSequence::actions(
                                                                  CCDelayTime::actionWithDuration( dur * 2.3f ),
                                                                  CCCallFunc::actionWithTarget( this, callfunc_selector( ScoreCounter::DisplayTimeScore )),
                                                                  NULL );
            _pointerFadeOutAction->retain();
            _pointerFastFadeOutAction->retain();
            _stopperMiddleDotFadeOutAction->retain();
            _counterSpriteFadeOutAction->retain();
            _runDisplayTimeAction->retain();
        
            //---------------------------
            // Coins fade out action
            CCFadeTo *fadeToAction;
            for ( int i = 0; i < 5; i++ )
            {
                fadeToAction = CCFadeTo::actionWithDuration( durFadeOutStopper, 0 );
                fadeToAction->retain();
                _coinsFadeOutActions.push_back( fadeToAction );
            }
        }
	}
}
//---------------------------------------------
void ScoreCounter::AddCoinEffect(CCNode *sender, void *data)
{
    CCNode *node;
    node = (CCNode*) data;
    node->addChild( ParticleEngine::Effect_CoinOnScore(), -10 );
}
//---------------------------------------------
void ScoreCounter::ReleaseAllActions()
{
//	CC_SAFE_RELEASE_NULL( _coinsRemainBgA1 );
//	CC_SAFE_RELEASE_NULL( _coinsRemainBgA2 );
    
    CC_SAFE_RELEASE_NULL( _pointerFadeOutAction );
    CC_SAFE_RELEASE_NULL( _pointerFastFadeOutAction );
    CC_SAFE_RELEASE_NULL( _counterSpriteFadeOutAction );
    CC_SAFE_RELEASE_NULL( _stopperMiddleDotFadeOutAction );
    CC_SAFE_RELEASE_NULL( _runDisplayTimeAction );
    
    for ( ActionVector::iterator it = _coinsFadeOutActions.begin(); it != _coinsFadeOutActions.end(); it++ )
    {
        CC_SAFE_RELEASE_NULL( *it );
    }
    _coinsFadeOutActions.clear();
}
//---------------------------------------------
ScoreCounter::~ScoreCounter()
{
	_remainBigCoinsNode->removeAllChildrenWithCleanup( true );
	_remainSmallCoinsNode->removeAllChildrenWithCleanup( true );

	removeAllChildrenWithCleanup( true );
	_coinsRemainSprites.clear();
}
//---------------------------------------------
void ScoreCounter::Step( cocos2d::ccTime dt )
{
#ifdef BUILD_EDITOR4ALL
	return;
#endif
	ProcessTime();
	ProcessRemainCoins();
}
//---------------------------------------------
void ScoreCounter::Start()
{
#ifdef BUILD_EDITOR4ALL
	return;
#endif
    
#ifdef INTRO_POINTER
	StartStopperAnim();
	_startTime = time( NULL );
	schedule( schedule_selector( ScoreCounter::Step ));
    return;
#endif
    
	if ( IsLevelDone() )
		return;

	StartStopperAnim();
	_startTime = time( NULL );
	schedule( schedule_selector( ScoreCounter::Step ));
}
//---------------------------------------------
void ScoreCounter::Stop()
{
#ifdef BUILD_EDITOR4ALL
	return;
#endif
	
	_stopCalled = true;
	unschedule( schedule_selector( ScoreCounter::Step ));


	//--------------------------
	// Fade out pointer & stoper
	{
		_pointer->stopAllActions();
		_pointerFast->stopAllActions();
//		
//		_pointer->runAction( _pointerFadeOutAction );
//		_pointerFast->runAction( _pointerFastFadeOutAction );
//		_stopperMiddleDot->runAction( _stopperMiddleDotFadeOutAction );
//		_counterSprite->runAction( _counterSpriteFadeOutAction );
	}


//	//----------------------
//	// Fade out coins remain and bg for coins remain
//    int count = 0;
//	for ( Sprites::iterator it = _coinsRemainSprites.begin(); it != _coinsRemainSprites.end(); it++ )
//	{
//		(*it)->stopAllActions();
//		(*it)->runAction( _coinsFadeOutActions[ count ] );
//        count++;
//	}
//	
//	_coinsRemainBg->runAction( _coinsRemainBgA1 );
//	_coinsRemainBg->runAction( _coinsRemainBgA2 );
}
//---------------------------------------------
void ScoreCounter::ShowAnim()
{
	//--------------------------
	// Fade out pointer & stoper
	{
		_pointer->runAction( _pointerFadeOutAction );
		_pointerFast->runAction( _pointerFastFadeOutAction );
		_stopperMiddleDot->runAction( _stopperMiddleDotFadeOutAction );
		_counterSprite->runAction( _counterSpriteFadeOutAction );
	}


	//----------------------
	// Fade out coins remain and bg for coins remain
    int count = 0;
	for ( Sprites::iterator it = _coinsRemainSprites.begin(); it != _coinsRemainSprites.end(); it++ )
	{
		(*it)->stopAllActions();
		(*it)->runAction( _coinsFadeOutActions[ count ] );
        count++;
	}

	runAction( _runDisplayTimeAction );
}
//---------------------------------------------
void ScoreCounter::StopQuiet()
{
#ifdef BUILD_EDITOR4ALL
	return;
#endif

	_remainCoinsContainer.clear();
    ReleaseAllActions();

	if ( _stopCalled )
		return;
	  
	stopAllActions();
//	_coinsRemainBg->stopAllActions();
	_counterSprite->stopAllActions();
	_pointer->stopAllActions();
	_pointerFast->stopAllActions();

	for ( Sprites::iterator it = _coinsRemainSprites.begin(); it != _coinsRemainSprites.end(); it++ )
		(*it)->stopAllActions();

	unschedule( schedule_selector( ScoreCounter::Step ));
}
//---------------------------------------------
void ScoreCounter::DisplayCoinScore()
{
	_remainBigCoinsNode->setPosition( ccp( 120.0f, -42.5f + _yCoins ));

	CCNode *node;
	CCAction *action;
	GameStatus *status;
	
	int coinCount;
	int coinCountBreaker;
	
	status				= Game::Get()->GetGameStatus();
	coinCount			= status->GetCoinCount( Game::Get()->GetGameStatus()->GetCurrentLevel() ); 
	coinCountBreaker	= coinCount * 3; // *3 because there are two action for each coin: scale & fade

	for (  NodeAndActions::iterator it = _remainCoinsContainer.begin(); it != _remainCoinsContainer.end() && coinCountBreaker > 0; it ++ )
	{
		node = it->_node;
		action = it->_action;
		
		if ( node && action )
        {
			node->runAction( action );
		}
		coinCountBreaker--;
	}

	//float dur = 0.4f; // this is from DisplayCoin
	float delay = 0.4f;
	float delayTotal = coinCount * delay + 0.5f;

	if ( delayTotal < 0 )
		delayTotal = 0; 

    // move to constructor
	CCFiniteTimeAction *showMenuAction;
	showMenuAction = CCSequence::actions(
		CCDelayTime::actionWithDuration( delayTotal ),
		CCCallFunc::actionWithTarget( this, callfunc_selector( ScoreCounter::ShowLevelMenu )),
		NULL );
    
    runAction( showMenuAction );

//    CCFiniteTimeAction *moveAll;
//    if ( _level->GetOrientation() ==  GameTypes::eLevel_Horizontal )
//    {
//        moveAll = CCSequence::actions(
//                CCDelayTime::actionWithDuration( delayTotal + 0.25f ),
//                CCMoveBy::actionWithDuration( 0.30f, ccp( -120.0f, 0.0f)),
//                NULL );
//    }
//    else
//    {
//        moveAll = CCSequence::actions(
//                CCDelayTime::actionWithDuration( delayTotal + 0.15f ),
//                CCMoveBy::actionWithDuration( 0.30f, ccp( 0.0f, -120.0f)),
//                NULL );
//    
//    }
//        
//     runAction( moveAll );
	_remainCoinsContainer.clear();
}
//---------------------------------------------
void ScoreCounter::DisplayTimeScore()
{
	_finalTime->Start();
}
//---------------------------------------------
bool ScoreCounter::IsLevelDone()
{
#ifdef BUILD_EDITOR4ALL
	return true;
#endif
	return ( Game::Get()->GetGameStatus()->GetStatus()->_statusTable[ Game::Get()->GetGameStatus()->GetCurrentLevel() ] == GameStatus::eLevelStatus_Done );
}
//---------------------------------------------
// Return: True if processed
bool ScoreCounter::ProcessTime()
{
#ifdef BUILD_EDITOR4ALL
	return false;
#endif
	
	if ( IsLevelDone() )
		return false;

	time_t	now;
	now = time (NULL);

	if ( ! (( now - _startTime ) > 0 )  )	
		return false;	

	GameStatus *status;
	status = Game::Get()->GetGameStatus();
	status->GetStatus()->_scoreTable[ status->GetCurrentLevel() ]._seconds += ( now - _startTime );

	_startTime = now;
	return true;
}
//---------------------------------------------
CCNode *ScoreCounter::GetCoinScoreNode()
{
	GameStatus *status;
	status = Game::Get()->GetGameStatus();

	int cointCount;
	cointCount = status->GetCoinCount( status->GetCurrentLevel() );

	float x, y, xDelta;

	x = 0.0f;
	y = -0.5f;
	xDelta = 42.0f;

	float dur;
	float delay;

	dur = 0.4f;
	delay = 0.4f;

	CCNode *node;
	CCSprite *coinSprite;

	node = CCNode::node();
	
	for ( int i = 0; i < cointCount; i++ )
	{
		coinSprite = CCSprite::spriteWithFile( Skins::GetSkinName( "Images/OtherPreload/ScoreCoin.png") );
		coinSprite->setPosition( ccp( x, y ));
		node->addChild( coinSprite, i + 10 );

		coinSprite->setOpacity( 0 );
		CCFiniteTimeAction*  actionFade = CCSequence::actions(
			CCMoveBy::actionWithDuration( ( i * delay ), ccp( 0.0f, 0.0f )),
			CCCallFunc::actionWithTarget(this, callfunc_selector( ScoreCounter::PlayCoinScore )), 
			CCFadeTo::actionWithDuration( dur, 255 ),			 
			NULL);

		coinSprite->setScale( 0.01f );
		CCFiniteTimeAction*  actionScale = CCSequence::actions(
			CCMoveBy::actionWithDuration( ( i * delay ), ccp( 0.0f, 0.0f )),
			CCScaleTo::actionWithDuration( dur, 1.2f ),
			CCScaleTo::actionWithDuration( dur / 4.0f, 0.9f ),
			CCScaleTo::actionWithDuration( dur / 4.0f, 1.0f ),
			NULL);

		coinSprite->runAction( actionFade );
		coinSprite->runAction( actionScale );
		x += xDelta;
	}

	return node;
}
//---------------------------------------------
void ScoreCounter::PlayCoinScore()
{
	if ( ! _mute )
	SoundEngine::Get()->PlayEffect( SoundEngine::eSoundCoinScored );
}
//---------------------------------------------
void ScoreCounter::PlayCoinScoreReverse()
{
	SoundEngine::Get()->PlayEffect( SoundEngine::eSoundCoinScoredReversed );
}
//---------------------------------------------
CCNode* ScoreCounter::GetCoinsRemain()
{
	if ( _remainSmallCoinsNode )
		return _remainSmallCoinsNode;

	_remainSmallCoinsNode = CCNode::node();

	GameStatus *status;
	status = Game::Get()->GetGameStatus();

	int cointCount;
	cointCount = status->GetCoinCount( status->GetCurrentLevel() );

#ifdef INTRO_POINTER
    cointCount = Tools::Rand( 3, 5 );
#endif
    
	if ( !cointCount )
		return NULL;

	float x, y, xDelta;

	x = 0.0f;
	y = 2.5f;
	xDelta = 15.0f;

	float dur;
	float delay;

	dur = 0.4f;
	delay = 0.4f;

	CCSprite *coinSprite;
	for ( int i = 0; i < cointCount; i++ )
	{
		coinSprite = CCSprite::spriteWithFile( Skins::GetSkinName( "Images/OtherPreload/ScoreCoinGame.png") );
		coinSprite->setPosition( ccp( x, y ));
		_remainSmallCoinsNode->addChild( coinSprite, i + 10 );

		coinSprite->setOpacity( 0 );
		CCFiniteTimeAction*  actionFade = CCSequence::actions(
			CCMoveBy::actionWithDuration( ( ( i + 2 ) * delay ), ccp( 0.0f, 0.0f )),
			//CCFadeTo::actionWithDuration( dur, Config::LevelNotifiersOpacity ),
			CCFadeTo::actionWithDuration( dur, 240 ),
			NULL);

		coinSprite->setScale( 0.01f );
		CCFiniteTimeAction*  actionScale = CCSequence::actions(
			CCMoveBy::actionWithDuration( ( ( i + 1 ) * delay ), ccp( 0.0f, 0.0f )),
			CCScaleTo::actionWithDuration( dur, 1.2f ),
			CCScaleTo::actionWithDuration( dur / 4.0f, 0.9f ),
			CCScaleTo::actionWithDuration( dur / 4.0f, 1.0f ),
			NULL);

		coinSprite->runAction( actionFade );
		coinSprite->runAction( actionScale );
		
		x += xDelta;

		_coinsRemainSprites.insert( _coinsRemainSprites.begin(), coinSprite );
	}

	return _remainSmallCoinsNode;
}
//---------------------------------------------
void ScoreCounter::ProcessRemainCoins()
{
	if ( ! _coinsRemainSprites.size() )
		return;

	GameStatus *status;
	status = Game::Get()->GetGameStatus();

	unsigned int coinCount;
	coinCount = status->GetCoinCount( status->GetCurrentLevel() );

	if ( coinCount != _coinsRemainSprites.size() )
	{
		CCSprite *coin;
		coin = *( _coinsRemainSprites.begin());

		_coinsRemainSprites.erase( _coinsRemainSprites.begin() );

		float dur;
		float delay;

		dur = 0.4f;
		delay = 0.4f;

		CCFiniteTimeAction*  actionScale;
		CCFiniteTimeAction*  actionFade = CCSequence::actions(
			CCDelayTime::actionWithDuration( delay ),
			CCCallFunc::actionWithTarget( this, callfunc_selector( ScoreCounter::PlayCoinScoreReverse )), 
			CCFadeTo::actionWithDuration( dur, 0 ),			 
			NULL);

		if ( _coinsRemainSprites.size() != 0 )
		{
			actionScale = CCSequence::actions(
				CCMoveBy::actionWithDuration( delay, ccp( 0.0f, 0.0f )),
				CCScaleTo::actionWithDuration( dur / 4.0f, 0.9f ),
				CCScaleTo::actionWithDuration( dur / 4.0f, 1.2f ),
				CCScaleTo::actionWithDuration( dur, 0.01f ),
				NULL);
		}
		else
		{
			actionScale = CCSequence::actions(
				CCMoveBy::actionWithDuration( delay, ccp( 0.0f, 0.0f )),
				CCScaleTo::actionWithDuration( dur / 4.0f, 0.9f ),
				CCScaleTo::actionWithDuration( dur / 4.0f, 1.2f ),
				CCScaleTo::actionWithDuration( dur, 0.01f ),
				CCCallFunc::actionWithTarget(this, callfunc_selector( ScoreCounter::HideRemainCoinsNode )), 
				NULL);
		}

		coin->stopAllActions();
		coin->runAction( actionFade );
		coin->runAction( actionScale );
	}
}
//---------------------------------------------
void ScoreCounter::HideRemainCoinsNode()
{
	float dur;
	float delay;

	dur = 2.5f;
	delay = 0.5f;

//	CCFiniteTimeAction*  actionFade = CCSequence::actions(
//		CCMoveBy::actionWithDuration( delay, ccp( 0.0f, 0.0f )),
//		CCFadeTo::actionWithDuration( dur, 0 ),			 
//		NULL);
//
//	_coinsRemainBg->runAction( actionFade );
}
//---------------------------------------------
void ScoreCounter::Pause()
{
	unschedule( schedule_selector( ScoreCounter::Step ));
	StopStopperAnim();
}
//---------------------------------------------
void ScoreCounter::StartStopperAnim()
{
	_pointerAction = _pointer->runAction( CCRepeatForever::actionWithAction( CCRotateBy::actionWithDuration( 60.0f, 360.0f )));
	_pointerAction->setTag( 12 );

	_pointerFastAction = _pointerFast->runAction( CCRepeatForever::actionWithAction( CCRotateBy::actionWithDuration( 1.0f, 360.0f )));
	_pointerFastAction->setTag( 13 );
}
//---------------------------------------------
void ScoreCounter::StopStopperAnim()
{
	_pointer->stopActionByTag( 12 );
	_pointerFast->stopActionByTag( 13 );
	return;
	if ( _pointerAction )
	{
		_pointerAction->stop();
		_pointerAction = NULL;
	}

	if ( _pointerFastAction )
	{
		_pointerFastAction->stop();
		_pointerFastAction = NULL;
	}
}
//---------------------------------------------
void ScoreCounter::SetOrientation( LevelOrientation orient )
{
	CCPoint pos;
    float d = 150.0f;
    
	switch ( orient )
	{
	case GameTypes::eLevel_Horizontal :

		pos = ccp( Config::GameSize.width / 2.0f - 780.0f + d, Config::GameSize.height / 2.0f - 50.0f);
		_counterSprite->setPosition( pos );

		if ( _remainSmallCoinsNode )
		{
			_remainSmallCoinsNode->setRotation( 0.0f );
			_remainSmallCoinsNode->setPosition( CCPoint( pos.x - 100.0f - d, pos.y + 15.0f ));
		}

		_finalScore->setRotation( 0.0f );
		_finalScorePos = ccp( -150.0f - d, 20.0f );
        _finalTime->setPosition( CCPoint( 233.0f, 125.0f + 27.75f ));
		// -240, 20
		break;

	case GameTypes::eLevel_Vertical :
		pos = ccp( Config::GameSize.width / 2.0f / RATIO - 475.0f ,  Config::GameSize.height / 2.0f / RATIO - 170.0f );
		_counterSprite->setRotation( -90.0f );
		_counterSprite->setPosition( pos );

		pos = ccp( Config::GameSize.width / 2.0f / RATIO - 270.0f,  Config::GameSize.height / 2.0f / RATIO - 190.0f );

		if ( _remainSmallCoinsNode )
		{
			_remainSmallCoinsNode->setRotation( -90.0f );
			_remainSmallCoinsNode->setPosition( ccp( pos.x - 225.0f , pos.y - 80.0f  ));
		}

		_finalScore->setRotation( -90.0f );
		_finalScorePos = ccp( -25.0f, -150.0f );
   		_finalTime->setPosition( CCPoint( 91.5f, 125.0f + 25.5f ));

		break;
		

	default:
		unAssertMsg(ScoreCounter, false, ( "Unknown orientation %d", orient ));
	}
	
	setPosition( _finalScorePos );
}
//-------------------------------------------------------------------------------------------
void ScoreCounter::CleanUpRemainBigCoins()
{
	
	if ( _remainCoinsContainer.size() > 0  )
	{
		CCAction *action;
		for ( NodeAndActions::iterator it = _remainCoinsContainer.begin(); it != _remainCoinsContainer.end(); it++ )
		{
			action = it->_action;
			if ( action )
				action->release();
		}
		_remainCoinsContainer.clear();
	}
}
//------------------------------------------------------------------------------------------
void ScoreCounter::ShowLevelMenu()
{
	_level->ScoreShown();
}
//------------------------------------------------------------------------------------------



//------------------------------------------------------------------------------------------
// Final Time Animation
//------------------------------------------------------------------------------------------
FinalTimeAnimation* FinalTimeAnimation::Get( ScoreCounter *scoreConter )
{

	FinalTimeAnimation* ret;
	ret = new FinalTimeAnimation( scoreConter );
	if ( ret )
	{
		ret->autorelease();
		ret->Init();
		return ret;
	}
	CC_SAFE_DELETE( ret );
	return NULL;
}
//------------------------------------------------------------------------------------------
FinalTimeAnimation::FinalTimeAnimation( ScoreCounter *scoreConter )
{
	_scoreCounter = scoreConter;
}
//------------------------------------------------------------------------------------------
FinalTimeAnimation::~FinalTimeAnimation()
{
	removeAllChildrenWithCleanup( true );
	_digitsContainer.clear();
}
//------------------------------------------------------------------------------------------
void FinalTimeAnimation::Start()
{
    int hours;
	int minutesA;
	int minutesB;
	int secondsA;
	int secondsB;

	GameStatus *status;

	float startX, startY;
	float digitDeltaX, colonDeltaX, colonDeltaY, deltaY;

	//------------------
	// Code
	status		= Game::Get()->GetGameStatus();

	hours		= status->GetStatus()->_scoreTable[ status->GetCurrentLevel() ].GetHoursCount() ;
	minutesA	= status->GetStatus()->_scoreTable[ status->GetCurrentLevel() ].GetMinutesCount() / 10;
	minutesB	= status->GetStatus()->_scoreTable[ status->GetCurrentLevel() ].GetMinutesCount() ;
	minutesB	= minutesB - ( minutesB / 10 ) * 10;

	secondsA	= status->GetStatus()->_scoreTable[ status->GetCurrentLevel() ].GetSecondsCount() / 10;
	secondsB	= status->GetStatus()->_scoreTable[ status->GetCurrentLevel() ].GetSecondsCount() ;
	secondsB	= secondsB - ( secondsB / 10 ) * 10;

	status = Game::Get()->GetGameStatus();

	int frameToWait = 0;

	_digitsContainer.push_back( FinalTimeSingleAnimData( _hoursNode,	hours, 1, &frameToWait ));
	_digitsContainer.push_back( FinalTimeSingleAnimData( _minutesNodeA, minutesA, 2, &frameToWait ));
	_digitsContainer.push_back( FinalTimeSingleAnimData( _minutesNodeB, minutesB, 3, &frameToWait ));
	_digitsContainer.push_back( FinalTimeSingleAnimData( _secondsNodeA, secondsA, 4, &frameToWait ));
	_digitsContainer.push_back( FinalTimeSingleAnimData( _secondsNodeB, secondsB, 5, &frameToWait ));

	_currentDigit = _digitsContainer.begin();

	startX = 0.0f;
	startY = 0.0f;

	digitDeltaX = 25.0f;
	colonDeltaX = 6.55f;
	colonDeltaY = 0.25f;
	deltaY = 0.0f;

	_hoursNode->setPosition( startX, startY );
	_colon1->setPosition( ccp( startX + digitDeltaX - colonDeltaX, startY + colonDeltaY ));
	_minutesNodeA->setPosition( startX + digitDeltaX + 2.0f * colonDeltaX, startY );
	_minutesNodeB->setPosition( startX + digitDeltaX * 2.0f + 2.0f * colonDeltaX, startY );
	_colon2->setPosition( ccp( startX + digitDeltaX * 3.0f + 1.0f * colonDeltaX, startY + colonDeltaY ));
	_secondsNodeA->setPosition( startX + digitDeltaX * 3.0f + 4.0f * colonDeltaX, startY );
	_secondsNodeB->setPosition( startX + digitDeltaX * 4.0f + 4.0f * colonDeltaX, startY );


	SingleDigitAnimCallback();
}
//------------------------------------------------------------------------------------------
void FinalTimeAnimation::GetRandomDigitAnim( CCNode *node, int finalDigit, int index )
{
	float fadeDur;

	fadeDur = 0.35f;

	float fps;
	CCSprite *rollingDigit;
	CCAnimation *animationFinal;

	fps = 1.0f / 35.0f;

	int framePerDigit = 4;

	if ( ! finalDigit )
	{
		animationFinal = AnimTools::Get()->GetAnimFrames( "ScoreCounter", framePerDigit + 1 );
	}
	else
	{
		D_INT(finalDigit)
		animationFinal = AnimTools::Get()->GetAnimFrames( "ScoreCounter", (framePerDigit ) * (finalDigit + 1) + 1 );
	}

	animationFinal->setDelay( fps );
	rollingDigit = CCSprite::spriteWithFile( Skins::GetSkinName( "Images/OtherPreload/TimerStartDigit.png" ) );

	//------------------------
	 //Sprite
	CCAnimate *animateFinal = CCAnimate::actionWithAnimation( animationFinal, false );

	CCActionInterval* seq;

	// Colon1
	if ( index == 1 )
	{
		seq = (CCActionInterval*)(CCSequence::actions( 
			animateFinal,
			CCCallFuncND::actionWithTarget( this, callfuncND_selector( FinalTimeAnimation::ShowColon), (void*) _colon1 ), 
			NULL) );
	}
	// Colon2
	else if ( index == 3 )
	{
		seq = (CCActionInterval*)(CCSequence::actions( 
			animateFinal,
			CCCallFuncND::actionWithTarget( this, callfuncND_selector( FinalTimeAnimation::ShowColon), (void*) _colon2 ), 
			NULL) );
	}
	// Digits
	else 
	{
		CCActionInterval* seqMusic;

		// Digit anim
		seq = (CCActionInterval*)(CCSequence::actions( 
			animateFinal,
			CCCallFunc::actionWithTarget( this, callfunc_selector( FinalTimeAnimation::SingleDigitAnimCallback )), 
			NULL) );

		// Sound feddback
		if ( finalDigit == 0 )
		{
			seqMusic = (CCActionInterval*) CCCallFunc::actionWithTarget( this, callfunc_selector( FinalTimeAnimation::PlayFinalTick ));
		}
		else
		{
			CCActionInterval *repeatMe = (CCActionInterval*)(CCSequence::actions( 
				CCCallFunc::actionWithTarget( this, callfunc_selector( FinalTimeAnimation::PlayTick )), 
				CCDelayTime::actionWithDuration( fps * framePerDigit ),
				NULL ));

			seqMusic = (CCActionInterval*) CCSequence::actions(
				CCRepeat::actionWithAction( repeatMe, finalDigit ),
				CCCallFunc::actionWithTarget( this, callfunc_selector( FinalTimeAnimation::PlayFinalTick )),
				NULL );
		}

		rollingDigit->runAction( seqMusic );

	}

	rollingDigit->runAction( seq );
	node->addChild( rollingDigit );
}
//------------------------------------------------------------------------------------------
string FinalTimeAnimation::GetRandomDigit()
{
	int digit;
	stringstream ret;

	digit = Tools::Rand( 0, 9 );
	ret << digit; 

	return ret.str();
}
//------------------------------------------------------------------------------------------
void FinalTimeAnimation::PlayTickTimeSound()
{
	SoundEngine::Get()->PlayEffect( SoundEngine::eSoundScoreTimeTick );
}
//------------------------------------------------------------------------------------------
void FinalTimeAnimation::Init()
{
	_hoursNode		= CCNode::node();
	_minutesNodeA	= CCNode::node();
	_minutesNodeB	= CCNode::node();
	_secondsNodeA	= CCNode::node();
	_secondsNodeB	= CCNode::node();

	_colon1			= CCSprite::spriteWithFile( Skins::GetSkinName( "Images/OtherPreload/SemiColon.png" ) );
	_colon2			= CCSprite::spriteWithFile( Skins::GetSkinName( "Images/OtherPreload/SemiColon.png" ) );
	_colon1->setOpacity( 0 );
	_colon2->setOpacity( 0 );

	addChild( _hoursNode );
	addChild( _minutesNodeA );
	addChild( _minutesNodeB );
	addChild( _secondsNodeA );
	addChild( _secondsNodeB );
	addChild( _colon1 );
	addChild( _colon2 );
}
//------------------------------------------------------------------------------------------
void FinalTimeAnimation::SingleDigitAnimCallback()
{
	if ( _currentDigit == _digitsContainer.end() )
	{
		_scoreCounter->DisplayCoinScore();
		return;
	}

	GetRandomDigitAnim( _currentDigit->_node, _currentDigit->_finalDigit, _currentDigit->_index );
	_currentDigit++;
}
//------------------------------------------------------------------------------------------
void FinalTimeAnimation::ShowColon( CCObject *sender, void *data )
{
	CCNode *colon;
	colon = ( CCNode * ) data;

	if ( ! colon )
		return;

	CCActionInterval* seq;
	seq = (CCActionInterval*)(CCSequence::actions( 
		CCFadeTo::actionWithDuration( 0.025f, 255 ),
		CCCallFunc::actionWithTarget( this, callfunc_selector( FinalTimeAnimation::SingleDigitAnimCallback )), 
		NULL) );

	colon->runAction( seq );
}
//------------------------------------------------------------------------------------------
void FinalTimeAnimation::PlayTick()
{
	D_HIT
	SoundEngine::Get()->PlayEffect( SoundEngine::eSoundScoreTimeTick );
}
//------------------------------------------------------------------------------------------
void FinalTimeAnimation::PlayFinalTick()
{
	D_HIT
	SoundEngine::Get()->PlayEffect( SoundEngine::eSoundScoreTimeTickFinal );
}
//------------------------------------------------------------------------------------------



//------------------------------------------------------------------------------------------
// Node and action
//------------------------------------------------------------------------------------------
NodeAndAction::NodeAndAction()
{
	_node		= NULL;
	_action		= NULL;
}
//------------------------------------------------------------------------------------------
NodeAndAction::NodeAndAction( CCNode* node, CCAction* action )
{
	_node		= node;
	_action		= action;
}



//------------------------------------------------------------------------------------------
// FinalTimeSingle AnimData
//------------------------------------------------------------------------------------------
FinalTimeSingleAnimData::FinalTimeSingleAnimData()
{
	_node			= NULL;
	_finalDigit		= 0;
	_index			= 0;
	_frameWaitTotal	= 0;
}
//------------------------------------------------------------------------------------------
FinalTimeSingleAnimData::FinalTimeSingleAnimData( CCNode *node, int finalDigit, int index, int *frameWaitTotal )
{
	_node			= node;
	_finalDigit		= finalDigit;
	_index			= index;
	_frameWaitTotal	= frameWaitTotal;
}



//------------------------------------------------------------------------------------------
// Node and actions
//------------------------------------------------------------------------------------------
void NodeAndActions::push_back( const NodeAndAction &node )
{
	node._action->retain();
	vector<NodeAndAction>::push_back( node);
}
//------------------------------------------------------------------------------------------
void NodeAndActions::clear()
{
	releaseAll();
	vector<NodeAndAction>::clear();
}
//------------------------------------------------------------------------------------------
void NodeAndActions::releaseAll()
{
	for ( iterator it = begin(); it != end(); it++ )
		it->_action->release();
}
