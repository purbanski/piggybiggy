#include <sstream>
#include "LevelAccelerometr.h"
#include "GameTypes.h"
#include "GameConfig.h"
#include "Tools.h"
#include "Platform/Lang.h"
#include "RunLogger/RunLogger.h"

#ifdef BUILD_EDITOR
#include "Editor/EditorConfig.h"
#endif

using namespace GameTypes;
//----------------------------------------------------------------------------------------------
LevelAccelerometrStarter::LevelAccelerometrStarter()
{
	//--------------
	// background
	_spriteBg = CCSprite::spriteWithFile( "Images/LevelAccel/bgAccel.png" );
	_spriteBg->setOpacity( 0 );

	{
		CCFadeTo *fadeIn = CCFadeTo::actionWithDuration( 1.2f, 170 );
		_spriteBg->runAction( fadeIn );
	}
	addChild( _spriteBg, 0 );


	//--------------
	// Hold flat
	_spriteHoldFlat = CCSprite::spriteWithFile( Lang::Get()->GetLang( "Images/LevelAccel/HoldFlat.png" ));
    _spriteHoldFlat->setPosition( ccp( 0.0f, -180.0f ));
	_spriteHoldFlat->setOpacity( 0 );
	{
		CCFadeTo *fadeIn = CCFadeTo::actionWithDuration( 1.2f, 255 );
		_spriteHoldFlat->runAction( fadeIn );
	}
	addChild( _spriteHoldFlat, 15 );


	//--------------
	// Hold Shadow
	_spriteHoldFlatShadow = CCSprite::spriteWithFile( Lang::Get()->GetLang( "Images/LevelAccel/HoldFlatShadow.png" ));
    _spriteHoldFlatShadow->setPosition( ccp( 0.0f, -180.0f ));
	_spriteHoldFlatShadow->setOpacity( 0 );
	{
		float fadeDur;
		fadeDur = 0.75f;

		CCFadeTo *fadeIn = CCFadeTo::actionWithDuration( fadeDur, 255 );
		CCFadeTo *fadeOut = CCFadeTo::actionWithDuration( fadeDur, 0 );

		CCRepeatForever *repeat = CCRepeatForever::actionWithAction( dynamic_cast<CCActionInterval*>( CCSequence::actions( fadeIn, CCDelayTime::actionWithDuration( 0.3f ), fadeOut, NULL )));

		_spriteHoldFlatShadow->runAction( repeat );

	}
	addChild( _spriteHoldFlatShadow, 10 );

    
    //--------------
	// iPhone
	_spriteiPhone = CCSprite::spriteWithFile( "Images/LevelAccel/iPhone.png" );
    _spriteiPhone->setPosition( ccp( 0.0f, 0.0f ));
    
	addChild( _spriteiPhone, 0 );
    
   
    //--------------
	// To Start
	_spriteToStart = CCSprite::spriteWithFile( Lang::Get()->GetLang( "Images/LevelAccel/ToStart.png" ));
    _spriteToStart->setPosition( ccp( 0.0f, 150.0f ));
	addChild( _spriteToStart, 0 );
   
#ifdef BUILD_EDITOR
	_spriteHoldFlat->stopAllActions();
	_spriteHoldFlatShadow->stopAllActions();
	_spriteBg->stopAllActions();

	_spriteHoldFlat->setOpacity( 0 );
	_spriteHoldFlatShadow->setOpacity( 0 );
	_spriteBg->setOpacity( 0 );
#endif

}
//----------------------------------------------------------------------------------------------
void LevelAccelerometrStarter::GoAway()
{
	_spriteBg->stopAllActions();
	_spriteHoldFlat->stopAllActions();
	_spriteHoldFlatShadow->stopAllActions();


	CCFadeTo *fade = CCFadeTo::actionWithDuration( 0.4f, 0 );
	_spriteBg->runAction( fade );

	CCFadeTo *fade1 = CCFadeTo::actionWithDuration( 0.4f, 0 );
	_spriteHoldFlat->runAction( fade1 );

	CCFadeTo *fade2 = CCFadeTo::actionWithDuration( 0.4f, 0 );
	_spriteHoldFlatShadow->runAction( fade2 );

   	CCFadeTo *fade3 = CCFadeTo::actionWithDuration( 0.4f, 0 );
	_spriteToStart->runAction( fade3 );

   	CCFadeTo *fade4 = CCFadeTo::actionWithDuration( 0.4f, 0 );
	_spriteiPhone->runAction( fade4 );

}
//----------------------------------------------------------------------------------------------
LevelAccelerometr::LevelAccelerometr( LevelEnum levelEnum, bool viewMode ) : Level( levelEnum, viewMode, false )
{
    _levelType = eLevelTypeAccelerometr;
    _levelTag = eLevelTag_Balancer;

    gAppDisplayRotateMode = eDisplay_LandscapeFixed;
    
	_mode = eModeStoped;
	_startMsg = NULL;
	
#ifdef BUILD_EDITOR
	_acceLabel = CCLabelBMFont::labelWithString( "", EditorConfig::LevelAccelerometrFont );
	_acceLabel->setPosition( ccp( 225.0f, -140.0f ));
	addChild( _acceLabel, GameTypes::eLevelDebug );

	_gravityLabel = CCLabelBMFont::labelWithString( "", EditorConfig::LevelAccelerometrFont );
	_gravityLabel ->setPosition( ccp( 135.0f, -150.0f ));
	addChild( _gravityLabel , GameTypes::eLevelDebug );
#endif
    _accelIcon.SetPosition(94.0f, 7.5f );
}
//----------------------------------------------------------------------------------------------
bool LevelAccelerometr::init()
{
	if ( ! Level::init() )
		return false;

    if ( ! _viewMode )
        setIsAccelerometerEnabled( true );
    
	return true;
}
//----------------------------------------------------------------------------------------------
void LevelAccelerometr::didAccelerate( CCAcceleration* pAccelerationValue )
{
	b2Vec2 gravity;

#if defined( ANDROID_BUILD ) && !defined( _DEBUG )
	gravity.x = -(float) ( pAccelerationValue->y * 10.0f );
	gravity.y = (float) ( pAccelerationValue->x * 10.0f );
#else
	gravity.x = (float) ( pAccelerationValue->x * 10.0f );
	gravity.y = (float) ( pAccelerationValue->y * 10.0f );
#endif

	_world.SetGravity( gravity );

#ifdef BUILD_EDITOR
	stringstream ss;

	ss.precision( 2 );
	ss << "X: " << pAccelerationValue->x << endl;
	ss << "Y: " << pAccelerationValue->y << endl;
	ss << "Z: " << pAccelerationValue->z << endl;
	_acceLabel->setString( ss.str().c_str() );
	
	ss.str("");
	ss << "gravity( " << gravity.x << ", "  << gravity.y << " )" << endl;
	_gravityLabel->setString( ss.str().c_str() );
#endif
}
//----------------------------------------------------------------------------------------------
LevelAccelerometr::~LevelAccelerometr()
{
}
//----------------------------------------------------------------------------------------------
void LevelAccelerometr::Quiting()
{
	Level::Quiting();
}
//----------------------------------------------------------------------------------------------
void LevelAccelerometr::Step( cocos2d::ccTime dt )
{
	Level::Step(dt);
}
//----------------------------------------------------------------------------------------------
void LevelAccelerometr::LevelRun()
{
	if ( ! _demoRuning && !_viewMode )
		SoundEngine::Get()->GetBackgroundMusic().SetMusic_Game();
	
	if ( ! _startMsg && ! _viewMode )
	{
		_startMsg = new LevelAccelerometrStarter();
		_startMsg->autorelease();
		_startMsg->setPosition( ccp( 0.0f, 0.0f ));
		addChild( _startMsg, GameTypes::eLevelHoldFlatMsg );
	}

	switch ( _mode )
	{
		case eModeStoped :
			_mode = eModeStarting;
			schedule( schedule_selector( LevelAccelerometr::StartCheck ));
		break;

		case eModeStarting :
		break;

		case eModeStarted :
			Level::LevelRun();
		break;

		default: 
			unAssertMsg( LevelAccelerometr, false, ("Wrong mode %d", _mode ));
	}
	
}
//----------------------------------------------------------------------------------------------
void LevelAccelerometr::StartCheck( cocos2d::ccTime dt )
{
	if (
		( abs( _world.GetGravity().x ) <= 2.4f ) &&	( abs( _world.GetGravity().y ) <= 2.4f ) &&
		( abs( _world.GetGravity().x ) != 0.0f ) &&	( abs( _world.GetGravity().y ) != 0.0f )
		)
	{
        RLOG_S("LEVEL_ACCELR", "STARTED");
		D_POINT( (_world.GetGravity()) )
		unschedule( schedule_selector( LevelAccelerometr::StartCheck ));

		_mode = eModeStarted;
		Level::LevelRun();
		_startMsg->GoAway();
	}
}
//----------------------------------------------------------------------------------------------
void LevelAccelerometr::LevelPause()
{
	switch ( _mode )
	{
		case eModeStoped :
		break;

		case eModeStarting :
			_mode = eModeStoped;
			unschedule( schedule_selector( LevelAccelerometr::StartCheck ));
			break;

		case eModeStarted :
			Level::LevelPause();
			break;

		default :
			unAssertMsg( LevelAccelerometr, false, ("Wrong mode %d", _mode ));
	}
}
//----------------------------------------------------------------------------------------------
void LevelAccelerometr::AdjustOldScreen()
{
	Level::AdjustOldScreen();
//	SetAccelNotifierPosition();
}
//----------------------------------------------------------------------------------------------
//void LevelAccelerometr::SetAccelNotifierPosition()
//{
//	b2Vec2 accelPos;
//	accelPos.Set( 3.0f, 6.5f );
//	_accelBlockV.SetRotation( 90.0f );
//	_accelBlockV.SetPosition( b2Vec2( accelPos.x, accelPos.y ));
//	_accelBlockH.SetPosition( b2Vec2( accelPos.x + 4.0f, accelPos.y - 6.0f ));
//}
//----------------------------------------------------------------------------------------------
