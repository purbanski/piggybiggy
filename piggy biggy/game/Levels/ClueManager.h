#ifndef __CLUEMANAGER_H__
#define __CLUEMANAGER_H__

#include <string>
#include <map>
#include <list>
#include "cocos2d.h"
#include "pu/ui/SlidingNode.h"
#include "GameTypes.h"

using namespace std;
USING_NS_CC;


//------------------------------------------------------------------------------------------------------
// Clue
//------------------------------------------------------------------------------------------------------
class Clue : public CCNode
{
public:
	static Clue* node( int timeout, const char *clue );
	
protected:
	Clue( int timeout, const char *clue );

public:
	int	_timeout;
	string _clue;
};



//------------------------------------------------------------------------------------------------------
// ClueList
//------------------------------------------------------------------------------------------------------
class ClueList : public list<Clue*>
{
public:
	void Debug();
	void removeFromParentAndCleanup( bool cleanup );
	void release();
	void AddToNode( CCNode *node );
};



//------------------------------------------------------------------------------------------------------
// ClueManager
//------------------------------------------------------------------------------------------------------
class ClueScreen;
class ClueManager : public CCNode
{
public:
	ClueManager();
	~ClueManager();

	void SetTimer( int timer );
	void SetClues( ClueList clues );
	void Tick( cocos2d::ccTime dt );
	void Menu_ShowClue( CCObject *sender );
	bool IsBusy();
	void Reset();

	void PauseTimer();
	void ResumeTimer();
	void UpdateGameStatus();

	//void /*ShowClueTimer*/();

private:
	void ProcessClue();
	void ShowClueButton();
	void HideClueTimer();

private:
	ClueList			_clues;
	ClueList			_cluesGot;

	int					_timeout;
	CCLabelBMFont		*_timerLabel;
	CCMenu				*_menu;
	
	ClueScreen			*_clueScreen;
	bool				_isTimerRunning;

};



//------------------------------------------------------------------------------------------------------
// ClueScreen
//------------------------------------------------------------------------------------------------------
class ClueScreen : public CCNode
{
public:
	ClueScreen();
	~ClueScreen();
	
	void Show( ClueList clues );
	void Hide();

	void CleanUp();

	void Menu_Exit( CCObject *sender );

private:
	enum { 
		eAction_Show = 1,
		eAction_Hide
	};

	CCSprite	*_background;
	ClueList	_clueList;
	SlidingNode	*_slidingNode;
};



#endif
