#ifndef __BLOCKFINDER_H__
#define __BLOCKFINDER_H__

#include "Blocks/puBlock.h"
#include "CommonDefs.h"

//-------------------------------------------------------------------------------------
class BlockFinderQuery: public b2QueryCallback
{
public:
	BlockFinderQuery(const b2Vec2& point);
	bool ReportFixture(b2Fixture* fixture);

	b2Vec2	_point;
	FixtureContainer _fixtures;
};
//-------------------------------------------------------------------------------------
class BlockFinderBase
{
protected:
	BlockFinderBase(){}
	virtual ~BlockFinderBase(){}

	virtual puBlock* BlockTouched( const b2Vec2& position );
	bool MouseLongMove( const b2Vec2 &startPos, const b2Vec2 &endPos, void *userData );

private:
	virtual void _doBlockTouch( puBlock *block, const b2Vec2 &pos, void *userData ) = 0;
	virtual void _doEmptyTouch( const b2Vec2 &pos, void *userData ) = 0;
};
//-------------------------------------------------------------------------------------
#endif
