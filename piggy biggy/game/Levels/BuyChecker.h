#ifndef __BUYCHECKER_H__
#define __BUYCHECKER_H__

#include "cocos2d.h"
#include "GameTypes.h"

USING_NS_CC;
//-------------------------------------------------------------------
class BuyChecker
{
public:
	BuyChecker();
	bool BuyIfNeeded( GameTypes::LevelEnum level );
	
private:
	bool IsPurchaseNeeded( GameTypes::LevelEnum level );
};
//-------------------------------------------------------------------
#endif
