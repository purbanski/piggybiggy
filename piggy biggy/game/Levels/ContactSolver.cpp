#include "ContactSolver.h"
#include "World.h"
#include "Game.h"
#include "Blocks/puBlock.h"
#include "Level.h"


ContactSolver::BlockSet				ContactSolver::_deadBlocks;
ContactSolver::BlockSet				ContactSolver::_glassCollisions;
ContactSolver::AirBubbleCollisions	ContactSolver::_airBubbleCollisions;

//---------------------------------------------------------------------------------------------------

ContactSolver::ContactSolver()
{

	_isQuiting = false;

	_rules[ GameTypes::eCoin			| GameTypes::eThief ]			= &ContactSolver::Rules::Coin_Thief;
	_rules[ GameTypes::eCoinSilver		| GameTypes::eThief ]			= &ContactSolver::Rules::Coin_Thief;
	
	_rules[ GameTypes::ePiggyBank		| GameTypes::eThief ]			= &ContactSolver::Rules::PiggyBank_Thief;
	
	_rules[ GameTypes::eCoin			| GameTypes::ePiggyBank ]		= &ContactSolver::Rules::Coin_PiggyBank;
	_rules[ GameTypes::eCoinSilver		| GameTypes::ePiggyBank ]		= &ContactSolver::Rules::CoinSilver_PiggyBank;
	
	_rules[ GameTypes::eCoinSilver		| GameTypes::eCoinSilver ]		= &ContactSolver::Rules::CoinSilver_Coin;
	_rules[ GameTypes::eCoinSilver		| GameTypes::eCoin ]			= &ContactSolver::Rules::CoinSilver_Coin;
	
	_rules[ GameTypes::eCop				| GameTypes::eThief ]			= &ContactSolver::Rules::Cop_Thief;
	_rules[ GameTypes::eSwat			| GameTypes::eThief ]			= &ContactSolver::Rules::Swat_Thief;
	
	_rules[ GameTypes::eBorder			| GameTypes::eCoin ]			= &ContactSolver::Rules::Out_Of_Border;
	_rules[ GameTypes::eBorder			| GameTypes::eCoinSilver ]		= &ContactSolver::Rules::Out_Of_Border;
	_rules[ GameTypes::eBorder			| GameTypes::ePiggyBank ]		= &ContactSolver::Rules::Out_Of_Border;
	_rules[ GameTypes::eBorder			| GameTypes::eVehical ]			= &ContactSolver::Rules::Out_Of_Border;
	
	_rules[ GameTypes::eBulletExplosive	| GameTypes::eThief ]			= &ContactSolver::Rules::BulletExplosive_Other;
	_rules[ GameTypes::eBulletExplosive	| GameTypes::eCoin ]			= &ContactSolver::Rules::BulletExplosive_Money;
	_rules[ GameTypes::eBulletExplosive	| GameTypes::eCoinSilver ]		= &ContactSolver::Rules::BulletExplosive_Money;
	_rules[ GameTypes::eBulletExplosive	| GameTypes::ePiggyBank ]		= &ContactSolver::Rules::BulletExplosive_Money;

	_rules[ GameTypes::eAirBubble		| GameTypes::ePiggyBank ]		= &ContactSolver::Rules::AirBubble_Others;
	_rules[ GameTypes::eAirBubble		| GameTypes::eCoin ]			= &ContactSolver::Rules::AirBubble_Others;
	_rules[ GameTypes::eAirBubble		| GameTypes::eAirBubble ]		= &ContactSolver::Rules::AirBubble_Others;

	_rules[ GameTypes::eBlockRange ]								= &ContactSolver::Rules::BlockRange_All_ContactBegin;
	_rules[ ~GameTypes::eBlockRange ]								= &ContactSolver::Rules::BlockRange_All_ContactEnd;

	_rules[ GameTypes::eGlass ]										= &ContactSolver::Rules::Glass_Others;

	_rules[ GameTypes::eCustom ]									= &ContactSolver::Rules::Custom_All_ContactBegin;
	_rules[ -GameTypes::eCustom ]									= &ContactSolver::Rules::Custom_All_ContactEnd;
}
//-----------------------------------------------------------------------------------------
ContactSolver::~ContactSolver()
{

	_rules.clear();
//	_switchMap.clear();
	_deadBlocks.clear();
}
//-----------------------------------------------------------------------------------------
void ContactSolver::BeginContact( b2Contact* contact )
{
  	if ( _isQuiting )
		return;

	puBlock* b1 = (puBlock *) contact->GetFixtureA()->GetBody()->GetUserData();
	puBlock* b2 = (puBlock *) contact->GetFixtureB()->GetBody()->GetUserData();

	if ( b1 == NULL || b2 == NULL ) 
		return;

	//----------
	// Switch
	if ( b1->GetBlockType() == GameTypes::eSwitch || b2->GetBlockType() == GameTypes::eSwitch )
	{
		puSwitchBase *sswitch;

		if ( b1->GetBlockType() == GameTypes::eSwitch )
			sswitch = ( puSwitchBase * ) b1;
		else 
			sswitch = ( puSwitchBase * ) b2;
		
		sswitch->SetState( puSwitchBase::eON );
	}
	//----------
	// Bomb
	else if (( b1->GetBlockType() | b2->GetBlockType() ) & GameTypes::eBlockRange )
	{
		RuleFnPtr fn;
		fn = _rules[ GameTypes::eBlockRange ];

		if ( fn != NULL )
        {
            RuleFuncSettings ruleSettings( _recv, contact, b1, b2 );
			(*fn)( &ruleSettings );
        }
	}
	//----------
	// Glass
	else if (( b1->GetBlockType() | b2->GetBlockType() ) & GameTypes::eGlass )
	{
		RuleFnPtr fn;
		fn = _rules[ GameTypes::eGlass ];

		if ( fn != NULL )
        {
            RuleFuncSettings ruleSettings( _recv, contact, b1, b2 );
			(*fn)( &ruleSettings );
        }
	}
	//----------
	// custom bodies
	else if (( b1->GetBlockType() | b2->GetBlockType() ) & GameTypes::eCustom )
	{
		RuleFnPtr fn;
		fn = _rules[ GameTypes::eCustom ];

		if ( fn != NULL )
        {
            RuleFuncSettings ruleSettings( _recv, contact, b1, b2 );
			(*fn)( &ruleSettings );
        }
	}
	//----------
	// other bodies
	else
	{
		RuleFnPtr fn;
		fn = _rules[ b1->GetBlockType() | b2->GetBlockType() ];
		if ( fn != NULL )
        {
            RuleFuncSettings ruleSettings( _recv, contact, b1, b2 );
			(*fn)( &ruleSettings );
        }
	}
}
//--------------------------------------------------------------------------------------
void ContactSolver::EndContact( b2Contact* contact )
{
	if ( _isQuiting )
		return;

	puBlock* b1 = (puBlock *) contact->GetFixtureA()->GetBody()->GetUserData();
	puBlock* b2 = (puBlock *) contact->GetFixtureB()->GetBody()->GetUserData();

	if ( b1 == NULL || b2 == NULL ) 
		return;
	
	//----------
	// Switch
	if (( b1->GetBlockType() | b2->GetBlockType() ) & GameTypes::eSwitch )
	{
		puSwitchBase *sswitch;
		sswitch = ( puSwitchBase * )( b1->GetBlockType() == GameTypes::eSwitch ? b1 : b2 );
		sswitch->SetState( puSwitchBase::eOFF );
	}


	//----------
	// Bomb
	else if (( b1->GetBlockType() | b2->GetBlockType() ) & GameTypes::eBlockRange )
	{
		RuleFnPtr fn;
		fn = _rules[ ~GameTypes::eBlockRange ];

		if ( fn != NULL )
        {
            RuleFuncSettings ruleSettings( _recv, contact, b1, b2 );
			(*fn)( &ruleSettings );
        }
    }


	//----------
	// Custom
	else if (( b1->GetBlockType() | b2->GetBlockType() ) & GameTypes::eCustom )
	{
		RuleFnPtr fn;
		fn = _rules[ -GameTypes::eCustom ];

        RuleFuncSettings ruleSettings( _recv, contact, b1, b2 );
		(*fn)( &ruleSettings );
	}
}
//--------------------------------------------------------------------------------------
void ContactSolver::PreSolve( b2Contact* contact, const b2Manifold* oldManifold )
{

	//puBlock* b1 = (puBlock *) contact->GetFixtureA()->GetBody()->GetUserData();
	//puBlock* b2 = (puBlock *) contact->GetFixtureB()->GetBody()->GetUserData();

	//if ( b1 == NULL || b2 == NULL ) 
	//	return;

	//RuleFnPtr fn;
	//fn = _rules[ b1->GetBlockType() | b2->GetBlockType() ];
	//
	//if ( fn != NULL )
	//	(*fn)( &RuleFuncSettings( contact, oldManifold, b1, b2 ));

}
//--------------------------------------------------------------------------------------
//void ContactSolver::SetSwitchRule( puSwitchBase* sswitch,  puBlock *target )
//{
//	int index;
//	
//	index = (int) sswitch;
//	_switchMap[ index ] = target;
//}
//--------------------------------------------------------------------------------------
void ContactSolver::Rules::Coin_Thief( RuleFuncSettings *param )
{
	Level *level;
	level = (Level *) param->_recv;
	puBlock *thief;

	if ( level->GetLevelState() == GameTypes::eLevelState_Finished )
		return;

	if ( param->_block1->GetBlockType() == GameTypes::eCoin || param->_block1->GetBlockType() == GameTypes::eCoinSilver )
	{
		_deadBlocks.insert( param->_block1 );
		thief = param->_block2;
	}
	else
	{
		_deadBlocks.insert( param->_block2 );
		thief = param->_block1;
	}

	param->_recv->GameCoinStolen( thief );
}
//--------------------------------------------------------------------------------------
void ContactSolver::Rules::PiggyBank_Thief( RuleFuncSettings *param )
{
	Level *level;
	puBlock *thief;
	puBlock *piggy;

	level = (Level *) param->_recv;

	if ( level->GetLevelState() == GameTypes::eLevelState_Finished )
		return;

	if ( param->_block1->GetBlockType() == GameTypes::ePiggyBank )
	{
		_deadBlocks.insert( param->_block1 );
		piggy = param->_block1;
		thief = param->_block2;
	}
	else
	{
		_deadBlocks.insert( param->_block2 );
		piggy = param->_block2;
		thief = param->_block1;
	}
	
	param->_recv->GamePiggyStolen( piggy, thief );
}
//--------------------------------------------------------------------------------------
void ContactSolver::Rules::Coin_Bomb( RuleFuncSettings *param )
{

	if ( param->_block1->GetBlockType() == GameTypes::eCoin || param->_block1->GetBlockType() == GameTypes::eCoinSilver )
		_deadBlocks.insert( param->_block1 );
	else
		_deadBlocks.insert( param->_block2 );

//	param->_recv->GameCoinStolen();
}
//--------------------------------------------------------------------------------------
void ContactSolver::Rules::PiggyBank_Bomb( RuleFuncSettings *param )
{

	if ( param->_block1->GetBlockType() == GameTypes::ePiggyBank )
		_deadBlocks.insert( param->_block1 );
	else
		_deadBlocks.insert( param->_block2 );

	//param->_recv->GameCoinStolen();
}
//--------------------------------------------------------------------------------------
void ContactSolver::Rules::Coin_PiggyBank( RuleFuncSettings *param  )
{
	puBlock *coin;
	puBlock *piggy;

	if ( param->_block1->GetBlockType() == GameTypes::eCoin || param->_block1->GetBlockType() == GameTypes::eCoinSilver )
	{
		coin = param->_block1;
		piggy = param->_block2;
	}
	else
	{
		coin = param->_block2;
		piggy = param->_block1;
	}

	if ( ! _deadBlocks.count( coin ))
	{
		_deadBlocks.insert( coin );
		param->_recv->GameCoinCollected( piggy );
	}
}
//--------------------------------------------------------------------------------------
void ContactSolver::Rules::CoinSilver_PiggyBank( RuleFuncSettings *param  )
{
	puBlock *coin;

	if ( param->_block1->GetBlockType() == GameTypes::eCoinSilver )
		coin = param->_block1;
	else
		coin = param->_block2;

	if ( ! _deadBlocks.count( coin ))
	{
		_deadBlocks.insert( coin );
		param->_recv->GameCoinSilverCollected( coin->GetPosition() );
	}
}
//--------------------------------------------------------------------------------------
void ContactSolver::Rules::Cop_Thief( RuleFuncSettings *param  )
{
	puBlock *policeman;

	if ( param->_block1->GetBlockType() == GameTypes::eThief )
	{
		_deadBlocks.insert( param->_block1 );
		policeman = param->_block2;
	}
	else
	{
		_deadBlocks.insert( param->_block2 );
		policeman = param->_block1;
	}

	param->_recv->GameThiefCaught( policeman );
}
//--------------------------------------------------------------------------------------
void ContactSolver::Rules::Swat_Thief( RuleFuncSettings *param  )
{
	_deadBlocks.insert( param->_block1 );
	_deadBlocks.insert( param->_block2 );
}
//--------------------------------------------------------------------------------------
void ContactSolver::Rules::Out_Of_Border( RuleFuncSettings *param  )
{
	param->_recv->GameOutOfBorder();
}
//--------------------------------------------------------------------------------------
void ContactSolver::Rules::BulletExplosive_Other( RuleFuncSettings *param )
{
	_deadBlocks.insert( param->_block1 );
	_deadBlocks.insert( param->_block2 );

	if ( param->_block1->GetBlockType() == GameTypes::eBulletExplosive )
		param->_recv->GameBlockExplode( param->_block2->GetPosition() );
	else
		param->_recv->GameBlockExplode( param->_block1->GetPosition() );
}
//--------------------------------------------------------------------------------------
void ContactSolver::Rules::BulletExplosive_Money( RuleFuncSettings *param )
{
	_deadBlocks.insert( param->_block1 );
	_deadBlocks.insert( param->_block2 );

	if ( param->_block1->GetBlockType() == GameTypes::eBulletExplosive )
		param->_recv->GameMoneyExplode( param->_block2->GetPosition() );
	else
		param->_recv->GameMoneyExplode( param->_block1->GetPosition() );

	param->_recv->GameOutOfBorder();
}
//--------------------------------------------------------------------------------------
void ContactSolver::Rules::BlockRange_All_ContactBegin( RuleFuncSettings *param )
{
	puBlockWithRange_	*ranger;
	puBlock				*victim;

	if ( param->_block1->GetBlockType() == GameTypes::eBlockRange )
	{
		ranger = ( puBlockWithRange_ *) ( param->_block1 )->GetRoot();
		victim = ( param->_block2 );
	}
	else
	{
		ranger = ( puBlockWithRange_ *) ( param->_block2 )->GetRoot();
		victim = ( param->_block1 );
	}

	ranger->BlockRangeIn( victim );
}
//--------------------------------------------------------------------------------------
void ContactSolver::Rules::BlockRange_All_ContactEnd( RuleFuncSettings *param )
{
	puBlockWithRange_	*ranger;
	puBlock				*victim;

	if ( param->_block1->GetBlockType() == GameTypes::eBlockRange )
	{
		ranger = ( puBlockWithRange_ *) ( param->_block1 )->GetRoot();
		victim = ( param->_block2 );
	}
	else
	{
		ranger = ( puBlockWithRange_ *) ( param->_block2 )->GetRoot();
		victim = ( param->_block1 );
	}

	ranger->BlockRangeOut( victim );
}
//--------------------------------------------------------------------------------------
void ContactSolver::Rules::Custom_All_ContactBegin( RuleFuncSettings *param )
{
	param->_recv->GameCustomAction_ContactBegin( param->_block1, param->_block2, param->_contact );
}
//--------------------------------------------------------------------------------------
void ContactSolver::Rules::Custom_All_ContactEnd( RuleFuncSettings *param )
{
	param->_recv->GameCustomAction_ContactEnd( param->_block1, param->_block2, param->_contact );
}
//--------------------------------------------------------------------------------------
void ContactSolver::Rules::CoinSilver_Coin( RuleFuncSettings *param )
{
	puCoinSilver_ *silverCoin;

	if ( param->_block1->GetBlockType() == GameTypes::eCoinSilver )
		silverCoin = (puCoinSilver_*) param->_block1;
	else
		silverCoin = (puCoinSilver_*) param->_block2;
		
	silverCoin->MoneyMakeMoney();
	_deadBlocks.insert( silverCoin );
}
//--------------------------------------------------------------------------------------
void ContactSolver::Rules::AirBubble_Others( RuleFuncSettings *param )
{
	puAirBubble_ *airBlock;
	puBlock *other;

	if ( param->_block1->GetBlockType() == GameTypes::eAirBubble )
	{
		airBlock = (puAirBubble_*) param->_block1;
		other = param->_block2;
	}
	else
	{
		airBlock = (puAirBubble_*) param->_block2;
		other = param->_block1;
	}
	
	_airBubbleCollisions.push_back( AirBubblePair( airBlock, other ));
}
//--------------------------------------------------------------------------------------
void ContactSolver::Rules::Glass_Others( RuleFuncSettings *param )
{
	if ( param->_block1->GetBlockType() == GameTypes::eGlass &&
		param->_block1->ShouldSpeedBreak() )
		_glassCollisions.insert( param->_block1 );

	if ( param->_block2->GetBlockType() == GameTypes::eGlass &&
		param->_block2->ShouldSpeedBreak() )
		_glassCollisions.insert( param->_block2 );
}
//--------------------------------------------------------------------------------------
void ContactSolver::Process()
{
	for ( BlockSet::iterator it = _deadBlocks.begin(); it != _deadBlocks.end(); it++ )
	{
		(*it)->Destroy();
	}
	_deadBlocks.clear();

	//---------------------
	for ( AirBubbleCollisions::iterator it = _airBubbleCollisions.begin(); it != _airBubbleCollisions.end(); it++ )
		(*it)._bubble->Colide( (*it)._block );

	_airBubbleCollisions.clear();

	//---------------------
	for ( BlockSet::iterator it = _glassCollisions.begin(); it != _glassCollisions.end(); it++ )
		(*it)->Destroy();

	_glassCollisions.clear();
}
//--------------------------------------------------------------------------------------
