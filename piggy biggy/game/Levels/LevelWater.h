#ifndef __LEVELWATER_H__
#define __LEVELWATER_H__

#include "Level.h"

//----------------------------------------------------------------------------------
class LevelWater : public Level
{
public:
	virtual void Step( cocos2d::ccTime dt );

	void SetWavingForce( b2Vec2 force ) { _wavingForce = force; }
	void SetWavingFrequency( float freq ) { _wavingFrequency = freq; }

protected:
	LevelWater( GameTypes::LevelEnum levelEnum, bool viewMode );

	b2Vec2 GetWaveForce();

protected:
	float	_wavingFrequency;
	float	_waterBounancy;
	b2Vec2	_wavingForce;
	
	float	_wavingCounter;
};
//----------------------------------------------------------------------------------
class LevelAquarium : public LevelWater
{
protected:
	LevelAquarium( GameTypes::LevelEnum levelEnum, bool viewMode );

protected:
	puWallBox<480,8,eBlockScaled>	_wallBox1;
	puWallBox<480,8,eBlockScaled>	_wallBox4;
	puWallBox<8, 320,eBlockScaled>	_wallBox2;
	puWallBox<8, 320,eBlockScaled>	_wallBox3;
};
//----------------------------------------------------------------------------------
class LevelSea : public LevelWater
{
protected:
	LevelSea( GameTypes::LevelEnum levelEnum, bool viewMode );

protected:
	puWallBox<480,8,eBlockScaled>	_wallBox1;
	puWallBox<480,8,eBlockScaled>	_wallBox4;
	puWallBox<8, 320,eBlockScaled>	_wallBox2;
	puWallBox<8, 320,eBlockScaled>	_wallBox3;
};
//----------------------------------------------------------------------------------

#endif

