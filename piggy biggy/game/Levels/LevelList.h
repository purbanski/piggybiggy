#ifndef __LEVELLIST_H__
#define __LEVELLIST_H__

#include <string>
#include <list>
#include "cocos2d.h"
#include "LevelFactory.h"
#include <set>

USING_NS_CC;
using namespace std;

//--------------------------------------------------------------------------------------------------------
class LevelList : public CCNode
{
public:
	virtual ~LevelList();
	void SelectLevel( CCObject* pSender );
	bool IsInList( GameTypes::LevelEnum );

	typedef list<GameTypes::LevelEnum> LevelListType;
	LevelListType GetList() { return _list; }

protected:
	LevelList();
	void CreateList();
	virtual string GetName() = 0;

protected:
	LevelListType	_list;
	CCMenu *_menu;
};
//--------------------------------------------------------------------------------------------------------
class LevelList_Intro : public LevelList
{
public:
	LevelList_Intro();

protected:
	virtual string GetName() { return string("Intro"); }
};
//--------------------------------------------------------------------------------------------------------
class LevelList_Roll : public LevelList
{
public:
	LevelList_Roll();

protected:
	virtual string GetName() { return string("Lets Roll"); }
};
//--------------------------------------------------------------------------------------------------------
class LevelList_Swing : public LevelList
{
public:
	LevelList_Swing();

protected:
	virtual string GetName() { return string("Lets Swing"); }
};
//--------------------------------------------------------------------------------------------------------

class LevelList_Other : public LevelList
{
public:
	LevelList_Other();

protected:
	virtual string GetName() { return string("A bit of logic"); }
};
//--------------------------------------------------------------------------------------------------------
class LevelList_Tetris : public LevelList
{
public:
	LevelList_Tetris();

protected:
	virtual string GetName() { return string("Blocks :)"); }
};
//--------------------------------------------------------------------------------------------------------
class LevelList_Domino : public LevelList
{
public:
	LevelList_Domino();

protected:
	virtual string GetName() { return string("Domino"); }
};
//--------------------------------------------------------------------------------------------------------
class LevelList_Construction_1 : public LevelList
{
public:
	LevelList_Construction_1();

protected:
	virtual string GetName() { return string("Constructions 1"); }
};
//--------------------------------------------------------------------------------------------------------
class LevelList_Construction_2 : public LevelList
{
public:
	LevelList_Construction_2();

protected:
	virtual string GetName() { return string("Constructions 2"); }
};
//--------------------------------------------------------------------------------------------------------
class LevelList_Gun : public LevelList
{
public:
	LevelList_Gun();

protected:
	virtual string GetName() { return string("Lets shoot"); }
};
//--------------------------------------------------------------------------------------------------------
class LevelList_Reel : public LevelList
{
public:
	LevelList_Reel();

protected:
	virtual string GetName() { return string("Lets reel"); }
};
//--------------------------------------------------------------------------------------------------------
class LevelList_Chain1 : public LevelList
{
public:
	LevelList_Chain1();

protected:
	virtual string GetName() { return string("Unchain my pig"); }
};
//--------------------------------------------------------------------------------------------------------
class LevelList_Chain2 : public LevelList
{
public:
	LevelList_Chain2();

protected:
	virtual string GetName() { return string("Unchain my pig 2"); }
};
//--------------------------------------------------------------------------------------------------------
class LevelList_Bank : public LevelList
{
public:
	LevelList_Bank();

protected:
	virtual string GetName() { return string("Lets robby a bank"); }
};
//--------------------------------------------------------------------------------------------------------
class LevelList_Rotate1 : public LevelList
{
public:
	LevelList_Rotate1();

protected:
	virtual string GetName() { return string("Lets rotate"); }
};
//--------------------------------------------------------------------------------------------------------
class LevelList_Rotate2 : public LevelList
{
public:
	LevelList_Rotate2();

protected:
	virtual string GetName() { return string("Keep on rotating"); }
};
//--------------------------------------------------------------------------------------------------------
class LevelList_Vehical : public LevelList
{
public:
	LevelList_Vehical();

protected:
	virtual string GetName() { return string("Lets drive"); }
};
//--------------------------------------------------------------------------------------------------------
class LevelList_Labyrinth : public LevelList
{
public:
	LevelList_Labyrinth();

protected:
	virtual string GetName() { return string("Labyrinth"); }
};
//--------------------------------------------------------------------------------------------------------
class LevelList_Bombs1 : public LevelList
{
public:
	LevelList_Bombs1();

protected:
	virtual string GetName() { return string("Lets Bomb 1!"); }
};
//--------------------------------------------------------------------------------------------------------
class LevelList_Bombs2 : public LevelList
{
public:
	LevelList_Bombs2();

protected:
	virtual string GetName() { return string("Lets Bomb 2!"); }
};
//--------------------------------------------------------------------------------------------------------
class LevelList_Others : public LevelList
{
public:
	LevelList_Others();

protected:
	virtual string GetName() { return string("Others"); }
};
//--------------------------------------------------------------------------------------------------------
class LevelList_Magnets : public LevelList
{
public:
	LevelList_Magnets();

protected:
	virtual string GetName() { return string("Magnets"); }
};
//--------------------------------------------------------------------------------------------------------
class LevelList_Synchronize : public LevelList
{
public:
	LevelList_Synchronize();

protected:
	virtual string GetName() { return string("Lets Synchronize!"); }
};
//--------------------------------------------------------------------------------------------------------
class LevelList_Test : public LevelList
{
public:
	LevelList_Test();

protected:
	virtual string GetName() { return string("Lets Test!"); }
};
//--------------------------------------------------------------------------------------------------------
class LevelList_Rafal1 : public LevelList
{
public:
	LevelList_Rafal1();

protected:
	virtual string GetName() { return string("Rafal 1"); }
};
//--------------------------------------------------------------------------------------------------------
class LevelList_Rafal2 : public LevelList
{
public:
	LevelList_Rafal2();

protected:
	virtual string GetName() { return string("Rafal 2"); }
};
//--------------------------------------------------------------------------------------------------------
class LevelList_Pawel1 : public LevelList
{
public:
	LevelList_Pawel1();

protected:
	virtual string GetName() { return string("Pawel 1"); }
};
//--------------------------------------------------------------------------------------------------------
class LevelList_Pawel2 : public LevelList
{
public:
	LevelList_Pawel2();

protected:
	virtual string GetName() { return string("Pawel 2"); }
};
//--------------------------------------------------------------------------------------------------------
class LevelList_Pawel3 : public LevelList
{
public:
	LevelList_Pawel3();

protected:
	virtual string GetName() { return string("Pawel 3"); }
};
//--------------------------------------------------------------------------------------------------------
class LevelList_Bogusia : public LevelList
{
public:
	LevelList_Bogusia();

protected:
	virtual string GetName() { return string("Bogusia"); }
};
//--------------------------------------------------------------------------------------------------------
class LevelList_TomekW : public LevelList
{
public:
	LevelList_TomekW();

protected:
	virtual string GetName() { return string("Tomek Wierel"); }
};
//--------------------------------------------------------------------------------------------------------
class LevelList_JacekZ : public LevelList
{
public:
	LevelList_JacekZ();

protected:
	virtual string GetName() { return string("Jacek Zielinski"); }
};
//--------------------------------------------------------------------------------------------------------
class LevelList_Marzena : public LevelList
{
public:
	LevelList_Marzena();

protected:
	virtual string GetName() { return string("Marzena"); }
};
//--------------------------------------------------------------------------------------------------------
#endif
