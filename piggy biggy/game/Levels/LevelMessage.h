#ifndef __LEVELMESSAGE_H__
#define __LEVELMESSAGE_H__

#include "cocos2d.h"
#include <Box2D/Box2D.h>
#include <list>
#include <set>
#include "GameTypes.h"

USING_NS_CC;
using namespace std;
using namespace GameTypes;

//----------------------------------------------------------------------------------------------
class LevelMsg : public CCNode
{
public:
	static float _sHandDeltaX;
	static float _sHandDeltaY;

	typedef enum
	{
		eMsg_PointingCircle,
		eMsg_MovingPointingCircle,
		eMsg_Text
	} MessageType;


	virtual void DisplayMsg() = 0;
	int GetTime() { return _time; }
	int GetZOrder();

protected:
	LevelMsg( MessageType type, int time, float duration, const char *text, const b2Vec2 &position, GameTypes::SpriteLayers zOrder = GameTypes::eLevelMsg );
    virtual ~LevelMsg();
	
	
	
	void Anim_ShowHide( CCSprite *sprite, float timeToUse, int finalOpacity, bool callback );
	
	typedef set<CCFiniteTimeAction *> Actions;

	void RunAnim( CCSprite *sprite, int finalOpacity, CCFiniteTimeAction* action );
	typedef enum {
		//eAnimCallback_None = 0,
		eAnimCallback_FadeLock,
		eAnimCallback_FadeUnlock
	} AnimCallback;

	void RunAnim2( CCSprite *sprite, int finalOpacity, float timeToUse, AnimCallback animCallbackType, CCFiniteTimeAction *action );

	Actions GetAnim_Intro( int finalOpacity );
	Actions GetAnim_Outro();

	virtual void AnimDo_MoveLock( CCNode* target, void* data );
	virtual void AnimDo_FadeLock( CCNode* target, void* data );

protected:
	int				_time;
	float			_duration;
	MessageType		_type;
	string			_text;
	b2Vec2			_position;
	int				_zOrder;

private:
};
//----------------------------------------------------------------------------------------------
class LevelMsg_Text : public LevelMsg
{
public:
	LevelMsg_Text( int time, float duration, const char *text, const b2Vec2 &textPosition );
	virtual void DisplayMsg();
};
//----------------------------------------------------------------------------------------------
class LevelMsg_TextMoving : public LevelMsg
{
public:
	LevelMsg_TextMoving( int time, float duration, const char *text, const b2Vec2 &textPosition, GameTypes::SpriteLayers zOrder = GameTypes::eLevelMsg );
	virtual void DisplayMsg();
};
//----------------------------------------------------------------------------------------------
class LevelMsg_PointingCircle : public LevelMsg
{
public:
	LevelMsg_PointingCircle( int time, float duration, const char *text, const b2Vec2 &position, const b2Vec2 &circlePosition, GameTypes::SpriteLayers zOrder =GameTypes::eLevelMsg );
	virtual void DisplayMsg();

protected:
	b2Vec2	_circlePosition;
};
//----------------------------------------------------------------------------------------------
class LevelMsg_MovingPointingCircle : public LevelMsg
{
public:
	LevelMsg_MovingPointingCircle( int time, float duration, const char *text, const b2Vec2 &position, const b2Vec2 &circlePositionStart, const b2Vec2 &circleMoveBy, GameTypes::SpriteLayers zOrder =GameTypes::eLevelMsg );
	
	virtual void DisplayMsg();

protected:
	b2Vec2	_circlePositionStart;
	b2Vec2	_circleMoveBy;
};
//----------------------------------------------------------------------------------------------
class LevelMsg_ReelIntro : public LevelMsg
{
public:
	LevelMsg_ReelIntro( int time, float duration, const char *text, const b2Vec2 &position, const b2Vec2 &circlePositionStart, const b2Vec2 &circleMoveBy, GameTypes::SpriteLayers zOrder =GameTypes::eLevelMsg );
	
	virtual void DisplayMsg();

protected:
	b2Vec2	_circlePositionStart;
	b2Vec2	_circleMoveBy;
};
//----------------------------------------------------------------------------------------------
class LevelMsg_SkipLevelIntro : public LevelMsg
{
public:
	LevelMsg_SkipLevelIntro( int time );

	virtual void DisplayMsg();
    
    void ShowLevelSubMenu();
    void CleanUp();
    
protected:
};
//----------------------------------------------------------------------------------------------
class LevelMsg_SkipLevelIntro_LevelFailed : public LevelMsg
{
public:
	LevelMsg_SkipLevelIntro_LevelFailed( int time );
	virtual void DisplayMsg();
    
    void RestartLevelPre();
    void RestartLevel();
};
//----------------------------------------------------------------------------------------------
class LevelMsg_ArrowMsg : public LevelMsg
{
public:
	LevelMsg_ArrowMsg(
                      int time,
                      const char *text,
                      float textSize,
                      const CCPoint &pos,
                      unsigned int repeat = 3,
                      bool flip = false,
                      float angle = 0.0f );
	virtual void DisplayMsg();
    
protected:
    string          _text;
    float           _textSize;
    CCPoint         _startPos;
    bool            _flip;
    unsigned int    _repeatCount;
    float           _angle;
};

//----------------------------------------------------------------------------------------------
// Level Msg Manager
//----------------------------------------------------------------------------------------------
class LevelMsgManager : public list<LevelMsg*>
{
public:
	LevelMsgManager();
	~LevelMsgManager();

	void clear();
	void push_back( GameTypes::OneTimeMsg message, LevelMsg *msg );
    iterator erase( iterator it );
};

#endif
