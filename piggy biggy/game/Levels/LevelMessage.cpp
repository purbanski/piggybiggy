#include "LevelMessage.h"
#include "GameConfig.h"
#include "Game.h"
#include "GameStatus.h"
#include "Skins.h"
#include "Debug/MyDebug.h"
#include "Preloader.h"
#include "Platform/Lang.h"
#include "cocos2dxExt/CCMoveByArc.h"

//--------------------------------------------------------------------------------------
float LevelMsg::_sHandDeltaX = 95.0f;
float LevelMsg::_sHandDeltaY = -115.0f;


//--------------------------------------------------------------------------------------
// Level Message Manager
//--------------------------------------------------------------------------------------
LevelMsgManager::LevelMsgManager()
{

}
//--------------------------------------------------------------------------------------
LevelMsgManager::~LevelMsgManager()
{
	clear();
}
//--------------------------------------------------------------------------------------
void LevelMsgManager::clear()
{
    D_LOG("size %lu",size());
	for ( iterator it = begin(); it != end(); it++ )
	{
		if ( *it && (*it)->retainCount() )
            (*it)->release();
	}
}
//--------------------------------------------------------------------------------------
LevelMsgManager::iterator LevelMsgManager::erase(iterator it)
{
    (*it)->release();
    return list<LevelMsg*>::erase( it );
}
//--------------------------------------------------------------------------------------
void LevelMsgManager::push_back( GameTypes::OneTimeMsg message, LevelMsg *msg )
{
	msg->autorelease();
	
	GameStatus *status;
	status = Game::Get()->GetGameStatus();

	if ( ! status->GetMsgPlayed( message ) /*|| message == eOTMsg_SkipLevelIntro*/ )
	{
		status->SetMsgPlayed( message );
		msg->retain();
		list<LevelMsg*>::push_back( msg );
	}
	else
	{
#ifdef BUILD_EDITOR
		status->SetMsgPlayed( message );
		msg->retain();
		list<LevelMsg*>::push_back( msg );
#endif
	}
}
//--------------------------------------------------------------------------------------






//--------------------------------------------------------------------------------------
// Level Message
//--------------------------------------------------------------------------------------
LevelMsg::LevelMsg( MessageType type, int time, float duration, const char *text, const b2Vec2 &position, GameTypes::SpriteLayers zOrder )
{
	_type = type;
	_time = time;
	_text.append( text );
	_position = position;
	_duration = duration;
	_zOrder = zOrder;

	Preloader::Get()->AddImageDir( "Images/HandPointer/" );
}
//--------------------------------------------------------------------------------------
LevelMsg::~LevelMsg()
{
    removeAllChildrenWithCleanup(true);
}
//--------------------------------------------------------------------------------------
void LevelMsg::DisplayMsg()
{
	if ( _text.size() == 0 )
		return;

	CCSprite *background = CCSprite::spriteWithFile( "Images/Other/none.png" );
	CCLabelBMFont *label = CCLabelBMFont::labelWithString( _text.c_str(), Config::LevelMsgFont );

	label->setPosition( ccp( _position.x, _position.y ));
	//label->setScale( 0.0f );
	

	label->setOpacity( 0 );
	background->setOpacity( 0 );

	addChild( label, 7 );
	addChild( background, 6 );

	{
		CCFadeTo *fadeOut = CCFadeTo::actionWithDuration(  0.6f, 0 );
		CCFadeTo *fadeIn = CCFadeTo::actionWithDuration( 0.6f, 210 ); 
		CCMoveBy *emptyAction = CCMoveBy::actionWithDuration( _duration, CCPointZero );

		background->runAction( CCSequence::actions( fadeIn, emptyAction, fadeOut, NULL));
	}

	{
		CCFadeTo *fadeOut = CCFadeTo::actionWithDuration(  0.6f, 0 );
		CCFadeTo *fadeIn = CCFadeTo::actionWithDuration( 0.6f, 255 );
		CCMoveBy *emptyAction = CCMoveBy::actionWithDuration( _duration, CCPointZero );

		label->runAction( CCSequence::actions( fadeIn, emptyAction, fadeOut, NULL));
	}
}
//--------------------------------------------------------------------------------------
void LevelMsg::Anim_ShowHide( CCSprite *sprite,float timeToUse, int finalOpacity, bool callback )
{
	float dur;
	dur = 0.4f;

	//-----------------------
	//----- scale action
	sprite->setScale( 0.01f );
	CCFiniteTimeAction*  actionScale = CCSequence::actions(
		CCScaleTo::actionWithDuration( dur, 1.2f ),
		CCScaleTo::actionWithDuration( dur / 4.0f, 0.9f ),
		CCScaleTo::actionWithDuration( dur / 4.0f, 1.0f ),
//		CCScaleTo::actionWithDuration( timeToUse , 1.0f ),
		CCCallFuncND::actionWithTarget(this, callfuncND_selector(LevelMsg::AnimDo_FadeLock), (void*) sprite ), 
		CCScaleTo::actionWithDuration( dur / 4.0f, 0.9f ),
		CCScaleTo::actionWithDuration( dur / 4.0f, 1.2f ),
		CCScaleTo::actionWithDuration( dur, 0.01f ),
		NULL);


	typedef void (CCObject::*SEL_CallFuncND)(CCNode*, void*);

	//-----------------------
	//----- fade action
	sprite->setOpacity( 0 );
	CCFiniteTimeAction*  actionFade = CCSequence::actions(
		CCFadeTo::actionWithDuration( dur + dur / 2.0f, finalOpacity ),
		CCCallFuncND::actionWithTarget(this, callfuncND_selector(LevelMsg::AnimDo_MoveLock), (void*) sprite ), 
		CCRotateBy::actionWithDuration( timeToUse, 0.0f ),
		CCFadeTo::actionWithDuration( dur + dur / 2.0f, 0 ),
		NULL);

	sprite->runAction( actionFade );
	sprite->runAction( actionScale );
}
//--------------------------------------------------------------------------------------
void LevelMsg::AnimDo_MoveLock( CCNode* target, void* data )
{
}
//--------------------------------------------------------------------------------------
int LevelMsg::GetZOrder()
{
	return _zOrder;
}
//--------------------------------------------------------------------------------------
LevelMsg::Actions LevelMsg::GetAnim_Intro( int finalOpacity )
{
	Actions anims;

	return anims;
}
//--------------------------------------------------------------------------------------
LevelMsg::Actions LevelMsg::GetAnim_Outro()
{

	Actions anims;

	return anims;
}
//--------------------------------------------------------------------------------------
void LevelMsg::RunAnim( CCSprite *sprite, int finalOpacity, CCFiniteTimeAction* action )
{
	//float dur;
	//dur = 0.4f;

	////-----------------------
	////----- scale action
	//sprite->setScale( 0.01f );
	//CCFiniteTimeAction*  actionScale = CCSequence::actions(
	//	CCScaleTo::actionWithDuration( dur, 1.2f ),
	//	CCScaleTo::actionWithDuration( dur / 4.0f, 0.9f ),
	//	CCScaleTo::actionWithDuration( dur / 4.0f, 1.0f ),
	//	//CCScaleTo::actionWithDuration( timeToUse , 1.0f ),
	//	CCScaleTo::actionWithDuration( dur / 4.0f, 0.9f ),
	//	CCScaleTo::actionWithDuration( dur / 4.0f, 1.2f ),
	//	CCScaleTo::actionWithDuration( dur, 0.01f ),
	//	NULL);


	//typedef void (CCObject::*SEL_CallFuncND)(CCNode*, void*);

	////-----------------------
	////----- fade action
	//sprite->setOpacity( 0 );
	//CCFiniteTimeAction*  actionFade = CCSequence::actions(
	//	CCFadeTo::actionWithDuration( dur + dur / 2.0f, finalOpacity ),
	//	CCCallFuncND::actionWithTarget(this, callfuncND_selector(LevelMsg::AnimDo_MoveLock), (void*) sprite ), 
	//	CCRotateBy::actionWithDuration( timeToUse, 0.0f ),
	//	CCFadeTo::actionWithDuration( dur + dur / 2.0f, 0 ),
	//	NULL);

	//sprite->runAction( actionFade );
	//sprite->runAction( actionScale );
}

void LevelMsg::RunAnim2(CCSprite *sprite, int finalOpacity, float timeToUse,
                        AnimCallback animCallbackType, CCFiniteTimeAction *action )
{
	float dur;
	dur = 0.4f;

	switch ( animCallbackType )
	{
		case eAnimCallback_FadeLock :
		{
			//-----------------------
			//----- scale action
			sprite->setScale( 0.01f );
			CCFiniteTimeAction*  actionScale = CCSequence::actions(
				CCScaleTo::actionWithDuration( dur, 1.2f ),
				CCScaleTo::actionWithDuration( dur / 4.0f, 0.9f ),
				CCScaleTo::actionWithDuration( dur / 4.0f, 1.0f ),
				action, 
				//CCCallFuncND::actionWithTarget( this, (SEL_CallFuncND)( func ), (void*) sprite ),		
				//CCScaleTo::actionWithDuration( timeToUse , 1.0f ),
				CCScaleTo::actionWithDuration( dur / 4.0f, 0.9f ),
				CCScaleTo::actionWithDuration( dur / 4.0f, 1.2f ),
				CCScaleTo::actionWithDuration( dur, 0.01f ),
				NULL);


			//-----------------------
			//----- fade action
			sprite->setOpacity( 0 );
			CCFiniteTimeAction*  actionFade = CCSequence::actions(
				CCFadeTo::actionWithDuration( dur + dur / 2.0f, finalOpacity ),
				CCRotateBy::actionWithDuration( timeToUse , 0.0f ),
				CCFadeTo::actionWithDuration( dur + dur / 2.0f, 0 ),
				NULL);

			sprite->runAction( actionFade );
			sprite->runAction( actionScale );
		}
		break;

		case eAnimCallback_FadeUnlock :
			{
				//-----------------------
				//----- scale action
				sprite->setScale( 0.01f );
				CCFiniteTimeAction*  actionScale = CCSequence::actions(
					CCScaleTo::actionWithDuration( dur, 1.2f ),
					CCScaleTo::actionWithDuration( dur / 4.0f, 0.9f ),
					CCScaleTo::actionWithDuration( dur / 4.0f, 1.0f ),
					CCRotateBy::actionWithDuration( timeToUse, 0.0f ),
					CCScaleTo::actionWithDuration( dur / 4.0f, 0.9f ),
					CCScaleTo::actionWithDuration( dur / 4.0f, 1.2f ),
					CCScaleTo::actionWithDuration( dur, 0.01f ),
					NULL);


				//-----------------------
				//----- fade action
				sprite->setOpacity( 0 );
				CCFiniteTimeAction*  actionFade = CCSequence::actions(
					CCFadeTo::actionWithDuration( dur + dur / 2.0f, finalOpacity ),
					action,
					CCFadeTo::actionWithDuration( dur + dur / 2.0f, 0 ),
					NULL);

				sprite->runAction( actionFade );
				sprite->runAction( actionScale );
			}
			break;

	}
}

void LevelMsg::AnimDo_FadeLock( CCNode* target, void* data )
{

}
//--------------------------------------------------------------------------------------





//--------------------------------------------------------------------------------------
// Level Message Text
//--------------------------------------------------------------------------------------
LevelMsg_Text::LevelMsg_Text( int time, float duration, const char *text, const b2Vec2 &position ) : LevelMsg( LevelMsg::eMsg_Text, time, duration, text, position)
{
}
//--------------------------------------------------------------------------------------
void LevelMsg_Text::DisplayMsg()
{
	LevelMsg::DisplayMsg();
}
//--------------------------------------------------------------------------------------




//--------------------------------------------------------------------------------------
// Level Message Text
//--------------------------------------------------------------------------------------
LevelMsg_TextMoving::LevelMsg_TextMoving( int time, float duration, const char *text, const b2Vec2 &position, GameTypes::SpriteLayers zOrder ) : LevelMsg( LevelMsg::eMsg_Text, time, duration, text, position, zOrder )
{
	//------------------------------
	// Preload font
	CCLabelBMFont::labelWithString( "", Config::LevelMsgFont );
}
//--------------------------------------------------------------------------------------
void LevelMsg_TextMoving::DisplayMsg()
{
	CCLabelBMFont *label = CCLabelBMFont::labelWithString( _text.c_str(), Config::LevelMsgFont );

	label->setPosition( ccp( _position.x, _position.y ) );
	label->setScale( 1.0f / getScale() );
	addChild( label );

	CCMoveTo *move = CCMoveTo::actionWithDuration( _duration, ccp( -_position.x, _position.y ));
	label->runAction( CCSequence::actions( move, NULL ));
}


//--------------------------------------------------------------------------------------
// Level Message Pointing Circle
//--------------------------------------------------------------------------------------
LevelMsg_PointingCircle::LevelMsg_PointingCircle( int time, float duration, const char *text, const b2Vec2 &textPosition, const b2Vec2 &circlePosition, GameTypes::SpriteLayers zOrder ) : LevelMsg( LevelMsg::eMsg_PointingCircle, time, duration, text, textPosition )
{
	_circlePosition.x = ( circlePosition.x - Config::GameSize.width / 2.0f / RATIO ) * RATIO;
	_circlePosition.y = ( circlePosition.y - Config::GameSize.height / 2.0f / RATIO ) * RATIO;
}

//--------------------------------------------------------------------------------------
void LevelMsg_PointingCircle::DisplayMsg()
{
	//-------------------------
	// Sprite Out
	//-------------------------
	CCSprite *hand = CCSprite::spriteWithFile( "Images/HandPointer/Hand.png" );
	CCSprite *handTap = CCSprite::spriteWithFile( "Images/HandPointer/HandTap.png" );

	handTap->setOpacity( 0 );

	addChild( hand, 5 );
	addChild( handTap, 10 );

	float fadeTime;
	float stopTime;
	float totalTime;
	
	float initWaitTime;
	float waitTime;
	float shortWaitTime;

	initWaitTime = 0.25f;
	waitTime = 0.25f;
	shortWaitTime = 0.02f;

	fadeTime = 0.01f;
	stopTime = 0.15f;

	totalTime = 18.0f * fadeTime + 9.0f * initWaitTime;

	
	//-------------------------------------------
	// Hand
	{
		CCFadeTo *fadeIn	= CCFadeTo::actionWithDuration( fadeTime, 255 );
		CCFadeTo *fadeOut	= CCFadeTo::actionWithDuration( fadeTime, 0 );

		CCMoveBy *initWait	= CCMoveBy::actionWithDuration( initWaitTime, CCPointZero );
		CCMoveBy *wait		= CCMoveBy::actionWithDuration( waitTime, CCPointZero );
		CCMoveBy *shortWait	= CCMoveBy::actionWithDuration( shortWaitTime, CCPointZero );

		CCFiniteTimeAction *animAction = CCSequence::actions( 
			initWait, shortWait, fadeOut, 
			wait, fadeIn, shortWait, 
			wait, shortWait, fadeOut, 
			wait, fadeIn, shortWait, 
			wait, shortWait, fadeOut,
			wait, fadeIn, shortWait, 
			wait, 
			NULL );

		hand->setPosition( ccp( _circlePosition.x + _sHandDeltaX, _circlePosition.y + _sHandDeltaY ));
		RunAnim2( hand, 255, totalTime, eAnimCallback_FadeUnlock, animAction );
	}

	//-------------------------------------------
	// Hand Tap
	{
		CCFadeTo *fadeIn = CCFadeTo::actionWithDuration( fadeTime, 255 );
		CCFadeTo *fadeOut = CCFadeTo::actionWithDuration( fadeTime, 0 );

		CCMoveBy *initWait	= CCMoveBy::actionWithDuration( initWaitTime, CCPointZero );
		CCMoveBy *wait		= CCMoveBy::actionWithDuration( waitTime, CCPointZero );
		CCMoveBy *shortWait	= CCMoveBy::actionWithDuration( shortWaitTime, CCPointZero );

		CCFiniteTimeAction *animAction = CCSequence::actions( 
			initWait, fadeIn, shortWait, 
			wait, shortWait, fadeOut, 
			wait, fadeIn,  shortWait, 
			wait, shortWait, fadeOut, 
			wait, fadeIn,  shortWait, 
			wait, shortWait, fadeOut, 
			wait, 
			NULL );

		handTap->setPosition( ccp( _circlePosition.x + _sHandDeltaX, _circlePosition.y + _sHandDeltaY ));
		RunAnim2( handTap, 0, totalTime, eAnimCallback_FadeUnlock, animAction );

	}
}
//--------------------------------------------------------------------------------------





//--------------------------------------------------------------------------------------
// Level Message Pointing Circle
//--------------------------------------------------------------------------------------
LevelMsg_MovingPointingCircle::LevelMsg_MovingPointingCircle( 
	int time, 
	float duration, 
	const char *text, 
	const b2Vec2 &textPosition, 
	const b2Vec2 &circlePositionStart, 
	const b2Vec2 &circleMoveBy, GameTypes::SpriteLayers zOrder ) : 
//--------------------------------------------------------------------------------------
LevelMsg( LevelMsg::eMsg_MovingPointingCircle, time, duration, text, textPosition, zOrder )
{
	_circlePositionStart.x = circlePositionStart.x * RATIO - Config::GameSize.width / 2.0f;
	_circlePositionStart.y = circlePositionStart.y * RATIO- Config::GameSize.height / 2.0f;

	_circleMoveBy.x = circleMoveBy.x * RATIO;
	_circleMoveBy.y = circleMoveBy.y * RATIO;
}
//--------------------------------------------------------------------------------------
void LevelMsg_MovingPointingCircle::DisplayMsg()
{
	//-------------------------
	// Sprite Out
	//-------------------------
	CCSprite *hand = CCSprite::spriteWithFile( "Images/HandPointer/Hand.png" );
	CCSprite *handTap = CCSprite::spriteWithFile( "Images/HandPointer/HandTap.png" );

	handTap->setOpacity( 0 );

	addChild( hand, 5 );
	addChild( handTap, 10 );

	float moveTime;
	float stopTime;

	float initWaitTime;
	float initPostWaitTime;
	float fadeTime;
	float fadeLongTime;
//	float waitTime;
	float shortWaitTime;
	float totalTime;

	totalTime = 4.25f;

	initWaitTime = 0.25f;
	initPostWaitTime = 0.25f;
	shortWaitTime = 0.02f;
	fadeTime = 0.02f;
	fadeLongTime = 0.5f;

	moveTime = 0.8f;
	stopTime = 0.15f;

	//-------------------------------------------
	// Hand
	{
		CCMoveBy *moveByAction	= CCMoveBy::actionWithDuration( moveTime, ccp( _circleMoveBy.x, _circleMoveBy.y ));
		//CCMoveBy *waitAction	= CCMoveBy::actionWithDuration( stopTime, CCPointZero );
		
		CCFadeTo *fadeIn = CCFadeTo::actionWithDuration( fadeTime, 255 );
		CCFadeTo *fadeOut = CCFadeTo::actionWithDuration( fadeTime, 0 );
		CCFadeTo *fadeOutLong = CCFadeTo::actionWithDuration( fadeLongTime, 0 );

		CCMoveBy *initWait	= CCMoveBy::actionWithDuration( initWaitTime, CCPointZero );
		CCMoveBy *shortWait	= CCMoveBy::actionWithDuration( shortWaitTime, CCPointZero );

		float longWaitTime;
		longWaitTime = initPostWaitTime + 2.0f * moveTime + 3.0f * stopTime - shortWaitTime;

		CCMoveBy *longWait	= CCMoveBy::actionWithDuration( longWaitTime, CCPointZero );

		CCFiniteTimeAction *animAction = CCSequence::actions( 
			initWait, shortWait, fadeOut, moveByAction, longWait, fadeIn, initWait, fadeOutLong,
			NULL );

		hand->setPosition( ccp( _circlePositionStart.x + _sHandDeltaX, _circlePositionStart.y + _sHandDeltaY ));
		RunAnim2( hand, 255, totalTime, eAnimCallback_FadeLock, animAction );
	}
	


	//-------------------------------------------
	// Hand Tap
	{
		CCFadeTo *fadeIn = CCFadeTo::actionWithDuration( fadeTime, 255 );
		CCFadeTo *fadeOut = CCFadeTo::actionWithDuration( fadeTime, 0 );

		CCMoveBy *moveByAction = CCMoveBy::actionWithDuration( moveTime, ccp( _circleMoveBy.x, _circleMoveBy.y ));
		CCMoveBy *waitAction = CCMoveBy::actionWithDuration( stopTime, CCPointZero );
		CCMoveBy *initWait	= CCMoveBy::actionWithDuration( initWaitTime, CCPointZero );
		CCMoveBy *initPostWait = CCMoveBy::actionWithDuration( initPostWaitTime, CCPointZero );
		CCMoveBy *shortWait	= CCMoveBy::actionWithDuration( shortWaitTime, CCPointZero );

		CCFiniteTimeAction *animAction = CCSequence::actions( 
			initWait, fadeIn, shortWait, 
			initPostWait, moveByAction, waitAction, moveByAction->reverse(), waitAction, moveByAction, waitAction, fadeOut, NULL );

		handTap->setPosition( ccp( _circlePositionStart.x + _sHandDeltaX, _circlePositionStart.y + _sHandDeltaY ));
		RunAnim2( handTap, 0, totalTime, eAnimCallback_FadeLock, animAction );
	}
	
}



//--------------------------------------------------------------------------------------
// Level Message Reel Intro
//--------------------------------------------------------------------------------------
LevelMsg_ReelIntro::LevelMsg_ReelIntro(
	int time, 
	float duration, 
	const char *text, 
	const b2Vec2 &textPosition, 
	const b2Vec2 &circlePositionStart, 
	const b2Vec2 &circleMoveBy, GameTypes::SpriteLayers zOrder ) : 
//--------------------------------------------------------------------------------------
LevelMsg( LevelMsg::eMsg_MovingPointingCircle, time, duration, text, textPosition, zOrder )
{
	_circlePositionStart.x = circlePositionStart.x * RATIO - Config::GameSize.width / 2.0f;
	_circlePositionStart.y = circlePositionStart.y * RATIO- Config::GameSize.height / 2.0f;

	_circleMoveBy.x = circleMoveBy.x * RATIO;
	_circleMoveBy.y = circleMoveBy.y * RATIO;
}
//--------------------------------------------------------------------------------------
void LevelMsg_ReelIntro::DisplayMsg()
{
	//-------------------------
	// Sprite Out
	//-------------------------
	CCSprite *hand = CCSprite::spriteWithFile( "Images/HandPointer/Hand.png" );
	CCSprite *handTap = CCSprite::spriteWithFile( "Images/HandPointer/HandTap.png" );

	handTap->setOpacity( 0 );

	addChild( hand, 5 );
	addChild( handTap, 10 );
    
    //--------------------
	float initWaitTime;
    float shortWaitTime;
	float totalTime;
    float fadeDur;
    
	totalTime = 5.5f;
	initWaitTime = 0.25f;
    shortWaitTime = 0.15f;
    fadeDur = 0.25f;

    CCPoint center;
    center.x = 377.5f;
    center.y = -87.5f;

	//-------------------------------------------
	// Hand
	{
		CCFiniteTimeAction *animAction;
        animAction = CCSequence::actions(
                                         CCDelayTime::actionWithDuration(initWaitTime),
                                         CCFadeTo::actionWithDuration(fadeDur, 0 ),
                                         CCDelayTime::actionWithDuration(shortWaitTime),
                                         NULL );

		hand->setPosition( ccp( _circlePositionStart.x + _sHandDeltaX, _circlePositionStart.y + _sHandDeltaY ));
		RunAnim2( hand, 255, totalTime, eAnimCallback_FadeLock, animAction );
	}
	

	//-------------------------------------------
	// Hand Tap
	{
		CCFiniteTimeAction *animAction;
        animAction = CCSequence::actions(
                                         CCDelayTime::actionWithDuration(initWaitTime),
                                         CCFadeTo::actionWithDuration(fadeDur, 255 ),
                                         CCDelayTime::actionWithDuration(shortWaitTime),
                                         CCMoveByCircle::actionWithDuration(1.2f, center, 0.75f ),
                                         CCDelayTime::actionWithDuration(0.9f),
                                         CCMoveByCircle::actionWithDuration(0.25f, center, -1.00f ),
                                         CCMoveByCircle::actionWithDuration(0.25f, center, -2.00f ),
                                         CCMoveByCircle::actionWithDuration(0.25f, center, -3.00f ),
                                         CCMoveByCircle::actionWithDuration(0.25f, center, -4.00f ),
                                         CCMoveByCircle::actionWithDuration(0.50f, center, -5.00f ),
                                         CCMoveByCircle::actionWithDuration(0.50f, center, -6.00f ),
                                         CCMoveByCircle::actionWithDuration(0.50f, center, -8.00f ),
                                         CCMoveByCircle::actionWithDuration(2.50f, center, -12.00f ),
                                         NULL );

		handTap->setPosition( ccp( _circlePositionStart.x + _sHandDeltaX, _circlePositionStart.y + _sHandDeltaY ));
		RunAnim2( handTap, 0, totalTime, eAnimCallback_FadeLock, animAction );
	}
	
}



//--------------------------------------------------------------------------------------
// Level Message Skip Level Intro
//--------------------------------------------------------------------------------------
LevelMsg_SkipLevelIntro::LevelMsg_SkipLevelIntro( int time ) :
//--------------------------------------------------------------------------------------
LevelMsg( LevelMsg::eMsg_MovingPointingCircle, time, 2.0f, "", b2Vec2(0.0f, 0.0f ), GameTypes::eSpritesSkipLevelIntro )
{
}
//--------------------------------------------------------------------------------------
void LevelMsg_SkipLevelIntro::DisplayMsg()
{
	//-------------------------
	// Sprite Out
	//-------------------------
	CCSprite *hand = CCSprite::spriteWithFile( "Images/HandPointer/Hand.png" );
	CCSprite *handTap = CCSprite::spriteWithFile( "Images/HandPointer/HandTap.png" );

	handTap->setOpacity( 0 );
    
    hand->setFlipX(true);
    handTap->setFlipX(true);

	addChild( hand, 5 );
	addChild( handTap, 10 );

	float initWaitTime;
	float fadeTime;
	float shortWaitTime;
//	float totalTime;
//
//	totalTime = 4.25f;

	initWaitTime = 0.25f;
	shortWaitTime = 0.2f;
	fadeTime = 0.02f;

    
	//-------------------------------------------
	// Hand
	{
		CCFiniteTimeAction *animAction;
        animAction = CCSequence::actions(
                                         CCDelayTime::actionWithDuration( initWaitTime + shortWaitTime ),
                                         CCMoveBy::actionWithDuration( 0.60f, ccp( 110.0f, 182.0f )),
                                         CCDelayTime::actionWithDuration( shortWaitTime ),
                                         CCFadeTo::actionWithDuration( fadeTime, 0 ), // finger tap
                                         CCDelayTime::actionWithDuration( 2.0f * shortWaitTime ),
                                         CCCallFunc::actionWithTarget(this, callfunc_selector( LevelMsg_SkipLevelIntro::ShowLevelSubMenu )),
                                         CCFadeTo::actionWithDuration( fadeTime, 255 ),
                                         CCDelayTime::actionWithDuration( 0.5f ),
                                         CCMoveBy::actionWithDuration( 0.80f, (ccp( -275.0f, -355.0f ))),
                                         CCDelayTime::actionWithDuration( shortWaitTime ),
                                         CCFadeTo::actionWithDuration( fadeTime, 0 ), // finger tap
                                         CCDelayTime::actionWithDuration( 1.0f ),
                                         CCFadeTo::actionWithDuration( fadeTime, 255 ),
                                         CCDelayTime::actionWithDuration( shortWaitTime ),
                                         CCCallFunc::actionWithTarget(this, callfunc_selector( LevelMsg_SkipLevelIntro::CleanUp )),
                                         NULL );

        hand->setPosition( ccp( 325.0f -_sHandDeltaX, _sHandDeltaY + 100 ));
		RunAnim2( hand, 255, 10.0f, eAnimCallback_FadeLock, animAction );
	}
	


   	//-------------------------------------------
	// Hand Tap
    {
		CCFiniteTimeAction *animAction;
        animAction = CCSequence::actions(
                                         CCDelayTime::actionWithDuration( initWaitTime + shortWaitTime + 0.60f + shortWaitTime ),
                                         CCFadeTo::actionWithDuration( fadeTime, 255 ), // finger tap
                                         CCDelayTime::actionWithDuration( 2.0f * shortWaitTime ),
                                         CCDelayTime::actionWithDuration( 0.001f ),
//                                         CCCallFunc::actionWithTarget(this, callfunc_selector( LevelMsg_SkipLevelIntro::ShowLevelSubMenu )),
                                         CCFadeTo::actionWithDuration( fadeTime, 0 ),
                                         CCDelayTime::actionWithDuration( 0.5f ),
                                         CCMoveBy::actionWithDuration( 0.80f, (ccp( -275.0f, -355.0f ))),
                                         CCDelayTime::actionWithDuration( shortWaitTime ),
                                         CCFadeTo::actionWithDuration( fadeTime, 255 ), // finger tap
                                         CCDelayTime::actionWithDuration( 1.0f ),
                                         CCFadeTo::actionWithDuration( fadeTime, 0 ),
                                         CCDelayTime::actionWithDuration( 2.0f ),
                                         NULL );

        handTap->setPosition( ccp( 325.0f + 110.0f -_sHandDeltaX, 100.0f + 182.0f + _sHandDeltaY ));
		RunAnim2( handTap, 0, 10.0f, eAnimCallback_FadeLock, animAction );
	}
}
//--------------------------------------------------------------------------------------
void LevelMsg_SkipLevelIntro::ShowLevelSubMenu()
{
    Game::Get()->GetRuningLevel()->ShowSubMenu(true);
}
//--------------------------------------------------------------------------------------
void LevelMsg_SkipLevelIntro::CleanUp()
{
    Game::Get()->GetRuningLevel()->SkipLevelIntroFinished();
}



//--------------------------------------------------------------------------------------
// Level Message Skip Level Intro Level Failed
//--------------------------------------------------------------------------------------
LevelMsg_SkipLevelIntro_LevelFailed::LevelMsg_SkipLevelIntro_LevelFailed( int time ) :
//--------------------------------------------------------------------------------------
LevelMsg( LevelMsg::eMsg_MovingPointingCircle, time, 2.0f, "", b2Vec2(0.0f, 0.0f ), GameTypes::eSpritesSkipLevelIntro )
{
}
//--------------------------------------------------------------------------------------
void LevelMsg_SkipLevelIntro_LevelFailed::DisplayMsg()
{
	//-------------------------
	// Sprite Out
	//-------------------------
	CCSprite *hand = CCSprite::spriteWithFile( "Images/HandPointer/Hand.png" );
	CCSprite *handTap = CCSprite::spriteWithFile( "Images/HandPointer/HandTap.png" );

	handTap->setOpacity( 0 );
    
    hand->setFlipX(true);
    handTap->setFlipX(true);

	addChild( hand, 5 );
	addChild( handTap, 10 );

	float initWaitTime;
	float fadeTime;
	float shortWaitTime;

	initWaitTime = 0.25f;
	shortWaitTime = 0.2f;
	fadeTime = 0.02f;

	//-------------------------------------------
	// Hand
	{
		CCFiniteTimeAction *animAction;
        animAction = CCSequence::actions(
                                         CCDelayTime::actionWithDuration( initWaitTime + shortWaitTime ),
                                         CCFadeTo::actionWithDuration( fadeTime, 0 ), // finger tap
                                         CCDelayTime::actionWithDuration( 1.0f ),
                                         CCFadeTo::actionWithDuration( fadeTime, 255 ),
                                         CCDelayTime::actionWithDuration( shortWaitTime ),
//                                         CCCallFunc::actionWithTarget(this,
//                                                                      callfunc_selector( LevelMsg_SkipLevelIntro_LevelFailed::RestartLevelPre )),
                                         NULL );

        hand->setPosition( ccp( 160.0f -_sHandDeltaX, _sHandDeltaY - 150.0f ));
		RunAnim2( hand, 255, 10.0f, eAnimCallback_FadeLock, animAction );
	}
	


   	//-------------------------------------------
	// Hand Tap
    {
		CCFiniteTimeAction *animAction;
        animAction = CCSequence::actions(
                                         CCDelayTime::actionWithDuration( initWaitTime + shortWaitTime ),
                                         CCFadeTo::actionWithDuration( fadeTime, 255 ), // finger tap
                                         CCDelayTime::actionWithDuration( 1.0f ),
                                         CCFadeTo::actionWithDuration( fadeTime, 0 ),
                                         NULL );

        handTap->setPosition( hand->getPosition() );
        RunAnim2( handTap, 0, 10.0f, eAnimCallback_FadeLock, animAction );
	}
}
//--------------------------------------------------------------------------------------
void LevelMsg_SkipLevelIntro_LevelFailed::RestartLevelPre()
{
    runAction(CCSequence::actions(
                                  CCDelayTime::actionWithDuration( 1.0f),
                                  CCCallFunc::actionWithTarget(this, callfunc_selector(LevelMsg_SkipLevelIntro_LevelFailed::RestartLevel)),
                                  NULL
                                  ));
}
//--------------------------------------------------------------------------------------
void LevelMsg_SkipLevelIntro_LevelFailed::RestartLevel()
{
//    Game::Get()->RestartLevel();
}




//--------------------------------------------------------------------------------------
// Level Message Arrow Msg
//--------------------------------------------------------------------------------------
LevelMsg_ArrowMsg::LevelMsg_ArrowMsg( int time, const char *text, float textSize,
                                     const CCPoint &pos, unsigned int repeat,
                                     bool flip, float angle ) :
//--------------------------------------------------------------------------------------
LevelMsg( LevelMsg::eMsg_MovingPointingCircle, time, 2.0f, "", b2Vec2(0.0f, 0.0f ), GameTypes::eLevelMsg )
{
    _text = text;
    _textSize = textSize;
    _startPos = pos;
    _flip = flip;
    _repeatCount = repeat;
    _angle = angle;
}
//--------------------------------------------------------------------------------------
void LevelMsg_ArrowMsg::DisplayMsg()
{
	//-------------------------
	// Sprite Out
	//-------------------------
	CCSprite *arrow;
    CCLabelTTF *label;

    label = CCLabelTTF::labelWithString( _text.c_str(), Config::FBHighScoreFont, _textSize );
    label->setColor( Config::FBMenuItemColor );

    CCPoint pos;
    
    if ( _flip )
    {
        pos.x = 150.5f;
        pos.y = 76.0f;
    }
    else
    {
        pos.x = 235.5f;
        pos.y = 76.0f;
    }
    
    D_POINT(pos)
    label->setPosition(pos);
    
    arrow = CCSprite::spriteWithFile( "Images/Other/ArrowLeft.png" );
    arrow->addChild(label);
    arrow->setFlipX(_flip);
    arrow->setRotation(_angle);
	addChild( arrow, 5 );

    //----------------------
    // Bouncing arrow action
    float scale;
    float scaleDur;
    
    scale = 0.05f;
    scaleDur = 0.7f;

    CCActionInterval *repeat;
    repeat = (CCActionInterval*)CCSequence::actions(
                                 CCScaleTo::actionWithDuration( scaleDur, 1 + scale ),
                                 CCScaleTo::actionWithDuration( scaleDur, 1 - scale ),
                                 NULL
                                 );
    repeat = CCRepeat::actionWithAction(repeat, _repeatCount);
    
    arrow->setPosition(_startPos);
	RunAnim2( arrow, 255, 10.0f, eAnimCallback_FadeLock, repeat );
}
//--------------------------------------------------------------------------------------

