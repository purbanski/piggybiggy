#ifndef __PARTICLEENGINE_H__
#define __PARTICLEENGINE_H__
//-------------------------------------------------------------------
#include "cocos2d.h"
#include <Box2D/Box2D.h>
#include <vector>
#include "Blocks/puBlock.h"
//-------------------------------------------------------------------
USING_NS_CC;
using namespace std;
//-------------------------------------------------------------------
class Level;
class ParticleEngine
{
public:
	static void PreloadGraphic();
    
    static CCParticleSystemQuad* Effect_CoinOnScore();
    
	ParticleEngine();
	~ParticleEngine();

	void PauseEngine();
	void ResumeEngine();

	void Update();
	void Effect_Stars( CCNode *level, b2Vec2 pos );
	void Effect_CoinCollected( Level *level, puBlock *block );

	void Effect_Burn( CCNode *level, b2Vec2 pos );
	void Effect_ChainCut( CCNode *level, b2Vec2 pos );
	void Effect_Smoke( CCNode *level, b2Vec2 pos );
	void Effect_Fireworks( CCNode *level, b2Vec2 pos );
	void Effect_LevelFinished( CCNode *level, b2Vec2 pos );
	void Effect_NewClue( CCNode *level, b2Vec2 pos );
	void Effect_PocketOpen( CCNode *level, b2Vec2 pos );
	void Effect_BombExloded( CCNode *level, b2Vec2 pos, float range );
	void Effect_Snow( CCNode *node );
	void Effect_Rain( CCNode *node );
	void Effect_FragileDestroyed( CCNode *node, b2Vec2 pos );
	void Effect_FragileDestroyed( CCNode *node, b2Vec2 pos, const char *filename );
	
	CCNode* Effect_Boobles();
	void Effect_HotSun( CCNode *node );

private:
	struct  EffectRecord
	{
		CCParticleSystem* node;
		int tick;

		EffectRecord( CCParticleSystem* node_, int tick_ ) : node(node_), tick( tick_ )
		{}
	} ;

	typedef vector<EffectRecord> EffectsList;
	EffectsList					_effectsList;
};
//-------------------------------------------------------------------
#endif
