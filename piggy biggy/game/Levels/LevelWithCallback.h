#ifndef __LEVELWITHCALLBACK_H__
#define __LEVELWITHCALLBACK_H__

#include "Level.h"

#include <list>

using namespace std;
//----------------------------------------------------
class LevelActivable : public Level
{
public:
	LevelActivable( GameTypes::LevelEnum levelEnum, bool viewMode );
	virtual void Step( cocos2d::ccTime dt );

protected:
	void ConstFinal();

protected:
	struct Sleeper
	{
		Sleeper( puBlock *block, int time )
		{
			_block = block;
			_block->GetBody()->SetActive( false );
			_block->GetBody()->SetAwake( false );
			_block->GetSprite()->setIsVisible( false );
			_block->SetBodyType( b2_staticBody );

			_time = time;
		}

		Sleeper()
		{
			_block = NULL;
			_time = 0;
		}
		puBlock *_block;
		int		_time;
	};

	typedef list<Sleeper> Sleepers;

	Sleepers		_sleepers;

private:
	int				_stepCounter;
};
//----------------------------------------------------
class LevelWithCallback : public Level
{
public:
	LevelWithCallback( GameTypes::LevelEnum levelEnum, bool viewMode, int stepTicks = 1 );
	virtual ~LevelWithCallback(){};

	virtual void Step( cocos2d::ccTime dt );

protected:
	virtual void CounterCallback() = 0;

	int				_stepCounter;
	int				_stepMax;
};
//----------------------------------------------------
#endif
