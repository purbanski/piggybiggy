#ifndef __LEVELADAMS_H__
#define __LEVELADAMS_H__

#include <Box2D/Box2D.h>
#include "Levels/Level.h"
#include "Blocks/AllBlocks.h"

//------------------------------------------------------------------
class LevelAdamS_PlainLogic : public Level
{
public:
	LevelAdamS_PlainLogic( GameTypes::LevelEnum levelEnum, bool viewMode );
	~LevelAdamS_PlainLogic();

private:
	void CreateJoints();
	void DestroyJoints();

	puBomb< 44 >	_bomb1;
	puBomb< 44 >	_bomb2;
	puCoin< 40 >	_coin1;
	puCoin< 40 >	_coin2;
	puFragileBoxStatic<192,96,eBlockScaled>	_fragileBoxStatic3;
	puFragileBoxStatic<96,96,eBlockScaled>	_fragileBoxStatic1;
	puFragileBoxStatic<96,96,eBlockScaled>	_fragileBoxStatic2;
	puPiggyBank< 60 >	_piggyBank1;
	puScrew< 11 >	_screw1;
	puScrew< 11 >	_screw2;
	puWallBox<120,20,eBlockScaled>	_wallBox8;
	puWallBox<20,20,eBlockScaled>	_wallBox1;
	puWallBox<20,20,eBlockScaled>	_wallBox2;
	puWallBox<230,20,eBlockScaled>	_wallBox11;
	puWallBox<250,20,eBlockScaled>	_wallBox9;
	puWallBox<50,20,eBlockScaled>	_wallBox3;
	puWallBox<50,20,eBlockScaled>	_wallBox4;
	puWallBox<50,20,eBlockScaled>	_wallBox5;
	puWallBox<50,20,eBlockScaled>	_wallBox6;
	puWallBox<50,20,eBlockScaled>	_wallBox7;
	puWallBox<550,20,eBlockScaled>	_wallBox10;
	puWood2Box<160,20,eBlockScaled>	_woodBox1;
	puWood2Box<160,20,eBlockScaled>	_woodBox2;
	puWoodBox<220,30,eBlockScaled>	_woodBox3;
	puWoodBox<340,30,eBlockScaled>	_woodBox4;

	b2RevoluteJoint	*_jointRev1;
	b2RevoluteJoint	*_jointRev2;
	b2GearJoint	*_jointGear1;
};
//------------------------------------------------------------------

#endif
