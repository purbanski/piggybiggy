#ifndef __LEVELDOMINO_H__
#define __LEVELDOMINO_H__

#include <Box2D/Box2D.h>
#include "Levels/Level.h"
#include "Blocks/AllBlocks.h"

class LevelDomino_1 : public Level
{
public:
	LevelDomino_1(  GameTypes::LevelEnum levelEnum, bool viewMode );

private:
	void CreateJoints();

	puCoin< 40 >	_coin1;
	puFragileBox<72,72,eBlockScaled>	_fragileBox10;
	puFragileBox<72,72,eBlockScaled>	_fragileBox11;
	puFragileBox<72,72,eBlockScaled>	_fragileBox1;
	puFragileBox<72,72,eBlockScaled>	_fragileBox2;
	puFragileBox<72,72,eBlockScaled>	_fragileBox3;
	puFragileBox<72,72,eBlockScaled>	_fragileBox4;
	puFragileBox<72,72,eBlockScaled>	_fragileBox5;
	puFragileBox<72,72,eBlockScaled>	_fragileBox6;
	puFragileBox<72,72,eBlockScaled>	_fragileBox7;
	puFragileBox<72,72,eBlockScaled>	_fragileBox8;
	puFragileBox<72,72,eBlockScaled>	_fragileBox9;
	puFragileBoxStatic<72,72,eBlockScaled>	_fragileBoxStatic1;
	puLScrewed<100,20,eBlockScaled>		_lScrewed1;
	puLiftPlatform<125,20,eBlockScaled>	_liftPlatform1;
	puPiggyBank< 44 >				_piggyBank1;
	puScrew< 10 >					_screw1;
	puThiefStatic< 44 >				_thiefStatic1;
	puWallBox<120,20,eBlockScaled>	_wallBox3;
	puWallBox<120,20,eBlockScaled>	_wallBox4;
	puWallBox<160,20,eBlockScaled>	_wallBox6;
	puWallBox<200,20,eBlockScaled>	_wallBox2;
	puWallBox<250,20,eBlockScaled>	_wallBox5;
	puWallBox<730,20,eBlockScaled>	_wallBox1;
	puWoodBox<140,20,eBlockScaled>	_woodBox12;
	puWoodBox<48,10,eBlockScaled>	_woodBox10;
	puWoodBox<48,10,eBlockScaled>	_woodBox11;
	puWoodBox<48,10,eBlockScaled>	_woodBox13;
	puWoodBox<48,10,eBlockScaled>	_woodBox14;
	puWoodBox<48,10,eBlockScaled>	_woodBox15;
	puWoodBox<48,10,eBlockScaled>	_woodBox16;
	puWoodBox<48,10,eBlockScaled>	_woodBox17;
	puWoodBox<48,10,eBlockScaled>	_woodBox18;
	puWoodBox<48,10,eBlockScaled>	_woodBox19;
	puWoodBox<48,10,eBlockScaled>	_woodBox1;
	puWoodBox<48,10,eBlockScaled>	_woodBox20;
	puWoodBox<48,10,eBlockScaled>	_woodBox21;
	puWoodBox<48,10,eBlockScaled>	_woodBox22;
	puWoodBox<48,10,eBlockScaled>	_woodBox23;
	puWoodBox<48,10,eBlockScaled>	_woodBox24;
	puWoodBox<48,10,eBlockScaled>	_woodBox25;
	puWoodBox<48,10,eBlockScaled>	_woodBox26;
	puWoodBox<48,10,eBlockScaled>	_woodBox27;
	puWoodBox<48,10,eBlockScaled>	_woodBox28;
	puWoodBox<48,10,eBlockScaled>	_woodBox29;
	puWoodBox<48,10,eBlockScaled>	_woodBox2;
	puWoodBox<48,10,eBlockScaled>	_woodBox30;
	puWoodBox<48,10,eBlockScaled>	_woodBox31;
	puWoodBox<48,10,eBlockScaled>	_woodBox32;
	puWoodBox<48,10,eBlockScaled>	_woodBox33;
	puWoodBox<48,10,eBlockScaled>	_woodBox34;
	puWoodBox<48,10,eBlockScaled>	_woodBox35;
	puWoodBox<48,10,eBlockScaled>	_woodBox36;
	puWoodBox<48,10,eBlockScaled>	_woodBox37;
	puWoodBox<48,10,eBlockScaled>	_woodBox38;
	puWoodBox<48,10,eBlockScaled>	_woodBox39;
	puWoodBox<48,10,eBlockScaled>	_woodBox3;
	puWoodBox<48,10,eBlockScaled>	_woodBox40;
	puWoodBox<48,10,eBlockScaled>	_woodBox41;
	puWoodBox<48,10,eBlockScaled>	_woodBox42;
	puWoodBox<48,10,eBlockScaled>	_woodBox43;
	puWoodBox<48,10,eBlockScaled>	_woodBox4;
	puWoodBox<48,10,eBlockScaled>	_woodBox5;
	puWoodBox<48,10,eBlockScaled>	_woodBox6;
	puWoodBox<48,10,eBlockScaled>	_woodBox7;
	puWoodBox<48,10,eBlockScaled>	_woodBox8;
	puWoodBox<48,10,eBlockScaled>	_woodBox9;


	b2PrismaticJoint	*_jointPris1;
	b2RevoluteJoint		*_jointRev1;
};

#endif
