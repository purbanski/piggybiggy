#ifndef __LEVELLETSSWING_H__
#define __LEVELLETSSWING_H__

#include <Box2D/Box2D.h>
#include "Levels/Level.h"
#include "Blocks/AllBlocks.h"

//----------------------------------------------------------------------------------------
class LevelLetsSwing_1 : public Level
{
public:
	LevelLetsSwing_1( GameTypes::LevelEnum levelEnum, bool viewMode );

private:
	puCoin< 40 >	_coin1;
	puCoin< 40 >	_coin2;
	puFragileBox<96,96,eBlockScaled>	_fragileBox1;
	puFragileBoxStatic<80,70,eBlockScaled>	_fragileBoxStatic1;
	puFragileBoxStatic<80,70,eBlockScaled>	_fragileBoxStatic2;
	puPiggyBank< 50 >	_piggyBank1;
	puSwingNoFloor	_swingNoFloor1;
	puThiefStatic< 50 >	_thiefStatic1;
	puWallBox<330,20,eBlockScaled>	_wallBox1;
	puWallBox<50,20,eBlockScaled>	_wallBox10;
	puWallBox<50,20,eBlockScaled>	_wallBox11;
	puWallBox<50,20,eBlockScaled>	_wallBox12;
	puWallBox<50,20,eBlockScaled>	_wallBox13;
	puWallBox<50,20,eBlockScaled>	_wallBox14;
	puWallBox<50,20,eBlockScaled>	_wallBox15;
	puWallBox<50,20,eBlockScaled>	_wallBox16;
	puWallBox<50,20,eBlockScaled>	_wallBox17;
	puWallBox<50,20,eBlockScaled>	_wallBox2;
	puWallBox<50,20,eBlockScaled>	_wallBox3;
	puWallBox<50,20,eBlockScaled>	_wallBox4;
	puWallBox<50,20,eBlockScaled>	_wallBox5;
	puWallBox<50,20,eBlockScaled>	_wallBox6;
	puWallBox<50,20,eBlockScaled>	_wallBox7;
	puWallBox<50,20,eBlockScaled>	_wallBox8;
	puWallBox<50,20,eBlockScaled>	_wallBox9;
	puWallBox<540,20,eBlockScaled>	_wallBox18;
};

//----------------------------------------------------------------------------------------
class LevelLetsSwing_2 : public Level
{
public:
	LevelLetsSwing_2( GameTypes::LevelEnum levelEnum, bool viewMode );

private:
	
	puBoxerGlove	_boxerGlove1;
	puBoxerGlove	_boxerGlove2;
	puBoxerGlove	_boxerGlove3;
	puBoxerGlove	_boxerGlove4;
	puCoin< 40 >	_coin1;
	puCoin< 40 >	_coin2;
	puCop< 50 >	_cop1;
	puFragileBox<192,96,eBlockScaled>	_fragileBox1;
	puFragileBox<96,96,eBlockScaled>	_fragileBox2;
	puFragileBox<96,96,eBlockScaled>	_fragileBox3;
	puPiggyBankStatic< 50 >	_piggyBankStatic1;
	puThief< 50 >	_thief1;
	puWallBox<120,20,eBlockScaled>	_wallBox1;
	puWallBox<120,20,eBlockScaled>	_wallBox2;
	puWallBox<120,20,eBlockScaled>	_wallBox3;
	puWallBox<120,20,eBlockScaled>	_wallBox4;
	puWallBox<600,20,eBlockScaled>	_wallBox5;
};
//----------------------------------------------------------------------------------------
class LevelLetsSwing_3 : public Level
{
public:
	LevelLetsSwing_3( GameTypes::LevelEnum levelEnum, bool viewMode );
	~LevelLetsSwing_3();

private:
	void CreateJoints();

private:
	puCoin< 40 >	_coin1;
	puCoin< 40 >	_coin2;
	puCoin< 40 >	_coin3;
	puFragileBox<72,72,eBlockScaled>	_fragileBox1;
	puFragileBox<72,72,eBlockScaled>	_fragileBox2;
	puFragileBox<72,72,eBlockScaled>	_fragileBox3;
	puFragileBox<96,96,eBlockScaled>	_fragileBox4;
	puFragileBox<96,96,eBlockScaled>	_fragileBox5;
	puFragileBoxStatic<96,96,eBlockScaled>	_fragileBoxStatic1;
	puPiggyBank< 44 >	_piggyBank1;
	puScrew< 10 >	_screw1;
	puScrew< 10 >	_screw2;
	puScrew< 10 >	_screw3;
	puSwingNoFloor	_swingNoFloor1;
	puSwingNoFloor	_swingNoFloor2;
	puWallBox<330,20,eBlockScaled>	_wallBox1;
	puWallBox<50,20,eBlockScaled>	_wallBox10;
	puWallBox<50,20,eBlockScaled>	_wallBox11;
	puWallBox<50,20,eBlockScaled>	_wallBox12;
	puWallBox<50,20,eBlockScaled>	_wallBox13;
	puWallBox<50,20,eBlockScaled>	_wallBox2;
	puWallBox<50,20,eBlockScaled>	_wallBox3;
	puWallBox<50,20,eBlockScaled>	_wallBox4;
	puWallBox<50,20,eBlockScaled>	_wallBox5;
	puWallBox<50,20,eBlockScaled>	_wallBox6;
	puWallBox<50,20,eBlockScaled>	_wallBox7;
	puWallBox<50,20,eBlockScaled>	_wallBox8;
	puWallBox<50,20,eBlockScaled>	_wallBox9;
	puWallBox<550,20,eBlockScaled>	_wallBox14;
	puWood2Box<120,20,eBlockScaled>	_woodBox1;
	puWood2Box<160,20,eBlockScaled>	_woodBox2;
	puWood2Box<160,20,eBlockScaled>	_woodBox3;

	b2RevoluteJoint	*_jointRev1;
	b2RevoluteJoint	*_jointRev2;
	b2RevoluteJoint	*_jointRev3;
	b2GearJoint	*_jointGear1;
	b2GearJoint	*_jointGear2;
};
//----------------------------------------------------------------------------------------
#endif
