#include "LevelLetsShoot.h"

//--------------------------------------------------------------------------
LevelLetsShoot_1::LevelLetsShoot_1( GameTypes::LevelEnum levelEnum, bool viewMode ) : LevelActivable( levelEnum, viewMode )
{
    _levelTag = eLevelTag_Spitter;

	// _coin1
	_coin1.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin1.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin1.SetBodyType( b2_dynamicBody );

	// _piggyBank1
	_piggyBank1.GetBody()->GetFixtureList()->SetDensity( 7.5000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_piggyBank1.GetBody()->SetAngularDamping(0.5000f );
	_piggyBank1.GetBody()->SetLinearDamping(0.5000f );
	_piggyBank1.FlipX();
	_piggyBank1.SetBodyType( b2_dynamicBody );

	// _wallBox1
	_wallBox1.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox1.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox1.SetBodyType( b2_staticBody );

	// _wallBox2
	_wallBox2.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox2.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox2.SetBodyType( b2_staticBody );

	// _gun1
	_gun1.GetBody()->GetFixtureList()->SetDensity( 7.5000f );
	_gun1.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_gun1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	//_gun1.FlipX();
	_gun1.SetBodyType( b2_dynamicBody );
	_gun1.SetBulletCount( 1 );


	//Set blocks positions
	_coin1.SetPosition( 48.0000f, 68.0000f );
	_piggyBank1.SetPosition( 13.5000f, 13.8000f );
	_wallBox1.SetPosition( 24.0000f, 6.0000f );
	_wallBox2.SetPosition( 82.2000f, 21.2500f );
	_gun1.SetPosition( 82.5000f, 28.5500f );

	_sleepers.push_back( Sleeper( &_coin1, 50 ));

	ConstFinal();
	SetIterations( Config::WorldGunVelocityIterations, Config::WorldGunPositionIterations );
}
//--------------------------------------------------------------------------
LevelLetsShoot_2::LevelLetsShoot_2( GameTypes::LevelEnum levelEnum, bool viewMode ) : Level( levelEnum, viewMode )
{
    _levelTag = eLevelTag_Spitter;
	_coinCount = 3 ;

	// _bouncerBox1
	_bouncerBox1.GetBody()->GetFixtureList()->SetDensity( 0.3000f );
	_bouncerBox1.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_bouncerBox1.GetBody()->GetFixtureList()->SetRestitution( 1.0000f );
	_bouncerBox1.SetBodyType( b2_staticBody );

	// _coin1
	_coin1.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin1.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_coin1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_coin1.SetBodyType( b2_dynamicBody );

	// _coin2
	_coin2.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin2.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_coin2.SetBodyType( b2_dynamicBody );

	// _coin3
	_coin3.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin3.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_coin3.SetBodyType( b2_dynamicBody );

	// _gun1
	_gun1.GetBody()->GetFixtureList()->SetDensity( 7.5000f );
	_gun1.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_gun1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_gun1.SetBodyType( b2_dynamicBody );

	// _piggyBankStatic1
	_piggyBankStatic1.GetBody()->GetFixtureList()->SetDensity( 7.5000f );
	_piggyBankStatic1.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_piggyBankStatic1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_piggyBankStatic1.FlipX();
	_piggyBankStatic1.SetBodyType( b2_staticBody );

	// _wallBox1
	_wallBox1.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox1.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox1.SetBodyType( b2_staticBody );

	// _wallBox2
	_wallBox2.SetRotation( 90.0000f );
	_wallBox2.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox2.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox2.SetBodyType( b2_staticBody );


	//Set blocks positions
	_bouncerBox1.SetPosition( 51.9500f, 5.8000f );
	_coin1.SetPosition( 52.0000f, 45.8500f );
	_coin2.SetPosition( 36.5000f, 52.2500f );
	_coin3.SetPosition( 67.4002f, 57.8500f );
	_gun1.SetPosition( 84.6000f, 40.3000f );
	_piggyBankStatic1.SetPosition( 20.6002f, 12.1000f );
	_wallBox1.SetPosition( 84.3000f, 32.0000f );
	_wallBox2.SetPosition( 8.0000f, 32.4000f );

	_gun1.SetBulletCount( 1 );
	_coin1.GetBody()->SetBullet( true );
	_coin2.GetBody()->SetBullet( true );
	_coin3.GetBody()->SetBullet( true );
	
	_border.SetBorderSize( 7.5f );
	ConstFinal();
	SetIterations( Config::WorldGunVelocityIterations, Config::WorldGunPositionIterations );
}
//--------------------------------------------------------------------------
LevelLetsShoot_3::LevelLetsShoot_3( GameTypes::LevelEnum levelEnum, bool viewMode ) : LevelActivable( levelEnum, viewMode )
{
    _levelTag = eLevelTag_Thief;

	// _coin1
	_coin1.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin1.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin1.SetBodyType( b2_dynamicBody );

	// _piggyBank1
	_piggyBank1.GetBody()->GetFixtureList()->SetDensity( 7.5000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_piggyBank1.SetBodyType( b2_dynamicBody );

	// _thief1
	_thief1.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_thief1.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_thief1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_thief1.SetBodyType( b2_staticBody );

	// _thief2
	_thief2.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_thief2.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_thief2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_thief2.SetBodyType( b2_staticBody );

	// _thief3
	_thief3.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_thief3.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_thief3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_thief3.SetBodyType( b2_staticBody );

	// _thief4
	_thief4.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_thief4.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_thief4.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_thief4.SetBodyType( b2_staticBody );

	// _gunExplosive1
	_gunExplosive1.GetBody()->GetFixtureList()->SetDensity( 7.5000f );
	_gunExplosive1.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_gunExplosive1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	//_gunExplosive1.FlipX();
	_gunExplosive1.SetBodyType( b2_dynamicBody );
	_gunExplosive1.SetBulletCount( 4 );

	// _wallBox1
	_wallBox1.SetRotation( 4.5000f );
	_wallBox1.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox1.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox1.SetBodyType( b2_staticBody );

	// _wallBox2
	_wallBox2.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox2.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox2.SetBodyType( b2_staticBody );

	// _wallBox3
	_wallBox3.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox3.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox3.SetBodyType( b2_staticBody );


	//Set blocks positions
	_coin1.SetPosition( 7.2500f, 12.6500f );
	_piggyBank1.SetPosition( 86.5501f, 16.7501f );
	_thief1.SetPosition( 11.8000f, 68.8000f );
	_thief2.SetPosition( 31.8000f, 68.8000f );
	_thief3.SetPosition( 51.8000f, 68.8000f );
	_thief4.SetPosition( 71.9000f, 68.8000f );
	_gunExplosive1.SetPosition( 86.5501f, 32.8001f );
	_wallBox1.SetPosition( 86.5501f, 9.8000f );
	_wallBox2.SetPosition( 86.5501f, 25.6000f );
	_wallBox3.SetPosition( 41.300f, 4.7000f );

	_sleepers.push_back( Sleeper( &_thief4, 130 ));
	_sleepers.push_back( Sleeper( &_thief3, 260 ));
	_sleepers.push_back( Sleeper( &_thief2, 400 ));
	_sleepers.push_back( Sleeper( &_thief1, 485 ));

	ConstFinal();
}
//--------------------------------------------------------------------------
LevelLetsShoot_4::LevelLetsShoot_4( GameTypes::LevelEnum levelEnum, bool viewMode ) : LevelActivable( levelEnum, viewMode )
{
    _levelTag = eLevelTag_Spitter;

	_coinCount = 1 + CoinCount;
	_world.SetGravity( b2Vec2( 0.0f, -6.0f  ));

	// _coin1
	_coin1.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin1.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin1.SetBodyType( b2_staticBody );

	// _piggyBank1
	_piggyBank1.GetBody()->GetFixtureList()->SetDensity( 7.5000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_piggyBank1.SetBodyType( b2_dynamicBody );
	_piggyBank1.FlipX();

	// _gun1
	_gun1.GetBody()->GetFixtureList()->SetDensity( 7.5000f );
	_gun1.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_gun1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_gun1.SetBodyType( b2_dynamicBody );

	// _gunExplosive1
	_gunExplosive1.GetBody()->GetFixtureList()->SetDensity( 7.5000f );
	_gunExplosive1.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_gunExplosive1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_gunExplosive1.SetBodyType( b2_dynamicBody );

	// _wallBox1
	_wallBox1.SetRotation( -30.0000f );
	_wallBox1.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox1.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox1.SetBodyType( b2_staticBody );

	// _wallBox2
	_wallBox2.SetRotation( 30.0000f );
	_wallBox2.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox2.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox2.SetBodyType( b2_staticBody );

	// _wallBox3
	_wallBox3.SetRotation( 60.0000f );
	_wallBox3.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox3.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox3.SetBodyType( b2_staticBody );

	// _wallBox4
	_wallBox4.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox4.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox4.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox4.SetBodyType( b2_staticBody );

	// _wallBox5
	_wallBox5.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox5.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox5.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox5.SetBodyType( b2_staticBody );

	// _wallBox6
	_wallBox6.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox6.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox6.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox6.SetBodyType( b2_staticBody );

	// _wallBox7
	_wallBox7.SetRotation( 60.0000f );
	_wallBox7.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox7.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox7.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox7.SetBodyType( b2_staticBody );

	// _wallBox8
	_wallBox8.SetRotation( 30.0000f );
	_wallBox8.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox8.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox8.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox8.SetBodyType( b2_staticBody );

	// _wallBox9
	_wallBox9.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox9.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox9.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox9.SetBodyType( b2_staticBody );

	// _wallBox10
	_wallBox10.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox10.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox10.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox10.SetBodyType( b2_staticBody );

	// _wallBox11
	_wallBox11.SetRotation( -60.0000f );
	_wallBox11.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox11.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox11.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox11.SetBodyType( b2_staticBody );

	// _wallBox12
	_wallBox12.SetRotation( 90.0000f );
	_wallBox12.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox12.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox12.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox12.SetBodyType( b2_staticBody );


	//Set blocks positions
	_coin1.SetPosition( 48.0000f, 8.5000f );
	_gun1.SetPosition( 83.1000f, 46.7000f );
	_gunExplosive1.SetPosition( 83.1000f, 25.4000f );
	_piggyBank1.SetPosition( 21.2000f, 41.3001f );
	_wallBox1.SetPosition( 13.2000f, 24.5000f );
	_wallBox2.SetPosition( 28.8000f, 24.5000f );
	_wallBox3.SetPosition( 34.4000f, 30.1000f );
	_wallBox4.SetPosition( 48.0000f, 3.9000f );
	_wallBox5.SetPosition( 83.1000f, 39.0999f );
	_wallBox6.SetPosition( 20.6000f, 59.6002f );
	_wallBox7.SetPosition( 7.4000f, 52.0001f );
	_wallBox8.SetPosition( 12.8000f, 57.6002f );
	_wallBox9.SetPosition( 83.1000f, 18.2000f );
	_wallBox10.SetPosition( 21.0000f, 22.5000f );
	_wallBox11.SetPosition( 7.6000f, 30.3000f );
	_wallBox12.SetPosition( 5.4000f, 41.1500f );

	_gun1.SetBulletCount( 32 );
	_gunExplosive1.SetBulletCount( 32 );

	b2Vec2 startPos;
	startPos.Set( 48.0f, 72.0f );

	//-----------------------------------
	// Set Positions
	//-----------------------------------
	Blocks allBlocks;
	for ( int i = 0; i < CoinCount ; i++ )
	{
		allBlocks.push_back( &_coins[i] );
		_coins[i].SetPosition( startPos );

	}

	for ( int i = 0; i < ThiefCount ; i++ )
	{
		allBlocks.push_back( &_thiefs[i] );
		_thiefs[i].SetPosition( startPos );
	}

	_thiefLast.SetPosition( startPos );
	_piggyBank4.SetPosition( startPos );


	//-----------------------------------
	// Set Random order
	//-----------------------------------
	int index;
	puBlock* block;
	Blocks allBlocksRand;
	Blocks::iterator it;

	while ( allBlocks.size() > 0 )
	{	
		index = rand() % allBlocks.size();
		D_LOG("size %d index %d", (int)allBlocks.size(), index );
		block = allBlocks.at( index );
		block->GetBody()->SetBullet( true );
		allBlocksRand.push_back( block );
		allBlocks.erase ( allBlocks.begin() + index );
	}


	int dTimeShort = 60;
	int dTimeLong =  120;
	int startDelay = 60;

	int delta;
	int count = 0;
	int timeTotal = 0;
	puBlock* blockPrev;

	timeTotal += startDelay;
	_sleepers.push_back( Sleeper( *allBlocksRand.begin(), timeTotal ));

	for ( it = allBlocksRand.begin() + 1; it != allBlocksRand.end(); it++ )
	{
		block = *it;
		blockPrev = *(it-1);


		if ( (*it)->GetBlockType() == GameTypes::eCoin &&  (*(it-1))->GetBlockType() == GameTypes::eThief )
			delta = dTimeLong;
		else
			delta = dTimeShort;
		
		timeTotal += delta; 
		//D_LOG( "prev: %s", blockPrev->GetClassName2() );
		//D_LOG( "cur: %s" , block->GetClassName2() );
		//D_LOG( "Total time: %i delta: %i", timeTotal, delta  );
		_sleepers.push_back( Sleeper( (*it), timeTotal ));
		count++;
	}

	timeTotal += dTimeShort;
	_sleepers.push_back( Sleeper( &_thiefLast, timeTotal ));

	timeTotal += dTimeShort;
	_sleepers.push_back( Sleeper( &_piggyBank4, timeTotal ));

	ConstFinal();
	//AdjustOldScreen();

	SetIterations( Config::WorldGunVelocityIterations, Config::WorldGunPositionIterations );
}
//--------------------------------------------------------------------------
