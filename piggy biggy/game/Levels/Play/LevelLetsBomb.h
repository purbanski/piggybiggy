#ifndef __LEVELLETSBOMB_H__
#define __LEVELLETSBOMB_H__

#include <Box2D/Box2D.h>
#include "Levels/Level.h"
#include "Blocks/AllBlocks.h"

//--------------------------------------------------------------------------------------------
class LevelLetsBomb_1 : public Level
{
public:
	LevelLetsBomb_1( GameTypes::LevelEnum levelEnum, bool viewMode );

private:
	puBomb< 44 >	_bomb1;
	puBomb< 44 >	_bomb2;
	puCoin< 40 >	_coin1;
	puCoin< 40 >	_coin2;
	puWallBox<900,20>	_floor1;
	puPiggyBank< 60 >	_piggyBank1;
	puPiggyBank< 60 >	_piggyBank2;
};
//--------------------------------------------------------------------------------------------
class LevelLetsBomb_1b : public Level
{
public:
	LevelLetsBomb_1b( GameTypes::LevelEnum levelEnum, bool viewMode );

private:
	puBomb< 44 >	_bomb1;
	puBomb< 44 >	_bomb2;
	puBomb< 44 >	_bomb3;
	puBomb< 44 >	_bomb4;
	puCoin< 40 >	_coin1;
	puCoin< 40 >	_coin2;
	puPiggyBankStatic< 60 >	_piggyBankStatic1;
	puPiggyBankStatic< 60 >	_piggyBankStatic2;
	puWallBox<700,20,eBlockScaled>	_wallBox1;
};
//--------------------------------------------------------------------------------------------
class LevelLetsBomb_2 : public Level
{
public:
	LevelLetsBomb_2( GameTypes::LevelEnum levelEnum, bool viewMode );

private:
	puBombStatic< 44 >	_bombStatic1;
	puChainCutable	_chain1;
	puChainCutable	_chain2;
	puCoinStatic< 40 >	_coinStatic1;
	puPiggyBank< 60 >	_piggyBank1;
	puWallBox<70,20,eBlockScaled>	_wallBox1;
};
//--------------------------------------------------------------------------------------------
class LevelLetsBomb_3 : public Level
{
public:
	LevelLetsBomb_3( GameTypes::LevelEnum levelEnum, bool viewMode );

private:
	puBomb< 44 >	_bomb1;
	puBomb< 44 >	_bomb2;
	puBomb< 44 >	_bomb3;
	puCoin< 40 >	_coin1;
	puPiggyBank< 60 >	_piggyBank1;
	puWallBox<70,20,eBlockScaled>	_wallBox1;
	puWallBox<740,20,eBlockScaled>	_wallBox2;
};
//--------------------------------------------------------------------------------------------
class LevelLetsBomb_4 : public Level
{
public:
	LevelLetsBomb_4( GameTypes::LevelEnum levelEnum, bool viewMode );

private:
	puBomb< 44 >	_bomb1;
	puBomb< 44 >	_bomb2;
	puBomb< 44 >	_bomb3;
	puChainCutable	_chain1;
	puChainCutable	_chain2;
	puCoin< 40 >	_coin1;
	puPiggyBank< 60 >	_piggyBank1;
	puWallBox<20,20,eBlockScaled>	_wallBox20;
	puWallBox<20,20,eBlockScaled>	_wallBox21;
	puWallBox<200,20,eBlockScaled>	_wallBox10;
	puWallBox<200,20,eBlockScaled>	_wallBox19;
	puWallBox<290,20,eBlockScaled>	_wallBox22;
	puWallBox<290,20,eBlockScaled>	_wallBox23;
	puWallBox<50,20,eBlockScaled>	_wallBox11;
	puWallBox<50,20,eBlockScaled>	_wallBox12;
	puWallBox<50,20,eBlockScaled>	_wallBox13;
	puWallBox<50,20,eBlockScaled>	_wallBox14;
	puWallBox<50,20,eBlockScaled>	_wallBox15;
	puWallBox<50,20,eBlockScaled>	_wallBox16;
	puWallBox<50,20,eBlockScaled>	_wallBox17;
	puWallBox<50,20,eBlockScaled>	_wallBox18;
	puWallBox<50,20,eBlockScaled>	_wallBox2;
	puWallBox<50,20,eBlockScaled>	_wallBox3;
	puWallBox<50,20,eBlockScaled>	_wallBox4;
	puWallBox<50,20,eBlockScaled>	_wallBox5;
	puWallBox<50,20,eBlockScaled>	_wallBox6;
	puWallBox<50,20,eBlockScaled>	_wallBox7;
	puWallBox<50,20,eBlockScaled>	_wallBox8;
	puWallBox<50,20,eBlockScaled>	_wallBox9;
	puWallBox<60,15,eBlockScaled>	_wallBox1;
};
//--------------------------------------------------------------------------------------------
class LevelLetsBomb_5 : public Level
{
public:
	LevelLetsBomb_5( GameTypes::LevelEnum levelEnum, bool viewMode );

private:
	puBomb< 44 >	_bomb1;
	puBomb< 44 >	_bomb2;
	puBomb< 44 >	_bomb3;
	puBomb< 44 >	_bomb4;
	puBomb< 44 >	_bomb5;
	puCoin< 40 >	_coin1;
	puPiggyBank< 60 >	_piggyBank1;
	puWallBox<160,20,eBlockScaled>	_wallBox4;
	puWallBox<20,20,eBlockScaled>	_wallBox15;
	puWallBox<20,20,eBlockScaled>	_wallBox16;
	puWallBox<20,20,eBlockScaled>	_wallBox17;
	puWallBox<20,20,eBlockScaled>	_wallBox18;
	puWallBox<20,20,eBlockScaled>	_wallBox19;
	puWallBox<20,20,eBlockScaled>	_wallBox20;
	puWallBox<20,20,eBlockScaled>	_wallBox21;
	puWallBox<20,20,eBlockScaled>	_wallBox22;
	puWallBox<20,20,eBlockScaled>	_wallBox23;
	puWallBox<290,20,eBlockScaled>	_wallBox24;
	puWallBox<290,20,eBlockScaled>	_wallBox25;
	puWallBox<35,20,eBlockScaled>	_wallBox10;
	puWallBox<35,20,eBlockScaled>	_wallBox11;
	puWallBox<35,20,eBlockScaled>	_wallBox12;
	puWallBox<35,20,eBlockScaled>	_wallBox13;
	puWallBox<35,20,eBlockScaled>	_wallBox14;
	puWallBox<35,20,eBlockScaled>	_wallBox5;
	puWallBox<35,20,eBlockScaled>	_wallBox6;
	puWallBox<35,20,eBlockScaled>	_wallBox7;
	puWallBox<35,20,eBlockScaled>	_wallBox8;
	puWallBox<35,20,eBlockScaled>	_wallBox9;
	puWallBox<50,20,eBlockScaled>	_wallBox3;
	puWallBox<70,20,eBlockScaled>	_wallBox1;
	puWallBox<70,20,eBlockScaled>	_wallBox2;
};
//--------------------------------------------------------------------------------------------
class LevelLetsBomb_6 : public Level
{
public:
	LevelLetsBomb_6( GameTypes::LevelEnum levelEnum, bool viewMode );

private:
	puBomb< 60 >	_bomb10;
	puBomb< 60 >	_bomb1;
	puBomb< 60 >	_bomb2;
	puBomb< 60 >	_bomb3;
	puBomb< 60 >	_bomb4;
	puBomb< 60 >	_bomb5;
	puBomb< 60 >	_bomb6;
	puBomb< 60 >	_bomb7;
	puBomb< 60 >	_bomb8;
	puBomb< 60 >	_bomb9;
	puCoin< 60 >	_coin1;
	puPiggyBankStatic< 60 >	_piggyBankStatic1;
	puWallBox<160,20,eBlockScaled>	_wallBox1;
	puWallBox<360,20,eBlockScaled>	_wallBox2;
	puWallBox<360,20,eBlockScaled>	_wallBox3;
	puWallBox<70,20,eBlockScaled>	_wallBox4;
};
//--------------------------------------------------------------------------------------------
class LevelLetsBomb_Pile : public Level
{
public:
	LevelLetsBomb_Pile( GameTypes::LevelEnum levelEnum, bool viewMode );

private:
	puBomb< 44 >	_bomb10;
	puBomb< 44 >	_bomb11;
	puBomb< 44 >	_bomb12;
	puBomb< 44 >	_bomb13;
	puBomb< 44 >	_bomb14;
	puBomb< 44 >	_bomb15;
	puBomb< 44 >	_bomb16;
	puBomb< 44 >	_bomb17;
	puBomb< 44 >	_bomb18;
	puBomb< 44 >	_bomb19;
	puBomb< 44 >	_bomb1;
	puBomb< 44 >	_bomb20;
	puBomb< 44 >	_bomb21;
	puBomb< 44 >	_bomb22;
	puBomb< 44 >	_bomb2;
	puBomb< 44 >	_bomb3;
	puBomb< 44 >	_bomb4;
	puBomb< 44 >	_bomb5;
	puBomb< 44 >	_bomb6;
	puBomb< 44 >	_bomb7;
	puBomb< 44 >	_bomb8;
	puBomb< 44 >	_bomb9;
	puCoin< 44 >	_coin1;
	puCoin< 44 >	_coin2;
	puCoin< 44 >	_coin3;
	puCoin< 44 >	_coin4;
	puCoin< 44 >	_coin5;
	puPiggyBank< 44 >	_piggyBank1;
	puWallBox<20,20,eBlockScaled>	_wallBox1;
	puWallBox<20,20,eBlockScaled>	_wallBox2;
	puWallBox<600,20,eBlockScaled>	_wallBox3;
};
//--------------------------------------------------------------------------------------------
class LevelLetsBomb_8 : public Level
{
public:
	LevelLetsBomb_8( GameTypes::LevelEnum levelEnum, bool viewMode );

private:
	puBomb< 48 >	_bomb1;
	puBomb< 48 >	_bomb2;
	puBomb< 48 >	_bomb3;
	puBomb< 48 >	_bomb4;
	puCoin< 40 >	_coin1;
	puCoin< 40 >	_coin2;
	puCoin< 48 >	_coin3;
	puCoinStatic< 40 >	_coinStatic1;
	puPiggyBank< 60 >	_piggyBank1;
	puPiggyBank< 60 >	_piggyBank2;
	puPiggyBank< 60 >	_piggyBank3;
	puWallBox<20,20,eBlockScaled>	_wallBox1;
	puWallBox<20,20,eBlockScaled>	_wallBox2;
	puWallBox<20,20,eBlockScaled>	_wallBox3;
	puWallBox<20,20,eBlockScaled>	_wallBox4;
	puWallBox<20,20,eBlockScaled>	_wallBox5;
	puWallBox<20,20,eBlockScaled>	_wallBox6;
	puWallBox<20,20,eBlockScaled>	_wallBox7;
	puWallBox<20,20,eBlockScaled>	_wallBox8;
	puWallBox<860,20,eBlockScaled>	_wallBox9;

};
//--------------------------------------------------------------------------------------------

#endif



