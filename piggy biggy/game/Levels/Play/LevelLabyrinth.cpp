#include "LevelLabyrinth.h"
#include "Tools.h"
#include "Debug/MyDebug.h"
#include <math.h>


//--------------------------------------------------------------------------
// LevelLabyrinth_1
//--------------------------------------------------------------------------
const float LevelLabyrinth_1::MinDistanceThief2Thief	= 16.0f;
const float LevelLabyrinth_1::MinDistanceThief2Pig		= 16.0f;
const float LevelLabyrinth_1::MinDistanceThief2Coin		= 16.0f;

//--------------------------------------------------------------------------
LevelLabyrinth_1::LevelLabyrinth_1( GameTypes::LevelEnum levelEnum, bool viewMode ) : LevelAccelerometr( levelEnum, viewMode )
{
	// _piggyBank1
	_piggyBank1.GetBody()->GetFixtureList()->SetDensity( 7.50f );
	_piggyBank1.GetBody()->GetFixtureList()->SetFriction( 0.5f );
	_piggyBank1.GetBody()->GetFixtureList()->SetRestitution( 0.25f );
	_piggyBank1.SetBodyType( b2_dynamicBody );

	// _coin1
	_coin1.GetBody()->GetFixtureList()->SetDensity( 3.50f );
	_coin1.GetBody()->GetFixtureList()->SetFriction( 0.5f );
	_coin1.GetBody()->GetFixtureList()->SetRestitution( 0.25f );
	_coin1.SetBodyType( b2_dynamicBody );

	//Set blocks positions
	_piggyBank1.SetPosition( 69.4f, 45.8f );
	_coin1.SetPosition( 50.60f, 2.90f );
	_labBorder.SetPosition( 0.0f, 0.0f );

	b2Filter filter;
	filter.groupIndex = 0;
	filter.categoryBits = Config::eFilterCategoryPiggy;
	filter.maskBits = ~Config::eFilterCategoryCoinBorder & 0xffff;

	_piggyBank1.GetBody()->GetFixtureList()->SetFilterData( filter );
	_coin1.GetBody()->GetFixtureList()->SetFilterData( filter );

	SetThiefs();

	//------------------------
	// Set labyrinth
	//------------------------

	// 0 row
	_edges.push_back( puEdges::Edge( 0.0f * 2.0f, 0.0f * 2.0f, 3.0f * 2.0f, 0.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 4.0f * 2.0f, 0.0f * 2.0f, 7.0f * 2.0f, 0.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 8.0f * 2.0f, 0.0f * 2.0f, 9.0f * 2.0f, 0.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 10.0f * 2.0f, 0.0f * 2.0f, 14.0f * 2.0f, 0.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 15.0f * 2.0f, 0.0f * 2.0f, 19.0f * 2.0f, 0.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 20.0f * 2.0f, 0.0f * 2.0f, 21.0f * 2.0f, 0.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 22.0f * 2.0f, 0.0f * 2.0f, 23.0f * 2.0f, 0.0f * 2.0f) );

	// 1 row
	_edges.push_back( puEdges::Edge( 0.0f * 2.0f, 1.0f * 2.0f, 1.0f * 2.0f, 1.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 2.0f * 2.0f, 1.0f * 2.0f, 3.0f * 2.0f, 1.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 5.0f * 2.0f, 1.0f * 2.0f, 7.0f * 2.0f, 1.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 10.0f * 2.0f, 1.0f * 2.0f, 11.0f * 2.0f, 1.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 12.0f * 2.0f, 1.0f * 2.0f, 14.0f * 2.0f, 1.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 15.0f * 2.0f, 1.0f * 2.0f, 19.0f * 2.0f, 1.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 20.0f * 2.0f, 1.0f * 2.0f, 21.0f * 2.0f, 1.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 22.0f * 2.0f, 1.0f * 2.0f, 23.0f * 2.0f, 1.0f * 2.0f) );

	// 2 row
	_edges.push_back( puEdges::Edge( 1.0f * 2.0f, 2.0f * 2.0f, 3.0f * 2.0f, 2.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 5.0f * 2.0f, 2.0f * 2.0f, 7.0f * 2.0f, 2.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 9.0f * 2.0f, 2.0f * 2.0f, 10.0f * 2.0f, 2.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 13.0f * 2.0f, 2.0f * 2.0f, 18.0f * 2.0f, 2.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 20.0f * 2.0f, 2.0f * 2.0f, 21.0f * 2.0f, 2.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 22.0f * 2.0f, 2.0f * 2.0f, 23.0f * 2.0f, 2.0f * 2.0f) );

	// 3 row
	_edges.push_back( puEdges::Edge( 2.0f * 2.0f, 3.0f * 2.0f, 7.0f * 2.0f, 3.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 4.0f * 2.0f, 3.0f * 2.0f, 7.0f * 2.0f, 3.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 9.0f * 2.0f, 3.0f * 2.0f, 12.0f * 2.0f, 3.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 13.0f * 2.0f, 3.0f * 2.0f, 15.0f * 2.0f, 3.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 16.0f * 2.0f, 3.0f * 2.0f, 18.0f * 2.0f, 3.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 19.0f * 2.0f, 3.0f * 2.0f, 21.0f * 2.0f, 3.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 22.0f * 2.0f, 3.0f * 2.0f, 24.0f * 2.0f, 3.0f * 2.0f) );

	// 4 row
	_edges.push_back( puEdges::Edge( 2.0f * 2.0f, 4.0f * 2.0f, 3.0f * 2.0f, 4.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 4.0f * 2.0f, 4.0f * 2.0f, 6.0f * 2.0f, 4.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 10.0f * 2.0f, 4.0f * 2.0f, 15.0f * 2.0f, 4.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 18.0f * 2.0f, 4.0f * 2.0f, 19.0f * 2.0f, 4.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 20.0f * 2.0f, 4.0f * 2.0f, 21.0f * 2.0f, 4.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 22.0f * 2.0f, 4.0f * 2.0f, 23.0f * 2.0f, 4.0f * 2.0f) );

	// 5 row
	_edges.push_back( puEdges::Edge( 3.0f * 2.0f, 5.0f * 2.0f, 5.0f * 2.0f, 5.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 6.0f * 2.0f, 5.0f * 2.0f, 8.0f * 2.0f, 5.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 9.0f * 2.0f, 5.0f * 2.0f, 11.0f * 2.0f, 5.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 12.0f * 2.0f, 5.0f * 2.0f, 13.0f * 2.0f, 5.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 20.0f * 2.0f, 5.0f * 2.0f, 23.0f * 2.0f, 5.0f * 2.0f) );

	// 6 row
	_edges.push_back( puEdges::Edge( 1.0f * 2.0f, 6.0f * 2.0f, 2.0f * 2.0f, 6.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 3.0f * 2.0f, 6.0f * 2.0f, 8.0f * 2.0f, 6.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 10.0f * 2.0f, 6.0f * 2.0f, 12.0f * 2.0f, 6.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 13.0f * 2.0f, 6.0f * 2.0f, 14.0f * 2.0f, 6.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 15.0f * 2.0f, 6.0f * 2.0f, 16.0f * 2.0f, 6.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 20.0f * 2.0f, 6.0f * 2.0f, 22.0f * 2.0f, 6.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 23.0f * 2.0f, 6.0f * 2.0f, 24.0f * 2.0f, 6.0f * 2.0f) );

	// 7 row
	_edges.push_back( puEdges::Edge( 0.0f * 2.0f, 7.0f * 2.0f, 1.0f * 2.0f, 7.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 3.0f * 2.0f, 7.0f * 2.0f, 5.0f * 2.0f, 7.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 7.0f * 2.0f, 7.0f * 2.0f, 9.0f * 2.0f, 7.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 10.0f * 2.0f, 7.0f * 2.0f, 12.0f * 2.0f, 7.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 13.0f * 2.0f, 7.0f * 2.0f, 20.0f * 2.0f, 7.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 21.0f * 2.0f, 7.0f * 2.0f, 23.0f * 2.0f, 7.0f * 2.0f) );

	// 8 row
	_edges.push_back( puEdges::Edge( 1.0f * 2.0f, 8.0f * 2.0f, 2.0f * 2.0f, 8.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 4.0f * 2.0f, 8.0f * 2.0f, 5.0f * 2.0f, 8.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 7.0f * 2.0f, 8.0f * 2.0f, 9.0f * 2.0f, 8.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 10.0f * 2.0f, 8.0f * 2.0f, 12.0f * 2.0f, 8.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 13.0f * 2.0f, 8.0f * 2.0f, 16.0f * 2.0f, 8.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 18.0f * 2.0f, 8.0f * 2.0f, 19.0f * 2.0f, 8.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 20.0f * 2.0f, 8.0f * 2.0f, 21.0f * 2.0f, 8.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 22.0f * 2.0f, 8.0f * 2.0f, 23.0f * 2.0f, 8.0f * 2.0f) );

	// 9 row
	_edges.push_back( puEdges::Edge( 1.0f * 2.0f, 9.0f * 2.0f, 2.0f * 2.0f, 9.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 3.0f * 2.0f, 9.0f * 2.0f, 4.0f * 2.0f, 9.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 5.0f * 2.0f, 9.0f * 2.0f, 7.0f * 2.0f, 9.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 8.0f * 2.0f, 9.0f * 2.0f, 14.0f * 2.0f, 9.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 15.0f * 2.0f, 9.0f * 2.0f, 16.0f * 2.0f, 9.0f * 2.0f) );

	// 10 row
	_edges.push_back( puEdges::Edge( 5.0f * 2.0f, 10.0f * 2.0f, 7.0f * 2.0f, 10.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 9.0f * 2.0f, 10.0f * 2.0f, 10.0f * 2.0f, 10.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 11.0f * 2.0f, 10.0f * 2.0f, 13.0f * 2.0f, 10.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 14.0f * 2.0f, 10.0f * 2.0f, 15.0f * 2.0f, 10.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 16.0f * 2.0f, 10.0f * 2.0f, 17.0f * 2.0f, 10.0f * 2.0f) );

	// 11 row
	_edges.push_back( puEdges::Edge( 2.0f * 2.0f, 11.0f * 2.0f, 3.0f * 2.0f, 11.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 5.0f * 2.0f, 11.0f * 2.0f, 8.0f * 2.0f, 11.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 12.0f * 2.0f, 11.0f * 2.0f, 13.0f * 2.0f, 11.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 15.0f * 2.0f, 11.0f * 2.0f, 16.0f * 2.0f, 11.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 17.0f * 2.0f, 11.0f * 2.0f, 18.0f * 2.0f, 11.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 19.0f * 2.0f, 11.0f * 2.0f, 20.0f * 2.0f, 11.0f * 2.0f) );

	// 12 row
	_edges.push_back( puEdges::Edge( 4.0f * 2.0f, 12.0f * 2.0f, 5.0f * 2.0f, 12.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 7.0f * 2.0f, 12.0f * 2.0f, 9.0f * 2.0f, 12.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 11.0f * 2.0f, 12.0f * 2.0f, 12.0f * 2.0f, 12.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 13.0f * 2.0f, 12.0f * 2.0f, 14.0f * 2.0f, 12.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 16.0f * 2.0f, 12.0f * 2.0f, 17.0f * 2.0f, 12.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 18.0f * 2.0f, 12.0f * 2.0f, 20.0f * 2.0f, 12.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 22.0f * 2.0f, 12.0f * 2.0f, 24.0f * 2.0f, 12.0f * 2.0f) );

	// 13 row
	_edges.push_back( puEdges::Edge( 1.0f * 2.0f, 13.0f * 2.0f, 2.0f * 2.0f, 13.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 3.0f * 2.0f, 13.0f * 2.0f, 5.0f * 2.0f, 13.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 6.0f * 2.0f, 13.0f * 2.0f, 8.0f * 2.0f, 13.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 9.0f * 2.0f, 13.0f * 2.0f, 12.0f * 2.0f, 13.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 13.0f * 2.0f, 13.0f * 2.0f, 14.0f * 2.0f, 13.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 15.0f * 2.0f, 13.0f * 2.0f, 16.0f * 2.0f, 13.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 17.0f * 2.0f, 13.0f * 2.0f, 19.0f * 2.0f, 13.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 22.0f * 2.0f, 13.0f * 2.0f, 23.0f * 2.0f, 13.0f * 2.0f) );

	// 14 row
	_edges.push_back( puEdges::Edge( 1.0f * 2.0f, 14.0f * 2.0f, 3.0f * 2.0f, 14.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 4.0f * 2.0f, 14.0f * 2.0f, 5.0f * 2.0f, 14.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 9.0f * 2.0f, 14.0f * 2.0f, 11.0f * 2.0f, 14.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 14.0f * 2.0f, 14.0f * 2.0f, 15.0f * 2.0f, 14.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 18.0f * 2.0f, 14.0f * 2.0f, 19.0f * 2.0f, 14.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 22.0f * 2.0f, 14.0f * 2.0f, 24.0f * 2.0f, 14.0f * 2.0f) );

	// 15 row
	_edges.push_back( puEdges::Edge( 0.0f * 2.0f, 15.0f * 2.0f, 4.0f * 2.0f, 15.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 5.0f * 2.0f, 15.0f * 2.0f, 6.0f * 2.0f, 15.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 8.0f * 2.0f, 15.0f * 2.0f, 11.0f * 2.0f, 15.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 14.0f * 2.0f, 15.0f * 2.0f, 16.0f * 2.0f, 15.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 18.0f * 2.0f, 15.0f * 2.0f, 23.0f * 2.0f, 15.0f * 2.0f) );

	// 16 row
	_edges.push_back( puEdges::Edge( 4.0f * 2.0f, 16.0f * 2.0f, 7.0f * 2.0f, 16.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 8.0f * 2.0f, 16.0f * 2.0f, 12.0f * 2.0f, 16.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 13.0f * 2.0f, 16.0f * 2.0f, 24.0f * 2.0f, 16.0f * 2.0f) );


	////////////////////////////////////////////////
	// 0 col
	_edges.push_back( puEdges::Edge( 0.0f * 2.0f, 0.0f * 2.0f, 0.0f * 2.0f, 1.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 0.0f * 2.0f, 2.0f * 2.0f, 0.0f * 2.0f, 7.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 0.0f * 2.0f, 8.0f * 2.0f, 0.0f * 2.0f, 15.0f * 2.0f) );

	// 1 col
	_edges.push_back( puEdges::Edge( 1.0f * 2.0f, 2.0f * 2.0f, 1.0f * 2.0f, 6.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 1.0f * 2.0f, 8.0f * 2.0f, 1.0f * 2.0f, 12.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 1.0f * 2.0f, 13.0f * 2.0f, 1.0f * 2.0f, 14.0f * 2.0f) );

	// 2 col
	_edges.push_back( puEdges::Edge( 2.0f * 2.0f, 2.0f * 2.0f, 2.0f * 2.0f, 3.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 2.0f * 2.0f, 4.0f * 2.0f, 2.0f * 2.0f, 8.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 2.0f * 2.0f, 10.0f * 2.0f, 2.0f * 2.0f, 13.0f * 2.0f) );

	// 3 col
	_edges.push_back( puEdges::Edge( 3.0f * 2.0f, 1.0f * 2.0f, 3.0f * 2.0f, 2.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 3.0f * 2.0f, 4.0f * 2.0f, 3.0f * 2.0f, 5.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 3.0f * 2.0f, 7.0f * 2.0f, 3.0f * 2.0f, 9.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 3.0f * 2.0f, 10.0f * 2.0f, 3.0f * 2.0f, 12.0f * 2.0f) );

	// 4 col
	_edges.push_back( puEdges::Edge( 4.0f * 2.0f, 0.0f * 2.0f, 4.0f * 2.0f, 2.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 4.0f * 2.0f, 3.0f * 2.0f, 4.0f * 2.0f, 4.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 4.0f * 2.0f, 5.0f * 2.0f, 4.0f * 2.0f, 6.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 4.0f * 2.0f, 9.0f * 2.0f, 4.0f * 2.0f, 13.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 4.0f * 2.0f, 15.0f * 2.0f, 4.0f * 2.0f, 16.0f * 2.0f) );

	// 5 col
	_edges.push_back( puEdges::Edge( 5.0f * 2.0f, 1.0f * 2.0f, 5.0f * 2.0f, 2.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 5.0f * 2.0f, 7.0f * 2.0f, 5.0f * 2.0f, 8.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 5.0f * 2.0f, 10.0f * 2.0f, 5.0f * 2.0f, 11.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 5.0f * 2.0f, 13.0f * 2.0f, 5.0f * 2.0f, 15.0f * 2.0f) );

	// 6 col
	_edges.push_back( puEdges::Edge( 6.0f * 2.0f, 4.0f * 2.0f, 6.0f * 2.0f, 5.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 6.0f * 2.0f, 7.0f * 2.0f, 6.0f * 2.0f, 9.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 6.0f * 2.0f, 11.0f * 2.0f, 6.0f * 2.0f, 15.0f * 2.0f) );

	// 7 col
	_edges.push_back( puEdges::Edge( 7.0f * 2.0f, 0.0f * 2.0f, 7.0f * 2.0f, 1.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 7.0f * 2.0f, 3.0f * 2.0f, 7.0f * 2.0f, 4.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 7.0f * 2.0f, 5.0f * 2.0f, 7.0f * 2.0f, 5.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 7.0f * 2.0f, 8.0f * 2.0f, 7.0f * 2.0f, 10.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 7.0f * 2.0f, 14.0f * 2.0f, 7.0f * 2.0f, 16.0f * 2.0f) );

	// 8 col
	_edges.push_back( puEdges::Edge( 8.0f * 2.0f, 0.0f * 2.0f, 8.0f * 2.0f, 2.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 8.0f * 2.0f, 3.0f * 2.0f, 8.0f * 2.0f, 5.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 8.0f * 2.0f, 6.0f * 2.0f, 8.0f * 2.0f, 7.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 8.0f * 2.0f, 9.0f * 2.0f, 8.0f * 2.0f, 10.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 8.0f * 2.0f, 11.0f * 2.0f, 8.0f * 2.0f, 12.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 8.0f * 2.0f, 13.0f * 2.0f, 8.0f * 2.0f, 15.0f * 2.0f) );

	// 9 col
	_edges.push_back( puEdges::Edge( 9.0f * 2.0f, 0.0f * 2.0f, 9.0f * 2.0f, 1.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 9.0f * 2.0f, 3.0f * 2.0f, 9.0f * 2.0f, 7.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 9.0f * 2.0f, 8.0f * 2.0f, 9.0f * 2.0f, 9.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 9.0f * 2.0f, 10.0f * 2.0f, 9.0f * 2.0f, 12.0f * 2.0f) );

	// 10 col
	_edges.push_back( puEdges::Edge( 10.0f * 2.0f, 1.0f * 2.0f, 10.0f * 2.0f, 2.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 10.0f * 2.0f, 6.0f * 2.0f, 10.0f * 2.0f, 7.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 10.0f * 2.0f, 10.0f * 2.0f, 10.0f * 2.0f, 12.0f * 2.0f) );

	// 11 col
	_edges.push_back( puEdges::Edge( 11.0f * 2.0f, 1.0f * 2.0f, 11.0f * 2.0f, 3.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 11.0f * 2.0f, 4.0f * 2.0f, 11.0f * 2.0f, 5.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 11.0f * 2.0f, 10.0f * 2.0f, 11.0f * 2.0f, 12.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 11.0f * 2.0f, 14.0f * 2.0f, 11.0f * 2.0f, 15.0f * 2.0f) );

	// 12 col
	_edges.push_back( puEdges::Edge( 12.0f * 2.0f, 1.0f * 2.0f, 12.0f * 2.0f, 3.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 12.0f * 2.0f, 5.0f * 2.0f, 12.0f * 2.0f, 6.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 12.0f * 2.0f, 7.0f * 2.0f, 12.0f * 2.0f, 8.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 12.0f * 2.0f, 11.0f * 2.0f, 12.0f * 2.0f, 12.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 12.0f * 2.0f, 13.0f * 2.0f, 12.0f * 2.0f, 16.0f * 2.0f) );

	// 13 col
	_edges.push_back( puEdges::Edge( 13.0f * 2.0f, 2.0f * 2.0f, 13.0f * 2.0f, 3.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 13.0f * 2.0f, 5.0f * 2.0f, 13.0f * 2.0f, 6.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 13.0f * 2.0f, 7.0f * 2.0f, 13.0f * 2.0f, 8.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 13.0f * 2.0f, 9.0f * 2.0f, 13.0f * 2.0f, 10.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 13.0f * 2.0f, 11.0f * 2.0f, 13.0f * 2.0f, 12.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 13.0f * 2.0f, 13.0f * 2.0f, 13.0f * 2.0f, 16.0f * 2.0f) );

	// 14 col
	_edges.push_back( puEdges::Edge( 14.0f * 2.0f, 0.0f * 2.0f, 14.0f * 2.0f, 1.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 14.0f * 2.0f, 4.0f * 2.0f, 14.0f * 2.0f, 6.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 14.0f * 2.0f, 9.0f * 2.0f, 14.0f * 2.0f, 10.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 14.0f * 2.0f, 11.0f * 2.0f, 14.0f * 2.0f, 13.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 14.0f * 2.0f, 14.0f * 2.0f, 14.0f * 2.0f, 15.0f * 2.0f) );

	// 15 col
	_edges.push_back( puEdges::Edge( 15.0f * 2.0f, 0.0f * 2.0f, 15.0f * 2.0f, 1.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 15.0f * 2.0f, 4.0f * 2.0f, 15.0f * 2.0f, 6.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 15.0f * 2.0f, 9.0f * 2.0f, 15.0f * 2.0f, 10.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 15.0f * 2.0f, 11.0f * 2.0f, 15.0f * 2.0f, 13.0f * 2.0f) );

	// 16 col
	_edges.push_back( puEdges::Edge( 16.0f * 2.0f, 3.0f * 2.0f, 16.0f * 2.0f, 5.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 16.0f * 2.0f, 8.0f * 2.0f, 16.0f * 2.0f, 9.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 16.0f * 2.0f, 11.0f * 2.0f, 16.0f * 2.0f, 12.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 16.0f * 2.0f, 13.0f * 2.0f, 16.0f * 2.0f, 15.0f * 2.0f) );

	// 17 col
	_edges.push_back( puEdges::Edge( 17.0f * 2.0f, 3.0f * 2.0f, 17.0f * 2.0f, 7.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 17.0f * 2.0f, 8.0f * 2.0f, 17.0f * 2.0f, 10.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 17.0f * 2.0f, 11.0f * 2.0f, 17.0f * 2.0f, 12.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 17.0f * 2.0f, 13.0f * 2.0f, 17.0f * 2.0f, 16.0f * 2.0f) );

	// 18 col
	_edges.push_back( puEdges::Edge( 18.0f * 2.0f, 2.0f * 2.0f, 18.0f * 2.0f, 3.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 18.0f * 2.0f, 4.0f * 2.0f, 18.0f * 2.0f, 6.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 18.0f * 2.0f, 8.0f * 2.0f, 18.0f * 2.0f, 10.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 18.0f * 2.0f, 11.0f * 2.0f, 18.0f * 2.0f, 12.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 18.0f * 2.0f, 14.0f * 2.0f, 18.0f * 2.0f, 15.0f * 2.0f) );

	// 19 col
	_edges.push_back( puEdges::Edge( 19.0f * 2.0f, 1.0f * 2.0f, 19.0f * 2.0f, 2.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 19.0f * 2.0f, 3.0f * 2.0f, 19.0f * 2.0f, 7.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 19.0f * 2.0f, 9.0f * 2.0f, 19.0f * 2.0f, 11.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 19.0f * 2.0f, 13.0f * 2.0f, 19.0f * 2.0f, 14.0f * 2.0f) );

	// 20 col
	_edges.push_back( puEdges::Edge( 20.0f * 2.0f, 0.0f * 2.0f, 20.0f * 2.0f, 1.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 20.0f * 2.0f, 4.0f * 2.0f, 20.0f * 2.0f, 5.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 20.0f * 2.0f, 6.0f * 2.0f, 20.0f * 2.0f, 11.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 20.0f * 2.0f, 12.0f * 2.0f, 20.0f * 2.0f, 15.0f * 2.0f) );

	// 21 col
	_edges.push_back( puEdges::Edge( 21.0f * 2.0f, 0.0f * 2.0f, 21.0f * 2.0f, 1.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 21.0f * 2.0f, 2.0f * 2.0f, 21.0f * 2.0f, 4.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 21.0f * 2.0f, 8.0f * 2.0f, 21.0f * 2.0f, 9.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 21.0f * 2.0f, 10.0f * 2.0f, 21.0f * 2.0f, 15.0f * 2.0f) );

	// 22 col
	_edges.push_back( puEdges::Edge( 22.0f * 2.0f, 0.0f * 2.0f, 22.0f * 2.0f, 1.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 22.0f * 2.0f, 2.0f * 2.0f, 22.0f * 2.0f, 3.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 22.0f * 2.0f, 8.0f * 2.0f, 22.0f * 2.0f, 11.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 22.0f * 2.0f, 12.0f * 2.0f, 22.0f * 2.0f, 13.0f * 2.0f) );

	// 23 col
	_edges.push_back( puEdges::Edge( 23.0f * 2.0f, 0.0f * 2.0f, 23.0f * 2.0f, 1.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 23.0f * 2.0f, 4.0f * 2.0f, 23.0f * 2.0f, 5.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 23.0f * 2.0f, 7.0f * 2.0f, 23.0f * 2.0f, 8.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 23.0f * 2.0f, 9.0f * 2.0f, 23.0f * 2.0f, 13.0f * 2.0f) );

	// 24 col
	_edges.push_back( puEdges::Edge( 24.0f * 2.0f, 2.0f * 2.0f, 24.0f * 2.0f, 3.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 24.0f * 2.0f, 4.0f * 2.0f, 24.0f * 2.0f, 6.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 24.0f * 2.0f, 7.0f * 2.0f, 24.0f * 2.0f, 11.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 24.0f * 2.0f, 13.0f * 2.0f, 24.0f * 2.0f, 16.0f * 2.0f) );

	//Set blocks positions
	//_labyrinth.SetPosition( Tools::GetScreenMiddleX() / 2.0f / RATIO , Tools::GetScreenMiddleY() / 2.0f / RATIO );
	//_labyrinth.SetPosition( Config::GameSize.width / 2.0f / RATIO, Config::GameSize.height / 2.0f / RATIO );
	float scale;
	float x;
	float y;

	scale = 1.95f;
	_labyrinth.SetEdges( &_edges, scale );

	x = Config::GameSize.width / 2.0f / RATIO - Config::GameSize.width / 4.0f / RATIO * scale ;
	y = Config::GameSize.height / 2.0f / RATIO - Config::GameSize.height / 4.0f / RATIO * scale ;
	_labyrinth.SetPosition( x, y );

	_border.SetBorderSize( 3.0f );

	//_background = CCSprite::spriteWithFile( Skins::GetSkinName( "Images/Levels/bg-labirynth.png" ) );
	ConstFinal();
}
//--------------------------------------------------------------------------
void LevelLabyrinth_1::SetThiefs()
{
	b2Vec2 pos;
	b2Vec2 posRand;
	float distance;
	bool posCheck;

	typedef list<b2Vec2> Positions;

	Positions positions;
	Positions::iterator it;

	//positions.push_back( _coin1.GetReversedPosition() );
	//positions.push_back( _piggyBank1.GetReversedPosition() );

	Thief *thief;
	for ( int i = 0; i < ThiefsCount; i++ )
	{
		thief = &_thiefs[i];

		posCheck = false;
		while( ! posCheck )
		{
			posRand = GetRandPos();
			posCheck = true;
			for ( it = positions.begin(); it != positions.end() && posCheck; it++ )
			{
				pos = (*it);
				distance = sqrt( pow (( posRand.x - pos.x ), 2.0f ) + pow (( posRand.y - pos.y ), 2.0f ));
				if ( distance < MinDistanceThief2Thief )
					posCheck = false;

				pos = _coin1.GetReversedPosition();
				distance = sqrt( pow (( posRand.x - pos.x ), 2.0f ) + pow (( posRand.y - pos.y ), 2.0f ));
				if ( distance < MinDistanceThief2Coin )
					posCheck = false;

				pos = _piggyBank1.GetReversedPosition();
				distance = sqrt( pow (( posRand.x - pos.x ), 2.0f ) + pow (( posRand.y - pos.y ), 2.0f ));
				if ( distance < MinDistanceThief2Pig )
					posCheck = false;
			}
		}

		D_LOG("Rand pos to set %f %f", posRand.x, posRand.y );
		thief->SetPosition( posRand );
		thief->GetBody()->GetFixtureList()->SetFriction( 0.5f );
		thief->GetBody()->GetFixtureList()->SetRestitution( 0.6f );

		positions.push_back( posRand );
	}

}
//--------------------------------------------------------------------------
b2Vec2 LevelLabyrinth_1::GetRandPos()
{
	b2Vec2 randPos;
	float x;
	float y;
	float border;

	border = 2.0f;

	x = border + ( float ) ( random() % (int) ( Config::GameSize.width / RATIO - 2.0f * border));
	y = border + ( float ) ( random() % (int) ( Config::GameSize.height / RATIO - 2.0f * border ));

	//D_LOG("Rand x: %f %y :%f", x, y );

	randPos.Set( x, y );
	return randPos;
}
//--------------------------------------------------------------------------




//--------------------------------------------------------------------------
// LevelLabyrinth_Imposible
//--------------------------------------------------------------------------
const float LevelLabyrinth_Imposible::MinDistanceThief2Thief	= 8.0f;
const float LevelLabyrinth_Imposible::MinDistanceThief2Pig		= 14.0f;
const float LevelLabyrinth_Imposible::MinDistanceThief2Coin		= 14.0f;

//--------------------------------------------------------------------------
LevelLabyrinth_Imposible::LevelLabyrinth_Imposible( GameTypes::LevelEnum levelEnum, bool viewMode ) : LevelAccelerometr( levelEnum, viewMode )
{
	// _piggyBank1
	_piggyBank1.GetBody()->GetFixtureList()->SetDensity( 7.50f );
	_piggyBank1.GetBody()->GetFixtureList()->SetFriction( 0.5f );
	_piggyBank1.GetBody()->GetFixtureList()->SetRestitution( 0.25f );
	_piggyBank1.SetBodyType( b2_dynamicBody );

	// _coin1
	_coin1.GetBody()->GetFixtureList()->SetDensity( 3.50f );
	_coin1.GetBody()->GetFixtureList()->SetFriction( 0.5f );
	_coin1.GetBody()->GetFixtureList()->SetRestitution( 0.25f );
	_coin1.SetBodyType( b2_dynamicBody );

	//Set blocks positions
	_piggyBank1.SetPosition( 57.6f, 56.7f );
	_coin1.SetPosition( 53.60f, 3.50f );
	_labBorder.SetPosition( 0.0f, 0.0f );

	b2Filter filter;
	filter.groupIndex = 0;
	filter.categoryBits = Config::eFilterCategoryPiggy;
	filter.maskBits = ~Config::eFilterCategoryCoinBorder & 0xffff;

	_piggyBank1.GetBody()->GetFixtureList()->SetFilterData( filter );
	_coin1.GetBody()->GetFixtureList()->SetFilterData( filter );

	SetThiefs();

	//------------------------
	// Set labyrinth
	//------------------------

	// 0 row
	_edges.push_back( puEdges::Edge( 0.0f * 2.0f, 0.0f * 2.0f, 3.0f * 2.0f, 0.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 4.0f * 2.0f, 0.0f * 2.0f, 7.0f * 2.0f, 0.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 8.0f * 2.0f, 0.0f * 2.0f, 9.0f * 2.0f, 0.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 10.0f * 2.0f, 0.0f * 2.0f, 14.0f * 2.0f, 0.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 15.0f * 2.0f, 0.0f * 2.0f, 19.0f * 2.0f, 0.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 20.0f * 2.0f, 0.0f * 2.0f, 21.0f * 2.0f, 0.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 22.0f * 2.0f, 0.0f * 2.0f, 23.0f * 2.0f, 0.0f * 2.0f) );

	// 1 row
	_edges.push_back( puEdges::Edge( 0.0f * 2.0f, 1.0f * 2.0f, 1.0f * 2.0f, 1.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 2.0f * 2.0f, 1.0f * 2.0f, 3.0f * 2.0f, 1.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 5.0f * 2.0f, 1.0f * 2.0f, 7.0f * 2.0f, 1.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 10.0f * 2.0f, 1.0f * 2.0f, 11.0f * 2.0f, 1.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 12.0f * 2.0f, 1.0f * 2.0f, 14.0f * 2.0f, 1.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 15.0f * 2.0f, 1.0f * 2.0f, 19.0f * 2.0f, 1.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 20.0f * 2.0f, 1.0f * 2.0f, 21.0f * 2.0f, 1.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 22.0f * 2.0f, 1.0f * 2.0f, 23.0f * 2.0f, 1.0f * 2.0f) );

	// 2 row
	_edges.push_back( puEdges::Edge( 1.0f * 2.0f, 2.0f * 2.0f, 3.0f * 2.0f, 2.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 5.0f * 2.0f, 2.0f * 2.0f, 7.0f * 2.0f, 2.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 9.0f * 2.0f, 2.0f * 2.0f, 10.0f * 2.0f, 2.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 13.0f * 2.0f, 2.0f * 2.0f, 18.0f * 2.0f, 2.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 19.0f * 2.0f, 2.0f * 2.0f, 21.0f * 2.0f, 2.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 22.0f * 2.0f, 2.0f * 2.0f, 23.0f * 2.0f, 2.0f * 2.0f) );

	// 3 row
	_edges.push_back( puEdges::Edge( 2.0f * 2.0f, 3.0f * 2.0f, 7.0f * 2.0f, 3.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 4.0f * 2.0f, 3.0f * 2.0f, 7.0f * 2.0f, 3.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 9.0f * 2.0f, 3.0f * 2.0f, 12.0f * 2.0f, 3.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 13.0f * 2.0f, 3.0f * 2.0f, 15.0f * 2.0f, 3.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 16.0f * 2.0f, 3.0f * 2.0f, 18.0f * 2.0f, 3.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 19.0f * 2.0f, 3.0f * 2.0f, 21.0f * 2.0f, 3.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 22.0f * 2.0f, 3.0f * 2.0f, 24.0f * 2.0f, 3.0f * 2.0f) );

	// 4 row
	_edges.push_back( puEdges::Edge( 2.0f * 2.0f, 4.0f * 2.0f, 3.0f * 2.0f, 4.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 4.0f * 2.0f, 4.0f * 2.0f, 6.0f * 2.0f, 4.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 10.0f * 2.0f, 4.0f * 2.0f, 15.0f * 2.0f, 4.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 18.0f * 2.0f, 4.0f * 2.0f, 19.0f * 2.0f, 4.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 20.0f * 2.0f, 4.0f * 2.0f, 21.0f * 2.0f, 4.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 22.0f * 2.0f, 4.0f * 2.0f, 23.0f * 2.0f, 4.0f * 2.0f) );

	// 5 row
	_edges.push_back( puEdges::Edge( 3.0f * 2.0f, 5.0f * 2.0f, 5.0f * 2.0f, 5.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 6.0f * 2.0f, 5.0f * 2.0f, 8.0f * 2.0f, 5.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 9.0f * 2.0f, 5.0f * 2.0f, 11.0f * 2.0f, 5.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 12.0f * 2.0f, 5.0f * 2.0f, 13.0f * 2.0f, 5.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 20.0f * 2.0f, 5.0f * 2.0f, 23.0f * 2.0f, 5.0f * 2.0f) );

	// 6 row
	_edges.push_back( puEdges::Edge( 1.0f * 2.0f, 6.0f * 2.0f, 2.0f * 2.0f, 6.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 3.0f * 2.0f, 6.0f * 2.0f, 8.0f * 2.0f, 6.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 10.0f * 2.0f, 6.0f * 2.0f, 12.0f * 2.0f, 6.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 13.0f * 2.0f, 6.0f * 2.0f, 14.0f * 2.0f, 6.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 15.0f * 2.0f, 6.0f * 2.0f, 16.0f * 2.0f, 6.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 20.0f * 2.0f, 6.0f * 2.0f, 22.0f * 2.0f, 6.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 23.0f * 2.0f, 6.0f * 2.0f, 24.0f * 2.0f, 6.0f * 2.0f) );

	// 7 row
	_edges.push_back( puEdges::Edge( 0.0f * 2.0f, 7.0f * 2.0f, 1.0f * 2.0f, 7.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 3.0f * 2.0f, 7.0f * 2.0f, 5.0f * 2.0f, 7.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 7.0f * 2.0f, 7.0f * 2.0f, 9.0f * 2.0f, 7.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 10.0f * 2.0f, 7.0f * 2.0f, 12.0f * 2.0f, 7.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 13.0f * 2.0f, 7.0f * 2.0f, 20.0f * 2.0f, 7.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 21.0f * 2.0f, 7.0f * 2.0f, 23.0f * 2.0f, 7.0f * 2.0f) );

	// 8 row
	_edges.push_back( puEdges::Edge( 1.0f * 2.0f, 8.0f * 2.0f, 2.0f * 2.0f, 8.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 4.0f * 2.0f, 8.0f * 2.0f, 5.0f * 2.0f, 8.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 7.0f * 2.0f, 8.0f * 2.0f, 9.0f * 2.0f, 8.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 10.0f * 2.0f, 8.0f * 2.0f, 12.0f * 2.0f, 8.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 13.0f * 2.0f, 8.0f * 2.0f, 16.0f * 2.0f, 8.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 18.0f * 2.0f, 8.0f * 2.0f, 19.0f * 2.0f, 8.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 20.0f * 2.0f, 8.0f * 2.0f, 21.0f * 2.0f, 8.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 22.0f * 2.0f, 8.0f * 2.0f, 23.0f * 2.0f, 8.0f * 2.0f) );

	// 9 row
	_edges.push_back( puEdges::Edge( 1.0f * 2.0f, 9.0f * 2.0f, 2.0f * 2.0f, 9.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 3.0f * 2.0f, 9.0f * 2.0f, 4.0f * 2.0f, 9.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 5.0f * 2.0f, 9.0f * 2.0f, 7.0f * 2.0f, 9.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 8.0f * 2.0f, 9.0f * 2.0f, 14.0f * 2.0f, 9.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 15.0f * 2.0f, 9.0f * 2.0f, 16.0f * 2.0f, 9.0f * 2.0f) );

	// 10 row
	_edges.push_back( puEdges::Edge( 5.0f * 2.0f, 10.0f * 2.0f, 7.0f * 2.0f, 10.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 9.0f * 2.0f, 10.0f * 2.0f, 10.0f * 2.0f, 10.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 11.0f * 2.0f, 10.0f * 2.0f, 13.0f * 2.0f, 10.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 14.0f * 2.0f, 10.0f * 2.0f, 15.0f * 2.0f, 10.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 16.0f * 2.0f, 10.0f * 2.0f, 17.0f * 2.0f, 10.0f * 2.0f) );

	// 11 row
	_edges.push_back( puEdges::Edge( 2.0f * 2.0f, 11.0f * 2.0f, 3.0f * 2.0f, 11.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 5.0f * 2.0f, 11.0f * 2.0f, 8.0f * 2.0f, 11.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 12.0f * 2.0f, 11.0f * 2.0f, 13.0f * 2.0f, 11.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 15.0f * 2.0f, 11.0f * 2.0f, 16.0f * 2.0f, 11.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 17.0f * 2.0f, 11.0f * 2.0f, 18.0f * 2.0f, 11.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 19.0f * 2.0f, 11.0f * 2.0f, 20.0f * 2.0f, 11.0f * 2.0f) );

	// 12 row
	_edges.push_back( puEdges::Edge( 4.0f * 2.0f, 12.0f * 2.0f, 5.0f * 2.0f, 12.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 7.0f * 2.0f, 12.0f * 2.0f, 9.0f * 2.0f, 12.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 11.0f * 2.0f, 12.0f * 2.0f, 12.0f * 2.0f, 12.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 13.0f * 2.0f, 12.0f * 2.0f, 14.0f * 2.0f, 12.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 16.0f * 2.0f, 12.0f * 2.0f, 17.0f * 2.0f, 12.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 18.0f * 2.0f, 12.0f * 2.0f, 20.0f * 2.0f, 12.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 22.0f * 2.0f, 12.0f * 2.0f, 24.0f * 2.0f, 12.0f * 2.0f) );

	// 13 row
	_edges.push_back( puEdges::Edge( 1.0f * 2.0f, 13.0f * 2.0f, 2.0f * 2.0f, 13.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 3.0f * 2.0f, 13.0f * 2.0f, 5.0f * 2.0f, 13.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 6.0f * 2.0f, 13.0f * 2.0f, 8.0f * 2.0f, 13.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 9.0f * 2.0f, 13.0f * 2.0f, 12.0f * 2.0f, 13.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 13.0f * 2.0f, 13.0f * 2.0f, 14.0f * 2.0f, 13.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 15.0f * 2.0f, 13.0f * 2.0f, 16.0f * 2.0f, 13.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 17.0f * 2.0f, 13.0f * 2.0f, 19.0f * 2.0f, 13.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 22.0f * 2.0f, 13.0f * 2.0f, 23.0f * 2.0f, 13.0f * 2.0f) );

	// 14 row
	_edges.push_back( puEdges::Edge( 1.0f * 2.0f, 14.0f * 2.0f, 3.0f * 2.0f, 14.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 4.0f * 2.0f, 14.0f * 2.0f, 5.0f * 2.0f, 14.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 9.0f * 2.0f, 14.0f * 2.0f, 11.0f * 2.0f, 14.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 14.0f * 2.0f, 14.0f * 2.0f, 15.0f * 2.0f, 14.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 18.0f * 2.0f, 14.0f * 2.0f, 19.0f * 2.0f, 14.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 22.0f * 2.0f, 14.0f * 2.0f, 24.0f * 2.0f, 14.0f * 2.0f) );

	// 15 row
	_edges.push_back( puEdges::Edge( 0.0f * 2.0f, 15.0f * 2.0f, 4.0f * 2.0f, 15.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 5.0f * 2.0f, 15.0f * 2.0f, 6.0f * 2.0f, 15.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 8.0f * 2.0f, 15.0f * 2.0f, 11.0f * 2.0f, 15.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 14.0f * 2.0f, 15.0f * 2.0f, 16.0f * 2.0f, 15.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 18.0f * 2.0f, 15.0f * 2.0f, 23.0f * 2.0f, 15.0f * 2.0f) );

	// 16 row
	_edges.push_back( puEdges::Edge( 4.0f * 2.0f, 16.0f * 2.0f, 7.0f * 2.0f, 16.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 8.0f * 2.0f, 16.0f * 2.0f, 12.0f * 2.0f, 16.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 13.0f * 2.0f, 16.0f * 2.0f, 16.0f * 2.0f, 16.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 17.0f * 2.0f, 16.0f * 2.0f, 24.0f * 2.0f, 16.0f * 2.0f) );


	////////////////////////////////////////////////
	// 0 col
	_edges.push_back( puEdges::Edge( 0.0f * 2.0f, 0.0f * 2.0f, 0.0f * 2.0f, 1.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 0.0f * 2.0f, 2.0f * 2.0f, 0.0f * 2.0f, 7.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 0.0f * 2.0f, 8.0f * 2.0f, 0.0f * 2.0f, 15.0f * 2.0f) );

	// 1 col
	_edges.push_back( puEdges::Edge( 1.0f * 2.0f, 2.0f * 2.0f, 1.0f * 2.0f, 6.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 1.0f * 2.0f, 8.0f * 2.0f, 1.0f * 2.0f, 12.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 1.0f * 2.0f, 13.0f * 2.0f, 1.0f * 2.0f, 14.0f * 2.0f) );

	// 2 col
	_edges.push_back( puEdges::Edge( 2.0f * 2.0f, 2.0f * 2.0f, 2.0f * 2.0f, 3.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 2.0f * 2.0f, 4.0f * 2.0f, 2.0f * 2.0f, 8.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 2.0f * 2.0f, 10.0f * 2.0f, 2.0f * 2.0f, 13.0f * 2.0f) );

	// 3 col
	_edges.push_back( puEdges::Edge( 3.0f * 2.0f, 1.0f * 2.0f, 3.0f * 2.0f, 2.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 3.0f * 2.0f, 4.0f * 2.0f, 3.0f * 2.0f, 5.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 3.0f * 2.0f, 7.0f * 2.0f, 3.0f * 2.0f, 9.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 3.0f * 2.0f, 10.0f * 2.0f, 3.0f * 2.0f, 12.0f * 2.0f) );

	// 4 col
	_edges.push_back( puEdges::Edge( 4.0f * 2.0f, 0.0f * 2.0f, 4.0f * 2.0f, 2.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 4.0f * 2.0f, 3.0f * 2.0f, 4.0f * 2.0f, 4.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 4.0f * 2.0f, 5.0f * 2.0f, 4.0f * 2.0f, 6.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 4.0f * 2.0f, 9.0f * 2.0f, 4.0f * 2.0f, 13.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 4.0f * 2.0f, 15.0f * 2.0f, 4.0f * 2.0f, 16.0f * 2.0f) );

	// 5 col
	_edges.push_back( puEdges::Edge( 5.0f * 2.0f, 1.0f * 2.0f, 5.0f * 2.0f, 2.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 5.0f * 2.0f, 7.0f * 2.0f, 5.0f * 2.0f, 8.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 5.0f * 2.0f, 10.0f * 2.0f, 5.0f * 2.0f, 11.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 5.0f * 2.0f, 13.0f * 2.0f, 5.0f * 2.0f, 15.0f * 2.0f) );

	// 6 col
	_edges.push_back( puEdges::Edge( 6.0f * 2.0f, 4.0f * 2.0f, 6.0f * 2.0f, 5.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 6.0f * 2.0f, 7.0f * 2.0f, 6.0f * 2.0f, 9.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 6.0f * 2.0f, 11.0f * 2.0f, 6.0f * 2.0f, 15.0f * 2.0f) );

	// 7 col
	_edges.push_back( puEdges::Edge( 7.0f * 2.0f, 0.0f * 2.0f, 7.0f * 2.0f, 1.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 7.0f * 2.0f, 3.0f * 2.0f, 7.0f * 2.0f, 4.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 7.0f * 2.0f, 5.0f * 2.0f, 7.0f * 2.0f, 5.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 7.0f * 2.0f, 8.0f * 2.0f, 7.0f * 2.0f, 10.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 7.0f * 2.0f, 14.0f * 2.0f, 7.0f * 2.0f, 16.0f * 2.0f) );

	// 8 col
	_edges.push_back( puEdges::Edge( 8.0f * 2.0f, 0.0f * 2.0f, 8.0f * 2.0f, 2.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 8.0f * 2.0f, 3.0f * 2.0f, 8.0f * 2.0f, 5.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 8.0f * 2.0f, 6.0f * 2.0f, 8.0f * 2.0f, 7.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 8.0f * 2.0f, 9.0f * 2.0f, 8.0f * 2.0f, 10.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 8.0f * 2.0f, 11.0f * 2.0f, 8.0f * 2.0f, 12.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 8.0f * 2.0f, 13.0f * 2.0f, 8.0f * 2.0f, 15.0f * 2.0f) );

	// 9 col
	_edges.push_back( puEdges::Edge( 9.0f * 2.0f, 0.0f * 2.0f, 9.0f * 2.0f, 1.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 9.0f * 2.0f, 3.0f * 2.0f, 9.0f * 2.0f, 7.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 9.0f * 2.0f, 8.0f * 2.0f, 9.0f * 2.0f, 9.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 9.0f * 2.0f, 10.0f * 2.0f, 9.0f * 2.0f, 12.0f * 2.0f) );

	// 10 col
	_edges.push_back( puEdges::Edge( 10.0f * 2.0f, 1.0f * 2.0f, 10.0f * 2.0f, 2.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 10.0f * 2.0f, 6.0f * 2.0f, 10.0f * 2.0f, 7.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 10.0f * 2.0f, 10.0f * 2.0f, 10.0f * 2.0f, 12.0f * 2.0f) );

	// 11 col
	_edges.push_back( puEdges::Edge( 11.0f * 2.0f, 1.0f * 2.0f, 11.0f * 2.0f, 3.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 11.0f * 2.0f, 4.0f * 2.0f, 11.0f * 2.0f, 5.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 11.0f * 2.0f, 10.0f * 2.0f, 11.0f * 2.0f, 12.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 11.0f * 2.0f, 14.0f * 2.0f, 11.0f * 2.0f, 15.0f * 2.0f) );

	// 12 col
	_edges.push_back( puEdges::Edge( 12.0f * 2.0f, 1.0f * 2.0f, 12.0f * 2.0f, 3.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 12.0f * 2.0f, 5.0f * 2.0f, 12.0f * 2.0f, 6.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 12.0f * 2.0f, 7.0f * 2.0f, 12.0f * 2.0f, 8.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 12.0f * 2.0f, 11.0f * 2.0f, 12.0f * 2.0f, 12.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 12.0f * 2.0f, 13.0f * 2.0f, 12.0f * 2.0f, 16.0f * 2.0f) );

	// 13 col
	_edges.push_back( puEdges::Edge( 13.0f * 2.0f, 2.0f * 2.0f, 13.0f * 2.0f, 3.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 13.0f * 2.0f, 5.0f * 2.0f, 13.0f * 2.0f, 6.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 13.0f * 2.0f, 7.0f * 2.0f, 13.0f * 2.0f, 8.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 13.0f * 2.0f, 9.0f * 2.0f, 13.0f * 2.0f, 10.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 13.0f * 2.0f, 11.0f * 2.0f, 13.0f * 2.0f, 12.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 13.0f * 2.0f, 13.0f * 2.0f, 13.0f * 2.0f, 16.0f * 2.0f) );

	// 14 col
	_edges.push_back( puEdges::Edge( 14.0f * 2.0f, 0.0f * 2.0f, 14.0f * 2.0f, 1.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 14.0f * 2.0f, 4.0f * 2.0f, 14.0f * 2.0f, 6.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 14.0f * 2.0f, 9.0f * 2.0f, 14.0f * 2.0f, 10.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 14.0f * 2.0f, 11.0f * 2.0f, 14.0f * 2.0f, 13.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 14.0f * 2.0f, 14.0f * 2.0f, 14.0f * 2.0f, 15.0f * 2.0f) );

	// 15 col
	_edges.push_back( puEdges::Edge( 15.0f * 2.0f, 0.0f * 2.0f, 15.0f * 2.0f, 1.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 15.0f * 2.0f, 4.0f * 2.0f, 15.0f * 2.0f, 6.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 15.0f * 2.0f, 9.0f * 2.0f, 15.0f * 2.0f, 10.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 15.0f * 2.0f, 11.0f * 2.0f, 15.0f * 2.0f, 13.0f * 2.0f) );

	// 16 col
	_edges.push_back( puEdges::Edge( 16.0f * 2.0f, 3.0f * 2.0f, 16.0f * 2.0f, 5.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 16.0f * 2.0f, 8.0f * 2.0f, 16.0f * 2.0f, 9.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 16.0f * 2.0f, 11.0f * 2.0f, 16.0f * 2.0f, 12.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 16.0f * 2.0f, 13.0f * 2.0f, 16.0f * 2.0f, 15.0f * 2.0f) );

	// 17 col
	_edges.push_back( puEdges::Edge( 17.0f * 2.0f, 3.0f * 2.0f, 17.0f * 2.0f, 7.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 17.0f * 2.0f, 8.0f * 2.0f, 17.0f * 2.0f, 10.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 17.0f * 2.0f, 11.0f * 2.0f, 17.0f * 2.0f, 12.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 17.0f * 2.0f, 13.0f * 2.0f, 17.0f * 2.0f, 16.0f * 2.0f) );

	// 18 col
	_edges.push_back( puEdges::Edge( 18.0f * 2.0f, 2.0f * 2.0f, 18.0f * 2.0f, 3.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 18.0f * 2.0f, 4.0f * 2.0f, 18.0f * 2.0f, 6.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 18.0f * 2.0f, 8.0f * 2.0f, 18.0f * 2.0f, 10.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 18.0f * 2.0f, 11.0f * 2.0f, 18.0f * 2.0f, 12.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 18.0f * 2.0f, 14.0f * 2.0f, 18.0f * 2.0f, 15.0f * 2.0f) );

	// 19 col
	_edges.push_back( puEdges::Edge( 19.0f * 2.0f, 1.0f * 2.0f, 19.0f * 2.0f, 2.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 19.0f * 2.0f, 3.0f * 2.0f, 19.0f * 2.0f, 7.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 19.0f * 2.0f, 9.0f * 2.0f, 19.0f * 2.0f, 11.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 19.0f * 2.0f, 13.0f * 2.0f, 19.0f * 2.0f, 14.0f * 2.0f) );

	// 20 col
	_edges.push_back( puEdges::Edge( 20.0f * 2.0f, 0.0f * 2.0f, 20.0f * 2.0f, 1.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 20.0f * 2.0f, 4.0f * 2.0f, 20.0f * 2.0f, 5.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 20.0f * 2.0f, 6.0f * 2.0f, 20.0f * 2.0f, 11.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 20.0f * 2.0f, 12.0f * 2.0f, 20.0f * 2.0f, 15.0f * 2.0f) );

	// 21 col
	_edges.push_back( puEdges::Edge( 21.0f * 2.0f, 0.0f * 2.0f, 21.0f * 2.0f, 1.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 21.0f * 2.0f, 2.0f * 2.0f, 21.0f * 2.0f, 4.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 21.0f * 2.0f, 8.0f * 2.0f, 21.0f * 2.0f, 9.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 21.0f * 2.0f, 10.0f * 2.0f, 21.0f * 2.0f, 15.0f * 2.0f) );

	// 22 col
	_edges.push_back( puEdges::Edge( 22.0f * 2.0f, 0.0f * 2.0f, 22.0f * 2.0f, 1.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 22.0f * 2.0f, 2.0f * 2.0f, 22.0f * 2.0f, 3.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 22.0f * 2.0f, 8.0f * 2.0f, 22.0f * 2.0f, 11.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 22.0f * 2.0f, 12.0f * 2.0f, 22.0f * 2.0f, 13.0f * 2.0f) );

	// 23 col
	_edges.push_back( puEdges::Edge( 23.0f * 2.0f, 0.0f * 2.0f, 23.0f * 2.0f, 1.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 23.0f * 2.0f, 4.0f * 2.0f, 23.0f * 2.0f, 5.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 23.0f * 2.0f, 7.0f * 2.0f, 23.0f * 2.0f, 8.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 23.0f * 2.0f, 9.0f * 2.0f, 23.0f * 2.0f, 13.0f * 2.0f) );

	// 24 col
	_edges.push_back( puEdges::Edge( 24.0f * 2.0f, 2.0f * 2.0f, 24.0f * 2.0f, 3.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 24.0f * 2.0f, 4.0f * 2.0f, 24.0f * 2.0f, 6.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 24.0f * 2.0f, 7.0f * 2.0f, 24.0f * 2.0f, 11.0f * 2.0f) );
	_edges.push_back( puEdges::Edge( 24.0f * 2.0f, 13.0f * 2.0f, 24.0f * 2.0f, 16.0f * 2.0f) );

	//Set blocks positions
	//_labyrinth.SetPosition( Tools::GetScreenMiddleX() / 2.0f / RATIO , Tools::GetScreenMiddleY() / 2.0f / RATIO );
	//_labyrinth.SetPosition( Config::GameSize.width / 2.0f / RATIO, Config::GameSize.height / 2.0f / RATIO );
	float scale;
	float x;
	float y;

	scale = 1.90f;
	_labyrinth.SetEdges( &_edges, scale );
	_labBorder.SetBorderSize( -0.75f );

	x = Config::GameSize.width / 2.0f / RATIO - Config::GameSize.width / 4.0f / RATIO * scale ;
	y = Config::GameSize.height / 2.0f / RATIO - Config::GameSize.height / 4.0f / RATIO * scale ;
	_labyrinth.SetPosition( x, y );

	_border.SetBorderSize( 20.0f );

	//_background = CCSprite::spriteWithFile( Skins::GetSkinName( "Images/Levels/bg-labirynth.png" ) );
	ConstFinal();
}
//--------------------------------------------------------------------------
void LevelLabyrinth_Imposible::SetThiefs()
{
	b2Vec2 pos;
	b2Vec2 posRand;
	float distance;
	bool posCheck;

	typedef list<b2Vec2> Positions;

	Positions positions;
	Positions::iterator it;

	//positions.push_back( _coin1.GetReversedPosition() );
	//positions.push_back( _piggyBank1.GetReversedPosition() );

	Thief *thief;
	for ( int i = 0; i < ThiefsCount; i++ )
	{
		thief = &_thiefs[i];

		posCheck = false;
		while( !posCheck )
		{
			posRand = GetRandPos();
			posCheck = true;

			for ( it = positions.begin(); it != positions.end() && posCheck; it++ )
			{
				pos = (*it);
				
				distance = sqrt( pow (( posRand.x - pos.x ), 2.0f ) + pow (( posRand.y - pos.y ), 2.0f ));
				if ( distance < MinDistanceThief2Thief )
					posCheck = false;

				pos = _coin1.GetReversedPosition();
				distance = sqrt( pow (( posRand.x - pos.x ), 2.0f ) + pow (( posRand.y - pos.y ), 2.0f ));
				if ( distance < MinDistanceThief2Coin )
					posCheck = false;

				pos = _piggyBank1.GetReversedPosition();
				distance = sqrt( pow (( posRand.x - pos.x ), 2.0f ) + pow (( posRand.y - pos.y ), 2.0f ));
				if ( distance < MinDistanceThief2Pig )
					posCheck = false;
			}
		}

		D_LOG("Rand pos to set %f %f", posRand.x, posRand.y );
		thief->SetPosition( posRand );
		thief->GetBody()->GetFixtureList()->SetFriction( 0.5f );
		thief->GetBody()->GetFixtureList()->SetRestitution( 0.6f );

		positions.push_back( posRand );
	}

}
//--------------------------------------------------------------------------
b2Vec2 LevelLabyrinth_Imposible::GetRandPos()
{
	b2Vec2 randPos;
	float x;
	float y;
	float border;

	border = 4.0f;

	x = border + ( float ) ( random() % (int) ( Config::GameSize.width / RATIO - 2.0f * border));
	y = border + ( float ) ( random() % (int) ( Config::GameSize.height / RATIO - 2.0f * border ));

	//D_LOG("Rand x: %f %y :%f", x, y );

	randPos.Set( x, y );
	return randPos;
}
//--------------------------------------------------------------------------
 
