#ifndef __LEVELMINIME_H__
#define __LEVELMINIME_H__

#include "Levels/Level.h"
#include "Blocks/Vehicals/puMiniMe.h"
//--------------------------------------------------------------------------------------------------------------------
// Vehical Base
//--------------------------------------------------------------------------------------------------------------------
class LevelVehicalBase : public Level
{
public:
	~LevelVehicalBase();
	virtual void Step( cocos2d::ccTime dt );

protected:
	LevelVehicalBase( GameTypes::LevelEnum levelEnum, bool viewMode );

protected:
	puVehical			_vehical;
};


//--------------------------------------------------------------------------------------------------------------------
// Vehical Levels
//--------------------------------------------------------------------------------------------------------------------
class LevelVehical_1 : public LevelVehicalBase
{
public:
	LevelVehical_1( GameTypes::LevelEnum levelEnum, bool viewMode );
	void CreateJoints();

private:
	puCoin< 40 >	_coin1;
	puCoin< 40 >	_coin2;
	puPiggyBank< 50 >	_piggyBank1;
	puScrew< 10 >	_screw1;
	puScrew< 10 >	_screw2;
	puWallBox<250,20,eBlockScaled>	_wallBox10;
	puWallBox<250,20,eBlockScaled>	_wallBox5;
	puWallBox<250,20,eBlockScaled>	_wallBox8;
	puWallBox<250,20,eBlockScaled>	_wallBox9;
	puWallBox<70,20,eBlockScaled>	_wallBox1;
	puWallBox<70,20,eBlockScaled>	_wallBox2;
	puWallBox<70,20,eBlockScaled>	_wallBox3;
	puWallBox<70,20,eBlockScaled>	_wallBox4;
	puWallBox<70,20,eBlockScaled>	_wallBox6;
	puWallBox<70,20,eBlockScaled>	_wallBox7;
	puWoodBox<80,20,eBlockScaled>	_woodBox1;
	puWoodBox<80,20,eBlockScaled>	_woodBox2;

	b2RevoluteJoint	*_jointRev1;
	b2RevoluteJoint	*_jointRev2;
};
//----------------------------------------------------------
class LevelVehical_2 : public LevelVehicalBase
{
public:
	LevelVehical_2( GameTypes::LevelEnum levelEnum, bool viewMode );

private:
	puCoin< 40 >	_coin1;
	puCoin< 40 >	_coin2;
	puPiggyBankStatic< 60 >	_piggyBank1;
	puPiggyBankStatic< 60 >	_piggyBank2;
	puWallBox<50,20,eBlockScaled>	_wallBox10;
	puWallBox<50,20,eBlockScaled>	_wallBox11;
	puWallBox<50,20,eBlockScaled>	_wallBox12;
	puWallBox<50,20,eBlockScaled>	_wallBox13;
	puWallBox<50,20,eBlockScaled>	_wallBox14;
	puWallBox<50,20,eBlockScaled>	_wallBox15;
	puWallBox<50,20,eBlockScaled>	_wallBox16;
	puWallBox<50,20,eBlockScaled>	_wallBox17;
	puWallBox<50,20,eBlockScaled>	_wallBox18;
	puWallBox<50,20,eBlockScaled>	_wallBox19;
	puWallBox<50,20,eBlockScaled>	_wallBox1;
	puWallBox<50,20,eBlockScaled>	_wallBox20;
	puWallBox<50,20,eBlockScaled>	_wallBox21;
	puWallBox<50,20,eBlockScaled>	_wallBox22;
	puWallBox<50,20,eBlockScaled>	_wallBox23;
	puWallBox<50,20,eBlockScaled>	_wallBox24;
	puWallBox<50,20,eBlockScaled>	_wallBox25;
	puWallBox<50,20,eBlockScaled>	_wallBox26;
	puWallBox<50,20,eBlockScaled>	_wallBox27;
	puWallBox<50,20,eBlockScaled>	_wallBox28;
	puWallBox<50,20,eBlockScaled>	_wallBox29;
	puWallBox<50,20,eBlockScaled>	_wallBox2;
	puWallBox<50,20,eBlockScaled>	_wallBox30;
	puWallBox<50,20,eBlockScaled>	_wallBox31;
	puWallBox<50,20,eBlockScaled>	_wallBox32;
	puWallBox<50,20,eBlockScaled>	_wallBox33;
	puWallBox<50,20,eBlockScaled>	_wallBox35;
	puWallBox<50,20,eBlockScaled>	_wallBox36;
	puWallBox<50,20,eBlockScaled>	_wallBox37;
	puWallBox<50,20,eBlockScaled>	_wallBox38;
	puWallBox<50,20,eBlockScaled>	_wallBox39;
	puWallBox<50,20,eBlockScaled>	_wallBox3;
	puWallBox<50,20,eBlockScaled>	_wallBox40;
	puWallBox<50,20,eBlockScaled>	_wallBox41;
	puWallBox<50,20,eBlockScaled>	_wallBox4;
	puWallBox<50,20,eBlockScaled>	_wallBox5;
	puWallBox<50,20,eBlockScaled>	_wallBox6;
	puWallBox<50,20,eBlockScaled>	_wallBox7;
	puWallBox<50,20,eBlockScaled>	_wallBox8;
	puWallBox<50,20,eBlockScaled>	_wallBox9;
	puWallBox<670,20,eBlockScaled>	_wallBox34;
};
//----------------------------------------------------------
class LevelVehical_3 : public LevelVehicalBase
{
public:
	LevelVehical_3( GameTypes::LevelEnum levelEnum, bool viewMode );

private:
	puCoin< 35 >	_coin1;
	puPiggyBank< 44 >	_piggyBank1;
	puWallBox<230,20,eBlockScaled>	_wallBox34;
	puWallBox<55,20,eBlockScaled>	_wallBox10;
	puWallBox<55,20,eBlockScaled>	_wallBox11;
	puWallBox<55,20,eBlockScaled>	_wallBox12;
	puWallBox<55,20,eBlockScaled>	_wallBox13;
	puWallBox<55,20,eBlockScaled>	_wallBox14;
	puWallBox<55,20,eBlockScaled>	_wallBox15;
	puWallBox<55,20,eBlockScaled>	_wallBox16;
	puWallBox<55,20,eBlockScaled>	_wallBox17;
	puWallBox<55,20,eBlockScaled>	_wallBox18;
	puWallBox<55,20,eBlockScaled>	_wallBox19;
	puWallBox<55,20,eBlockScaled>	_wallBox1;
	puWallBox<55,20,eBlockScaled>	_wallBox20;
	puWallBox<55,20,eBlockScaled>	_wallBox21;
	puWallBox<55,20,eBlockScaled>	_wallBox22;
	puWallBox<55,20,eBlockScaled>	_wallBox23;
	puWallBox<55,20,eBlockScaled>	_wallBox24;
	puWallBox<55,20,eBlockScaled>	_wallBox25;
	puWallBox<55,20,eBlockScaled>	_wallBox26;
	puWallBox<55,20,eBlockScaled>	_wallBox27;
	puWallBox<55,20,eBlockScaled>	_wallBox28;
	puWallBox<55,20,eBlockScaled>	_wallBox29;
	puWallBox<55,20,eBlockScaled>	_wallBox2;
	puWallBox<55,20,eBlockScaled>	_wallBox30;
	puWallBox<55,20,eBlockScaled>	_wallBox31;
	puWallBox<55,20,eBlockScaled>	_wallBox32;
	puWallBox<55,20,eBlockScaled>	_wallBox33;
	puWallBox<55,20,eBlockScaled>	_wallBox3;
	puWallBox<55,20,eBlockScaled>	_wallBox4;
	puWallBox<55,20,eBlockScaled>	_wallBox5;
	puWallBox<55,20,eBlockScaled>	_wallBox6;
	puWallBox<55,20,eBlockScaled>	_wallBox7;
	puWallBox<55,20,eBlockScaled>	_wallBox8;
	puWallBox<55,20,eBlockScaled>	_wallBox9;

};
//----------------------------------------------------------
class LevelVehical_4 : public LevelVehicalBase
{
public:
	LevelVehical_4( GameTypes::LevelEnum levelEnum, bool viewMode );
	
	virtual void Step( cocos2d::ccTime dt );
	virtual void GameCustomAction_ContactBegin( puBlock *aBlock, puBlock *bBlock, b2Contact *contact );
	virtual void GameCustomAction_ContactEnd( puBlock *aBlock, puBlock *bBlock, b2Contact *contact );

private:
	typedef enum
		{ 
			eNotingTouching			= 0x00,
			eLiftVehicalTouching	= 0x01,
			eLiftCoinTouching		= 0x02,
			eCoinVehicalTouching	= 0x04
	} TouchingStates;

	void CreateJoints();
	void SortWheel( puBlock **aBlock, puBlock **bBlock) {};

	void UpdateTouchingState();
	void UpdateWheel();
	void SetActiveWheel( puBlock *aBlock, puBlock *bBlock );

private:
	puCircleWood< 30 >	_circleWood1;
	puCoin< 20 >	_coin1;

	puPiggyBank< 20 >	_piggyBank1;
	puWallBox<110,7,eBlockScaled>	_wallBox2;
	puWallBox<130,7,eBlockScaled>	_wallBox12;
	puWallBox<20,140,eBlockScaled>	_wallBox5;
	puWallBox<210,7,eBlockScaled>	_wallBox1;
	puWallBox<210,7,eBlockScaled>	_wallBox3;
	puWallBox<41,7,eBlockScaled>	_wallBox11;
	puWallBox<55,7,eBlockScaled>	_wallBox10;
	puWallBox<55,7,eBlockScaled>	_wallBox6;
	puWallBox<55,7,eBlockScaled>	_wallBox7;
	puWallBox<55,7,eBlockScaled>	_wallBox8;
	puWallBox<55,7,eBlockScaled>	_wallBox9;
	puWallBox<65,7,eBlockScaled>	_wallBox4;
	puWoodBox<45,7,eBlockScaled>	_lift1;
	puWoodBox<65,7,eBlockScaled>	_woodBox3;
	puWoodBox<65,7,eBlockScaled>	_woodBox4;
	puWoodBox<65,7,eBlockScaled>	_woodBox5;
	puWoodBox<65,7,eBlockScaled>	_woodBox6;
	puWoodBox<65,7,eBlockScaled>	_woodBox7;
	puWoodBox<65,7,eBlockScaled>	_woodBox1;

	puFloor1	_floor;

	b2RevoluteJoint	*_jointRev1;
	b2RevoluteJoint	*_jointRev2;
	b2RevoluteJoint	*_jointRev3;
	b2RevoluteJoint	*_jointRev4;
	b2RevoluteJoint	*_jointRev5;
	b2RevoluteJoint	*_jointRev6;
	b2PrismaticJoint	*_jointPris1;
	b2RevoluteJoint	*_jointRev7;
	b2GearJoint	*_jointGear1;

	int _touchState;
	puBlock *_activeWheel;
};
//--------------------------------------------------------------------------------------------------------------------

#endif
