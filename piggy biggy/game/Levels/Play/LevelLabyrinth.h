#ifndef __LEVELLETSLABYRINTH_H__
#define __LEVELLETSLABYRINTH_H__

#include <list>
#include <Box2D/Box2D.h>
#include "Levels/LevelAccelerometr.h"
#include "Blocks/AllBlocks.h"

using namespace std;

//--------------------------------------------------------------------------------------------
class LevelLabyrinth_1 : public LevelAccelerometr
{
public:
	LevelLabyrinth_1( GameTypes::LevelEnum levelEnum, bool viewMode );

private:
	void SetThiefs();
	b2Vec2 GetRandPos();

private:
	static const int ThiefsCount	= 5;
	
	static const float MinDistanceThief2Thief;
	static const float MinDistanceThief2Pig;
	static const float MinDistanceThief2Coin;


	typedef puThiefRolled< 180, eBlockScaled > Thief;
	typedef Thief Thiefs[ThiefsCount];

private:
	puBorderLab		_labBorder;
	puEdges			_labyrinth;
	puEdges::Edges	_edges;
	puCoin< 180, eBlockScaled >			_coin1;
	puPiggyBankRolled< 180, eBlockScaled >	_piggyBank1;
	Thiefs			_thiefs;
};
//--------------------------------------------------------------------------------------------
class LevelLabyrinth_Imposible : public LevelAccelerometr
{
public:
	LevelLabyrinth_Imposible( GameTypes::LevelEnum levelEnum, bool viewMode );

private:
	void SetThiefs();
	b2Vec2 GetRandPos();

private:
	static const int ThiefsCount			= 4;
	
	static const float MinDistanceThief2Thief;
	static const float MinDistanceThief2Pig;
	static const float MinDistanceThief2Coin;

	typedef puThiefRolled< 180, eBlockScaled > Thief;
	typedef Thief Thiefs[ThiefsCount];

private:
	puBorderLab		_labBorder;
	puEdges			_labyrinth;
	puEdges::Edges	_edges;
	puCoin< 180, eBlockScaled >			_coin1;
	puPiggyBankRolled< 180, eBlockScaled >	_piggyBank1;
	Thiefs			_thiefs;
};
//--------------------------------------------------------------------------------------------
#endif
