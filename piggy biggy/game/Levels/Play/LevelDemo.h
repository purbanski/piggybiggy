#ifndef __LEVELDEMO_H__
#define __LEVELDEMO_H__

#include <Box2D/Box2D.h>
#include "Levels/Level.h"
#include "Blocks/puBox.h"
#include "Blocks/puCircle.h"

//----------------------------------------------------------------------------------------
class LevelDemo_0 : public LevelDemo
{
public:
	LevelDemo_0( GameTypes::LevelEnum levelEnum, bool viewMode );

private:
	puCoin< 40 >		_coin1;
	puPiggyBank< 60 >	_piggyBank1;

	puWallBox<55,20,eBlockScaled>	_wallBox10;
	puWallBox<55,20,eBlockScaled>	_wallBox11;
	puWallBox<55,20,eBlockScaled>	_wallBox12;
	puWallBox<55,20,eBlockScaled>	_wallBox13;
	puWallBox<55,20,eBlockScaled>	_wallBox14;
	puWallBox<55,20,eBlockScaled>	_wallBox15;
	puWallBox<55,20,eBlockScaled>	_wallBox16;
	puWallBox<55,20,eBlockScaled>	_wallBox17;
	puWallBox<55,20,eBlockScaled>	_wallBox18;
	puWallBox<55,20,eBlockScaled>	_wallBox19;
	puWallBox<55,20,eBlockScaled>	_wallBox1;
	puWallBox<55,20,eBlockScaled>	_wallBox20;
	puWallBox<55,20,eBlockScaled>	_wallBox21;
	puWallBox<55,20,eBlockScaled>	_wallBox22;
	puWallBox<55,20,eBlockScaled>	_wallBox23;
	puWallBox<55,20,eBlockScaled>	_wallBox24;
	puWallBox<55,20,eBlockScaled>	_wallBox25;
	puWallBox<55,20,eBlockScaled>	_wallBox26;
	puWallBox<55,20,eBlockScaled>	_wallBox27;
	puWallBox<55,20,eBlockScaled>	_wallBox28;
	puWallBox<55,20,eBlockScaled>	_wallBox2;
	puWallBox<55,20,eBlockScaled>	_wallBox3;
	puWallBox<55,20,eBlockScaled>	_wallBox4;
	puWallBox<55,20,eBlockScaled>	_wallBox5;
	puWallBox<55,20,eBlockScaled>	_wallBox6;
	puWallBox<55,20,eBlockScaled>	_wallBox7;
	puWallBox<55,20,eBlockScaled>	_wallBox8;
	puWallBox<55,20,eBlockScaled>	_wallBox9;
};
//----------------------------------------------------------------------------------------
class LevelDemo_1 : public LevelDemo
{
public:
	LevelDemo_1( GameTypes::LevelEnum levelEnum, bool viewMode );

private:
	puCoin< 40 >	_coin1;
	puCoin< 40 >	_coin2;
	puCop< 50 >	_cop1;
	puFragileBox<96,96,eBlockScaled>	_fragileBox1;
	puFragileBox<96,96,eBlockScaled>	_fragileBox2;
	puFragileBox<96,96,eBlockScaled>	_fragileBox3;
	puFragileBox<96,96,eBlockScaled>	_fragileBox4;
	puPiggyBank< 50 >	_piggyBank1;
	puSwingNoFloor	_swingNoFloor1;
	puSwingNoFloor	_swingNoFloor2;
	puSwingNoFloor	_swingNoFloor3;
	puSwingNoFloor	_swingNoFloor4;
	puThief< 50 >	_thief1;
	puWallBox<310,20,eBlockScaled>	_wallBox1;
	puWallBox<310,20,eBlockScaled>	_wallBox2;
	puWallBox<310,20,eBlockScaled>	_wallBox3;
	puWallBox<310,20,eBlockScaled>	_wallBox4;
	puWallBox<540,20,eBlockScaled>	_wallBox5;
};
//----------------------------------------------------------------------------------------
class LevelDemo_2 : public LevelDemo
{
public:
	LevelDemo_2( GameTypes::LevelEnum levelEnum, bool viewMode );

private:
	puCoin< 40 >	_coin1;
	puCoin< 40 >	_coin2;
	puCoin< 40 >	_coin3;
	puCoin< 40 >	_coin4;
	puCoin< 40 >	_coin5;
	puFragileBox<96,96,eBlockScaled>	_fragileBox1;
	puFragileBox<96,96,eBlockScaled>	_fragileBox2;
	puFragileBox<96,96,eBlockScaled>	_fragileBox3;
	puFragileBox<96,96,eBlockScaled>	_fragileBox4;
	puFragileBox<96,96,eBlockScaled>	_fragileBox5;
	puFragileBox<96,96,eBlockScaled>	_fragileBox6;
	puPiggyBank< 60 >	_piggyBank1;
	puWallBox<310,20,eBlockScaled>	_wallBox1;
	puWallBox<310,20,eBlockScaled>	_wallBox2;
	puWallBox<310,20,eBlockScaled>	_wallBox3;
	puWallBox<310,20,eBlockScaled>	_wallBox4;
	puWallBox<310,20,eBlockScaled>	_wallBox5;
	puWallBox<310,20,eBlockScaled>	_wallBox6;
};
//--------------------------------------------------------------------------------------------------------
class LevelDemo_3 : public LevelDemo
{
public:
	LevelDemo_3( GameTypes::LevelEnum levelEnum, bool viewMode );

private:
	puCoin<> _coin1;
	puCoin<> _coin2;

	puFragileBox<96,96,eBlockScaled>	_fragileBox1;
	puFragileBox<96,96,eBlockScaled>	_fragileBox2;
	puFragileBox<96,96,eBlockScaled>	_fragileBox3;

	puPiggyBank<>					_piggyBank1;

	puWallBox<480,20,eBlockScaled>	_wall1;
	puWallBox<480,20,eBlockScaled>	_wall2;
	puWallBox<480,20,eBlockScaled>	_wall3;
};
#endif
