#ifndef __LEVELCONSTRUCTION_H__
#define __LEVELCONSTRUCTION_H__

#include <Box2D/Box2D.h>
#include "Levels/Level.h"
#include "Blocks/puBox.h"
#include "Blocks/puCircle.h"

//-----------------------------------------------------------------------------------------
class LevelConstruction_1a : public Level
{
public:
	LevelConstruction_1a( GameTypes::LevelEnum levelEnum, bool viewMode );
private:
	puCoin< 40 >	_coin1;
	puFragileBox<96,96,eBlockScaled>	_fragileBox1;
	puFragileBox<96,96,eBlockScaled>	_fragileBox2;
	puFragileBox<96,96,eBlockScaled>	_fragileBox3;
	puFragileBox<96,96,eBlockScaled>	_fragileBox4;
	puFragileBox<96,96,eBlockScaled>	_fragileBox5;
	puFragileBox<96,96,eBlockScaled>	_fragileBox6;
	puFragileBox<96,96,eBlockScaled>	_fragileBox7;
	puFragileBox<96,96,eBlockScaled>	_fragileBox8;
	puPiggyBank< 60 >	_piggyBank1;
	puWallBox<820,20,eBlockScaled>	_wallBox1;
	puWoodBox<360,20,eBlockScaled>	_woodBox1;
};
//-----------------------------------------------------------------------------------------
class LevelConstruction_1b : public Level
{
public:
	LevelConstruction_1b( GameTypes::LevelEnum levelEnum, bool viewMode );

private:
	puCoin< 40 >	_coin1;
	puFragileBox<72,72,eBlockScaled>	_fragileBox1;
	puFragileBox<72,72,eBlockScaled>	_fragileBox2;
	puFragileBox<96,96,eBlockScaled>	_fragileBox3;
	puFragileBox<96,96,eBlockScaled>	_fragileBox4;
	puFragileBox<96,96,eBlockScaled>	_fragileBox5;
	puFragileBox<96,96,eBlockScaled>	_fragileBox6;
	puFragileBox<96,96,eBlockScaled>	_fragileBox7;
	puFragileBox<96,96,eBlockScaled>	_fragileBox8;
	puPiggyBank< 60 >	_piggyBank1;
	puWallBox<160,20,eBlockScaled>	_wallBox2;
	puWallBox<100,20,eBlockScaled>	_wallBox3;
	puWallBox<740,20,eBlockScaled>	_wallBox1;
	puWoodBox<360,20,eBlockScaled>	_woodBox1;
};
//-----------------------------------------------------------------------------------------
class LevelConstruction_3a : public Level
{
public:
	LevelConstruction_3a( GameTypes::LevelEnum levelEnum, bool viewMode );

private:
	puCoin< 40 >	_coin1;
	puCoin< 40 >	_coin2;
	puFragileBox<192,96,eBlockScaled>	_fragileBox1;
	puFragileBox<192,96,eBlockScaled>	_fragileBox2;
	puFragileBox<192,96,eBlockScaled>	_fragileBox3;
	puFragileBox<192,96,eBlockScaled>	_fragileBox4;
	puFragileBox<96,96,eBlockScaled>	_fragileBox10;
	puFragileBox<96,96,eBlockScaled>	_fragileBox11;
	puFragileBox<96,96,eBlockScaled>	_fragileBox5;
	puFragileBox<96,96,eBlockScaled>	_fragileBox6;
	puFragileBox<96,96,eBlockScaled>	_fragileBox7;
	puFragileBox<96,96,eBlockScaled>	_fragileBox8;
	puFragileBox<96,96,eBlockScaled>	_fragileBox9;
	puPiggyBank< 60 >	_piggyBank1;
	puWallBox<860,20,eBlockScaled>	_wallBox1;
};

//-----------------------------------------------------------------------------------------
class LevelConstruction_1c : public Level
{
public:
	LevelConstruction_1c( GameTypes::LevelEnum levelEnum, bool viewMode );

private:
	puCoin< 40 >	_coin1;
	puFragileBox<96,96,eBlockScaled>	_fragileBox1;
	puFragileBox<96,96,eBlockScaled>	_fragileBox2;
	puFragileBox<96,96,eBlockScaled>	_fragileBox3;
	puFragileBox<96,96,eBlockScaled>	_fragileBox4;
	puFragileBox<96,96,eBlockScaled>	_fragileBox5;
	puFragileBox<96,96,eBlockScaled>	_fragileBox6;
	puFragileBox<96,96,eBlockScaled>	_fragileBox7;
	puFragileBox<96,96,eBlockScaled>	_fragileBox8;
	puPiggyBank< 60 >	_piggyBank1;
	puThief< 40 >	_thief1;
	puThief< 40 >	_thief2;
	puThief< 40 >	_thief3;
	puWallBox<730,20,eBlockScaled>	_wallBox1;
	puWoodBox<360,20,eBlockScaled>	_woodBox1;
};
//-----------------------------------------------------------------------------------------
class LevelConstruction_1d : public Level
{
public:
	LevelConstruction_1d( GameTypes::LevelEnum levelEnum, bool viewMode );

private:
	puCoin< 40 >	_coin1;
	puCoin< 40 >	_coin2;
	puFragileBox<96,96,eBlockScaled>	_fragileBox10;
	puFragileBox<96,96,eBlockScaled>	_fragileBox11;
	puFragileBox<96,96,eBlockScaled>	_fragileBox12;
	puFragileBox<96,96,eBlockScaled>	_fragileBox13;
	puFragileBox<96,96,eBlockScaled>	_fragileBox14;
	puFragileBox<96,96,eBlockScaled>	_fragileBox15;
	puFragileBox<96,96,eBlockScaled>	_fragileBox16;
	puFragileBox<96,96,eBlockScaled>	_fragileBox1;
	puFragileBox<96,96,eBlockScaled>	_fragileBox2;
	puFragileBox<96,96,eBlockScaled>	_fragileBox3;
	puFragileBox<96,96,eBlockScaled>	_fragileBox4;
	puFragileBox<96,96,eBlockScaled>	_fragileBox5;
	puFragileBox<96,96,eBlockScaled>	_fragileBox6;
	puFragileBox<96,96,eBlockScaled>	_fragileBox7;
	puFragileBox<96,96,eBlockScaled>	_fragileBox8;
	puFragileBox<96,96,eBlockScaled>	_fragileBox9;
	puPiggyBank< 50 >	_piggyBank1;
	puThief< 50 >	_thief1;
	puThief< 50 >	_thief2;
	puWallBox<360,20,eBlockScaled>	_wallBox1;
	puWallBox<480,20,eBlockScaled>	_wallBox2;
	puWoodBox<460,20,eBlockScaled>	_woodBox2;
	puWoodBox<500,20,eBlockScaled>	_woodBox1;
};
//-----------------------------------------------------------------------------------------
class LevelConstruction_2a : public Level
{
public:
	LevelConstruction_2a( GameTypes::LevelEnum levelEnum, bool viewMode );

private:
	puCoin< 40 >	_coin1;
	puFragileBox<96,96,eBlockScaled>	_fragileBox10;
	puFragileBox<96,96,eBlockScaled>	_fragileBox11;
	puFragileBox<96,96,eBlockScaled>	_fragileBox12;
	puFragileBox<96,96,eBlockScaled>	_fragileBox13;
	puFragileBox<96,96,eBlockScaled>	_fragileBox14;
	puFragileBox<96,96,eBlockScaled>	_fragileBox15;
	puFragileBox<96,96,eBlockScaled>	_fragileBox1;
	puFragileBox<96,96,eBlockScaled>	_fragileBox2;
	puFragileBox<96,96,eBlockScaled>	_fragileBox3;
	puFragileBox<96,96,eBlockScaled>	_fragileBox4;
	puFragileBox<96,96,eBlockScaled>	_fragileBox5;
	puFragileBox<96,96,eBlockScaled>	_fragileBox6;
	puFragileBox<96,96,eBlockScaled>	_fragileBox7;
	puFragileBox<96,96,eBlockScaled>	_fragileBox8;
	puFragileBox<96,96,eBlockScaled>	_fragileBox9;
	puPiggyBank< 60 >	_piggyBank1;
	puWallBox<290,20,eBlockScaled>	_wallBox1;
	puWallBox<510,20,eBlockScaled>	_wallBox2;
	puWoodBox<460,20,eBlockScaled>	_woodBox1;
};
//-----------------------------------------------------------------------------------------
class LevelConstruction_4a : public Level
{
public:
	LevelConstruction_4a( GameTypes::LevelEnum levelEnum, bool viewMode );

private:
	puPiggyBankStatic< 60 >	_piggyBankStatic1;
	puPolygonCoin_Custom1	_polygonCoin_Custom11;
	puPolygonCoin_Custom1	_polygonCoin_Custom12;
	puPolygonCoin_Custom1	_polygonCoin_Custom13;
	puPolygonFragile_Custom1	_polygonFragile_Custom110;
	puPolygonFragile_Custom1	_polygonFragile_Custom111;
	puPolygonFragile_Custom1	_polygonFragile_Custom112;
	puPolygonFragile_Custom1	_polygonFragile_Custom11;
	puPolygonFragile_Custom1	_polygonFragile_Custom12;
	puPolygonFragile_Custom1	_polygonFragile_Custom13;
	puPolygonFragile_Custom1	_polygonFragile_Custom14;
	puPolygonFragile_Custom1	_polygonFragile_Custom15;
	puPolygonFragile_Custom1	_polygonFragile_Custom16;
	puPolygonFragile_Custom1	_polygonFragile_Custom17;
	puPolygonFragile_Custom1	_polygonFragile_Custom18;
	puPolygonFragile_Custom1	_polygonFragile_Custom19;
	puWallBox<700,20,eBlockScaled>	_wallBox1;
};
//-----------------------------------------------------------------------------------------
class LevelConstruction_4b : public Level
{
public:
	LevelConstruction_4b( GameTypes::LevelEnum levelEnum, bool viewMode );

private:
	puCoin< 40 >	_coin1;
	puFragileBoxStatic<96,96,eBlockScaled>	_fragileBoxStatic1;
	puPiggyBank< 60 >	_piggyBank1;
	puPolygonFragile_Custom2	_polygonFragile10;
	puPolygonFragile_Custom2	_polygonFragile11;
	puPolygonFragile_Custom2	_polygonFragile12;
	puPolygonFragile_Custom2	_polygonFragile13;
	puPolygonFragile_Custom2	_polygonFragile14;
	puPolygonFragile_Custom2	_polygonFragile15;
	puPolygonFragile_Custom2	_polygonFragile1;
	puPolygonFragile_Custom2	_polygonFragile2;
	puPolygonFragile_Custom2	_polygonFragile3;
	puPolygonFragile_Custom2	_polygonFragile4;
	puPolygonFragile_Custom2	_polygonFragile5;
	puPolygonFragile_Custom2	_polygonFragile6;
	puPolygonFragile_Custom2	 _polygonFragile7;
	puPolygonFragile_Custom2	_polygonFragile8;
	puPolygonFragile_Custom2	_polygonFragile9;
	puWallBox<20,20,eBlockScaled>	_wallBox1;
	puWallBox<20,20,eBlockScaled>	_wallBox2;
	puWallBox<20,20,eBlockScaled>	_wallBox3;
	puWallBox<20,20,eBlockScaled>	_wallBox4;
	puWallBox<20,20,eBlockScaled>	_wallBox5;
	puWallBox<20,20,eBlockScaled>	_wallBox6;
	puWallBox<20,20,eBlockScaled>	_wallBox7;
	puWallBox<20,20,eBlockScaled>	_wallBox8;
	puWallBox<70,20,eBlockScaled>	_wallBox9;
	puWallBox<730,20,eBlockScaled>	_wallBox10;
};
//-----------------------------------------------------------------------------------------
class LevelConstruction_4c : public Level
{
public:
	LevelConstruction_4c( GameTypes::LevelEnum levelEnum, bool viewMode );

private:
	puCoin< 40 >	_coin1;
	puFragileBox<140,100,eBlockScaled>	_fragileBox1;
	puFragileBox<140,120,eBlockScaled>	_fragileBox10;
	puFragileBox<140,120,eBlockScaled>	_fragileBox2;
	puFragileBox<140,120,eBlockScaled>	_fragileBox3;
	puFragileBox<140,140,eBlockScaled>	_fragileBox11;
	puFragileBox<140,140,eBlockScaled>	_fragileBox4;
	puFragileBox<140,140,eBlockScaled>	_fragileBox5;
	puFragileBox<140,140,eBlockScaled>	_fragileBox6;
	puFragileBox<140,60,eBlockScaled>	_fragileBox7;
	puFragileBox<160,140,eBlockScaled>	_fragileBox12;
	puFragileBox<160,140,eBlockScaled>	_fragileBox8;
	puFragileBox<220,140,eBlockScaled>	_fragileBox9;
	puPiggyBankStatic< 60 >	_piggyBankStatic1;
	puWallBox<20,20,eBlockScaled>	_wallBox1;
	puWallBox<20,20,eBlockScaled>	_wallBox2;
	puWallBox<600,20,eBlockScaled>	_wallBox3;
};
//-----------------------------------------------------------------------------------------

#endif
