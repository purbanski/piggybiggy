#ifndef __LEVELINTRO_H__
#define __LEVELINTRO_H__

#include <Box2D/Box2D.h>
#include "Levels/LevelWithCallback.h"
#include "Blocks/AllBlocks.h"

//--------------------------------------------------------------------------------------------
class LevelIntro_Welcome : public LevelActivable
{
public:
	LevelIntro_Welcome( GameTypes::LevelEnum levelEnum, bool viewMode );

private:
	puCoin< 40 >		_coin1;
	puPiggyBank< 60 >	_piggyBank1;

	puWallBox<55,20,eBlockScaled>	_wallBox10;
	puWallBox<55,20,eBlockScaled>	_wallBox11;
	puWallBox<55,20,eBlockScaled>	_wallBox12;
	puWallBox<55,20,eBlockScaled>	_wallBox13;
	puWallBox<55,20,eBlockScaled>	_wallBox14;
	puWallBox<55,20,eBlockScaled>	_wallBox15;
	puWallBox<55,20,eBlockScaled>	_wallBox16;
	puWallBox<55,20,eBlockScaled>	_wallBox17;
	puWallBox<55,20,eBlockScaled>	_wallBox18;
	puWallBox<55,20,eBlockScaled>	_wallBox19;
	puWallBox<55,20,eBlockScaled>	_wallBox1;
	puWallBox<55,20,eBlockScaled>	_wallBox20;
	puWallBox<55,20,eBlockScaled>	_wallBox21;
	puWallBox<55,20,eBlockScaled>	_wallBox22;
	puWallBox<55,20,eBlockScaled>	_wallBox23;
	puWallBox<55,20,eBlockScaled>	_wallBox24;
	puWallBox<55,20,eBlockScaled>	_wallBox25;
	puWallBox<55,20,eBlockScaled>	_wallBox26;
	puWallBox<55,20,eBlockScaled>	_wallBox27;
	puWallBox<55,20,eBlockScaled>	_wallBox28;
	puWallBox<55,20,eBlockScaled>	_wallBox2;
	puWallBox<55,20,eBlockScaled>	_wallBox3;
	puWallBox<55,20,eBlockScaled>	_wallBox4;
	puWallBox<55,20,eBlockScaled>	_wallBox5;
	puWallBox<55,20,eBlockScaled>	_wallBox6;
	puWallBox<55,20,eBlockScaled>	_wallBox7;
	puWallBox<55,20,eBlockScaled>	_wallBox8;
	puWallBox<55,20,eBlockScaled>	_wallBox9;
};
//--------------------------------------------------------------------------------------------
class LevelIntro_DestroyBlock : public LevelActivable
{
public:
	LevelIntro_DestroyBlock( GameTypes::LevelEnum levelEnum, bool viewMode );

private:
	puCoin< 40 >	_coin1;
	puFragileBox<96,96,eBlockScaled>	_fragileBox1;
	puFragileBox<96,96,eBlockScaled>	_fragileBox2;
	puFragileBox<96,96,eBlockScaled>	_fragileBox3;
	puPiggyBank< 60 >	_piggyBank1;
	puWallBox<550,20,eBlockScaled>	_wallBox1;
};
//--------------------------------------------------------------------------------------------
class LevelIntro_Gun : public Level
{
public:
	LevelIntro_Gun( GameTypes::LevelEnum levelEnum, bool viewMode );

private:
	puCoin< 40 >	_coin1;
	puCoin< 40 >	_coin2;
	puCoin< 40 >	_coin3;
	puCoin< 40 >	_coin4;
	puCoin< 40 >	_coin5;
	puCoin< 40 >	_coin6;
	puCoin< 40 >	_coin7;
	puGun	_gun1;
	puPiggyBank< 60 >	_piggyBank1;
	puWallBox<35,20,eBlockScaled>	_wallBox10;
	puWallBox<35,20,eBlockScaled>	_wallBox11;
	puWallBox<35,20,eBlockScaled>	_wallBox12;
	puWallBox<35,20,eBlockScaled>	_wallBox13;
	puWallBox<35,20,eBlockScaled>	_wallBox14;
	puWallBox<35,20,eBlockScaled>	_wallBox15;
	puWallBox<35,20,eBlockScaled>	_wallBox16;
	puWallBox<35,20,eBlockScaled>	_wallBox17;
	puWallBox<35,20,eBlockScaled>	_wallBox18;
	puWallBox<35,20,eBlockScaled>	_wallBox19;
	puWallBox<35,20,eBlockScaled>	_wallBox1;
	puWallBox<35,20,eBlockScaled>	_wallBox20;
	puWallBox<35,20,eBlockScaled>	_wallBox21;
	puWallBox<35,20,eBlockScaled>	_wallBox22;
	puWallBox<35,20,eBlockScaled>	_wallBox23;
	puWallBox<35,20,eBlockScaled>	_wallBox24;
	puWallBox<35,20,eBlockScaled>	_wallBox25;
	puWallBox<35,20,eBlockScaled>	_wallBox26;
	puWallBox<35,20,eBlockScaled>	_wallBox27;
	puWallBox<35,20,eBlockScaled>	_wallBox28;
	puWallBox<35,20,eBlockScaled>	_wallBox29;
	puWallBox<35,20,eBlockScaled>	_wallBox2;
	puWallBox<35,20,eBlockScaled>	_wallBox30;
	puWallBox<35,20,eBlockScaled>	_wallBox31;
	puWallBox<35,20,eBlockScaled>	_wallBox32;
	puWallBox<35,20,eBlockScaled>	_wallBox33;
	puWallBox<35,20,eBlockScaled>	_wallBox34;
	puWallBox<35,20,eBlockScaled>	_wallBox35;
	puWallBox<35,20,eBlockScaled>	_wallBox36;
	puWallBox<35,20,eBlockScaled>	_wallBox37;
	puWallBox<35,20,eBlockScaled>	_wallBox38;
	puWallBox<35,20,eBlockScaled>	_wallBox39;
	puWallBox<35,20,eBlockScaled>	_wallBox3;
	puWallBox<35,20,eBlockScaled>	_wallBox40;
	puWallBox<35,20,eBlockScaled>	_wallBox41;
	puWallBox<35,20,eBlockScaled>	_wallBox42;
	puWallBox<35,20,eBlockScaled>	_wallBox46;
	puWallBox<35,20,eBlockScaled>	_wallBox47;
	puWallBox<35,20,eBlockScaled>	_wallBox4;
	puWallBox<35,20,eBlockScaled>	_wallBox5;
	puWallBox<35,20,eBlockScaled>	_wallBox6;
	puWallBox<35,20,eBlockScaled>	_wallBox7;
	puWallBox<35,20,eBlockScaled>	_wallBox8;
	puWallBox<35,20,eBlockScaled>	_wallBox9;
	puWallBox<550,20,eBlockScaled>	_wallBox43;
	puWallBox<550,20,eBlockScaled>	_wallBox44;
	puWallBox<70,20,eBlockScaled>	_wallBox45;
};
//--------------------------------------------------------------------------------------------
class LevelIntro_MoneyMakeMoney : public Level
{
public:
	LevelIntro_MoneyMakeMoney( GameTypes::LevelEnum levelEnum, bool viewMode );

private:
	puCoin< 40 >	_coin1;
	puCoinSilver< 40 >	_coinSilver1;
	puFragileBoxStatic<96,96,eBlockScaled>	_fragileBoxStatic1;
	puFragileBoxStatic<96,96,eBlockScaled>	_fragileBoxStatic2;
	puPiggyBank< 60 >	_piggyBank1;
	puWallBox<550,20,eBlockScaled>	_wallBox1;
};
//--------------------------------------------------------------------------------------------
class LevelIntro_Rope : public Level
{
public:
	LevelIntro_Rope( GameTypes::LevelEnum levelEnum, bool viewMode );

private:
	puChainCutable _chain1;
	puChainCutable _chain2;
	puCoin< 40 >	_coin1;
	puPiggyBank< 60 >	_piggyBank1;
	puWallBox<70,20,eBlockScaled>	_wallBox1;
};
//--------------------------------------------------------------------------------------------

#endif
