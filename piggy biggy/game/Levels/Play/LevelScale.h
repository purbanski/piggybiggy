#ifndef __LEVELSCALE_H__
#define __LEVELSCALE_H__

#include <Box2D/Box2D.h>
#include "Levels/Level.h"
#include "Blocks/AllBlocks.h"

//-----------------------------------------------------------------
class LevelScale_1 : public Level
{
public:
	LevelScale_1( GameTypes::LevelEnum levelEnum, bool viewMode );
	~LevelScale_1();
	virtual void Quiting();

private:
	void CreateJoints();

	puCoin< 40 >	_coin1;
	puCop< 50 >		_cop1;
	puDoor_1		_door_11;
	puDoor_1		_door_12;
	puDoor_1		_door_13;
	puFragileBox<96,96,eBlockScaled>	_fragileBox1;
	puFragileBox<96,96,eBlockScaled>	_fragileBox2;
	puFragileBox<96,96,eBlockScaled>	_fragileBox3;
	puFragileBox<96,96,eBlockScaled>	_fragileBox4;
	puFragileBox<96,96,eBlockScaled>	_fragileBox5;
	puFragileBox<96,96,eBlockScaled>	_fragileBox6;
	puPiggyBank< 50 >	_piggyBank1;
	puScrew< 12 >		_screw1;
	puThief< 50 >		_thief1;
	puWallBox<20,20,eBlockScaled>	_wallBox1;
	puWallBox<20,20,eBlockScaled>	_wallBox2;
	puWallBox<20,20,eBlockScaled>	_wallBox3;
	puWallBox<20,20,eBlockScaled>	_wallBox4;
	puWallBox<20,20,eBlockScaled>	_wallBox5;
	puWallBox<380,20,eBlockScaled>	_wallBox6;
	puWallBox<380,20,eBlockScaled>	_wallBox7;
	puWoodBox<600,20,eBlockScaled>	_woodBox1;

	b2RevoluteJoint	*_jointRev1;

	puSwitchBase	_doorSwitch1;
	puSwitchBase	_doorSwitch2;
	puSwitchBase	_doorSwitch3;
};
//-----------------------------------------------------------------
#endif
