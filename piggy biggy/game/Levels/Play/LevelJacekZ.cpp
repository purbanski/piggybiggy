#include "LevelJacekZ.h"

//--------------------------------------------------------------------------
LevelJacekZ_Fastfrog::LevelJacekZ_Fastfrog( GameTypes::LevelEnum levelEnum, bool viewMode ) : Level( levelEnum, viewMode )
{
	// _bomb1
	_bomb1.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_bomb1.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_bomb1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bomb1.SetBodyType( b2_dynamicBody );

	// _circleWall1
	_circleWall1.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_circleWall1.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_circleWall1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_circleWall1.SetBodyType( b2_staticBody );

	// _coin1
	_coin1.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin1.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin1.SetBodyType( b2_dynamicBody );

	// _fragileBox1
	_fragileBox1.SetRotation( 90.0000f );
	_fragileBox1.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox1.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox1.SetBodyType( b2_dynamicBody );

	// _fragileBox2
	_fragileBox2.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox2.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox2.SetBodyType( b2_dynamicBody );

	// _fragileBox3
	_fragileBox3.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox3.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox3.SetBodyType( b2_dynamicBody );

	// _fragileBox4
	_fragileBox4.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox4.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox4.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox4.SetBodyType( b2_dynamicBody );

	// _fragileBoxStatic1
	_fragileBoxStatic1.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBoxStatic1.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBoxStatic1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBoxStatic1.SetBodyType( b2_staticBody );

	// _fragileBoxStatic2
	_fragileBoxStatic2.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBoxStatic2.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBoxStatic2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBoxStatic2.SetBodyType( b2_staticBody );

	// _fragileBoxStatic3
	_fragileBoxStatic3.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBoxStatic3.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBoxStatic3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBoxStatic3.SetBodyType( b2_staticBody );

	// _fragileBoxStatic4
	_fragileBoxStatic4.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBoxStatic4.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBoxStatic4.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBoxStatic4.SetBodyType( b2_staticBody );

	// _piggyBank1
	_piggyBank1.GetBody()->GetFixtureList()->SetDensity( 7.5000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_piggyBank1.SetBodyType( b2_dynamicBody );

	// _tennisBall1
	_tennisBall1.GetBody()->GetFixtureList()->SetDensity( 3.7500f );
	_tennisBall1.GetBody()->GetFixtureList()->SetFriction( 0.2000f );
	_tennisBall1.GetBody()->GetFixtureList()->SetRestitution( 1.0000f );
	_tennisBall1.GetBody()->SetAngularDamping(0.0500f );
	_tennisBall1.SetBodyType( b2_dynamicBody );

	// _wallBox1
	_wallBox1.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox1.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox1.SetBodyType( b2_staticBody );

	// _woodBox1
	_woodBox1.SetRotation( 90.0000f );
	_woodBox1.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox1.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_woodBox1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_woodBox1.SetBodyType( b2_dynamicBody );

	// _woodBox2
	_woodBox2.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox2.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_woodBox2.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_woodBox2.SetBodyType( b2_dynamicBody );

	// _woodBox3
	_woodBox3.SetRotation( 90.0000f );
	_woodBox3.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox3.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_woodBox3.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_woodBox3.SetBodyType( b2_dynamicBody );

	// _woodBox4
	_woodBox4.SetRotation( 90.0000f );
	_woodBox4.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox4.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_woodBox4.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_woodBox4.SetBodyType( b2_dynamicBody );

	// _woodBox5
	_woodBox5.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox5.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_woodBox5.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_woodBox5.SetBodyType( b2_dynamicBody );

	// _woodBox6
	_woodBox6.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox6.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_woodBox6.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_woodBox6.SetBodyType( b2_dynamicBody );


	//Set blocks positions
	_bomb1.SetPosition( 88.2000f, 26.2499f );
	_circleWall1.SetPosition( 69.0000f, 9.8499f );
	_coin1.SetPosition( 64.7000f, 52.7504f );
	_fragileBox1.SetPosition( 26.6000f, 18.5499f );
	_fragileBox2.SetPosition( 7.3000f, 13.7499f );
	_fragileBox3.SetPosition( 17.0000f, 13.6499f );
	_fragileBox4.SetPosition( 16.9000f, 23.2499f );
	_fragileBoxStatic1.SetPosition( 74.9000f, 29.8502f );
	_fragileBoxStatic2.SetPosition( 65.9000f, 42.3504f );
	_fragileBoxStatic3.SetPosition( 47.8000f, 37.1504f );
	_fragileBoxStatic4.SetPosition( 88.5000f, 17.1499f );
	_piggyBank1.SetPosition( 46.3000f, 24.3999f );
	_tennisBall1.SetPosition( 75.2000f, 38.6504f );
	_wallBox1.SetPosition( 48.0000f, 7.1999f );
	_woodBox1.SetPosition( 81.5000f, 18.4501f );
	_woodBox2.SetPosition( 46.4000f, 17.4999f );
	_woodBox3.SetPosition( 42.0000f, 12.3999f );
	_woodBox4.SetPosition( 50.5000f, 12.5999f );
	_woodBox5.SetPosition( 66.7000f, 47.8504f );
	_woodBox6.SetPosition( 47.9000f, 42.7504f );

	//Bombs Construction
	_bomb1.SetRange( 16.0000f );
	_bomb1.SetBombImpulse( 4412.5000f );

	ConstFinal();
}
//--------------------------------------------------------------------------
