#ifndef __LEVELLETSSHOOT_H__
#define __LEVELLETSSHOOT_H__

#include <list>
#include <vector>
#include <Box2D/Box2D.h>

#include "Levels/LevelWithCallback.h"
#include "Blocks/puBox.h"
#include "Blocks/puCircle.h"
#include "Blocks/puGun.h"
#include "Blocks/puFloor1.h"

using namespace std;

//---------------------------------------------------------------------------------------
class LevelLetsShoot_1 : public LevelActivable
{
public:
	LevelLetsShoot_1( GameTypes::LevelEnum levelEnum, bool viewMode );

private:
	puCoin< 40 >	_coin1;
	puGun	_gun1;
	puPiggyBank< 60 >	_piggyBank1;
	puWallBox<310,20,eBlockScaled>	_wallBox1;
	puWallBox<70,20,eBlockScaled>	_wallBox2;
};
//---------------------------------------------------------------------------------------
class LevelLetsShoot_2 : public Level
{
public:
	LevelLetsShoot_2( GameTypes::LevelEnum levelEnum, bool viewMode );

private:
	puBouncerBox<400,20,eBlockScaled>	_bouncerBox1;
	puCoin< 40 >	_coin1;
	puCoin< 40 >	_coin2;
	puCoin< 40 >	_coin3;
	puGun	_gun1;
	puPiggyBankStatic< 60 >	_piggyBankStatic1;
	puWallBox<100,20,eBlockScaled>	_wallBox1;
	puWallBox<310,20,eBlockScaled>	_wallBox2;
};
//---------------------------------------------------------------------------------------
class LevelLetsShoot_3 : public LevelActivable
{
public:
	LevelLetsShoot_3( GameTypes::LevelEnum levelEnum, bool viewMode );

private:
	puCoin< 40 >	_coin1;
	puGunExplosive	_gunExplosive1;
	puPiggyBank< 60 >	_piggyBank1;
	puThief< 30 >	_thief1;
	puThief< 35 >	_thief2;
	puThief< 40 >	_thief3;
	puThief< 40 >	_thief4;
	puWallBox<100,20,eBlockScaled>	_wallBox1;
	puWallBox<100,20,eBlockScaled>	_wallBox2;
	puWallBox<730,20,eBlockScaled>	_wallBox3;
};
//---------------------------------------------------------------------------------------
class LevelLetsShoot_4 : public LevelActivable
{
public:
	LevelLetsShoot_4( GameTypes::LevelEnum levelEnum, bool viewMode );

private:

	static const int ThiefCount = 4; // plus one extra just before pig
	static const int CoinCount = 5;

	typedef puThief< >		Thiefs[ThiefCount];
	typedef puCoin< >		Coins[CoinCount];
	typedef vector<puBlock *>	Blocks;

	Thiefs			_thiefs;
	Coins			_coins;
	puPiggyBank< >	_piggyBank4;
	puThief< >		_thiefLast;

	puCoin< 40 >	_coin1;
	puGun	_gun1;
	puGunExplosive	_gunExplosive1;
	puPiggyBank< 60 >	_piggyBank1;
	puWallBox<140,20,eBlockScaled>	_wallBox12;
	puWallBox<70,20,eBlockScaled>	_wallBox10;
	puWallBox<70,20,eBlockScaled>	_wallBox11;
	puWallBox<70,20,eBlockScaled>	_wallBox1;
	puWallBox<70,20,eBlockScaled>	_wallBox2;
	puWallBox<70,20,eBlockScaled>	_wallBox3;
	puWallBox<70,20,eBlockScaled>	_wallBox4;
	puWallBox<70,20,eBlockScaled>	_wallBox5;
	puWallBox<70,20,eBlockScaled>	_wallBox6;
	puWallBox<70,20,eBlockScaled>	_wallBox7;
	puWallBox<70,20,eBlockScaled>	_wallBox8;
	puWallBox<70,20,eBlockScaled>	_wallBox9;

};
//---------------------------------------------------------------------------------------
#endif
