#ifndef __LEVELBANK_H__
#define __LEVELBANK_H__

#include <Box2D/Box2D.h>
#include "Levels/LevelWithCallback.h"
#include "Blocks/puBankSaveWheel.h"
#include "Blocks/puBankLock.h"
#include "Blocks/puBank.h"

//---------------------------------------------------------------------------------------------
class LevelBankBase : public LevelWithCallback
{
public:
	LevelBankBase( GameTypes::LevelEnum levelEnum, bool viewMode, BankLockCombination *lockComb );
	virtual void CounterCallback();

protected:
	puBank					_bank;
	puBankLock				_bankLock;
};
//---------------------------------------------------------------------------------------------
class LevelBank_1 : public LevelBankBase
{
public:
	LevelBank_1( GameTypes::LevelEnum levelEnum, bool viewMode );
private:
	BankLockCombination_1	_lockCombination;
};
//---------------------------------------------------------------------------------------------
class LevelBank_2 : public LevelBankBase
{
public:
	LevelBank_2( GameTypes::LevelEnum levelEnum, bool viewMode ) : LevelBankBase( levelEnum, viewMode, &_lockCombination ) {}
private:
	BankLockCombination_2	_lockCombination;
};
//---------------------------------------------------------------------------------------------
class LevelBank_3 : public LevelBankBase
{
public:
	LevelBank_3( GameTypes::LevelEnum levelEnum, bool viewMode ) : LevelBankBase( levelEnum, viewMode, &_lockCombination ) {}
private:
	BankLockCombination_3	_lockCombination;
};
//---------------------------------------------------------------------------------------------
class LevelBank_4 : public LevelBankBase
{
public:
	LevelBank_4( GameTypes::LevelEnum levelEnum, bool viewMode ) : LevelBankBase( levelEnum, viewMode, &_lockCombination ) 
	{
		_bankLock.HideLocks();
	}
private:
	BankLockCombination_4	_lockCombination;
};
//---------------------------------------------------------------------------------------------
class LevelBank_5 : public LevelBankBase
{
public:
	LevelBank_5( GameTypes::LevelEnum levelEnum, bool viewMode ) : LevelBankBase( levelEnum, viewMode, &_lockCombination ) 
	{
		_bankLock.HideLocks();
	}
private:
	BankLockCombination_5	_lockCombination;
};
//---------------------------------------------------------------------------------------------
#endif
