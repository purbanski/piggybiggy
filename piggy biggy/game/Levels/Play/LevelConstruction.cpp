#include "LevelConstruction.h"

//--------------------------------------------------------------------------
LevelConstruction_1a::LevelConstruction_1a( GameTypes::LevelEnum levelEnum, bool viewMode ) : Level( levelEnum, viewMode )
{
    _levelTag = eLevelTag_Frigle;
    
	// _coin1
	_coin1.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin1.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin1.SetBodyType( b2_dynamicBody );

	// _fragileBox1
	_fragileBox1.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox1.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_fragileBox1.GetBody()->GetFixtureList()->SetRestitution( 0.0000f );
	_fragileBox1.SetBodyType( b2_dynamicBody );

	// _fragileBox2
	_fragileBox2.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox2.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_fragileBox2.GetBody()->GetFixtureList()->SetRestitution( 0.0000f );
	_fragileBox2.SetBodyType( b2_dynamicBody );

	// _fragileBox3
	_fragileBox3.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox3.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_fragileBox3.GetBody()->GetFixtureList()->SetRestitution( 0.0000f );
	_fragileBox3.SetBodyType( b2_dynamicBody );

	// _fragileBox4
	_fragileBox4.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox4.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_fragileBox4.GetBody()->GetFixtureList()->SetRestitution( 0.0000f );
	_fragileBox4.SetBodyType( b2_dynamicBody );

	// _fragileBox5
	_fragileBox5.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox5.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_fragileBox5.GetBody()->GetFixtureList()->SetRestitution( 0.0000f );
	_fragileBox5.SetBodyType( b2_dynamicBody );

	// _fragileBox6
	_fragileBox6.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox6.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_fragileBox6.GetBody()->GetFixtureList()->SetRestitution( 0.0000f );
	_fragileBox6.SetBodyType( b2_dynamicBody );

	// _fragileBox7
	_fragileBox7.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox7.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_fragileBox7.GetBody()->GetFixtureList()->SetRestitution( 0.0000f );
	_fragileBox7.SetBodyType( b2_dynamicBody );

	// _fragileBox8
	_fragileBox8.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox8.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_fragileBox8.GetBody()->GetFixtureList()->SetRestitution( 0.0000f );
	_fragileBox8.SetBodyType( b2_dynamicBody );

	// _piggyBank1
	_piggyBank1.FlipX();
	_piggyBank1.GetBody()->GetFixtureList()->SetDensity( 7.5000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_piggyBank1.SetBodyType( b2_dynamicBody );

	// _wallBox1
	_wallBox1.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox1.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox1.SetBodyType( b2_staticBody );

	// _woodBox1
	_woodBox1.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox1.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_woodBox1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_woodBox1.SetBodyType( b2_dynamicBody );


	//Set blocks positions
	_coin1.SetPosition( 54.945f, 60.0000f );
	_fragileBox1.SetPosition( 68.9496f, 12.8499f );
	_fragileBox2.SetPosition( 68.9496f, 32.8503f );
	_fragileBox3.SetPosition( 64.9496f, 43.0503f );
	_fragileBox4.SetPosition( 64.9496f, 22.8499f );
	_fragileBox5.SetPosition( 44.9501f, 22.8499f );
	_fragileBox6.SetPosition( 48.9501f, 32.8503f );
	_fragileBox7.SetPosition( 44.9501f, 43.0503f );
	_fragileBox8.SetPosition( 48.9501f, 12.8499f );
	_piggyBank1.SetPosition( 30.5501f, 14.9999f );
	_wallBox1.SetPosition( 48.0000f, 6.7999f );
	_woodBox1.SetPosition( 55.1500f, 51.6503f );

	ConstFinal();
}
//--------------------------------------------------------------------------
LevelConstruction_1b::LevelConstruction_1b( GameTypes::LevelEnum levelEnum, bool viewMode ) : Level( levelEnum, viewMode )
{
    _levelTag = eLevelTag_Frigle;
    
	// _coin1
	_coin1.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin1.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin1.SetBodyType( b2_dynamicBody );

	// _wallBox1
	_wallBox1.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox1.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox1.SetBodyType( b2_staticBody );

	// _fragileBox1
	_fragileBox1.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox1.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox1.SetBodyType( b2_dynamicBody );

	// _fragileBox2
	_fragileBox2.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox2.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox2.SetBodyType( b2_dynamicBody );

	// _fragileBox3
	_fragileBox3.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox3.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_fragileBox3.GetBody()->GetFixtureList()->SetRestitution( 0.0000f );
	_fragileBox3.SetBodyType( b2_dynamicBody );

	// _fragileBox4
	_fragileBox4.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox4.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_fragileBox4.GetBody()->GetFixtureList()->SetRestitution( 0.0000f );
	_fragileBox4.SetBodyType( b2_dynamicBody );

	// _fragileBox5
	_fragileBox5.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox5.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_fragileBox5.GetBody()->GetFixtureList()->SetRestitution( 0.0000f );
	_fragileBox5.SetBodyType( b2_dynamicBody );

	// _fragileBox6
	_fragileBox6.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox6.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_fragileBox6.GetBody()->GetFixtureList()->SetRestitution( 0.0000f );
	_fragileBox6.SetBodyType( b2_dynamicBody );

	// _fragileBox7
	_fragileBox7.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox7.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox7.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox7.SetBodyType( b2_dynamicBody );

	// _fragileBox8
	_fragileBox8.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox8.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox8.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox8.SetBodyType( b2_dynamicBody );

	// _piggyBank1
	_piggyBank1.GetBody()->GetFixtureList()->SetDensity( 7.5000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_piggyBank1.FlipX();
	_piggyBank1.SetBodyType( b2_dynamicBody );

	// _wallBox2
	_wallBox2.SetRotation( -90.0000f );
	_wallBox2.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox2.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox2.SetBodyType( b2_staticBody );

	// _wallBox3
	_wallBox3.SetRotation( -90.0000f );
	_wallBox3.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox3.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox3.SetBodyType( b2_staticBody );

	// _woodBox1
	_woodBox1.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox1.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_woodBox1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_woodBox1.SetBodyType( b2_dynamicBody );


	//Set blocks positions
	_coin1.SetPosition( 65.2001f, 57.5f );
	_wallBox1.SetPosition( 48.0000f, 7.9000f );
	_fragileBox1.SetPosition( 76.2000f, 44.3501f );
	_fragileBox2.SetPosition( 54.2000f, 44.7001f );
	_fragileBox3.SetPosition( 76.2000f, 14.5001f );
	_fragileBox4.SetPosition( 76.2000f, 24.7000f );
	_fragileBox5.SetPosition( 76.2000f, 35.1001f );
	_fragileBox6.SetPosition( 54.2000f, 14.7502f );
	_fragileBox7.SetPosition( 54.2000f, 35.4500f );
	_fragileBox8.SetPosition( 54.2000f, 25.0500f );
	_piggyBank1.SetPosition( 20.2000f, 16.1001f );
	_wallBox2.SetPosition( 36.7000f, 18.3501f );
	_wallBox3.SetPosition( 36.7000f, 41.70f );
	_woodBox1.SetPosition( 65.2001f, 51.0491f );

	ConstFinal();
}
//--------------------------------------------------------------------------
LevelConstruction_3a::LevelConstruction_3a( GameTypes::LevelEnum levelEnum, bool viewMode ) : Level( levelEnum, viewMode )
{
	_coinCount = 2;
    _levelTag = eLevelTag_Frigle;
    
	// _coin1
	_coin1.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin1.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin1.GetBody()->SetAngularDamping(0.3000f );
	_coin1.GetBody()->SetLinearDamping(0.3000f );
	_coin1.SetBodyType( b2_dynamicBody );

	// _coin2
	_coin2.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin2.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_coin2.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin2.GetBody()->SetAngularDamping(0.2000f );
	_coin2.GetBody()->SetLinearDamping(0.4000f );
	_coin2.SetBodyType( b2_dynamicBody );

	// _fragileBox1
	_fragileBox1.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox1.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox1.SetBodyType( b2_dynamicBody );

	// _fragileBox2
	_fragileBox2.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox2.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_fragileBox2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox2.SetBodyType( b2_dynamicBody );

	// _fragileBox3
	_fragileBox3.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox3.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox3.SetBodyType( b2_dynamicBody );

	// _fragileBox4
	_fragileBox4.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox4.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox4.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox4.SetBodyType( b2_dynamicBody );

	// _fragileBox5
	_fragileBox5.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox5.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox5.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox5.SetBodyType( b2_dynamicBody );

	// _fragileBox6
	_fragileBox6.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox6.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox6.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox6.SetBodyType( b2_dynamicBody );

	// _fragileBox7
	_fragileBox7.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox7.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox7.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox7.SetBodyType( b2_dynamicBody );

	// _fragileBox8
	_fragileBox8.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox8.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox8.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox8.SetBodyType( b2_dynamicBody );

	// _fragileBox9
	_fragileBox9.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox9.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox9.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox9.SetBodyType( b2_dynamicBody );

	// _fragileBox10
	_fragileBox10.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox10.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox10.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox10.SetBodyType( b2_dynamicBody );

	// _fragileBox11
	_fragileBox11.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox11.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox11.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox11.SetBodyType( b2_dynamicBody );

	// _piggyBank1
	_piggyBank1.GetBody()->GetFixtureList()->SetDensity( 7.5000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_piggyBank1.GetBody()->SetAngularDamping(0.4000f );
	_piggyBank1.GetBody()->SetLinearDamping(0.4000f );
	_piggyBank1.SetBodyType( b2_dynamicBody );

	// _wallBox1
	_wallBox1.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox1.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox1.SetBodyType( b2_staticBody );


	//Set blocks positions
	_coin1.SetPosition( 15.2501f, 57.5501f );
	_coin2.SetPosition( 83.9501f, 38.1501f );
	_fragileBox1.SetPosition( 15.2501f, 45.4501f );
	_fragileBox2.SetPosition( 48.8500f, 25.6499f );
	_fragileBox3.SetPosition( 78.7501f, 25.6499f );
	_fragileBox4.SetPosition( 15.2501f, 25.5499f );
	_fragileBox5.SetPosition( 54.2501f, 15.6501f );
	_fragileBox6.SetPosition( 74.0500f, 15.4501f );
	_fragileBox7.SetPosition( 44.4501f, 35.8501f );
	_fragileBox8.SetPosition( 84.5500f, 15.4501f );
	_fragileBox9.SetPosition( 43.5500f, 15.5501f );
	_fragileBox10.SetPosition( 15.4501f, 15.5501f );
	_fragileBox11.SetPosition( 15.2501f, 35.5501f );
	_piggyBank1.SetPosition( 55.1501f, 36.8501f );
	_wallBox1.SetPosition( 48.0000f, 8.7500f );

	ConstFinal();
}
//--------------------------------------------------------------------------
LevelConstruction_1c::LevelConstruction_1c( GameTypes::LevelEnum levelEnum, bool viewMode ) : Level( levelEnum, viewMode )
{
    _levelTag = eLevelTag_Hacker;
    
	// _coin1
	_coin1.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin1.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin1.SetBodyType( b2_dynamicBody );

	// _fragileBox1
	_fragileBox1.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox1.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_fragileBox1.GetBody()->GetFixtureList()->SetRestitution( 0.0000f );
	_fragileBox1.SetBodyType( b2_dynamicBody );

	// _fragileBox2
	_fragileBox2.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox2.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_fragileBox2.GetBody()->GetFixtureList()->SetRestitution( 0.0000f );
	_fragileBox2.SetBodyType( b2_dynamicBody );

	// _fragileBox3
	_fragileBox3.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox3.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_fragileBox3.GetBody()->GetFixtureList()->SetRestitution( 0.0000f );
	_fragileBox3.SetBodyType( b2_dynamicBody );

	// _fragileBox4
	_fragileBox4.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox4.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_fragileBox4.GetBody()->GetFixtureList()->SetRestitution( 0.0000f );
	_fragileBox4.SetBodyType( b2_dynamicBody );

	// _fragileBox5
	_fragileBox5.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox5.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_fragileBox5.GetBody()->GetFixtureList()->SetRestitution( 0.0000f );
	_fragileBox5.SetBodyType( b2_dynamicBody );

	// _fragileBox6
	_fragileBox6.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox6.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_fragileBox6.GetBody()->GetFixtureList()->SetRestitution( 0.0000f );
	_fragileBox6.SetBodyType( b2_dynamicBody );

	// _fragileBox7
	_fragileBox7.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox7.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_fragileBox7.GetBody()->GetFixtureList()->SetRestitution( 0.0000f );
	_fragileBox7.SetBodyType( b2_dynamicBody );

	// _fragileBox8
	_fragileBox8.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox8.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_fragileBox8.GetBody()->GetFixtureList()->SetRestitution( 0.0000f );
	_fragileBox8.SetBodyType( b2_dynamicBody );

	// _piggyBank1
	_piggyBank1.GetBody()->GetFixtureList()->SetDensity( 7.5000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_piggyBank1.FlipX();
	_piggyBank1.SetBodyType( b2_dynamicBody );

	// _wallBox1
	_wallBox1.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox1.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox1.SetBodyType( b2_staticBody );

	// _woodBox1
	_woodBox1.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox1.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_woodBox1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_woodBox1.SetBodyType( b2_dynamicBody );

	// _thief1
	_thief1.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_thief1.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_thief1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_thief1.SetBodyType( b2_dynamicBody );

	// _thief2
	_thief2.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_thief2.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_thief2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_thief2.SetBodyType( b2_dynamicBody );

	// _thief3
	_thief3.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_thief3.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_thief3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_thief3.SetBodyType( b2_dynamicBody );


	//Set blocks positions
	_coin1.SetPosition( 63.1801f, 55.5182f );
	_fragileBox1.SetPosition( 74.9999f, 14.1499f );
	_fragileBox2.SetPosition( 75.4105f, 33.9898f );
	_fragileBox3.SetPosition( 71.5705f, 44.3046f );
	_fragileBox4.SetPosition( 70.9999f, 24.1499f );
	_fragileBox5.SetPosition( 50.9996f, 24.1499f );
	_fragileBox6.SetPosition( 55.4102f, 33.9898f );
	_fragileBox7.SetPosition( 51.5702f, 43.9046f );
	_fragileBox8.SetPosition( 54.9996f, 14.1499f );
	_piggyBank1.SetPosition( 22.5563f, 15.1089f );
	_wallBox1.SetPosition( 48.0000f, 6.7999f );
	_woodBox1.SetPosition( 61.5800f, 50.0684f );
	_thief1.SetPosition( 38.0999f, 12.7500f );
	_thief2.SetPosition( 38.0999f, 20.7500f );
	_thief3.SetPosition( 38.0999f, 28.7500f );

	ConstFinal();
}
//--------------------------------------------------------------------------
LevelConstruction_4a::LevelConstruction_4a( GameTypes::LevelEnum levelEnum, bool viewMode ) : Level( levelEnum, viewMode )
{
	_coinCount = 3;
    _levelTag = eLevelTag_Frigle;
    
	// _piggyBankStatic1
	_piggyBankStatic1.GetBody()->GetFixtureList()->SetDensity( 7.5000f );
	_piggyBankStatic1.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_piggyBankStatic1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_piggyBankStatic1.GetBody()->SetLinearDamping(2.0000f );
	_piggyBankStatic1.FlipX();
	_piggyBankStatic1.SetBodyType( b2_staticBody );

	// _polygonCoin_Custom11
	_polygonCoin_Custom11.GetBody()->GetFixtureList()->SetDensity( 3.4000f );
	_polygonCoin_Custom11.GetBody()->GetFixtureList()->SetFriction( 1.0000f );
	_polygonCoin_Custom11.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_polygonCoin_Custom11.SetBodyType( b2_dynamicBody );

	// _polygonCoin_Custom12
	_polygonCoin_Custom12.GetBody()->GetFixtureList()->SetDensity( 3.4000f );
	_polygonCoin_Custom12.GetBody()->GetFixtureList()->SetFriction( 1.0000f );
	_polygonCoin_Custom12.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_polygonCoin_Custom12.SetBodyType( b2_dynamicBody );

	// _polygonCoin_Custom13
	_polygonCoin_Custom13.GetBody()->GetFixtureList()->SetDensity( 3.4000f );
	_polygonCoin_Custom13.GetBody()->GetFixtureList()->SetFriction( 1.0000f );
	_polygonCoin_Custom13.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_polygonCoin_Custom13.SetBodyType( b2_dynamicBody );

	// _polygonFragile_Custom11
	_polygonFragile_Custom11.GetBody()->GetFixtureList()->SetDensity( 3.4000f );
	_polygonFragile_Custom11.GetBody()->GetFixtureList()->SetFriction( 1.0000f );
	_polygonFragile_Custom11.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_polygonFragile_Custom11.SetBodyType( b2_dynamicBody );

	// _polygonFragile_Custom12
	_polygonFragile_Custom12.GetBody()->GetFixtureList()->SetDensity( 3.4000f );
	_polygonFragile_Custom12.GetBody()->GetFixtureList()->SetFriction( 1.0000f );
	_polygonFragile_Custom12.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_polygonFragile_Custom12.SetBodyType( b2_dynamicBody );

	// _polygonFragile_Custom13
	_polygonFragile_Custom13.GetBody()->GetFixtureList()->SetDensity( 3.4000f );
	_polygonFragile_Custom13.GetBody()->GetFixtureList()->SetFriction( 1.0000f );
	_polygonFragile_Custom13.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_polygonFragile_Custom13.SetBodyType( b2_dynamicBody );

	// _polygonFragile_Custom14
	_polygonFragile_Custom14.GetBody()->GetFixtureList()->SetDensity( 3.4000f );
	_polygonFragile_Custom14.GetBody()->GetFixtureList()->SetFriction( 1.0000f );
	_polygonFragile_Custom14.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_polygonFragile_Custom14.SetBodyType( b2_dynamicBody );

	// _polygonFragile_Custom15
	_polygonFragile_Custom15.GetBody()->GetFixtureList()->SetDensity( 3.4000f );
	_polygonFragile_Custom15.GetBody()->GetFixtureList()->SetFriction( 1.0000f );
	_polygonFragile_Custom15.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_polygonFragile_Custom15.SetBodyType( b2_dynamicBody );

	// _polygonFragile_Custom16
	_polygonFragile_Custom16.GetBody()->GetFixtureList()->SetDensity( 3.4000f );
	_polygonFragile_Custom16.GetBody()->GetFixtureList()->SetFriction( 1.0000f );
	_polygonFragile_Custom16.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_polygonFragile_Custom16.SetBodyType( b2_dynamicBody );

	// _polygonFragile_Custom17
	_polygonFragile_Custom17.GetBody()->GetFixtureList()->SetDensity( 3.4000f );
	_polygonFragile_Custom17.GetBody()->GetFixtureList()->SetFriction( 1.0000f );
	_polygonFragile_Custom17.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_polygonFragile_Custom17.SetBodyType( b2_dynamicBody );

	// _polygonFragile_Custom18
	_polygonFragile_Custom18.GetBody()->GetFixtureList()->SetDensity( 3.4000f );
	_polygonFragile_Custom18.GetBody()->GetFixtureList()->SetFriction( 1.0000f );
	_polygonFragile_Custom18.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_polygonFragile_Custom18.SetBodyType( b2_dynamicBody );

	// _polygonFragile_Custom19
	_polygonFragile_Custom19.GetBody()->GetFixtureList()->SetDensity( 3.4000f );
	_polygonFragile_Custom19.GetBody()->GetFixtureList()->SetFriction( 1.0000f );
	_polygonFragile_Custom19.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_polygonFragile_Custom19.SetBodyType( b2_dynamicBody );

	// _polygonFragile_Custom110
	_polygonFragile_Custom110.GetBody()->GetFixtureList()->SetDensity( 3.4000f );
	_polygonFragile_Custom110.GetBody()->GetFixtureList()->SetFriction( 1.0000f );
	_polygonFragile_Custom110.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_polygonFragile_Custom110.SetBodyType( b2_dynamicBody );

	// _polygonFragile_Custom111
	_polygonFragile_Custom111.GetBody()->GetFixtureList()->SetDensity( 3.4000f );
	_polygonFragile_Custom111.GetBody()->GetFixtureList()->SetFriction( 1.0000f );
	_polygonFragile_Custom111.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_polygonFragile_Custom111.SetBodyType( b2_dynamicBody );

	// _polygonFragile_Custom112
	_polygonFragile_Custom112.GetBody()->GetFixtureList()->SetDensity( 3.4000f );
	_polygonFragile_Custom112.GetBody()->GetFixtureList()->SetFriction( 1.0000f );
	_polygonFragile_Custom112.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_polygonFragile_Custom112.SetBodyType( b2_dynamicBody );

	// _wallBox1
	_wallBox1.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox1.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox1.SetBodyType( b2_staticBody );


	//Set blocks positions
	_piggyBankStatic1.SetPosition( 9.2500f, 11.6001f );
	_polygonCoin_Custom11.SetPosition( 74.7499f, 34.1500f );
	_polygonCoin_Custom12.SetPosition( 68.5499f, 44.7500f );
	_polygonCoin_Custom13.SetPosition( 62.3499f, 55.3499f );
	_polygonFragile_Custom11.SetPosition( 62.3499f, 12.9501f );
	_polygonFragile_Custom12.SetPosition( 74.7499f, 12.9501f );
	_polygonFragile_Custom13.SetPosition( 87.1499f, 12.9501f );
	_polygonFragile_Custom14.SetPosition( 43.7500f, 23.5500f );
	_polygonFragile_Custom15.SetPosition( 56.1500f, 23.5500f );
	_polygonFragile_Custom16.SetPosition( 68.5499f, 23.5500f );
	_polygonFragile_Custom17.SetPosition( 80.9499f, 23.5500f );
	_polygonFragile_Custom18.SetPosition( 49.9500f, 34.1500f );
	_polygonFragile_Custom19.SetPosition( 62.3499f, 34.1500f );
	_polygonFragile_Custom110.SetPosition( 56.1500f, 44.7500f );
	_polygonFragile_Custom111.SetPosition( 37.5500f, 12.9501f );
	_polygonFragile_Custom112.SetPosition( 49.9500f, 12.9501f );
	_wallBox1.SetPosition( 56.1000f, 4.7500f );

	ConstFinal();
}
//--------------------------------------------------------------------------
LevelConstruction_4b::LevelConstruction_4b( GameTypes::LevelEnum levelEnum, bool viewMode ) : Level( levelEnum, viewMode )
{
    _levelTag = eLevelTag_Frigle;
    
	// _coin1
	_coin1.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin1.GetBody()->GetFixtureList()->SetFriction( 0.9000f );
	_coin1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin1.GetBody()->SetAngularDamping(0.1000f );
	_coin1.GetBody()->SetLinearDamping(0.1000f );
	_coin1.SetBodyType( b2_dynamicBody );

	// _fragileBoxStatic1
	_fragileBoxStatic1.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBoxStatic1.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBoxStatic1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBoxStatic1.SetBodyType( b2_staticBody );

	// _piggyBank1
	_piggyBank1.GetBody()->GetFixtureList()->SetDensity( 7.5000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_piggyBank1.SetBodyType( b2_dynamicBody );

	// _polygonFragile1
	_polygonFragile1.SetRotation( -90.0000f );
	_polygonFragile1.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_polygonFragile1.GetBody()->GetFixtureList()->SetFriction( 0.9000f );
	_polygonFragile1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_polygonFragile1.SetBodyType( b2_dynamicBody );

	// _polygonFragile2
	_polygonFragile2.SetRotation( -90.0000f );
	_polygonFragile2.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_polygonFragile2.GetBody()->GetFixtureList()->SetFriction( 0.9000f );
	_polygonFragile2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_polygonFragile2.SetBodyType( b2_dynamicBody );

	// _polygonFragile3
	_polygonFragile3.SetRotation( -90.0000f );
	_polygonFragile3.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_polygonFragile3.GetBody()->GetFixtureList()->SetFriction( 0.9000f );
	_polygonFragile3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_polygonFragile3.SetBodyType( b2_dynamicBody );

	// _polygonFragile4
	_polygonFragile4.SetRotation( -90.0000f );
	_polygonFragile4.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_polygonFragile4.GetBody()->GetFixtureList()->SetFriction( 0.9000f );
	_polygonFragile4.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_polygonFragile4.SetBodyType( b2_dynamicBody );

	// _polygonFragile5
	_polygonFragile5.SetRotation( -90.0000f );
	_polygonFragile5.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_polygonFragile5.GetBody()->GetFixtureList()->SetFriction( 0.9000f );
	_polygonFragile5.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_polygonFragile5.SetBodyType( b2_dynamicBody );

	// _polygonFragile6
	_polygonFragile6.SetRotation( -90.0000f );
	_polygonFragile6.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_polygonFragile6.GetBody()->GetFixtureList()->SetFriction( 0.9000f );
	_polygonFragile6.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_polygonFragile6.SetBodyType( b2_dynamicBody );

	// _polygonFragile7
	_polygonFragile7.SetRotation( -90.0000f );
	_polygonFragile7.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_polygonFragile7.GetBody()->GetFixtureList()->SetFriction( 0.9000f );
	_polygonFragile7.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_polygonFragile7.SetBodyType( b2_dynamicBody );

	// _polygonFragile8
	_polygonFragile8.SetRotation( -90.0000f );
	_polygonFragile8.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_polygonFragile8.GetBody()->GetFixtureList()->SetFriction( 0.9000f );
	_polygonFragile8.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_polygonFragile8.SetBodyType( b2_dynamicBody );

	// _polygonFragile9
	_polygonFragile9.SetRotation( -90.0000f );
	_polygonFragile9.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_polygonFragile9.GetBody()->GetFixtureList()->SetFriction( 0.9000f );
	_polygonFragile9.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_polygonFragile9.SetBodyType( b2_dynamicBody );

	// _polygonFragile10
	_polygonFragile10.SetRotation( -90.0000f );
	_polygonFragile10.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_polygonFragile10.GetBody()->GetFixtureList()->SetFriction( 0.9000f );
	_polygonFragile10.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_polygonFragile10.SetBodyType( b2_dynamicBody );

	// _polygonFragile11
	_polygonFragile11.SetRotation( -90.0000f );
	_polygonFragile11.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_polygonFragile11.GetBody()->GetFixtureList()->SetFriction( 0.9000f );
	_polygonFragile11.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_polygonFragile11.SetBodyType( b2_dynamicBody );

	// _polygonFragile12
	_polygonFragile12.SetRotation( -90.0000f );
	_polygonFragile12.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_polygonFragile12.GetBody()->GetFixtureList()->SetFriction( 0.9000f );
	_polygonFragile12.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_polygonFragile12.SetBodyType( b2_dynamicBody );

	// _polygonFragile13
	_polygonFragile13.SetRotation( -90.0000f );
	_polygonFragile13.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_polygonFragile13.GetBody()->GetFixtureList()->SetFriction( 0.9000f );
	_polygonFragile13.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_polygonFragile13.SetBodyType( b2_dynamicBody );

	// _polygonFragile14
	_polygonFragile14.SetRotation( -90.0000f );
	_polygonFragile14.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_polygonFragile14.GetBody()->GetFixtureList()->SetFriction( 0.9000f );
	_polygonFragile14.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_polygonFragile14.SetBodyType( b2_dynamicBody );

	// _polygonFragile15
	_polygonFragile15.SetRotation( -90.0000f );
	_polygonFragile15.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_polygonFragile15.GetBody()->GetFixtureList()->SetFriction( 0.9000f );
	_polygonFragile15.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_polygonFragile15.SetBodyType( b2_dynamicBody );

	// _wallBox1
	_wallBox1.SetRotation( 90.0000f );
	_wallBox1.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox1.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox1.SetBodyType( b2_staticBody );

	// _wallBox2
	_wallBox2.SetRotation( 102.6000f );
	_wallBox2.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox2.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox2.SetBodyType( b2_staticBody );

	// _wallBox3
	_wallBox3.SetRotation( 115.2000f );
	_wallBox3.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox3.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox3.SetBodyType( b2_staticBody );

	// _wallBox4
	_wallBox4.SetRotation( 127.8000f );
	_wallBox4.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox4.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox4.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox4.SetBodyType( b2_staticBody );

	// _wallBox5
	_wallBox5.SetRotation( 140.4000f );
	_wallBox5.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox5.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox5.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox5.SetBodyType( b2_staticBody );

	// _wallBox6
	_wallBox6.SetRotation( 153.0000f );
	_wallBox6.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox6.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox6.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox6.SetBodyType( b2_staticBody );

	// _wallBox7
	_wallBox7.SetRotation( 165.6000f );
	_wallBox7.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox7.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox7.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox7.SetBodyType( b2_staticBody );

	// _wallBox8
	_wallBox8.SetRotation( 178.2000f );
	_wallBox8.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox8.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox8.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox8.SetBodyType( b2_staticBody );

	// _wallBox9
	_wallBox9.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox9.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox9.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox9.SetBodyType( b2_staticBody );

	// _wallBox10
	_wallBox10.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox10.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox10.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox10.SetBodyType( b2_staticBody );


	//Set blocks positions
	_coin1.SetPosition( 8.1500f, 67.1002f );
	_fragileBoxStatic1.SetPosition( 8.2500f, 49.0000f );
	_piggyBank1.SetPosition( 88.9504f, 24.3000f );
	_polygonFragile1.SetPosition( 41.3502f, 44.7002f );
	_polygonFragile2.SetPosition( 54.9502f, 44.7002f );
	_polygonFragile3.SetPosition( 48.1502f, 56.1000f );
	_polygonFragile4.SetPosition( 20.9500f, 10.5000f );
	_polygonFragile5.SetPosition( 34.5502f, 10.5000f );
	_polygonFragile6.SetPosition( 48.1502f, 10.5000f );
	_polygonFragile7.SetPosition( 61.7502f, 10.5000f );
	_polygonFragile8.SetPosition( 75.3502f, 10.5000f );
	_polygonFragile9.SetPosition( 27.7500f, 21.9000f );
	_polygonFragile10.SetPosition( 41.3502f, 21.9000f );
	_polygonFragile11.SetPosition( 54.9502f, 21.9000f );
	_polygonFragile12.SetPosition( 68.5502f, 21.9000f );
	_polygonFragile13.SetPosition( 34.5502f, 33.3001f );
	_polygonFragile14.SetPosition( 48.1502f, 33.3001f );
	_polygonFragile15.SetPosition( 61.7502f, 33.3001f );
	_wallBox1.SetPosition( 3.1500f, 31.8502f );
	_wallBox2.SetPosition( 3.5352f, 28.3600f );
	_wallBox3.SetPosition( 4.6728f, 25.0376f );
	_wallBox4.SetPosition( 6.5074f, 22.0436f );
	_wallBox5.SetPosition( 8.9512f, 19.5218f );
	_wallBox6.SetPosition( 11.8862f, 17.5940f );
	_wallBox7.SetPosition( 15.1710f, 16.3528f );
	_wallBox8.SetPosition( 18.6474f, 15.8580f );
	_wallBox9.SetPosition( 88.9504f, 15.4000f );
	_wallBox10.SetPosition( 48.0000f, 3.7500f );

	ConstFinal();
}
//--------------------------------------------------------------------------
LevelConstruction_1d::LevelConstruction_1d( GameTypes::LevelEnum levelEnum, bool viewMode ) : Level( levelEnum, viewMode )
{
	_coinCount = 2;
    _levelTag = eLevelTag_Thief;
    
	// _coin1
	_coin1.GetBody()->GetFixtureList()->SetDensity( 1.5500f );
	_coin1.GetBody()->GetFixtureList()->SetFriction( 0.2500f );
	_coin1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin1.GetBody()->SetAngularDamping(0.0500f );
	_coin1.SetBodyType( b2_dynamicBody );

	// _fragileBox1
	_fragileBox1.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox1.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_fragileBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox1.SetBodyType( b2_dynamicBody );

	// _fragileBox2
	_fragileBox2.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox2.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_fragileBox2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox2.SetBodyType( b2_dynamicBody );

	// _fragileBox3
	_fragileBox3.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox3.GetBody()->GetFixtureList()->SetFriction( 0.9000f );
	_fragileBox3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox3.SetBodyType( b2_dynamicBody );

	// _fragileBox4
	_fragileBox4.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox4.GetBody()->GetFixtureList()->SetFriction( 0.9000f );
	_fragileBox4.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox4.SetBodyType( b2_dynamicBody );

	// _fragileBox5
	_fragileBox5.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox5.GetBody()->GetFixtureList()->SetFriction( 0.9000f );
	_fragileBox5.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox5.SetBodyType( b2_dynamicBody );

	// _fragileBox6
	_fragileBox6.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox6.GetBody()->GetFixtureList()->SetFriction( 0.7500f );
	_fragileBox6.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox6.SetBodyType( b2_dynamicBody );

	// _fragileBox7
	_fragileBox7.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox7.GetBody()->GetFixtureList()->SetFriction( 0.7500f );
	_fragileBox7.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox7.SetBodyType( b2_dynamicBody );

	// _fragileBox8
	_fragileBox8.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox8.GetBody()->GetFixtureList()->SetFriction( 0.7500f );
	_fragileBox8.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox8.SetBodyType( b2_dynamicBody );

	// _fragileBox9
	_fragileBox9.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox9.GetBody()->GetFixtureList()->SetFriction( 0.7500f );
	_fragileBox9.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox9.SetBodyType( b2_dynamicBody );

	// _fragileBox10
	_fragileBox10.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox10.GetBody()->GetFixtureList()->SetFriction( 0.7500f );
	_fragileBox10.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox10.SetBodyType( b2_dynamicBody );

	// _fragileBox11
	_fragileBox11.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox11.GetBody()->GetFixtureList()->SetFriction( 0.7500f );
	_fragileBox11.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox11.SetBodyType( b2_dynamicBody );

	// _fragileBox12
	_fragileBox12.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox12.GetBody()->GetFixtureList()->SetFriction( 0.7500f );
	_fragileBox12.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox12.SetBodyType( b2_dynamicBody );

	// _fragileBox13
	_fragileBox13.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox13.GetBody()->GetFixtureList()->SetFriction( 0.7500f );
	_fragileBox13.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox13.SetBodyType( b2_dynamicBody );

	// _fragileBox14
	_fragileBox14.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox14.GetBody()->GetFixtureList()->SetFriction( 0.9000f );
	_fragileBox14.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox14.SetBodyType( b2_dynamicBody );

	// _fragileBox15
	_fragileBox15.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox15.GetBody()->GetFixtureList()->SetFriction( 0.9000f );
	_fragileBox15.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox15.SetBodyType( b2_dynamicBody );

	// _fragileBox16
	_fragileBox16.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox16.GetBody()->GetFixtureList()->SetFriction( 0.9000f );
	_fragileBox16.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox16.SetBodyType( b2_dynamicBody );

	// _piggyBank1
	_piggyBank1.GetBody()->GetFixtureList()->SetDensity( 7.5000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_piggyBank1.FlipX();
	_piggyBank1.SetBodyType( b2_dynamicBody );

	// _thief1
	_thief1.GetBody()->GetFixtureList()->SetDensity( 9.5500f );
	_thief1.GetBody()->GetFixtureList()->SetFriction( 1.0000f );
	_thief1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_thief1.GetBody()->SetAngularDamping(0.1000f );
	_thief1.GetBody()->SetLinearDamping(0.0500f );
	_thief1.FlipX();
	_thief1.SetBodyType( b2_dynamicBody );

	// _wallBox1
	_wallBox1.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox1.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox1.SetBodyType( b2_staticBody );

	// _woodBox1
	_woodBox1.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox1.GetBody()->GetFixtureList()->SetFriction( 0.5500f );
	_woodBox1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_woodBox1.SetBodyType( b2_dynamicBody );

	// _thief2
	_thief2.GetBody()->GetFixtureList()->SetDensity( 9.5500f );
	_thief2.GetBody()->GetFixtureList()->SetFriction( 1.0000f );
	_thief2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_thief2.GetBody()->SetAngularDamping(0.1000f );
	_thief2.GetBody()->SetLinearDamping(0.0500f );
	_thief2.FlipX();
	_thief2.SetBodyType( b2_dynamicBody );

	// _coin2
	_coin2.GetBody()->GetFixtureList()->SetDensity( 1.5500f );
	_coin2.GetBody()->GetFixtureList()->SetFriction( 0.2500f );
	_coin2.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin2.GetBody()->SetAngularDamping(0.0500f );
	_coin2.SetBodyType( b2_dynamicBody );

	// _woodBox2
	_woodBox2.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox2.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_woodBox2.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_woodBox2.SetBodyType( b2_dynamicBody );

	// _wallBox2
	_wallBox2.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox2.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox2.SetBodyType( b2_staticBody );

	//Set blocks positions
	_coin1.SetPosition( 88.5506f, 55.8502f );
	_fragileBox1.SetPosition( 65.0502f, 44.4502f );
	_fragileBox2.SetPosition( 85.0502f, 44.5500f );
	_fragileBox3.SetPosition( 85.0502f, 34.8000f );
	_fragileBox4.SetPosition( 85.0502f, 25.1000f );
	_fragileBox5.SetPosition( 85.0502f, 15.3500f );
	_fragileBox6.SetPosition( 33.7999f, 40.8500f );
	_fragileBox7.SetPosition( 33.7999f, 31.1500f );
	_fragileBox8.SetPosition( 33.7999f, 21.4000f );
	_fragileBox9.SetPosition( 13.7999f, 40.7502f );
	_fragileBox10.SetPosition( 13.7999f, 31.0502f );
	_fragileBox11.SetPosition( 13.7999f, 21.3000f );
	_fragileBox12.SetPosition( 33.7999f, 11.7500f );
	_fragileBox13.SetPosition( 13.7999f, 11.6500f );
	_fragileBox14.SetPosition( 65.0502f, 15.2500f );
	_fragileBox15.SetPosition( 65.0502f, 25.0000f );
	_fragileBox16.SetPosition( 65.0502f, 34.7002f );
	_piggyBank1.SetPosition( 45.3500f, 13.0000f );
	_thief1.SetPosition( 27.8999f, 52.7000f );
	_wallBox1.SetPosition( 73.9002f, 9.3000f );
	_woodBox1.SetPosition( 30.0500f, 46.5500f );
	_thief2.SetPosition( 15.0999f, 52.7000f );
	_coin2.SetPosition( 80.1508f, 55.8502f );
	_woodBox2.SetPosition( 69.0002f, 50.6000f );
	_wallBox2.SetPosition( 29.2000f, 5.4000f );

	ConstFinal();
}
//--------------------------------------------------------------------------
LevelConstruction_2a::LevelConstruction_2a( GameTypes::LevelEnum levelEnum, bool viewMode ) : Level( levelEnum, viewMode )
{
    _levelTag = eLevelTag_Hacker;
    
	// _coin1
	_coin1.SetRotation( 2.1000f );
	_coin1.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin1.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin1.SetBodyType( b2_dynamicBody );

	// _fragileBox1
	_fragileBox1.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox1.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_fragileBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox1.SetBouyancyRate(0.1500f );
	_fragileBox1.SetBodyType( b2_dynamicBody );

	// _fragileBox2
	_fragileBox2.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox2.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_fragileBox2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox2.SetBouyancyRate(0.1500f );
	_fragileBox2.SetBodyType( b2_dynamicBody );

	// _fragileBox3
	_fragileBox3.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox3.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_fragileBox3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox3.SetBouyancyRate(0.1500f );
	_fragileBox3.SetBodyType( b2_dynamicBody );

	// _fragileBox4
	_fragileBox4.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox4.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_fragileBox4.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox4.SetBouyancyRate(0.1500f );
	_fragileBox4.SetBodyType( b2_dynamicBody );

	// _fragileBox5
	_fragileBox5.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox5.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_fragileBox5.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox5.SetBouyancyRate(0.1500f );
	_fragileBox5.SetBodyType( b2_dynamicBody );

	// _fragileBox6
	_fragileBox6.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox6.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_fragileBox6.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox6.SetBouyancyRate(0.1500f );
	_fragileBox6.SetBodyType( b2_dynamicBody );

	// _fragileBox7
	_fragileBox7.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox7.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_fragileBox7.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox7.SetBouyancyRate(0.1500f );
	_fragileBox7.SetBodyType( b2_dynamicBody );

	// _fragileBox8
	_fragileBox8.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox8.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_fragileBox8.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox8.SetBodyType( b2_dynamicBody );

	// _fragileBox9
	_fragileBox9.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox9.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_fragileBox9.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox9.SetBodyType( b2_dynamicBody );

	// _fragileBox10
	_fragileBox10.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox10.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_fragileBox10.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox10.SetBodyType( b2_dynamicBody );

	// _fragileBox11
	_fragileBox11.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox11.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_fragileBox11.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox11.SetBodyType( b2_dynamicBody );

	// _fragileBox12
	_fragileBox12.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox12.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_fragileBox12.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox12.SetBodyType( b2_dynamicBody );

	// _fragileBox13
	_fragileBox13.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox13.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_fragileBox13.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox13.SetBouyancyRate(0.1500f );
	_fragileBox13.SetBodyType( b2_dynamicBody );

	// _fragileBox14
	_fragileBox14.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox14.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_fragileBox14.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox14.SetBouyancyRate(0.1500f );
	_fragileBox14.SetBodyType( b2_dynamicBody );

	// _fragileBox15
	_fragileBox15.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox15.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_fragileBox15.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox15.SetBouyancyRate(0.1500f );
	_fragileBox15.SetBodyType( b2_dynamicBody );

	// _piggyBank1
	_piggyBank1.FlipX();
	_piggyBank1.SetRotation( 0.00f );
	_piggyBank1.GetBody()->GetFixtureList()->SetDensity( 7.5000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_piggyBank1.SetBodyType( b2_dynamicBody );
	

	// _wallBox1
	_wallBox1.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox1.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox1.SetBodyType( b2_staticBody );

	// _wallBox2
	_wallBox2.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox2.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox2.SetBodyType( b2_staticBody );

	// _woodBox1
	_woodBox1.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox1.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_woodBox1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_woodBox1.SetBodyType( b2_dynamicBody );


	//Set blocks positions
	_coin1.SetPosition( 96.0f - 61.3929f, 53.2170f );
	_fragileBox1.SetPosition( 96.0f - 38.2999f, 22.8999f );
	_fragileBox2.SetPosition( 96.0f - 39.5010f, 42.4499f );
	_fragileBox3.SetPosition( 96.0f - 53.1007f, 32.6498f );
	_fragileBox4.SetPosition( 96.0f - 43.3000f, 32.6498f );
	_fragileBox5.SetPosition( 96.0f - 33.4000f, 32.6498f );
	_fragileBox6.SetPosition( 96.0f - 28.5000f, 22.8999f );
	_fragileBox7.SetPosition( 96.0f - 49.2003f, 42.4499f );
	_fragileBox8.SetPosition( 96.0f - 21.5500f, 11.2000f );
	_fragileBox9.SetPosition( 96.0f - 11.7500f, 11.2000f );
	_fragileBox10.SetPosition( 96.0f - 41.2500f, 11.2000f );
	_fragileBox11.SetPosition( 96.0f - 51.0502f, 11.2000f );
	_fragileBox12.SetPosition( 96.0f - 31.3500f, 11.2000f );
	_fragileBox13.SetPosition( 96.0f - 29.8011f, 42.4499f );
	_fragileBox14.SetPosition( 96.0f - 23.6008f, 32.6498f );
	_fragileBox15.SetPosition( 96.0f - 48.2004f, 22.8999f );
	_piggyBank1.SetPosition( 96.0f - 84.0004f, 54.9998f );
	_wallBox1.SetPosition( 96.0f - 74.6500f, 47.1200f );
	_wallBox2.SetPosition( 96.0f -  31.4000f, 5.1500f );
	_woodBox1.SetPosition( 96.0f - 30.3000f, 17.0500f );

	SetIterations( Config::WorldVelocityIterationsHigh, Config::WorldPositionIterationsHigh );
	ConstFinal();
}
//--------------------------------------------------------------------------
LevelConstruction_4c::LevelConstruction_4c( GameTypes::LevelEnum levelEnum, bool viewMode ) : Level( levelEnum, viewMode )
{
    _levelTag = eLevelTag_Frigle;
    
	// _coin1
	_coin1.SetRotation( -2.4000f );
	_coin1.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin1.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin1.SetBodyType( b2_dynamicBody );

	// _fragileBox1
	_fragileBox1.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox1.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox1.GetBody()->GetFixtureList()->SetRestitution( 0.0500f );
	_fragileBox1.SetBodyType( b2_dynamicBody );

	// _fragileBox2
	_fragileBox2.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox2.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox2.GetBody()->GetFixtureList()->SetRestitution( 0.0500f );
	_fragileBox2.SetBodyType( b2_dynamicBody );

	// _fragileBox3
	_fragileBox3.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox3.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox3.GetBody()->GetFixtureList()->SetRestitution( 0.0500f );
	_fragileBox3.SetBodyType( b2_dynamicBody );

	// _fragileBox4
	_fragileBox4.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox4.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox4.GetBody()->GetFixtureList()->SetRestitution( 0.0500f );
	_fragileBox4.SetBodyType( b2_dynamicBody );

	// _fragileBox5
	_fragileBox5.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox5.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox5.GetBody()->GetFixtureList()->SetRestitution( 0.0500f );
	_fragileBox5.SetBodyType( b2_dynamicBody );

	// _fragileBox6
	_fragileBox6.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox6.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox6.GetBody()->GetFixtureList()->SetRestitution( 0.0500f );
	_fragileBox6.SetBodyType( b2_dynamicBody );

	// _fragileBox7
	_fragileBox7.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox7.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox7.GetBody()->GetFixtureList()->SetRestitution( 0.0500f );
	_fragileBox7.SetBodyType( b2_dynamicBody );

	// _fragileBox8
	_fragileBox8.SetRotation( 90.0000f );
	_fragileBox8.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox8.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox8.GetBody()->GetFixtureList()->SetRestitution( 0.0500f );
	_fragileBox8.SetBodyType( b2_dynamicBody );

	// _fragileBox9
	_fragileBox9.SetRotation( 90.0000f );
	_fragileBox9.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox9.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox9.GetBody()->GetFixtureList()->SetRestitution( 0.0500f );
	_fragileBox9.SetBodyType( b2_dynamicBody );

	// _fragileBox10
	_fragileBox10.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox10.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox10.GetBody()->GetFixtureList()->SetRestitution( 0.0500f );
	_fragileBox10.SetBodyType( b2_dynamicBody );

	// _fragileBox11
	_fragileBox11.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox11.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox11.GetBody()->GetFixtureList()->SetRestitution( 0.0500f );
	_fragileBox11.SetBodyType( b2_dynamicBody );

	// _fragileBox12
	_fragileBox12.SetRotation( 90.0000f );
	_fragileBox12.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox12.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox12.GetBody()->GetFixtureList()->SetRestitution( 0.0500f );
	_fragileBox12.SetBodyType( b2_dynamicBody );

	// _piggyBankStatic1
	_piggyBankStatic1.GetBody()->GetFixtureList()->SetDensity( 7.5000f );
	_piggyBankStatic1.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_piggyBankStatic1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_piggyBankStatic1.FlipX();
	_piggyBankStatic1.SetBodyType( b2_staticBody );

	// _wallBox1
	_wallBox1.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox1.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox1.SetBodyType( b2_staticBody );

	// _wallBox2
	_wallBox2.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox2.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox2.SetBodyType( b2_staticBody );

	// _wallBox3
	_wallBox3.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox3.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox3.SetBodyType( b2_staticBody );


	//Set blocks positions
	_coin1.SetPosition( 77.7001f, 55.5512f );
	_fragileBox1.SetPosition( 59.1999f, 19.7500f );
	_fragileBox2.SetPosition( 73.5496f, 28.8003f );
	_fragileBox3.SetPosition( 44.8497f, 14.4500f );
	_fragileBox4.SetPosition( 73.5496f, 15.4000f );
	_fragileBox5.SetPosition( 44.8497f, 41.9501f );
	_fragileBox6.SetPosition( 44.8497f, 27.6500f );
	_fragileBox7.SetPosition( 59.1999f, 11.4000f );
	_fragileBox8.SetPosition( 73.5496f, 43.2503f );
	_fragileBox9.SetPosition( 59.1999f, 36.1003f );
	_fragileBox10.SetPosition( 30.4498f, 14.4502f );
	_fragileBox11.SetPosition( 30.4498f, 27.7499f );
	_fragileBox12.SetPosition( 30.4498f, 43.0504f );
	_piggyBankStatic1.SetPosition( 13.7501f, 46.5002f );
	_wallBox1.SetPosition( 69.3002f, 54.1502f );
	_wallBox2.SetPosition( 81.7000f, 38.1504f );
	_wallBox3.SetPosition( 52.0004f, 7.1000f );

	ConstFinal();
}
