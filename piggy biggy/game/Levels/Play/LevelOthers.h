#ifndef __LEVELOTHERS_H__
#define __LEVELOTHERS_H__

#include <Box2D/Box2D.h>
#include "Levels/Level.h"
#include "Levels/LevelWithCallback.h"
#include "Blocks/AllBlocks.h"

//--------------------------------------------------------------------------------------------
class LevelOthers_1 : public Level
{
public:
	LevelOthers_1( GameTypes::LevelEnum levelEnum, bool viewMode );

private:
	puBombStatic< 44 >	_bombStatic1;
	puChainCutable	_chain1;
	puChainCutable	_chain2;
	puCoin< 40 >	_coin1;
	puCoin< 40 >	_coin2;
	puCoin< 40 >	_coin3;
	puFragileBox<96,96,eBlockScaled>	_fragileBox1;
	puPiggyBank< 60 >	_piggyBank1;
	puPiggyBank< 60 >	_piggyBank2;
	puWallBox<310,20,eBlockScaled>	_wallBox1;
	puWallBox<35,20,eBlockScaled>	_wallBox10;
	puWallBox<35,20,eBlockScaled>	_wallBox11;
	puWallBox<35,20,eBlockScaled>	_wallBox12;
	puWallBox<35,20,eBlockScaled>	_wallBox2;
	puWallBox<35,20,eBlockScaled>	_wallBox3;
	puWallBox<35,20,eBlockScaled>	_wallBox4;
	puWallBox<35,20,eBlockScaled>	_wallBox5;
	puWallBox<35,20,eBlockScaled>	_wallBox6;
	puWallBox<35,20,eBlockScaled>	_wallBox7;
	puWallBox<35,20,eBlockScaled>	_wallBox8;
	puWallBox<35,20,eBlockScaled>	_wallBox9;
	puWallBox<70,20,eBlockScaled>	_wallBox13;
};
//--------------------------------------------------------------------------------------------
class LevelOthers_2 : public Level
{
public:
	LevelOthers_2( GameTypes::LevelEnum levelEnum, bool viewMode );

private:
	puBomb< 44 >	_bomb1;
	puBombStatic< 44 >	_bombStatic1;
	puChainCutable	_chain1;
	puChainCutable	_chain2;
	puChainCutable	_chain3;
	puCoin< 40 >	_coin1;
	puFragileBoxStatic<96,96,eBlockScaled>	_fragileBoxStatic1;
	puFragileBoxStatic<96,96,eBlockScaled>	_fragileBoxStatic2;
	puPiggyBankStatic< 60 >	_piggyBankStatic1;
	puWallBox<400,20,eBlockScaled>	_wallBox2;
	puWallBox<50,20,eBlockScaled>	_wallBox3;
	puWallBox<50,20,eBlockScaled>	_wallBox4;
	puWallBox<600,20,eBlockScaled>	_wallBox1;
	puWoodBox<280,20,eBlockScaled>	_woodBox1;

};
//--------------------------------------------------------------------------------------------
class LevelSwingAndBomb : public Level
{
public:
	LevelSwingAndBomb ( GameTypes::LevelEnum levelEnum, bool viewMode );

private:
	puBomb< 44 >	_bomb1;
	puBoxerGlove	_boxerGlove1;
	puCoin< 40 >	_coin1;
	puCoin< 40 >	_coin2;
	puFragileBox<192,96,eBlockScaled>	_fragileBox1;
	puFragileBox<96,96,eBlockScaled>	_fragileBox2;
	puFragileBox<96,96,eBlockScaled>	_fragileBox3;
	puPiggyBank< 60 >	_piggyBank1;
	puWallBox<20,20,eBlockScaled>	_wallBox1;
	puWallBox<400,20,eBlockScaled>	_wallBox2;
	puWallBox<50,20,eBlockScaled>	_wallBox10;
	puWallBox<50,20,eBlockScaled>	_wallBox11;
	puWallBox<50,20,eBlockScaled>	_wallBox12;
	puWallBox<50,20,eBlockScaled>	_wallBox13;
	puWallBox<50,20,eBlockScaled>	_wallBox14;
	puWallBox<50,20,eBlockScaled>	_wallBox15;
	puWallBox<50,20,eBlockScaled>	_wallBox16;
	puWallBox<50,20,eBlockScaled>	_wallBox17;
	puWallBox<50,20,eBlockScaled>	_wallBox18;
	puWallBox<50,20,eBlockScaled>	_wallBox3;
	puWallBox<50,20,eBlockScaled>	_wallBox4;
	puWallBox<50,20,eBlockScaled>	_wallBox5;
	puWallBox<50,20,eBlockScaled>	_wallBox6;
	puWallBox<50,20,eBlockScaled>	_wallBox7;
	puWallBox<50,20,eBlockScaled>	_wallBox8;
	puWallBox<50,20,eBlockScaled>	_wallBox9;
};
//--------------------------------------------------------------------------------------------
class LevelRotateL : public LevelVertical
{
public:
	LevelRotateL( GameTypes::LevelEnum levelEnum, bool viewMode );

private:
	puCoin< 40 >	_coin1;
	puCoin< 40 >	_coin2;
	puFragileBox<90,90,eBlockScaled>	_fragileBox10;
	puFragileBox<90,90,eBlockScaled>	_fragileBox11;
	puFragileBox<90,90,eBlockScaled>	_fragileBox12;
	puFragileBox<90,90,eBlockScaled>	_fragileBox1;
	puFragileBox<90,90,eBlockScaled>	_fragileBox2;
	puFragileBox<90,90,eBlockScaled>	_fragileBox3;
	puFragileBoxStatic<90,90,eBlockScaled>	_fragileBox4;
	puFragileBoxStatic<90,90,eBlockScaled>	_fragileBox5;
	puFragileBoxStatic<90,90,eBlockScaled>	_fragileBox6;
	puFragileBox<90,90,eBlockScaled>	_fragileBox7;
	puFragileBox<90,90,eBlockScaled>	_fragileBox8;
	puFragileBox<90,90,eBlockScaled>	_fragileBox9;
	puL<100,30,eBlockScaled>	_l1;
	puPiggyBank< 50 >	_piggyBank1;
	puThief< 50 >	_thief1;
	puWallBox<540,20,eBlockScaled>	_wallBox1;
};
//--------------------------------------------------------------------------------------------
class LevelOthers_ControlWheel : public LevelActivable
{
public:
	LevelOthers_ControlWheel( GameTypes::LevelEnum levelEnum, bool viewMode );
	~LevelOthers_ControlWheel();

private:
	void CreateJoints();

	puCoin< 34 > _coin2;
	puCoin< 34 > _coin3;
	puPiggyBank< 34 >	_piggyBank2;

	puCoin< 40 > _coin1;
	puPiggyBank< 40 >	_piggyBank1;

	puThief< > _thief1;
	puThief< > _thief2;
	puThief< > _thief3;
	puThief< > _thief4;
	puThief< > _thief5;
	puThief< > _thief6;
	puThief< > _thief7;
	puThief< > _thief8;
	puThief< > _thief9;
	puThief< > _thief10;

	puWallBox<14,40,eBlockScaled>	_wallBox10;
	puWallBox<14,40,eBlockScaled>	_wallBox1;
	puWallBox<14,40,eBlockScaled>	_wallBox2;
	puWallBox<14,40,eBlockScaled>	_wallBox3;
	puWallBox<14,40,eBlockScaled>	_wallBox4;
	puWallBox<14,40,eBlockScaled>	_wallBox5;
	puWallBox<14,40,eBlockScaled>	_wallBox7;
	puWallBox<14,40,eBlockScaled>	_wallBox8;
	puWallBox<14,40,eBlockScaled>	_wallBox9;
	
	puScrew< 8 >	_screw2;
	puWoodBox<250,16,eBlockScaled>	_woodBox1;
	puCircleWood< 20 >	_circleWood1;

	puCircleWood< 48 >	_circleWood2;

	b2GearJoint	*_jointGear1;
	b2GearJoint	*_jointGear2;
};
//--------------------------------------------------------------------------------------------
class LevelOthers_4 : public Level
{
public:
	LevelOthers_4( GameTypes::LevelEnum levelEnum, bool viewMode );

private:
	puBorderAllClose _floor;

	puPolygonWood	_polygonFragile1;
	puPolygonWood	_polygonFragile2;
	puPolygonWood	_polygonFragile3;
	puPolygonWood	_polygonFragile4;

	puPolygonWood	_polygonFragile5;
	puPolygonWood	_polygonFragile6;
	puPolygonWood	_polygonFragile7;
};
//--------------------------------------------------------------------------------------------
class LevelOthers_6 : public Level
{
public:
	LevelOthers_6( GameTypes::LevelEnum levelEnum, bool viewMode );

private:
	puBombStatic< 44 >	_bombStatic1;
	puBombStatic< 44 >	_bombStatic2;
	puBombStatic< 44 >	_bombStatic3;
	puButton< 40 >	_button1;
	puCoin< 40 >	_coin1;
	puCoin< 40 >	_coin2;
	puPiggyBank< 60 >	_piggyBank1;
	puWallBox<160,20,eBlockScaled>	_wallBox1;
	puWallBox<160,20,eBlockScaled>	_wallBox2;
	puWallBox<160,20,eBlockScaled>	_wallBox3;
	puWallBox<600,20,eBlockScaled>	_wallBox4;
};
//--------------------------------------------------------------------------------------------
class LevelOthers_7 : public LevelVertical
{
public:
	LevelOthers_7( GameTypes::LevelEnum levelEnum, bool viewMode );

private:
	puCoin< 40 >	_coin1;
	puFragileBoxStatic<96,96,eBlockScaled>	_fragileBoxStatic1;
	puFragileBoxStatic<96,96,eBlockScaled>	_fragileBoxStatic2;
	puFragileBoxStatic<96,96,eBlockScaled>	_fragileBoxStatic3;
	puFragileBoxStatic<96,96,eBlockScaled>	_fragileBoxStatic4;
	puFragileBoxStatic<96,96,eBlockScaled>	_fragileBoxStatic5;
	puFragileBoxStatic<96,96,eBlockScaled>	_fragileBoxStatic6;
	puPiggyBank< 60 >	_piggyBank1;
	puThief< 50 >	_thief1;
	puThief< 50 >	_thief2;
	puWallBox<100,20,eBlockScaled>	_wallBox1;
	puWoodBox<460,20,eBlockScaled>	_woodBox1;
};
//--------------------------------------------------------------------------------------------


#endif
