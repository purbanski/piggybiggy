#ifndef __LEVELCHAIN_H__
#define __LEVELCHAIN_H__

#include <Box2D/Box2D.h>
#include "Levels/Level.h"
#include "Blocks/AllBlocks.h"

//---------------------------------------------------------------------------------------
class LevelChain_1 : public Level
{
public:
	LevelChain_1( GameTypes::LevelEnum levelEnum, bool viewMode );
	~LevelChain_1();

private:
	void CreateJoints();

	puButton<40>	_button1;
	puChainCutable	_chain1;
	puChainCutable	_chain2;
	puChainCutable	_chain3;
	puChainCutable	_chain4;
	puCoin< 40 >	_coin1;
	puCoin< 40 >	_coin2;
	puPiggyBank< 60 >	_piggyBank1;
	puScrew< 10 >	_screw1;
	puWallBox<20,20,eBlockScaled>	_wallBox1;
	puWallBox<20,20,eBlockScaled>	_wallBox2;
	puWallBox<20,20,eBlockScaled>	_wallBox3;
	puWallBox<250,20,eBlockScaled>	_wallBox4;
	puWallBox<360,20,eBlockScaled>	_wallBox11;
	puWallBox<50,20,eBlockScaled>	_wallBox10;
	puWallBox<50,20,eBlockScaled>	_wallBox5;
	puWallBox<50,20,eBlockScaled>	_wallBox6;
	puWallBox<50,20,eBlockScaled>	_wallBox7;
	puWallBox<50,20,eBlockScaled>	_wallBox8;
	puWallBox<50,20,eBlockScaled>	_wallBox9;
	puWood2Box<100,20,eBlockScaled>	_woodBox1;
	puWood2Box<100,20,eBlockScaled>	_woodBox2;

	b2PrismaticJoint	*_jointPris1;
	b2RevoluteJoint	*_jointRev1;
	b2GearJoint	*_jointGear1;

};
//---------------------------------------------------------------------------------------
class LevelChain_2a : public Level
{
public:
	LevelChain_2a( GameTypes::LevelEnum levelEnum, bool viewMode );

private:
	puChainCutable	_chain1;
	puChainCutable	_chain2;
	puChainCutable	_chain3;
	puCoinStatic< 40 >	_coinStatic1;
	puCoinStatic< 40 >	_coinStatic2;
	puPiggyBank< 60 >	_piggyBank1;
};
//---------------------------------------------------------------------------------------
class LevelChain_2b : public Level
{
public:
	LevelChain_2b( GameTypes::LevelEnum levelEnum, bool viewMode );

private:
	puChainCutable	_chain1;
	puChainCutable	_chain2;
	puChainCutable	_chain3;
	puCoinStatic< 40 >	_coinStatic1;
	puCoinStatic< 40 >	_coinStatic2;
	puPiggyBank< 60 >	_piggyBank1;
	puThief< 60 >	_thief1;
	puThief< 60 >	_thief2;
	puWallBox<70,20,eBlockScaled>	_wallBox1;
	puWallBox<70,20,eBlockScaled>	_wallBox2;
};
//---------------------------------------------------------------------------------------
class LevelChain_2c : public Level
{
public:
	LevelChain_2c( GameTypes::LevelEnum levelEnum, bool viewMode );

private:
	puChainCutable	_chain1;
	puChainCutable	_chain2;
	puChainCutable	_chain3;
	puCoinStatic< 40 >	_coinStatic1;
	puCoinStatic< 40 >	_coinStatic2;
	puPiggyBank< 60 >	_piggyBank1;
	puThiefStatic< 60 >	_thiefStatic1;
};
//---------------------------------------------------------------------------------------
class LevelChain_3 : public LevelVertical
{
public:
	LevelChain_3( GameTypes::LevelEnum levelEnum, bool viewMode );

private:
	puChainCutable	_chain1;
	puChainCutable	_chain2;
	puCoin< 40 >	_coin1;
	puCoin< 40 >	_coin2;
	puCoin< 40 >	_coin3;
	puCoin< 40 >	_coin4;
	puCoin< 40 >	_coin5;
	puPiggyBank< 60 >	_piggyBank1;
	puThief< 60 >	_thief1;
	puThief< 60 >	_thief2;
	puThief< 60 >	_thief3;
	puThief< 60 >	_thief4;
//	puWallBox<310,20,eBlockScaled>	_wallBox1;
	puWallBox<500,20,eBlockScaled>	_wallBox1;
};
//---------------------------------------------------------------------------------------
class LevelChain_4 : public Level
{
public:
	LevelChain_4( GameTypes::LevelEnum levelEnum, bool viewMode );

private:
	puChainCutable		_chain1;
	puChainCutable		_chain2;
	puChainCutable		_chain3;
	puCoinStatic< 40 >	_coin1;
	puCoinStatic< 40 >	_coin2;
	puCoinStatic< 40 >	_coin3;
	puCoinStatic< 40 >	_coin4;
	puCoinStatic< 40 >	_coin5;
	puCoinStatic< 40 >	_coin6;
	puPiggyBank< 60 >	_piggyBank1;
};
//---------------------------------------------------------------------------------------
class LevelChain_5 : public LevelVertical
{
public:
	LevelChain_5( GameTypes::LevelEnum levelEnum, bool viewMode );

private:
	puChainCutable	_chain1;
	puChainCutable	_chain2;
	puChainCutable	_chain3;
	puChainCutable	_chain4;
	puChainCutable	_chain5;
	puChainCutable	_chain6;
	puCoin< 40 >	_coin1;
	puCoin< 40 >	_coin2;
	puCoin< 40 >	_coin3;
	puPiggyBankStatic< 60 >	_piggyBankStatic1;
	puThiefStatic< 50 >	_thiefStatic1;
	puThiefStatic< 50 >	_thiefStatic2;
	puThiefStatic< 50 >	_thiefStatic3;
	puThiefStatic< 50 >	_thiefStatic4;
	puThiefStatic< 50 >	_thiefStatic5;
};
//---------------------------------------------------------------------------------------
class LevelChain_6 : public LevelVertical
{
public:
	LevelChain_6( GameTypes::LevelEnum levelEnum, bool viewMode );

private:
	puChainCutable	_chain1;
	puChainCutable	_chain2;
	puChainCutable	_chain3;
	puChainCutable	_chain4;
	puChainCutable	_chain5;
	puChainCutable	_chain6;
	puCoin< 40 >	_coin1;
	puCoin< 40 >	_coin2;
	puCoin< 40 >	_coin3;
	puPiggyBank< 44 >	_piggyBank1;
	puPiggyBank< 44 >	_piggyBank2;
	puPiggyBank< 44 >	_piggyBank3;
	puThief< 44 >	_thief1;
	puThief< 44 >	_thief2;
	puThief< 44 >	_thief3;
	puThief< 44 >	_thief4;
	puThief< 44 >	_thief5;
	puThief< 44 >	_thief6;
	puWallBox<140,20,eBlockScaled>	_wallBox1;
	puWallBox<140,20,eBlockScaled>	_wallBox2;
	puWallBox<140,20,eBlockScaled>	_wallBox3;
	puWallBox<140,20,eBlockScaled>	_wallBox4;
	puWallBox<140,20,eBlockScaled>	_wallBox5;
	puWallBox<140,20,eBlockScaled>	_wallBox6;
};
//---------------------------------------------------------------------------------------
class LevelChain_7a : public Level
{
public:
	LevelChain_7a( GameTypes::LevelEnum levelEnum, bool viewMode );

private:
	puChainCutable	_chain1;
	puChainCutable	_chain2;
	puChainCutable	_chain3;
	puChainCutable	_chain4;
	puCoin< 40 >	_coin1;
	puPiggyBank< 60 >	_piggyBank1;
	puWallBox<100,20,eBlockScaled>	_wallBox1;
};
//---------------------------------------------------------------------------------------
class LevelChain_7b : public Level
{
public:
	LevelChain_7b( GameTypes::LevelEnum levelEnum, bool viewMode );

private:
	puChainCutable	_chain1;
	puChainCutable	_chain2;
	puChainCutable	_chain3;
	puChainCutable	_chain4;
	puCoin< 40 >	_coin1;
	puPiggyBank< 60 >	_piggyBank1;
	puWallBox<140,20,eBlockScaled>	_wallBox2;
	puWallBox<140,20,eBlockScaled>	_wallBox3;
};
//---------------------------------------------------------------------------------------
#endif
