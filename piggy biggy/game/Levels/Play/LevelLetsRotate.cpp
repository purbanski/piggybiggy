#include "LevelLetsRotate.h"
#include "Platform/Lang.h"
//--------------------------------------------------------------------------
LevelLetsRotate_1::LevelLetsRotate_1( GameTypes::LevelEnum levelEnum, bool viewMode ) : LevelAccelerometr( levelEnum, viewMode)
{
	// _circleWall1
	_circleWall1.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_circleWall1.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_circleWall1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_circleWall1.SetBodyType( b2_staticBody );

	// _circleWall2
	_circleWall2.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_circleWall2.GetBody()->GetFixtureList()->SetFriction( 0.1500f );
	_circleWall2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_circleWall2.SetBodyType( b2_staticBody );

	// _coin1
	_coin1.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin1.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin1.SetBodyType( b2_dynamicBody );

	// _piggyBank1
	_piggyBank1.GetBody()->GetFixtureList()->SetDensity( 7.5000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_piggyBank1.FlipX();
	_piggyBank1.SetBodyType( b2_dynamicBody );

	// _wallBox1
	_wallBox1.SetRotation( 90.0000f );
	_wallBox1.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox1.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox1.SetBodyType( b2_staticBody );

	// _wallBox2
	_wallBox2.SetRotation( 90.0000f );
	_wallBox2.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox2.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox2.SetBodyType( b2_staticBody );

	// _wallBox3
	_wallBox3.SetRotation( 90.0000f );
	_wallBox3.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox3.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox3.SetBodyType( b2_staticBody );

	// _wallBox4
	_wallBox4.SetRotation( 90.0000f );
	_wallBox4.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox4.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox4.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox4.SetBodyType( b2_staticBody );

	// _wallBox5
	_wallBox5.SetRotation( 90.0000f );
	_wallBox5.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox5.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox5.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox5.SetBodyType( b2_staticBody );

	// _wallBox6
	_wallBox6.SetRotation( 90.0000f );
	_wallBox6.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox6.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox6.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox6.SetBodyType( b2_staticBody );

	// _wallBox7
	_wallBox7.SetRotation( 90.0000f );
	_wallBox7.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox7.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox7.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox7.SetBodyType( b2_staticBody );

	// _wallBox8
	_wallBox8.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox8.GetBody()->GetFixtureList()->SetFriction( 0.1500f );
	_wallBox8.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox8.SetBodyType( b2_staticBody );

	// _wallBox9
	_wallBox9.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox9.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox9.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox9.SetBodyType( b2_staticBody );


	//Set blocks positions
	_circleWall1.SetPosition( 57.3000f, 49.5500f );
	_circleWall2.SetPosition( 57.3000f, 12.6531f );
	_coin1.SetPosition( 84.2000f, 52.1169f );
	_piggyBank1.SetPosition( 10.1500f, 33.6500f );
	_wallBox1.SetPosition( 39.0000f, 12.2500f );
	_wallBox2.SetPosition( 75.2000f, 12.2500f );
	_wallBox3.SetPosition( 39.0000f, 46.7000f );
	_wallBox4.SetPosition( 75.2000f, 46.7000f );
	_wallBox5.SetPosition( 57.3000f, 31.1000f );
	_wallBox6.SetPosition( 19.7000f, 24.2500f );
	_wallBox7.SetPosition( 92.8000f, 32.3500f );
	_wallBox8.SetPosition( 48.4500f, 3.0500f );
	_wallBox9.SetPosition( 48.4500f, 61.0499f );

    float textSize;
    
    if (Lang::Get()->IsChinese())
        textSize = 32.0f;
    else
        textSize = 26.0f;
    
    _msgManager.push_back(eOTMsg_AccelIntro,
                          new LevelMsg_ArrowMsg( 260.0f,
                                                LocalString("AccelIntro"), textSize,
                                                CCPoint( 115.0f, -240.0f),
                                                3, true ));

	ConstFinal();
}
//--------------------------------------------------------------------------
LevelLetsRotate_2::LevelLetsRotate_2( GameTypes::LevelEnum levelEnum, bool viewMode ) : LevelAccelerometr( levelEnum, viewMode )
{
	// _coin1
	_coin1.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin1.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin1.SetBodyType( b2_dynamicBody );

	// _piggyBank1
	_piggyBank1.GetBody()->GetFixtureList()->SetDensity( 7.5000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_piggyBank1.SetBodyType( b2_dynamicBody );

	// _wallBox1
	_wallBox1.SetRotation( 90.0000f );
	_wallBox1.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox1.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox1.SetBodyType( b2_staticBody );

	// _wallBox2
	_wallBox2.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox2.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox2.SetBodyType( b2_staticBody );

	// _wallBox3
	_wallBox3.SetRotation( 90.0000f );
	_wallBox3.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox3.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox3.SetBodyType( b2_staticBody );

	// _wallBox4
	_wallBox4.SetRotation( 90.0000f );
	_wallBox4.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox4.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox4.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox4.SetBodyType( b2_staticBody );

	// _wallBox5
	_wallBox5.SetRotation( 90.0000f );
	_wallBox5.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox5.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox5.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox5.SetBodyType( b2_staticBody );

	// _wallBox6
	_wallBox6.SetRotation( 90.0000f );
	_wallBox6.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox6.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox6.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox6.SetBodyType( b2_staticBody );

	// _wallBox7
	_wallBox7.SetRotation( 90.0000f );
	_wallBox7.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox7.GetBody()->GetFixtureList()->SetFriction( 0.1000f );
	_wallBox7.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox7.SetBodyType( b2_staticBody );

	// _wallBox8
	_wallBox8.SetRotation( 90.0000f );
	_wallBox8.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox8.GetBody()->GetFixtureList()->SetFriction( 0.1000f );
	_wallBox8.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox8.SetBodyType( b2_staticBody );

	// _wallBox9
	_wallBox9.SetRotation( 180.0000f );
	_wallBox9.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox9.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox9.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox9.SetBodyType( b2_staticBody );

	// _wallBox10
	_wallBox10.SetRotation( 210.0000f );
	_wallBox10.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox10.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox10.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox10.SetBodyType( b2_staticBody );

	// _wallBox11
	_wallBox11.SetRotation( 240.0000f );
	_wallBox11.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox11.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox11.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox11.SetBodyType( b2_staticBody );

	// _wallBox12
	_wallBox12.SetRotation( 270.0000f );
	_wallBox12.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox12.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox12.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox12.SetBodyType( b2_staticBody );

	// _wallBox13
	_wallBox13.SetRotation( 300.0000f );
	_wallBox13.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox13.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox13.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox13.SetBodyType( b2_staticBody );

	// _wallBox14
	_wallBox14.SetRotation( 330.0000f );
	_wallBox14.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox14.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox14.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox14.SetBodyType( b2_staticBody );

	// _wallBox15
	_wallBox15.SetRotation( 360.0000f );
	_wallBox15.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox15.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox15.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox15.SetBodyType( b2_staticBody );

	// _wallBox16
	_wallBox16.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox16.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox16.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox16.SetBodyType( b2_staticBody );


	//Set blocks positions
	_coin1.SetPosition( 9.3500f, 31.9999f );
	_piggyBank1.SetPosition( 63.2502f, 31.9998f );
	_wallBox1.SetPosition( 3.1000f, 31.9000f );
	_wallBox2.SetPosition( 23.8000f, 60.9000f );
	_wallBox3.SetPosition( 28.8999f, 23.6500f );
	_wallBox4.SetPosition( 15.9999f, 40.2501f );
	_wallBox5.SetPosition( 15.9999f, 7.1500f );
	_wallBox6.SetPosition( 28.8999f, 56.7501f );
	_wallBox7.SetPosition( 41.8000f, 7.1500f );
	_wallBox8.SetPosition( 41.8000f, 40.2500f );
	_wallBox9.SetPosition( 79.6002f, 36.6000f );
	_wallBox10.SetPosition( 85.1002f, 38.0737f );
	_wallBox11.SetPosition( 89.1264f, 42.1000f );
	_wallBox12.SetPosition( 90.6002f, 47.6000f );
	_wallBox13.SetPosition( 89.1264f, 53.1000f );
	_wallBox14.SetPosition( 85.1002f, 57.1263f );
	_wallBox15.SetPosition( 79.6002f, 58.6000f );
	_wallBox16.SetPosition( 23.8000f, 2.9500f );

	ConstFinal();
}
//--------------------------------------------------------------------------
LevelLetsRotate_3::LevelLetsRotate_3( GameTypes::LevelEnum levelEnum, bool viewMode ) : LevelAccelerometr( levelEnum, viewMode )
{
	// _coin1
	_coin1.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin1.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin1.SetBodyType( b2_dynamicBody );

	// _piggyBank1
	_piggyBank1.GetBody()->GetFixtureList()->SetDensity( 7.5000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_piggyBank1.SetBodyType( b2_dynamicBody );

	// _wallBox1
	_wallBox1.SetRotation( 90.0000f );
	_wallBox1.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox1.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox1.SetBodyType( b2_staticBody );

	// _wallBox2
	_wallBox2.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox2.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox2.SetBodyType( b2_staticBody );

	// _wallBox3
	_wallBox3.SetRotation( 90.0000f );
	_wallBox3.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox3.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox3.SetBodyType( b2_staticBody );

	// _wallBox4
	_wallBox4.SetRotation( 90.0000f );
	_wallBox4.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox4.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox4.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox4.SetBodyType( b2_staticBody );

	// _wallBox5
	_wallBox5.SetRotation( 90.0000f );
	_wallBox5.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox5.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox5.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox5.SetBodyType( b2_staticBody );

	// _wallBox6
	_wallBox6.SetRotation( 90.0000f );
	_wallBox6.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox6.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox6.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox6.SetBodyType( b2_staticBody );

	// _wallBox7
	_wallBox7.SetRotation( 90.0000f );
	_wallBox7.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox7.GetBody()->GetFixtureList()->SetFriction( 0.1000f );
	_wallBox7.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox7.SetBodyType( b2_staticBody );

	// _wallBox8
	_wallBox8.SetRotation( 90.0000f );
	_wallBox8.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox8.GetBody()->GetFixtureList()->SetFriction( 0.1000f );
	_wallBox8.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox8.SetBodyType( b2_staticBody );

	// _wallBox9
	_wallBox9.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox9.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox9.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox9.SetBodyType( b2_staticBody );


	//Set blocks positions
	_coin1.SetPosition( 9.3500f, 31.9999f );
	_piggyBank1.SetPosition( 69.3505f, 31.9998f );
	
	_wallBox2.SetPosition( 23.8000f, 60.9000f );
	
	_wallBox1.SetPosition( 3.1000f, 31.9000f );
	
	
	_wallBox4.SetPosition( 15.9999f, 23.6500f );
	_wallBox5.SetPosition( 15.9999f, 56.7501f  );
	
	_wallBox6.SetPosition( 28.8999f, 7.1500f );
	_wallBox3.SetPosition( 28.8999f, 40.2500f );

	_wallBox7.SetPosition( 41.8000f, 56.7501f );
	_wallBox8.SetPosition( 41.8000f, 23.6500f );
	_wallBox9.SetPosition( 23.8000f, 2.9500f );

	ConstFinal();
}
//--------------------------------------------------------------------------
LevelLetsRotate_4::LevelLetsRotate_4( GameTypes::LevelEnum levelEnum, bool viewMode ) : LevelAccelerometr( levelEnum, viewMode )
{
	// _coin1
	_coin1.GetBody()->GetFixtureList()->SetDensity( 3.50f );
	_coin1.GetBody()->GetFixtureList()->SetFriction( 0.60f );
	_coin1.GetBody()->GetFixtureList()->SetRestitution( 0.20f );
	_coin1.SetBodyType( b2_dynamicBody );

	// _piggyBank1
	_piggyBank1.GetBody()->GetFixtureList()->SetDensity( 7.50f );
	_piggyBank1.GetBody()->GetFixtureList()->SetFriction( 0.40f );
	_piggyBank1.GetBody()->GetFixtureList()->SetRestitution( 0.20f );
	_piggyBank1.SetBodyType( b2_dynamicBody );
	_piggyBank1.FlipX();

	// _wallBox1
	_wallBox1.GetBody()->GetFixtureList()->SetDensity( 8.0f );
	_wallBox1.GetBody()->GetFixtureList()->SetFriction( 0.80f );
	_wallBox1.GetBody()->GetFixtureList()->SetRestitution( 0.10f );
	_wallBox1.SetBodyType( b2_staticBody );
	_wallBox1.SetRotation( 90.0f );

	//Set blocks positions
	_coin1.SetPosition( 2.0f * 33.50f, 2.0f * 16.0f );
	_piggyBank1.SetPosition( 2.0f * 14.50f, 2.0f * 16.0f );
	_wallBox1.SetPosition( 2.0f * 24.0f, 2.0f * 16.0f );
	
	ConstFinal();
}
//--------------------------------------------------------------------------
LevelLetsRotate_5::LevelLetsRotate_5( GameTypes::LevelEnum levelEnum, bool viewMode ) : LevelAccelerometr( levelEnum, viewMode )
{
	// _wallBox1
	_wallBox1.GetBody()->GetFixtureList()->SetDensity( 8.0f );
	_wallBox1.GetBody()->GetFixtureList()->SetFriction( 0.80f );
	_wallBox1.GetBody()->GetFixtureList()->SetRestitution( 0.10f );
	_wallBox1.SetBodyType( b2_staticBody );
	_wallBox1.SetRotation( 90.0f );

	// _coin1
	_coin1.GetBody()->GetFixtureList()->SetDensity( 3.50f );
	_coin1.GetBody()->GetFixtureList()->SetFriction( 0.60f );
	_coin1.GetBody()->GetFixtureList()->SetRestitution( 0.20f );
	_coin1.SetBodyType( b2_dynamicBody );

	// _piggyBank1
	_piggyBank1.FlipX();
	_piggyBank1.GetBody()->GetFixtureList()->SetDensity( 7.50f );
	_piggyBank1.GetBody()->GetFixtureList()->SetFriction( 0.40f );
	_piggyBank1.GetBody()->GetFixtureList()->SetRestitution( 0.20f );
	_piggyBank1.SetBodyType( b2_dynamicBody );

	// _thief1
	_thief1.GetBody()->GetFixtureList()->SetDensity( 6.50f );
	_thief1.GetBody()->GetFixtureList()->SetFriction( 0.50f );
	_thief1.GetBody()->GetFixtureList()->SetRestitution( 0.10f );
	_thief1.SetBodyType( b2_dynamicBody );

	// _thief2
	_thief2.FlipX();
	_thief2.GetBody()->GetFixtureList()->SetDensity( 6.50f );
	_thief2.GetBody()->GetFixtureList()->SetFriction( 0.50f );
	_thief2.GetBody()->GetFixtureList()->SetRestitution( 0.10f );
	_thief2.SetBodyType( b2_dynamicBody );

	// _thief3
	_thief3.GetBody()->GetFixtureList()->SetDensity( 6.50f );
	_thief3.GetBody()->GetFixtureList()->SetFriction( 0.50f );
	_thief3.GetBody()->GetFixtureList()->SetRestitution( 0.10f );
	_thief3.SetBodyType( b2_dynamicBody );

	// _thief4
	_thief4.FlipX();
	_thief4.GetBody()->GetFixtureList()->SetDensity( 6.50f );
	_thief4.GetBody()->GetFixtureList()->SetFriction( 0.50f );
	_thief4.GetBody()->GetFixtureList()->SetRestitution( 0.10f );
	_thief4.SetBodyType( b2_dynamicBody );

	// _thief5
	_thief5.GetBody()->GetFixtureList()->SetDensity( 6.50f );
	_thief5.GetBody()->GetFixtureList()->SetFriction( 0.50f );
	_thief5.GetBody()->GetFixtureList()->SetRestitution( 0.10f );
	_thief5.SetBodyType( b2_dynamicBody );

	// _thief6
	_thief6.FlipX();
	_thief6.GetBody()->GetFixtureList()->SetDensity( 6.50f );
	_thief6.GetBody()->GetFixtureList()->SetFriction( 0.50f );
	_thief6.GetBody()->GetFixtureList()->SetRestitution( 0.10f );
	_thief6.SetBodyType( b2_dynamicBody );


	//Set blocks positions
	_wallBox1.SetPosition( 2.0f * 24.0f, 2.0f * 16.0f );
	_coin1.SetPosition( 2.0f * 33.50f, 2.0f * 16.0f );
	_piggyBank1.SetPosition( 2.0f * 14.50f, 2.0f * 16.0f );
	_thief1.SetPosition( 2.0f * 39.50f, 2.0f * 16.0f );
	_thief5.SetPosition( 2.0f * 33.50f, 2.0f * 22.050f );
	_thief3.SetPosition( 2.0f * 33.50f, 2.0f * 10.0f );
	
	_thief4.SetPosition( 2.0f * 14.50f, 2.0f * 22.0f );
	_thief2.SetPosition( 2.0f * 14.50f, 2.0f * 10.0f );
	_thief6.SetPosition( 2.0f * 8.50f, 2.0f * 16.0f );

	ConstFinal();
}
//--------------------------------------------------------------------------
LevelLetsRotate_6::LevelLetsRotate_6( GameTypes::LevelEnum levelEnum, bool viewMode ) : LevelAccelerometr( levelEnum, viewMode )
{
	// _coin1
	_coin1.GetBody()->GetFixtureList()->SetDensity( 3.50f );
	_coin1.GetBody()->GetFixtureList()->SetFriction( 0.60f );
	_coin1.GetBody()->GetFixtureList()->SetRestitution( 0.20f );
	_coin1.SetBodyType( b2_dynamicBody );

	// _piggyBank1
	_piggyBank1.FlipX();
	_piggyBank1.GetBody()->GetFixtureList()->SetDensity( 7.50f );
	_piggyBank1.GetBody()->GetFixtureList()->SetFriction( 0.40f );
	_piggyBank1.GetBody()->GetFixtureList()->SetRestitution( 0.20f );
	_piggyBank1.SetBodyType( b2_dynamicBody );

	// _thief1
	_thief1.GetBody()->GetFixtureList()->SetDensity( 6.50f );
	_thief1.GetBody()->GetFixtureList()->SetFriction( 0.50f );
	_thief1.GetBody()->GetFixtureList()->SetRestitution( 0.10f );
	_thief1.SetBodyType( b2_dynamicBody );

	// _thief2
	_thief2.FlipX();
	_thief2.GetBody()->GetFixtureList()->SetDensity( 6.50f );
	_thief2.GetBody()->GetFixtureList()->SetFriction( 0.50f );
	_thief2.GetBody()->GetFixtureList()->SetRestitution( 0.10f );
	_thief2.SetBodyType( b2_dynamicBody );

	// _thief3
	_thief3.GetBody()->GetFixtureList()->SetDensity( 6.50f );
	_thief3.GetBody()->GetFixtureList()->SetFriction( 0.50f );
	_thief3.GetBody()->GetFixtureList()->SetRestitution( 0.10f );
	_thief3.SetBodyType( b2_dynamicBody );

	// _thief4
	_thief4.FlipX();
	_thief4.GetBody()->GetFixtureList()->SetDensity( 6.50f );
	_thief4.GetBody()->GetFixtureList()->SetFriction( 0.50f );
	_thief4.GetBody()->GetFixtureList()->SetRestitution( 0.10f );
	_thief4.SetBodyType( b2_dynamicBody );

	// _thief5
	_thief5.GetBody()->GetFixtureList()->SetDensity( 6.50f );
	_thief5.GetBody()->GetFixtureList()->SetFriction( 0.50f );
	_thief5.GetBody()->GetFixtureList()->SetRestitution( 0.10f );
	_thief5.SetBodyType( b2_dynamicBody );

	// _thief6
	_thief6.FlipX();
	_thief6.GetBody()->GetFixtureList()->SetDensity( 6.50f );
	_thief6.GetBody()->GetFixtureList()->SetFriction( 0.50f );
	_thief6.GetBody()->GetFixtureList()->SetRestitution( 0.10f );
	_thief6.SetBodyType( b2_dynamicBody );

	// _wallBox1
	_wallBox1.GetBody()->GetFixtureList()->SetDensity( 8.0f );
	_wallBox1.GetBody()->GetFixtureList()->SetFriction( 0.80f );
	_wallBox1.GetBody()->GetFixtureList()->SetRestitution( 0.10f );
	_wallBox1.SetBodyType( b2_staticBody );
	_wallBox1.SetRotation( 90.0f );

	// _thief7
	_thief7.FlipX();
	_thief7.GetBody()->GetFixtureList()->SetDensity( 6.50f );
	_thief7.GetBody()->GetFixtureList()->SetFriction( 0.50f );
	_thief7.GetBody()->GetFixtureList()->SetRestitution( 0.10f );
	_thief7.SetBodyType( b2_dynamicBody );

	// _thief8
	
	_thief8.GetBody()->GetFixtureList()->SetDensity( 6.50f );
	_thief8.GetBody()->GetFixtureList()->SetFriction( 0.50f );
	_thief8.GetBody()->GetFixtureList()->SetRestitution( 0.10f );
	_thief8.SetBodyType( b2_dynamicBody );


	//Set blocks positions
	_coin1.SetPosition( 2.0f * 33.50f, 2.0f * 16.0f );
	_piggyBank1.SetPosition( 2.0f * 14.50f, 2.0f * 16.0f );
	
	
	
	_thief3.SetPosition( 2.0f * 33.50f, 2.0f * 10.0f );
	_thief1.SetPosition( 2.0f * 39.50f, 2.0f * 16.0f );
	_thief5.SetPosition( 2.0f * 33.50f, 2.0f * 22.050f );
	_thief4.SetPosition( 2.0f * 14.50f, 2.0f * 22.0f );
	_thief6.SetPosition( 2.0f * 8.50f, 2.0f * 16.0f );
	_thief2.SetPosition( 2.0f * 14.50f, 2.0f * 10.0f );
	
	_thief7.SetPosition( 2.0f * 27.50f, 2.0f * 16.0f );
	_thief8.SetPosition( 2.0f * 20.50f, 2.0f * 16.0f );

	_wallBox1.SetPosition( 2.0f * 24.0f, 2.0f * 16.0f );
	

	ConstFinal();
}
//--------------------------------------------------------------------------
LevelLetsRotate_7::LevelLetsRotate_7( GameTypes::LevelEnum levelEnum, bool viewMode ) : LevelAccelerometr( levelEnum, viewMode )
{
	_coinCount = 3;

	// _coin1
	_coin1.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin1.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin1.GetBody()->SetAngularDamping(0.1000f );
	_coin1.GetBody()->SetLinearDamping(0.1000f );
	_coin1.SetBodyType( b2_dynamicBody );

	// _coin2
	_coin2.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin2.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin2.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin2.GetBody()->SetAngularDamping(0.1000f );
	_coin2.GetBody()->SetLinearDamping(0.1000f );
	_coin2.SetBodyType( b2_dynamicBody );

	// _coin3
	_coin3.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin3.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin3.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin3.GetBody()->SetAngularDamping(0.1000f );
	_coin3.GetBody()->SetLinearDamping(0.1000f );
	_coin3.SetBodyType( b2_dynamicBody );

	// _piggyBank1
	_piggyBank1.GetBody()->GetFixtureList()->SetDensity( 7.5000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_piggyBank1.GetBody()->SetAngularDamping(0.1000f );
	_piggyBank1.GetBody()->SetLinearDamping(0.1000f );
	_piggyBank1.SetBodyType( b2_dynamicBody );

	// _thief1
	_thief1.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_thief1.GetBody()->GetFixtureList()->SetFriction( 0.2000f );
	_thief1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_thief1.GetBody()->SetAngularDamping(0.1000f );
	_thief1.GetBody()->SetLinearDamping(0.1000f );
	_thief1.SetBodyType( b2_dynamicBody );

	// _thief2
	_thief2.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_thief2.GetBody()->GetFixtureList()->SetFriction( 0.2000f );
	_thief2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_thief2.GetBody()->SetAngularDamping(0.1000f );
	_thief2.GetBody()->SetLinearDamping(0.1000f );
	_thief2.SetBodyType( b2_dynamicBody );

	// _thief3
	_thief3.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_thief3.GetBody()->GetFixtureList()->SetFriction( 0.2000f );
	_thief3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_thief3.GetBody()->SetAngularDamping(0.1000f );
	_thief3.GetBody()->SetLinearDamping(0.1000f );
	_thief3.FlipX();
	_thief3.SetBodyType( b2_dynamicBody );

	// _wallBox1
	_wallBox1.SetRotation( 270.0000f );
	_wallBox1.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox1.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox1.SetBodyType( b2_staticBody );

	// _wallBox2
	_wallBox2.SetRotation( 270.0000f );
	_wallBox2.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox2.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox2.SetBodyType( b2_staticBody );

	// _wallBox3
	_wallBox3.SetRotation( 90.0000f );
	_wallBox3.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox3.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox3.SetBodyType( b2_staticBody );

	// _wallBox4
	_wallBox4.SetRotation( 90.0000f );
	_wallBox4.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox4.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox4.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox4.SetBodyType( b2_staticBody );

	// _wallBox5
	_wallBox5.SetRotation( 210.0000f );
	_wallBox5.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox5.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox5.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox5.SetBodyType( b2_staticBody );

	// _wallBox6
	_wallBox6.SetRotation( 240.0000f );
	_wallBox6.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox6.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox6.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox6.SetBodyType( b2_staticBody );

	// _wallBox7
	_wallBox7.SetRotation( 300.0000f );
	_wallBox7.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox7.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox7.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox7.SetBodyType( b2_staticBody );

	// _wallBox8
	_wallBox8.SetRotation( 330.0000f );
	_wallBox8.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox8.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox8.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox8.SetBodyType( b2_staticBody );

	// _wallBox9
	_wallBox9.SetRotation( 390.0000f );
	_wallBox9.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox9.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox9.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox9.SetBodyType( b2_staticBody );

	// _wallBox10
	_wallBox10.SetRotation( 420.0000f );
	_wallBox10.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox10.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox10.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox10.SetBodyType( b2_staticBody );

	// _wallBox11
	_wallBox11.SetRotation( 120.0000f );
	_wallBox11.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox11.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox11.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox11.SetBodyType( b2_staticBody );

	// _wallBox12
	_wallBox12.SetRotation( 150.0000f );
	_wallBox12.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox12.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox12.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox12.SetBodyType( b2_staticBody );

	// _wallBox13
	_wallBox13.SetRotation( 180.0000f );
	_wallBox13.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox13.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox13.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox13.SetBodyType( b2_staticBody );

	// _wallBox14
	_wallBox14.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox14.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox14.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox14.SetBodyType( b2_staticBody );

	// _wallBox15
	_wallBox15.SetRotation( 90.0000f );
	_wallBox15.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox15.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox15.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox15.SetBodyType( b2_staticBody );

	// _wallBox16
	_wallBox16.SetRotation( 270.0000f );
	_wallBox16.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox16.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox16.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox16.SetBodyType( b2_staticBody );

	// _wallBox17
	_wallBox17.SetRotation( 270.0000f );
	_wallBox17.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox17.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox17.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox17.SetBodyType( b2_staticBody );

	// _wallBox18
	_wallBox18.SetRotation( 90.0000f );
	_wallBox18.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox18.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox18.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox18.SetBodyType( b2_staticBody );


	//Set blocks positions
	_coin1.SetPosition( 19.7000f, 18.2000f );
	_coin2.SetPosition( 25.7000f, 18.2000f );
	_coin3.SetPosition( 22.7000f, 23.4000f );
	_piggyBank1.SetPosition( 73.8500f, 21.3000f );
	_thief1.SetPosition( 43.5000f, 18.3000f );
	_thief2.SetPosition( 51.5000f, 18.3000f );
	_thief3.SetPosition( 47.5000f, 25.2000f );
	_wallBox1.SetPosition( 34.7500f, 46.8000f );
	_wallBox2.SetPosition( 60.7500f, 46.8000f );
	_wallBox3.SetPosition( 34.7500f, 17.3000f );
	_wallBox4.SetPosition( 60.7500f, 17.3000f );
	_wallBox5.SetPosition( 83.1000f, 11.7369f );
	_wallBox6.SetPosition( 84.7631f, 13.7500f );
	_wallBox7.SetPosition( 84.8131f, 50.3000f );
	_wallBox8.SetPosition( 82.8000f, 52.3131f );
	_wallBox9.SetPosition( 13.1000f, 52.1632f );
	_wallBox10.SetPosition( 11.2869f, 50.1500f );
	_wallBox11.SetPosition( 11.1369f, 14.0500f );
	_wallBox12.SetPosition( 13.4500f, 11.8869f );
	_wallBox13.SetPosition( 48.0025f, 53.3000f );
	_wallBox14.SetPosition( 48.0000f, 10.8000f );
	_wallBox15.SetPosition( 10.2262f, 19.3084f );
	_wallBox16.SetPosition( 10.2262f, 44.8916f );
	_wallBox17.SetPosition( 85.6764f, 44.7916f );
	_wallBox18.SetPosition( 85.6762f, 19.2084f );

	ConstFinal();
}
//--------------------------------------------------------------------------
LevelLetsRotate_Circle::LevelLetsRotate_Circle( GameTypes::LevelEnum levelEnum, bool viewMode ) : LevelAccelerometr( levelEnum, viewMode )
{
	_coinCount = 2;

	// _coin1
	_coin1.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin1.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin1.SetBodyType( b2_dynamicBody );

	// _piggyBank1
	_piggyBank1.GetBody()->GetFixtureList()->SetDensity( 7.5000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_piggyBank1.SetBodyType( b2_dynamicBody );

	// _wallBox1
	_wallBox1.SetRotation( 330.0000f );
	_wallBox1.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox1.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox1.SetBodyType( b2_staticBody );

	// _wallBox2
	_wallBox2.SetRotation( 395.4546f );
	_wallBox2.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox2.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox2.SetBodyType( b2_staticBody );

	// _wallBox3
	_wallBox3.SetRotation( 406.3636f );
	_wallBox3.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox3.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox3.SetBodyType( b2_staticBody );

	// _wallBox4
	_wallBox4.SetRotation( 417.2727f );
	_wallBox4.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox4.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox4.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox4.SetBodyType( b2_staticBody );

	// _wallBox5
	_wallBox5.SetRotation( 396.0000f );
	_wallBox5.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox5.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox5.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox5.SetBodyType( b2_staticBody );

	// _wallBox6
	_wallBox6.SetRotation( 378.0000f );
	_wallBox6.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox6.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox6.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox6.SetBodyType( b2_staticBody );

	// _wallBox7
	_wallBox7.SetRotation( 432.0000f );
	_wallBox7.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox7.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox7.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox7.SetBodyType( b2_staticBody );

	// _wallBox8
	_wallBox8.SetRotation( 414.0000f );
	_wallBox8.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox8.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox8.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox8.SetBodyType( b2_staticBody );

	// _wallBox9
	_wallBox9.SetRotation( 323.9999f );
	_wallBox9.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox9.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox9.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox9.SetBodyType( b2_staticBody );

	// _wallBox10
	_wallBox10.SetRotation( 342.0000f );
	_wallBox10.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox10.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox10.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox10.SetBodyType( b2_staticBody );

	// _wallBox11
	_wallBox11.SetRotation( 122.7273f );
	_wallBox11.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox11.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox11.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox11.SetBodyType( b2_staticBody );

	// _wallBox12
	_wallBox12.SetRotation( 360.0000f );
	_wallBox12.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox12.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox12.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox12.SetBodyType( b2_staticBody );

	// _wallBox13
	_wallBox13.SetRotation( 305.9999f );
	_wallBox13.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox13.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox13.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox13.SetBodyType( b2_staticBody );

	// _wallBox14
	_wallBox14.SetRotation( 288.0000f );
	_wallBox14.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox14.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox14.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox14.SetBodyType( b2_staticBody );

	// _wallBox15
	_wallBox15.SetRotation( 108.0000f );
	_wallBox15.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox15.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox15.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox15.SetBodyType( b2_staticBody );

	// _wallBox16
	_wallBox16.SetRotation( 90.0000f );
	_wallBox16.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox16.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox16.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox16.SetBodyType( b2_staticBody );

	// _wallBox17
	_wallBox17.SetRotation( 126.0000f );
	_wallBox17.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox17.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox17.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox17.SetBodyType( b2_staticBody );

	// _wallBox18
	_wallBox18.SetRotation( 144.0000f );
	_wallBox18.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox18.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox18.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox18.SetBodyType( b2_staticBody );

	// _wallBox19
	_wallBox19.SetRotation( 234.0000f );
	_wallBox19.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox19.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox19.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox19.SetBodyType( b2_staticBody );

	// _wallBox20
	_wallBox20.SetRotation( 252.0000f );
	_wallBox20.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox20.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox20.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox20.SetBodyType( b2_staticBody );

	// _wallBox21
	_wallBox21.SetRotation( 270.0000f );
	_wallBox21.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox21.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox21.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox21.SetBodyType( b2_staticBody );

	// _wallBox22
	_wallBox22.SetRotation( 133.6364f );
	_wallBox22.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox22.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox22.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox22.SetBodyType( b2_staticBody );

	// _wallBox23
	_wallBox23.SetRotation( 216.0000f );
	_wallBox23.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox23.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox23.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox23.SetBodyType( b2_staticBody );

	// _wallBox24
	_wallBox24.SetRotation( 198.0000f );
	_wallBox24.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox24.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox24.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox24.SetBodyType( b2_staticBody );

	// _wallBox25
	_wallBox25.SetRotation( 162.0000f );
	_wallBox25.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox25.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox25.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox25.SetBodyType( b2_staticBody );

	// _wallBox26
	_wallBox26.SetRotation( 180.0000f );
	_wallBox26.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox26.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox26.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox26.SetBodyType( b2_staticBody );

	// _wallBox27
	_wallBox27.SetRotation( 144.5455f );
	_wallBox27.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox27.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox27.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox27.SetBodyType( b2_staticBody );

	// _wallBox28
	_wallBox28.SetRotation( 210.0000f );
	_wallBox28.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox28.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox28.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox28.SetBodyType( b2_staticBody );

	// _wallBox29
	_wallBox29.SetRotation( 220.9091f );
	_wallBox29.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox29.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox29.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox29.SetBodyType( b2_staticBody );

	// _wallBox30
	_wallBox30.SetRotation( 231.8181f );
	_wallBox30.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox30.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox30.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox30.SetBodyType( b2_staticBody );

	// _wallBox31
	_wallBox31.SetRotation( 242.7273f );
	_wallBox31.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox31.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox31.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox31.SetBodyType( b2_staticBody );

	// _wallBox32
	_wallBox32.SetRotation( 308.1818f );
	_wallBox32.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox32.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox32.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox32.SetBodyType( b2_staticBody );

	// _wallBox33
	_wallBox33.SetRotation( 319.0909f );
	_wallBox33.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox33.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox33.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox33.SetBodyType( b2_staticBody );

	// _coin2
	_coin2.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin2.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin2.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin2.SetBodyType( b2_dynamicBody );


	//Set blocks positions
	_coin1.SetPosition( 59.1999f, 52.2000f );
	_piggyBank1.SetPosition( 59.1998f, 11.8000f );
	_wallBox1.SetPosition( 62.5000f, 57.1148f );
	_wallBox2.SetPosition( 31.1782f, 55.6227f );
	_wallBox3.SetPosition( 27.0116f, 52.0123f );
	_wallBox4.SetPosition( 23.6035f, 47.6786f );
	_wallBox5.SetPosition( 38.0077f, 45.7532f );
	_wallBox6.SetPosition( 42.7468f, 48.1679f );
	_wallBox7.SetPosition( 31.8321f, 37.2532f );
	_wallBox8.SetPosition( 34.2468f, 41.9923f );
	_wallBox9.SetPosition( 57.9925f, 45.7532f );
	_wallBox10.SetPosition( 53.2534f, 48.1679f );
	_wallBox11.SetPosition( 23.6035f, 16.3214f );
	_wallBox12.SetPosition( 48.0001f, 48.9999f );
	_wallBox13.SetPosition( 61.7535f, 41.9923f );
	_wallBox14.SetPosition( 64.1682f, 37.2532f );
	_wallBox15.SetPosition( 31.8321f, 26.7466f );
	_wallBox16.SetPosition( 31.0001f, 31.9999f );
	_wallBox17.SetPosition( 34.2468f, 22.0076f );
	_wallBox18.SetPosition( 38.0077f, 18.2466f );
	_wallBox19.SetPosition( 61.7535f, 22.0076f );
	_wallBox20.SetPosition( 64.1682f, 26.7466f );
	_wallBox21.SetPosition( 65.0002f, 31.9999f );
	_wallBox22.SetPosition( 27.0116f, 11.9877f );
	_wallBox23.SetPosition( 57.9925f, 18.2466f );
	_wallBox24.SetPosition( 53.2534f, 15.8320f );
	_wallBox25.SetPosition( 42.7468f, 15.8320f );
	_wallBox26.SetPosition( 48.0001f, 15.0000f );
	_wallBox27.SetPosition( 31.1782f, 8.3773f );
	_wallBox28.SetPosition( 62.5000f, 6.8853f );
	_wallBox29.SetPosition( 66.9910f, 10.0833f );
	_wallBox30.SetPosition( 70.7956f, 14.0734f );
	_wallBox31.SetPosition( 73.7763f, 18.7114f );
	_wallBox32.SetPosition( 70.7956f, 49.9266f );
	_wallBox33.SetPosition( 66.9910f, 53.9167f );
	_coin2.SetPosition( 28.1998f, 43.6000f );

	ConstFinal();
}
