#include "LevelAdam.h"

//--------------------------------------------------------------------------
LevelAdamS_PlainLogic::LevelAdamS_PlainLogic( GameTypes::LevelEnum levelEnum, bool viewMode ) : Level( levelEnum, viewMode )
{
    _levelTag = eLevelTag_Hacker;
	_coinCount = 2;

	// _bomb1
	_bomb1.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_bomb1.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_bomb1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bomb1.SetBodyType( b2_dynamicBody );

	// _coin1
	_coin1.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin1.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin1.SetBodyType( b2_dynamicBody );

	// _fragileBoxStatic1
	_fragileBoxStatic1.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBoxStatic1.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBoxStatic1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBoxStatic1.SetBodyType( b2_staticBody );

	// _fragileBoxStatic2
	_fragileBoxStatic2.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBoxStatic2.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBoxStatic2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBoxStatic2.SetBodyType( b2_staticBody );

	// _piggyBank1
	_piggyBank1.GetBody()->GetFixtureList()->SetDensity( 7.5000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_piggyBank1.SetBodyType( b2_dynamicBody );

	// _screw1
	_screw1.GetBody()->GetFixtureList()->SetDensity( 0.1000f );
	_screw1.GetBody()->GetFixtureList()->SetFriction( 0.1000f );
	_screw1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_screw1.SetBodyType( b2_staticBody );

	// _wallBox1
	_wallBox1.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox1.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox1.SetBodyType( b2_staticBody );

	// _wallBox2
	_wallBox2.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox2.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox2.SetBodyType( b2_staticBody );

	// _wallBox3
	_wallBox3.SetRotation( 191.6367f );
	_wallBox3.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox3.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox3.SetBodyType( b2_staticBody );

	// _wallBox4
	_wallBox4.SetRotation( 210.5842f );
	_wallBox4.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox4.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox4.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox4.SetBodyType( b2_staticBody );

	// _wallBox5
	_wallBox5.SetRotation( 229.5317f );
	_wallBox5.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox5.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox5.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox5.SetBodyType( b2_staticBody );

	// _wallBox6
	_wallBox6.SetRotation( 248.4790f );
	_wallBox6.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox6.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox6.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox6.SetBodyType( b2_staticBody );

	// _wallBox7
	_wallBox7.SetRotation( 267.4264f );
	_wallBox7.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox7.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox7.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox7.SetBodyType( b2_staticBody );

	// _woodBox1
	_woodBox1.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox1.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_woodBox1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_woodBox1.SetBodyType( b2_dynamicBody );

	// _woodBox2
	_woodBox2.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox2.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_woodBox2.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_woodBox2.SetBodyType( b2_dynamicBody );

	// _screw2
	_screw2.GetBody()->GetFixtureList()->SetDensity( 0.1000f );
	_screw2.GetBody()->GetFixtureList()->SetFriction( 0.1000f );
	_screw2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_screw2.SetBodyType( b2_staticBody );

	// _wallBox8
	_wallBox8.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox8.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox8.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox8.SetBodyType( b2_staticBody );

	// _woodBox3
	_woodBox3.SetRotation( 90.0000f );
	_woodBox3.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox3.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_woodBox3.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_woodBox3.SetBodyType( b2_dynamicBody );

	// _wallBox9
	_wallBox9.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox9.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox9.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox9.SetBodyType( b2_staticBody );

	// _woodBox4
	_woodBox4.SetRotation( 90.0000f );
	_woodBox4.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox4.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_woodBox4.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_woodBox4.SetBodyType( b2_dynamicBody );

	// _fragileBoxStatic3
	_fragileBoxStatic3.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBoxStatic3.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBoxStatic3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBoxStatic3.SetBodyType( b2_staticBody );

	// _bomb2
	_bomb2.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_bomb2.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_bomb2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bomb2.SetBodyType( b2_dynamicBody );

	// _coin2
	_coin2.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin2.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin2.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin2.SetBodyType( b2_dynamicBody );

	// _wallBox10
	_wallBox10.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox10.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox10.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox10.SetBodyType( b2_staticBody );

	// _wallBox11
	_wallBox11.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox11.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox11.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox11.SetBodyType( b2_staticBody );


	//Set blocks positions
	_bomb1.SetPosition( 8.5997f, 53.0000f );
	_coin1.SetPosition( 18.8995f, 52.9000f );
	_fragileBoxStatic1.SetPosition( 57.9001f, 33.6500f );
	_fragileBoxStatic2.SetPosition( 31.8503f, 21.5999f );
	_piggyBank1.SetPosition( 44.6498f, 34.9501f );
	_screw1.SetPosition( 37.8500f, 27.4000f );
	_wallBox1.SetPosition( 29.4003f, 27.4994f );
	_wallBox2.SetPosition( 34.5001f, 27.4994f );
	_wallBox3.SetPosition( 75.0741f, 28.2434f );
	_wallBox4.SetPosition( 80.6018f, 30.3775f );
	_wallBox5.SetPosition( 85.1371f, 34.1909f );
	_wallBox6.SetPosition( 88.1885f, 39.2702f );
	_wallBox7.SetPosition( 89.4253f, 45.0651f );
	_woodBox1.SetPosition( 44.8002f, 27.4000f );
	_woodBox2.SetPosition( 68.2005f, 45.5500f );
	_screw2.SetPosition( 61.2499f, 45.5500f );
	_wallBox8.SetPosition( 65.7000f, 27.6501f );
	_woodBox3.SetPosition( 58.1502f, 52.4502f );
	_wallBox9.SetPosition( 17.9502f, 46.4501f );
	_woodBox4.SetPosition( 31.9502f, 46.2501f );
	_fragileBoxStatic3.SetPosition( 46.0000f, 21.6000f );
	_bomb2.SetPosition( 8.5998f, 11.7497f );
	_coin2.SetPosition( 18.8995f, 11.6497f );
	_wallBox10.SetPosition( 32.8500f, 5.1500f );
	_wallBox11.SetPosition( 45.0000f, 46.4500f );

	//Bombs Construction
	_bomb1.SetRange( 16.0000f );
	_bomb1.SetBombImpulse( 8000.0000f );
	_bomb2.SetRange( 16.0000f );
	_bomb2.SetBombImpulse( 8000.0000f );

	ConstFinal();
	CreateJoints();
}
//--------------------------------------------------------------------------
void LevelAdamS_PlainLogic::CreateJoints()
{
	b2RevoluteJointDef jointRevDef1;
	jointRevDef1.collideConnected = false;
	jointRevDef1.Initialize( _screw1.GetBody(), _woodBox1.GetBody(), _screw1.GetPosition()  );
	_jointRev1 = (b2RevoluteJoint *) _world.CreateJoint( &jointRevDef1 );

	_jointRev1->SetLimits( -1.5500f, 0.0000f );
	_jointRev1->EnableLimit( true );
	_jointRev1->EnableMotor( true );
	_jointRev1->SetMotorSpeed( -0.1000f );
	_jointRev1->SetMaxMotorTorque( 4900.0000f );

	b2RevoluteJointDef jointRevDef2;
	jointRevDef2.collideConnected = false;
	jointRevDef2.Initialize( _screw2.GetBody(), _woodBox2.GetBody(), _screw2.GetPosition() );
	_jointRev2 = (b2RevoluteJoint *) _world.CreateJoint( &jointRevDef2 );

	_jointRev2->EnableMotor( true );
	_jointRev2->SetMotorSpeed( 0.0000f );
	_jointRev2->SetMaxMotorTorque( 0.0000f );

	b2GearJointDef jointGearDef1;
	jointGearDef1.bodyA = _woodBox1.GetBody();
	jointGearDef1.bodyB = _woodBox2.GetBody();
	jointGearDef1.joint1 = _jointRev1;
	jointGearDef1.joint2 = _jointRev2;
	jointGearDef1.ratio = 1;
	_jointGear1 = (b2GearJoint *) _world.CreateJoint( &jointGearDef1 );

}
//--------------------------------------------------------------------------
void LevelAdamS_PlainLogic::DestroyJoints()
{
	if ( _jointGear1 )	_world.DestroyJoint( _jointGear1 );
	if ( _jointRev1 )	_world.DestroyJoint( _jointRev1 );
	if ( _jointRev2 )	_world.DestroyJoint( _jointRev2 );
}
//--------------------------------------------------------------------------
LevelAdamS_PlainLogic::~LevelAdamS_PlainLogic()
{
	DestroyJoints();
}
//--------------------------------------------------------------------------
