#ifndef __LEVELTOMEKW_H__
#define __LEVELTOMEKW_H__

#include <Box2D/Box2D.h>
#include "Levels/Level.h"
#include "Blocks/AllBlocks.h"

//------------------------------------------------------------------
class LevelTomekW_Mapa3 : public Level
{
public:
	LevelTomekW_Mapa3( GameTypes::LevelEnum levelEnum, bool viewMode );

private:
	puCoin< 40 >	_coin1;
	puFragileBox<96,96,eBlockScaled>	_fragileBox1;
	puFragileBox<96,96,eBlockScaled>	_fragileBox2;
	puFragileBox<96,96,eBlockScaled>	_fragileBox3;
	puFragileBox<96,96,eBlockScaled>	_fragileBox4;
	puFragileBox<96,96,eBlockScaled>	_fragileBox5;
	puFragileBox<96,96,eBlockScaled>	_fragileBox6;
	puFragileBox<96,96,eBlockScaled>	_fragileBox7;
	puFragileBox<96,96,eBlockScaled>	_fragileBox8;
	puPiggyBankStatic< 60 >	_piggyBankStatic1;
	puWallBox<400,20,eBlockScaled>	_wallBox1;
	puWoodBox<300,20,eBlockScaled>	_woodBox1;
};
//------------------------------------------------------------------
class LevelTomekW_Prosty4 : public Level
{
public:
	LevelTomekW_Prosty4( GameTypes::LevelEnum levelEnum, bool viewMode );

private:
	puBomb< 40 >	_bomb1;
	puBomb< 44 >	_bomb2;
	puBorderAllClose	_borderAllClose1;
	puCoin< 40 >	_coin1;
	puFragileBoxStatic<96,96,eBlockScaled>	_fragileBoxStatic1;
	puFragileBoxStatic<96,96,eBlockScaled>	_fragileBoxStatic2;
	puFragileBoxStatic<96,96,eBlockScaled>	_fragileBoxStatic3;
	puFragileBoxStatic<96,96,eBlockScaled>	_fragileBoxStatic4;
	puPiggyBank< 60 >	_piggyBank1;
	puWallBox<100,15,eBlockScaled>	_wallBox1;
	puWallBox<100,15,eBlockScaled>	_wallBox2;
	puWallBox<200,15,eBlockScaled>	_wallBox3;
	puWallBox<200,15,eBlockScaled>	_wallBox4;
	puWallBox<200,15,eBlockScaled>	_wallBox5;
	puWallBox<400,15,eBlockScaled>	_wallBox6;
	puWallBox<60,10,eBlockScaled>	_wallBox10;
	puWallBox<60,10,eBlockScaled>	_wallBox12;
	puWallBox<60,10,eBlockScaled>	_wallBox7;
	puWallBox<60,12,eBlockScaled>	_wallBox8;
	puWallBox<60,12,eBlockScaled>	_wallBox9;
	puWallBox<90,2,eBlockScaled>	_wallBox11;
	puWoodBox<60,190,eBlockScaled>	_woodBox1;
	puWoodBox<85,15,eBlockScaled>	_woodBox2;
};
//------------------------------------------------------------------
class LevelTomekW_CalmWood : public Level
{
public:
	LevelTomekW_CalmWood( GameTypes::LevelEnum levelEnum, bool viewMode );

private:
	puBomb< 44 >	_bomb2;
	puButton< 44 >	_button1;
	puCoin< 40 >	_coin1;
	puFragileBoxStatic<96,96,eBlockScaled>	_fragileBoxStatic1;
	puFragileBoxStatic<96,96,eBlockScaled>	_fragileBoxStatic2;
	puFragileBoxStatic<96,96,eBlockScaled>	_fragileBoxStatic3;
	puPiggyBank< 60 >	_piggyBank1;
	puWallBox<100,20,eBlockScaled>	_wallBox11;
	puWallBox<160,20,eBlockScaled>	_wallBox1;
	puWallBox<160,20,eBlockScaled>	_wallBox2;
	puWallBox<20,20,eBlockScaled>	_wallBox3;
	puWallBox<200,20,eBlockScaled>	_wallBox10;
	puWallBox<50,20,eBlockScaled>	_wallBox4;
	puWallBox<50,20,eBlockScaled>	_wallBox5;
	puWallBox<50,20,eBlockScaled>	_wallBox6;
	puWallBox<50,20,eBlockScaled>	_wallBox7;
	puWallBox<50,20,eBlockScaled>	_wallBox8;
	puWallBox<50,20,eBlockScaled>	_wallBox9;
	puWoodBox<400,20,eBlockScaled>	_woodBox1;
};
//------------------------------------------------------------------
class LevelTomekW_SelfMadeCannon : public Level
{
public:
	LevelTomekW_SelfMadeCannon( GameTypes::LevelEnum levelEnum, bool viewMode );

private:
	puBomb< 44 >	_bomb1;
	puBomb< 44 >	_bomb2;
	puButton< 40 >	_button1;
	puCoin< 40 >	_coin1;
	puFragileBox<96,96,eBlockScaled>	_fragileBox1;
	puFragileBox<96,96,eBlockScaled>	_fragileBox2;
	puFragileBox<96,96,eBlockScaled>	_fragileBox3;
	puFragileBoxStatic<96,96,eBlockScaled>	_fragileBoxStatic1;
	puPiggyBank< 60 >	_piggyBank1;
	puWallBox<20,20,eBlockScaled>	_wallBox11;
	puWallBox<20,20,eBlockScaled>	_wallBox1;
	puWallBox<20,20,eBlockScaled>	_wallBox21;
	puWallBox<20,20,eBlockScaled>	_wallBox2;
	puWallBox<20,20,eBlockScaled>	_wallBox3;
	puWallBox<20,20,eBlockScaled>	_wallBox4;
	puWallBox<300,20,eBlockScaled>	_wallBox19;
	puWallBox<480,20,eBlockScaled>	_wallBox22;
	puWallBox<50,20,eBlockScaled>	_wallBox10;
	puWallBox<50,20,eBlockScaled>	_wallBox13;
	puWallBox<50,20,eBlockScaled>	_wallBox14;
	puWallBox<50,20,eBlockScaled>	_wallBox15;
	puWallBox<50,20,eBlockScaled>	_wallBox16;
	puWallBox<50,20,eBlockScaled>	_wallBox17;
	puWallBox<50,20,eBlockScaled>	_wallBox18;
	puWallBox<50,20,eBlockScaled>	_wallBox20;
	puWallBox<50,20,eBlockScaled>	_wallBox5;
	puWallBox<50,20,eBlockScaled>	_wallBox6;
	puWallBox<50,20,eBlockScaled>	_wallBox7;
	puWallBox<50,20,eBlockScaled>	_wallBox8;
	puWallBox<50,20,eBlockScaled>	_wallBox9;
	puWallBox<550,20,eBlockScaled>	_wallBox12;
	puWoodBox<100,20,eBlockScaled>	_woodBox1;
	puWoodBox<300,20,eBlockScaled>	_woodBox2;
};
//------------------------------------------------------------------

#endif
