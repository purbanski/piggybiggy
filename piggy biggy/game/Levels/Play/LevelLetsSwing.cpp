#include "LevelLetsSwing.h"

//--------------------------------------------------------------------------
LevelLetsSwing_1::LevelLetsSwing_1( GameTypes::LevelEnum levelEnum, bool viewMode ) : Level( levelEnum, viewMode )
{
	_coinCount = 2;
    _levelTag = eLevelTag_Thief;
    
	// _coin1
	_coin1.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin1.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin1.GetBody()->SetAngularDamping(0.0500f );
	_coin1.GetBody()->SetLinearDamping(0.0500f );
	_coin1.SetBodyType( b2_dynamicBody );

	// _coin2
	_coin2.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin2.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin2.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin2.GetBody()->SetAngularDamping(0.0500f );
	_coin2.GetBody()->SetLinearDamping(0.0500f );
	_coin2.SetBodyType( b2_dynamicBody );

	// _fragileBox1
	_fragileBox1.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox1.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox1.SetBodyType( b2_dynamicBody );

	// _fragileBoxStatic1
	_fragileBoxStatic1.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBoxStatic1.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBoxStatic1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBoxStatic1.SetBodyType( b2_staticBody );

	// _fragileBoxStatic2
	_fragileBoxStatic2.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBoxStatic2.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBoxStatic2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBoxStatic2.SetBodyType( b2_staticBody );

	// _piggyBank1
	_piggyBank1.GetBody()->GetFixtureList()->SetDensity( 7.5000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_piggyBank1.GetBody()->SetAngularDamping(0.1000f );
	_piggyBank1.GetBody()->SetLinearDamping(0.1000f );
	_piggyBank1.SetBodyType( b2_dynamicBody );

	// _swingNoFloor1
	b2Filter filter;
	filter.groupIndex = 0;
	filter.categoryBits = Config::eFilterCategorySwing;
	filter.maskBits = ~Config::eFilterCategorySwing & 0xffff;

	_swingNoFloor1.GetBody()->GetFixtureList()->SetDensity( 0.1000f );
	_swingNoFloor1.GetBody()->GetFixtureList()->SetFriction( 0.1000f );
	_swingNoFloor1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_swingNoFloor1.SetBodyType( b2_staticBody );
	_swingNoFloor1.FlipX();

	// _thiefStatic1
	_thiefStatic1.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_thiefStatic1.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_thiefStatic1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_thiefStatic1.SetBodyType( b2_staticBody );

	// _wallBox1
	_wallBox1.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox1.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox1.SetBodyType( b2_staticBody );

	// _wallBox2
	_wallBox2.SetRotation( 180.0f - 115.5000f );
	_wallBox2.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox2.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox2.SetBodyType( b2_staticBody );

	// _wallBox3
	_wallBox3.SetRotation( 180.0f - 70.5000f );
	_wallBox3.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox3.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox3.SetBodyType( b2_staticBody );

	// _wallBox4
	_wallBox4.SetRotation( 180.0f - 59.2500f );
	_wallBox4.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox4.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox4.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox4.SetBodyType( b2_staticBody );

	// _wallBox5
	_wallBox5.SetRotation( 180.0f - 41.7704f );
	_wallBox5.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox5.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox5.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox5.SetBodyType( b2_staticBody );

	// _wallBox6
	_wallBox6.SetRotation( 180.0f - 20.5940f );
	_wallBox6.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox6.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox6.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox6.SetBodyType( b2_staticBody );

	// _wallBox7
	_wallBox7.SetRotation( 180.0f - 159.4060f );
	_wallBox7.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox7.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox7.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox7.SetBodyType( b2_staticBody );

	// _wallBox8
	_wallBox8.SetRotation( 180.0f - 138.2296f );
	_wallBox8.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox8.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox8.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox8.SetBodyType( b2_staticBody );

	// _wallBox9
	_wallBox9.SetRotation( 180.0f - 109.5000f );
	_wallBox9.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox9.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox9.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox9.SetBodyType( b2_staticBody );

	// _wallBox10
	_wallBox10.SetRotation( 180.0f - 120.7500f );
	_wallBox10.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox10.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox10.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox10.SetBodyType( b2_staticBody );

	// _wallBox11
	_wallBox11.SetRotation( 180.0f - 64.5000f );
	_wallBox11.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox11.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox11.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox11.SetBodyType( b2_staticBody );

	// _wallBox12
	_wallBox12.SetRotation( 180.0f - 75.7500f );
	_wallBox12.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox12.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox12.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox12.SetBodyType( b2_staticBody );

	// _wallBox13
	_wallBox13.SetRotation( 180.0f - 87.0000f );
	_wallBox13.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox13.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox13.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox13.SetBodyType( b2_staticBody );

	// _wallBox14
	_wallBox14.SetRotation( 180.0f - 98.2501f );
	_wallBox14.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox14.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox14.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox14.SetBodyType( b2_staticBody );

	// _wallBox15
	_wallBox15.SetRotation( 180.0f - 93.0000f );
	_wallBox15.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox15.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox15.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox15.SetBodyType( b2_staticBody );

	// _wallBox16
	_wallBox16.SetRotation( 180.0f - 81.7499f );
	_wallBox16.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox16.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox16.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox16.SetBodyType( b2_staticBody );

	// _wallBox17
	_wallBox17.SetRotation( 180.0f - 104.2500f );
	_wallBox17.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox17.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox17.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox17.SetBodyType( b2_staticBody );

	// _wallBox18
	_wallBox18.SetRotation( 180.0f );
	_wallBox18.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox18.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox18.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox18.SetBodyType( b2_staticBody );


	//Set blocks positions
	_coin1.SetPosition(96.0f -  41.1499f, 56.2996f );
	_coin2.SetPosition( 96.0f - 49.3498f, 56.2996f );
	_fragileBox1.SetPosition( 96.0f - 67.2501f, 30.8998f );
	_fragileBoxStatic1.SetPosition( 96.0f - 49.3493f, 48.3497f );
	_fragileBoxStatic2.SetPosition(96.0f -  41.1492f, 48.3497f );
	_piggyBank1.SetPosition( 96.0f - 28.2000f, 10.6000f );
	_swingNoFloor1.SetPosition( 96.0f - 54.7502f, 40.6496f );
	_thiefStatic1.SetPosition(96.0f -  28.2000f, 25.0495f );
	_wallBox1.SetPosition( 96.0f - 55.5004f, 25.0495f );
	_wallBox2.SetPosition(96.0f -  89.0695f, 41.6378f );
	_wallBox3.SetPosition(96.0f -  90.2711f, 18.7083f );
	_wallBox4.SetPosition(96.0f -  87.7741f, 13.3838f );
	_wallBox5.SetPosition(96.0f -  83.9598f, 8.6919f );
	_wallBox6.SetPosition(96.0f -  78.6149f, 5.4570f );
	_wallBox7.SetPosition( 96.0f - 17.3849f, 5.5071f );
	_wallBox8.SetPosition(96.0f -  12.0401f, 8.7418f );
	_wallBox9.SetPosition( 96.0f - 5.7287f, 18.7583f );
	_wallBox10.SetPosition( 96.0f - 8.2257f, 13.4337f );
	_wallBox11.SetPosition(96.0f -  6.9304f, 41.6878f );
	_wallBox12.SetPosition(96.0f -  4.9310f, 36.1571f );
	_wallBox13.SetPosition( 96.0f - 4.0490f, 30.3426f );
	_wallBox14.SetPosition( 96.0f - 4.3184f, 24.4677f );
	_wallBox15.SetPosition( 96.0f - 91.9508f, 30.2927f );
	_wallBox16.SetPosition( 96.0f - 91.6815f, 24.4180f );
	_wallBox17.SetPosition(96.0f -  91.0688f, 36.1071f );
	_wallBox18.SetPosition( 96.0f - 48.0000f, 4.3500f );

	
	filter.groupIndex = 0;
	filter.categoryBits = Config::eFilterCategorySwing;
	filter.maskBits = ~Config::eFilterCategorySwing & 0xffff;
	_wallBox1.GetBody()->GetFixtureList()->SetFilterData( filter );


	ConstFinal();
}




//--------------------------------------------------------------------------
// Swing 2
//--------------------------------------------------------------------------
LevelLetsSwing_2::LevelLetsSwing_2( GameTypes::LevelEnum levelEnum, bool viewMode ) : Level( levelEnum, viewMode )
{
	_coinCount = 2;
    _levelTag = eLevelTag_Cop;
    
	// _coin1
	_coin1.GetBody()->GetFixtureList()->SetDensity( 3.0000f );
	_coin1.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_coin1.SetBodyType( b2_dynamicBody );

	// _coin2
	_coin2.GetBody()->GetFixtureList()->SetDensity( 4.5000f );
	_coin2.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin2.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin2.SetBodyType( b2_dynamicBody );

	// _fragileBox1
	_fragileBox1.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox1.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox1.GetBody()->GetFixtureList()->SetRestitution( 0.0500f );
	_fragileBox1.SetBodyType( b2_dynamicBody );

	// _fragileBox2
	_fragileBox2.GetBody()->GetFixtureList()->SetDensity( 5.0000f );
	_fragileBox2.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox2.SetBodyType( b2_dynamicBody );

	// _fragileBox3
	_fragileBox3.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox3.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox3.SetBodyType( b2_dynamicBody );

	// _boxerGlove1
	_boxerGlove1.SetRotation( 0.0000f );
	_boxerGlove1.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_boxerGlove1.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_boxerGlove1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_boxerGlove1.SetBodyType( b2_dynamicBody );

	// _thief1
	_thief1.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_thief1.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_thief1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_thief1.SetBodyType( b2_dynamicBody );

	// _boxerGlove2
	_boxerGlove2.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_boxerGlove2.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_boxerGlove2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_boxerGlove2.SetBodyType( b2_dynamicBody );

	// _boxerGlove3
	_boxerGlove3.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_boxerGlove3.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_boxerGlove3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_boxerGlove3.FlipX();
	_boxerGlove3.SetBodyType( b2_dynamicBody );

	// _boxerGlove4
	_boxerGlove4.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_boxerGlove4.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_boxerGlove4.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_boxerGlove4.FlipX();
	_boxerGlove4.SetBodyType( b2_dynamicBody );

	// _cop1
	_cop1.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_cop1.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_cop1.GetBody()->GetFixtureList()->SetRestitution( 0.3500f );
	_cop1.SetBodyType( b2_dynamicBody );

	// _wallBox1
	_wallBox1.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox1.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_wallBox1.SetBodyType( b2_staticBody );

	// _wallBox2
	_wallBox2.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox2.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox2.SetBodyType( b2_staticBody );

	// _wallBox3
	_wallBox3.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox3.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox3.SetBodyType( b2_staticBody );

	// _wallBox4
	_wallBox4.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox4.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox4.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox4.SetBodyType( b2_staticBody );

	// _piggyBankStatic1
	_piggyBankStatic1.GetBody()->GetFixtureList()->SetDensity( 7.5000f );
	_piggyBankStatic1.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_piggyBankStatic1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_piggyBankStatic1.SetBodyType( b2_staticBody );

	// _wallBox5
	_wallBox5.SetRotation( 2.0000f );
	_wallBox5.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox5.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox5.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox5.SetBodyType( b2_staticBody );


	//Set blocks positions
	_coin1.SetPosition( 25.3499f, 35.0001f );
	_coin2.SetPosition( 25.3499f, 56.4999f );
	_fragileBox1.SetPosition( 38.6006f, 20.7000f );
	_fragileBox2.SetPosition( 36.6508f, 10.2998f );
	_fragileBox3.SetPosition( 46.3508f, 10.2999f );
	_boxerGlove1.SetPosition( 83.5006f, 54.6499f );
	_thief1.SetPosition( 70.6001f, 36.6002f );
	_boxerGlove2.SetPosition( 83.5006f, 33.3000f );
	_boxerGlove3.SetPosition( 12.4504f, 54.6499f );
	_boxerGlove4.SetPosition( 12.4504f, 33.2998f );
	_cop1.SetPosition( 70.6001f, 58.0000f );
	_wallBox1.SetPosition( 25.3499f, 49.2499f );
	_wallBox2.SetPosition( 70.6001f, 49.2499f );
	_wallBox3.SetPosition( 70.6001f, 27.8999f );
	_wallBox4.SetPosition( 25.3499f, 27.8999f );
	_piggyBankStatic1.SetPosition( 83.4999f, 12.1999f );
	_wallBox5.SetPosition( 48.0001f, 3.8500f );

	//Boxer Gloves Construction
	_boxerGlove1.SetBoxerForce( 22500.0000f );
	_boxerGlove2.SetBoxerForce( 15000.0000f );
	_boxerGlove3.SetBoxerForce( 15000.0000f );
	_boxerGlove4.SetBoxerForce( 17500.0000f );

	ConstFinal();
}


//--------------------------------------------------------------------------
// Swing 3
//--------------------------------------------------------------------------
LevelLetsSwing_3::LevelLetsSwing_3( GameTypes::LevelEnum levelEnum, bool viewMode ) : Level( levelEnum, viewMode )
{
	b2Filter filter;
	_coinCount = 3;
    _levelTag = eLevelTag_Boxing;
	
	// _coin1
	_coin1.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin1.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin1.SetBodyType( b2_dynamicBody );

	// _coin2
	_coin2.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin2.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin2.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin2.SetBodyType( b2_dynamicBody );

	// _coin3
	_coin3.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin3.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin3.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin3.SetBodyType( b2_dynamicBody );

	// _fragileBox1
	_fragileBox1.GetBody()->GetFixtureList()->SetDensity( 3.4000f );
	_fragileBox1.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_fragileBox1.GetBody()->GetFixtureList()->SetRestitution( 0.3000f );
	_fragileBox1.SetBodyType( b2_dynamicBody );

	// _fragileBox2
	_fragileBox2.GetBody()->GetFixtureList()->SetDensity( 2.3000f );
	_fragileBox2.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_fragileBox2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox2.SetBodyType( b2_dynamicBody );

	// _fragileBox3
	_fragileBox3.GetBody()->GetFixtureList()->SetDensity( 2.3000f );
	_fragileBox3.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_fragileBox3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox3.SetBodyType( b2_dynamicBody );

	// _fragileBox4
	_fragileBox4.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox4.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox4.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox4.SetBodyType( b2_dynamicBody );

	// _fragileBox5
	_fragileBox5.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox5.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox5.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox5.SetBodyType( b2_dynamicBody );

	// _fragileBoxStatic1
	_fragileBoxStatic1.SetRotation( -48.6000f );
	_fragileBoxStatic1.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBoxStatic1.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBoxStatic1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBoxStatic1.SetBodyType( b2_staticBody );

	// _piggyBank1
	_piggyBank1.GetBody()->GetFixtureList()->SetDensity( 7.5000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_piggyBank1.FlipX();
	_piggyBank1.SetBodyType( b2_dynamicBody );

	// _screw1
	_screw1.GetBody()->GetFixtureList()->SetDensity( 0.1000f );
	_screw1.GetBody()->GetFixtureList()->SetFriction( 0.1000f );
	_screw1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	filter.groupIndex = -1;
	filter.categoryBits = 0;
	filter.maskBits = 65535;
	_screw1.GetBody()->GetFixtureList()->SetFilterData( filter );
	_screw1.SetBodyType( b2_staticBody );

	// _screw2
	_screw2.SetRotation( -180.0000f );
	_screw2.GetBody()->GetFixtureList()->SetDensity( 0.1000f );
	_screw2.GetBody()->GetFixtureList()->SetFriction( 0.1000f );
	_screw2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	filter.groupIndex = -1;
	filter.categoryBits = 0;
	filter.maskBits = 65535;
	_screw2.GetBody()->GetFixtureList()->SetFilterData( filter );
	_screw2.SetBodyType( b2_staticBody );

	// _screw3
	_screw3.SetRotation( -243.3000f );
	_screw3.GetBody()->GetFixtureList()->SetDensity( 0.1000f );
	_screw3.GetBody()->GetFixtureList()->SetFriction( 0.1000f );
	_screw3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	filter.groupIndex = -1;
	filter.categoryBits = 0;
	filter.maskBits = 65535;
	_screw3.GetBody()->GetFixtureList()->SetFilterData( filter );
	_screw3.SetBodyType( b2_staticBody );

	// _swingNoFloor1
	_swingNoFloor1.GetBody()->GetFixtureList()->SetDensity( 0.1000f );
	_swingNoFloor1.GetBody()->GetFixtureList()->SetFriction( 0.1000f );
	_swingNoFloor1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_swingNoFloor1.SetBodyType( b2_staticBody );

	// _swingNoFloor2
	_swingNoFloor2.GetBody()->GetFixtureList()->SetDensity( 0.1000f );
	_swingNoFloor2.GetBody()->GetFixtureList()->SetFriction( 0.1000f );
	_swingNoFloor2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_swingNoFloor2.SetBodyType( b2_staticBody );

	// _wallBox1
	_wallBox1.SetRotation( 180.0000f );
	_wallBox1.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox1.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox1.SetBodyType( b2_staticBody );

	// _wallBox2
	_wallBox2.SetRotation( 203.3666f );
	_wallBox2.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox2.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox2.SetBodyType( b2_staticBody );

	// _wallBox3
	_wallBox3.SetRotation( 190.0333f );
	_wallBox3.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox3.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox3.SetBodyType( b2_staticBody );

	// _wallBox4
	_wallBox4.SetRotation( 180.0000f );
	_wallBox4.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox4.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox4.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox4.SetBodyType( b2_staticBody );

	// _wallBox5
	_wallBox5.SetRotation( 177.3333f );
	_wallBox5.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox5.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox5.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox5.SetBodyType( b2_staticBody );

	// _wallBox6
	_wallBox6.SetRotation( 156.6667f );
	_wallBox6.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox6.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox6.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox6.SetBodyType( b2_staticBody );

	// _wallBox7
	_wallBox7.SetRotation( 170.0000f );
	_wallBox7.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox7.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox7.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox7.SetBodyType( b2_staticBody );

	// _wallBox8
	_wallBox8.SetRotation( 143.3333f );
	_wallBox8.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox8.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox8.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox8.SetBodyType( b2_staticBody );

	// _wallBox9
	_wallBox9.SetRotation( 130.0000f );
	_wallBox9.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox9.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox9.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox9.SetBodyType( b2_staticBody );

	// _wallBox10
	_wallBox10.SetRotation( 116.6667f );
	_wallBox10.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox10.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox10.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox10.SetBodyType( b2_staticBody );

	// _wallBox11
	_wallBox11.SetRotation( 243.3665f );
	_wallBox11.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox11.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox11.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox11.SetBodyType( b2_staticBody );

	// _wallBox12
	_wallBox12.SetRotation( 230.0331f );
	_wallBox12.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox12.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox12.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox12.SetBodyType( b2_staticBody );

	// _wallBox13
	_wallBox13.SetRotation( 216.7000f );
	_wallBox13.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox13.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox13.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox13.SetBodyType( b2_staticBody );

	// _wallBox14
	_wallBox14.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox14.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox14.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox14.SetBodyType( b2_staticBody );

	// _woodBox1
	_woodBox1.SetRotation( -116.4f );
	_woodBox1.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox1.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_woodBox1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_woodBox1.SetBodyType( b2_dynamicBody );

	// _woodBox2
	_woodBox2.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox2.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_woodBox2.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_woodBox2.SetBodyType( b2_dynamicBody );

	// _woodBox3
	_woodBox3.SetRotation( -180.0000f );
	_woodBox3.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox3.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_woodBox3.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_woodBox3.SetBodyType( b2_dynamicBody );


	//Set blocks positions
	_coin1.SetPosition( 47.9000f, 44.1500f );
	_coin2.SetPosition( 72.2500f, 44.1500f );
	_coin3.SetPosition( 24.0000f, 44.1500f );
	_fragileBox1.SetPosition( 59.5000f, 51.9000f );
	_fragileBox2.SetPosition( 63.9501f, 44.1500f );
	_fragileBox3.SetPosition( 56.6499f, 44.1500f );
	_fragileBox4.SetPosition( 66.3006f, 11.7000f );
	_fragileBox5.SetPosition( 38.5000f, 44.1500f );
	_fragileBoxStatic1.SetPosition( 13.1000f, 18.8500f );
	_piggyBank1.SetPosition( 7.3000f, 26.3500f );
	_screw1.SetPosition( 5.0000f, 38.1500f );
	_screw2.SetPosition( 91.0000f, 38.1501f );
	_screw3.SetPosition( 22.90f, 21.1f );
	_swingNoFloor1.SetPosition( 54.2998f, 21.2000f );
	_swingNoFloor2.SetPosition( 26.9000f, 53.6500f );
	_wallBox1.SetPosition( 48.0500f, 5.7923f );
	_wallBox2.SetPosition( 79.3209f, 8.1209f );
	_wallBox3.SetPosition( 73.7607f, 6.4528f );
	_wallBox4.SetPosition( 68.1000f, 5.7923f );
	_wallBox5.SetPosition( 28.0035f, 6.0423f );
	_wallBox6.SetPosition( 16.6478f, 8.4447f );
	_wallBox7.SetPosition( 22.2086f, 6.7798f );
	_wallBox8.SetPosition( 11.6209f, 11.3471f );
	_wallBox9.SetPosition( 7.3989f, 15.3303f );
	_wallBox10.SetPosition( 4.2092f, 20.1800f );
	_wallBox11.SetPosition( 91.7530f, 19.8635f );
	_wallBox12.SetPosition( 88.5662f, 15.0121f );
	_wallBox13.SetPosition( 84.3462f, 11.0262f );
	_wallBox14.SetPosition( 48.0000f, 38.1500f );
	_woodBox1.SetPosition( 20.770f, 16.796f );
	_woodBox2.SetPosition( 12.0000f, 38.1500f );
	_woodBox3.SetPosition( 84.0002f, 38.1500f );

	filter.groupIndex = 0;
	filter.categoryBits = Config::eFilterCategorySwing;
	filter.maskBits = ~Config::eFilterCategorySwing & 0xffff;
	
	_wallBox1.GetBody()->GetFixtureList()->SetFilterData( filter );
	_wallBox14.GetBody()->GetFixtureList()->SetFilterData( filter );
	_woodBox2.GetBody()->GetFixtureList()->SetFilterData( filter );

	ConstFinal();
	CreateJoints();
}
//--------------------------------------------------------------------------
void LevelLetsSwing_3::CreateJoints()
{
	b2RevoluteJointDef jointRevDef1;
	jointRevDef1.collideConnected = false;
	jointRevDef1.Initialize( _screw1.GetBody(), _woodBox2.GetBody(), _screw1.GetPosition() );
	_jointRev1 = (b2RevoluteJoint *) _world.CreateJoint( &jointRevDef1 );

	_jointRev1->SetLimits( 0.0000f, 2.1000f );
	_jointRev1->EnableLimit( true );
	_jointRev1->EnableMotor( true );
	_jointRev1->SetMotorSpeed( 0.0000f );
	_jointRev1->SetMaxMotorTorque( 0.0000f );

	b2RevoluteJointDef jointRevDef2;
	jointRevDef2.collideConnected = false;
	jointRevDef2.Initialize( _screw2.GetBody(), _woodBox3.GetBody(), _screw2.GetPosition() );
	_jointRev2 = (b2RevoluteJoint *) _world.CreateJoint( &jointRevDef2 );

	_jointRev2->SetLimits( -1.5500f, -0.0000f );
	_jointRev2->EnableLimit( true );
	_jointRev2->EnableMotor( true );
	_jointRev2->SetMotorSpeed( 0.0000f );
	_jointRev2->SetMaxMotorTorque( 0.0000f );

	b2RevoluteJointDef jointRevDef3;
	jointRevDef3.collideConnected = false;
	jointRevDef3.Initialize( _screw3.GetBody(), _woodBox1.GetBody(), _screw3.GetPosition() );
	_jointRev3 = (b2RevoluteJoint *) _world.CreateJoint( &jointRevDef3 );

	_jointRev3->SetLimits( 0.0000f, 2.0000f );
	_jointRev3->EnableLimit( true );
	_jointRev3->EnableMotor( true );
	_jointRev3->SetMotorSpeed( 1.8000f );
	_jointRev3->SetMaxMotorTorque( 15100.0000f );

	b2GearJointDef jointGearDef1;
	jointGearDef1.bodyA = _woodBox1.GetBody();
	jointGearDef1.bodyB = _woodBox3.GetBody();
	jointGearDef1.joint1 = _jointRev1;
	jointGearDef1.joint2 = _jointRev3;
	jointGearDef1.ratio = -0.77f;
	_jointGear1 = (b2GearJoint *) _world.CreateJoint( &jointGearDef1 );

	b2GearJointDef jointGearDef2;
	jointGearDef2.bodyA = _woodBox2.GetBody();
	jointGearDef2.bodyB = _woodBox3.GetBody();
	jointGearDef2.joint1 = _jointRev2;
	jointGearDef2.joint2 = _jointRev3;
	jointGearDef2.ratio = 0.77f;
	_jointGear2 = (b2GearJoint *) _world.CreateJoint( &jointGearDef2 );

}
//--------------------------------------------------------------------------
LevelLetsSwing_3::~LevelLetsSwing_3()
{

}
//--------------------------------------------------------------------------
