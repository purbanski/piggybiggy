#ifndef __LEVELBOGUSIA_H__
#define __LEVELBOGUSIA_H__

#include <Box2D/Box2D.h>
#include "Levels/Level.h"
#include "Blocks/AllBlocks.h"

//------------------------------------------------------------------
class LevelBogusia_1 : public Level
{
public:
	LevelBogusia_1( GameTypes::LevelEnum levelEnum, bool viewMode );

private:
	puButton< 60 >	_button1;
	puCoin< 40 >	_coin1;
	puFragileBox<192,96,eBlockScaled>	_fragileBox1;
	puFragileBox<96,96,eBlockScaled>	_fragileBox2;
	puFragileBoxStatic<96,96,eBlockScaled>	_fragileBoxStatic1;
	puPiggyBank< 60 >	_piggyBank1;
	puWallBox<480,20,eBlockScaled>	_wallBox1;
	puWallBox<50,20,eBlockScaled>	_wallBox2;
	puWallBox<50,20,eBlockScaled>	_wallBox3;
	puWallBox<50,20,eBlockScaled>	_wallBox4;
	puWallBox<50,20,eBlockScaled>	_wallBox5;
	puWallBox<50,20,eBlockScaled>	_wallBox6;
	puWallBox<50,20,eBlockScaled>	_wallBox7;
	puWallBox<50,20,eBlockScaled>	_wallBox8;
	puWallBox<50,20,eBlockScaled>	_wallBox9;
};
//------------------------------------------------------------------
class LevelBogusia_Etap3a : public Level
{
public:
	LevelBogusia_Etap3a( GameTypes::LevelEnum levelEnum, bool viewMode );

private:
	puCoin< 40 >	_coin1;
	puFragileBox<192,96,eBlockScaled>	_fragileBox1;
	puFragileBox<192,96,eBlockScaled>	_fragileBox2;
	puFragileBox<96,96,eBlockScaled>	_fragileBox3;
	puFragileBox<96,96,eBlockScaled>	_fragileBox4;
	puFragileBox<96,96,eBlockScaled>	_fragileBox5;
	puFragileBoxStatic<192,96,eBlockScaled>	_fragileBoxStatic1;
	puFragileBoxStatic<192,96,eBlockScaled>	_fragileBoxStatic2;
	puPiggyBank< 60 >	_piggyBank1;
	puWallBox<860,20,eBlockScaled>	_wallBox1;
	puWoodBox<240,20,eBlockScaled>	_woodBox1;
	puWoodBox<240,20,eBlockScaled>	_woodBox2;
};
//------------------------------------------------------------------
class LevelBogusia_Etap3b : public Level
{
public:
	LevelBogusia_Etap3b( GameTypes::LevelEnum levelEnum, bool viewMode );

private:
	puCoin< 20 >	_coin1;
	puFloor2	_floor21;
	puFragileBox<48,48,eBlockScaled>	_fragileBox1;
	puFragileBox<48,48,eBlockScaled>	_fragileBox2;
	puFragileBox<48,48,eBlockScaled>	_fragileBox5;
	puFragileBox<48,48,eBlockScaled>	_fragileBox6;
	puFragileBox<48,96,eBlockScaled>	_fragileBox3;
	puFragileBox<48,96,eBlockScaled>	_fragileBox4;
	puFragileBoxStatic<48,48,eBlockScaled>	_fragileBoxStatic1;
	puFragileBoxStatic<48,48,eBlockScaled>	_fragileBoxStatic2;
	puPiggyBank< 30 >	_piggyBank1;
	puWallBox<48,48,eBlockScaled>	_wallBox1;
	puWoodBox<120,10,eBlockScaled>	_woodBox1;
	puWoodBox<125,10,eBlockScaled>	_woodBox2;
};
//------------------------------------------------------------------
class LevelBogusia_Etap7 : public Level
{
public:
	LevelBogusia_Etap7( GameTypes::LevelEnum levelEnum, bool viewMode );

private:
	puBomb< 44 >	_bomb1;
	puCoin< 40 >	_coin1;
	puCoin< 40 >	_coin2;
	puFragileBoxStatic<192,96,eBlockScaled>	_fragileBoxStatic1;
	puFragileBoxStatic<96,96,eBlockScaled>	_fragileBoxStatic2;
	puFragileBoxStatic<96,96,eBlockScaled>	_fragileBoxStatic3;
	puFragileBoxStatic<96,96,eBlockScaled>	_fragileBoxStatic4;
	puFragileBoxStatic<96,96,eBlockScaled>	_fragileBoxStatic5;
	puPiggyBank< 60 >	_piggyBank1;
	puWallBox<860,20,eBlockScaled>	_wallBox1;
	puWoodBox<340,20,eBlockScaled>	_woodBox1;
};
//------------------------------------------------------------------

#endif
