#ifndef __LEVELRAFAL_H__
#define __LEVELRAFAL_H__

#include <Box2D/Box2D.h>
#include "Levels/Level.h"
#include "Blocks/AllBlocks.h"

class LevelPathFinder_1 : public Level
{
public:
	LevelPathFinder_1( GameTypes::LevelEnum levelEnum, bool viewMode );

private:
	puCircleFragileStatic< 30 >	_circleFragileStatic10;
	puCircleFragileStatic< 30 >	_circleFragileStatic11;
	puCircleFragileStatic< 30 >	_circleFragileStatic12;
	puCircleFragileStatic< 30 >	_circleFragileStatic13;
	puCircleFragileStatic< 30 >	_circleFragileStatic14;
	puCircleFragileStatic< 30 >	_circleFragileStatic15;
	puCircleFragileStatic< 30 >	_circleFragileStatic16;
	puCircleFragileStatic< 30 >	_circleFragileStatic17;
	puCircleFragileStatic< 30 >	_circleFragileStatic18;
	puCircleFragileStatic< 30 >	_circleFragileStatic19;
	puCircleFragileStatic< 30 >	_circleFragileStatic1;
	puCircleFragileStatic< 30 >	_circleFragileStatic20;
	puCircleFragileStatic< 30 >	_circleFragileStatic21;
	puCircleFragileStatic< 30 >	_circleFragileStatic22;
	puCircleFragileStatic< 30 >	_circleFragileStatic23;
	puCircleFragileStatic< 30 >	_circleFragileStatic24;
	puCircleFragileStatic< 30 >	_circleFragileStatic25;
	puCircleFragileStatic< 30 >	_circleFragileStatic26;
	puCircleFragileStatic< 30 >	_circleFragileStatic27;
	puCircleFragileStatic< 30 >	_circleFragileStatic28;
	puCircleFragileStatic< 30 >	_circleFragileStatic29;
	puCircleFragileStatic< 30 >	_circleFragileStatic2;
	puCircleFragileStatic< 30 >	_circleFragileStatic30;
	puCircleFragileStatic< 30 >	_circleFragileStatic31;
	puCircleFragileStatic< 30 >	_circleFragileStatic32;
	puCircleFragileStatic< 30 >	_circleFragileStatic33;
	puCircleFragileStatic< 30 >	_circleFragileStatic34;
	puCircleFragileStatic< 30 >	_circleFragileStatic35;
	puCircleFragileStatic< 30 >	_circleFragileStatic36;
	puCircleFragileStatic< 30 >	_circleFragileStatic37;
	puCircleFragileStatic< 30 >	_circleFragileStatic38;
	puCircleFragileStatic< 30 >	_circleFragileStatic39;
	puCircleFragileStatic< 30 >	_circleFragileStatic3;
	puCircleFragileStatic< 30 >	_circleFragileStatic40;
	puCircleFragileStatic< 30 >	_circleFragileStatic41;
	puCircleFragileStatic< 30 >	_circleFragileStatic42;
	puCircleFragileStatic< 30 >	_circleFragileStatic43;
	puCircleFragileStatic< 30 >	_circleFragileStatic44;
	puCircleFragileStatic< 30 >	_circleFragileStatic45;
	puCircleFragileStatic< 30 >	_circleFragileStatic46;
	puCircleFragileStatic< 30 >	_circleFragileStatic47;
	puCircleFragileStatic< 30 >	_circleFragileStatic48;
	puCircleFragileStatic< 30 >	_circleFragileStatic49;
	puCircleFragileStatic< 30 >	_circleFragileStatic4;
	puCircleFragileStatic< 30 >	_circleFragileStatic50;
	puCircleFragileStatic< 30 >	_circleFragileStatic5;
	puCircleFragileStatic< 30 >	_circleFragileStatic6;
	puCircleFragileStatic< 30 >	_circleFragileStatic7;
	puCircleFragileStatic< 30 >	_circleFragileStatic8;
	puCircleFragileStatic< 30 >	_circleFragileStatic9;
	puCircleWall< 30 >	_circleWall1;
	puCircleWall< 30 >	_circleWall2;
	puCoin< 35 >	_coin1;
	puCoin< 35 >	_coin2;
	puCoin< 35 >	_coin3;
	puCoin< 35 >	_coin4;
	puCoin< 35 >	_coin5;
	puPiggyBank< 50 >	_piggyBank1;
	puWallBox<100,20,eBlockScaled>	_wallBox1;
	puWallBox<100,20,eBlockScaled>	_wallBox2;
	puWallBox<380,20,eBlockScaled>	_wallBox3;
	puWallBox<380,20,eBlockScaled>	_wallBox4;
};
//------------------------------------------------------------------
class LevelScale_3 : public Level
{
public:
	LevelScale_3( GameTypes::LevelEnum levelEnum, bool viewMode );

private:
	puCircleWall< 50 >	_circleWall1;
	puCoin< 40 >	_coin1;
	puFragileBox<192,96,eBlockScaled>	_fragileBox1;
	puFragileBox<192,96,eBlockScaled>	_fragileBox2;
	puFragileBox<192,96,eBlockScaled>	_fragileBox3;
	puFragileBox<96,96,eBlockScaled>	_fragileBox4;
	puFragileBox<96,96,eBlockScaled>	_fragileBox5;
	puFragileBox<96,96,eBlockScaled>	_fragileBox6;
	puFragileBox<96,96,eBlockScaled>	_fragileBox7;
	puFragileBox<96,96,eBlockScaled>	_fragileBox8;
	puFragileBox<96,96,eBlockScaled>	_fragileBox9;
	puPiggyBank< 60 >	_piggyBank1;
	puWallBox<200,20,eBlockScaled>	_wallBox1;
	puWallBox<35,20,eBlockScaled>	_wallBox10;
	puWallBox<35,20,eBlockScaled>	_wallBox2;
	puWallBox<35,20,eBlockScaled>	_wallBox3;
	puWallBox<35,20,eBlockScaled>	_wallBox4;
	puWallBox<35,20,eBlockScaled>	_wallBox5;
	puWallBox<35,20,eBlockScaled>	_wallBox6;
	puWallBox<35,20,eBlockScaled>	_wallBox7;
	puWallBox<35,20,eBlockScaled>	_wallBox8;
	puWallBox<35,20,eBlockScaled>	_wallBox9;
	puWoodBox<600,20,eBlockScaled>	_woodBox1;
};
//------------------------------------------------------------------
class LevelBombAndRoll_1 : public Level
{
public:
	LevelBombAndRoll_1( GameTypes::LevelEnum levelEnum, bool viewMode );

private:
	puBomb< 44 >	_bomb1;
	puBomb< 44 >	_bomb2;
	puBomb< 44 >	_bomb3;
	puBomb< 44 >	_bomb4;
	puCoin< 70 >	_coin1;
	puCoin< 70 >	_coin2;
	puFragileBoxStatic<96,96,eBlockScaled>	_fragileBoxStatic1;
	puFragileBoxStatic<96,96,eBlockScaled>	_fragileBoxStatic2;
	puFragileBoxStatic<96,96,eBlockScaled>	_fragileBoxStatic3;
	puFragileBoxStatic<96,96,eBlockScaled>	_fragileBoxStatic4;
	puPiggyBank< 60 >	_piggyBank1;
	puWallBox<35,20,eBlockScaled>	_wallBox10;
	puWallBox<35,20,eBlockScaled>	_wallBox11;
	puWallBox<35,20,eBlockScaled>	_wallBox12;
	puWallBox<35,20,eBlockScaled>	_wallBox13;
	puWallBox<35,20,eBlockScaled>	_wallBox14;
	puWallBox<35,20,eBlockScaled>	_wallBox15;
	puWallBox<35,20,eBlockScaled>	_wallBox16;
	puWallBox<35,20,eBlockScaled>	_wallBox17;
	puWallBox<35,20,eBlockScaled>	_wallBox18;
	puWallBox<35,20,eBlockScaled>	_wallBox19;
	puWallBox<35,20,eBlockScaled>	_wallBox1;
	puWallBox<35,20,eBlockScaled>	_wallBox20;
	puWallBox<35,20,eBlockScaled>	_wallBox21;
	puWallBox<35,20,eBlockScaled>	_wallBox22;
	puWallBox<35,20,eBlockScaled>	_wallBox23;
	puWallBox<35,20,eBlockScaled>	_wallBox24;
	puWallBox<35,20,eBlockScaled>	_wallBox25;
	puWallBox<35,20,eBlockScaled>	_wallBox26;
	puWallBox<35,20,eBlockScaled>	_wallBox27;
	puWallBox<35,20,eBlockScaled>	_wallBox28;
	puWallBox<35,20,eBlockScaled>	_wallBox29;
	puWallBox<35,20,eBlockScaled>	_wallBox2;
	puWallBox<35,20,eBlockScaled>	_wallBox30;
	puWallBox<35,20,eBlockScaled>	_wallBox31;
	puWallBox<35,20,eBlockScaled>	_wallBox32;
	puWallBox<35,20,eBlockScaled>	_wallBox33;
	puWallBox<35,20,eBlockScaled>	_wallBox34;
	puWallBox<35,20,eBlockScaled>	_wallBox35;
	puWallBox<35,20,eBlockScaled>	_wallBox36;
	puWallBox<35,20,eBlockScaled>	_wallBox37;
	puWallBox<35,20,eBlockScaled>	_wallBox38;
	puWallBox<35,20,eBlockScaled>	_wallBox39;
	puWallBox<35,20,eBlockScaled>	_wallBox3;
	puWallBox<35,20,eBlockScaled>	_wallBox40;
	puWallBox<35,20,eBlockScaled>	_wallBox41;
	puWallBox<35,20,eBlockScaled>	_wallBox42;
	puWallBox<35,20,eBlockScaled>	_wallBox43;
	puWallBox<35,20,eBlockScaled>	_wallBox44;
	puWallBox<35,20,eBlockScaled>	_wallBox45;
	puWallBox<35,20,eBlockScaled>	_wallBox46;
	puWallBox<35,20,eBlockScaled>	_wallBox47;
	puWallBox<35,20,eBlockScaled>	_wallBox48;
	puWallBox<35,20,eBlockScaled>	_wallBox49;
	puWallBox<35,20,eBlockScaled>	_wallBox4;
	puWallBox<35,20,eBlockScaled>	_wallBox50;
	puWallBox<35,20,eBlockScaled>	_wallBox51;
	puWallBox<35,20,eBlockScaled>	_wallBox52;
	puWallBox<35,20,eBlockScaled>	_wallBox53;
	puWallBox<35,20,eBlockScaled>	_wallBox54;
	puWallBox<35,20,eBlockScaled>	_wallBox55;
	puWallBox<35,20,eBlockScaled>	_wallBox56;
	puWallBox<35,20,eBlockScaled>	_wallBox57;
	puWallBox<35,20,eBlockScaled>	_wallBox58;
	puWallBox<35,20,eBlockScaled>	_wallBox59;
	puWallBox<35,20,eBlockScaled>	_wallBox5;
	puWallBox<35,20,eBlockScaled>	_wallBox60;
	puWallBox<35,20,eBlockScaled>	_wallBox61;
	puWallBox<35,20,eBlockScaled>	_wallBox62;
	puWallBox<35,20,eBlockScaled>	_wallBox63;
	puWallBox<35,20,eBlockScaled>	_wallBox64;
	puWallBox<35,20,eBlockScaled>	_wallBox6;
	puWallBox<35,20,eBlockScaled>	_wallBox7;
	puWallBox<35,20,eBlockScaled>	_wallBox8;
	puWallBox<35,20,eBlockScaled>	_wallBox9;
	puWallBox<540,20,eBlockScaled>	_wallBox65;
	
};
//------------------------------------------------------------------
class LevelTunnel_1 : public Level
{
public:
	LevelTunnel_1( GameTypes::LevelEnum levelEnum, bool viewMode );

private:
	puBomb< 44 >	_bomb1;
	puCoin< 40 >	_coin1;
	puFragileBox<110,96,eBlockScaled>	_fragileBox1;
	puFragileBox<110,96,eBlockScaled>	_fragileBox2;
	puFragileBoxStatic<96,96,eBlockScaled>	_fragileBoxStatic1;
	puFragileBoxStatic<96,96,eBlockScaled>	_fragileBoxStatic2;
	puFragileBoxStatic<96,96,eBlockScaled>	_fragileBoxStatic3;
	puFragileBoxStatic<96,96,eBlockScaled>	_fragileBoxStatic4;
	puPiggyBank< 60 >	_piggyBank1;
	puWallBox<100,15,eBlockScaled>	_wallBox1;
	puWallBox<120,15,eBlockScaled>	_wallBox2;
	puWallBox<120,15,eBlockScaled>	_wallBox3;
	puWallBox<30,15,eBlockScaled>	_wallBox10;
	puWallBox<30,15,eBlockScaled>	_wallBox11;
	puWallBox<30,15,eBlockScaled>	_wallBox12;
	puWallBox<30,15,eBlockScaled>	_wallBox13;
	puWallBox<30,15,eBlockScaled>	_wallBox14;
	puWallBox<30,15,eBlockScaled>	_wallBox15;
	puWallBox<30,15,eBlockScaled>	_wallBox16;
	puWallBox<30,15,eBlockScaled>	_wallBox17;
	puWallBox<30,15,eBlockScaled>	_wallBox18;
	puWallBox<30,15,eBlockScaled>	_wallBox19;
	puWallBox<30,15,eBlockScaled>	_wallBox20;
	puWallBox<30,15,eBlockScaled>	_wallBox21;
	puWallBox<30,15,eBlockScaled>	_wallBox4;
	puWallBox<30,15,eBlockScaled>	_wallBox5;
	puWallBox<30,15,eBlockScaled>	_wallBox6;
	puWallBox<30,15,eBlockScaled>	_wallBox7;
	puWallBox<30,15,eBlockScaled>	_wallBox8;
	puWallBox<30,15,eBlockScaled>	_wallBox9;
	puWallBox<640,20,eBlockScaled>	_wallBox22;
	puWoodBox<160,15,eBlockScaled>	_woodBox1;
	puWoodBox<240,15,eBlockScaled>	_woodBox2;
	puWoodBox<240,15,eBlockScaled>	_woodBox3;
	puWoodBox<240,15,eBlockScaled>	_woodBox4;
	puWoodBox<240,15,eBlockScaled>	_woodBox5;
	puWoodBox<240,15,eBlockScaled>	_woodBox6;
};
//------------------------------------------------------------------
class LevelExplosivePath : public Level
{
public:
	LevelExplosivePath( GameTypes::LevelEnum levelEnum, bool viewMode );

private:
	puBombStatic< 44 >	_bombStatic1;
	puBombStatic< 44 >	_bombStatic2;
	puBombStatic< 44 >	_bombStatic3;
	puBombStatic< 44 >	_bombStatic4;
	puBombStatic< 44 >	_bombStatic5;
	puBombStatic< 44 >	_bombStatic6;
	puCoin< 40 >	_coin1;
	puPiggyBankStatic< 60 >	_piggyBankStatic1;
	puWallBox<100,20,eBlockScaled>	_wallBox10;
	puWallBox<100,20,eBlockScaled>	_wallBox6;
	puWallBox<100,20,eBlockScaled>	_wallBox7;
	puWallBox<100,20,eBlockScaled>	_wallBox8;
	puWallBox<100,20,eBlockScaled>	_wallBox9;
	puWallBox<50,20,eBlockScaled>	_wallBox11;
	puWallBox<50,20,eBlockScaled>	_wallBox12;
	puWallBox<50,20,eBlockScaled>	_wallBox1;
	puWallBox<50,20,eBlockScaled>	_wallBox2;
	puWallBox<50,20,eBlockScaled>	_wallBox3;
	puWallBox<50,20,eBlockScaled>	_wallBox4;
	puWallBox<50,20,eBlockScaled>	_wallBox5;
};
//------------------------------------------------------------------
class LevelBouncingCoin_1 : public Level
{
public:
	LevelBouncingCoin_1( GameTypes::LevelEnum levelEnum, bool viewMode );

private:
	puCoin< 20 >	_coin1;
	puFloor2	_floor21;
	puFragileBoxStatic<48,48,eBlockScaled>	_fragileBoxStatic10;
	puFragileBoxStatic<48,48,eBlockScaled>	_fragileBoxStatic11;
	puFragileBoxStatic<48,48,eBlockScaled>	_fragileBoxStatic12;
	puFragileBoxStatic<48,48,eBlockScaled>	_fragileBoxStatic13;
	puFragileBoxStatic<48,48,eBlockScaled>	_fragileBoxStatic14;
	puFragileBoxStatic<48,48,eBlockScaled>	_fragileBoxStatic15;
	puFragileBoxStatic<48,48,eBlockScaled>	_fragileBoxStatic16;
	puFragileBoxStatic<48,48,eBlockScaled>	_fragileBoxStatic17;
	puFragileBoxStatic<48,48,eBlockScaled>	_fragileBoxStatic1;
	puFragileBoxStatic<48,48,eBlockScaled>	_fragileBoxStatic2;
	puFragileBoxStatic<48,48,eBlockScaled>	_fragileBoxStatic3;
	puFragileBoxStatic<48,48,eBlockScaled>	_fragileBoxStatic4;
	puFragileBoxStatic<48,48,eBlockScaled>	_fragileBoxStatic5;
	puFragileBoxStatic<48,48,eBlockScaled>	_fragileBoxStatic6;
	puFragileBoxStatic<48,48,eBlockScaled>	_fragileBoxStatic7;
	puFragileBoxStatic<48,48,eBlockScaled>	_fragileBoxStatic8;
	puFragileBoxStatic<48,48,eBlockScaled>	_fragileBoxStatic9;
	puPiggyBank< 30 >	_piggyBank1;
	puWallBox<50,8,eBlockScaled>	_wallBox18;
	puWallBox<7,20,eBlockScaled>	_wallBox10;
	puWallBox<7,20,eBlockScaled>	_wallBox11;
	puWallBox<7,20,eBlockScaled>	_wallBox12;
	puWallBox<7,20,eBlockScaled>	_wallBox13;
	puWallBox<7,20,eBlockScaled>	_wallBox14;
	puWallBox<7,20,eBlockScaled>	_wallBox15;
	puWallBox<7,20,eBlockScaled>	_wallBox16;
	puWallBox<7,20,eBlockScaled>	_wallBox17;
	puWallBox<7,20,eBlockScaled>	_wallBox1;
	puWallBox<7,20,eBlockScaled>	_wallBox2;
	puWallBox<7,20,eBlockScaled>	_wallBox3;
	puWallBox<7,20,eBlockScaled>	_wallBox4;
	puWallBox<7,20,eBlockScaled>	_wallBox5;
	puWallBox<7,20,eBlockScaled>	_wallBox6;
	puWallBox<7,20,eBlockScaled>	_wallBox7;
	puWallBox<7,20,eBlockScaled>	_wallBox8;
	puWallBox<7,20,eBlockScaled>	_wallBox9;
};
//------------------------------------------------------------------
class LevelScale_2 : public Level
{
public:
	LevelScale_2( GameTypes::LevelEnum levelEnum, bool viewMode );

private:
	puCircleWall< 40 >	_circleWall1;
	puCoin< 40 >	_coin1;
	puFragileBox<96,96,eBlockScaled>	_fragileBox1;
	puFragileBox<96,96,eBlockScaled>	_fragileBox2;
	puFragileBox<96,96,eBlockScaled>	_fragileBox3;
	puFragileBox<96,96,eBlockScaled>	_fragileBox4;
	puFragileBoxStatic<96,96,eBlockScaled>	_fragileBoxStatic1;
	puFragileBoxStatic<96,96,eBlockScaled>	_fragileBoxStatic2;
	puFragileBoxStatic<96,96,eBlockScaled>	_fragileBoxStatic3;
	puFragileBoxStatic<96,96,eBlockScaled>	_fragileBoxStatic4;
	puFragileBoxStatic<96,96,eBlockScaled>	_fragileBoxStatic5;
	puFragileBoxStatic<96,96,eBlockScaled>	_fragileBoxStatic6;
	puPiggyBank< 60 >	_piggyBank1;
	puWallBox<70,20,eBlockScaled>	_wallBox1;
	puWoodBox<500,20,eBlockScaled>	_woodBox1;
};
//------------------------------------------------------------------
class LevelSpiderWeb : public Level
{
public:
	LevelSpiderWeb( GameTypes::LevelEnum levelEnum, bool viewMode );

private:
	puChainCutable	_chain10;
	puChainCutable	_chain1;
	puChainCutable	_chain2;
	puChainCutable	_chain3;
	puChainCutable	_chain4;
	puChainCutable	_chain5;
	puChainCutable	_chain6;
	puChainCutable	_chain7;
	puChainCutable	_chain8;
	puChainCutable	_chain9;
	puCoin< 40 >	_coin1;
	puPiggyBank< 60 >	_piggyBank1;
	puWallBox<400,16,eBlockScaled>	_wallBox5;
	puWallBox<600,16,eBlockScaled>	_wallBox6;
	puWallBox<100,16,eBlockScaled>	_wallBox1;
	puWallBox<100,16,eBlockScaled>	_wallBox2;
	puWallBox<100,16,eBlockScaled>	_wallBox3;
	puWallBox<100,16,eBlockScaled>	_wallBox4;
};
//------------------------------------------------------------------

#endif
