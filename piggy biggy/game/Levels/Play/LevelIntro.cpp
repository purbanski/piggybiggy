#include "LevelIntro.h"
#include "Levels/LevelMenu.h"
#include "Levels/ScoreCounter.h"

//--------------------------------------------------------------------------
LevelIntro_Welcome::LevelIntro_Welcome( GameTypes::LevelEnum levelEnum, bool viewMode ) : LevelActivable( levelEnum, viewMode )
{
	// _coin1
	_coin1.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin1.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin1.SetBodyType( b2_staticBody );

	// _piggyBank1
	_piggyBank1.GetBody()->GetFixtureList()->SetDensity( 7.5000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_piggyBank1.SetBodyType( b2_staticBody );

	// _wallBox1
	_wallBox1.SetRotation( 192.0860f );
	_wallBox1.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox1.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox1.SetBodyType( b2_staticBody );

	// _wallBox2
	_wallBox2.SetRotation( 158.1430f );
	_wallBox2.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox2.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox2.SetBodyType( b2_staticBody );

	// _wallBox3
	_wallBox3.SetRotation( 180.5143f );
	_wallBox3.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox3.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox3.SetBodyType( b2_staticBody );

	// _wallBox4
	_wallBox4.SetRotation( 123.4800f );
	_wallBox4.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox4.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox4.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox4.SetBodyType( b2_staticBody );

	// _wallBox5
	_wallBox5.SetRotation( 203.1400f );
	_wallBox5.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox5.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox5.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox5.SetBodyType( b2_staticBody );

	// _wallBox6
	_wallBox6.SetRotation( 169.2001f );
	_wallBox6.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox6.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox6.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox6.SetBodyType( b2_staticBody );

	// _wallBox7
	_wallBox7.SetRotation( 237.0860f );
	_wallBox7.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox7.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox7.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox7.SetBodyType( b2_staticBody );

	// _wallBox8
	_wallBox8.SetRotation( 191.8290f );
	_wallBox8.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox8.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox8.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox8.SetBodyType( b2_staticBody );

	// _wallBox9
	_wallBox9.SetRotation( 225.7720f );
	_wallBox9.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox9.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox9.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox9.SetBodyType( b2_staticBody );

	// _wallBox10
	_wallBox10.SetRotation( 146.8287f );
	_wallBox10.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox10.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox10.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox10.SetBodyType( b2_staticBody );

	// _wallBox11
	_wallBox11.SetRotation( 124.2000f );
	_wallBox11.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox11.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox11.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox11.SetBodyType( b2_staticBody );

	// _wallBox12
	_wallBox12.SetRotation( 169.4573f );
	_wallBox12.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox12.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox12.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox12.SetBodyType( b2_staticBody );

	// _wallBox13
	_wallBox13.SetRotation( 180.7700f );
	_wallBox13.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox13.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox13.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox13.SetBodyType( b2_staticBody );

	// _wallBox14
	_wallBox14.SetRotation( 135.5143f );
	_wallBox14.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox14.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox14.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox14.SetBodyType( b2_staticBody );

	// _wallBox15
	_wallBox15.SetRotation( 156.9600f );
	_wallBox15.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox15.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox15.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox15.SetBodyType( b2_staticBody );

	// _wallBox16
	_wallBox16.SetRotation( 190.4400f );
	_wallBox16.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox16.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox16.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox16.SetBodyType( b2_staticBody );

	// _wallBox17
	_wallBox17.SetRotation( 223.9200f );
	_wallBox17.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox17.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox17.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox17.SetBodyType( b2_staticBody );

	// _wallBox18
	_wallBox18.SetRotation( 257.4000f );
	_wallBox18.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox18.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox18.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox18.SetBodyType( b2_staticBody );

	// _wallBox19
	_wallBox19.SetRotation( 290.8800f );
	_wallBox19.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox19.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox19.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox19.SetBodyType( b2_staticBody );

	// _wallBox20
	_wallBox20.SetRotation( 324.3600f );
	_wallBox20.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox20.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox20.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox20.SetBodyType( b2_staticBody );

	// _wallBox21
	_wallBox21.SetRotation( 214.4570f );
	_wallBox21.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox21.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox21.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox21.SetBodyType( b2_staticBody );

	// _wallBox22
	_wallBox22.SetRotation( 124.2000f );
	_wallBox22.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox22.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox22.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox22.SetBodyType( b2_staticBody );

	// _wallBox23
	_wallBox23.SetRotation( 192.0860f );
	_wallBox23.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox23.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox23.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox23.SetBodyType( b2_staticBody );

	// _wallBox24
	_wallBox24.SetRotation( 135.5100f );
	_wallBox24.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox24.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox24.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox24.SetBodyType( b2_staticBody );

	// _wallBox25
	_wallBox25.SetRotation( 146.8200f );
	_wallBox25.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox25.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox25.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox25.SetBodyType( b2_staticBody );

	// _wallBox26
	_wallBox26.SetRotation( 158.1430f );
	_wallBox26.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox26.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox26.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox26.SetBodyType( b2_staticBody );

	// _wallBox27
	_wallBox27.SetRotation( 169.4573f );
	_wallBox27.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox27.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox27.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox27.SetBodyType( b2_staticBody );

	// _wallBox28
	_wallBox28.SetRotation( 180.7716f );
	_wallBox28.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox28.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox28.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox28.SetBodyType( b2_staticBody );


	//Set blocks positions
	_coin1.SetPosition( 10.1500f, 78.4000f );
	_piggyBank1.SetPosition( 84.6500f, 73.1000f );
	_wallBox1.SetPosition( 37.9700f, 46.7000f );
	_wallBox2.SetPosition( 20.9100f, 17.2600f );
	_wallBox3.SetPosition( 63.9899f, 29.7900f );
	_wallBox4.SetPosition( 49.4500f, 7.6360f );
	_wallBox5.SetPosition( 75.3100f, 32.1500f );
	_wallBox6.SetPosition( 58.1900f, 30.3100f );
	_wallBox7.SetPosition( 88.4900f, 43.2500f );
	_wallBox8.SetPosition( 69.7700f, 30.4100f );
	_wallBox9.SetPosition( 84.8700f, 38.7100f );
	_wallBox10.SetPosition( 15.7440f, 19.9480f );
	_wallBox11.SetPosition( 7.4860f, 28.0600f );
	_wallBox12.SetPosition( 26.4900f, 15.6380f );
	_wallBox13.SetPosition( 32.2900f, 15.1420f );
	_wallBox14.SetPosition( 11.2120f, 23.6000f );
	_wallBox15.SetPosition( 53.2300f, 4.1180f );
	_wallBox16.SetPosition( 58.3900f, 3.5490f );
	_wallBox17.SetPosition( 62.9898f, 5.9180f );
	_wallBox18.SetPosition( 65.7300f, 10.6360f );
	_wallBox19.SetPosition( 65.3500f, 15.8080f );
	_wallBox20.SetPosition( 62.1898f, 19.9140f );
	_wallBox21.SetPosition( 80.4100f, 34.9700f );
	_wallBox22.SetPosition( 7.3860f, 58.9599f );
	_wallBox23.SetPosition( 38.0700f, 15.7940f );
	_wallBox24.SetPosition( 11.1120f, 54.5000f );
	_wallBox25.SetPosition( 15.6440f, 50.8400f );
	_wallBox26.SetPosition( 20.8100f, 48.1600f );
	_wallBox27.SetPosition( 26.3900f, 46.5400f );
	_wallBox28.SetPosition( 32.1900f, 46.0400f );

	_sleepers.push_back( Sleeper( &_piggyBank1, 50 ));
	_sleepers.push_back( Sleeper( &_coin1, 50 ));

	ConstFinal();
	_border.DestroyBody();


	_msgManager.push_back( eOTMsg_IntroWelcome, new LevelMsg_TextMoving( 
		Config::LevelFirstMsgTick + 50, 
		9.0f, 
		"Piggy must collect all coins. And remember, time is money... ", 
		b2Vec2( 1200.0f, 10.0f )
		));
};

//--------------------------------------------------------------------------
LevelIntro_DestroyBlock::LevelIntro_DestroyBlock( GameTypes::LevelEnum levelEnum, bool viewMode ) : LevelActivable( levelEnum, viewMode )
{
	// _coin1
	_coin1.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin1.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin1.SetBodyType( b2_dynamicBody );

	// _piggyBank1
	_piggyBank1.GetBody()->GetFixtureList()->SetDensity( 7.5000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_piggyBank1.SetBodyType( b2_dynamicBody );

	// _fragileBox1
	_fragileBox1.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox1.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox1.SetBodyType( b2_dynamicBody );

	// _fragileBox2
	_fragileBox2.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox2.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox2.SetBodyType( b2_dynamicBody );

	// _fragileBox3
	_fragileBox3.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox3.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox3.SetBodyType( b2_dynamicBody );

	// _wallBox1
	_wallBox1.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox1.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox1.SetBodyType( b2_staticBody );


	//Set blocks positions
	_coin1.SetPosition( 48.0000f, 61.2496f );
	_piggyBank1.SetPosition( 48.0000f, 13.8000f );
	_fragileBox1.SetPosition( 48.0000f, 45.1005f );
	_fragileBox2.SetPosition( 48.0000f, 35.1005f );
	_fragileBox3.SetPosition( 48.0000f, 25.1001f );
	_wallBox1.SetPosition( 48.0000f, 6.000f );

	ConstFinal();

	_msgManager.push_back( eOTMsg_IntroBlockDestroy, new LevelMsg_MovingPointingCircle( 
			Config::LevelFirstMsgTick + 15,
			2.0f, 
			"", 
			b2Vec2( 0.0f, 0.0f ), 
			_fragileBox1.GetReversedPosition(), 
			b2Vec2( 0.0f, -20.5f ),
			GameTypes::eLevelMsg
			));
}
//--------------------------------------------------------------------------
LevelIntro_Gun::LevelIntro_Gun( GameTypes::LevelEnum levelEnum, bool viewMode ) : Level( levelEnum, viewMode )
{
    _levelTag = eLevelTag_Spitter;
	_coinCount = 7;

	// _coin1
	_coin1.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin1.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin1.GetBody()->SetAngularDamping(0.1000f );
	_coin1.SetBodyType( b2_dynamicBody );

	// _coin2
	_coin2.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin2.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin2.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin2.GetBody()->SetAngularDamping(0.1000f );
	_coin2.SetBodyType( b2_dynamicBody );

	// _coin3
	_coin3.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin3.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin3.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin3.GetBody()->SetAngularDamping(0.1000f );
	_coin3.SetBodyType( b2_dynamicBody );

	// _coin4
	_coin4.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin4.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin4.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin4.GetBody()->SetAngularDamping(0.1000f );
	_coin4.SetBodyType( b2_dynamicBody );

	// _coin5
	_coin5.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin5.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin5.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin5.GetBody()->SetAngularDamping(0.1000f );
	_coin5.SetBodyType( b2_dynamicBody );

	// _coin6
	_coin6.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin6.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin6.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin6.GetBody()->SetAngularDamping(0.1000f );
	_coin6.SetBodyType( b2_dynamicBody );

	// _coin7
	_coin7.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin7.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin7.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin7.GetBody()->SetAngularDamping(0.1000f );
	_coin7.SetBodyType( b2_dynamicBody );

	// _gun1
	_gun1.GetBody()->GetFixtureList()->SetDensity( 7.5000f );
	_gun1.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_gun1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	//_gun1.FlipX();
	_gun1.SetBodyType( b2_dynamicBody );

	// _piggyBank1
	_piggyBank1.GetBody()->GetFixtureList()->SetDensity( 7.5000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_piggyBank1.SetBodyType( b2_dynamicBody );

	// _wallBox1
	_wallBox1.SetRotation( 186.0000f );
	_wallBox1.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox1.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox1.SetBodyType( b2_staticBody );

	// _wallBox2
	_wallBox2.SetRotation( -101.7000f );
	_wallBox2.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox2.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox2.SetBodyType( b2_staticBody );

	// _wallBox3
	_wallBox3.SetRotation( -141.6999f );
	_wallBox3.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox3.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox3.SetBodyType( b2_staticBody );

	// _wallBox4
	_wallBox4.SetRotation( 91.5000f );
	_wallBox4.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox4.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox4.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox4.SetBodyType( b2_staticBody );

	// _wallBox5
	_wallBox5.SetRotation( -106.7001f );
	_wallBox5.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox5.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox5.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox5.SetBodyType( b2_staticBody );

	// _wallBox6
	_wallBox6.SetRotation( -96.6999f );
	_wallBox6.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox6.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox6.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox6.SetBodyType( b2_staticBody );

	// _wallBox7
	_wallBox7.SetRotation( -91.6998f );
	_wallBox7.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox7.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox7.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox7.SetBodyType( b2_staticBody );

	// _wallBox8
	_wallBox8.SetRotation( -121.7000f );
	_wallBox8.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox8.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox8.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox8.SetBodyType( b2_staticBody );

	// _wallBox9
	_wallBox9.SetRotation( -136.6999f );
	_wallBox9.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox9.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox9.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox9.SetBodyType( b2_staticBody );

	// _wallBox10
	_wallBox10.SetRotation( -131.6998f );
	_wallBox10.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox10.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox10.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox10.SetBodyType( b2_staticBody );

	// _wallBox11
	_wallBox11.SetRotation( 146.5000f );
	_wallBox11.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox11.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox11.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox11.SetBodyType( b2_staticBody );

	// _wallBox12
	_wallBox12.SetRotation( 136.5000f );
	_wallBox12.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox12.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox12.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox12.SetBodyType( b2_staticBody );

	// _wallBox13
	_wallBox13.SetRotation( 101.5000f );
	_wallBox13.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox13.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox13.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox13.SetBodyType( b2_staticBody );

	// _wallBox14
	_wallBox14.SetRotation( -126.7000f );
	_wallBox14.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox14.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox14.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox14.SetBodyType( b2_staticBody );

	// _wallBox15
	_wallBox15.SetRotation( -146.7000f );
	_wallBox15.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox15.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox15.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox15.SetBodyType( b2_staticBody );

	// _wallBox16
	_wallBox16.SetRotation( -156.7000f );
	_wallBox16.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox16.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox16.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox16.SetBodyType( b2_staticBody );

	// _wallBox17
	_wallBox17.SetRotation( 96.5000f );
	_wallBox17.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox17.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox17.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox17.SetBodyType( b2_staticBody );

	// _wallBox18
	_wallBox18.SetRotation( -151.6999f );
	_wallBox18.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox18.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox18.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox18.SetBodyType( b2_staticBody );

	// _wallBox19
	_wallBox19.SetRotation( -161.7001f );
	_wallBox19.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox19.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox19.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox19.SetBodyType( b2_staticBody );

	// _wallBox20
	_wallBox20.SetRotation( -166.6997f );
	_wallBox20.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox20.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox20.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox20.SetBodyType( b2_staticBody );

	// _wallBox21
	_wallBox21.SetRotation( -177.6999f );
	_wallBox21.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox21.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox21.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox21.SetBodyType( b2_staticBody );

	// _wallBox22
	_wallBox22.SetRotation( 151.5000f );
	_wallBox22.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox22.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox22.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox22.SetBodyType( b2_staticBody );

	// _wallBox23
	_wallBox23.SetRotation( -171.7000f );
	_wallBox23.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox23.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox23.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox23.SetBodyType( b2_staticBody );

	// _wallBox24
	_wallBox24.SetRotation( 141.5000f );
	_wallBox24.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox24.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox24.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox24.SetBodyType( b2_staticBody );

	// _wallBox25
	_wallBox25.SetRotation( 106.5000f );
	_wallBox25.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox25.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox25.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox25.SetBodyType( b2_staticBody );

	// _wallBox26
	_wallBox26.SetRotation( 90.0000f );
	_wallBox26.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox26.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox26.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox26.SetBodyType( b2_staticBody );

	// _wallBox27
	_wallBox27.SetRotation( 90.0000f );
	_wallBox27.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox27.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox27.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox27.SetBodyType( b2_staticBody );

	// _wallBox28
	_wallBox28.SetRotation( 90.0000f );
	_wallBox28.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox28.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox28.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox28.SetBodyType( b2_staticBody );

	// _wallBox29
	_wallBox29.SetRotation( 90.0000f );
	_wallBox29.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox29.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox29.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox29.SetBodyType( b2_staticBody );

	// _wallBox30
	_wallBox30.SetRotation( -111.6997f );
	_wallBox30.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox30.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox30.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox30.SetBodyType( b2_staticBody );

	// _wallBox31
	_wallBox31.SetRotation( 111.5000f );
	_wallBox31.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox31.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox31.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox31.SetBodyType( b2_staticBody );

	// _wallBox32
	_wallBox32.SetRotation( 116.5000f );
	_wallBox32.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox32.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox32.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox32.SetBodyType( b2_staticBody );

	// _wallBox33
	_wallBox33.SetRotation( 156.5000f );
	_wallBox33.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox33.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox33.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox33.SetBodyType( b2_staticBody );

	// _wallBox34
	_wallBox34.SetRotation( 121.5000f );
	_wallBox34.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox34.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox34.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox34.SetBodyType( b2_staticBody );

	// _wallBox35
	_wallBox35.SetRotation( 126.5000f );
	_wallBox35.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox35.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox35.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox35.SetBodyType( b2_staticBody );

	// _wallBox36
	_wallBox36.SetRotation( 131.5000f );
	_wallBox36.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox36.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox36.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox36.SetBodyType( b2_staticBody );

	// _wallBox37
	_wallBox37.SetRotation( 161.5000f );
	_wallBox37.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox37.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox37.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox37.SetBodyType( b2_staticBody );

	// _wallBox38
	_wallBox38.SetRotation( 166.5000f );
	_wallBox38.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox38.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox38.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox38.SetBodyType( b2_staticBody );

	// _wallBox39
	_wallBox39.SetRotation( -116.7002f );
	_wallBox39.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox39.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox39.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox39.SetBodyType( b2_staticBody );

	// _wallBox40
	_wallBox40.SetRotation( 171.5000f );
	_wallBox40.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox40.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox40.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox40.SetBodyType( b2_staticBody );

	// _wallBox41
	_wallBox41.SetRotation( 174.0000f );
	_wallBox41.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox41.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox41.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox41.SetBodyType( b2_staticBody );

	// _wallBox42
	_wallBox42.SetRotation( 176.5000f );
	_wallBox42.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox42.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox42.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox42.SetBodyType( b2_staticBody );

	// _wallBox43
	_wallBox43.SetRotation( 90.0000f );
	_wallBox43.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox43.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox43.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox43.GetBody()->SetAngularDamping(0.1000f );
	_wallBox43.SetBodyType( b2_staticBody );

	// _wallBox44
	_wallBox44.SetRotation( 90.0000f );
	_wallBox44.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox44.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox44.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox44.GetBody()->SetAngularDamping(0.1000f );
	_wallBox44.SetBodyType( b2_staticBody );

	// _wallBox45
	_wallBox45.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox45.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox45.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox45.SetBodyType( b2_staticBody );

	// _wallBox46
	_wallBox46.SetRotation( 90.0000f );
	_wallBox46.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox46.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox46.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox46.SetBodyType( b2_staticBody );

	// _wallBox47
	_wallBox47.SetRotation( 90.0000f );
	_wallBox47.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox47.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox47.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox47.SetBodyType( b2_staticBody );


	//Set blocks positions
	_coin1.SetPosition( 28.2498f, 60.9502f );
	_coin2.SetPosition( 28.2498f, 103.7497f );
	_coin3.SetPosition( 28.2498f, 69.6499f );
	_coin4.SetPosition( 28.2498f, 94.9498f );
	_coin5.SetPosition( 28.2498f, 86.0495f );
	_coin6.SetPosition( 28.2498f, 78.0499f );
	_coin7.SetPosition( 28.2498f, 52.7503f );
	_gun1.SetPosition( 67.1999f, 53.3999f );
	_piggyBank1.SetPosition( 84.1998f, 67.1000f );
	_wallBox1.SetPosition( 30.1834f, 46.3503f );
	_wallBox2.SetPosition( 90.3275f, 39.2555f );
	_wallBox3.SetPosition( 74.5118f, 13.6482f );
	_wallBox4.SetPosition( 4.5922f, 47.0519f );
	_wallBox5.SetPosition( 89.3858f, 35.5343f );
	_wallBox6.SetPosition( 90.9412f, 43.0446f );
	_wallBox7.SetPosition( 91.3721f, 46.8806f );
	_wallBox8.SetPosition( 84.6773f, 25.0574f );
	_wallBox9.SetPosition( 77.4174f, 16.1561f );
	_wallBox10.SetPosition( 80.0937f, 18.9081f );
	_wallBox11.SetPosition( 24.2919f, 11.5124f );
	_wallBox12.SetPosition( 18.2897f, 16.2869f );
	_wallBox13.SetPosition( 5.4604f, 39.4314f );
	_wallBox14.SetPosition( 82.5196f, 21.8826f );
	_wallBox15.SetPosition( 71.3986f, 11.4025f );
	_wallBox16.SetPosition( 64.6456f, 7.7665f );
	_wallBox17.SetPosition( 4.8599f, 43.2225f );
	_wallBox18.SetPosition( 68.1015f, 9.4371f );
	_wallBox19.SetPosition( 61.0574f, 6.4035f );
	_wallBox20.SetPosition( 57.3640f, 5.3584f );
	_wallBox21.SetPosition( 49.7746f, 4.2512f );
	_wallBox22.SetPosition( 27.5822f, 9.5355f );
	_wallBox23.SetPosition( 53.5936f, 4.6391f );
	_wallBox24.SetPosition( 21.1866f, 13.7687f );
	_wallBox25.SetPosition( 6.3890f, 35.7071f );
	_wallBox26.SetPosition( 91.4246f, 54.4512f );
	_wallBox27.SetPosition( 91.4246f, 50.6512f );
	_wallBox28.SetPosition( 4.5246f, 50.9012f );
	_wallBox29.SetPosition( 4.5246f, 54.7012f );
	_wallBox30.SetPosition( 88.1233f, 31.9093f );
	_wallBox31.SetPosition( 7.6388f, 32.0775f );
	_wallBox32.SetPosition( 9.1999f, 28.5710f );
	_wallBox33.SetPosition( 31.0320f, 7.8528f );
	_wallBox34.SetPosition( 11.0611f, 25.2136f );
	_wallBox35.SetPosition( 13.2075f, 22.0313f );
	_wallBox36.SetPosition( 15.6231f, 19.0481f );
	_wallBox37.SetPosition( 34.6157f, 6.4771f );
	_wallBox38.SetPosition( 38.3055f, 5.4191f );
	_wallBox39.SetPosition( 86.5498f, 28.4082f );
	_wallBox40.SetPosition( 42.0735f, 4.6867f );
	_wallBox41.SetPosition( 26.5502f, 46.3503f );
	_wallBox42.SetPosition( 45.8909f, 4.2855f );
	_wallBox43.SetPosition( 23.2500f, 94.5504f );
	_wallBox44.SetPosition( 33.3000f, 94.5504f );
	_wallBox45.SetPosition( 66.8001f, 45.9500f );
	_wallBox46.SetPosition( 23.25f, 58.45f );
	_wallBox47.SetPosition( 33.3f, 58.45f );

	//--- ---------
	// manual
	_coin1.GetBody()->SetBullet( true );
	_coin2.GetBody()->SetBullet( true );
	_coin3.GetBody()->SetBullet( true );
	_coin4.GetBody()->SetBullet( true );
	_coin5.GetBody()->SetBullet( true );
	_coin6.GetBody()->SetBullet( true );
	_coin7.GetBody()->SetBullet( true );

	ConstFinal();
	_border.SetBorderSize( 50.0f );

	_msgManager.push_back( eOTMsg_IntroGun, new LevelMsg_PointingCircle( 
		Config::LevelFirstMsgTick + 15,
		2.0f, 
		"", 
		b2Vec2( 0.0f, 0.0f ), 
		_gun1.GetReversedPosition()
	));
}
//--------------------------------------------------------------------------
LevelIntro_MoneyMakeMoney::LevelIntro_MoneyMakeMoney( GameTypes::LevelEnum levelEnum, bool viewMode ) : Level( levelEnum, viewMode )
{
	_coinCount = 3;

	// _coin1
	_coin1.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin1.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin1.SetBodyType( b2_dynamicBody );

	// _coinSilver1
	_coinSilver1.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coinSilver1.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coinSilver1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coinSilver1.SetBodyType( b2_dynamicBody );

	// _fragileBoxStatic1
	_fragileBoxStatic1.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBoxStatic1.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBoxStatic1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBoxStatic1.SetBodyType( b2_staticBody );

	// _fragileBoxStatic2
	_fragileBoxStatic2.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBoxStatic2.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBoxStatic2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBoxStatic2.SetBodyType( b2_staticBody );

	// _piggyBank1
	_piggyBank1.GetBody()->GetFixtureList()->SetDensity( 7.5000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_piggyBank1.SetBodyType( b2_dynamicBody );

	// _wallBox1
	_wallBox1.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox1.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox1.SetBodyType( b2_staticBody );


	//Set blocks positions
	_coin1.SetPosition( 48.0000f, 38.6495f );
	_coinSilver1.SetPosition( 48.0000f, 59.9493f );
	_fragileBoxStatic1.SetPosition( 48.0000f, 48.4499f );
	_fragileBoxStatic2.SetPosition( 48.0000f, 27.1501f );
	_piggyBank1.SetPosition( 48.0000f, 12.5500f );
	_wallBox1.SetPosition( 48.0000f, 4.8000f );


	_msgManager.push_back( eOTMsg_IntroMoneyMakeMoney,  new LevelMsg_PointingCircle( 
		Config::LevelFirstMsgTick + 15,
		2.0f, 
		"", 
		b2Vec2( 0.0f, 0.0f ), 
		_fragileBoxStatic1.GetReversedPosition()
		));

	ConstFinal();
}
//--------------------------------------------------------------------------
LevelIntro_Rope::LevelIntro_Rope( GameTypes::LevelEnum levelEnum, bool viewMode ) : Level( levelEnum, viewMode )
{
    _levelTag = eLevelTag_Flies;
    
	// _coin1
	_coin1.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin1.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin1.SetBodyType( b2_dynamicBody );

	// _piggyBank1
	_piggyBank1.GetBody()->GetFixtureList()->SetDensity( 7.5000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_piggyBank1.SetBodyType( b2_dynamicBody );
	_piggyBank1.FlipX();

	// _wallBox1
	_wallBox1.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox1.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox1.SetBodyType( b2_staticBody );

	// _chain1
	_chain1.SetChain( 22.5000f);
	_chain1.SetRotation( 0.0000f );
	_chain1.FixHook( false );

	// _chain2
	_chain2.SetChain( 22.5000f);
	_chain2.SetRotation( 180.0000f );
	_chain2.FixHook( false );


	//Set blocks positions
	_coin1.SetPosition( 55.2000f, 47.6000f );
	_piggyBank1.SetPosition( 11.5000f, 16.0502f );
	_wallBox1.SetPosition( 11.5000f, 7.1501f );
	_chain1.SetPosition( 34.4000f, 47.6000f );
	_chain2.SetPosition( 76.0000f, 47.6000f );

	//Set chains hooks
	_chain1.HookOnChain( &_coin1 );
	_chain2.HookOnChain( &_coin1 );

	ConstFinal();

	_msgManager.push_back( eOTMsg_IntroRope, new LevelMsg_MovingPointingCircle(
		Config::LevelFirstMsgTick + 10,
		2.0f, 
		"", 
		b2Vec2( 0.0f, 0.0f ), 
		b2Vec2( 67.0f, 37.85f + 18.0f ), 
		b2Vec2( 0.0f, -18.0f ) 
		));
}
//--------------------------------------------------------------------------
