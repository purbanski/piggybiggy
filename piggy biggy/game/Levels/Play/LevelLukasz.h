#ifndef __LEVELLUKASZ_H__
#define __LEVELLUKASZ_H__

#include <Box2D/Box2D.h>
#include "Levels/Level.h"
#include "Blocks/puBox.h"
//------------------------------------------------------------------------------------
class LevelLukasz_1a : public Level
{
public:
	LevelLukasz_1a( GameTypes::LevelEnum levelEnum, bool viewMode );

private:
	puBomb< 44 >	_bomb1;
	puCoin< 40 >	_coin1;
	puFragileBox<96,96,eBlockScaled>	_fragileBox10;
	puFragileBox<96,96,eBlockScaled>	_fragileBox11;
	puFragileBox<96,96,eBlockScaled>	_fragileBox12;
	puFragileBox<96,96,eBlockScaled>	_fragileBox13;
	puFragileBox<96,96,eBlockScaled>	_fragileBox1;
	puFragileBox<96,96,eBlockScaled>	_fragileBox2;
	puFragileBox<96,96,eBlockScaled>	_fragileBox3;
	puFragileBox<96,96,eBlockScaled>	_fragileBox4;
	puFragileBox<96,96,eBlockScaled>	_fragileBox5;
	puFragileBox<96,96,eBlockScaled>	_fragileBox6;
	puFragileBox<96,96,eBlockScaled>	_fragileBox7;
	puFragileBox<96,96,eBlockScaled>	_fragileBox8;
	puFragileBox<96,96,eBlockScaled>	_fragileBox9;
	puPiggyBank< 60 >	_piggyBank1;
	puWallBox<860,20,eBlockScaled>	_wallBox1;
};
//------------------------------------------------------------------------------------
class LevelLukasz_1b : public Level
{
public:
	LevelLukasz_1b( GameTypes::LevelEnum levelEnum, bool viewMode );

private:
	puBomb< 44 >	_bomb1;
	puCoin< 40 >	_coin1;
	puFragileBox<96,96,eBlockScaled>	_fragileBox10;
	puFragileBox<96,96,eBlockScaled>	_fragileBox11;
	puFragileBox<96,96,eBlockScaled>	_fragileBox12;
	puFragileBox<96,96,eBlockScaled>	_fragileBox13;
	puFragileBox<96,96,eBlockScaled>	_fragileBox1;
	puFragileBox<96,96,eBlockScaled>	_fragileBox2;
	puFragileBox<96,96,eBlockScaled>	_fragileBox3;
	puFragileBox<96,96,eBlockScaled>	_fragileBox4;
	puFragileBox<96,96,eBlockScaled>	_fragileBox5;
	puFragileBox<96,96,eBlockScaled>	_fragileBox6;
	puFragileBox<96,96,eBlockScaled>	_fragileBox7;
	puFragileBox<96,96,eBlockScaled>	_fragileBox8;
	puFragileBox<96,96,eBlockScaled>	_fragileBox9;
	puPiggyBank< 60 >	_piggyBank1;
	puWallBox<140,20,eBlockScaled>	_wallBox2;
	puWallBox<480,20,eBlockScaled>	_wallBox1;
};
//------------------------------------------------------------------------------------
class LevelLukasz_2 : public Level
{
public:
	LevelLukasz_2( GameTypes::LevelEnum levelEnum, bool viewMode );

private:
	puBomb< 44 >	_bomb1;
	puCoin< 40 >	_coin1;
	puCoin< 40 >	_coin2;
	puCop< 50 >	_cop1;
	puFragileBoxStatic<192,96,eBlockScaled>	_fragileBoxStatic3;
	puFragileBoxStatic<192,96,eBlockScaled>	_fragileBoxStatic4;
	puFragileBoxStatic<96,96,eBlockScaled>	_fragileBoxStatic1;
	puFragileBoxStatic<96,96,eBlockScaled>	_fragileBoxStatic2;
	puPiggyBankStatic< 60 >	_piggyBankStatic1;
	puThief< 50 >	_thief1;
	puThief< 50 >	_thief2;
	puWallBox<20,20,eBlockScaled>	_wallBox1;
	puWallBox<20,20,eBlockScaled>	_wallBox2;
	puWallBox<330,20,eBlockScaled>	_wallBox3;
	puWallBox<330,20,eBlockScaled>	_wallBox4;
};
//------------------------------------------------------------------------------------


#endif
