#ifndef __LEVELJACEKZ_H__
#define __LEVELJACEKZ_H__

#include <Box2D/Box2D.h>
#include "Levels/Level.h"
#include "Blocks/AllBlocks.h"

//------------------------------------------------------------------
class LevelJacekZ_Fastfrog : public Level
{
public:
	LevelJacekZ_Fastfrog( GameTypes::LevelEnum levelEnum, bool viewMode );

private:
	puBomb< 44 >	_bomb1;
	puCircleWall< 16 >	_circleWall1;
	puCoin< 40 >	_coin1;
	puFragileBox<192,96,eBlockScaled>	_fragileBox1;
	puFragileBox<96,96,eBlockScaled>	_fragileBox2;
	puFragileBox<96,96,eBlockScaled>	_fragileBox3;
	puFragileBox<96,96,eBlockScaled>	_fragileBox4;
	puFragileBoxStatic<96,96,eBlockScaled>	_fragileBoxStatic1;
	puFragileBoxStatic<96,96,eBlockScaled>	_fragileBoxStatic2;
	puFragileBoxStatic<96,96,eBlockScaled>	_fragileBoxStatic3;
	puFragileBoxStatic<96,96,eBlockScaled>	_fragileBoxStatic4;
	puPiggyBank< 60 >	_piggyBank1;
	puGumBall< 40 >	_tennisBall1;
	puWallBox<920,20,eBlockScaled>	_wallBox1;
	puWoodBox<160,15,eBlockScaled>	_woodBox2;
	puWoodBox<150,15,eBlockScaled>	_woodBox1;
	puWoodBox<240,15,eBlockScaled>	_woodBox5;
	puWoodBox<240,15,eBlockScaled>	_woodBox6;
	puWoodBox<80,20,eBlockScaled>	_woodBox3;
	puWoodBox<80,20,eBlockScaled>	_woodBox4;
};
//------------------------------------------------------------------

#endif
