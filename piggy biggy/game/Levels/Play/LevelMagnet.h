#ifndef __LEVELMAGNET_H__
#define __LEVELMAGNET_H__

#include "Levels/Level.h"
#include "Blocks/AllBlocks.h"
//------------------------------------------------------------------------------------
class LevelMagnet_2 : public LevelVertical
{
public:
	LevelMagnet_2( GameTypes::LevelEnum levelEnum, bool viewMode );

private:
	void CreateJoints();
	void SetJoint( puBlock &block );

private:
	puCircleWall< 20 >	_circleWall1;
	puCircleWall< 20 >	_circleWall2;
	puCoin< 40 >	_coin1;
	puCoin< 40 >	_coin2;
	puCoin< 40 >	_coin3;
	puCoin< 40 >	_coin4;
	puCoin< 40 >	_coin5;
	puCoin< 40 >	_coin6;
	puCoin< 40 >	_coin7;
	puCoin< 40 >	_coin8;
	puWallBox<160,20>	_floor11;

	puMagnetBomb< 44 >		_magnetBomb1;
	puMagnetBomb< 44 >		_magnetBomb2;
	puMagnetBombMinus< 44 >	_magnetBombMinus1;
	puMagnetBombPlus< 44 >	_magnetBombPlus1;
	puMagnetBombPlus< 44 >	_magnetBombPlus2;
	puMagnetBombPlus< 44 >	_magnetBombPlus3;
	puPiggyBank< 60 >		_piggyBank1;
};
//------------------------------------------------------------------------------------
class LevelMagnet_1 : public Level
{
public:
	LevelMagnet_1( GameTypes::LevelEnum levelEnum, bool viewMode );

private:
	puChainCutable	_chain1;
	puChainCutable	_chain2;
	puChainCutable	_chain3;
	puCoin< 40 >	_coin1;
	puMagnetBomb< 44 >	_magnetBomb1;
	puMagnetBomb< 44 >	_magnetBomb2;
	puPiggyBank< 60 >	_piggyBank1;
	puWallBox<100,20,eBlockScaled>	_wallBox25;
	puWallBox<20,20,eBlockScaled>	_wallBox15;
	puWallBox<20,20,eBlockScaled>	_wallBox1;
	puWallBox<20,20,eBlockScaled>	_wallBox22;
	puWallBox<20,20,eBlockScaled>	_wallBox23;
	puWallBox<20,20,eBlockScaled>	_wallBox2;
	puWallBox<50,20,eBlockScaled>	_wallBox10;
	puWallBox<50,20,eBlockScaled>	_wallBox11;
	puWallBox<50,20,eBlockScaled>	_wallBox12;
	puWallBox<50,20,eBlockScaled>	_wallBox13;
	puWallBox<50,20,eBlockScaled>	_wallBox14;
	puWallBox<50,20,eBlockScaled>	_wallBox16;
	puWallBox<50,20,eBlockScaled>	_wallBox17;
	puWallBox<50,20,eBlockScaled>	_wallBox18;
	puWallBox<50,20,eBlockScaled>	_wallBox19;
	puWallBox<50,20,eBlockScaled>	_wallBox20;
	puWallBox<50,20,eBlockScaled>	_wallBox24;
	puWallBox<50,20,eBlockScaled>	_wallBox3;
	puWallBox<50,20,eBlockScaled>	_wallBox4;
	puWallBox<50,20,eBlockScaled>	_wallBox5;
	puWallBox<50,20,eBlockScaled>	_wallBox6;
	puWallBox<50,20,eBlockScaled>	_wallBox7;
	puWallBox<50,20,eBlockScaled>	_wallBox8;
	puWallBox<50,20,eBlockScaled>	_wallBox9;
	puWallBox<500,20,eBlockScaled>	_wallBox21;
	puWallBox<70,20,eBlockScaled>	_wallBox26;


};
//------------------------------------------------------------------------------------

#endif
