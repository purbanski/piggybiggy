#include "LevelScale.h"

//--------------------------------------------------------------------------
LevelScale_1::LevelScale_1( GameTypes::LevelEnum levelEnum, bool viewMode ) : Level( levelEnum, viewMode )
{
    _levelTag = eLevelTag_Cop;
    
	b2Filter filter;

	// _coin1
	_coin1.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin1.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin1.SetBodyType( b2_dynamicBody );
	
	// _cop1
	_cop1.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_cop1.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_cop1.GetBody()->GetFixtureList()->SetRestitution( 0.3500f );
	_cop1.SetBodyType( b2_dynamicBody );
	_cop1.FlipX();

	// _door_11
	_door_11.GetBody()->GetFixtureList()->SetDensity( 10.0000f );
	_door_11.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_door_11.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_door_11.SetBodyType( b2_dynamicBody );

	// _door_12
	_door_12.GetBody()->GetFixtureList()->SetDensity( 10.0000f );
	_door_12.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_door_12.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_door_12.SetBodyType( b2_dynamicBody );

	// _door_13
	_door_13.GetBody()->GetFixtureList()->SetDensity( 10.0000f );
	_door_13.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_door_13.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_door_13.SetBodyType( b2_dynamicBody );

	// _fragileBox1
	_fragileBox1.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox1.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox1.SetBodyType( b2_dynamicBody );

	// _fragileBox2
	_fragileBox2.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox2.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox2.SetBodyType( b2_dynamicBody );

	// _fragileBox3
	_fragileBox3.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox3.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox3.SetBodyType( b2_dynamicBody );

	// _fragileBox4
	_fragileBox4.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox4.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox4.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox4.SetBodyType( b2_dynamicBody );

	// _fragileBox5
	_fragileBox5.SetRotation( -12.0000f );
	_fragileBox5.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox5.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox5.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox5.SetBodyType( b2_dynamicBody );

	// _fragileBox6
	_fragileBox6.SetRotation( 12.0000f );
	_fragileBox6.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox6.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox6.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox6.SetBodyType( b2_dynamicBody );

	// _piggyBank1
	_piggyBank1.GetBody()->GetFixtureList()->SetDensity( 7.5000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_piggyBank1.SetBodyType( b2_dynamicBody );

	// _screw1
	_screw1.GetBody()->GetFixtureList()->SetDensity( 0.0000f );
	_screw1.GetBody()->GetFixtureList()->SetFriction( 0.0000f );
	_screw1.GetBody()->GetFixtureList()->SetRestitution( 0.0000f );
	_screw1.GetBody()->GetFixtureList()->SetFilterData( filter );
	_screw1.SetBodyType( b2_staticBody );

	// _thief1
	_thief1.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_thief1.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_thief1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_thief1.SetBodyType( b2_dynamicBody );

	// _wallBox1
	_wallBox1.SetRotation( 120.0000f );
	_wallBox1.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox1.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox1.SetBodyType( b2_staticBody );

	// _wallBox2
	_wallBox2.SetRotation( 150.0000f );
	_wallBox2.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox2.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox2.SetBodyType( b2_staticBody );

	// _wallBox3
	_wallBox3.SetRotation( 180.0000f );
	_wallBox3.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox3.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox3.SetBodyType( b2_staticBody );

	// _wallBox4
	_wallBox4.SetRotation( 210.0000f );
	_wallBox4.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox4.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox4.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox4.SetBodyType( b2_staticBody );

	// _wallBox5
	_wallBox5.SetRotation( 240.0000f );
	_wallBox5.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox5.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox5.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox5.SetBodyType( b2_staticBody );

	// _wallBox6
	_wallBox6.SetRotation( -12.0000f );
	_wallBox6.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox6.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox6.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox6.SetBodyType( b2_staticBody );

	// _wallBox7
	_wallBox7.SetRotation( 12.0000f );
	_wallBox7.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox7.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox7.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox7.SetBodyType( b2_staticBody );

	// _woodBox1
	_woodBox1.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox1.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_woodBox1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_woodBox1.SetBodyType( b2_dynamicBody );


	//Set blocks positions
	_coin1.SetPosition( 7.5000f, 29.2500f );
	_cop1.SetPosition( 26.8000f, 26.3000f );
	_door_11.SetPosition( 36.6000f, 23.6000f );
	_door_12.SetPosition( 61.6000f, 23.6000f );
	_door_13.SetPosition( 57.3000f, 23.6000f );
	_fragileBox1.SetPosition( 33.5000f, 53.1500f );
	_fragileBox2.SetPosition( 23.5000f, 53.1500f );
	_fragileBox3.SetPosition( 72.5000f, 53.1500f );
	_fragileBox4.SetPosition( 62.5000f, 53.1500f );
	_fragileBox5.SetPosition( 16.6500f, 28.1000f );
	_fragileBox6.SetPosition( 78.8000f, 27.9000f );
	_piggyBank1.SetPosition( 88.5000f, 30.2000f );
	_screw1.SetPosition( 48.0000f, 47.2500f );
	_thief1.SetPosition( 69.0000f, 26.0000f );
	_wallBox1.SetPosition( 42.3708f, 7.5000f );
	_wallBox2.SetPosition( 44.7500f, 5.1208f );
	_wallBox3.SetPosition( 48.0000f, 4.2500f );
	_wallBox4.SetPosition( 51.2500f, 5.1208f );
	_wallBox5.SetPosition( 53.6292f, 7.5000f );
	_wallBox6.SetPosition( 22.5000f, 19.1500f );
	_wallBox7.SetPosition( 73.5000f, 19.1500f );
	_woodBox1.SetPosition( 48.0000f, 47.2500f );


	//-------------------------------------
	// Custom
	_doorSwitch1.SetTarget( &_door_11 );
	_doorSwitch2.SetTarget( &_door_12 );
	_doorSwitch3.SetTarget( &_door_13 );

	_doorSwitch1.SetPosition( 20.3f, 41.50f );
	_doorSwitch2.SetPosition( 48.0f, 11.0f );
	_doorSwitch3.SetPosition( 75.70f, 41.50f );

	filter.groupIndex = 0;
	filter.categoryBits = Config::eFilterCategoryDoor;
	filter.maskBits = ~Config::eFilterCategoryDoor & 0xffff;

	_wallBox6.GetBody()->GetFixtureList()->SetFilterData( filter );
	_wallBox7.GetBody()->GetFixtureList()->SetFilterData( filter );


	ConstFinal();
	CreateJoints();
}
//--------------------------------------------------------------------------
void LevelScale_1::CreateJoints()
{
	b2RevoluteJointDef jointDef1;
	jointDef1.Initialize( _woodBox1.GetBody(), _screw1.GetBody(), _screw1.GetPosition());
	_jointRev1 = (b2RevoluteJoint *) _world.CreateJoint( &jointDef1 );

	_jointRev1->SetLimits( -0.15f, 0.15f );
	_jointRev1->EnableLimit( true );
	_jointRev1->EnableMotor( true );
	_jointRev1->SetMotorSpeed( 0.0f );
	_jointRev1->SetMaxMotorTorque( 0.0f );
}
//--------------------------------------------------------------------------
LevelScale_1::~LevelScale_1()
{
}
//--------------------------------------------------------------------------
void LevelScale_1::Quiting()
{
	_contactSolver.Quiting();
	Level::Quiting();
}

