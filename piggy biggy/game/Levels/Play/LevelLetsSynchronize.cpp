#include "LevelLetsSynchronize.h"

//--------------------------------------------------------------------------
LevelSynchronize_1::LevelSynchronize_1( GameTypes::LevelEnum levelEnum, bool viewMode ) : Level( levelEnum, viewMode )
{
	_coinCount = 2;

	// _coin1
	_coin1.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin1.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin1.SetBodyType( b2_dynamicBody );

	// _coin2
	_coin2.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin2.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin2.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin2.SetBodyType( b2_dynamicBody );

	// _fragileBox1
	_fragileBox1.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox1.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox1.SetBodyType( b2_dynamicBody );

	// _fragileBox2
	_fragileBox2.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox2.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox2.SetBodyType( b2_dynamicBody );

	// _fragileBox3
	_fragileBox3.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox3.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox3.SetBodyType( b2_dynamicBody );

	// _piggyBank1
	_piggyBank1.GetBody()->GetFixtureList()->SetDensity( 7.5000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_piggyBank1.SetBodyType( b2_dynamicBody );

	// _wallBox1
	_wallBox1.SetRotation( 8.0000f );
	_wallBox1.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox1.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox1.SetBodyType( b2_staticBody );

	// _wallBox2
	_wallBox2.SetRotation( 8.0000f );
	_wallBox2.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox2.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox2.SetBodyType( b2_staticBody );

	// _wallBox3
	_wallBox3.SetRotation( 8.0000f );
	_wallBox3.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox3.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox3.SetBodyType( b2_staticBody );


	//Set blocks positions
	_coin1.SetPosition( 75.5997f, 22.0000f );
	_coin2.SetPosition( 75.5997f, 57.0000f );
	_fragileBox1.SetPosition( 65.8499f, 39.0000f );
	_fragileBox2.SetPosition( 65.8499f, 57.0000f );
	_fragileBox3.SetPosition( 65.8499f, 22.0000f );
	_piggyBank1.SetPosition( 77.3499f, 41.4500f );
	_wallBox1.SetPosition( 59.3499f, 31.0000f );
	_wallBox2.SetPosition( 59.3499f, 49.0000f );
	_wallBox3.SetPosition( 59.3499f, 13.0000f );

	ConstFinal();
}
//--------------------------------------------------------------------------
LevelSynchronize_2::LevelSynchronize_2( GameTypes::LevelEnum levelEnum, bool viewMode ) : Level( levelEnum, viewMode )
{
	_coinCount = 2;

	// _coin1
	_coin1.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin1.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin1.SetBodyType( b2_dynamicBody );

	// _coin2
	_coin2.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin2.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin2.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin2.SetBodyType( b2_dynamicBody );

	// _fragileBox1
	_fragileBox1.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox1.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox1.SetBodyType( b2_dynamicBody );

	// _fragileBox2
	_fragileBox2.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox2.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox2.SetBodyType( b2_dynamicBody );

	// _fragileBox3
	_fragileBox3.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox3.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox3.SetBodyType( b2_dynamicBody );

	// _piggyBank1
	_piggyBank1.GetBody()->GetFixtureList()->SetDensity( 7.5000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_piggyBank1.SetBodyType( b2_dynamicBody );

	// _wallBox1
	_wallBox1.SetRotation( 8.0000f );
	_wallBox1.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox1.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox1.SetBodyType( b2_staticBody );

	// _wallBox2
	_wallBox2.SetRotation( 8.0000f );
	_wallBox2.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox2.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox2.SetBodyType( b2_staticBody );

	// _wallBox3
	_wallBox3.SetRotation( 8.0000f );
	_wallBox3.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox3.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox3.SetBodyType( b2_staticBody );


	//Set blocks positions
	_coin1.SetPosition( 60.9498f, 21.2000f );
	_coin2.SetPosition( 53.3500f, 55.0000f );
	_fragileBox1.SetPosition( 65.3498f, 40.0000f );
	_fragileBox2.SetPosition( 43.3500f, 55.4000f );
	_fragileBox3.SetPosition( 50.9500f, 21.6000f );
	_piggyBank1.SetPosition( 77.3498f, 42.4000f );
	_wallBox1.SetPosition( 59.3499f, 31.0000f );
	_wallBox2.SetPosition( 59.3499f, 49.0000f );
	_wallBox3.SetPosition( 59.3499f, 13.0000f );

	ConstFinal();
}
//--------------------------------------------------------------------------
LevelSynchronize_3::LevelSynchronize_3( GameTypes::LevelEnum levelEnum, bool viewMode ) : Level( levelEnum, viewMode )
{
	_coinCount = 5;

	// _coin1
	_coin1.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin1.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin1.SetBodyType( b2_dynamicBody );

	// _coin2
	_coin2.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin2.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin2.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin2.SetBodyType( b2_dynamicBody );

	// _coin3
	_coin3.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin3.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin3.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin3.SetBodyType( b2_dynamicBody );

	// _coin4
	_coin4.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin4.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin4.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin4.SetBodyType( b2_dynamicBody );

	// _coin5
	_coin5.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin5.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin5.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin5.SetBodyType( b2_dynamicBody );

	// _fragileBox1
	_fragileBox1.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox1.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox1.SetBodyType( b2_dynamicBody );

	// _fragileBox2
	_fragileBox2.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox2.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox2.SetBodyType( b2_dynamicBody );

	// _fragileBox3
	_fragileBox3.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox3.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox3.SetBodyType( b2_dynamicBody );

	// _fragileBox4
	_fragileBox4.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox4.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox4.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox4.SetBodyType( b2_dynamicBody );

	// _fragileBox5
	_fragileBox5.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox5.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox5.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox5.SetBodyType( b2_dynamicBody );

	// _fragileBox6
	_fragileBox6.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox6.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox6.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox6.SetBodyType( b2_dynamicBody );

	// _piggyBank1
	_piggyBank1.GetBody()->GetFixtureList()->SetDensity( 7.5000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_piggyBank1.SetBodyType( b2_dynamicBody );

	// _wallBox1
	_wallBox1.SetRotation( 8.0000f );
	_wallBox1.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox1.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox1.SetBodyType( b2_staticBody );

	// _wallBox2
	_wallBox2.SetRotation( -8.0000f );
	_wallBox2.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox2.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox2.SetBodyType( b2_staticBody );

	// _wallBox3
	_wallBox3.SetRotation( -8.0000f );
	_wallBox3.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox3.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox3.SetBodyType( b2_staticBody );

	// _wallBox4
	_wallBox4.SetRotation( -8.0000f );
	_wallBox4.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox4.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox4.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox4.SetBodyType( b2_staticBody );

	// _wallBox5
	_wallBox5.SetRotation( 8.0000f );
	_wallBox5.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox5.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox5.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox5.SetBodyType( b2_staticBody );

	// _wallBox6
	_wallBox6.SetRotation( 8.0000f );
	_wallBox6.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox6.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox6.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox6.SetBodyType( b2_staticBody );


	//Set blocks positions
	_coin1.SetPosition( 15.2502f, 55.5000f );
	_coin2.SetPosition( 23.5001f, 34.5000f );
	_coin3.SetPosition( 5.5000f, 16.9000f );
	_coin4.SetPosition( 72.4017f, 34.5000f );
	_coin5.SetPosition( 90.5017f, 16.9000f );
	_fragileBox1.SetPosition( 71.5999f, 55.5000f );
	_fragileBox2.SetPosition( 24.4001f, 55.5000f );
	_fragileBox3.SetPosition( 32.9999f, 34.5000f );
	_fragileBox4.SetPosition( 14.9001f, 16.9000f );
	_fragileBox5.SetPosition( 62.9999f, 34.5000f );
	_fragileBox6.SetPosition( 81.0999f, 16.9000f );
	_piggyBank1.SetPosition( 82.4999f, 57.6999f );
	_wallBox1.SetPosition( 75.6999f, 29.1000f );
	_wallBox2.SetPosition( 20.6001f, 9.1000f );
	_wallBox3.SetPosition( 20.6001f, 49.0500f );
	_wallBox4.SetPosition( 20.6001f, 29.1000f );
	_wallBox5.SetPosition( 75.6999f, 49.0500f );
	_wallBox6.SetPosition( 75.6999f, 9.1000f );

	ConstFinal();
}
//--------------------------------------------------------------------------

