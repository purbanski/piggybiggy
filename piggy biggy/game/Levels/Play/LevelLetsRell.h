#ifndef __LEVELLETSRELL_H__
#define __LEVELLETSRELL_H__

#include <Box2D/Box2D.h>
#include "Levels/Level.h"
#include "Blocks/AllBlocks.h"

//--------------------------------------------------------------------------------------------
class LevelLetsRell_1 : public Level
{
public:
	LevelLetsRell_1( GameTypes::LevelEnum levelEnum, bool viewMode );

private:
	puCoin< 40 >	_coin1;
	puPiggyBank< 60 >	_piggyBank1;
	puReel< 150 >	_reel1;
	puWallBox<480,20,eBlockScaled>	_wallBox1;
};


//--------------------------------------------------------------------------------------------
class LevelLetsRell_2 : public Level
{
public:
	LevelLetsRell_2( GameTypes::LevelEnum levelEnum, bool viewMode );

private:
	puCoin< 40 >			_coin1;
	puReel< 150 >			_reel1;
	puPiggyBank< 60 >		_piggyBank1;
	puPolygonWall_Custom1	_polygonWall1;
	puWallBox<500,20,eBlockScaled>	_wallBox1;

};
//--------------------------------------------------------------------------------------------
class LevelLetsRell_3 : public Level
{
public:
	LevelLetsRell_3( GameTypes::LevelEnum levelEnum, bool viewMode );

private:
	puCoin< 40 >			_coin1;
	puWallBox<860,20>		_floor11;
	puPiggyBank< 60 >		_piggyBank1;
	puPolygonWall_Custom2	_polygonWall1;
	puReel< 150 >			_reel1;
};
//--------------------------------------------------------------------------------------------

#endif
