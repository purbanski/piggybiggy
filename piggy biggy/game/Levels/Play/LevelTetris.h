#ifndef __LEVELTETRIS_H__
#define __LEVELTETRIS_H__

#include <Box2D/Box2D.h>
#include "Levels/LevelWithCallback.h"
#include "Blocks/puFloor1.h"
#include "Blocks/puTetris.h"
#include "CommonDefs.h"

//---------------------------------------------------------------------------------
class LevelTetris_1 : public LevelWithCallback
{
public:
	LevelTetris_1( GameTypes::LevelEnum levelEnum, bool viewMode );

	void RandomHandTap();

	virtual ~LevelTetris_1();

protected:
	virtual void CounterCallback();

private:
	BlockContainer	_blocks;
	BlockContainer::iterator	_fallingBlock;

	puCoin<>		_coin1;
	puPiggyBank<>	_piggyBank1;

	puWallBox<200,20,eBlockScaled>		_wall3;
	puWallBox<160,20,eBlockScaled>		_wall4;
	puFragileBox<96,96,eBlockScaled>	_fragileBox1;

	puWallBox<550,20,eBlockScaled>		_floor11;
	
	
	puTetrisSM		_tetrisSM1;
	puTetrisLM		_tetrisLM1;
	puTetrisS		_tetrisS1;
	puTetrisSM		_tetrisSM2;
	puTetrisT		_tetrisT1;
	puTetrisLM		_tetrisLM2;
	puTetrisSquare	_tetrisSquare1;
	puTetrisSM		_tetrisSM3;
	puTetrisLM		_tetrisLM3;
	puTetrisL		_tetrisL1;

	
	puTetrisS		_fake1;
	//puTetrisI		_fake2;
	puTetrisL		_fake3;
	puTetrisL		_fake4;


	puTetrisSquare	_fake5;
	puTetrisT		_fake6;
	puTetrisS		_fake7;


	puTetrisSM		_fake8;
	puTetrisT		_fake9;
	puTetrisS		_fake10;
	puTetrisT		_fake11;


	puTetrisS		_fake12;
	puTetrisSM		_fake13;
	puTetrisSquare	_fake14;
};
//---------------------------------------------------------------------------------
class LevelTetris_2 : public LevelWithCallback
{
public:
	LevelTetris_2( GameTypes::LevelEnum levelEnum, bool viewMode );
	virtual ~LevelTetris_2();

protected:
	virtual void CounterCallback();

private:
	BlockContainer	_blocks;
	BlockContainer::iterator	_fallingBlock;

	puThief<40>			_thief;
	puCoin<40>			_coin1;
	puPiggyBank<44>		_piggyBank1;

	puWallBox<200,20,eBlockScaled>		_wallLeftUp;
	puWallBox<200,20,eBlockScaled>		_wallLeftDown;
	
	puWallBox<160,20,eBlockScaled>		_wallRightUp;
	puWallBox<160,20,eBlockScaled>		_wallRightDown;
	puFragileBox<72,72,eBlockScaled>	_fragileBox1;
	puFragileBox<72,72,eBlockScaled>	_fragileBox2;
	puFragileBox<72,72,eBlockScaled>	_fragileBox3;

	puDoor_1		_door1;
	puSwitchBase	_doorSwitch1;

	puWallBox<550,20,eBlockScaled>		_floor11;

	puTetrisI		_tetrisI1;
	puTetrisI		_tetrisI2;
	puTetrisL		_tetrisL1;
	puTetrisLM		_tetrisLM1;
	puTetrisLM		_tetrisLM2;
	puTetrisS		_tetrisS1;
	puTetrisSM		_tetrisSM1;
	puTetrisSM		_tetrisSM2;
	puTetrisSM		_tetrisSM3;
	puTetrisSquare	_tetrisSquare1;
	puTetrisSquare	_tetrisSquare2;
	puTetrisT		_tetrisT1;
	puTetrisT		_tetrisT2;

	puTetrisS		_fake1;
	puTetrisLM		_fake2;
	puTetrisS		_fake3;
	puTetrisI		_fake4;
	puTetrisLM		_fake5;
	puTetrisL		_fake6;
	puTetrisSquare	_fake7;

	puTetrisL		_fake8;
	puTetrisS		_fake9;
	puTetrisL		_fake10;
	puTetrisT		_fake11;
	puTetrisT		_fake12;
};
//---------------------------------------------------------------------------------
#endif
