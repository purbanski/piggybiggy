#ifndef __LEVEL1000_H__
#define __LEVEL1000_H__

#include <Box2D/Box2D.h>
#include "Levels/Level.h"
#include "Levels/LevelAccelerometr.h"
#include "Levels/LevelWater.h"

#include "Blocks/puCastel.h"

//--------------------------------------------------------------------
class LevelScreenshot : public Level
{
public:
	LevelScreenshot( GameTypes::LevelEnum levelEnum, bool viewMode );
};
//--------------------------------------------------------------------
class LevelTestEmpty : public Level
{
public:
	LevelTestEmpty( GameTypes::LevelEnum levelEnum, bool viewMode );
	virtual void Step( cocos2d::ccTime dt );
	virtual void LevelRun();

private:
	puVehical	*_vehical;
};
//--------------------------------------------------------------------
class LevelTestEmptyWithSkin : public Level
{
public:
	LevelTestEmptyWithSkin( GameTypes::LevelEnum levelEnum, bool viewMode );
};
//--------------------------------------------------------------------
class LevelTestVertical : public LevelVertical
{
public:
	LevelTestVertical( GameTypes::LevelEnum levelEnum, bool viewMode );
};
//--------------------------------------------------------------------
class LevelTestAccelerometr : public LevelAccelerometr
{
public:
	LevelTestAccelerometr( GameTypes::LevelEnum levelEnum, bool viewMode );
};
//--------------------------------------------------------------------
class LevelTestSpace : public Level
{
public:
	LevelTestSpace( GameTypes::LevelEnum levelEnum, bool viewMode );
};
//--------------------------------------------------------------------
class LevelTestDestroyBlocks : public Level
{
public:
	LevelTestDestroyBlocks( GameTypes::LevelEnum levelEnum, bool viewMode );

private:
	puCoin< 20 >	_coin1;
	puCoin< 20 >	_coin2;
	puFragileBox<48,48,eBlockScaled>	_fragileBox1;
	puFragileBox<48,48,eBlockScaled>	_fragileBox2;
	puFragileBox<48,48,eBlockScaled>	_fragileBox3;
	puPiggyBank< 30 >	_piggyBank1;
	puWallBox<240,10,eBlockScaled>	_wallBox1;
	puWallBox<240,10,eBlockScaled>	_wallBox2;
	puWallBox<240,10,eBlockScaled>	_wallBox3;
};
//--------------------------------------------------------------------




//--------------------------------------------------------------------
// Editor Aquarium Test
//--------------------------------------------------------------------
class LevelEditorAquarium : public LevelAquarium
{
public:
	LevelEditorAquarium( GameTypes::LevelEnum levelEnum, bool viewMode );
};


//--------------------------------------------------------------------
// Editor Sea Test
//--------------------------------------------------------------------
class LevelEditorSea: public LevelSea
{
public:
	LevelEditorSea( GameTypes::LevelEnum levelEnum, bool viewMode );
};


//--------------------------------------------------------------------
// Aquarium Test
//--------------------------------------------------------------------
class LevelTestAquarium : public LevelAquarium
{
public:
	LevelTestAquarium( GameTypes::LevelEnum levelEnum, bool viewMode );

private:
	puAirBubble_	_airBubble1;
	puCircleStone< 30 >	_circleStone1;
	puCircleStone< 30 >	_circleStone2;
	puCircleStone< 30 >	_circleStone3;
	puCircleStone< 30 >	_circleStone4;
	puCircleStone< 30 >	_circleStone5;
	puCircleWood< 30 >	_circleWood1;
	puCircleWood< 30 >	_circleWood2;
	puCircleWood< 30 >	_circleWood3;
	puCoin< 20 >	_coin1;
	puPiggyBank< 30 >	_piggyBank1;

};


//--------------------------------------------------------------------
// Aquarium Test
//--------------------------------------------------------------------
class LevelTestAquariumBlank : public LevelAquarium
{
public:
	LevelTestAquariumBlank( GameTypes::LevelEnum levelEnum, bool viewMode );

private:
};



//--------------------------------------------------------------------
// Sea Test
//--------------------------------------------------------------------
class LevelTestSea : public LevelSea
{
public:
	LevelTestSea( GameTypes::LevelEnum levelEnum, bool viewMode );

private:
	puAirBubble_	_airBubble1;
	puCircleStone< 30 >	_circleStone1;
	puCircleStone< 30 >	_circleStone2;
	puCircleStone< 30 >	_circleStone3;
	puCircleStone< 30 >	_circleStone4;
	puCircleStone< 30 >	_circleStone5;
	puCircleWood< 30 >	_circleWood1;
	puCircleWood< 30 >	_circleWood2;
	puCircleWood< 30 >	_circleWood3;
	puCoin< 20 >	_coin1;
	puPiggyBank< 30 >	_piggyBank1;
};



//--------------------------------------------------------------------
// Konkurs
//--------------------------------------------------------------------
class LevelKonkurs : public Level
{
public:
	LevelKonkurs( GameTypes::LevelEnum levelEnum, bool viewMode );

private:
	puFloor2	_floor21;
	puFloor2	_floor22;
	puFloor2	_floor23;
	puFloor2	_floor24;
	puFloor2	_floor25;
	puPiggyBank< 30 >	_piggyBank1;
	puPiggyBank< 30 >	_piggyBank2;
	puPiggyBank< 30 >	_piggyBank3;
	puPiggyBank< 30 >	_piggyBank4;
};


//--------------------------------------------------------------------
// Level Test
//--------------------------------------------------------------------
class LevelUserTest : public Level
{
public:
	LevelUserTest( GameTypes::LevelEnum levelEnum, bool viewMode );

private:
	puFragileBoxStatic<192,96,eBlockScaled>	_fragileBoxStatic1;
	puFragileBoxStatic<192,96,eBlockScaled>	_fragileBoxStatic2;
	puGun	_gun1;
	puGunExplosive	_gunExplosive1;
	puPiggyBank< 60 >	_piggyBank1;
	puPiggyBank< 60 >	_piggyBank2;
	puPiggyBank< 60 >	_piggyBank3;
	puPiggyBank< 60 >	_piggyBank4;
	puPiggyBankStatic< 60 >	_piggyBankStatic1;
	puPiggyBankStatic< 60 >	_piggyBankStatic2;
	puWallBox<740,20,eBlockScaled>	_wallBox1;
	puWallBox<910,20,eBlockScaled>	_wallBox2;
	puWallBox<910,20,eBlockScaled>	_wallBox3;
};


#endif
