#include "LevelLetsRell.h"

//--------------------------------------------------------------------------
LevelLetsRell_1::LevelLetsRell_1( GameTypes::LevelEnum levelEnum, bool viewMode ) : Level( levelEnum, viewMode )
{
    _levelTag = eLevelTag_Reel;
    
	// _coin1
	// _coin1
	_coin1.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin1.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin1.GetBody()->SetAngularDamping(0.1500f );
	_coin1.GetBody()->SetLinearDamping(0.1500f );
	_coin1.SetBodyType( b2_dynamicBody );

	// _piggyBank1
	_piggyBank1.GetBody()->GetFixtureList()->SetDensity( 7.5000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_piggyBank1.FlipX();
	_piggyBank1.SetBodyType( b2_dynamicBody );

	// _reel1
	_reel1.GetBody()->GetFixtureList()->SetDensity( 5.0000f );
	_reel1.GetBody()->GetFixtureList()->SetFriction( 0.2000f );
	_reel1.GetBody()->GetFixtureList()->SetRestitution( 0.5000f );
	_reel1.SetBodyType( b2_dynamicBody );

	// _wallBox1
	_wallBox1.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox1.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox1.SetBodyType( b2_staticBody );


	//Set blocks positions
	_coin1.SetPosition( 77.0000f, 59.5998f );
	_piggyBank1.SetPosition( 10.0000f, 27.5999f );
	_reel1.SetPosition( 77.0000f, 34.5999f );
	_wallBox1.SetPosition( 29.5000f, 5.6000f );

	_msgManager.push_back( eOTMsg_IntroRell, new LevelMsg_ReelIntro(
		Config::LevelFirstMsgTick /*+ 60*/,
		2.0f, // duration
		"", 
		b2Vec2( 0.0f, 0.0f ), // msg position
		b2Vec2( 80.0f, 46.0f ), // position
		b2Vec2( -3.0f, -14.0f ) // move
		));	

	ConstFinal();
}
//--------------------------------------------------------------------------
LevelLetsRell_2::LevelLetsRell_2( GameTypes::LevelEnum levelEnum, bool viewMode ) : Level( levelEnum, viewMode )
{
    _levelTag = eLevelTag_Reel;

	// _coin1
	_coin1.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin1.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin1.SetBodyType( b2_dynamicBody );

	// _piggyBank1
	_piggyBank1.GetBody()->GetFixtureList()->SetDensity( 7.5000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_piggyBank1.SetBodyType( b2_dynamicBody );
	_piggyBank1.FlipX();

	// _reel1
	_reel1.GetBody()->GetFixtureList()->SetDensity( 5.0000f );
	_reel1.GetBody()->GetFixtureList()->SetFriction( 0.2000f );
	_reel1.GetBody()->GetFixtureList()->SetRestitution( 0.5000f );
	_reel1.SetBodyType( b2_dynamicBody );

	// _wallBox1
	_wallBox1.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox1.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox1.SetBodyType( b2_staticBody );


	//Set blocks positions
	_coin1.SetPosition( 77.0000f, 59.5998f );
	_piggyBank1.SetPosition( 10.50f, 27.5999f );
	_reel1.SetPosition( 77.0000f, 34.5999f );
	_wallBox1.SetPosition( 29.6500f, 5.6000f );
	_polygonWall1.SetPosition( 20.5, 10.65f );

	ConstFinal();
}
//--------------------------------------------------------------------------
LevelLetsRell_3::LevelLetsRell_3( GameTypes::LevelEnum levelEnum, bool viewMode ) : Level( levelEnum, viewMode )
{
    _levelTag = eLevelTag_Reel;
    
	// _coin1
	_coin1.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin1.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin1.SetBodyType( b2_dynamicBody );

	// _floor11
	_floor11.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_floor11.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_floor11.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_floor11.SetBodyType( b2_staticBody );

	// _piggyBank1
	_piggyBank1.GetBody()->GetFixtureList()->SetDensity( 7.5000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_piggyBank1.SetBodyType( b2_dynamicBody );

	// _polygonWall1
	_polygonWall1.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_polygonWall1.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_polygonWall1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_polygonWall1.SetBodyType( b2_staticBody );

	// _reel1
	_reel1.GetBody()->GetFixtureList()->SetDensity( 5.0000f );
	_reel1.GetBody()->GetFixtureList()->SetFriction( 0.2000f );
	_reel1.GetBody()->GetFixtureList()->SetRestitution( 0.5000f );
	_reel1.SetBodyType( b2_dynamicBody );


	//Set blocks positions
	_coin1.SetPosition( 48.0000f, 59.7998f );
	_floor11.SetPosition( 48.0000f, 3.8000f );
	_piggyBank1.SetPosition( 48.0000f, 14.8000f );
	_polygonWall1.SetPosition( 48.0000f, 5.8000f );
	_reel1.SetPosition( 48.0000f, 38.8000f );

	ConstFinal();
}
