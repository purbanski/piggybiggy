#include "LevelLetsTest.h"
#include "Tools.h"
#include "Game.h"

//--------------------------------------------------------------------------
LevelScreenshot::LevelScreenshot( GameTypes::LevelEnum levelEnum, bool viewMode ) : Level( levelEnum, viewMode )
{
	_background = CCSprite::spriteWithFile("Editor/Images/Levels/bg-screenshot.png");
	ConstFinal();
}
//--------------------------------------------------------------------------
LevelTestEmpty::LevelTestEmpty( GameTypes::LevelEnum levelEnum, bool viewMode ) : Level( levelEnum, viewMode )
{
	_coinCount = 60;
	_background = CCSprite::spriteWithFile("Editor/Images/Levels/bg.png");
	_styleSet = true;

	_vehical = NULL;

	ConstFinal();
}
//--------------------------------------------------------------------------
void LevelTestEmpty::Step( cocos2d::ccTime dt )
{
	Level::Step( dt );
	SoundEngine::Get()->Update();
	if ( _vehical )	
		_vehical->Update();
}
//--------------------------------------------------------------------------
void LevelTestEmpty::LevelRun()
{
	//-----
	// Check for vehical
	puBlock *block;
	b2Body *body;
	
	body = _world.GetBodyList();
	while ( body )
	{
		block = (puBlock *) body->GetUserData();
		if (( block ) && ( block->GetBlockEnum() == eBlockVehical ))
		{
			_vehical = (puVehical *) block;
			break;
		}
		body = body->GetNext();
	}

	Level::LevelRun();
}
//--------------------------------------------------------------------------
LevelTestEmptyWithSkin::LevelTestEmptyWithSkin( GameTypes::LevelEnum levelEnum, bool viewMode ) : Level( levelEnum, viewMode )
{
	_coinCount = 60;
	ConstFinal();
}
//--------------------------------------------------------------------------
LevelTestVertical::LevelTestVertical( GameTypes::LevelEnum levelEnum, bool viewMode ) : LevelVertical( levelEnum, viewMode )
{
	_coinCount = 60;
	_background = CCSprite::spriteWithFile( "Editor/Images/Levels/bg-vert.png" );
	//_world.SetGravity( b2Vec2( 10.0f, 0.0f ));
	ConstFinal();
}
//--------------------------------------------------------------------------
LevelTestAccelerometr::LevelTestAccelerometr( GameTypes::LevelEnum levelEnum, bool viewMode ) : LevelAccelerometr( levelEnum, viewMode )
{
	_coinCount = 60;
	//_background = CCSprite::spriteWithFile( "Editor/Images/Levels/bg-accel.png" );
	ConstFinal();
}
//--------------------------------------------------------------------------
LevelTestSpace::LevelTestSpace( GameTypes::LevelEnum levelEnum, bool viewMode ) : Level( levelEnum, viewMode )
{
	_coinCount = 60;
	//_background = CCSprite::spriteWithFile( "Editor/Images/Levels/bg-space.png" );
	_world.SetGravity( b2Vec2( 0.0f, 0.0f ));
	ConstFinal();
}
//--------------------------------------------------------------------------
LevelTestDestroyBlocks::LevelTestDestroyBlocks( GameTypes::LevelEnum levelEnum, bool viewMode ) : Level( levelEnum, viewMode )
{
	// _coin1
	_coin1.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin1.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin1.SetBodyType( b2_dynamicBody );

	// _coin2
	_coin2.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin2.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin2.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin2.SetBodyType( b2_dynamicBody );

	// _fragileBox1
	_fragileBox1.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox1.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox1.SetBodyType( b2_dynamicBody );

	// _fragileBox2
	_fragileBox2.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox2.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox2.SetBodyType( b2_dynamicBody );

	// _fragileBox3
	_fragileBox3.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox3.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox3.SetBodyType( b2_dynamicBody );

	// _piggyBank1
	_piggyBank1.GetBody()->GetFixtureList()->SetDensity( 7.5000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_piggyBank1.SetBodyType( b2_dynamicBody );

	// _wallBox1
	_wallBox1.SetRotation( 8.0000f );
	_wallBox1.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox1.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox1.SetBodyType( b2_staticBody );

	// _wallBox2
	_wallBox2.SetRotation( 8.0000f );
	_wallBox2.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox2.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox2.SetBodyType( b2_staticBody );

	// _wallBox3
	_wallBox3.SetRotation( 8.0000f );
	_wallBox3.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox3.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox3.SetBodyType( b2_staticBody );


	//Set blocks positions
	_coin1.SetPosition( 29.8500f, 4.8000f );
	_coin2.SetPosition( 22.7500f, 28.5000f );
	_fragileBox1.SetPosition( 35.1000f, 27.7501f );
	_fragileBox2.SetPosition( 17.7500f, 28.7000f );
	_fragileBox3.SetPosition( 24.9500f, 4.6000f );
	_piggyBank1.SetPosition( 41.0999f, 28.9501f );
	_wallBox1.SetPosition( -8.5500f, 25.4000f );
	_wallBox2.SetPosition( 14.7500f, 24.2000f );
	_wallBox3.SetPosition( 34.6500f, 1.9500f );
	
	ConstFinal();
}
//--------------------------------------------------------------------------




//--------------------------------------------------------------------------
// Level Aquarium Test
//--------------------------------------------------------------------------
LevelTestAquarium::LevelTestAquarium( GameTypes::LevelEnum levelEnum, bool viewMode ) : LevelAquarium( levelEnum, viewMode )
{
	_coinCount = 2;

	// _coin1
	_coin1.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin1.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin1.SetBouyancyRate(0.1500f );
	_coin1.SetBodyType( b2_dynamicBody );

	// _circleStone1
	_circleStone1.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_circleStone1.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_circleStone1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_circleStone1.SetBouyancyRate(0.2500f );
	_circleStone1.SetBodyType( b2_dynamicBody );

	// _circleStone2
	_circleStone2.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_circleStone2.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_circleStone2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_circleStone2.SetBouyancyRate(0.2500f );
	_circleStone2.SetBodyType( b2_dynamicBody );

	// _circleStone3
	_circleStone3.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_circleStone3.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_circleStone3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_circleStone3.SetBouyancyRate(0.2500f );
	_circleStone3.SetBodyType( b2_dynamicBody );

	// _circleStone4
	_circleStone4.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_circleStone4.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_circleStone4.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_circleStone4.SetBouyancyRate(0.2500f );
	_circleStone4.SetBodyType( b2_dynamicBody );

	// _circleStone5
	_circleStone5.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_circleStone5.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_circleStone5.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_circleStone5.SetBouyancyRate(0.2400f );
	_circleStone5.SetBodyType( b2_dynamicBody );

	// _circleWood1
	_circleWood1.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_circleWood1.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_circleWood1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_circleWood1.SetBouyancyRate(0.6000f );
	_circleWood1.SetBodyType( b2_dynamicBody );

	// _circleWood2
	_circleWood2.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_circleWood2.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_circleWood2.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_circleWood2.SetBouyancyRate(1.2600f );
	_circleWood2.SetBodyType( b2_dynamicBody );

	// _circleWood3
	_circleWood3.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_circleWood3.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_circleWood3.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_circleWood3.SetBouyancyRate(1.2600f );
	_circleWood3.SetBodyType( b2_dynamicBody );

	// _piggyBank1
	_piggyBank1.GetBody()->GetFixtureList()->SetDensity( 7.5000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_piggyBank1.SetBouyancyRate(0.6100f );
	_piggyBank1.SetBodyType( b2_dynamicBody );

	// _airBubble1
	_airBubble1.GetBody()->GetFixtureList()->SetDensity( 7.5000f );
	_airBubble1.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_airBubble1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_airBubble1.SetBouyancyRate(0.9000f );
	_airBubble1.SetBodyType( b2_dynamicBody );


	//Set blocks positions
	_coin1.SetPosition( 43.7000f, 22.9000f );
	_circleStone1.SetPosition( 10.2500f, 10.7001f );
	_circleStone2.SetPosition( 3.9500f, 10.5501f );
	_circleStone3.SetPosition( 4.3500f, 17.3001f );
	_circleStone4.SetPosition( 10.6500f, 17.4501f );
	_circleStone5.SetPosition( 19.0500f, 14.8001f );
	_circleWood1.SetPosition( 25.1500f, 14.9500f );
	_circleWood2.SetPosition( 10.5000f, 4.3500f );
	_circleWood3.SetPosition( 4.1000f, 4.1500f );
	_piggyBank1.SetPosition( 43.9000f, 28.7001f );
	_airBubble1.SetPosition( 31.4500f, 15.2000f );

//	_msgManager.push_back( new LevelMsg_Text( Config::LevelFirstMsgTick, 2.0f, "Aquarium Test", b2Vec2( 0.0f, 0.0f )));
	ConstFinal();
}
//--------------------------------------------------------------------------


//--------------------------------------------------------------------------
// Level Aquarium Test
//--------------------------------------------------------------------------
LevelTestAquariumBlank::LevelTestAquariumBlank( GameTypes::LevelEnum levelEnum, bool viewMode ) : LevelAquarium( levelEnum, viewMode )
{
	_background = CCSprite::spriteWithFile( "Editor/Images/Levels/bg-aquarium.png");
	ConstFinal();
}
//--------------------------------------------------------------------------



//--------------------------------------------------------------------------
// Level Sea Test
//--------------------------------------------------------------------------
LevelTestSea::LevelTestSea( GameTypes::LevelEnum levelEnum, bool viewMode ) : LevelSea( levelEnum, viewMode )
{
	_coinCount = 2;

	// _coin1
	_coin1.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin1.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin1.SetBouyancyRate(0.1500f );
	_coin1.SetBodyType( b2_dynamicBody );

	// _circleStone1
	_circleStone1.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_circleStone1.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_circleStone1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_circleStone1.SetBouyancyRate(0.2500f );
	_circleStone1.SetBodyType( b2_dynamicBody );

	// _circleStone2
	_circleStone2.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_circleStone2.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_circleStone2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_circleStone2.SetBouyancyRate(0.2500f );
	_circleStone2.SetBodyType( b2_dynamicBody );

	// _circleStone3
	_circleStone3.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_circleStone3.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_circleStone3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_circleStone3.SetBouyancyRate(0.2500f );
	_circleStone3.SetBodyType( b2_dynamicBody );

	// _circleStone4
	_circleStone4.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_circleStone4.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_circleStone4.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_circleStone4.SetBouyancyRate(0.2500f );
	_circleStone4.SetBodyType( b2_dynamicBody );

	// _circleStone5
	_circleStone5.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_circleStone5.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_circleStone5.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_circleStone5.SetBouyancyRate(0.2400f );
	_circleStone5.SetBodyType( b2_dynamicBody );

	// _circleWood1
	_circleWood1.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_circleWood1.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_circleWood1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_circleWood1.SetBouyancyRate(0.6000f );
	_circleWood1.SetBodyType( b2_dynamicBody );

	// _circleWood2
	_circleWood2.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_circleWood2.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_circleWood2.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_circleWood2.SetBouyancyRate(1.2600f );
	_circleWood2.SetBodyType( b2_dynamicBody );

	// _circleWood3
	_circleWood3.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_circleWood3.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_circleWood3.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_circleWood3.SetBouyancyRate(1.2600f );
	_circleWood3.SetBodyType( b2_dynamicBody );

	// _piggyBank1
	_piggyBank1.GetBody()->GetFixtureList()->SetDensity( 7.5000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_piggyBank1.SetBouyancyRate(0.6100f );
	_piggyBank1.SetBodyType( b2_dynamicBody );

	// _airBubble1
	_airBubble1.GetBody()->GetFixtureList()->SetDensity( 7.5000f );
	_airBubble1.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_airBubble1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_airBubble1.SetBouyancyRate(0.9000f );
	_airBubble1.SetBodyType( b2_dynamicBody );


	//Set blocks positions
	_coin1.SetPosition( 43.7000f, 20.9000f );
	_circleStone1.SetPosition( 10.2500f, 10.7001f );
	_circleStone2.SetPosition( 3.9500f, 10.5501f );
	_circleStone3.SetPosition( 4.3500f, 17.3001f );
	_circleStone4.SetPosition( 10.6500f, 17.4501f );
	_circleStone5.SetPosition( 19.0500f, 14.8001f );
	_circleWood1.SetPosition( 25.1500f, 14.9500f );
	_circleWood2.SetPosition( 10.5000f, 4.3500f );
	_circleWood3.SetPosition( 4.1000f, 4.1500f );
	_piggyBank1.SetPosition( 43.9000f, 26.7001f );
	_airBubble1.SetPosition( 31.4500f, 15.2000f );

//	_msgManager.push_back( new LevelMsg_Text( Config::LevelFirstMsgTick, 2.0f, "Sea Test", b2Vec2( 0.0f, 0.0f )));
	ConstFinal();
}


//--------------------------------------------------------------------------
// Level Editor Aquarium
//--------------------------------------------------------------------------
LevelEditorAquarium::LevelEditorAquarium( GameTypes::LevelEnum levelEnum, bool viewMode ) : LevelAquarium( levelEnum, viewMode )
{
	ConstFinal();
}


//--------------------------------------------------------------------------
// Level Editor Sea
//--------------------------------------------------------------------------
LevelEditorSea::LevelEditorSea( GameTypes::LevelEnum levelEnum, bool viewMode ) : LevelSea( levelEnum, viewMode )
{
	ConstFinal();
}


//--------------------------------------------------------------------------
// Level Konrkus
//--------------------------------------------------------------------------
LevelKonkurs::LevelKonkurs( GameTypes::LevelEnum levelEnum, bool viewMode ) : Level( levelEnum, viewMode )
{
	// _piggyBank1
	_piggyBank1.GetBody()->GetFixtureList()->SetDensity( 7.5000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_piggyBank1.SetBodyType( b2_dynamicBody );

	// _floor21
	_floor21.SetRotation( -15.0000f );
	_floor21.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_floor21.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_floor21.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_floor21.SetBodyType( b2_staticBody );

	// _floor22
	_floor22.SetRotation( -15.0000f );
	_floor22.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_floor22.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_floor22.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_floor22.SetBodyType( b2_staticBody );

	// _floor23
	_floor23.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_floor23.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_floor23.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_floor23.SetBodyType( b2_staticBody );

	// _floor24
	_floor24.SetRotation( 15.0000f );
	_floor24.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_floor24.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_floor24.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_floor24.SetBodyType( b2_staticBody );

	// _floor25
	_floor25.SetRotation( 90.0000f );
	_floor25.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_floor25.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_floor25.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_floor25.SetBodyType( b2_staticBody );

	// _piggyBank2
	_piggyBank2.GetBody()->GetFixtureList()->SetDensity( 7.5000f );
	_piggyBank2.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_piggyBank2.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_piggyBank2.SetBodyType( b2_dynamicBody );

	// _piggyBank3
	_piggyBank3.GetBody()->GetFixtureList()->SetDensity( 7.5000f );
	_piggyBank3.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_piggyBank3.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_piggyBank3.SetBodyType( b2_dynamicBody );

	// _piggyBank4
	_piggyBank4.GetBody()->GetFixtureList()->SetDensity( 7.5000f );
	_piggyBank4.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_piggyBank4.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_piggyBank4.SetBodyType( b2_dynamicBody );


	//Set blocks positions
	_floor21.SetPosition( -3.0500f, 24.8001f );
	_floor22.SetPosition( 2.9500f, 3.4500f );
	_floor23.SetPosition( 49.7999f, -2.7000f );
	_floor24.SetPosition( 35.1500f, 15.1501f );
	_floor25.SetPosition( 48.9499f, -6.9500f );
	_piggyBank1.SetPosition( -7.3500f, 31.6500f );
	_piggyBank2.SetPosition( 21.6500f, 5.3000f );
	_piggyBank3.SetPosition( -7.7500f, 10.8501f );
	_piggyBank4.SetPosition( 47.0999f, 24.5501f );

	_piggyBank1.SetSprite( CCSprite::spriteWithFile("Editor/Images/test/piggybank.png"));
	_piggyBank2.SetSprite( CCSprite::spriteWithFile("Editor/Images/test/piggybankmiddle.png"));
	_piggyBank3.SetSprite( CCSprite::spriteWithFile("Editor/Images/test/swinka1.png"));
	_piggyBank4.SetSprite( CCSprite::spriteWithFile("Editor/Images/test/swinka2.png"));

	_background = CCSprite::spriteWithFile("Editor/Images/test/bg.png");
	ConstFinal();
}


//--------------------------------------------------------------------------
// Level Konrkus
//--------------------------------------------------------------------------
LevelUserTest::LevelUserTest( GameTypes::LevelEnum levelEnum, bool viewMode ) : Level( levelEnum, viewMode )
{
	// _fragileBoxStatic1
	_fragileBoxStatic1.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBoxStatic1.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBoxStatic1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBoxStatic1.SetBodyType( b2_staticBody );

	// _gun1
	_gun1.GetBody()->GetFixtureList()->SetDensity( 7.5000f );
	_gun1.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_gun1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_gun1.FlipX();
	_gun1.SetBodyType( b2_staticBody );

	// _gunExplosive1
	_gunExplosive1.GetBody()->GetFixtureList()->SetDensity( 7.5000f );
	_gunExplosive1.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_gunExplosive1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_gunExplosive1.SetBodyType( b2_staticBody );

	// _piggyBank1
	_piggyBank1.GetBody()->GetFixtureList()->SetDensity( 7.5000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_piggyBank1.SetBodyType( b2_dynamicBody );

	// _piggyBank2
	_piggyBank2.GetBody()->GetFixtureList()->SetDensity( 7.5000f );
	_piggyBank2.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_piggyBank2.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_piggyBank2.SetBodyType( b2_dynamicBody );

	// _piggyBank3
	_piggyBank3.GetBody()->GetFixtureList()->SetDensity( 7.5000f );
	_piggyBank3.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_piggyBank3.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_piggyBank3.FlipX();
	_piggyBank3.SetBodyType( b2_dynamicBody );

	// _piggyBankStatic1
	_piggyBankStatic1.GetBody()->GetFixtureList()->SetDensity( 7.5000f );
	_piggyBankStatic1.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_piggyBankStatic1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_piggyBankStatic1.SetBodyType( b2_staticBody );

	// _piggyBankStatic2
	_piggyBankStatic2.GetBody()->GetFixtureList()->SetDensity( 7.5000f );
	_piggyBankStatic2.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_piggyBankStatic2.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_piggyBankStatic2.FlipX();
	_piggyBankStatic2.SetBodyType( b2_staticBody );

	// _wallBox1
	_wallBox1.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox1.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox1.SetBodyType( b2_staticBody );

	// _wallBox2
	_wallBox2.SetRotation( 6.0000f );
	_wallBox2.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox2.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox2.SetBodyType( b2_staticBody );

	// _fragileBoxStatic2
	_fragileBoxStatic2.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBoxStatic2.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBoxStatic2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBoxStatic2.SetBodyType( b2_staticBody );

	// _wallBox3
	_wallBox3.SetRotation( 6.0000f );
	_wallBox3.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox3.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox3.SetBodyType( b2_staticBody );

	// _piggyBank4
	_piggyBank4.GetBody()->GetFixtureList()->SetDensity( 7.5000f );
	_piggyBank4.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_piggyBank4.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_piggyBank4.FlipX();
	_piggyBank4.SetBodyType( b2_dynamicBody );


	//Set blocks positions
	_fragileBoxStatic1.SetPosition( 15.5000f, 44.7000f );
	_gun1.SetPosition( 21.8000f, 58.9000f );
	_gunExplosive1.SetPosition( 83.1000f, 14.1000f );
	_piggyBank1.SetPosition( 84.1000f, 56.4998f );
	_piggyBank2.SetPosition( 9.9000f, 9.5000f );
	_piggyBank3.SetPosition( 25.0000f, 10.0000f );
	_piggyBankStatic1.SetPosition( 44.0000f, 12.8000f );
	_piggyBankStatic2.SetPosition( 57.0000f, 12.5000f );
	_wallBox1.SetPosition( 39.5000f, 2.5000f );
	_wallBox2.SetPosition( 47.2000f, 42.6000f );
	_fragileBoxStatic2.SetPosition( 17.1000f, 28.1000f );
	_wallBox3.SetPosition( 50.0000f, 24.3000f );
	_piggyBank4.SetPosition( 87.1000f, 38.3998f );

	ConstFinal();
}
//--------------------------------------------------------------------------
