#ifndef __LEVELMARZENA_H__
#define __LEVELMARZENA_H__

#include <Box2D/Box2D.h>
#include "Levels/Level.h"
#include "Blocks/AllBlocks.h"

//------------------------------------------------------------------
class LevelMarzena_Etap5 : public Level
{
public:
	LevelMarzena_Etap5( GameTypes::LevelEnum levelEnum, bool viewMode );

private:
	puCoin< 40 >	_coin1;
	puFloor2	_floor21;
	puFragileBox<96,192,eBlockScaled>	_fragileBox1;
	puFragileBox<96,192,eBlockScaled>	_fragileBox2;
	puFragileBox<96,192,eBlockScaled>	_fragileBox3;
	puFragileBox<96,192,eBlockScaled>	_fragileBox4;
	puFragileBox<96,192,eBlockScaled>	_fragileBox5;
	puFragileBox<96,192,eBlockScaled>	_fragileBox6;
	puFragileBox<96,192,eBlockScaled>	_fragileBox7;
	puFragileBoxStatic<96,192,eBlockScaled>	_fragileBoxStatic1;
	puPiggyBank< 60 >	_piggyBank1;
	puWoodBox<240,15,eBlockScaled>	_woodBox1;
	puWoodBox<240,15,eBlockScaled>	_woodBox2;
	puWoodBox<240,15,eBlockScaled>	_woodBox3;
	puWoodBox<240,15,eBlockScaled>	_woodBox4;
	puWoodBox<240,15,eBlockScaled>	_woodBox5;
};
//------------------------------------------------------------------

#endif
