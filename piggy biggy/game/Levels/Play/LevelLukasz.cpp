#include "LevelLukasz.h"

//--------------------------------------------------------------------------------------
LevelLukasz_1a::LevelLukasz_1a( GameTypes::LevelEnum levelEnum, bool viewMode ) : Level( levelEnum, viewMode )
{
    _levelTag = eLevelTag_Frigle;
    
	// _bomb1
	_bomb1.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_bomb1.GetBody()->GetFixtureList()->SetFriction( 0.3500f );
	_bomb1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bomb1.GetBody()->SetAngularDamping(0.0500f );
	_bomb1.SetBodyType( b2_dynamicBody );

	// _coin1
	_coin1.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin1.GetBody()->GetFixtureList()->SetFriction( 0.3500f );
	_coin1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin1.GetBody()->SetAngularDamping(0.0500f );
	_coin1.GetBody()->SetLinearDamping(0.1000f );
	_coin1.SetBodyType( b2_dynamicBody );

	// _fragileBox1
	_fragileBox1.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox1.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox1.SetBodyType( b2_dynamicBody );

	// _fragileBox2
	_fragileBox2.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox2.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox2.SetBodyType( b2_dynamicBody );

	// _fragileBox3
	_fragileBox3.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox3.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox3.SetBodyType( b2_dynamicBody );

	// _fragileBox4
	_fragileBox4.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox4.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox4.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox4.SetBodyType( b2_dynamicBody );

	// _fragileBox5
	_fragileBox5.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox5.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox5.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox5.SetBodyType( b2_dynamicBody );

	// _fragileBox6
	_fragileBox6.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox6.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox6.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox6.SetBodyType( b2_dynamicBody );

	// _fragileBox7
	_fragileBox7.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox7.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox7.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox7.SetBodyType( b2_dynamicBody );

	// _fragileBox8
	_fragileBox8.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox8.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox8.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox8.SetBodyType( b2_dynamicBody );

	// _fragileBox9
	_fragileBox9.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox9.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox9.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox9.SetBodyType( b2_dynamicBody );

	// _fragileBox10
	_fragileBox10.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox10.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox10.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox10.SetBodyType( b2_dynamicBody );

	// _fragileBox11
	_fragileBox11.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox11.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox11.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox11.SetBodyType( b2_dynamicBody );

	// _fragileBox12
	_fragileBox12.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox12.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox12.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox12.SetBodyType( b2_dynamicBody );

	// _fragileBox13
	_fragileBox13.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox13.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox13.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox13.SetBodyType( b2_dynamicBody );

	// _piggyBank1
	_piggyBank1.GetBody()->GetFixtureList()->SetDensity( 7.5000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_piggyBank1.FlipX();
	_piggyBank1.SetBodyType( b2_dynamicBody );
    _piggyBank1.GetBody()->SetAngularDamping( 0.8f );
    _piggyBank1.GetBody()->SetLinearDamping( 0.8f );

	// _wallBox1
	_wallBox1.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox1.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox1.SetBodyType( b2_staticBody );


	//Set blocks positions
	_bomb1.SetPosition( 46.5001f, 45.6500f );
	_coin1.SetPosition( 68.6992f, 36.1502f );
	_fragileBox1.SetPosition( 49.3001f, 25.9502f );
	_fragileBox2.SetPosition( 58.9998f, 25.9502f );
	_fragileBox3.SetPosition( 78.3999f, 25.9502f );
	_fragileBox4.SetPosition( 68.6993f, 25.9502f );
	_fragileBox5.SetPosition( 39.6001f, 35.6501f );
	_fragileBox6.SetPosition( 49.3001f, 35.6501f );
	_fragileBox7.SetPosition( 58.9998f, 35.6501f );
	_fragileBox8.SetPosition( 49.3001f, 16.2500f );
	_fragileBox9.SetPosition( 39.6001f, 16.2500f );
	_fragileBox10.SetPosition( 58.9998f, 16.2500f );
	_fragileBox11.SetPosition( 78.3999f, 16.2500f );
	_fragileBox12.SetPosition( 68.6993f, 16.2500f );
	_fragileBox13.SetPosition( 39.6001f, 25.9502f );
	_piggyBank1.SetPosition( 11.9000f, 17.4500f );
	_wallBox1.SetPosition( 48.0000f, 9.2501f );

	//Bombs Construction
	_bomb1.SetRange( 16.0000f );
	_bomb1.SetBombImpulse( 6000.0000f );


	ConstFinal();
}
//--------------------------------------------------------------------------------------
LevelLukasz_1b::LevelLukasz_1b( GameTypes::LevelEnum levelEnum, bool viewMode ) : Level( levelEnum, viewMode )
{
    _levelTag = eLevelTag_Frigle;
    
	// _bomb1
	_bomb1.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_bomb1.GetBody()->GetFixtureList()->SetFriction( 0.4500f );
	_bomb1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bomb1.GetBody()->SetAngularDamping(0.0500f );
	_bomb1.SetBodyType( b2_dynamicBody );

	// _coin1
	_coin1.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin1.GetBody()->GetFixtureList()->SetFriction( 0.4500f );
	_coin1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin1.SetBodyType( b2_dynamicBody );

	// _fragileBox1
	_fragileBox1.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox1.GetBody()->GetFixtureList()->SetFriction( 0.4500f );
	_fragileBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox1.SetBodyType( b2_dynamicBody );

	// _fragileBox2
	_fragileBox2.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox2.GetBody()->GetFixtureList()->SetFriction( 0.4500f );
	_fragileBox2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox2.SetBodyType( b2_dynamicBody );

	// _fragileBox3
	_fragileBox3.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox3.GetBody()->GetFixtureList()->SetFriction( 0.4500f );
	_fragileBox3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox3.SetBodyType( b2_dynamicBody );

	// _fragileBox4
	_fragileBox4.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox4.GetBody()->GetFixtureList()->SetFriction( 0.4500f );
	_fragileBox4.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox4.SetBodyType( b2_dynamicBody );

	// _fragileBox5
	_fragileBox5.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox5.GetBody()->GetFixtureList()->SetFriction( 0.4500f );
	_fragileBox5.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox5.SetBodyType( b2_dynamicBody );

	// _fragileBox6
	_fragileBox6.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox6.GetBody()->GetFixtureList()->SetFriction( 0.4500f );
	_fragileBox6.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox6.SetBodyType( b2_dynamicBody );

	// _fragileBox7
	_fragileBox7.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox7.GetBody()->GetFixtureList()->SetFriction( 0.4500f );
	_fragileBox7.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox7.SetBodyType( b2_dynamicBody );

	// _fragileBox8
	_fragileBox8.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox8.GetBody()->GetFixtureList()->SetFriction( 0.4500f );
	_fragileBox8.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox8.SetBodyType( b2_dynamicBody );

	// _fragileBox9
	_fragileBox9.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox9.GetBody()->GetFixtureList()->SetFriction( 0.4500f );
	_fragileBox9.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox9.SetBodyType( b2_dynamicBody );

	// _fragileBox10
	_fragileBox10.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox10.GetBody()->GetFixtureList()->SetFriction( 0.4500f );
	_fragileBox10.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox10.SetBodyType( b2_dynamicBody );

	// _fragileBox11
	_fragileBox11.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox11.GetBody()->GetFixtureList()->SetFriction( 0.4500f );
	_fragileBox11.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox11.SetBodyType( b2_dynamicBody );

	// _fragileBox12
	_fragileBox12.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox12.GetBody()->GetFixtureList()->SetFriction( 0.4500f );
	_fragileBox12.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox12.SetBodyType( b2_dynamicBody );

	// _fragileBox13
	_fragileBox13.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox13.GetBody()->GetFixtureList()->SetFriction( 0.4500f );
	_fragileBox13.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox13.SetBodyType( b2_dynamicBody );

	// _piggyBank1
	_piggyBank1.GetBody()->GetFixtureList()->SetDensity( 7.5000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_piggyBank1.FlipX();
	_piggyBank1.SetBodyType( b2_dynamicBody );

	// _wallBox1
	_wallBox1.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox1.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox1.SetBodyType( b2_staticBody );

	// _wallBox2
	_wallBox2.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox2.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox2.SetBodyType( b2_staticBody );


	//Set blocks positions
	_bomb1.SetPosition( 53.0501f, 44.1499f );
	_coin1.SetPosition( 74.7500f, 34.9502f );
	_fragileBox1.SetPosition( 65.0500f, 15.2500f );
	_fragileBox2.SetPosition( 84.4505f, 15.2500f );
	_fragileBox3.SetPosition( 74.7502f, 15.2500f );
	_fragileBox4.SetPosition( 45.6500f, 24.8502f );
	_fragileBox5.SetPosition( 55.3500f, 24.8502f );
	_fragileBox6.SetPosition( 65.0500f, 24.8502f );
	_fragileBox7.SetPosition( 84.4505f, 24.8502f );
	_fragileBox8.SetPosition( 74.7502f, 24.8502f );
	_fragileBox9.SetPosition( 45.6500f, 34.4501f );
	_fragileBox10.SetPosition( 55.3500f, 34.4501f );
	_fragileBox11.SetPosition( 65.0500f, 34.4501f );
	_fragileBox12.SetPosition( 55.3500f, 15.2500f );
	_fragileBox13.SetPosition( 45.6500f, 15.2500f );
	_piggyBank1.SetPosition( 12.7500f, 18.7000f );
	_wallBox1.SetPosition( 64.9500f, 9.2500f );
	_wallBox2.SetPosition( 12.7500f, 9.2500f );

	//Bombs Construction
	_bomb1.SetRange( 16.0000f );
	_bomb1.SetBombImpulse( 6400.0000f );

	ConstFinal();
}
//------------------------------------------------------------------------------------
LevelLukasz_2::LevelLukasz_2( GameTypes::LevelEnum levelEnum, bool viewMode ) : Level( levelEnum, viewMode )
{
	_coinCount = 2;
    _levelTag = eLevelTag_Cop;
    
	// _fragileBoxStatic1
	_fragileBoxStatic1.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBoxStatic1.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBoxStatic1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBoxStatic1.SetBodyType( b2_staticBody );

	// _fragileBoxStatic2
	_fragileBoxStatic2.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBoxStatic2.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBoxStatic2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBoxStatic2.SetBodyType( b2_staticBody );

	// _coin1
	_coin1.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin1.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin1.SetBodyType( b2_dynamicBody );

	// _coin2
	_coin2.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin2.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin2.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin2.SetBodyType( b2_dynamicBody );

	// _bomb1
	_bomb1.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_bomb1.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_bomb1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bomb1.SetBodyType( b2_dynamicBody );

	// _piggyBankStatic1
	_piggyBankStatic1.GetBody()->GetFixtureList()->SetDensity( 7.5000f );
	_piggyBankStatic1.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_piggyBankStatic1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_piggyBankStatic1.SetBodyType( b2_staticBody );

	// _fragileBoxStatic3
	_fragileBoxStatic3.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBoxStatic3.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBoxStatic3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBoxStatic3.SetBodyType( b2_staticBody );

	// _fragileBoxStatic4
	_fragileBoxStatic4.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBoxStatic4.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBoxStatic4.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBoxStatic4.SetBodyType( b2_staticBody );

	// _wallBox1
	_wallBox1.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox1.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox1.SetBodyType( b2_staticBody );

	// _wallBox2
	_wallBox2.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox2.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox2.SetBodyType( b2_staticBody );

	// _wallBox3
	_wallBox3.SetRotation( -15.0000f );
	_wallBox3.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox3.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox3.SetBodyType( b2_staticBody );

	// _wallBox4
	_wallBox4.SetRotation( 15.0000f );
	_wallBox4.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox4.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox4.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox4.SetBodyType( b2_staticBody );

	// _cop1
	_cop1.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_cop1.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_cop1.GetBody()->GetFixtureList()->SetRestitution( 0.3500f );
	_cop1.FlipX();
	_cop1.SetBodyType( b2_dynamicBody );

	// _thief1
	_thief1.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_thief1.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_thief1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_thief1.FlipX();
	_thief1.SetBodyType( b2_dynamicBody );

	// _thief2
	_thief2.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_thief2.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_thief2.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_thief2.SetBodyType( b2_dynamicBody );


	//Set blocks positions
	_fragileBoxStatic1.SetPosition( 24.7000f, 44.9500f );
	_fragileBoxStatic2.SetPosition( 90.6004f, 44.9500f );
	_coin1.SetPosition( 4.4000f, 55.2501f );
	_coin2.SetPosition( 81.3503f, 55.2501f );
	_bomb1.SetPosition( 90.6002f, 55.6507f );
	_piggyBankStatic1.SetPosition( 48.0000f, 10.2000f );
	_fragileBoxStatic3.SetPosition( 10.0000f, 44.9500f );
	_fragileBoxStatic4.SetPosition( 75.9500f, 44.9500f );
	_wallBox1.SetPosition( 59.5000f, 6.0000f );
	_wallBox2.SetPosition( 36.5001f, 6.0000f );
	_wallBox3.SetPosition( 18.0000f, 19.9500f );
	_wallBox4.SetPosition( 78.0000f, 19.9500f );
	_cop1.SetPosition( 24.7000f, 56.2500f );
	_thief1.SetPosition( 14.3000f, 56.2500f );
	_thief2.SetPosition( 71.4501f, 56.2500f );

	//Bombs Construction
	_bomb1.SetRange( 16.0000f );
	_bomb1.SetBombImpulse( 5000.0000f );

	ConstFinal();
}
//------------------------------------------------------------------------------------
