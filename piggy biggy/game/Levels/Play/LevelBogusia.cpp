#include "LevelBogusia.h"

//--------------------------------------------------------------------------
LevelBogusia_1::LevelBogusia_1( GameTypes::LevelEnum levelEnum, bool viewMode ) : Level( levelEnum, viewMode )
{
    _levelTag = eLevelTag_Button;
    
	// _button1
	_button1.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_button1.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_button1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_button1.SetBodyType( b2_dynamicBody );

	// _coin1
	_coin1.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin1.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin1.SetBodyType( b2_dynamicBody );

	// _fragileBox1
	_fragileBox1.SetRotation( 90.0000f );
	_fragileBox1.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox1.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox1.SetBodyType( b2_dynamicBody );

	// _fragileBox2
	_fragileBox2.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox2.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox2.SetBodyType( b2_dynamicBody );

	// _fragileBoxStatic1
	_fragileBoxStatic1.SetRotation( -42.8000f );
	_fragileBoxStatic1.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBoxStatic1.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBoxStatic1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBoxStatic1.SetBodyType( b2_staticBody );

	// _piggyBank1
	_piggyBank1.GetBody()->GetFixtureList()->SetDensity( 7.5000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_piggyBank1.FlipX();
	_piggyBank1.SetBodyType( b2_dynamicBody );

	// _wallBox1
	_wallBox1.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox1.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox1.SetBodyType( b2_staticBody );

	// _wallBox2
	_wallBox2.SetRotation( 111.1765f );
	_wallBox2.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox2.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox2.SetBodyType( b2_staticBody );

	// _wallBox3
	_wallBox3.SetRotation( 121.7647f );
	_wallBox3.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox3.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox3.SetBodyType( b2_staticBody );

	// _wallBox4
	_wallBox4.SetRotation( 132.3529f );
	_wallBox4.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox4.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox4.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox4.SetBodyType( b2_staticBody );

	// _wallBox5
	_wallBox5.SetRotation( 142.9412f );
	_wallBox5.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox5.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox5.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox5.SetBodyType( b2_staticBody );

	// _wallBox6
	_wallBox6.SetRotation( 153.5294f );
	_wallBox6.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox6.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox6.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox6.SetBodyType( b2_staticBody );

	// _wallBox7
	_wallBox7.SetRotation( 164.1176f );
	_wallBox7.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox7.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox7.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox7.SetBodyType( b2_staticBody );

	// _wallBox8
	_wallBox8.SetRotation( 174.7059f );
	_wallBox8.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox8.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox8.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox8.SetBodyType( b2_staticBody );

	// _wallBox9
	_wallBox9.SetRotation( 179.2941f );
	_wallBox9.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox9.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox9.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox9.SetBodyType( b2_staticBody );


	//Set blocks positions
	_button1.SetPosition( 57.0992f, 41.1499f );
	_coin1.SetPosition( 87.1000f, 20.5000f );
	_fragileBox1.SetPosition( 59.2997f, 25.3498f );
	_fragileBox2.SetPosition( 48.4495f, 20.8499f );
	_fragileBoxStatic1.SetPosition( 18.2500f, 27.9002f );
	_piggyBank1.SetPosition( 12.3499f, 42.7999f );
	_wallBox1.SetPosition( 66.6001f, 13.1000f );
	_wallBox2.SetPosition( 6.4109f, 33.7904f );
	_wallBox3.SetPosition( 9.0430f, 28.5043f );
	_wallBox4.SetPosition( 12.6017f, 23.7917f );
	_wallBox5.SetPosition( 16.9658f, 19.8134f );
	_wallBox6.SetPosition( 21.9864f, 16.7048f );
	_wallBox7.SetPosition( 27.4928f, 14.5716f );
	_wallBox8.SetPosition( 33.2974f, 13.4865f );
	_wallBox9.SetPosition( 39.2026f, 13.1865f );

	ConstFinal();
}
//--------------------------------------------------------------------------
LevelBogusia_Etap3a::LevelBogusia_Etap3a( GameTypes::LevelEnum levelEnum, bool viewMode ) : Level( levelEnum, viewMode )
{
    _levelTag = eLevelTag_Frigle;
    
	// _coin1
	_coin1.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin1.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin1.SetBodyType( b2_dynamicBody );

	// _fragileBox1
	_fragileBox1.SetRotation( 90.0000f );
	_fragileBox1.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox1.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox1.SetBodyType( b2_dynamicBody );

	// _fragileBox2
	_fragileBox2.SetRotation( 90.0000f );
	_fragileBox2.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox2.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox2.SetBodyType( b2_dynamicBody );

	// _fragileBox3
	_fragileBox3.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox3.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox3.SetBodyType( b2_dynamicBody );

	// _fragileBox4
	_fragileBox4.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox4.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox4.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox4.SetBodyType( b2_dynamicBody );

	// _fragileBox5
	_fragileBox5.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox5.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox5.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox5.SetBodyType( b2_dynamicBody );

	// _fragileBoxStatic1
	_fragileBoxStatic1.SetRotation( 90.0000f );
	_fragileBoxStatic1.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBoxStatic1.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBoxStatic1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBoxStatic1.SetBodyType( b2_staticBody );

	// _fragileBoxStatic2
	_fragileBoxStatic2.SetRotation( 90.0000f );
	_fragileBoxStatic2.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBoxStatic2.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBoxStatic2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBoxStatic2.SetBodyType( b2_staticBody );

	// _piggyBank1
	_piggyBank1.GetBody()->GetFixtureList()->SetDensity( 7.5000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_piggyBank1.SetBodyType( b2_dynamicBody );

	// _wallBox1
	_wallBox1.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox1.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox1.SetBodyType( b2_staticBody );

	// _woodBox1
	_woodBox1.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox1.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_woodBox1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_woodBox1.SetBodyType( b2_dynamicBody );

	// _woodBox2
	_woodBox2.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox2.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_woodBox2.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_woodBox2.SetBodyType( b2_dynamicBody );


	//Set blocks positions
	_coin1.SetPosition( 19.8499f, 56.9004f );
	_fragileBox1.SetPosition( 12.9499f, 18.8499f );
	_fragileBox2.SetPosition( 80.2502f, 19.3499f );
	_fragileBox3.SetPosition( 26.7499f, 23.6499f );
	_fragileBox4.SetPosition( 26.7499f, 13.9499f );
	_fragileBox5.SetPosition( 80.2502f, 37.1499f );
	_fragileBoxStatic1.SetPosition( 26.7499f, 39.7004f );
	_fragileBoxStatic2.SetPosition( 12.9499f, 39.7004f );
	_piggyBank1.SetPosition( 80.2502f, 49.6504f );
	_wallBox1.SetPosition( 48.0000f, 7.9000f );
	_woodBox1.SetPosition( 19.9499f, 29.3002f );
	_woodBox2.SetPosition( 80.2502f, 30.4502f );

	ConstFinal();
}
//--------------------------------------------------------------------------
LevelBogusia_Etap3b::LevelBogusia_Etap3b( GameTypes::LevelEnum levelEnum, bool viewMode ) : Level( levelEnum, viewMode )
{
	// _fragileBoxStatic1
	_fragileBoxStatic1.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBoxStatic1.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBoxStatic1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBoxStatic1.SetBodyType( b2_staticBody );

	// _piggyBank1
	_piggyBank1.GetBody()->GetFixtureList()->SetDensity( 7.5000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_piggyBank1.SetBodyType( b2_dynamicBody );

	// _coin1
	_coin1.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin1.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin1.SetBodyType( b2_dynamicBody );

	// _fragileBox1
	_fragileBox1.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox1.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox1.SetBodyType( b2_dynamicBody );

	// _fragileBox2
	_fragileBox2.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox2.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox2.SetBodyType( b2_dynamicBody );

	// _woodBox1
	_woodBox1.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox1.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_woodBox1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_woodBox1.SetBodyType( b2_dynamicBody );

	// _fragileBox3
	_fragileBox3.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox3.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox3.SetBodyType( b2_dynamicBody );

	// _fragileBox4
	_fragileBox4.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox4.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox4.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox4.SetBodyType( b2_dynamicBody );

	// _floor21
	_floor21.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_floor21.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_floor21.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_floor21.SetBodyType( b2_staticBody );

	// _woodBox2
	_woodBox2.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox2.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_woodBox2.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_woodBox2.SetBodyType( b2_dynamicBody );

	// _wallBox1
	_wallBox1.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox1.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox1.SetBodyType( b2_staticBody );

	// _fragileBox5
	_fragileBox5.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox5.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox5.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox5.SetBodyType( b2_dynamicBody );

	// _fragileBox6
	_fragileBox6.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox6.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox6.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox6.SetBodyType( b2_dynamicBody );

	// _fragileBoxStatic2
	_fragileBoxStatic2.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBoxStatic2.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBoxStatic2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBoxStatic2.SetBodyType( b2_staticBody );


	//Set blocks positions
	_fragileBoxStatic1.SetPosition( 41.2000f, 14.8000f );
	_piggyBank1.SetPosition( 41.2000f, 21.1500f );
	_coin1.SetPosition( 10.9500f, 22.9000f );
	_fragileBox1.SetPosition( 14.5000f, 8.3000f );
	_fragileBox2.SetPosition( 14.5000f, 3.4500f );
	_woodBox1.SetPosition( 11.0000f, 15.8000f );
	_fragileBox3.SetPosition( 7.4000f, 10.6000f );
	_fragileBox4.SetPosition( 41.2000f, 6.0000f );
	_floor21.SetPosition( 24.0000f, 0.5000f );
	_woodBox2.SetPosition( 41.2000f, 11.5500f );
	_wallBox1.SetPosition( 14.5000f, 18.6500f );
	_fragileBox5.SetPosition( 14.5000f, 13.1500f );
	_fragileBox6.SetPosition( 7.4000f, 3.3000f );
	_fragileBoxStatic2.SetPosition( 7.4000f, 18.6500f );

	ConstFinal();
	AdjustOldScreen();
}
//--------------------------------------------------------------------------
LevelBogusia_Etap7::LevelBogusia_Etap7( GameTypes::LevelEnum levelEnum, bool viewMode ) : Level( levelEnum, viewMode )
{
    _levelTag = eLevelTag_Frigle;
	_coinCount = 2;

	// _bomb1
	_bomb1.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_bomb1.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_bomb1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bomb1.SetBodyType( b2_dynamicBody );

	// _coin1
	_coin1.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin1.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin1.SetBodyType( b2_dynamicBody );

	// _coin2
	_coin2.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin2.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin2.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin2.SetBodyType( b2_dynamicBody );

	// _fragileBoxStatic1
	_fragileBoxStatic1.SetRotation( 90.0000f );
	_fragileBoxStatic1.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBoxStatic1.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBoxStatic1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBoxStatic1.SetBodyType( b2_staticBody );

	// _fragileBoxStatic2
	_fragileBoxStatic2.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBoxStatic2.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBoxStatic2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBoxStatic2.SetBodyType( b2_staticBody );

	// _fragileBoxStatic3
	_fragileBoxStatic3.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBoxStatic3.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBoxStatic3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBoxStatic3.SetBodyType( b2_staticBody );

	// _fragileBoxStatic4
	_fragileBoxStatic4.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBoxStatic4.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBoxStatic4.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBoxStatic4.SetBodyType( b2_staticBody );

	// _fragileBoxStatic5
	_fragileBoxStatic5.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBoxStatic5.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBoxStatic5.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBoxStatic5.SetBodyType( b2_staticBody );

	// _wallBox1
	_wallBox1.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox1.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox1.SetBodyType( b2_staticBody );

	// _woodBox1
	_woodBox1.SetRotation( -27.0000f );
	_woodBox1.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox1.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_woodBox1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_woodBox1.SetBodyType( b2_dynamicBody );

	// _piggyBank1
	_piggyBank1.GetBody()->GetFixtureList()->SetDensity( 7.5000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_piggyBank1.FlipX();
	_piggyBank1.SetBodyType( b2_dynamicBody );


	//Set blocks positions
	_bomb1.SetPosition( 63.2997f, 23.0499f );
	_coin1.SetPosition( 63.1995f, 55.5506f );
	_coin2.SetPosition( 47.9999f, 55.5506f );
	_fragileBoxStatic1.SetPosition( 32.6999f, 18.6499f );
	_fragileBoxStatic2.SetPosition( 63.2997f, 13.9499f );
	_fragileBoxStatic3.SetPosition( 63.2996f, 44.5506f );
	_fragileBoxStatic4.SetPosition( 47.9999f, 44.5506f );
	_fragileBoxStatic5.SetPosition( 32.6996f, 44.5506f );
	_wallBox1.SetPosition( 47.9999f, 7.6500f );
	_woodBox1.SetPosition( 43.7000f, 26.7501f );
	_piggyBank1.SetPosition( 48.0000f, 15.1500f );

	//Bombs Construction
	_bomb1.SetRange( 16.0000f );
	_bomb1.SetBombImpulse( 6000.0000f );

	ConstFinal();
}
//--------------------------------------------------------------------------
