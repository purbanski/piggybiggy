#include "LevelLetsRoll.h"
//--------------------------------------------------------------------------
LevelLetsRoll_1::LevelLetsRoll_1( GameTypes::LevelEnum levelEnum, bool viewMode ) : Level( levelEnum, viewMode )
{
	_coinCount = 2;

	// _coin1
	_coin1.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin1.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin1.SetBodyType( b2_dynamicBody );

	// _coin2
	_coin2.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin2.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin2.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin2.SetBodyType( b2_dynamicBody );

	// _fragileBox1
	_fragileBox1.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox1.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_fragileBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox1.SetBodyType( b2_dynamicBody );

	// _fragileBox2
	_fragileBox2.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox2.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_fragileBox2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox2.SetBodyType( b2_dynamicBody );

	// _fragileBox3
	_fragileBox3.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox3.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_fragileBox3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox3.SetBodyType( b2_dynamicBody );

	// _fragileBox4
	_fragileBox4.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox4.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_fragileBox4.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox4.SetBodyType( b2_dynamicBody );

	// _piggyBank1
	_piggyBank1.GetBody()->GetFixtureList()->SetDensity( 7.5000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_piggyBank1.SetBodyType( b2_dynamicBody );

	// _wallBox1
	_wallBox1.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox1.GetBody()->GetFixtureList()->SetFriction( 0.2200f );
	_wallBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox1.SetBodyType( b2_staticBody );

	// _wallBox2
	_wallBox2.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox2.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox2.SetBodyType( b2_staticBody );

	// _wallBox3
	_wallBox3.SetRotation( 12.0000f );
	_wallBox3.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox3.GetBody()->GetFixtureList()->SetFriction( 0.2200f );
	_wallBox3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox3.SetBodyType( b2_staticBody );

	// _wallBox4
	_wallBox4.SetRotation( 12.0000f );
	_wallBox4.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox4.GetBody()->GetFixtureList()->SetFriction( 0.2200f );
	_wallBox4.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox4.SetBodyType( b2_staticBody );


	//Set blocks positions
	_coin1.SetPosition( 14.6000f, 23.8000f );
	_coin2.SetPosition( 82.3998f, 57.4000f );
	_fragileBox1.SetPosition( 62.5998f, 52.8000f );
	_fragileBox2.SetPosition( 62.5998f, 36.8000f );
	_fragileBox3.SetPosition( 30.6000f, 14.8000f );
	_fragileBox4.SetPosition( 30.6000f, 24.8000f );
	_piggyBank1.SetPosition( 82.3998f, 41.2000f );
	_wallBox1.SetPosition( 14.6000f, 15.8000f );
	_wallBox2.SetPosition( 39.0001f, 6.8000f );
	_wallBox3.SetPosition( 72.5998f, 46.8000f );
	_wallBox4.SetPosition( 72.5998f, 30.8000f );

	// Clues
	/*_levelClues.push_back( Clue::node( 7, "I'm sexy and I roll it :)"));
	_levelClues.push_back( Clue::node( 10, "love this"));
	_levelClues.push_back( Clue::node( 13, "3 333in :)"));*/

	ConstFinal();
}
//--------------------------------------------------------------------------
LevelLetsRoll_2::LevelLetsRoll_2( GameTypes::LevelEnum levelEnum, bool viewMode ) : Level( levelEnum, viewMode )
{
	_coinCount = 2;

	// _coin1
	_coin1.GetBody()->GetFixtureList()->SetDensity( 3.50f );
	_coin1.GetBody()->GetFixtureList()->SetFriction( 0.60f );
	_coin1.GetBody()->GetFixtureList()->SetRestitution( 0.20f );
	_coin1.SetBodyType( b2_dynamicBody );

	// _coin2
	_coin2.GetBody()->GetFixtureList()->SetDensity( 3.50f );
	_coin2.GetBody()->GetFixtureList()->SetFriction( 0.60f );
	_coin2.GetBody()->GetFixtureList()->SetRestitution( 0.20f );
	_coin2.SetBodyType( b2_dynamicBody );

	// _fragileBox1
	_fragileBox1.GetBody()->GetFixtureList()->SetDensity( 9.0f );
	_fragileBox1.GetBody()->GetFixtureList()->SetFriction( 0.40f );
	_fragileBox1.GetBody()->GetFixtureList()->SetRestitution( 0.10f );
	_fragileBox1.SetBodyType( b2_dynamicBody );

	// _fragileBox2
	_fragileBox2.GetBody()->GetFixtureList()->SetDensity( 9.0f );
	_fragileBox2.GetBody()->GetFixtureList()->SetFriction( 0.40f );
	_fragileBox2.GetBody()->GetFixtureList()->SetRestitution( 0.10f );
	_fragileBox2.SetBodyType( b2_dynamicBody );

	// _fragileBox3
	_fragileBox3.GetBody()->GetFixtureList()->SetDensity( 9.0f );
	_fragileBox3.GetBody()->GetFixtureList()->SetFriction( 0.40f );
	_fragileBox3.GetBody()->GetFixtureList()->SetRestitution( 0.10f );
	_fragileBox3.SetBodyType( b2_dynamicBody );

	// _fragileBox4
	_fragileBox4.GetBody()->GetFixtureList()->SetDensity( 9.0f );
	_fragileBox4.GetBody()->GetFixtureList()->SetFriction( 0.40f );
	_fragileBox4.GetBody()->GetFixtureList()->SetRestitution( 0.40f );
	_fragileBox4.SetBodyType( b2_dynamicBody );

	// _piggyBank1
	_piggyBank1.GetBody()->GetFixtureList()->SetDensity( 7.50f );
	_piggyBank1.GetBody()->GetFixtureList()->SetFriction( 0.40f );
	_piggyBank1.GetBody()->GetFixtureList()->SetRestitution( 0.20f );
	_piggyBank1.GetBody()->SetAngularDamping(0.10f );
	_piggyBank1.GetBody()->SetLinearDamping(0.10f );
	_piggyBank1.SetBodyType( b2_dynamicBody );

	// _piggyBank2
	_piggyBank2.GetBody()->GetFixtureList()->SetDensity( 7.50f );
	_piggyBank2.GetBody()->GetFixtureList()->SetFriction( 0.40f );
	_piggyBank2.GetBody()->GetFixtureList()->SetRestitution( 0.20f );
	_piggyBank2.GetBody()->SetAngularDamping(0.10f );
	_piggyBank2.GetBody()->SetLinearDamping(0.10f );
	_piggyBank2.SetBodyType( b2_dynamicBody );

	// _wallBox2
	_wallBox2.SetRotation( 12 );
	_wallBox2.GetBody()->GetFixtureList()->SetDensity( 8.0f );
	_wallBox2.GetBody()->GetFixtureList()->SetFriction( 0.220f );
	_wallBox2.GetBody()->GetFixtureList()->SetRestitution( 0.10f );
	_wallBox2.SetBodyType( b2_staticBody );

	// _wallBox3
	_wallBox3.SetRotation( 12 );
	_wallBox3.GetBody()->GetFixtureList()->SetDensity( 8.0f );
	_wallBox3.GetBody()->GetFixtureList()->SetFriction( 0.220f );
	_wallBox3.GetBody()->GetFixtureList()->SetRestitution( 0.10f );
	_wallBox3.SetBodyType( b2_staticBody );

	// _wallBox4
	_wallBox4.GetBody()->GetFixtureList()->SetDensity( 8.0f );
	_wallBox4.GetBody()->GetFixtureList()->SetFriction( 0.80f );
	_wallBox4.GetBody()->GetFixtureList()->SetRestitution( 0.0f );
	_wallBox4.SetBodyType( b2_staticBody );

	// _wallBox5
	_wallBox5.GetBody()->GetFixtureList()->SetDensity( 8.0f );
	_wallBox5.GetBody()->GetFixtureList()->SetFriction( 0.220f );
	_wallBox5.GetBody()->GetFixtureList()->SetRestitution( 0.10f );
	_wallBox5.SetBodyType( b2_staticBody );


	//Set blocks positions
	_coin1.SetPosition( 9.950f, 11.50f );
	_coin2.SetPosition( 40.40f, 7.550f );
	_fragileBox1.SetPosition( 38.30f, 26.950f );
	_fragileBox2.SetPosition( 38.30f, 18.80f );
	_fragileBox3.SetPosition( 19.0f, 6.250f );
	_fragileBox4.SetPosition( 17.650f, 11.150f );
	_piggyBank1.SetPosition( 45.20f, 28.150f );
	_piggyBank2.SetPosition( 45.90f, 20.80f );
	_wallBox2.SetPosition( 38.0f, 23.0f );
	_wallBox3.SetPosition( 38.0f, 15.0f );
	_wallBox4.SetPosition( 28.60f, 3.0f );
	_wallBox5.SetPosition( 9.70f, 7.50f );

	ConstFinal();
	AdjustOldScreen();

	//_demoList.push_back( DemoStep( 35, DemoStep::eDestroyBlock, &_fragileBox1 ));
	//_demoList.push_back( DemoStep( 65, DemoStep::eDestroyBlock, &_fragileBox2 ));
	//_demoList.push_back( DemoStep( 95, DemoStep::eDestroyBlock, &_fragileBox3 ));
}
//--------------------------------------------------------------------------
