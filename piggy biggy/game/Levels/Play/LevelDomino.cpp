#include "LevelDomino.h"

//--------------------------------------------------------------------------
LevelDomino_1::LevelDomino_1( GameTypes::LevelEnum levelEnum, bool viewMode ) : Level( levelEnum, viewMode )
{
    _levelTag = eLevelTag_Hacker;
    
	// _coin1
	_coin1.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin1.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin1.SetBodyType( b2_dynamicBody );

	// _fragileBox1
	_fragileBox1.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox1.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox1.SetBodyType( b2_dynamicBody );

	// _fragileBox2
	_fragileBox2.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox2.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox2.SetBodyType( b2_dynamicBody );

	// _fragileBox3
	_fragileBox3.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox3.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_fragileBox3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox3.SetBodyType( b2_dynamicBody );

	// _fragileBox4
	_fragileBox4.SetRotation( -0.0000f );
	_fragileBox4.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox4.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_fragileBox4.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox4.SetBodyType( b2_dynamicBody );

	// _fragileBox5
	_fragileBox5.SetRotation( -0.0000f );
	_fragileBox5.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox5.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_fragileBox5.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox5.SetBodyType( b2_dynamicBody );

	// _fragileBox6
	_fragileBox6.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox6.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox6.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox6.SetBodyType( b2_dynamicBody );

	// _fragileBox7
	_fragileBox7.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox7.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox7.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox7.SetBodyType( b2_dynamicBody );

	// _fragileBox8
	_fragileBox8.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox8.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox8.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox8.SetBodyType( b2_dynamicBody );

	// _fragileBoxStatic1
	_fragileBoxStatic1.SetRotation( -45.0000f );
	_fragileBoxStatic1.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBoxStatic1.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBoxStatic1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBoxStatic1.SetBodyType( b2_staticBody );

	// _lScrewed1
	_lScrewed1.SetRotation( -225.0000f );
	_lScrewed1.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_lScrewed1.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_lScrewed1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_lScrewed1.SetBodyType( b2_dynamicBody );

	// _liftPlatform1
	_liftPlatform1.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_liftPlatform1.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_liftPlatform1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_liftPlatform1.SetBodyType( b2_dynamicBody );

	// _piggyBank1
	_piggyBank1.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_piggyBank1.GetBody()->SetAngularDamping(0.4000f );
	_piggyBank1.SetBodyType( b2_dynamicBody );

	// _screw1
	_screw1.SetRotation( -20.4000f );
	_screw1.GetBody()->GetFixtureList()->SetDensity( 0.1000f );
	_screw1.GetBody()->GetFixtureList()->SetFriction( 0.1000f );
	_screw1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	b2Filter filter;
	filter.groupIndex = -1;
	filter.categoryBits = 0;
	filter.maskBits = 65535;
	_screw1.GetBody()->GetFixtureList()->SetFilterData( filter );
	_screw1.SetBodyType( b2_staticBody );

	// _woodBox1
	_woodBox1.SetRotation( 90.0000f );
	_woodBox1.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox1.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_woodBox1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_woodBox1.SetBodyType( b2_dynamicBody );

	// _woodBox2
	_woodBox2.SetRotation( 90.0000f );
	_woodBox2.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox2.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_woodBox2.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_woodBox2.SetBodyType( b2_dynamicBody );

	// _woodBox3
	_woodBox3.SetRotation( 90.0000f );
	_woodBox3.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox3.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_woodBox3.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_woodBox3.SetBodyType( b2_dynamicBody );

	// _woodBox4
	_woodBox4.SetRotation( 90.0000f );
	_woodBox4.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox4.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_woodBox4.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_woodBox4.SetBodyType( b2_dynamicBody );

	// _woodBox5
	_woodBox5.SetRotation( 90.0000f );
	_woodBox5.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox5.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_woodBox5.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_woodBox5.SetBodyType( b2_dynamicBody );

	// _woodBox6
	_woodBox6.SetRotation( 90.0000f );
	_woodBox6.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox6.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_woodBox6.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_woodBox6.SetBodyType( b2_dynamicBody );

	// _woodBox7
	_woodBox7.SetRotation( 90.0000f );
	_woodBox7.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox7.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_woodBox7.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_woodBox7.SetBodyType( b2_dynamicBody );

	// _woodBox8
	_woodBox8.SetRotation( 90.0000f );
	_woodBox8.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox8.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_woodBox8.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_woodBox8.SetBodyType( b2_dynamicBody );

	// _woodBox9
	_woodBox9.SetRotation( 90.0000f );
	_woodBox9.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox9.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_woodBox9.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_woodBox9.SetBodyType( b2_dynamicBody );

	// _thiefStatic1
	_thiefStatic1.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_thiefStatic1.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_thiefStatic1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_thiefStatic1.SetBodyType( b2_staticBody );

	// _wallBox1
	_wallBox1.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox1.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox1.SetBodyType( b2_staticBody );

	// _woodBox10
	_woodBox10.SetRotation( 180.0000f );
	_woodBox10.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox10.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_woodBox10.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_woodBox10.SetBodyType( b2_dynamicBody );

	// _wallBox2
	_wallBox2.SetRotation( 28.0000f );
	_wallBox2.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox2.GetBody()->GetFixtureList()->SetFriction( 0.3500f );
	_wallBox2.GetBody()->GetFixtureList()->SetRestitution( 0.0500f );
	_wallBox2.SetBodyType( b2_staticBody );

	// _wallBox3
	_wallBox3.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox3.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox3.SetBodyType( b2_staticBody );

	// _wallBox4
	_wallBox4.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox4.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox4.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox4.SetBodyType( b2_staticBody );

	// _woodBox11
	_woodBox11.SetRotation( 180.0000f );
	_woodBox11.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox11.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_woodBox11.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_woodBox11.SetBodyType( b2_dynamicBody );

	// _wallBox5
	_wallBox5.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox5.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox5.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox5.SetBodyType( b2_staticBody );

	// _wallBox6
	_wallBox6.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox6.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox6.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox6.SetBodyType( b2_staticBody );

	// _woodBox12
	_woodBox12.SetRotation( 100.0000f );
	_woodBox12.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox12.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_woodBox12.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_woodBox12.SetBodyType( b2_dynamicBody );

	// _woodBox13
	_woodBox13.SetRotation( 90.0000f );
	_woodBox13.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox13.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_woodBox13.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_woodBox13.SetBodyType( b2_dynamicBody );

	// _woodBox14
	_woodBox14.SetRotation( 90.0000f );
	_woodBox14.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox14.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_woodBox14.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_woodBox14.SetBodyType( b2_dynamicBody );

	// _woodBox15
	_woodBox15.SetRotation( 90.0000f );
	_woodBox15.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox15.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_woodBox15.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_woodBox15.SetBodyType( b2_dynamicBody );

	// _woodBox16
	_woodBox16.SetRotation( 90.0000f );
	_woodBox16.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox16.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_woodBox16.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_woodBox16.SetBodyType( b2_dynamicBody );

	// _woodBox17
	_woodBox17.SetRotation( 90.0000f );
	_woodBox17.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox17.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_woodBox17.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_woodBox17.SetBodyType( b2_dynamicBody );

	// _woodBox18
	_woodBox18.SetRotation( 90.0000f );
	_woodBox18.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox18.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_woodBox18.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_woodBox18.SetBodyType( b2_dynamicBody );

	// _woodBox19
	_woodBox19.SetRotation( 90.0000f );
	_woodBox19.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox19.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_woodBox19.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_woodBox19.SetBodyType( b2_dynamicBody );

	// _woodBox20
	_woodBox20.SetRotation( 90.0000f );
	_woodBox20.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox20.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_woodBox20.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_woodBox20.SetBodyType( b2_dynamicBody );

	// _woodBox21
	_woodBox21.SetRotation( 90.0000f );
	_woodBox21.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox21.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_woodBox21.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_woodBox21.SetBodyType( b2_dynamicBody );

	// _woodBox22
	_woodBox22.SetRotation( 90.0000f );
	_woodBox22.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox22.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_woodBox22.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_woodBox22.SetBodyType( b2_dynamicBody );

	// _woodBox23
	_woodBox23.SetRotation( 90.0000f );
	_woodBox23.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox23.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_woodBox23.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_woodBox23.SetBodyType( b2_dynamicBody );

	// _fragileBox9
	_fragileBox9.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox9.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_fragileBox9.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox9.SetBodyType( b2_dynamicBody );

	// _woodBox24
	_woodBox24.SetRotation( 90.0000f );
	_woodBox24.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox24.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_woodBox24.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_woodBox24.SetBodyType( b2_dynamicBody );

	// _woodBox25
	_woodBox25.SetRotation( 90.0000f );
	_woodBox25.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox25.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_woodBox25.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_woodBox25.SetBodyType( b2_dynamicBody );

	// _woodBox26
	_woodBox26.SetRotation( 90.0000f );
	_woodBox26.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox26.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_woodBox26.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_woodBox26.SetBodyType( b2_dynamicBody );

	// _fragileBox10
	_fragileBox10.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox10.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox10.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox10.SetBodyType( b2_dynamicBody );

	// _woodBox27
	_woodBox27.SetRotation( 90.0000f );
	_woodBox27.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox27.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_woodBox27.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_woodBox27.SetBodyType( b2_dynamicBody );

	// _fragileBox11
	_fragileBox11.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox11.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox11.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox11.SetBodyType( b2_dynamicBody );

	// _woodBox28
	_woodBox28.SetRotation( 90.0000f );
	_woodBox28.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox28.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_woodBox28.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_woodBox28.SetBodyType( b2_dynamicBody );

	// _woodBox29
	_woodBox29.SetRotation( 90.0000f );
	_woodBox29.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox29.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_woodBox29.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_woodBox29.SetBodyType( b2_dynamicBody );

	// _woodBox30
	_woodBox30.SetRotation( 90.0000f );
	_woodBox30.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox30.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_woodBox30.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_woodBox30.SetBodyType( b2_dynamicBody );

	// _woodBox31
	_woodBox31.SetRotation( 90.0000f );
	_woodBox31.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox31.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_woodBox31.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_woodBox31.SetBodyType( b2_dynamicBody );

	// _woodBox32
	_woodBox32.SetRotation( 90.0000f );
	_woodBox32.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox32.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_woodBox32.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_woodBox32.SetBodyType( b2_dynamicBody );

	// _woodBox33
	_woodBox33.SetRotation( 90.0000f );
	_woodBox33.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox33.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_woodBox33.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_woodBox33.SetBodyType( b2_dynamicBody );

	// _woodBox34
	_woodBox34.SetRotation( 90.0000f );
	_woodBox34.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox34.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_woodBox34.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_woodBox34.SetBodyType( b2_dynamicBody );

	// _woodBox35
	_woodBox35.SetRotation( 90.0000f );
	_woodBox35.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox35.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_woodBox35.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_woodBox35.SetBodyType( b2_dynamicBody );

	// _woodBox36
	_woodBox36.SetRotation( 90.0000f );
	_woodBox36.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox36.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_woodBox36.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_woodBox36.SetBodyType( b2_dynamicBody );

	// _woodBox37
	_woodBox37.SetRotation( 90.0000f );
	_woodBox37.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox37.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_woodBox37.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_woodBox37.SetBodyType( b2_dynamicBody );

	// _woodBox38
	_woodBox38.SetRotation( 90.0000f );
	_woodBox38.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox38.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_woodBox38.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_woodBox38.SetBodyType( b2_dynamicBody );

	// _woodBox39
	_woodBox39.SetRotation( 90.0000f );
	_woodBox39.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox39.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_woodBox39.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_woodBox39.SetBodyType( b2_dynamicBody );

	// _woodBox40
	_woodBox40.SetRotation( 90.0000f );
	_woodBox40.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox40.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_woodBox40.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_woodBox40.SetBodyType( b2_dynamicBody );

	// _woodBox41
	_woodBox41.SetRotation( 90.0000f );
	_woodBox41.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox41.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_woodBox41.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_woodBox41.SetBodyType( b2_dynamicBody );

	// _woodBox42
	_woodBox42.SetRotation( 90.0000f );
	_woodBox42.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox42.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_woodBox42.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_woodBox42.SetBodyType( b2_dynamicBody );

	// _woodBox43
	_woodBox43.SetRotation( 90.0000f );
	_woodBox43.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox43.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_woodBox43.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_woodBox43.SetBodyType( b2_dynamicBody );


	//Set blocks positions
	_coin1.SetPosition( 66.7008f, 50.5500f );
	_fragileBox1.SetPosition( 11.0619f, 49.9703f );
	_fragileBox2.SetPosition( 13.6480f, 57.3103f );
	_fragileBox3.SetPosition( 16.1041f, 10.4300f );
	_fragileBox4.SetPosition( 69.7810f, 33.3102f );
	_fragileBox5.SetPosition( 70.9010f, 26.1700f );
	_fragileBox6.SetPosition( 13.3019f, 17.7600f );
	_fragileBox7.SetPosition( 8.6100f, 10.4300f );
	_fragileBox8.SetPosition( 87.5014f, 56.9499f );
	_fragileBoxStatic1.SetPosition( 47.1001f, 32.8501f );
	_lScrewed1.SetPosition( 50.0401f, 36.5901f );
	_liftPlatform1.SetPosition( 86.4514f, 52.0498f );
	_piggyBank1.SetPosition( 51.0003f, 50.6501f );
	_screw1.SetPosition( 76.2374f, 30.7018f );
	_woodBox1.SetPosition( 74.3009f, 48.9900f );
	_woodBox2.SetPosition( -32.7999f, 63.2403f );
	_woodBox3.SetPosition( -50.7999f, 56.2403f );
	_woodBox4.SetPosition( -46.7999f, 56.2403f );
	_woodBox5.SetPosition( 77.6672f, 48.8900f );
	_woodBox6.SetPosition( -54.7999f, 56.2403f );
	_woodBox7.SetPosition( -67.7998f, 56.2403f );
	_woodBox8.SetPosition( -63.7998f, 56.2403f );
	_woodBox9.SetPosition( -59.7999f, 56.2403f );
	_thiefStatic1.SetPosition( 57.9001f, 16.9000f );
	_wallBox1.SetPosition( 41.9002f, 5.5500f );
	_woodBox10.SetPosition( 14.9167f, 61.6403f );
	_wallBox2.SetPosition( 28.3500f, 29.8000f );
	_wallBox3.SetPosition( 72.1006f, 45.1499f );
	_wallBox4.SetPosition( 72.1006f, 21.3000f );
	_woodBox11.SetPosition( 77.6672f, 51.9401f );
	_wallBox5.SetPosition( 18.1000f, 45.1500f );
	_wallBox6.SetPosition( 43.0000f, 45.1500f );
	_woodBox12.SetPosition( 75.1250f, 36.6999f );
	_woodBox13.SetPosition( -24.9000f, 11.6000f );
	_woodBox14.SetPosition( -19.7000f, 11.6000f );
	_woodBox15.SetPosition( -14.5000f, 11.6000f );
	_woodBox16.SetPosition( -35.7000f, 14.4000f );
	_woodBox17.SetPosition( -30.5000f, 14.4000f );
	_woodBox18.SetPosition( -25.3000f, 14.4000f );
	_woodBox19.SetPosition( -20.1000f, 14.4000f );
	_woodBox20.SetPosition( -14.9000f, 14.4000f );
	_woodBox21.SetPosition( -9.7000f, 14.4000f );
	_woodBox22.SetPosition( 57.3000f, 9.0000f );
	_woodBox23.SetPosition( 52.1000f, 9.0000f );
	_fragileBox9.SetPosition( 29.7041f, -28.1700f );
	_woodBox24.SetPosition( 46.9000f, 9.0000f );
	_woodBox25.SetPosition( 31.3000f, 9.0000f );
	_woodBox26.SetPosition( 36.5000f, 9.0000f );
	_fragileBox10.SetPosition( 25.2100f, -29.3700f );
	_woodBox27.SetPosition( 41.7000f, 9.0000f );
	_fragileBox11.SetPosition( 24.1019f, -27.4400f );
	_woodBox28.SetPosition( 26.1000f, 9.0000f );
	_woodBox29.SetPosition( 62.5000f, 9.0000f );
	_woodBox30.SetPosition( 72.9001f, 9.0000f );
	_woodBox31.SetPosition( 67.7001f, 9.0000f );
	_woodBox32.SetPosition( 78.1001f, 9.0000f );
	_woodBox33.SetPosition( 35.3999f, 48.7000f );
	_woodBox34.SetPosition( 40.0999f, 48.7000f );
	_woodBox35.SetPosition( 44.6999f, 48.7000f );
	_woodBox36.SetPosition( -19.7999f, 48.7001f );
	_woodBox37.SetPosition( -25.0000f, 48.7001f );
	_woodBox38.SetPosition( -9.3990f, 48.7001f );
	_woodBox39.SetPosition( -14.5994f, 48.7001f );
	_woodBox40.SetPosition( -49.6001f, 44.7001f );
	_woodBox41.SetPosition( 30.4499f, 48.7000f );
	_woodBox42.SetPosition( 20.9499f, 48.7000f );
	_woodBox43.SetPosition( 25.7499f, 48.7000f );

	ConstFinal();
	CreateJoints();
}
//--------------------------------------------------------------------------
void LevelDomino_1::CreateJoints()
{
	b2PrismaticJointDef jointPrisDef1;
	jointPrisDef1.collideConnected = false;
	jointPrisDef1.Initialize( _wallBox1.GetBody(), _liftPlatform1.GetBody(), _wallBox1.GetPosition() , b2Vec2( 0.0f, 1.0f ) );
	_jointPris1 = (b2PrismaticJoint *) _world.CreateJoint( &jointPrisDef1 );

	_jointPris1->SetLimits( -46.4996f, 6.4000f );
	_jointPris1->EnableLimit( true );
	_jointPris1->EnableMotor( true );
	_jointPris1->SetMotorSpeed( 6.8000f );
	_jointPris1->SetMaxMotorForce( 5050.0000f );

	b2RevoluteJointDef jointRevDef1;
	jointRevDef1.collideConnected = false;
	jointRevDef1.Initialize( _woodBox12.GetBody(), _screw1.GetBody(), _screw1.GetPosition() );
	_jointRev1 = (b2RevoluteJoint *) _world.CreateJoint( &jointRevDef1 );

	_jointRev1->EnableMotor( true );
	_jointRev1->SetMotorSpeed( 0.0000f );
	_jointRev1->SetMaxMotorTorque( 0.0000f );
}
//--------------------------------------------------------------------------

