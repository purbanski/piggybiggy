#include "LevelDemo.h"

//--------------------------------------------------------------------------
LevelDemo_0::LevelDemo_0( GameTypes::LevelEnum levelEnum, bool viewMode ) : LevelDemo( levelEnum, viewMode )
{
	// _coin1
	_coin1.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin1.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	//_coin1.SetBodyType( b2_staticBody );

	// _piggyBank1
	_piggyBank1.GetBody()->GetFixtureList()->SetDensity( 7.5000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	//_piggyBank1.SetBodyType( b2_staticBody );

	// _wallBox1
	_wallBox1.SetRotation( 192.0860f );
	_wallBox1.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox1.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox1.SetBodyType( b2_staticBody );

	// _wallBox2
	_wallBox2.SetRotation( 158.1430f );
	_wallBox2.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox2.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox2.SetBodyType( b2_staticBody );

	// _wallBox3
	_wallBox3.SetRotation( 180.5143f );
	_wallBox3.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox3.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox3.SetBodyType( b2_staticBody );

	// _wallBox4
	_wallBox4.SetRotation( 123.4800f );
	_wallBox4.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox4.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox4.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox4.SetBodyType( b2_staticBody );

	// _wallBox5
	_wallBox5.SetRotation( 203.1400f );
	_wallBox5.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox5.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox5.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox5.SetBodyType( b2_staticBody );

	// _wallBox6
	_wallBox6.SetRotation( 169.2001f );
	_wallBox6.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox6.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox6.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox6.SetBodyType( b2_staticBody );

	// _wallBox7
	_wallBox7.SetRotation( 237.0860f );
	_wallBox7.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox7.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox7.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox7.SetBodyType( b2_staticBody );

	// _wallBox8
	_wallBox8.SetRotation( 191.8290f );
	_wallBox8.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox8.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox8.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox8.SetBodyType( b2_staticBody );

	// _wallBox9
	_wallBox9.SetRotation( 225.7720f );
	_wallBox9.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox9.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox9.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox9.SetBodyType( b2_staticBody );

	// _wallBox10
	_wallBox10.SetRotation( 146.8287f );
	_wallBox10.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox10.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox10.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox10.SetBodyType( b2_staticBody );

	// _wallBox11
	_wallBox11.SetRotation( 124.2000f );
	_wallBox11.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox11.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox11.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox11.SetBodyType( b2_staticBody );

	// _wallBox12
	_wallBox12.SetRotation( 169.4573f );
	_wallBox12.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox12.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox12.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox12.SetBodyType( b2_staticBody );

	// _wallBox13
	_wallBox13.SetRotation( 180.7700f );
	_wallBox13.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox13.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox13.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox13.SetBodyType( b2_staticBody );

	// _wallBox14
	_wallBox14.SetRotation( 135.5143f );
	_wallBox14.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox14.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox14.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox14.SetBodyType( b2_staticBody );

	// _wallBox15
	_wallBox15.SetRotation( 156.9600f );
	_wallBox15.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox15.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox15.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox15.SetBodyType( b2_staticBody );

	// _wallBox16
	_wallBox16.SetRotation( 190.4400f );
	_wallBox16.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox16.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox16.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox16.SetBodyType( b2_staticBody );

	// _wallBox17
	_wallBox17.SetRotation( 223.9200f );
	_wallBox17.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox17.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox17.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox17.SetBodyType( b2_staticBody );

	// _wallBox18
	_wallBox18.SetRotation( 257.4000f );
	_wallBox18.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox18.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox18.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox18.SetBodyType( b2_staticBody );

	// _wallBox19
	_wallBox19.SetRotation( 290.8800f );
	_wallBox19.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox19.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox19.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox19.SetBodyType( b2_staticBody );

	// _wallBox20
	_wallBox20.SetRotation( 324.3600f );
	_wallBox20.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox20.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox20.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox20.SetBodyType( b2_staticBody );

	// _wallBox21
	_wallBox21.SetRotation( 214.4570f );
	_wallBox21.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox21.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox21.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox21.SetBodyType( b2_staticBody );

	// _wallBox22
	_wallBox22.SetRotation( 124.2000f );
	_wallBox22.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox22.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox22.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox22.SetBodyType( b2_staticBody );

	// _wallBox23
	_wallBox23.SetRotation( 192.0860f );
	_wallBox23.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox23.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox23.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox23.SetBodyType( b2_staticBody );

	// _wallBox24
	_wallBox24.SetRotation( 135.5100f );
	_wallBox24.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox24.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox24.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox24.SetBodyType( b2_staticBody );

	// _wallBox25
	_wallBox25.SetRotation( 146.8200f );
	_wallBox25.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox25.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox25.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox25.SetBodyType( b2_staticBody );

	// _wallBox26
	_wallBox26.SetRotation( 158.1430f );
	_wallBox26.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox26.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox26.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox26.SetBodyType( b2_staticBody );

	// _wallBox27
	_wallBox27.SetRotation( 169.4573f );
	_wallBox27.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox27.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox27.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox27.SetBodyType( b2_staticBody );

	// _wallBox28
	_wallBox28.SetRotation( 180.7716f );
	_wallBox28.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox28.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox28.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox28.SetBodyType( b2_staticBody );


	//Set blocks positions
	_coin1.SetPosition( 10.1500f, 78.4000f );
	_piggyBank1.SetPosition( 84.6500f, 73.1000f );
	_wallBox1.SetPosition( 37.9700f, 46.7000f );
	_wallBox2.SetPosition( 20.9100f, 17.2600f );
	_wallBox3.SetPosition( 63.9899f, 29.7900f );
	_wallBox4.SetPosition( 49.4500f, 7.6360f );
	_wallBox5.SetPosition( 75.3100f, 32.1500f );
	_wallBox6.SetPosition( 58.1900f, 30.3100f );
	_wallBox7.SetPosition( 88.4900f, 43.2500f );
	_wallBox8.SetPosition( 69.7700f, 30.4100f );
	_wallBox9.SetPosition( 84.8700f, 38.7100f );
	_wallBox10.SetPosition( 15.7440f, 19.9480f );
	_wallBox11.SetPosition( 7.4860f, 28.0600f );
	_wallBox12.SetPosition( 26.4900f, 15.6380f );
	_wallBox13.SetPosition( 32.2900f, 15.1420f );
	_wallBox14.SetPosition( 11.2120f, 23.6000f );
	_wallBox15.SetPosition( 53.2300f, 4.1180f );
	_wallBox16.SetPosition( 58.3900f, 3.5490f );
	_wallBox17.SetPosition( 62.9898f, 5.9180f );
	_wallBox18.SetPosition( 65.7300f, 10.6360f );
	_wallBox19.SetPosition( 65.3500f, 15.8080f );
	_wallBox20.SetPosition( 62.1898f, 19.9140f );
	_wallBox21.SetPosition( 80.4100f, 34.9700f );
	_wallBox22.SetPosition( 7.3860f, 58.9599f );
	_wallBox23.SetPosition( 38.0700f, 15.7940f );
	_wallBox24.SetPosition( 11.1120f, 54.5000f );
	_wallBox25.SetPosition( 15.6440f, 50.8400f );
	_wallBox26.SetPosition( 20.8100f, 48.1600f );
	_wallBox27.SetPosition( 26.3900f, 46.5400f );
	_wallBox28.SetPosition( 32.1900f, 46.0400f );

//	_sleepers.push_back( Sleeper( &_piggyBank1, 50 ));
//	_sleepers.push_back( Sleeper( &_coin1, 50 ));

	ConstFinal();
	_border.DestroyBody();
};
//--------------------------------------------------------------------------------
LevelDemo_1::LevelDemo_1( GameTypes::LevelEnum levelEnum, bool viewMode ) : LevelDemo( levelEnum, viewMode )
{
	b2Filter filter;
	// _coin1
	_coin1.GetBody()->GetFixtureList()->SetDensity( 3.0000f );
	_coin1.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_coin1.SetBodyType( b2_dynamicBody );

	// _cop1
	_cop1.GetBody()->GetFixtureList()->SetDensity( 4.5000f );
	_cop1.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_cop1.GetBody()->GetFixtureList()->SetRestitution( 0.3000f );
	_cop1.FlipX();
	_cop1.SetBodyType( b2_dynamicBody );

	// _thief1
	_thief1.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_thief1.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_thief1.GetBody()->GetFixtureList()->SetRestitution( 0.3000f );
	_thief1.FlipX();
	_thief1.SetBodyType( b2_dynamicBody );

	// _swingNoFloor1
	_swingNoFloor1.GetBody()->GetFixtureList()->SetDensity( 0.1000f );
	_swingNoFloor1.GetBody()->GetFixtureList()->SetFriction( 0.1000f );
	_swingNoFloor1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_swingNoFloor1.GetBody()->GetFixtureList()->SetFilterData( filter );
	_swingNoFloor1.SetBodyType( b2_staticBody );

	// _wallBox1
	_wallBox1.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox1.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox1.SetBodyType( b2_staticBody );

	// _fragileBox1
	_fragileBox1.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox1.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox1.SetBodyType( b2_dynamicBody );

	// _swingNoFloor2
	_swingNoFloor2.GetBody()->GetFixtureList()->SetDensity( 0.1000f );
	_swingNoFloor2.GetBody()->GetFixtureList()->SetFriction( 0.1000f );
	_swingNoFloor2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_swingNoFloor2.GetBody()->GetFixtureList()->SetFilterData( filter );
	_swingNoFloor2.FlipX();
	_swingNoFloor2.SetBodyType( b2_staticBody );

	// _fragileBox2
	_fragileBox2.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox2.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox2.FlipX();
	_fragileBox2.SetBodyType( b2_dynamicBody );

	// _wallBox2
	_wallBox2.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox2.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox2.FlipX();
	_wallBox2.SetBodyType( b2_staticBody );

	// _wallBox3
	_wallBox3.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox3.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox3.SetBodyType( b2_staticBody );

	// _fragileBox3
	_fragileBox3.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox3.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox3.SetBodyType( b2_dynamicBody );

	// _swingNoFloor3
	_swingNoFloor3.GetBody()->GetFixtureList()->SetDensity( 0.1000f );
	_swingNoFloor3.GetBody()->GetFixtureList()->SetFriction( 0.1000f );
	_swingNoFloor3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_swingNoFloor3.GetBody()->GetFixtureList()->SetFilterData( filter );
	_swingNoFloor3.SetBodyType( b2_staticBody );

	// _swingNoFloor4
	_swingNoFloor4.GetBody()->GetFixtureList()->SetDensity( 0.1000f );
	_swingNoFloor4.GetBody()->GetFixtureList()->SetFriction( 0.1000f );
	_swingNoFloor4.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_swingNoFloor4.GetBody()->GetFixtureList()->SetFilterData( filter );
	_swingNoFloor4.FlipX();
	_swingNoFloor4.SetBodyType( b2_staticBody );

	// _fragileBox4
	_fragileBox4.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox4.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox4.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox4.FlipX();
	_fragileBox4.SetBodyType( b2_dynamicBody );

	// _coin2
	_coin2.GetBody()->GetFixtureList()->SetDensity( 3.0000f );
	_coin2.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_coin2.SetBodyType( b2_dynamicBody );

	// _wallBox4
	_wallBox4.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox4.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox4.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox4.FlipX();
	_wallBox4.SetBodyType( b2_staticBody );

	// _wallBox5
	_wallBox5.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox5.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox5.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox5.SetBodyType( b2_staticBody );

	// _piggyBank1
	_piggyBank1.GetBody()->GetFixtureList()->SetDensity( 7.5000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_piggyBank1.FlipX();
	_piggyBank1.SetBodyType( b2_dynamicBody );


	//Set blocks positions
	_coin1.SetPosition( 63.7000f, 49.1501f );
	_cop1.SetPosition( 30.6003f, 49.9499f );
	_thief1.SetPosition( 30.8000f, 26.1501f );
	_swingNoFloor1.SetPosition( 75.0003f, 58.1002f );
	_wallBox1.SetPosition( 76.6503f, 42.6001f );
	_fragileBox1.SetPosition( 87.4128f, 48.3758f );
	_swingNoFloor2.SetPosition( 21.0001f, 58.1002f );
	_fragileBox2.SetPosition( 8.6003f, 48.3499f );
	_wallBox2.SetPosition( 19.3501f, 42.6001f );
	_wallBox3.SetPosition( 76.6502f, 19.3001f );
	_fragileBox3.SetPosition( 87.4128f, 25.0759f );
	_swingNoFloor3.SetPosition( 75.0002f, 34.8002f );
	_swingNoFloor4.SetPosition( 21.0000f, 34.8003f );
	_fragileBox4.SetPosition( 8.6003f, 25.0499f );
	_coin2.SetPosition( 63.6999f, 25.7001f );
	_wallBox4.SetPosition( 19.3501f, 19.3001f );
	_wallBox5.SetPosition( 47.9998f, 3.4000f );
	_piggyBank1.SetPosition( 30.1000f, 10.2000f );

	ConstFinal();

	filter.groupIndex = 0;
	filter.categoryBits = Config::eFilterCategorySwing;
	filter.maskBits = ~Config::eFilterCategorySwing & 0xffff;

	_wallBox1.GetBody()->GetFixtureList()->SetFilterData( filter );
	_wallBox2.GetBody()->GetFixtureList()->SetFilterData( filter );
	_wallBox3.GetBody()->GetFixtureList()->SetFilterData( filter );
	_wallBox4.GetBody()->GetFixtureList()->SetFilterData( filter );

	_demoList.push_back( DemoStep( 35, DemoStep::eDestroyBlock, &_fragileBox4 ));
	_demoList.push_back( DemoStep( 165, DemoStep::eDestroyBlock, &_fragileBox3 ));
	_demoList.push_back( DemoStep( 295, DemoStep::eDestroyBlock, &_fragileBox2 ));
	_demoList.push_back( DemoStep( 405, DemoStep::eDestroyBlock, &_fragileBox1 ));
}
//--------------------------------------------------------------------------------------
LevelDemo_2::LevelDemo_2( GameTypes::LevelEnum levelEnum, bool viewMode ) : LevelDemo( levelEnum, viewMode )
{
	// _coin1
	_coin1.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin1.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin1.SetBodyType( b2_dynamicBody );

	// _coin2
	_coin2.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin2.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin2.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin2.SetBodyType( b2_dynamicBody );

	// _coin3
	_coin3.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin3.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin3.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin3.SetBodyType( b2_dynamicBody );

	// _coin4
	_coin4.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin4.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin4.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin4.SetBodyType( b2_dynamicBody );

	// _coin5
	_coin5.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin5.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin5.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin5.SetBodyType( b2_dynamicBody );

	// _fragileBox1
	_fragileBox1.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox1.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox1.SetBodyType( b2_dynamicBody );

	// _fragileBox2
	_fragileBox2.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox2.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox2.SetBodyType( b2_dynamicBody );

	// _fragileBox3
	_fragileBox3.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox3.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox3.SetBodyType( b2_dynamicBody );

	// _fragileBox4
	_fragileBox4.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox4.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox4.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox4.SetBodyType( b2_dynamicBody );

	// _fragileBox5
	_fragileBox5.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox5.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox5.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox5.SetBodyType( b2_dynamicBody );

	// _fragileBox6
	_fragileBox6.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox6.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox6.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox6.SetBodyType( b2_dynamicBody );

	// _piggyBank1
	_piggyBank1.GetBody()->GetFixtureList()->SetDensity( 7.5000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_piggyBank1.SetBodyType( b2_dynamicBody );

	// _wallBox1
	_wallBox1.SetRotation( 8.0000f );
	_wallBox1.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox1.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox1.SetBodyType( b2_staticBody );

	// _wallBox2
	_wallBox2.SetRotation( -8.0000f );
	_wallBox2.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox2.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox2.SetBodyType( b2_staticBody );

	// _wallBox3
	_wallBox3.SetRotation( -8.0000f );
	_wallBox3.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox3.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox3.SetBodyType( b2_staticBody );

	// _wallBox4
	_wallBox4.SetRotation( -8.0000f );
	_wallBox4.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox4.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox4.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox4.SetBodyType( b2_staticBody );

	// _wallBox5
	_wallBox5.SetRotation( 8.0000f );
	_wallBox5.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox5.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox5.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox5.SetBodyType( b2_staticBody );

	// _wallBox6
	_wallBox6.SetRotation( 8.0000f );
	_wallBox6.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox6.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox6.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox6.SetBodyType( b2_staticBody );


	//Set blocks positions
	_coin1.SetPosition( 15.2502f, 55.5000f );
	_coin2.SetPosition( 23.5001f, 34.5000f );
	_coin3.SetPosition( 5.5000f, 16.9000f );
	_coin4.SetPosition( 72.4017f, 34.5000f );
	_coin5.SetPosition( 90.5017f, 16.9000f );
	_fragileBox1.SetPosition( 71.5999f, 55.5000f );
	_fragileBox2.SetPosition( 24.4001f, 55.5000f );
	_fragileBox3.SetPosition( 32.9999f, 34.5000f );
	_fragileBox4.SetPosition( 14.9001f, 16.9000f );
	_fragileBox5.SetPosition( 62.9999f, 34.5000f );
	_fragileBox6.SetPosition( 81.0999f, 16.9000f );
	_piggyBank1.SetPosition( 82.4999f, 57.6999f );
	_wallBox1.SetPosition( 75.6999f, 29.1000f );
	_wallBox2.SetPosition( 20.6001f, 9.1000f );
	_wallBox3.SetPosition( 20.6001f, 49.0500f );
	_wallBox4.SetPosition( 20.6001f, 29.1000f );
	_wallBox5.SetPosition( 75.6999f, 49.0500f );
	_wallBox6.SetPosition( 75.6999f, 9.1000f );

	ConstFinal();

	_demoList.push_back( DemoStep( 80, DemoStep::eDestroyBlock, &_fragileBox4 ));
	_demoList.push_back( DemoStep( 170, DemoStep::eDestroyBlock, &_fragileBox1 ));
	_demoList.push_back( DemoStep( 210, DemoStep::eDestroyBlock, &_fragileBox2 ));
	_demoList.push_back( DemoStep( 310, DemoStep::eDestroyBlock, &_fragileBox3 ));
	_demoList.push_back( DemoStep( 410, DemoStep::eDestroyBlock, &_fragileBox5 ));
	_demoList.push_back( DemoStep( 510, DemoStep::eDestroyBlock, &_fragileBox6 ));
}
//--------------------------------------------------------------------------
LevelDemo_3::LevelDemo_3( GameTypes::LevelEnum levelEnum, bool viewMode ) : LevelDemo( levelEnum, viewMode )
{
	// _wall1
	_wall1.SetRotation( 8 );
	_wall1.SetDeltaRotation( 8 );
	_wall1.GetBody()->GetFixtureList()->SetDensity( 8.0f );
	_wall1.GetBody()->GetFixtureList()->SetFriction( 0.8f );
	_wall1.GetBody()->GetFixtureList()->SetRestitution( 0.1f );
	_wall1.SetBodyType( b2_staticBody );

	// _wall2
	_wall2.SetRotation( 8 );
	_wall2.SetDeltaRotation( 8 );
	_wall2.GetBody()->GetFixtureList()->SetDensity( 8.0f );
	_wall2.GetBody()->GetFixtureList()->SetFriction( 0.8f );
	_wall2.GetBody()->GetFixtureList()->SetRestitution( 0.1f );
	_wall2.SetBodyType( b2_staticBody );

	// _wall3
	_wall3.SetRotation( 8 );
	_wall3.SetDeltaRotation( 8 );
	_wall3.GetBody()->GetFixtureList()->SetDensity( 8.0f );
	_wall3.GetBody()->GetFixtureList()->SetFriction( 0.8f );
	_wall3.GetBody()->GetFixtureList()->SetRestitution( 0.1f );
	_wall3.SetBodyType( b2_staticBody );

	// _fragileBox1
	_fragileBox1.GetBody()->GetFixtureList()->SetDensity( 9.0f );
	_fragileBox1.GetBody()->GetFixtureList()->SetFriction( 0.7f );
	_fragileBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1f );
	_fragileBox1.SetBodyType( b2_dynamicBody );

	// _fragileBox2
	_fragileBox2.GetBody()->GetFixtureList()->SetDensity( 9.0f );
	_fragileBox2.GetBody()->GetFixtureList()->SetFriction( 0.7f );
	_fragileBox2.GetBody()->GetFixtureList()->SetRestitution( 0.1f );
	_fragileBox2.SetBodyType( b2_dynamicBody );

	// _fragileBox3
	_fragileBox3.GetBody()->GetFixtureList()->SetDensity( 9.0f );
	_fragileBox3.GetBody()->GetFixtureList()->SetFriction( 0.7f );
	_fragileBox3.GetBody()->GetFixtureList()->SetRestitution( 0.1f );
	_fragileBox3.SetBodyType( b2_dynamicBody );

	// _coin1
	_coin1.GetBody()->GetFixtureList()->SetDensity( 3.5f );
	_coin1.GetBody()->GetFixtureList()->SetFriction( 0.6f );
	_coin1.GetBody()->GetFixtureList()->SetRestitution( 0.2f );
	_coin1.SetBodyType( b2_dynamicBody );

	// _piggyBank1
	_piggyBank1.GetBody()->GetFixtureList()->SetDensity( 7.5f );
	_piggyBank1.GetBody()->GetFixtureList()->SetFriction( 0.4f );
	_piggyBank1.GetBody()->GetFixtureList()->SetRestitution( 0.2f );
	_piggyBank1.SetBodyType( b2_dynamicBody );

	// _coin2
	_coin2.GetBody()->GetFixtureList()->SetDensity( 3.5f );
	_coin2.GetBody()->GetFixtureList()->SetFriction( 0.6f );
	_coin2.GetBody()->GetFixtureList()->SetRestitution( 0.2f );
	_coin2.SetBodyType( b2_dynamicBody );


	_wall1.SetPosition( 31.0f, 15.5f );
	_wall2.SetPosition( 31.0f, 24.5f );
	_wall3.SetPosition( 31.0f, 7.0f );
	_fragileBox1.SetPosition( 34.0f, 20.0f );
	_fragileBox2.SetPosition( 34.0f, 29.0f );
	_fragileBox3.SetPosition( 34.0f, 11.5f );
	_coin1.SetPosition( 39.0f, 11.3f );
	_piggyBank1.SetPosition( 40.0f, 21.2f );
	_coin2.SetPosition( 39.0f, 28.8f );

	ConstFinal();
	AdjustOldScreen();

	_demoList.push_back( DemoStep( 35, DemoStep::eDestroyBlock, &_fragileBox1 ));
	_demoList.push_back( DemoStep( 75, DemoStep::eDestroyBlock, &_fragileBox2 ));
	_demoList.push_back( DemoStep( 105, DemoStep::eDestroyBlock, &_fragileBox3 ));
}
//--------------------------------------------------------------------------
