#ifndef __LEVELSYNCRONIZE_H__
#define __LEVELSYNCRONIZE_H__

#include <Box2D/Box2D.h>
#include "Levels/Level.h"
#include "Blocks/AllBlocks.h"

//--------------------------------------------------------------------------------------------------------
class LevelSynchronize_1 : public Level
{
public:
	LevelSynchronize_1( GameTypes::LevelEnum levelEnum, bool viewMode );

private:
	puCoin< 40 >	_coin1;
	puCoin< 40 >	_coin2;
	puFragileBox<96,96,eBlockScaled>	_fragileBox1;
	puFragileBox<96,96,eBlockScaled>	_fragileBox2;
	puFragileBox<96,96,eBlockScaled>	_fragileBox3;
	puPiggyBank< 60 >	_piggyBank1;
	puWallBox<480,20,eBlockScaled>	_wallBox1;
	puWallBox<480,20,eBlockScaled>	_wallBox2;
	puWallBox<480,20,eBlockScaled>	_wallBox3;
};
//--------------------------------------------------------------------------------------------------------
class LevelSynchronize_2 : public Level
{
public:
	LevelSynchronize_2( GameTypes::LevelEnum levelEnum, bool viewMode );

private:
	puCoin< 40 >	_coin1;
	puCoin< 40 >	_coin2;
	puFragileBox<96,96,eBlockScaled>	_fragileBox1;
	puFragileBox<96,96,eBlockScaled>	_fragileBox2;
	puFragileBox<96,96,eBlockScaled>	_fragileBox3;
	puPiggyBank< 60 >	_piggyBank1;
	puWallBox<480,20,eBlockScaled>	_wallBox1;
	puWallBox<480,20,eBlockScaled>	_wallBox2;
	puWallBox<480,20,eBlockScaled>	_wallBox3;
};
//--------------------------------------------------------------------------------------------------------
class LevelSynchronize_3 : public Level
{
public:
	LevelSynchronize_3( GameTypes::LevelEnum levelEnum, bool viewMode );

private:
	puCoin< 40 >	_coin1;
	puCoin< 40 >	_coin2;
	puCoin< 40 >	_coin3;
	puCoin< 40 >	_coin4;
	puCoin< 40 >	_coin5;
	puFragileBox<96,96,eBlockScaled>	_fragileBox1;
	puFragileBox<96,96,eBlockScaled>	_fragileBox2;
	puFragileBox<96,96,eBlockScaled>	_fragileBox3;
	puFragileBox<96,96,eBlockScaled>	_fragileBox4;
	puFragileBox<96,96,eBlockScaled>	_fragileBox5;
	puFragileBox<96,96,eBlockScaled>	_fragileBox6;
	puPiggyBank< 60 >	_piggyBank1;
	puWallBox<310,20,eBlockScaled>	_wallBox1;
	puWallBox<310,20,eBlockScaled>	_wallBox2;
	puWallBox<310,20,eBlockScaled>	_wallBox3;
	puWallBox<310,20,eBlockScaled>	_wallBox4;
	puWallBox<310,20,eBlockScaled>	_wallBox5;
	puWallBox<310,20,eBlockScaled>	_wallBox6;
};

#endif
