#include "LevelBank.h"
#include "Debug/MyDebug.h"

//--------------------------------------------------------------------------
LevelBankBase::LevelBankBase( GameTypes::LevelEnum levelEnum, bool viewMode, BankLockCombination *lockComb ) : LevelWithCallback( levelEnum, viewMode, 1 ), _bankLock( lockComb )
{
	_coinCount = 5;
    _levelTag = GameTypes::eLevelTag_Bank;
    
	_bank.SetPosition( 70.4f / 2.0f, 35.9f / 2.0f );
	_bankLock.SetStartLocks( 5.0f, -3.3f, 2.7f );
	_bankLock.SetPosition( 11.25f + 0.25f / 2.0f, 18.0f );

	ConstFinal();
	AdjustOldScreen();

	_bankLock.DestroyJoints();
	_bankLock.CreateJoints();

}
//--------------------------------------------------------------------------
void LevelBankBase::CounterCallback()
{
	_bankLock.Tick();
	if ( _bankLock.IsLockOpened() )
		_bank.OpenBankInit();
}
//--------------------------------------------------------------------------
LevelBank_1::LevelBank_1( GameTypes::LevelEnum levelEnum, bool viewMode ) : LevelBankBase( levelEnum, viewMode, &_lockCombination )
{
	float duration;
	duration = 2.0f;

	_msgManager.push_back( eOTMsg_IntroBank1, new LevelMsg_MovingPointingCircle( 
		Config::LevelFirstMsgTick + 65, 
		duration, 
		"", 
		b2Vec2( 0.0f, 0.0f ), 
		b2Vec2( 8.0f, 27.5f ), 
		b2Vec2( 2.0f, -17.5f )));	

	
	_msgManager.push_back( eOTMsg_IntroBank2, new LevelMsg_MovingPointingCircle( 
		Config::LevelFirstMsgTick + 75, 
		duration, 
		"", 
		b2Vec2( 0.0f, 0.0f ), 
		b2Vec2( 39.5f, 16.0f ), 
		b2Vec2( 4.0f, 12.5f )));	

	_msgManager.push_back( eOTMsg_IntroBank3, new LevelMsg_MovingPointingCircle( 
		Config::LevelFirstMsgTick + 85, 
		duration, 
		"", 
		b2Vec2( 0.0f, 0.0f ), 
		b2Vec2( 68.0f, 24.0f ), 
		b2Vec2( 12.0f, 1.0f )));	
}
