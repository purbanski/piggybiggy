#include "LevelMarzena.h"
//-------------------------------------------------------------------------
LevelMarzena_Etap5::LevelMarzena_Etap5( GameTypes::LevelEnum levelEnum, bool viewMode ) : Level( levelEnum, viewMode )
{
	// _fragileBox1
	_fragileBox1.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox1.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox1.SetBodyType( b2_dynamicBody );

	// _fragileBox2
	_fragileBox2.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox2.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox2.SetBodyType( b2_dynamicBody );

	// _fragileBox3
	_fragileBox3.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox3.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox3.SetBodyType( b2_dynamicBody );

	// _fragileBox4
	_fragileBox4.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox4.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox4.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox4.SetBodyType( b2_dynamicBody );

	// _woodBox1
	_woodBox1.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox1.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_woodBox1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_woodBox1.SetBodyType( b2_dynamicBody );

	// _woodBox2
	_woodBox2.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox2.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_woodBox2.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_woodBox2.SetBodyType( b2_dynamicBody );

	// _woodBox3
	_woodBox3.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox3.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_woodBox3.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_woodBox3.SetBodyType( b2_dynamicBody );

	// _fragileBox5
	_fragileBox5.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox5.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox5.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox5.SetBodyType( b2_dynamicBody );

	// _fragileBox6
	_fragileBox6.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox6.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox6.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox6.SetBodyType( b2_dynamicBody );

	// _fragileBox7
	_fragileBox7.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox7.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox7.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox7.SetBodyType( b2_dynamicBody );

	// _woodBox4
	_woodBox4.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox4.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_woodBox4.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_woodBox4.SetBodyType( b2_dynamicBody );

	// _woodBox5
	_woodBox5.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox5.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_woodBox5.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_woodBox5.SetBodyType( b2_dynamicBody );

	// _fragileBoxStatic1
	_fragileBoxStatic1.SetRotation( -90.0000f );
	_fragileBoxStatic1.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBoxStatic1.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBoxStatic1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBoxStatic1.SetBodyType( b2_staticBody );

	// _coin1
	_coin1.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin1.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin1.GetBody()->SetLinearDamping(0.1000f );
	_coin1.SetBodyType( b2_dynamicBody );

	// _piggyBank1
	_piggyBank1.GetBody()->GetFixtureList()->SetDensity( 7.5000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_piggyBank1.SetBodyType( b2_dynamicBody );

	// _floor21
	_floor21.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_floor21.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_floor21.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_floor21.SetBodyType( b2_staticBody );


	//Set blocks positions
	_fragileBox1.SetPosition( 9.4000f, 5.8500f );
	_fragileBox2.SetPosition( 21.3000f, 5.7500f );
	_fragileBox3.SetPosition( 33.9500f, 5.8500f );
	_fragileBox4.SetPosition( 44.7500f, 5.7500f );
	_woodBox1.SetPosition( 39.8500f, 10.9500f );
	_woodBox2.SetPosition( 15.3500f, 11.0000f );
	_woodBox3.SetPosition( 27.6500f, 10.9500f );
	_fragileBox5.SetPosition( 15.2000f, 16.1500f );
	_fragileBox6.SetPosition( 39.6000f, 16.1000f );
	_fragileBox7.SetPosition( 27.0000f, 16.0500f );
	_woodBox4.SetPosition( 19.8500f, 21.4500f );
	_woodBox5.SetPosition( 34.8500f, 21.3500f );
	_fragileBoxStatic1.SetPosition( 26.8500f, 24.5500f );
	_coin1.SetPosition( 27.0000f, 29.3000f );
	_piggyBank1.SetPosition( 3.8000f, 3.8000f );
	_floor21.SetPosition( 24.0000f, 0.5500f );

	ConstFinal();
	AdjustOldScreen();
}
//--------------------------------------------------------------------------
