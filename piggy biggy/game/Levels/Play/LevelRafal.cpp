#include "LevelRafal.h"

//--------------------------------------------------------------------------
LevelPathFinder_1::LevelPathFinder_1( GameTypes::LevelEnum levelEnum, bool viewMode ) : Level( levelEnum, viewMode )
{
	_coinCount = 5;
    _levelTag = eLevelTag_Frigle;
    
	// _circleFragileStatic1
	_circleFragileStatic1.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_circleFragileStatic1.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_circleFragileStatic1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_circleFragileStatic1.SetBodyType( b2_staticBody );

	// _circleFragileStatic2
	_circleFragileStatic2.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_circleFragileStatic2.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_circleFragileStatic2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_circleFragileStatic2.SetBodyType( b2_staticBody );

	// _circleFragileStatic3
	_circleFragileStatic3.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_circleFragileStatic3.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_circleFragileStatic3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_circleFragileStatic3.SetBodyType( b2_staticBody );

	// _circleFragileStatic4
	_circleFragileStatic4.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_circleFragileStatic4.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_circleFragileStatic4.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_circleFragileStatic4.SetBodyType( b2_staticBody );

	// _circleFragileStatic5
	_circleFragileStatic5.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_circleFragileStatic5.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_circleFragileStatic5.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_circleFragileStatic5.SetBodyType( b2_staticBody );

	// _circleFragileStatic6
	_circleFragileStatic6.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_circleFragileStatic6.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_circleFragileStatic6.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_circleFragileStatic6.SetBodyType( b2_staticBody );

	// _circleFragileStatic7
	_circleFragileStatic7.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_circleFragileStatic7.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_circleFragileStatic7.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_circleFragileStatic7.SetBodyType( b2_staticBody );

	// _circleFragileStatic8
	_circleFragileStatic8.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_circleFragileStatic8.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_circleFragileStatic8.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_circleFragileStatic8.SetBodyType( b2_staticBody );

	// _circleFragileStatic9
	_circleFragileStatic9.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_circleFragileStatic9.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_circleFragileStatic9.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_circleFragileStatic9.SetBodyType( b2_staticBody );

	// _circleFragileStatic10
	_circleFragileStatic10.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_circleFragileStatic10.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_circleFragileStatic10.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_circleFragileStatic10.SetBodyType( b2_staticBody );

	// _circleFragileStatic11
	_circleFragileStatic11.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_circleFragileStatic11.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_circleFragileStatic11.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_circleFragileStatic11.SetBodyType( b2_staticBody );

	// _circleFragileStatic12
	_circleFragileStatic12.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_circleFragileStatic12.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_circleFragileStatic12.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_circleFragileStatic12.SetBodyType( b2_staticBody );

	// _circleFragileStatic13
	_circleFragileStatic13.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_circleFragileStatic13.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_circleFragileStatic13.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_circleFragileStatic13.SetBodyType( b2_staticBody );

	// _circleFragileStatic14
	_circleFragileStatic14.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_circleFragileStatic14.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_circleFragileStatic14.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_circleFragileStatic14.SetBodyType( b2_staticBody );

	// _circleFragileStatic15
	_circleFragileStatic15.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_circleFragileStatic15.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_circleFragileStatic15.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_circleFragileStatic15.SetBodyType( b2_staticBody );

	// _circleFragileStatic16
	_circleFragileStatic16.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_circleFragileStatic16.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_circleFragileStatic16.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_circleFragileStatic16.SetBodyType( b2_staticBody );

	// _circleFragileStatic17
	_circleFragileStatic17.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_circleFragileStatic17.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_circleFragileStatic17.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_circleFragileStatic17.SetBodyType( b2_staticBody );

	// _circleFragileStatic18
	_circleFragileStatic18.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_circleFragileStatic18.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_circleFragileStatic18.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_circleFragileStatic18.SetBodyType( b2_staticBody );

	// _circleFragileStatic19
	_circleFragileStatic19.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_circleFragileStatic19.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_circleFragileStatic19.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_circleFragileStatic19.SetBodyType( b2_staticBody );

	// _circleFragileStatic20
	_circleFragileStatic20.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_circleFragileStatic20.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_circleFragileStatic20.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_circleFragileStatic20.SetBodyType( b2_staticBody );

	// _circleFragileStatic21
	_circleFragileStatic21.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_circleFragileStatic21.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_circleFragileStatic21.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_circleFragileStatic21.SetBodyType( b2_staticBody );

	// _circleFragileStatic22
	_circleFragileStatic22.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_circleFragileStatic22.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_circleFragileStatic22.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_circleFragileStatic22.SetBodyType( b2_staticBody );

	// _circleFragileStatic23
	_circleFragileStatic23.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_circleFragileStatic23.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_circleFragileStatic23.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_circleFragileStatic23.SetBodyType( b2_staticBody );

	// _circleFragileStatic24
	_circleFragileStatic24.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_circleFragileStatic24.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_circleFragileStatic24.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_circleFragileStatic24.SetBodyType( b2_staticBody );

	// _circleFragileStatic25
	_circleFragileStatic25.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_circleFragileStatic25.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_circleFragileStatic25.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_circleFragileStatic25.SetBodyType( b2_staticBody );

	// _circleFragileStatic26
	_circleFragileStatic26.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_circleFragileStatic26.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_circleFragileStatic26.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_circleFragileStatic26.SetBodyType( b2_staticBody );

	// _circleFragileStatic27
	_circleFragileStatic27.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_circleFragileStatic27.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_circleFragileStatic27.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_circleFragileStatic27.SetBodyType( b2_staticBody );

	// _circleFragileStatic28
	_circleFragileStatic28.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_circleFragileStatic28.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_circleFragileStatic28.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_circleFragileStatic28.SetBodyType( b2_staticBody );

	// _circleFragileStatic29
	_circleFragileStatic29.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_circleFragileStatic29.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_circleFragileStatic29.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_circleFragileStatic29.SetBodyType( b2_staticBody );

	// _circleFragileStatic30
	_circleFragileStatic30.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_circleFragileStatic30.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_circleFragileStatic30.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_circleFragileStatic30.SetBodyType( b2_staticBody );

	// _circleFragileStatic31
	_circleFragileStatic31.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_circleFragileStatic31.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_circleFragileStatic31.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_circleFragileStatic31.SetBodyType( b2_staticBody );

	// _circleFragileStatic32
	_circleFragileStatic32.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_circleFragileStatic32.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_circleFragileStatic32.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_circleFragileStatic32.SetBodyType( b2_staticBody );

	// _circleFragileStatic33
	_circleFragileStatic33.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_circleFragileStatic33.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_circleFragileStatic33.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_circleFragileStatic33.SetBodyType( b2_staticBody );

	// _circleFragileStatic34
	_circleFragileStatic34.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_circleFragileStatic34.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_circleFragileStatic34.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_circleFragileStatic34.SetBodyType( b2_staticBody );

	// _circleFragileStatic35
	_circleFragileStatic35.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_circleFragileStatic35.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_circleFragileStatic35.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_circleFragileStatic35.SetBodyType( b2_staticBody );

	// _circleFragileStatic36
	_circleFragileStatic36.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_circleFragileStatic36.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_circleFragileStatic36.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_circleFragileStatic36.SetBodyType( b2_staticBody );

	// _circleFragileStatic37
	_circleFragileStatic37.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_circleFragileStatic37.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_circleFragileStatic37.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_circleFragileStatic37.SetBodyType( b2_staticBody );

	// _circleFragileStatic38
	_circleFragileStatic38.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_circleFragileStatic38.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_circleFragileStatic38.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_circleFragileStatic38.SetBodyType( b2_staticBody );

	// _circleFragileStatic39
	_circleFragileStatic39.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_circleFragileStatic39.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_circleFragileStatic39.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_circleFragileStatic39.SetBodyType( b2_staticBody );

	// _circleFragileStatic40
	_circleFragileStatic40.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_circleFragileStatic40.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_circleFragileStatic40.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_circleFragileStatic40.SetBodyType( b2_staticBody );

	// _circleFragileStatic41
	_circleFragileStatic41.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_circleFragileStatic41.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_circleFragileStatic41.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_circleFragileStatic41.SetBodyType( b2_staticBody );

	// _circleFragileStatic42
	_circleFragileStatic42.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_circleFragileStatic42.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_circleFragileStatic42.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_circleFragileStatic42.SetBodyType( b2_staticBody );

	// _circleFragileStatic43
	_circleFragileStatic43.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_circleFragileStatic43.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_circleFragileStatic43.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_circleFragileStatic43.SetBodyType( b2_staticBody );

	// _circleFragileStatic44
	_circleFragileStatic44.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_circleFragileStatic44.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_circleFragileStatic44.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_circleFragileStatic44.SetBodyType( b2_staticBody );

	// _circleFragileStatic45
	_circleFragileStatic45.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_circleFragileStatic45.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_circleFragileStatic45.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_circleFragileStatic45.SetBodyType( b2_staticBody );

	// _circleFragileStatic46
	_circleFragileStatic46.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_circleFragileStatic46.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_circleFragileStatic46.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_circleFragileStatic46.SetBodyType( b2_staticBody );

	// _circleFragileStatic47
	_circleFragileStatic47.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_circleFragileStatic47.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_circleFragileStatic47.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_circleFragileStatic47.SetBodyType( b2_staticBody );

	// _circleFragileStatic48
	_circleFragileStatic48.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_circleFragileStatic48.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_circleFragileStatic48.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_circleFragileStatic48.SetBodyType( b2_staticBody );

	// _circleFragileStatic49
	_circleFragileStatic49.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_circleFragileStatic49.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_circleFragileStatic49.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_circleFragileStatic49.SetBodyType( b2_staticBody );

	// _circleFragileStatic50
	_circleFragileStatic50.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_circleFragileStatic50.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_circleFragileStatic50.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_circleFragileStatic50.SetBodyType( b2_staticBody );

	// _circleWall1
	_circleWall1.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_circleWall1.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_circleWall1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_circleWall1.SetBodyType( b2_staticBody );

	// _circleWall2
	_circleWall2.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_circleWall2.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_circleWall2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_circleWall2.SetBodyType( b2_staticBody );

	// _coin1
	_coin1.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin1.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin1.SetBodyType( b2_dynamicBody );

	// _coin2
	_coin2.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin2.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin2.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin2.SetBodyType( b2_dynamicBody );

	// _coin3
	_coin3.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin3.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin3.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin3.SetBodyType( b2_dynamicBody );

	// _coin4
	_coin4.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin4.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin4.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin4.SetBodyType( b2_dynamicBody );

	// _coin5
	_coin5.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin5.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin5.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin5.SetBodyType( b2_dynamicBody );

	// _piggyBank1
	_piggyBank1.GetBody()->GetFixtureList()->SetDensity( 7.5000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_piggyBank1.SetBodyType( b2_dynamicBody );

	// _wallBox1
	_wallBox1.SetRotation( -33.0000f );
	_wallBox1.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox1.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox1.SetBodyType( b2_staticBody );

	// _wallBox2
	_wallBox2.SetRotation( 33.0000f );
	_wallBox2.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox2.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox2.SetBodyType( b2_staticBody );

	// _wallBox3
	_wallBox3.SetRotation( 3.0000f );
	_wallBox3.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox3.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox3.SetBodyType( b2_staticBody );

	// _wallBox4
	_wallBox4.SetRotation( -3.0000f );
	_wallBox4.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox4.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox4.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox4.SetBodyType( b2_staticBody );


	//Set blocks positions
	_circleFragileStatic1.SetPosition( 48.7000f, 28.8001f );
	_circleFragileStatic2.SetPosition( 88.2000f, 39.6001f );
	_circleFragileStatic3.SetPosition( 92.4500f, 46.3501f );
	_circleFragileStatic4.SetPosition( 37.8000f, 23.4000f );
	_circleFragileStatic5.SetPosition( 92.3499f, 32.7001f );
	_circleFragileStatic6.SetPosition( 61.8000f, 30.0001f );
	_circleFragileStatic7.SetPosition( 24.4000f, 8.4000f );
	_circleFragileStatic8.SetPosition( 68.7500f, 42.9501f );
	_circleFragileStatic9.SetPosition( 2.9000f, 34.4001f );
	_circleFragileStatic10.SetPosition( 58.4001f, 20.6000f );
	_circleFragileStatic11.SetPosition( 29.3000f, 16.9000f );
	_circleFragileStatic12.SetPosition( 80.9000f, 46.6001f );
	_circleFragileStatic13.SetPosition( 33.9000f, 31.7001f );
	_circleFragileStatic14.SetPosition( 3.4500f, 46.8001f );
	_circleFragileStatic15.SetPosition( 16.7000f, 34.2001f );
	_circleFragileStatic16.SetPosition( 27.1000f, 43.0001f );
	_circleFragileStatic17.SetPosition( 53.7000f, 43.9001f );
	_circleFragileStatic18.SetPosition( 72.3000f, 34.2001f );
	_circleFragileStatic19.SetPosition( 42.6000f, 44.1001f );
	_circleFragileStatic20.SetPosition( 16.0000f, 46.6001f );
	_circleFragileStatic21.SetPosition( 41.8000f, 35.7001f );
	_circleFragileStatic22.SetPosition( 25.2000f, 34.7001f );
	_circleFragileStatic23.SetPosition( 68.3000f, 17.2000f );
	_circleFragileStatic24.SetPosition( 59.7000f, 51.5001f );
	_circleFragileStatic25.SetPosition( 32.4000f, 48.0501f );
	_circleFragileStatic26.SetPosition( 81.6000f, 7.9000f );
	_circleFragileStatic27.SetPosition( 54.8000f, 35.3001f );
	_circleFragileStatic28.SetPosition( 78.2000f, 17.4000f );
	_circleFragileStatic29.SetPosition( 19.3000f, 16.5000f );
	_circleFragileStatic30.SetPosition( 33.9000f, 9.4000f );
	_circleFragileStatic31.SetPosition( 61.3000f, 9.2000f );
	_circleFragileStatic32.SetPosition( 15.9000f, 24.8000f );
	_circleFragileStatic33.SetPosition( 61.7000f, 41.3501f );
	_circleFragileStatic34.SetPosition( 10.2000f, 15.2000f );
	_circleFragileStatic35.SetPosition( 6.4000f, 25.1000f );
	_circleFragileStatic36.SetPosition( 81.3000f, 25.3000f );
	_circleFragileStatic37.SetPosition( 9.3000f, 39.4001f );
	_circleFragileStatic38.SetPosition( 92.0000f, 23.0000f );
	_circleFragileStatic39.SetPosition( 48.0000f, 52.5100f );
	_circleFragileStatic40.SetPosition( 68.7500f, 52.5100f );
	_circleFragileStatic41.SetPosition( 6.5000f, 52.5100f );
	_circleFragileStatic42.SetPosition( 78.9500f, 56.0100f );
	_circleFragileStatic43.SetPosition( 89.5000f, 52.5100f );
	_circleFragileStatic44.SetPosition( 34.7000f, 39.7001f );
	_circleFragileStatic45.SetPosition( 27.2500f, 52.5100f );
	_circleFragileStatic46.SetPosition( 72.7000f, 9.6000f );
	_circleFragileStatic47.SetPosition( 88.2000f, 14.1000f );
	_circleFragileStatic48.SetPosition( 48.6000f, 38.7001f );
	_circleFragileStatic49.SetPosition( 15.7000f, 7.7000f );
	_circleFragileStatic50.SetPosition( 81.5000f, 34.2001f );
	_circleWall1.SetPosition( 24.5000f, 24.9000f );
	_circleWall2.SetPosition( 72.5000f, 25.2000f );
	_coin1.SetPosition( 6.5000f, 60.3998f );
	_coin2.SetPosition( 27.2502f, 60.3998f );
	_coin3.SetPosition( 48.0000f, 60.3998f );
	_coin4.SetPosition( 68.7499f, 60.3998f );
	_coin5.SetPosition( 89.4993f, 60.3998f );
	_piggyBank1.SetPosition( 48.0000f, 11.3000f );
	_wallBox1.SetPosition( 55.5001f, 16.2000f );
	_wallBox2.SetPosition( 40.4999f, 16.1000f );
	_wallBox3.SetPosition( 27.0000f, 3.3000f );
	_wallBox4.SetPosition( 69.0001f, 3.3000f );

	ConstFinal();
}
//--------------------------------------------------------------------------
LevelScale_3::LevelScale_3( GameTypes::LevelEnum levelEnum, bool viewMode ) : Level( levelEnum, viewMode )
{
    _levelTag = eLevelTag_Frigle;
    
	// _circleWall1
	_circleWall1.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_circleWall1.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_circleWall1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_circleWall1.SetBodyType( b2_staticBody );

	// _coin1
	_coin1.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin1.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin1.SetBodyType( b2_dynamicBody );

	// _fragileBox1
	_fragileBox1.SetRotation( 90.0000f );
	_fragileBox1.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox1.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox1.SetBodyType( b2_dynamicBody );

	// _fragileBox2
	_fragileBox2.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox2.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox2.SetBodyType( b2_dynamicBody );

	// _fragileBox3
	_fragileBox3.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox3.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox3.SetBodyType( b2_dynamicBody );

	// _fragileBox4
	_fragileBox4.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox4.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox4.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox4.SetBodyType( b2_dynamicBody );

	// _fragileBox5
	_fragileBox5.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox5.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox5.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox5.SetBodyType( b2_dynamicBody );

	// _fragileBox6
	_fragileBox6.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox6.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox6.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox6.SetBodyType( b2_dynamicBody );

	// _fragileBox7
	_fragileBox7.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox7.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox7.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox7.SetBodyType( b2_dynamicBody );

	// _fragileBox8
	_fragileBox8.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox8.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox8.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox8.SetBodyType( b2_dynamicBody );

	// _fragileBox9
	_fragileBox9.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox9.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox9.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox9.SetBodyType( b2_dynamicBody );

	// _piggyBank1
	_piggyBank1.FlipX();
	_piggyBank1.GetBody()->GetFixtureList()->SetDensity( 7.5000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_piggyBank1.SetBodyType( b2_dynamicBody );

	// _wallBox1
	_wallBox1.SetRotation( 90.0000f );
	_wallBox1.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox1.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox1.SetBodyType( b2_staticBody );

	// _wallBox2
	_wallBox2.SetRotation( 210.9001f );
	_wallBox2.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox2.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox2.SetBodyType( b2_staticBody );

	// _wallBox3
	_wallBox3.SetRotation( 227.7002f );
	_wallBox3.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox3.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox3.SetBodyType( b2_staticBody );

	// _wallBox4
	_wallBox4.SetRotation( 271.2001f );
	_wallBox4.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox4.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox4.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox4.SetBodyType( b2_staticBody );

	// _wallBox5
	_wallBox5.SetRotation( 110.0999f );
	_wallBox5.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox5.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox5.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox5.SetBodyType( b2_staticBody );

	// _wallBox6
	_wallBox6.SetRotation( 126.9001f );
	_wallBox6.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox6.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox6.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox6.SetBodyType( b2_staticBody );

	// _wallBox7
	_wallBox7.SetRotation( 143.7001f );
	_wallBox7.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox7.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox7.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox7.SetBodyType( b2_staticBody );

	// _wallBox8
	_wallBox8.SetRotation( 160.5001f );
	_wallBox8.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox8.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox8.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox8.SetBodyType( b2_staticBody );

	// _wallBox9
	_wallBox9.SetRotation( 177.3001f );
	_wallBox9.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox9.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox9.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox9.SetBodyType( b2_staticBody );

	// _wallBox10
	_wallBox10.SetRotation( 194.1002f );
	_wallBox10.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox10.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox10.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox10.SetBodyType( b2_staticBody );

	// _woodBox1
	_woodBox1.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox1.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_woodBox1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_woodBox1.SetBodyType( b2_dynamicBody );


	//Set blocks positions
	_circleWall1.SetPosition( 62.2009f, 12.7002f );
	_coin1.SetPosition( 62.2009f, 23.8504f );
	_fragileBox1.SetPosition( 53.3007f, 29.7002f );
	_fragileBox2.SetPosition( 38.8004f, 34.6002f );
	_fragileBox3.SetPosition( 85.6013f, 34.6002f );
	_fragileBox4.SetPosition( 37.1004f, 24.9002f );
	_fragileBox5.SetPosition( 87.3012f, 24.8002f );
	_fragileBox6.SetPosition( 71.1013f, 24.9002f );
	_fragileBox7.SetPosition( 71.1013f, 44.3004f );
	_fragileBox8.SetPosition( 71.1013f, 34.6004f );
	_fragileBox9.SetPosition( 53.3007f, 44.2004f );
	_piggyBank1.SetPosition( 15.5001f, 15.5000f );
	_wallBox1.SetPosition( 3.2002f, 32.6500f );
	_wallBox2.SetPosition( 23.5437f, 9.0401f );
	_wallBox3.SetPosition( 26.5959f, 11.5385f );
	_wallBox4.SetPosition( 3.1828f, 20.3694f );
	_wallBox5.SetPosition( 3.9333f, 15.9842f );
	_wallBox6.SetPosition( 5.8153f, 12.5181f );
	_wallBox7.SetPosition( 8.6190f, 9.7437f );
	_wallBox8.SetPosition( 12.1047f, 7.8981f );
	_wallBox9.SetPosition( 15.9750f, 7.1390f );
	_wallBox10.SetPosition( 19.8998f, 7.5307f );
	_woodBox1.SetPosition( 62.2009f, 18.8003f );

	ConstFinal();
}
//--------------------------------------------------------------------------
LevelBombAndRoll_1::LevelBombAndRoll_1( GameTypes::LevelEnum levelEnum, bool viewMode ) : Level( levelEnum, viewMode )
{
	_coinCount = 2;

	// _bomb1
	_bomb1.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_bomb1.GetBody()->GetFixtureList()->SetFriction( 0.2000f );
	_bomb1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bomb1.SetBodyType( b2_dynamicBody );

	// _bomb2
	_bomb2.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_bomb2.GetBody()->GetFixtureList()->SetFriction( 0.2000f );
	_bomb2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bomb2.SetBodyType( b2_dynamicBody );

	// _coin1
	_coin1.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin1.GetBody()->GetFixtureList()->SetFriction( 0.4500f );
	_coin1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin1.SetBodyType( b2_dynamicBody );

	// _coin2
	_coin2.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin2.GetBody()->GetFixtureList()->SetFriction( 0.4500f );
	_coin2.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin2.SetBodyType( b2_dynamicBody );

	// _piggyBank1
	_piggyBank1.GetBody()->GetFixtureList()->SetDensity( 10.0000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_piggyBank1.SetBodyType( b2_dynamicBody );

	// _wallBox1
	_wallBox1.SetRotation( 220.7295f );
	_wallBox1.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox1.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox1.SetBodyType( b2_staticBody );

	// _wallBox2
	_wallBox2.SetRotation( 137.9294f );
	_wallBox2.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox2.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox2.SetBodyType( b2_staticBody );

	// _wallBox3
	_wallBox3.SetRotation( 186.8648f );
	_wallBox3.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox3.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox3.SetBodyType( b2_staticBody );

	// _wallBox4
	_wallBox4.SetRotation( 225.6001f );
	_wallBox4.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox4.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox4.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox4.SetBodyType( b2_staticBody );

	// _wallBox5
	_wallBox5.SetRotation( 230.4707f );
	_wallBox5.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox5.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox5.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox5.SetBodyType( b2_staticBody );

	// _wallBox6
	_wallBox6.SetRotation( 96.9647f );
	_wallBox6.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox6.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox6.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox6.SetBodyType( b2_staticBody );

	// _wallBox7
	_wallBox7.SetRotation( 235.3412f );
	_wallBox7.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox7.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox7.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox7.SetBodyType( b2_staticBody );

	// _wallBox8
	_wallBox8.SetRotation( 240.2118f );
	_wallBox8.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox8.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox8.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox8.SetBodyType( b2_staticBody );

	// _wallBox9
	_wallBox9.SetRotation( 245.0824f );
	_wallBox9.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox9.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox9.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox9.SetBodyType( b2_staticBody );

	// _wallBox10
	_wallBox10.SetRotation( 249.9530f );
	_wallBox10.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox10.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox10.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox10.SetBodyType( b2_staticBody );

	// _wallBox11
	_wallBox11.SetRotation( 158.2765f );
	_wallBox11.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox11.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox11.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox11.SetBodyType( b2_staticBody );

	// _wallBox12
	_wallBox12.SetRotation( 254.8236f );
	_wallBox12.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox12.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox12.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox12.SetBodyType( b2_staticBody );

	// _wallBox13
	_wallBox13.SetRotation( 262.6942f );
	_wallBox13.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox13.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox13.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox13.SetBodyType( b2_staticBody );

	// _wallBox14
	_wallBox14.SetRotation( 190.0413f );
	_wallBox14.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox14.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox14.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox14.SetBodyType( b2_staticBody );

	// _wallBox15
	_wallBox15.SetRotation( 142.8000f );
	_wallBox15.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox15.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox15.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox15.SetBodyType( b2_staticBody );

	// _wallBox16
	_wallBox16.SetRotation( 0.1200f );
	_wallBox16.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox16.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox16.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox16.SetBodyType( b2_staticBody );

	// _wallBox17
	_wallBox17.SetRotation( 13.0800f );
	_wallBox17.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox17.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox17.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox17.SetBodyType( b2_staticBody );

	// _wallBox18
	_wallBox18.SetRotation( 26.0400f );
	_wallBox18.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox18.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox18.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox18.SetBodyType( b2_staticBody );

	// _wallBox19
	_wallBox19.SetRotation( 103.8353f );
	_wallBox19.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox19.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox19.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox19.SetBodyType( b2_staticBody );

	// _wallBox20
	_wallBox20.SetRotation( 39.0000f );
	_wallBox20.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox20.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox20.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox20.SetBodyType( b2_staticBody );

	// _wallBox21
	_wallBox21.SetRotation( 183.6884f );
	_wallBox21.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox21.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox21.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox21.SetBodyType( b2_staticBody );

	// _wallBox22
	_wallBox22.SetRotation( 161.4530f );
	_wallBox22.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox22.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox22.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox22.SetBodyType( b2_staticBody );

	// _wallBox23
	_wallBox23.SetRotation( 51.9600f );
	_wallBox23.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox23.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox23.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox23.SetBodyType( b2_staticBody );

	// _wallBox24
	_wallBox24.SetRotation( 68.9200f );
	_wallBox24.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox24.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox24.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox24.SetBodyType( b2_staticBody );

	// _wallBox25
	_wallBox25.SetRotation( 81.8800f );
	_wallBox25.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox25.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox25.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox25.SetBodyType( b2_staticBody );

	// _wallBox26
	_wallBox26.SetRotation( 193.2178f );
	_wallBox26.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox26.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox26.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox26.SetBodyType( b2_staticBody );

	// _wallBox27
	_wallBox27.SetRotation( 360.4201f );
	_wallBox27.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox27.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox27.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox27.SetBodyType( b2_staticBody );

	// _wallBox28
	_wallBox28.SetRotation( 147.6705f );
	_wallBox28.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox28.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox28.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox28.SetBodyType( b2_staticBody );

	// _wallBox29
	_wallBox29.SetRotation( 347.4600f );
	_wallBox29.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox29.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox29.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox29.SetBodyType( b2_staticBody );

	// _wallBox30
	_wallBox30.SetRotation( 334.5002f );
	_wallBox30.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox30.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox30.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox30.SetBodyType( b2_staticBody );

	// _wallBox31
	_wallBox31.SetRotation( 321.5401f );
	_wallBox31.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox31.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox31.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox31.SetBodyType( b2_staticBody );

	// _wallBox32
	_wallBox32.SetRotation( 185.6352f );
	_wallBox32.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox32.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox32.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox32.SetBodyType( b2_staticBody );

	// _wallBox33
	_wallBox33.SetRotation( 164.6294f );
	_wallBox33.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox33.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox33.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox33.SetBodyType( b2_staticBody );

	// _wallBox34
	_wallBox34.SetRotation( 308.5801f );
	_wallBox34.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox34.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox34.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox34.SetBodyType( b2_staticBody );

	// _wallBox35
	_wallBox35.SetRotation( 108.7059f );
	_wallBox35.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox35.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox35.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox35.SetBodyType( b2_staticBody );

	// _wallBox36
	_wallBox36.SetRotation( 288.6201f );
	_wallBox36.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox36.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox36.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox36.SetBodyType( b2_staticBody );

	// _wallBox37
	_wallBox37.SetRotation( 196.3943f );
	_wallBox37.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox37.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox37.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox37.SetBodyType( b2_staticBody );

	// _wallBox38
	_wallBox38.SetRotation( 113.5764f );
	_wallBox38.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox38.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox38.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox38.SetBodyType( b2_staticBody );

	// _wallBox39
	_wallBox39.SetRotation( 118.4470f );
	_wallBox39.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox39.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox39.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox39.SetBodyType( b2_staticBody );

	// _wallBox40
	_wallBox40.SetRotation( 152.5412f );
	_wallBox40.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox40.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox40.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox40.SetBodyType( b2_staticBody );

	// _wallBox41
	_wallBox41.SetRotation( 123.3176f );
	_wallBox41.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox41.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox41.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox41.SetBodyType( b2_staticBody );

	// _wallBox42
	_wallBox42.SetRotation( 167.8059f );
	_wallBox42.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox42.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox42.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox42.SetBodyType( b2_staticBody );

	// _wallBox43
	_wallBox43.SetRotation( 191.5058f );
	_wallBox43.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox43.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox43.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox43.SetBodyType( b2_staticBody );

	// _wallBox44
	_wallBox44.SetRotation( 128.1882f );
	_wallBox44.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox44.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox44.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox44.SetBodyType( b2_staticBody );

	// _wallBox45
	_wallBox45.SetRotation( 133.0588f );
	_wallBox45.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox45.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox45.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox45.SetBodyType( b2_staticBody );

	// _wallBox46
	_wallBox46.SetRotation( 157.4118f );
	_wallBox46.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox46.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox46.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox46.SetBodyType( b2_staticBody );

	// _wallBox47
	_wallBox47.SetRotation( 162.2823f );
	_wallBox47.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox47.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox47.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox47.SetBodyType( b2_staticBody );

	// _wallBox48
	_wallBox48.SetRotation( 167.1529f );
	_wallBox48.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox48.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox48.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox48.SetBodyType( b2_staticBody );

	// _wallBox49
	_wallBox49.SetRotation( 174.0234f );
	_wallBox49.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox49.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox49.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox49.SetBodyType( b2_staticBody );

	// _wallBox50
	_wallBox50.SetRotation( 199.5707f );
	_wallBox50.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox50.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox50.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox50.SetBodyType( b2_staticBody );

	// _wallBox51
	_wallBox51.SetRotation( 178.8940f );
	_wallBox51.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox51.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox51.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox51.SetBodyType( b2_staticBody );

	// _wallBox52
	_wallBox52.SetRotation( 202.7473f );
	_wallBox52.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox52.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox52.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox52.SetBodyType( b2_staticBody );

	// _wallBox53
	_wallBox53.SetRotation( 205.9237f );
	_wallBox53.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox53.GetBody()->GetFixtureList()->SetFriction( 0.1500f );
	_wallBox53.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox53.SetBodyType( b2_staticBody );

	// _wallBox54
	_wallBox54.SetRotation( 196.3764f );
	_wallBox54.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox54.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox54.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox54.SetBodyType( b2_staticBody );

	// _wallBox55
	_wallBox55.SetRotation( 170.9824f );
	_wallBox55.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox55.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox55.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox55.SetBodyType( b2_staticBody );

	// _wallBox56
	_wallBox56.SetRotation( 155.1001f );
	_wallBox56.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox56.GetBody()->GetFixtureList()->SetFriction( 0.1500f );
	_wallBox56.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox56.SetBodyType( b2_staticBody );

	// _wallBox57
	_wallBox57.SetRotation( 174.1589f );
	_wallBox57.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox57.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox57.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox57.SetBodyType( b2_staticBody );

	// _wallBox58
	_wallBox58.SetRotation( 177.3354f );
	_wallBox58.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox58.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox58.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox58.SetBodyType( b2_staticBody );

	// _wallBox59
	_wallBox59.SetRotation( 180.5119f );
	_wallBox59.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox59.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox59.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox59.SetBodyType( b2_staticBody );

	// _wallBox60
	_wallBox60.SetRotation( 272.6942f );
	_wallBox60.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox60.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox60.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox60.SetBodyType( b2_staticBody );

	// _wallBox61
	_wallBox61.SetRotation( 201.2470f );
	_wallBox61.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox61.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox61.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox61.SetBodyType( b2_staticBody );

	// _wallBox62
	_wallBox62.SetRotation( 206.1177f );
	_wallBox62.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox62.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox62.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox62.SetBodyType( b2_staticBody );

	// _wallBox63
	_wallBox63.SetRotation( 210.9883f );
	_wallBox63.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox63.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox63.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox63.SetBodyType( b2_staticBody );

	// _wallBox64
	_wallBox64.SetRotation( 215.8589f );
	_wallBox64.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox64.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox64.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox64.SetBodyType( b2_staticBody );

	// _wallBox65
	_wallBox65.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox65.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox65.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox65.SetBodyType( b2_staticBody );

	// _fragileBoxStatic1
	_fragileBoxStatic1.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBoxStatic1.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBoxStatic1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBoxStatic1.SetBodyType( b2_staticBody );

	// _fragileBoxStatic2
	_fragileBoxStatic2.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBoxStatic2.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBoxStatic2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBoxStatic2.SetBodyType( b2_staticBody );

	// _fragileBoxStatic3
	_fragileBoxStatic3.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBoxStatic3.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBoxStatic3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBoxStatic3.SetBodyType( b2_staticBody );

	// _bomb3
	_bomb3.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_bomb3.GetBody()->GetFixtureList()->SetFriction( 0.2000f );
	_bomb3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bomb3.SetBodyType( b2_dynamicBody );

	// _fragileBoxStatic4
	_fragileBoxStatic4.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBoxStatic4.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBoxStatic4.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBoxStatic4.SetBodyType( b2_staticBody );

	// _bomb4
	_bomb4.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_bomb4.GetBody()->GetFixtureList()->SetFriction( 0.2000f );
	_bomb4.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bomb4.SetBodyType( b2_dynamicBody );


	//Set blocks positions
	_bomb1.SetPosition( 27.9999f, 31.9002f );
	_bomb2.SetPosition( 37.6997f, 31.9002f );
	_coin1.SetPosition( 23.9499f, 52.0503f );
	_coin2.SetPosition( 72.0502f, 52.0503f );
	_piggyBank1.SetPosition( 48.5001f, 12.5500f );
	_wallBox1.SetPosition( 76.9823f, 15.0101f );
	_wallBox2.SetPosition( 18.3131f, 15.7453f );
	_wallBox3.SetPosition( 56.0422f, 38.2446f );
	_wallBox4.SetPosition( 79.9265f, 17.7714f );
	_wallBox5.SetPosition( 82.6257f, 20.7728f );
	_wallBox6.SetPosition( 3.3204f, 43.6036f );
	_wallBox7.SetPosition( 85.0603f, 23.9928f );
	_wallBox8.SetPosition( 87.2129f, 27.4076f );
	_wallBox9.SetPosition( 89.0677f, 30.9930f );
	_wallBox10.SetPosition( 90.6113f, 34.7228f );
	_wallBox11.SetPosition( 22.5664f, 42.4140f );
	_wallBox12.SetPosition( 91.8327f, 38.5704f );
	_wallBox13.SetPosition( 92.6229f, 42.5078f );
	_wallBox14.SetPosition( 59.8804f, 38.8148f );
	_wallBox15.SetPosition( 21.4217f, 13.1703f );
	_wallBox16.SetPosition( 18.8851f, 60.4956f );
	_wallBox17.SetPosition( 14.9977f, 60.0806f );
	_wallBox18.SetPosition( 11.5946f, 58.8716f );
	_wallBox19.SetPosition( 4.0184f, 39.6466f );
	_wallBox20.SetPosition( 8.5498f, 56.9300f );
	_wallBox21.SetPosition( 52.1784f, 37.8876f );
	_wallBox22.SetPosition( 25.9096f, 41.2282f );
	_wallBox23.SetPosition( 6.0176f, 54.3554f );
	_wallBox24.SetPosition( 4.1772f, 51.0782f );
	_wallBox25.SetPosition( 3.2754f, 47.3553f );
	_wallBox26.SetPosition( 63.6810f, 39.5970f );
	_wallBox27.SetPosition( 76.9643f, 60.4736f );
	_wallBox28.SetPosition( 24.7379f, 10.8684f );
	_wallBox29.SetPosition( 80.8555f, 60.0924f );
	_wallBox30.SetPosition( 84.2697f, 58.9156f );
	_wallBox31.SetPosition( 87.3329f, 57.0028f );
	_wallBox32.SetPosition( 51.4783f, 3.7736f );
	_wallBox33.SetPosition( 29.3710f, 40.1964f );
	_wallBox34.SetPosition( 90.1891f, 54.2520f );
	_wallBox35.SetPosition( 5.1492f, 35.7716f );
	_wallBox36.SetPosition( 92.1585f, 50.7925f );
	_wallBox37.SetPosition( 67.2826f, 40.5386f );
	_wallBox38.SetPosition( 6.6052f, 32.0066f );
	_wallBox39.SetPosition( 8.3754f, 28.3788f );
	_wallBox40.SetPosition( 28.2373f, 8.8566f );
	_wallBox41.SetPosition( 10.4474f, 24.9144f );
	_wallBox42.SetPosition( 32.8896f, 39.3220f );
	_wallBox43.SetPosition( 55.4643f, 4.4100f );
	_wallBox44.SetPosition( 12.8058f, 21.6386f );
	_wallBox45.SetPosition( 15.4341f, 18.5748f );
	_wallBox46.SetPosition( 31.8951f, 7.1492f );
	_wallBox47.SetPosition( 35.6845f, 5.7584f );
	_wallBox48.SetPosition( 39.5785f, 4.6444f );
	_wallBox49.SetPosition( 43.5487f, 3.8650f );
	_wallBox50.SetPosition( 70.7233f, 41.6368f );
	_wallBox51.SetPosition( 47.5163f, 3.5752f );
	_wallBox52.SetPosition( 74.0921f, 42.9374f );
	_wallBox53.SetPosition( 77.3777f, 44.4364f );
	_wallBox54.SetPosition( 59.3821f, 5.4326f );
	_wallBox55.SetPosition( 36.7035f, 38.6078f );
	_wallBox56.SetPosition( 19.3027f, 43.7996f );
	_wallBox57.SetPosition( 40.5514f, 38.1062f );
	_wallBox58.SetPosition( 44.4210f, 37.8182f );
	_wallBox59.SetPosition( 48.3005f, 37.7454f );
	_wallBox60.SetPosition( 92.8729f, 46.7080f );
	_wallBox61.SetPosition( 63.2033f, 6.7342f );
	_wallBox62.SetPosition( 66.8999f, 8.3556f );
	_wallBox63.SetPosition( 70.4453f, 10.2850f );
	_wallBox64.SetPosition( 73.8141f, 12.5085f );
	_wallBox65.SetPosition( 48.0001f, 60.4500f );
	_fragileBoxStatic1.SetPosition( 28.0000f, 22.2000f );
	_fragileBoxStatic2.SetPosition( 37.7006f, 22.2000f );
	_fragileBoxStatic3.SetPosition( 58.5001f, 22.2000f );
	_bomb3.SetPosition( 58.5000f, 31.9002f );
	_fragileBoxStatic4.SetPosition( 68.2011f, 22.2000f );
	_bomb4.SetPosition( 68.2002f, 31.9002f );

	//Bombs Construction
	_bomb1.SetRange( 20.0000f );
	_bomb1.SetBombImpulse( 23675.0000f );
	_bomb2.SetRange( 20.0000f );
	_bomb2.SetBombImpulse( 23675.0000f );
	_bomb3.SetRange( 20.0000f );
	_bomb3.SetBombImpulse( 23675.0000f );
	_bomb4.SetRange( 20.0000f );
	_bomb4.SetBombImpulse( 23675.0000f );

	ConstFinal();
}
//--------------------------------------------------------------------------
LevelTunnel_1::LevelTunnel_1( GameTypes::LevelEnum levelEnum, bool viewMode ) : Level( levelEnum, viewMode )
{
	// _bomb1
	_bomb1.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_bomb1.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_bomb1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bomb1.SetBodyType( b2_dynamicBody );

	// _coin1
	_coin1.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin1.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin1.SetBodyType( b2_dynamicBody );

	// _fragileBox1
	_fragileBox1.SetRotation( 90.0000f );
	_fragileBox1.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox1.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox1.SetBodyType( b2_dynamicBody );

	// _fragileBox2
	_fragileBox2.SetRotation( 90.0000f );
	_fragileBox2.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox2.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox2.SetBodyType( b2_dynamicBody );

	// _fragileBoxStatic1
	_fragileBoxStatic1.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBoxStatic1.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBoxStatic1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBoxStatic1.SetBodyType( b2_staticBody );

	// _fragileBoxStatic2
	_fragileBoxStatic2.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBoxStatic2.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBoxStatic2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBoxStatic2.SetBodyType( b2_staticBody );

	// _fragileBoxStatic3
	_fragileBoxStatic3.SetRotation( 10.2000f );
	_fragileBoxStatic3.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBoxStatic3.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBoxStatic3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBoxStatic3.SetBodyType( b2_staticBody );

	// _fragileBoxStatic4
	_fragileBoxStatic4.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBoxStatic4.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBoxStatic4.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBoxStatic4.SetBodyType( b2_staticBody );

	// _piggyBank1
	_piggyBank1.GetBody()->GetFixtureList()->SetDensity( 7.5000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_piggyBank1.SetBodyType( b2_dynamicBody );

	// _wallBox1
	_wallBox1.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox1.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox1.SetBodyType( b2_staticBody );

	// _wallBox2
	_wallBox2.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox2.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox2.SetBodyType( b2_staticBody );

	// _wallBox3
	_wallBox3.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox3.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox3.SetBodyType( b2_staticBody );

	// _wallBox4
	_wallBox4.SetRotation( 139.3000f );
	_wallBox4.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox4.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox4.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox4.SetBodyType( b2_staticBody );

	// _wallBox5
	_wallBox5.SetRotation( 242.4999f );
	_wallBox5.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox5.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox5.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox5.SetBodyType( b2_staticBody );

	// _wallBox6
	_wallBox6.SetRotation( 294.1000f );
	_wallBox6.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox6.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox6.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox6.SetBodyType( b2_staticBody );

	// _wallBox7
	_wallBox7.SetRotation( 276.9000f );
	_wallBox7.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox7.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox7.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox7.SetBodyType( b2_staticBody );

	// _wallBox8
	_wallBox8.SetRotation( 380.0998f );
	_wallBox8.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox8.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox8.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox8.SetBodyType( b2_staticBody );

	// _wallBox9
	_wallBox9.SetRotation( 362.9002f );
	_wallBox9.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox9.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox9.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox9.SetBodyType( b2_staticBody );

	// _wallBox10
	_wallBox10.SetRotation( 156.5000f );
	_wallBox10.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox10.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox10.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox10.SetBodyType( b2_staticBody );

	// _wallBox11
	_wallBox11.SetRotation( 122.0999f );
	_wallBox11.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox11.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox11.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox11.SetBodyType( b2_staticBody );

	// _wallBox12
	_wallBox12.SetRotation( 225.3000f );
	_wallBox12.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox12.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox12.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox12.SetBodyType( b2_staticBody );

	// _wallBox13
	_wallBox13.SetRotation( 208.1000f );
	_wallBox13.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox13.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox13.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox13.SetBodyType( b2_staticBody );

	// _wallBox14
	_wallBox14.SetRotation( 397.3002f );
	_wallBox14.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox14.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox14.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox14.SetBodyType( b2_staticBody );

	// _wallBox15
	_wallBox15.SetRotation( 414.5004f );
	_wallBox15.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox15.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox15.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox15.SetBodyType( b2_staticBody );

	// _wallBox16
	_wallBox16.SetRotation( 311.2999f );
	_wallBox16.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox16.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox16.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox16.SetBodyType( b2_staticBody );

	// _wallBox17
	_wallBox17.SetRotation( 328.4998f );
	_wallBox17.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox17.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox17.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox17.SetBodyType( b2_staticBody );

	// _wallBox18
	_wallBox18.SetRotation( 345.7002f );
	_wallBox18.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox18.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox18.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox18.SetBodyType( b2_staticBody );

	// _wallBox19
	_wallBox19.SetRotation( 259.7000f );
	_wallBox19.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox19.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox19.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox19.SetBodyType( b2_staticBody );

	// _wallBox20
	_wallBox20.SetRotation( 173.7000f );
	_wallBox20.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox20.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox20.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox20.SetBodyType( b2_staticBody );

	// _wallBox21
	_wallBox21.SetRotation( 190.9000f );
	_wallBox21.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox21.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox21.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox21.SetBodyType( b2_staticBody );

	// _wallBox22
	_wallBox22.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox22.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox22.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox22.SetBodyType( b2_staticBody );

	// _woodBox1
	_woodBox1.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox1.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_woodBox1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_woodBox1.SetBodyType( b2_dynamicBody );

	// _woodBox2
	_woodBox2.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox2.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_woodBox2.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_woodBox2.SetBodyType( b2_dynamicBody );

	// _woodBox3
	_woodBox3.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox3.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_woodBox3.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_woodBox3.SetBodyType( b2_dynamicBody );

	// _woodBox4
	_woodBox4.SetRotation( 9.6000f );
	_woodBox4.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox4.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_woodBox4.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_woodBox4.SetBodyType( b2_dynamicBody );

	// _woodBox5
	_woodBox5.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox5.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_woodBox5.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_woodBox5.SetBodyType( b2_dynamicBody );

	// _woodBox6
	_woodBox6.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox6.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_woodBox6.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_woodBox6.SetBodyType( b2_dynamicBody );


	//Set blocks positions
	_bomb1.SetPosition( 34.7000f, 9.9500f );
	_coin1.SetPosition( 22.9000f, 59.7498f );
	_fragileBox1.SetPosition( 44.4000f, 10.5000f );
	_fragileBox2.SetPosition( 25.2000f, 10.5000f );
	_fragileBoxStatic1.SetPosition( 22.5000f, 48.8500f );
	_fragileBoxStatic2.SetPosition( 40.3000f, 34.8500f );
	_fragileBoxStatic3.SetPosition( 16.5000f, 35.4500f );
	_fragileBoxStatic4.SetPosition( 20.2000f, 22.9500f );
	_piggyBank1.SetPosition( 80.1498f, 27.5500f );
	_wallBox1.SetPosition( 8.9000f, 15.3501f );
	_wallBox2.SetPosition( 62.2500f, 32.8002f );
	_wallBox3.SetPosition( 62.2500f, 22.5500f );
	_wallBox4.SetPosition( 72.6101f, 18.4868f );
	_wallBox5.SetPosition( 91.0795f, 22.0434f );
	_wallBox6.SetPosition( 91.3893f, 32.4844f );
	_wallBox7.SetPosition( 92.3484f, 29.0260f );
	_wallBox8.SetPosition( 76.3112f, 38.8534f );
	_wallBox9.SetPosition( 79.8280f, 39.5690f );
	_wallBox10.SetPosition( 75.6503f, 16.5798f );
	_wallBox11.SetPosition( 70.2699f, 21.2076f );
	_wallBox12.SetPosition( 88.9649f, 19.1436f );
	_wallBox13.SetPosition( 86.0874f, 16.9988f );
	_wallBox14.SetPosition( 73.1635f, 37.1300f );
	_wallBox15.SetPosition( 70.6658f, 34.5528f );
	_wallBox16.SetPosition( 89.4504f, 35.5044f );
	_wallBox17.SetPosition( 86.7053f, 37.8160f );
	_wallBox18.SetPosition( 83.3993f, 39.2124f );
	_wallBox19.SetPosition( 92.2421f, 25.4388f );
	_wallBox20.SetPosition( 79.1182f, 15.6569f );
	_wallBox21.SetPosition( 82.7043f, 15.8009f );
	_wallBox22.SetPosition( 34.0000f, 4.0500f );
	_woodBox1.SetPosition( 23.3000f, 54.5500f );
	_woodBox2.SetPosition( 41.0000f, 40.6500f );
	_woodBox3.SetPosition( 20.4000f, 28.7500f );
	_woodBox4.SetPosition( 16.7000f, 41.3500f );
	_woodBox5.SetPosition( 21.4000f, 16.9500f );
	_woodBox6.SetPosition( 45.5000f, 16.9500f );

	//Bombs Construction
	_bomb1.SetRange( 16.0000f );
	_bomb1.SetBombImpulse( 6000.0000f );

	ConstFinal();
}
//--------------------------------------------------------------------------
LevelExplosivePath::LevelExplosivePath( GameTypes::LevelEnum levelEnum, bool viewMode ) : Level( levelEnum, viewMode )
{
    _levelTag = eLevelTag_Bombs;
    
	// _coin1
	_coin1.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin1.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin1.SetBodyType( b2_dynamicBody );

	// _bombStatic1
	_bombStatic1.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_bombStatic1.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_bombStatic1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bombStatic1.SetBodyType( b2_staticBody );

	// _bombStatic2
	_bombStatic2.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_bombStatic2.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_bombStatic2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bombStatic2.SetBodyType( b2_staticBody );

	// _bombStatic3
	_bombStatic3.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_bombStatic3.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_bombStatic3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bombStatic3.SetBodyType( b2_staticBody );

	// _wallBox1
	_wallBox1.SetRotation( 225.0000f );
	_wallBox1.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox1.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox1.SetBodyType( b2_staticBody );

	// _wallBox2
	_wallBox2.SetRotation( 180.0000f );
	_wallBox2.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox2.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox2.SetBodyType( b2_staticBody );

	// _wallBox3
	_wallBox3.SetRotation( 135.0000f );
	_wallBox3.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox3.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox3.SetBodyType( b2_staticBody );

	// _wallBox4
	_wallBox4.SetRotation( 90.0000f );
	_wallBox4.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox4.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox4.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox4.SetBodyType( b2_staticBody );

	// _wallBox5
	_wallBox5.SetRotation( 270.0000f );
	_wallBox5.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox5.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox5.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox5.SetBodyType( b2_staticBody );

	// _wallBox6
	_wallBox6.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox6.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox6.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox6.SetBodyType( b2_staticBody );

	// _bombStatic4
	_bombStatic4.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_bombStatic4.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_bombStatic4.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bombStatic4.SetBodyType( b2_staticBody );

	// _wallBox7
	_wallBox7.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox7.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox7.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox7.SetBodyType( b2_staticBody );

	// _wallBox8
	_wallBox8.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox8.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox8.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox8.SetBodyType( b2_staticBody );

	// _bombStatic5
	_bombStatic5.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_bombStatic5.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_bombStatic5.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bombStatic5.SetBodyType( b2_staticBody );

	// _wallBox9
	_wallBox9.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox9.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox9.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox9.SetBodyType( b2_staticBody );

	// _bombStatic6
	_bombStatic6.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_bombStatic6.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_bombStatic6.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bombStatic6.SetBodyType( b2_staticBody );

	// _wallBox10
	_wallBox10.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox10.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox10.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox10.SetBodyType( b2_staticBody );

	// _piggyBankStatic1
	_piggyBankStatic1.GetBody()->GetFixtureList()->SetDensity( 7.5000f );
	_piggyBankStatic1.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_piggyBankStatic1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_piggyBankStatic1.SetBodyType( b2_staticBody );

	// _wallBox11
	_wallBox11.SetRotation( 90.0000f );
	_wallBox11.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox11.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox11.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox11.SetBodyType( b2_staticBody );

	// _wallBox12
	_wallBox12.SetRotation( 45.0000f );
	_wallBox12.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox12.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox12.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox12.SetBodyType( b2_staticBody );


	//Set blocks positions
	_coin1.SetPosition( 84.2502f, 19.1500f );
	_bombStatic1.SetPosition( 35.5003f, 34.8496f );
	_bombStatic2.SetPosition( 10.5000f, 8.0001f );
	_bombStatic3.SetPosition( 85.5002f, 8.0001f );
	_wallBox1.SetPosition( 14.2125f, 15.4377f );
	_wallBox2.SetPosition( 10.5000f, 13.9000f );
	_wallBox3.SetPosition( 6.7877f, 15.4377f );
	_wallBox4.SetPosition( 5.2501f, 19.1500f );
	_wallBox5.SetPosition( 15.6001f, 19.1500f );
	_wallBox6.SetPosition( 35.5003f, 40.7489f );
	_bombStatic4.SetPosition( 60.5001f, 34.8496f );
	_wallBox7.SetPosition( 60.5001f, 40.7489f );
	_wallBox8.SetPosition( 35.5003f, 13.8994f );
	_bombStatic5.SetPosition( 35.5003f, 8.0001f );
	_wallBox9.SetPosition( 60.5001f, 13.8994f );
	_bombStatic6.SetPosition( 60.5001f, 8.0001f );
	_wallBox10.SetPosition( 85.5017f, 13.8994f );
	_piggyBankStatic1.SetPosition( 85.5002f, 51.9000f );
	_wallBox11.SetPosition( 5.2501f, 24.1499f );
	_wallBox12.SetPosition( 8.1001f, 54.7002f );

	//Bombs Construction
	_bombStatic1.SetRange( 16.0000f );
	_bombStatic1.SetBombImpulse( 6000.0000f );
	_bombStatic2.SetRange( 16.5000f );
	_bombStatic2.SetBombImpulse( 19700.0000f );
	_bombStatic3.SetRange( 16.0000f );
	_bombStatic3.SetBombImpulse( 9550.0000f );
	_bombStatic4.SetRange( 16.0000f );
	_bombStatic4.SetBombImpulse( 6000.0000f );
	_bombStatic5.SetRange( 16.0000f );
	_bombStatic5.SetBombImpulse( 6500.0000f );
	_bombStatic6.SetRange( 16.0000f );
	_bombStatic6.SetBombImpulse( 6000.0000f );

	ConstFinal();
}
//--------------------------------------------------------------------------
LevelBouncingCoin_1::LevelBouncingCoin_1( GameTypes::LevelEnum levelEnum, bool viewMode ) : Level( levelEnum, viewMode )
{
	// _coin1
	_coin1.GetBody()->GetFixtureList()->SetDensity( 2.5000f );
	_coin1.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_coin1.GetBody()->GetFixtureList()->SetRestitution( 0.9800f );
	_coin1.SetBodyType( b2_dynamicBody );

	// _wallBox1
	_wallBox1.SetRotation( -17.1000f );
	_wallBox1.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox1.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox1.SetBodyType( b2_staticBody );

	// _wallBox2
	_wallBox2.SetRotation( 234.9000f );
	_wallBox2.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox2.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox2.SetBodyType( b2_staticBody );

	// _wallBox3
	_wallBox3.SetRotation( 324.9004f );
	_wallBox3.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox3.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox3.SetBodyType( b2_staticBody );

	// _wallBox4
	_wallBox4.SetRotation( 306.9004f );
	_wallBox4.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox4.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox4.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox4.SetBodyType( b2_staticBody );

	// _wallBox5
	_wallBox5.SetRotation( 198.9004f );
	_wallBox5.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox5.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox5.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox5.SetBodyType( b2_staticBody );

	// _wallBox6
	_wallBox6.SetRotation( 180.9004f );
	_wallBox6.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox6.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox6.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox6.SetBodyType( b2_staticBody );

	// _wallBox7
	_wallBox7.SetRotation( 162.9004f );
	_wallBox7.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox7.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox7.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox7.SetBodyType( b2_staticBody );

	// _wallBox8
	_wallBox8.SetRotation( 144.9004f );
	_wallBox8.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox8.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox8.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox8.SetBodyType( b2_staticBody );

	// _wallBox9
	_wallBox9.SetRotation( 90.9000f );
	_wallBox9.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox9.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox9.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox9.SetBodyType( b2_staticBody );

	// _wallBox10
	_wallBox10.SetRotation( 54.9000f );
	_wallBox10.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox10.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox10.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox10.SetBodyType( b2_staticBody );

	// _wallBox11
	_wallBox11.SetRotation( 36.9000f );
	_wallBox11.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox11.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox11.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox11.SetBodyType( b2_staticBody );

	// _wallBox12
	_wallBox12.SetRotation( 216.9002f );
	_wallBox12.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox12.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox12.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox12.SetBodyType( b2_staticBody );

	// _wallBox13
	_wallBox13.SetRotation( 126.9004f );
	_wallBox13.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox13.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox13.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox13.SetBodyType( b2_staticBody );

	// _wallBox14
	_wallBox14.SetRotation( 108.9000f );
	_wallBox14.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox14.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox14.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox14.SetBodyType( b2_staticBody );

	// _wallBox15
	_wallBox15.SetRotation( 72.9000f );
	_wallBox15.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox15.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox15.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox15.SetBodyType( b2_staticBody );

	// _wallBox16
	_wallBox16.SetRotation( 18.9000f );
	_wallBox16.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox16.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox16.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox16.SetBodyType( b2_staticBody );

	// _wallBox17
	_wallBox17.SetRotation( 0.9000f );
	_wallBox17.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox17.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox17.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox17.SetBodyType( b2_staticBody );

	// _floor21
	_floor21.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_floor21.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_floor21.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_floor21.SetBodyType( b2_staticBody );

	// _fragileBoxStatic1
	_fragileBoxStatic1.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBoxStatic1.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBoxStatic1.GetBody()->GetFixtureList()->SetRestitution( 0.0000f );
	_fragileBoxStatic1.SetBodyType( b2_staticBody );

	// _fragileBoxStatic2
	_fragileBoxStatic2.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBoxStatic2.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBoxStatic2.GetBody()->GetFixtureList()->SetRestitution( 0.0000f );
	_fragileBoxStatic2.SetBodyType( b2_staticBody );

	// _fragileBoxStatic3
	_fragileBoxStatic3.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBoxStatic3.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBoxStatic3.GetBody()->GetFixtureList()->SetRestitution( 0.0000f );
	_fragileBoxStatic3.SetBodyType( b2_staticBody );

	// _fragileBoxStatic4
	_fragileBoxStatic4.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBoxStatic4.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBoxStatic4.GetBody()->GetFixtureList()->SetRestitution( 0.0000f );
	_fragileBoxStatic4.SetBodyType( b2_staticBody );

	// _fragileBoxStatic5
	_fragileBoxStatic5.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBoxStatic5.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBoxStatic5.GetBody()->GetFixtureList()->SetRestitution( 0.0000f );
	_fragileBoxStatic5.SetBodyType( b2_staticBody );

	// _fragileBoxStatic6
	_fragileBoxStatic6.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBoxStatic6.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBoxStatic6.GetBody()->GetFixtureList()->SetRestitution( 0.0000f );
	_fragileBoxStatic6.SetBodyType( b2_staticBody );

	// _fragileBoxStatic7
	_fragileBoxStatic7.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBoxStatic7.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBoxStatic7.GetBody()->GetFixtureList()->SetRestitution( 0.0000f );
	_fragileBoxStatic7.SetBodyType( b2_staticBody );

	// _fragileBoxStatic8
	_fragileBoxStatic8.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBoxStatic8.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBoxStatic8.GetBody()->GetFixtureList()->SetRestitution( 0.0000f );
	_fragileBoxStatic8.SetBodyType( b2_staticBody );

	// _fragileBoxStatic9
	_fragileBoxStatic9.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBoxStatic9.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBoxStatic9.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBoxStatic9.SetBodyType( b2_staticBody );

	// _fragileBoxStatic10
	_fragileBoxStatic10.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBoxStatic10.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBoxStatic10.GetBody()->GetFixtureList()->SetRestitution( 0.0000f );
	_fragileBoxStatic10.SetBodyType( b2_staticBody );

	// _fragileBoxStatic11
	_fragileBoxStatic11.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBoxStatic11.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_fragileBoxStatic11.GetBody()->GetFixtureList()->SetRestitution( 0.0000f );
	_fragileBoxStatic11.SetBodyType( b2_staticBody );

	// _fragileBoxStatic12
	_fragileBoxStatic12.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBoxStatic12.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_fragileBoxStatic12.GetBody()->GetFixtureList()->SetRestitution( 1.0000f );
	_fragileBoxStatic12.SetBodyType( b2_staticBody );

	// _fragileBoxStatic13
	_fragileBoxStatic13.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBoxStatic13.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBoxStatic13.GetBody()->GetFixtureList()->SetRestitution( 1.0000f );
	_fragileBoxStatic13.SetBodyType( b2_staticBody );

	// _fragileBoxStatic14
	_fragileBoxStatic14.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBoxStatic14.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBoxStatic14.GetBody()->GetFixtureList()->SetRestitution( 0.0000f );
	_fragileBoxStatic14.SetBodyType( b2_staticBody );

	// _fragileBoxStatic15
	_fragileBoxStatic15.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBoxStatic15.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBoxStatic15.GetBody()->GetFixtureList()->SetRestitution( 0.0000f );
	_fragileBoxStatic15.SetBodyType( b2_staticBody );

	// _fragileBoxStatic16
	_fragileBoxStatic16.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBoxStatic16.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBoxStatic16.GetBody()->GetFixtureList()->SetRestitution( 0.7500f );
	_fragileBoxStatic16.SetBodyType( b2_staticBody );

	// _fragileBoxStatic17
	_fragileBoxStatic17.SetRotation( 23.1000f );
	_fragileBoxStatic17.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBoxStatic17.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBoxStatic17.GetBody()->GetFixtureList()->SetRestitution( 0.0000f );
	_fragileBoxStatic17.SetBodyType( b2_staticBody );

	// _piggyBank1
	_piggyBank1.GetBody()->GetFixtureList()->SetDensity( 7.5000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_piggyBank1.SetBodyType( b2_dynamicBody );

	// _wallBox18
	_wallBox18.SetRotation( -69.6000f );
	_wallBox18.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox18.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox18.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox18.SetBodyType( b2_staticBody );


	//Set blocks positions
	_coin1.SetPosition( 2.3500f, 30.1000f );
	_wallBox1.SetPosition( 35.8877f, 9.2602f );
	_wallBox2.SetPosition( 45.0725f, 12.4050f );
	_wallBox3.SetPosition( 36.7136f, 10.9461f );
	_wallBox4.SetPosition( 38.0199f, 12.2942f );
	_wallBox5.SetPosition( 47.2988f, 9.4396f );
	_wallBox6.SetPosition( 47.6216f, 7.5903f );
	_wallBox7.SetPosition( 47.3571f, 5.7317f );
	_wallBox8.SetPosition( 46.5312f, 4.0459f );
	_wallBox9.SetPosition( 41.7165f, 1.4966f );
	_wallBox10.SetPosition( 38.1723f, 2.5870f );
	_wallBox11.SetPosition( 36.8242f, 3.8933f );
	_wallBox12.SetPosition( 46.4205f, 11.0987f );
	_wallBox13.SetPosition( 45.2248f, 2.6978f );
	_wallBox14.SetPosition( 43.5659f, 1.8194f );
	_wallBox15.SetPosition( 39.8580f, 1.7611f );
	_wallBox16.SetPosition( 35.9459f, 5.5524f );
	_wallBox17.SetPosition( 35.6232f, 7.4017f );
	_floor21.SetPosition( 12.2000f, 0.5000f );
	_fragileBoxStatic1.SetPosition( 7.2500f, 8.3000f );
	_fragileBoxStatic2.SetPosition( 7.2500f, 13.1500f );
	_fragileBoxStatic3.SetPosition( 15.2500f, 13.1500f );
	_fragileBoxStatic4.SetPosition( 7.2500f, 18.0000f );
	_fragileBoxStatic5.SetPosition( 15.2500f, 18.0000f );
	_fragileBoxStatic6.SetPosition( 24.3000f, 13.1000f );
	_fragileBoxStatic7.SetPosition( 24.3000f, 17.9500f );
	_fragileBoxStatic8.SetPosition( 32.5000f, 17.9500f );
	_fragileBoxStatic9.SetPosition( 7.2500f, 3.4500f );
	_fragileBoxStatic10.SetPosition( 24.3000f, 3.4499f );
	_fragileBoxStatic11.SetPosition( 15.2500f, 3.4500f );
	_fragileBoxStatic12.SetPosition( 15.2500f, 8.3001f );
	_fragileBoxStatic13.SetPosition( 24.3000f, 8.2500f );
	_fragileBoxStatic14.SetPosition( 32.5000f, 3.4499f );
	_fragileBoxStatic15.SetPosition( 32.5000f, 8.2500f );
	_fragileBoxStatic16.SetPosition( 32.5000f, 13.1001f );
	_fragileBoxStatic17.SetPosition( 4.3500f, 26.1500f );
	_piggyBank1.SetPosition( 41.5500f, 7.9001f );
	_wallBox18.SetPosition( 0.6000f, 27.9500f );

	ConstFinal();
	AdjustOldScreen();
}
//--------------------------------------------------------------------------
LevelScale_2::LevelScale_2( GameTypes::LevelEnum levelEnum, bool viewMode ) : Level( levelEnum, viewMode )
{
    _levelTag = eLevelTag_Frigle;
    
	// _circleWall1
	_circleWall1.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_circleWall1.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_circleWall1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_circleWall1.SetBodyType( b2_staticBody );

	// _coin1
	_coin1.GetBody()->GetFixtureList()->SetDensity( 1.4500f );
	_coin1.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin1.GetBody()->SetAngularDamping(0.2000f );
	_coin1.SetBodyType( b2_dynamicBody );

	// _fragileBox1
	_fragileBox1.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox1.GetBody()->GetFixtureList()->SetFriction( 1.0000f );
	_fragileBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox1.SetBodyType( b2_dynamicBody );

	// _fragileBox2
	_fragileBox2.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox2.GetBody()->GetFixtureList()->SetFriction( 1.0000f );
	_fragileBox2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox2.SetBodyType( b2_dynamicBody );

	// _fragileBox3
	_fragileBox3.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox3.GetBody()->GetFixtureList()->SetFriction( 1.0000f );
	_fragileBox3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox3.SetBodyType( b2_dynamicBody );

	// _fragileBox4
	_fragileBox4.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox4.GetBody()->GetFixtureList()->SetFriction( 1.0000f );
	_fragileBox4.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox4.SetBodyType( b2_dynamicBody );

	// _fragileBoxStatic1
	_fragileBoxStatic1.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBoxStatic1.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBoxStatic1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBoxStatic1.SetBodyType( b2_staticBody );

	// _fragileBoxStatic2
	_fragileBoxStatic2.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBoxStatic2.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBoxStatic2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBoxStatic2.SetBodyType( b2_staticBody );

	// _fragileBoxStatic3
	_fragileBoxStatic3.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBoxStatic3.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBoxStatic3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBoxStatic3.SetBodyType( b2_staticBody );

	// _fragileBoxStatic4
	_fragileBoxStatic4.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBoxStatic4.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBoxStatic4.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBoxStatic4.SetBodyType( b2_staticBody );

	// _fragileBoxStatic5
	_fragileBoxStatic5.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBoxStatic5.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBoxStatic5.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBoxStatic5.SetBodyType( b2_staticBody );

	// _fragileBoxStatic6
	_fragileBoxStatic6.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBoxStatic6.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBoxStatic6.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBoxStatic6.SetBodyType( b2_staticBody );

	// _piggyBank1
	_piggyBank1.GetBody()->GetFixtureList()->SetDensity( 7.5000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_piggyBank1.GetBody()->SetAngularDamping(0.4000f );
	_piggyBank1.GetBody()->SetLinearDamping(0.4000f );
	_piggyBank1.FlipX();
	_piggyBank1.SetBodyType( b2_dynamicBody );

	// _woodBox1
	_woodBox1.GetBody()->GetFixtureList()->SetDensity( 1.6500f );
	_woodBox1.GetBody()->GetFixtureList()->SetFriction( 0.5500f );
	_woodBox1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_woodBox1.SetBodyType( b2_dynamicBody );

	// _wallBox1
	_wallBox1.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox1.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox1.SetBodyType( b2_staticBody );


	//Set blocks positions
	_circleWall1.SetPosition( 64.0001f, 11.0000f );
	_coin1.SetPosition( 64.0001f, 21.3002f );
	_fragileBox1.SetPosition( 44.2002f, 53.9001f );
	_fragileBox2.SetPosition( 53.9002f, 53.9001f );
	_fragileBox3.SetPosition( 83.6002f, 53.9001f );
	_fragileBox4.SetPosition( 73.9004f, 53.9001f );
	_fragileBoxStatic1.SetPosition( 83.6006f, 10.4000f );
	_fragileBoxStatic2.SetPosition( 44.4001f, 10.4000f );
	_fragileBoxStatic3.SetPosition( 53.9002f, 43.9999f );
	_fragileBoxStatic4.SetPosition( 44.2002f, 43.9999f );
	_fragileBoxStatic5.SetPosition( 73.9004f, 43.9999f );
	_fragileBoxStatic6.SetPosition( 83.6002f, 43.9999f );
	_piggyBank1.SetPosition( 13.2000f, 51.2000f );
	_woodBox1.SetPosition( 64.0001f, 16.2000f );
	_wallBox1.SetPosition( 13.2000f, 42.7000f );

	ConstFinal();
}
//--------------------------------------------------------------------------
LevelSpiderWeb::LevelSpiderWeb( GameTypes::LevelEnum levelEnum, bool viewMode ) : Level( levelEnum, viewMode )
{
	// _coin1
	_coin1.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin1.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin1.SetBodyType( b2_dynamicBody );

	// _chain1
	_chain1.SetChain( 2.0f * 17.0000f);
	_chain1.SetRotation( 180.2999f );
	_chain1.FixHook( false );

	// _chain2
	_chain2.SetChain( 2.0f * 9.7500f);
	_chain2.SetRotation( 270.0000f );
	_chain2.FixHook( false );

	// _chain3
	_chain3.SetChain( 2.0f * 21.2500f);
	_chain3.SetRotation( 365.7002f );
	_chain3.FixHook( false );

	// _chain4
	_chain4.SetChain( 2.0f * 21.2500f);
	_chain4.SetRotation( 211.8001f );
	_chain4.FixHook( false );

	// _wallBox1
	_wallBox1.SetRotation( -90.0000f );
	_wallBox1.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox1.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox1.SetBodyType( b2_staticBody );

	// _wallBox2
	_wallBox2.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox2.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox2.SetBodyType( b2_staticBody );

	// _wallBox3
	_wallBox3.SetRotation( -269.9998f );
	_wallBox3.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox3.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox3.SetBodyType( b2_staticBody );

	// _wallBox4
	_wallBox4.SetRotation( 0.3001f );
	_wallBox4.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox4.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox4.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox4.SetBodyType( b2_staticBody );

	// _chain5
	_chain5.SetChain( 2.0f * 14.0000f);
	_chain5.SetRotation( 107.6999f );
	_chain5.FixHook( false );

	// _wallBox5
	_wallBox5.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox5.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox5.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox5.SetBodyType( b2_staticBody );

	// _wallBox6
	_wallBox6.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox6.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox6.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox6.SetBodyType( b2_staticBody );

	// _chain6
	_chain6.SetChain( 2.0f * 15.7500f);
	_chain6.SetRotation( 157.5000f );
	_chain6.FixHook( false );

	// _chain7
	_chain7.SetChain( 2.0f * 15.7500f);
	_chain7.SetRotation( 34.8000f );
	_chain7.FixHook( false );

	// _piggyBank1
	_piggyBank1.GetBody()->GetFixtureList()->SetDensity( 7.5000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_piggyBank1.SetBodyType( b2_staticBody );

	// _chain8
	_chain8.SetChain( 2.0f * 17.0000f);
	_chain8.SetRotation( 312.6000f );
	_chain8.FixHook( false );

	// _chain9
	_chain9.SetChain( 2.0f * 9.7500f);
	_chain9.SetRotation( 71.9999f );
	_chain9.FixHook( false );

	// _chain10
	_chain10.SetChain( 2.0f * 9.7500f);
	_chain10.SetRotation( -10.5001f );
	_chain10.FixHook( false );


	//Set blocks positions
	_coin1.SetPosition( 20.5500f, 20.1000f );
	_chain1.SetPosition( 38.9000f, 20.2000f );
	_chain2.SetPosition( 20.7000f, 30.4500f );
	_chain3.SetPosition( -1.5500f, 17.7000f );
	_chain4.SetPosition( 39.3997f, 32.1000f );
	_wallBox1.SetPosition( 33.9498f, 10.0000f );
	_wallBox2.SetPosition( 26.3000f, 7.0500f );
	_wallBox3.SetPosition( 39.6997f, 10.1000f );
	_wallBox4.SetPosition( 36.8500f, 8.0000f );
	_chain5.SetPosition( 25.0500f, 5.4000f );
	_wallBox5.SetPosition( 43.4000f, 2.2500f );
	_wallBox6.SetPosition( 38.6500f, 12.9000f );
	_chain6.SetPosition( 35.4000f, 13.3500f );
	_chain7.SetPosition( 7.0500f, 10.8000f );
	_piggyBank1.SetPosition( 44.0500f, 5.6500f );
	_chain8.SetPosition( 7.8500f, 33.4000f );
	_chain9.SetPosition( 17.2500f, 10.2500f );
	_chain10.SetPosition( 10.3500f, 22.1000f );


	ConstFinal();
	AdjustOldScreen();

	//Set chains hooks
	_chain1.HookOnChain( &_coin1 );
	_chain2.HookOnChain( &_coin1 );
	_chain3.HookOnChain( &_coin1 );
	_chain4.HookOnChain( &_coin1 );
	_chain5.HookOnChain( &_coin1 );
	_chain6.HookOnChain( &_coin1 );
	_chain7.HookOnChain( &_coin1 );
	_chain8.HookOnChain( &_coin1 );
	_chain9.HookOnChain( &_coin1 );
	_chain10.HookOnChain( &_coin1 );
}
//--------------------------------------------------------------------------
