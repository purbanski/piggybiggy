#include "LevelVehical.h"
#include "Tools.h"
#include "Platform/Lang.h"

//----------------------------------------------------------------------------------------------
LevelVehicalBase::LevelVehicalBase( GameTypes::LevelEnum levelEnum, bool viewMode ) : Level( levelEnum, viewMode )
{
    _levelTag = eLevelTag_Car;
    _levelType = eLevelTypeCar;
	//_controls.SetVehicle( &_vehical );
}
//----------------------------------------------------------------------------------------------
LevelVehicalBase::~LevelVehicalBase()
{
}
//----------------------------------------------------------------------------------------------
void LevelVehicalBase::Step( cocos2d::ccTime dt )
{
	Level::Step( dt );
	_vehical.Update();
}


//----------------------------------------------------------------------------------------------
// LevelVehical_1
//----------------------------------------------------------------------------------------------
LevelVehical_1::LevelVehical_1( GameTypes::LevelEnum levelEnum, bool viewMode ) : LevelVehicalBase( levelEnum, viewMode )
{
	b2Filter filter;
	_coinCount = 2;

	// _coin1
	_coin1.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin1.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin1.SetBodyType( b2_dynamicBody );

	// _coin2
	_coin2.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin2.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin2.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin2.SetBodyType( b2_dynamicBody );

	// _screw1
	_screw1.GetBody()->GetFixtureList()->SetDensity( 0.1000f );
	_screw1.GetBody()->GetFixtureList()->SetFriction( 0.1000f );
	_screw1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_screw1.GetBody()->GetFixtureList()->SetFilterData( filter );
	_screw1.SetBodyType( b2_staticBody );

	// _screw2
	_screw2.GetBody()->GetFixtureList()->SetDensity( 0.1000f );
	_screw2.GetBody()->GetFixtureList()->SetFriction( 0.1000f );
	_screw2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_screw2.GetBody()->GetFixtureList()->SetFilterData( filter );
	_screw2.SetBodyType( b2_staticBody );

	// _piggyBank1
	_piggyBank1.GetBody()->GetFixtureList()->SetDensity( 7.5000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_piggyBank1.SetBodyType( b2_dynamicBody );

	// _wallBox1
	_wallBox1.SetRotation( 25.0000f );
	_wallBox1.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox1.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox1.SetBodyType( b2_staticBody );

	// _wallBox2
	_wallBox2.SetRotation( 50.0000f );
	_wallBox2.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox2.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox2.SetBodyType( b2_staticBody );

	// _wallBox3
	_wallBox3.SetRotation( 50.0000f );
	_wallBox3.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox3.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox3.SetBodyType( b2_staticBody );

	// _wallBox4
	_wallBox4.SetRotation( 25.0000f );
	_wallBox4.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox4.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox4.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox4.SetBodyType( b2_staticBody );

	// _wallBox5
	_wallBox5.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox5.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox5.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox5.SetBodyType( b2_staticBody );

	// _wallBox6
	_wallBox6.SetRotation( 50.0000f );
	_wallBox6.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox6.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox6.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox6.SetBodyType( b2_staticBody );

	// _wallBox7
	_wallBox7.SetRotation( 50.0000f );
	_wallBox7.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox7.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox7.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox7.SetBodyType( b2_staticBody );

	// _wallBox8
	_wallBox8.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox8.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox8.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	filter.groupIndex = -4;
	filter.categoryBits = 1;
	filter.maskBits = 65535;
	_wallBox8.GetBody()->GetFixtureList()->SetFilterData( filter );
	_wallBox8.SetBodyType( b2_staticBody );

	// _woodBox1
	_woodBox1.SetRotation( -25.0000f );
	_woodBox1.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox1.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_woodBox1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	filter.groupIndex = -4;
	filter.categoryBits = 1;
	filter.maskBits = 65535;
	_woodBox1.GetBody()->GetFixtureList()->SetFilterData( filter );
	_woodBox1.SetBodyType( b2_dynamicBody );

	// _woodBox2
	_woodBox2.SetRotation( -50.0000f );
	_woodBox2.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox2.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_woodBox2.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	filter.groupIndex = -4;
	filter.categoryBits = 1;
	filter.maskBits = 65535;
	_woodBox2.GetBody()->GetFixtureList()->SetFilterData( filter );
	_woodBox2.SetBodyType( b2_dynamicBody );

	// _wallBox9
	_wallBox9.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox9.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox9.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	filter.groupIndex = -4;
	filter.categoryBits = 1;
	filter.maskBits = 65535;
	_wallBox9.GetBody()->GetFixtureList()->SetFilterData( filter );
	_wallBox9.SetBodyType( b2_staticBody );

	// _wallBox10
	_wallBox10.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox10.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox10.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	filter.groupIndex = -4;
	filter.categoryBits = 1;
	filter.maskBits = 65535;
	_wallBox10.GetBody()->GetFixtureList()->SetFilterData( filter );
	_wallBox10.SetBodyType( b2_staticBody );


	//Set blocks positions
	_vehical.SetPosition( 27.9498f, 7.3001f );
	_coin1.SetPosition( 71.4502f, 48.2500f );
	_coin2.SetPosition( 71.4501f, 27.9000f );
	_screw1.SetPosition( 61.2497f, 19.8500f );
	_screw2.SetPosition( 50.3500f, 28.2001f );
	_piggyBank1.SetPosition( 82.9502f, 27.9000f );
	_wallBox1.SetPosition( 42.9498f, 5.8000f );
	_wallBox2.SetPosition( 49.0498f, 10.5000f );
	_wallBox3.SetPosition( 36.8998f, 35.4000f );
	_wallBox4.SetPosition( 30.7998f, 30.6999f );
	_wallBox5.SetPosition( 75.5001f, 40.4000f );
	_wallBox6.SetPosition( 91.4999f, 43.7000f );
	_wallBox7.SetPosition( 91.5002f, 23.0499f );
	_wallBox8.SetPosition( 75.5004f, 19.7499f );
	_woodBox1.SetPosition( 58.4000f, 21.1500f );
	_woodBox2.SetPosition( 52.3000f, 25.8000f );
	_wallBox9.SetPosition( 26.2502f, 4.1999f );
	_wallBox10.SetPosition( 14.1503f, 29.0999f );

	ConstFinal();
	CreateJoints();
}
//----------------------------------------------------------------------------------------------
void LevelVehical_1::CreateJoints()
{
	b2RevoluteJointDef jointRevDef1;
	jointRevDef1.collideConnected = false;
	jointRevDef1.Initialize( _screw2.GetBody(), _woodBox2.GetBody(), _screw2.GetPosition() );
	_jointRev1 = (b2RevoluteJoint *) _world.CreateJoint( &jointRevDef1 );

	_jointRev1->SetLimits( 0.0000f, 2.4000f );
	_jointRev1->EnableLimit( true );
	_jointRev1->EnableMotor( true );
	_jointRev1->SetMotorSpeed( -0.5000f );
	_jointRev1->SetMaxMotorTorque( 400.0000f );

	b2RevoluteJointDef jointRevDef2;
	jointRevDef2.collideConnected = false;
	jointRevDef2.Initialize( _screw1.GetBody(), _woodBox1.GetBody(), _screw1.GetPosition() );
	_jointRev2 = (b2RevoluteJoint *) _world.CreateJoint( &jointRevDef2 );

	_jointRev2->SetLimits( -2.7000f, 0.0000f );
	_jointRev2->EnableLimit( true );
	_jointRev2->EnableMotor( true );
	_jointRev2->SetMotorSpeed( 2.9000f );
	_jointRev2->SetMaxMotorTorque( 3400.0000f );
}



//----------------------------------------------------------------------------------------------
// LevelVehical_2
//----------------------------------------------------------------------------------------------
LevelVehical_2::LevelVehical_2( GameTypes::LevelEnum levelEnum, bool viewMode ) : LevelVehicalBase( levelEnum, viewMode )
{
	_coinCount = 2;

	// _wallBox1
	_wallBox1.SetRotation( 180.5825f );
	_wallBox1.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox1.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox1.SetBodyType( b2_staticBody );

	// _wallBox2
	_wallBox2.SetRotation( 159.4060f );
	_wallBox2.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox2.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox2.SetBodyType( b2_staticBody );

	// _wallBox3
	_wallBox3.SetRotation( 138.2296f );
	_wallBox3.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox3.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox3.SetBodyType( b2_staticBody );

	// _wallBox4
	_wallBox4.SetRotation( 109.5000f );
	_wallBox4.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox4.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox4.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox4.SetBodyType( b2_staticBody );

	// _wallBox5
	_wallBox5.SetRotation( 120.7500f );
	_wallBox5.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox5.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox5.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox5.SetBodyType( b2_staticBody );

	// _wallBox6
	_wallBox6.SetRotation( 117.0530f );
	_wallBox6.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox6.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox6.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox6.SetBodyType( b2_staticBody );

	// _wallBox7
	_wallBox7.SetRotation( 64.5000f );
	_wallBox7.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox7.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox7.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox7.SetBodyType( b2_staticBody );

	// _wallBox8
	_wallBox8.SetRotation( 53.2500f );
	_wallBox8.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox8.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox8.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox8.SetBodyType( b2_staticBody );

	// _wallBox9
	_wallBox9.SetRotation( 42.0000f );
	_wallBox9.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox9.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox9.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox9.SetBodyType( b2_staticBody );

	// _wallBox10
	_wallBox10.SetRotation( 75.7500f );
	_wallBox10.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox10.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox10.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox10.SetBodyType( b2_staticBody );

	// _wallBox11
	_wallBox11.SetRotation( 87.0000f );
	_wallBox11.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox11.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox11.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox11.SetBodyType( b2_staticBody );

	// _wallBox12
	_wallBox12.SetRotation( 98.2501f );
	_wallBox12.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox12.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox12.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox12.SetBodyType( b2_staticBody );

	// _wallBox13
	_wallBox13.SetRotation( 90.0000f );
	_wallBox13.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox13.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox13.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox13.SetBodyType( b2_staticBody );

	// _wallBox14
	_wallBox14.SetRotation( 110.0000f );
	_wallBox14.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox14.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox14.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox14.SetBodyType( b2_staticBody );

	// _wallBox15
	_wallBox15.SetRotation( 130.0000f );
	_wallBox15.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox15.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox15.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox15.SetBodyType( b2_staticBody );

	// _wallBox16
	_wallBox16.SetRotation( 150.0000f );
	_wallBox16.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox16.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox16.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox16.SetBodyType( b2_staticBody );

	// _wallBox17
	_wallBox17.SetRotation( 170.0000f );
	_wallBox17.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox17.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox17.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox17.SetBodyType( b2_staticBody );

	// _wallBox18
	_wallBox18.SetRotation( 190.0000f );
	_wallBox18.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox18.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox18.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox18.SetBodyType( b2_staticBody );

	// _wallBox19
	_wallBox19.SetRotation( 210.0000f );
	_wallBox19.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox19.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox19.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox19.SetBodyType( b2_staticBody );

	// _wallBox20
	_wallBox20.SetRotation( 230.0000f );
	_wallBox20.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox20.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox20.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox20.SetBodyType( b2_staticBody );

	// _wallBox21
	_wallBox21.SetRotation( 250.0000f );
	_wallBox21.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox21.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox21.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox21.SetBodyType( b2_staticBody );

	// _wallBox22
	_wallBox22.SetRotation( 270.0000f );
	_wallBox22.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox22.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox22.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox22.SetBodyType( b2_staticBody );

	// _wallBox23
	_wallBox23.SetRotation( 290.0000f );
	_wallBox23.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox23.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox23.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox23.SetBodyType( b2_staticBody );

	// _wallBox24
	_wallBox24.SetRotation( 310.0000f );
	_wallBox24.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox24.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox24.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox24.SetBodyType( b2_staticBody );

	// _wallBox25
	_wallBox25.SetRotation( 410.0000f );
	_wallBox25.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox25.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox25.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox25.SetBodyType( b2_staticBody );

	// _wallBox26
	_wallBox26.SetRotation( 430.0000f );
	_wallBox26.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox26.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox26.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox26.SetBodyType( b2_staticBody );

	// _wallBox27
	_wallBox27.SetRotation( 230.0000f );
	_wallBox27.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox27.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox27.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox27.SetBodyType( b2_staticBody );

	// _wallBox28
	_wallBox28.SetRotation( 250.0000f );
	_wallBox28.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox28.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox28.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox28.SetBodyType( b2_staticBody );

	// _wallBox29
	_wallBox29.SetRotation( 150.0000f );
	_wallBox29.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox29.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox29.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox29.SetBodyType( b2_staticBody );

	// _wallBox30
	_wallBox30.SetRotation( 110.0000f );
	_wallBox30.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox30.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox30.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox30.SetBodyType( b2_staticBody );

	// _wallBox31
	_wallBox31.SetRotation( 90.0000f );
	_wallBox31.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox31.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox31.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox31.SetBodyType( b2_staticBody );

	// _wallBox32
	_wallBox32.SetRotation( 310.0000f );
	_wallBox32.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox32.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox32.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox32.SetBodyType( b2_staticBody );

	// _wallBox33
	_wallBox33.SetRotation( 430.0000f );
	_wallBox33.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox33.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox33.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox33.SetBodyType( b2_staticBody );

	// _wallBox34
	_wallBox34.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox34.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox34.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox34.SetBodyType( b2_staticBody );

	// _piggyBank1
	_piggyBank1.GetBody()->GetFixtureList()->SetDensity( 7.5000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetFriction( 0.1000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_piggyBank1.FlipX();
	_piggyBank1.SetBodyType( b2_staticBody );

	// _coin1
	_coin1.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin1.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin1.SetBodyType( b2_dynamicBody );

	// _wallBox35
	_wallBox35.SetRotation( 210.0000f );
	_wallBox35.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox35.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox35.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox35.SetBodyType( b2_staticBody );

	// _coin2
	_coin2.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin2.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin2.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin2.SetBodyType( b2_dynamicBody );

	// _wallBox36
	_wallBox36.SetRotation( 170.0000f );
	_wallBox36.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox36.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox36.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox36.SetBodyType( b2_staticBody );

	// _wallBox37
	_wallBox37.SetRotation( 190.0000f );
	_wallBox37.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox37.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox37.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox37.SetBodyType( b2_staticBody );

	// _wallBox38
	_wallBox38.SetRotation( 290.0000f );
	_wallBox38.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox38.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox38.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox38.SetBodyType( b2_staticBody );

	// _wallBox39
	_wallBox39.SetRotation( 270.0000f );
	_wallBox39.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox39.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox39.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox39.SetBodyType( b2_staticBody );

	// _piggyBank2
	_piggyBank2.GetBody()->GetFixtureList()->SetDensity( 7.5000f );
	_piggyBank2.GetBody()->GetFixtureList()->SetFriction( 0.1000f );
	_piggyBank2.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_piggyBank2.SetBodyType( b2_staticBody );

	// _wallBox40
	_wallBox40.SetRotation( 410.0000f );
	_wallBox40.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox40.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox40.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox40.SetBodyType( b2_staticBody );

	// _wallBox41
	_wallBox41.SetRotation( 130.0000f );
	_wallBox41.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox41.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox41.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox41.SetBodyType( b2_staticBody );


	//Set blocks positions
	_wallBox1.SetPosition( 22.7875f, 4.5715f );
	_wallBox2.SetPosition( 16.6350f, 5.6570f );
	_wallBox3.SetPosition( 11.2901f, 8.8918f );
	_wallBox4.SetPosition( 4.9787f, 18.9082f );
	_wallBox5.SetPosition( 7.4757f, 13.5837f );
	_wallBox6.SetPosition( 7.4747f, 13.8388f );
	_wallBox7.SetPosition( 6.1804f, 41.8378f );
	_wallBox8.SetPosition( 9.2203f, 46.8722f );
	_wallBox9.SetPosition( 13.0354f, 51.0831f );
	_wallBox10.SetPosition( 4.1810f, 36.3071f );
	_wallBox11.SetPosition( 3.2990f, 30.4925f );
	_wallBox12.SetPosition( 3.5684f, 24.6177f );
	_wallBox13.SetPosition( 14.7501f, 30.2000f );
	_wallBox14.SetPosition( 15.7904f, 24.3002f );
	_wallBox15.SetPosition( 18.7859f, 19.1119f );
	_wallBox16.SetPosition( 23.3752f, 15.2611f );
	_wallBox17.SetPosition( 29.0048f, 13.2121f );
	_wallBox18.SetPosition( 34.9957f, 13.2121f );
	_wallBox19.SetPosition( 40.6253f, 15.2611f );
	_wallBox20.SetPosition( 45.2147f, 19.1119f );
	_wallBox21.SetPosition( 48.2101f, 24.3002f );
	_wallBox22.SetPosition( 49.2504f, 30.2000f );
	_wallBox23.SetPosition( 48.2101f, 36.0999f );
	_wallBox24.SetPosition( 45.2147f, 41.2881f );
	_wallBox25.SetPosition( 18.7859f, 41.2881f );
	_wallBox26.SetPosition( 15.7904f, 36.0999f );
	_wallBox27.SetPosition( 88.6677f, 19.1119f );
	_wallBox28.SetPosition( 91.6631f, 24.3002f );
	_wallBox29.SetPosition( 66.8282f, 15.2611f );
	_wallBox30.SetPosition( 59.2432f, 24.3002f );
	_wallBox31.SetPosition( 58.2028f, 30.2000f );
	_wallBox32.SetPosition( 88.6676f, 41.2881f );
	_wallBox33.SetPosition( 59.2432f, 36.0999f );
	_wallBox34.SetPosition( 59.9501f, 4.5500f );
	_piggyBank1.SetPosition( 27.2999f, 30.4999f );
	_coin1.SetPosition( 31.8000f, 18.3000f );
	_vehical.SetPosition( 53.5502f, 9.4001f );
	_wallBox35.SetPosition( 84.0756f, 15.2611f );
	_coin2.SetPosition( 75.2502f, 18.3000f );
	_wallBox36.SetPosition( 72.4550f, 13.2121f );
	_wallBox37.SetPosition( 78.4460f, 13.2121f );
	_wallBox38.SetPosition( 91.6604f, 36.0999f );
	_wallBox39.SetPosition( 92.7007f, 30.2000f );
	_piggyBank2.SetPosition( 80.1508f, 30.5000f );
	_wallBox40.SetPosition( 62.2359f, 41.2881f );
	_wallBox41.SetPosition( 62.2359f, 19.1119f );

    
    float textSize;
    
    if (Lang::Get()->IsChinese())
        textSize = 42.0f;
    else
        textSize = 34.0f;
    
    _msgManager.push_back(eOTMsg_CarIntroA,
                          new LevelMsg_ArrowMsg( 260.0f, LocalString("CarIntro"), textSize,
                                                CCPoint( 195.0f, -70.0f), 3, true, 30.0f ));

    _msgManager.push_back(eOTMsg_CarIntroB,
                          new LevelMsg_ArrowMsg( 260.0f, LocalString("CarIntro"), textSize,
                                                CCPoint( -195.0f, -70.0f), 3, false, -32.0f ));

    
	ConstFinal();
}



//----------------------------------------------------------------------------------------------
// LevelVehical_3
//----------------------------------------------------------------------------------------------
LevelVehical_3::LevelVehical_3( GameTypes::LevelEnum levelEnum, bool viewMode ) : LevelVehicalBase( levelEnum, viewMode )
{
	// _piggyBank1
	_piggyBank1.GetBody()->GetFixtureList()->SetDensity( 7.5000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_piggyBank1.SetBodyType( b2_dynamicBody );

	// _wallBox1
	_wallBox1.SetRotation( 234.0004f );
	_wallBox1.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox1.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox1.SetBodyType( b2_staticBody );

	// _wallBox2
	_wallBox2.SetRotation( 216.0003f );
	_wallBox2.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox2.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox2.SetBodyType( b2_staticBody );

	// _wallBox3
	_wallBox3.SetRotation( 557.9990f );
	_wallBox3.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox3.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox3.SetBodyType( b2_staticBody );

	// _wallBox4
	_wallBox4.SetRotation( 468.0006f );
	_wallBox4.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox4.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox4.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox4.SetBodyType( b2_staticBody );

	// _wallBox5
	_wallBox5.SetRotation( 486.0005f );
	_wallBox5.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox5.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox5.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox5.SetBodyType( b2_staticBody );

	// _wallBox6
	_wallBox6.SetRotation( 504.0008f );
	_wallBox6.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox6.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox6.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox6.SetBodyType( b2_staticBody );

	// _wallBox7
	_wallBox7.SetRotation( 539.9998f );
	_wallBox7.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox7.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox7.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox7.SetBodyType( b2_staticBody );

	// _wallBox8
	_wallBox8.SetRotation( 522.0002f );
	_wallBox8.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox8.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox8.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox8.SetBodyType( b2_staticBody );

	// _wallBox9
	_wallBox9.SetRotation( 232.8005f );
	_wallBox9.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox9.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox9.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox9.SetBodyType( b2_staticBody );

	// _wallBox10
	_wallBox10.SetRotation( 484.8005f );
	_wallBox10.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox10.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox10.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox10.SetBodyType( b2_staticBody );

	// _wallBox11
	_wallBox11.SetRotation( 556.7991f );
	_wallBox11.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox11.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox11.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox11.SetBodyType( b2_staticBody );

	// _wallBox12
	_wallBox12.SetRotation( 538.7997f );
	_wallBox12.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox12.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox12.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox12.SetBodyType( b2_staticBody );

	// _wallBox13
	_wallBox13.SetRotation( 214.8004f );
	_wallBox13.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox13.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox13.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox13.SetBodyType( b2_staticBody );

	// _wallBox14
	_wallBox14.SetRotation( 520.8004f );
	_wallBox14.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox14.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox14.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox14.SetBodyType( b2_staticBody );

	// _wallBox15
	_wallBox15.SetRotation( 502.8009f );
	_wallBox15.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox15.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox15.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox15.SetBodyType( b2_staticBody );

	// _wallBox16
	_wallBox16.SetRotation( 434.4008f );
	_wallBox16.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox16.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox16.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox16.SetBodyType( b2_staticBody );

	// _wallBox17
	_wallBox17.SetRotation( 416.4002f );
	_wallBox17.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox17.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox17.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox17.SetBodyType( b2_staticBody );

	// _wallBox18
	_wallBox18.SetRotation( 452.4001f );
	_wallBox18.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox18.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox18.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox18.SetBodyType( b2_staticBody );

	// _wallBox19
	_wallBox19.SetRotation( 470.4004f );
	_wallBox19.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox19.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox19.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox19.SetBodyType( b2_staticBody );

	// _wallBox20
	_wallBox20.SetRotation( 344.4004f );
	_wallBox20.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox20.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox20.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox20.SetBodyType( b2_staticBody );

	// _wallBox21
	_wallBox21.SetRotation( 326.3998f );
	_wallBox21.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox21.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox21.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox21.SetBodyType( b2_staticBody );

	// _wallBox22
	_wallBox22.SetRotation( 362.4001f );
	_wallBox22.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox22.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox22.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox22.SetBodyType( b2_staticBody );

	// _wallBox23
	_wallBox23.SetRotation( 380.4012f );
	_wallBox23.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox23.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox23.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox23.SetBodyType( b2_staticBody );

	// _wallBox24
	_wallBox24.SetRotation( 398.4009f );
	_wallBox24.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox24.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox24.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox24.SetBodyType( b2_staticBody );

	// _wallBox25
	_wallBox25.SetRotation( 290.4002f );
	_wallBox25.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox25.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox25.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox25.SetBodyType( b2_staticBody );

	// _wallBox26
	_wallBox26.SetRotation( 308.4004f );
	_wallBox26.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox26.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox26.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox26.SetBodyType( b2_staticBody );

	// _wallBox27
	_wallBox27.SetRotation( 236.4004f );
	_wallBox27.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox27.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox27.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox27.SetBodyType( b2_staticBody );

	// _wallBox28
	_wallBox28.SetRotation( 560.3995f );
	_wallBox28.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox28.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox28.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox28.SetBodyType( b2_staticBody );

	// _wallBox29
	_wallBox29.SetRotation( 578.3981f );
	_wallBox29.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox29.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox29.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox29.SetBodyType( b2_staticBody );

	// _wallBox30
	_wallBox30.SetRotation( 542.3994f );
	_wallBox30.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox30.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox30.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox30.SetBodyType( b2_staticBody );

	// _wallBox31
	_wallBox31.SetRotation( 524.4006f );
	_wallBox31.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox31.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox31.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox31.SetBodyType( b2_staticBody );

	// _wallBox32
	_wallBox32.SetRotation( 506.4005f );
	_wallBox32.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox32.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox32.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox32.SetBodyType( b2_staticBody );

	// _wallBox33
	_wallBox33.SetRotation( 488.4004f );
	_wallBox33.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox33.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox33.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox33.SetBodyType( b2_staticBody );

	// _coin1
	_coin1.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin1.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin1.SetBodyType( b2_dynamicBody );

	// _wallBox34
	_wallBox34.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox34.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox34.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox34.SetBodyType( b2_staticBody );


	//Set blocks positions
	_piggyBank1.SetPosition( 26.0000f, 53.0000f );
	_vehical.SetPosition( 20.5633f, 7.6758f );
	_wallBox1.SetPosition( 34.6688f, 10.3203f );
	_wallBox2.SetPosition( 30.9077f, 6.5593f );
	_wallBox3.SetPosition( 26.1687f, 4.1446f );
	_wallBox4.SetPosition( 4.7469f, 15.0592f );
	_wallBox5.SetPosition( 7.1617f, 10.3203f );
	_wallBox6.SetPosition( 10.9228f, 6.5594f );
	_wallBox7.SetPosition( 20.9153f, 3.3126f );
	_wallBox8.SetPosition( 15.6622f, 4.1446f );
	_wallBox9.SetPosition( 89.7351f, 22.2442f );
	_wallBox10.SetPosition( 62.2335f, 22.8203f );
	_wallBox11.SetPosition( 81.1075f, 16.2479f );
	_wallBox12.SetPosition( 75.8379f, 15.5260f );
	_wallBox13.SetPosition( 85.8960f, 18.5628f );
	_wallBox14.SetPosition( 70.6031f, 16.4679f );
	_wallBox15.SetPosition( 65.9152f, 18.9814f );
	_wallBox16.SetPosition( 4.3123f, 47.9953f );
	_wallBox17.SetPosition( 6.5264f, 52.8314f );
	_wallBox18.SetPosition( 3.7009f, 42.7117f );
	_wallBox19.SetPosition( 4.7524f, 37.4979f );
	_wallBox20.SetPosition( 25.2576f, 59.7975f );
	_wallBox21.SetPosition( 30.0938f, 57.5834f );
	_wallBox22.SetPosition( 19.9741f, 60.4089f );
	_wallBox23.SetPosition( 14.7602f, 59.3576f );
	_wallBox24.SetPosition( 10.1265f, 56.7466f );
	_wallBox25.SetPosition( 36.6201f, 49.3493f );
	_wallBox26.SetPosition( 34.0089f, 53.9831f );
	_wallBox27.SetPosition( 34.8457f, 34.0159f );
	_wallBox28.SetPosition( 26.6119f, 27.4898f );
	_wallBox29.SetPosition( 31.2457f, 30.1008f );
	_wallBox30.SetPosition( 21.3982f, 26.4385f );
	_wallBox31.SetPosition( 16.1145f, 27.0499f );
	_wallBox32.SetPosition( 11.2784f, 29.2640f );
	_wallBox33.SetPosition( 7.3632f, 32.8641f );
	_coin1.SetPosition( 16.6000f, 52.4000f );
	_wallBox34.SetPosition( 24.5000f, 47.2000f );

	ConstFinal();
}


//----------------------------------------------------------------------------------------------
// LevelVehical_3
//----------------------------------------------------------------------------------------------
 LevelVehical_4::LevelVehical_4( GameTypes::LevelEnum levelEnum, bool viewMode ) : LevelVehicalBase( levelEnum, viewMode )
{
	_touchState = eNotingTouching;
	_vehical.SetPosition( b2Vec2( 11.5f, 2.0f ));

	b2Filter filter;

	// _wallBox1
	_wallBox1.GetBody()->GetFixtureList()->SetDensity( 8.0f );
	_wallBox1.GetBody()->GetFixtureList()->SetFriction( 0.20f );
	_wallBox1.GetBody()->GetFixtureList()->SetRestitution( 0.10f );
	filter.groupIndex = -1;
	filter.categoryBits = 1;
	filter.maskBits = 65535;
	_wallBox1.GetBody()->GetFixtureList()->SetFilterData( filter );
	_wallBox1.SetBodyType( b2_staticBody );

	// _wallBox2
	_wallBox2.SetRotation( 41 );
	_wallBox2.GetBody()->GetFixtureList()->SetDensity( 8.0f );
	_wallBox2.GetBody()->GetFixtureList()->SetFriction( 1.0f );
	_wallBox2.GetBody()->GetFixtureList()->SetRestitution( 0.10f );
	filter.groupIndex = -1;
	filter.categoryBits = 1;
	filter.maskBits = 65535;
	_wallBox2.GetBody()->GetFixtureList()->SetFilterData( filter );
	_wallBox2.SetBodyType( b2_staticBody );

	// _circleWood1
	_circleWood1.GetBody()->GetFixtureList()->SetDensity( 5.50f );
	_circleWood1.GetBody()->GetFixtureList()->SetFriction( 0.60f );
	_circleWood1.GetBody()->GetFixtureList()->SetRestitution( 0.0f );
	_circleWood1.GetBody()->SetAngularDamping(0.60f );
	_circleWood1.FlipY();
	_circleWood1.SetBodyType( b2_dynamicBody );
	_circleWood1.SetBlockType( GameTypes::eCustom );

	// _wallBox3
	_wallBox3.GetBody()->GetFixtureList()->SetDensity( 8.0f );
	_wallBox3.GetBody()->GetFixtureList()->SetFriction( 0.80f );
	_wallBox3.GetBody()->GetFixtureList()->SetRestitution( 0.10f );
	filter.groupIndex = -1;
	filter.categoryBits = 1;
	filter.maskBits = 65535;
	_wallBox3.GetBody()->GetFixtureList()->SetFilterData( filter );
	_wallBox3.SetBodyType( b2_staticBody );

	// _woodBox1
	_woodBox1.GetBody()->GetFixtureList()->SetDensity( 5.50f );
	_woodBox1.GetBody()->GetFixtureList()->SetFriction( 0.50f );
	_woodBox1.GetBody()->GetFixtureList()->SetRestitution( 0.20f );
	filter.groupIndex = -1;
	filter.categoryBits = 1;
	filter.maskBits = 65535;
	_woodBox1.GetBody()->GetFixtureList()->SetFilterData( filter );
	_woodBox1.SetBodyType( b2_dynamicBody );

	// _coin1
	_coin1.GetBody()->GetFixtureList()->SetDensity( 3.50f );
	_coin1.GetBody()->GetFixtureList()->SetFriction( 0.60f );
	_coin1.GetBody()->GetFixtureList()->SetRestitution( 0.10f );
	_coin1.SetBodyType( b2_dynamicBody );

	// _woodBox2
	_lift1.GetBody()->GetFixtureList()->SetDensity( 5.50f );
	_lift1.GetBody()->GetFixtureList()->SetFriction( 0.50f );
	_lift1.GetBody()->GetFixtureList()->SetRestitution( 0.20f );
	_lift1.GetBody()->SetLinearDamping(0.10f );
	_lift1.SetBodyType( b2_dynamicBody );

	// _wallBox4
	_wallBox4.SetRotation( 40 );
	_wallBox4.GetBody()->GetFixtureList()->SetDensity( 8.0f );
	_wallBox4.GetBody()->GetFixtureList()->SetFriction( 0.10f );
	_wallBox4.GetBody()->GetFixtureList()->SetRestitution( 0.10f );
	_wallBox4.SetBodyType( b2_staticBody );

	// _piggyBank1
	_piggyBank1.GetBody()->GetFixtureList()->SetDensity( 7.50f );
	_piggyBank1.GetBody()->GetFixtureList()->SetFriction( 0.60f );
	_piggyBank1.GetBody()->GetFixtureList()->SetRestitution( 0.20f );
	_piggyBank1.GetBody()->SetAngularDamping(0.10f );
	_piggyBank1.SetBodyType( b2_dynamicBody );

	// _wallBox5
	_wallBox5.GetBody()->GetFixtureList()->SetDensity( 8.0f );
	_wallBox5.GetBody()->GetFixtureList()->SetFriction( 0.80f );
	_wallBox5.GetBody()->GetFixtureList()->SetRestitution( 0.10f );
	_wallBox5.SetBodyType( b2_staticBody );

	// _wallBox6
	_wallBox6.GetBody()->GetFixtureList()->SetDensity( 8.0f );
	_wallBox6.GetBody()->GetFixtureList()->SetFriction( 0.80f );
	_wallBox6.GetBody()->GetFixtureList()->SetRestitution( 0.10f );
	filter.groupIndex = -1;
	filter.categoryBits = 1;
	filter.maskBits = 65535;
	_wallBox6.GetBody()->GetFixtureList()->SetFilterData( filter );
	_wallBox6.SetBodyType( b2_staticBody );

	// _woodBox3
	_woodBox3.SetRotation( 30 );
	_woodBox3.GetBody()->GetFixtureList()->SetDensity( 5.50f );
	_woodBox3.GetBody()->GetFixtureList()->SetFriction( 1.0f );
	_woodBox3.GetBody()->GetFixtureList()->SetRestitution( 0.50f );
	filter.groupIndex = -1;
	filter.categoryBits = 1;
	filter.maskBits = 65535;
	_woodBox3.GetBody()->GetFixtureList()->SetFilterData( filter );
	_woodBox3.SetBodyType( b2_dynamicBody );

	// _woodBox4
	_woodBox4.SetRotation( 30 );
	_woodBox4.GetBody()->GetFixtureList()->SetDensity( 5.50f );
	_woodBox4.GetBody()->GetFixtureList()->SetFriction( 1.0f );
	_woodBox4.GetBody()->GetFixtureList()->SetRestitution( 0.50f );
	filter.groupIndex = -1;
	filter.categoryBits = 1;
	filter.maskBits = 65535;
	_woodBox4.GetBody()->GetFixtureList()->SetFilterData( filter );
	_woodBox4.SetBodyType( b2_dynamicBody );

	// _wallBox7
	_wallBox7.GetBody()->GetFixtureList()->SetDensity( 8.0f );
	_wallBox7.GetBody()->GetFixtureList()->SetFriction( 0.80f );
	_wallBox7.GetBody()->GetFixtureList()->SetRestitution( 0.10f );
	filter.groupIndex = -1;
	filter.categoryBits = 1;
	filter.maskBits = 65535;
	_wallBox7.GetBody()->GetFixtureList()->SetFilterData( filter );
	_wallBox7.SetBodyType( b2_staticBody );

	// _woodBox5
	_woodBox5.SetRotation( -30 );
	_woodBox5.GetBody()->GetFixtureList()->SetDensity( 5.50f );
	_woodBox5.GetBody()->GetFixtureList()->SetFriction( 1.0f );
	_woodBox5.GetBody()->GetFixtureList()->SetRestitution( 0.20f );
	filter.groupIndex = -1;
	filter.categoryBits = 1;
	filter.maskBits = 65535;
	_woodBox5.GetBody()->GetFixtureList()->SetFilterData( filter );
	_woodBox5.SetBodyType( b2_dynamicBody );

	// _wallBox8
	_wallBox8.GetBody()->GetFixtureList()->SetDensity( 8.0f );
	_wallBox8.GetBody()->GetFixtureList()->SetFriction( 0.80f );
	_wallBox8.GetBody()->GetFixtureList()->SetRestitution( 0.10f );
	filter.groupIndex = -1;
	filter.categoryBits = 1;
	filter.maskBits = 65535;
	_wallBox8.GetBody()->GetFixtureList()->SetFilterData( filter );
	_wallBox8.SetBodyType( b2_staticBody );

	// _wallBox9
	_wallBox9.GetBody()->GetFixtureList()->SetDensity( 8.0f );
	_wallBox9.GetBody()->GetFixtureList()->SetFriction( 0.80f );
	_wallBox9.GetBody()->GetFixtureList()->SetRestitution( 0.10f );
	filter.groupIndex = -1;
	filter.categoryBits = 1;
	filter.maskBits = 65535;
	_wallBox9.GetBody()->GetFixtureList()->SetFilterData( filter );
	_wallBox9.SetBodyType( b2_staticBody );

	// _woodBox6
	_woodBox6.SetRotation( -30 );
	_woodBox6.GetBody()->GetFixtureList()->SetDensity( 5.50f );
	_woodBox6.GetBody()->GetFixtureList()->SetFriction( 1.0f );
	_woodBox6.GetBody()->GetFixtureList()->SetRestitution( 0.20f );
	filter.groupIndex = -1;
	filter.categoryBits = 1;
	filter.maskBits = 65535;
	_woodBox6.GetBody()->GetFixtureList()->SetFilterData( filter );
	_woodBox6.SetBodyType( b2_dynamicBody );

	// _wallBox10
	_wallBox10.GetBody()->GetFixtureList()->SetDensity( 8.0f );
	_wallBox10.GetBody()->GetFixtureList()->SetFriction( 0.80f );
	_wallBox10.GetBody()->GetFixtureList()->SetRestitution( 0.10f );
	filter.groupIndex = -1;
	filter.categoryBits = 1;
	filter.maskBits = 65535;
	_wallBox10.GetBody()->GetFixtureList()->SetFilterData( filter );
	_wallBox10.SetBodyType( b2_staticBody );

	// _woodBox7
	_woodBox7.SetRotation( 30 );
	_woodBox7.GetBody()->GetFixtureList()->SetDensity( 5.50f );
	_woodBox7.GetBody()->GetFixtureList()->SetFriction( 1.0f );
	_woodBox7.GetBody()->GetFixtureList()->SetRestitution( 0.50f );
	filter.groupIndex = -1;
	filter.categoryBits = 1;
	filter.maskBits = 65535;
	_woodBox7.GetBody()->GetFixtureList()->SetFilterData( filter );
	_woodBox7.SetBodyType( b2_dynamicBody );

	// _wallBox11
	_wallBox11.GetBody()->GetFixtureList()->SetDensity( 8.0f );
	_wallBox11.GetBody()->GetFixtureList()->SetFriction( 0.80f );
	_wallBox11.GetBody()->GetFixtureList()->SetRestitution( 0.10f );
	_wallBox11.SetBodyType( b2_staticBody );

	// _wallBox12
	_wallBox12.SetRotation( 10 );
	_wallBox12.GetBody()->GetFixtureList()->SetDensity( 8.0f );
	_wallBox12.GetBody()->GetFixtureList()->SetFriction( 0.80f );
	_wallBox12.GetBody()->GetFixtureList()->SetRestitution( 0.10f );
	_wallBox12.SetBodyType( b2_staticBody );


	//Set blocks positions
	_floor.SetPosition( b2Vec2( 24.0f, 0.0f ));
	_wallBox1.SetPosition( 10.50f, 6.550f );
	_wallBox2.SetPosition( 23.10f, 3.050f );
	_circleWood1.SetPosition( 1.20f, 9.950f );
	_wallBox3.SetPosition( 37.60f, 6.550f );
	_woodBox1.SetPosition( 23.650f, 6.550f );
	_coin1.SetPosition( 13.0f, 21.90f );
	_lift1.SetPosition( 41.75f, 19.80f );
	_wallBox4.SetPosition( 32.75f, 7.60f );
	_piggyBank1.SetPosition( 38.0f, 13.45f );
	_wallBox5.SetPosition( 30.350f, 17.60f );
	_wallBox6.SetPosition( 23.30f, 22.30f );
	_woodBox3.SetPosition( 17.90f, 20.750f );
	_woodBox4.SetPosition( 18.30f, 8.250f );
	_wallBox7.SetPosition( 12.850f, 13.0f );
	_woodBox5.SetPosition( 18.150f, 11.450f );
	_wallBox8.SetPosition( 23.70f, 9.80f );
	_wallBox9.SetPosition( 12.650f, 19.250f );
	_woodBox6.SetPosition( 17.950f, 17.70f );
	_wallBox10.SetPosition( 23.50f, 16.050f );
	_woodBox7.SetPosition( 18.10f, 14.50f );
	_wallBox11.SetPosition( 37.25f, 9.60f );
	_wallBox12.SetPosition( 32.45f, 23.45f );


	ConstFinal();
	CreateJoints();
}
//--------------------------------------------------------------------------
void LevelVehical_4::CreateJoints()
{
	b2RevoluteJointDef jointRevDef1;
	jointRevDef1.collideConnected = false;
	jointRevDef1.Initialize( _wallBox1.GetBody(), _woodBox1.GetBody(), b2Vec2( 20.750f, 6.550f ));
	_jointRev1 = (b2RevoluteJoint *) _world.CreateJoint( &jointRevDef1 );

	_jointRev1->SetLimits( 0.0f, 2.450f );
	_jointRev1->EnableLimit( true );
	_jointRev1->EnableMotor( true );
	_jointRev1->SetMotorSpeed( -3.30f );
	_jointRev1->SetMaxMotorTorque( 490.0f );

	b2RevoluteJointDef jointRevDef2;
	jointRevDef2.collideConnected = false;
	jointRevDef2.Initialize( _wallBox6.GetBody(), _woodBox3.GetBody(), b2Vec2( 20.650f, 22.30f ));
	_jointRev2 = (b2RevoluteJoint *) _world.CreateJoint( &jointRevDef2 );

	_jointRev2->SetLimits( -2.40f, 0.0f );
	_jointRev2->EnableLimit( true );
	_jointRev2->EnableMotor( true );
	_jointRev2->SetMotorSpeed( 2.50f );
	_jointRev2->SetMaxMotorTorque( 440.0f );

	b2RevoluteJointDef jointRevDef3;
	jointRevDef3.collideConnected = false;
	jointRevDef3.Initialize( _wallBox8.GetBody(), _woodBox4.GetBody(), b2Vec2( 20.950f, 9.750f ));
	_jointRev3 = (b2RevoluteJoint *) _world.CreateJoint( &jointRevDef3 );

	_jointRev3->SetLimits( -2.40f, 0.0f );
	_jointRev3->EnableLimit( true );
	_jointRev3->EnableMotor( true );
	_jointRev3->SetMotorSpeed( 2.50f );
	_jointRev3->SetMaxMotorTorque( 440.0f );

	b2RevoluteJointDef jointRevDef4;
	jointRevDef4.collideConnected = false;
	jointRevDef4.Initialize( _wallBox7.GetBody(), _woodBox5.GetBody(), b2Vec2( 15.40f, 13.10f ));
	_jointRev4 = (b2RevoluteJoint *) _world.CreateJoint( &jointRevDef4 );

	_jointRev4->SetLimits( 0.0f, 2.40f );
	_jointRev4->EnableLimit( true );
	_jointRev4->EnableMotor( true );
	_jointRev4->SetMotorSpeed( -2.40f );
	_jointRev4->SetMaxMotorTorque( 440.0f );

	b2RevoluteJointDef jointRevDef5;
	jointRevDef5.collideConnected = false;
	jointRevDef5.Initialize( _wallBox10.GetBody(), _woodBox7.GetBody(), b2Vec2( 20.550f, 16.050f ));
	_jointRev5 = (b2RevoluteJoint *) _world.CreateJoint( &jointRevDef5 );

	_jointRev5->SetLimits( -2.40f, 0.0f );
	_jointRev5->EnableLimit( true );
	_jointRev5->EnableMotor( true );
	_jointRev5->SetMotorSpeed( 2.50f );
	_jointRev5->SetMaxMotorTorque( 440.0f );

	b2RevoluteJointDef jointRevDef6;
	jointRevDef6.collideConnected = false;
	jointRevDef6.Initialize( _wallBox9.GetBody(), _woodBox6.GetBody(), b2Vec2( 15.350f, 19.30f ));
	_jointRev6 = (b2RevoluteJoint *) _world.CreateJoint( &jointRevDef6 );

	_jointRev6->SetLimits( 0.0f, 2.40f );
	_jointRev6->EnableLimit( true );
	_jointRev6->EnableMotor( true );
	_jointRev6->SetMotorSpeed( -2.50f );
	_jointRev6->SetMaxMotorTorque( 440.0f );

	b2PrismaticJointDef jointPrisDef1;
	jointPrisDef1.collideConnected = true;
	jointPrisDef1.Initialize( _wallBox3.GetBody(), _lift1.GetBody(), _lift1.GetPosition(), b2Vec2( 0.0f, 1.0f ) );
	_jointPris1 = (b2PrismaticJoint *) _world.CreateJoint( &jointPrisDef1 );

	_jointPris1->EnableMotor( true );
	_jointPris1->SetMotorSpeed( 0.0f );
	_jointPris1->SetMaxMotorForce( 1000.0f );

	b2RevoluteJointDef jointRevDef7;
	jointRevDef7.collideConnected = false;
	jointRevDef7.Initialize( _wallBox1.GetBody(), _circleWood1.GetBody(), b2Vec2( 1.20f, 9.950f ));
	_jointRev7 = (b2RevoluteJoint *) _world.CreateJoint( &jointRevDef7 );

	_jointRev7->EnableMotor( true );
	_jointRev7->SetMotorSpeed( 0.0f );
	_jointRev7->SetMaxMotorTorque( 1000.0f );

	b2GearJointDef jointGearDef1;
	jointGearDef1.bodyA = _circleWood1.GetBody();
	jointGearDef1.bodyB = _lift1.GetBody();
	jointGearDef1.joint1 = _jointRev7;
	jointGearDef1.joint2 = _jointPris1;
	jointGearDef1.ratio = -5.0f;
	_jointGear1 = (b2GearJoint *) _world.CreateJoint( &jointGearDef1 );
}
//--------------------------------------------------------------------------
void LevelVehical_4::Step( cocos2d::ccTime dt )
{
	UpdateWheel();
	LevelVehicalBase::Step( dt );
}
//--------------------------------------------------------------------------
void LevelVehical_4::GameCustomAction_ContactBegin( puBlock *aBlock, puBlock *bBlock, b2Contact *contact )
{
	D_LOG("Contact");
}
//--------------------------------------------------------------------------
void LevelVehical_4::GameCustomAction_ContactEnd( puBlock *aBlock, puBlock *bBlock, b2Contact *contact )
{
	//_touch = false;
	D_LOG("Contact stop");
}

//--------------------------------------------------------------------------
void LevelVehical_4::UpdateWheel()
{
	UpdateTouchingState();
	if ( _touchState == eLiftVehicalTouching )
	{
		float speed = _vehical.GetFrontWheel()->GetBody()->GetAngularVelocity();
		_circleWood1.GetBody()->SetAngularVelocity( -speed / 10.0f );
	}
	else if ( _touchState == ( eLiftCoinTouching | eCoinVehicalTouching) )
	{
		float speed = _vehical.GetFrontWheel()->GetBody()->GetAngularVelocity();
		_coin1.GetBody()->SetAngularVelocity( -speed / 10.0f );
		_circleWood1.GetBody()->SetAngularVelocity( speed / 10.0f );
	}
	else 
	{
		_circleWood1.GetBody()->SetAngularVelocity( 0.0f );
	}
}
//--------------------------------------------------------------------------
void LevelVehical_4::UpdateTouchingState()
{
	b2ContactEdge* ce;
	puBlock *blockA;
	puBlock *blockB;

	_touchState = eNotingTouching;
	for ( ce = _circleWood1.GetBody()->GetContactList(); ce; ce = ce->next )
	{
		b2Contact* c = ce->contact;
		blockA = (puBlock *) c->GetFixtureA()->GetBody()->GetUserData();
		blockB = (puBlock *) c->GetFixtureB()->GetBody()->GetUserData();

		_touchState = eLiftVehicalTouching;
		SetActiveWheel( blockA, blockB );
	}

	for ( ce = _coin1.GetBody()->GetContactList(); ce; ce = ce->next )
	{
		b2Contact* c = ce->contact;
		blockA = (puBlock *) c->GetFixtureA()->GetBody()->GetUserData();
		blockB = (puBlock *) c->GetFixtureB()->GetBody()->GetUserData();

		if ( blockA == &_circleWood1 || blockB == &_circleWood1 )
		{
			_touchState = _touchState | eLiftCoinTouching;
		}
		if ( blockA == _vehical.GetFrontWheel() || blockB == _vehical.GetFrontWheel() ||
			blockA == _vehical.GetBackWheel()  || blockB == _vehical.GetBackWheel()   )
		{
			_touchState = _touchState | eCoinVehicalTouching;
			SetActiveWheel( blockA, blockB );
		}
	}
}
//--------------------------------------------------------------------------
void LevelVehical_4::SetActiveWheel( puBlock *blockA, puBlock *blockB)
{
	if ( blockA == _vehical.GetFrontWheel() || blockB == _vehical.GetFrontWheel() ||
		blockA == _vehical.GetBackWheel()  || blockB == _vehical.GetBackWheel()   )
	{
		if ( blockA == _vehical.GetFrontWheel() || blockB == _vehical.GetFrontWheel() )
			_activeWheel = _vehical.GetFrontWheel();
		else
			_activeWheel = _vehical.GetBackWheel();
	}
	else
	{
		_activeWheel = NULL;
	}
}
