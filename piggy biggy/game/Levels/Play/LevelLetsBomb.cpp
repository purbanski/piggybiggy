#include "LevelLetsBomb.h"

//------------------------------------------------------------------------------------
LevelLetsBomb_1::LevelLetsBomb_1( GameTypes::LevelEnum levelEnum, bool viewMode ) : Level( levelEnum, viewMode )
{
	_coinCount = 2;

	// _bomb1
	_bomb1.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_bomb1.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_bomb1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bomb1.GetBody()->SetAngularDamping(0.2000f );
	_bomb1.GetBody()->SetLinearDamping(0.2000f );
	_bomb1.SetBodyType( b2_dynamicBody );

	// _bomb2
	_bomb2.SetRotation( 0.0000f );
	_bomb2.GetBody()->GetFixtureList()->SetDensity( 10.0000f );
	_bomb2.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_bomb2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bomb2.GetBody()->SetAngularDamping(0.4000f );
	_bomb2.GetBody()->SetLinearDamping(0.3000f );
	_bomb2.SetBodyType( b2_dynamicBody );

	// _coin1
	_coin1.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_coin1.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_coin1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_coin1.GetBody()->SetAngularDamping(0.4000f );
	_coin1.GetBody()->SetLinearDamping(0.3000f );
	_coin1.SetBodyType( b2_dynamicBody );

	// _coin2
	_coin2.GetBody()->GetFixtureList()->SetDensity( 1.0000f );
	_coin2.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin2.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin2.GetBody()->SetAngularDamping(0.2000f );
	_coin2.GetBody()->SetLinearDamping(0.2000f );
	_coin2.SetBodyType( b2_dynamicBody );

	// _floor11
	_floor1.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_floor1.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_floor1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_floor1.SetBodyType( b2_staticBody );

	// _piggyBank1
	_piggyBank1.GetBody()->GetFixtureList()->SetDensity( 7.5000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_piggyBank1.GetBody()->SetAngularDamping(0.2000f );
	_piggyBank1.GetBody()->SetLinearDamping(0.2000f );
	_piggyBank1.SetBodyType( b2_dynamicBody );

	// _piggyBank2
	_piggyBank2.GetBody()->GetFixtureList()->SetDensity( 7.5000f );
	_piggyBank2.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_piggyBank2.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_piggyBank2.GetBody()->SetAngularDamping(0.2000f );
	_piggyBank2.GetBody()->SetLinearDamping(0.2000f );
	_piggyBank2.SetBodyType( b2_dynamicBody );


	//Set blocks positions
	_bomb1.SetPosition( 42.6000f, 20.0001f );
	_bomb2.SetPosition( 53.4000f, 20.0001f );
	_coin1.SetPosition( 74.0000f, 20.0001f );
	_coin2.SetPosition( 28.8000f, 20.0001f );
	_floor1.SetPosition( 47.8000f, 12.3001f );
	_piggyBank1.SetPosition( 6.4000f, 20.0001f );
	_piggyBank2.SetPosition( 89.6000f, 20.0001f );

	//Bombs Construction
	_bomb1.SetRange( 25.8000f );
	_bomb1.SetBombImpulse( 3000.0000f );
	_bomb2.SetRange( 14.8000f );
	_bomb2.SetBombImpulse( 10600.0000f );

	ConstFinal();
}
//------------------------------------------------------------------------------------
LevelLetsBomb_1b::LevelLetsBomb_1b( GameTypes::LevelEnum levelEnum, bool viewMode ) : Level( levelEnum, viewMode )
{
    _levelTag = eLevelTag_Bombs;
	_coinCount = 2;

	// _bomb1
	_bomb1.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_bomb1.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_bomb1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bomb1.GetBody()->SetAngularDamping(0.4000f );
	_bomb1.GetBody()->SetLinearDamping(0.3000f );
	_bomb1.SetBodyType( b2_dynamicBody );

	// _bomb2
	_bomb2.GetBody()->GetFixtureList()->SetDensity( 10.0000f );
	_bomb2.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_bomb2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bomb2.GetBody()->SetAngularDamping(0.4000f );
	_bomb2.GetBody()->SetLinearDamping(0.3000f );
	_bomb2.SetBodyType( b2_dynamicBody );

	// _bomb3
	_bomb3.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_bomb3.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_bomb3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bomb3.GetBody()->SetAngularDamping(0.4000f );
	_bomb3.GetBody()->SetLinearDamping(0.3000f );
	_bomb3.SetBodyType( b2_dynamicBody );

	// _bomb4
	_bomb4.GetBody()->GetFixtureList()->SetDensity( 10.0000f );
	_bomb4.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_bomb4.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bomb4.GetBody()->SetAngularDamping(0.4000f );
	_bomb4.GetBody()->SetLinearDamping(0.3000f );
	_bomb4.SetBodyType( b2_dynamicBody );

	// _coin1
	_coin1.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_coin1.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_coin1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_coin1.GetBody()->SetAngularDamping(0.4000f );
	_coin1.GetBody()->SetLinearDamping(0.3500f );
	_coin1.SetBodyType( b2_dynamicBody );

	// _coin2
	_coin2.GetBody()->GetFixtureList()->SetDensity( 1.0000f );
	_coin2.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin2.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin2.GetBody()->SetAngularDamping(0.4000f );
	_coin2.GetBody()->SetLinearDamping(0.3500f );
	_coin2.SetBodyType( b2_dynamicBody );

	// _piggyBankStatic1
	_piggyBankStatic1.GetBody()->GetFixtureList()->SetDensity( 7.5000f );
	_piggyBankStatic1.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_piggyBankStatic1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_piggyBankStatic1.SetBodyType( b2_staticBody );

	// _piggyBankStatic2
	_piggyBankStatic2.GetBody()->GetFixtureList()->SetDensity( 7.5000f );
	_piggyBankStatic2.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_piggyBankStatic2.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_piggyBankStatic2.FlipX();
	_piggyBankStatic2.SetBodyType( b2_staticBody );

	// _wallBox1
	_wallBox1.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox1.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox1.SetBodyType( b2_staticBody );


	//Set blocks positions
	_bomb1.SetPosition( 43.2001f, 35.0502f );
	_bomb2.SetPosition( 31.1002f, 35.0502f );
	_bomb3.SetPosition( 65.6001f, 35.0502f );
	_bomb4.SetPosition( 53.2999f, 35.0502f );
	_coin1.SetPosition( 77.5002f, 35.0497f );
	_coin2.SetPosition( 17.8001f, 35.0497f );
	_piggyBankStatic1.SetPosition( 87.3000f, 17.2000f );
	_piggyBankStatic2.SetPosition( 8.7000f, 17.2000f );
	_wallBox1.SetPosition( 48.0000f, 28.3498f );

	//Bombs Construction
	_bomb1.SetRange( 11.8000f );
	_bomb1.SetBombImpulse( 12000.0000f );
	_bomb2.SetRange( 12.2000f );
	_bomb2.SetBombImpulse( 15125.0000f );
	_bomb3.SetRange( 9.8000f );
	_bomb3.SetBombImpulse( 12000.0000f );
	_bomb4.SetRange( 11.6000f );
	_bomb4.SetBombImpulse( 22100.0000f );

	ConstFinal();
}
//------------------------------------------------------------------------------------
LevelLetsBomb_2::LevelLetsBomb_2( GameTypes::LevelEnum levelEnum, bool viewMode ) : Level( levelEnum, viewMode )
{
    _levelTag = eLevelTag_Bombs;
    
	// _bombStatic1
	_bombStatic1.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_bombStatic1.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_bombStatic1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bombStatic1.SetBodyType( b2_staticBody );

	// _coinStatic1
	_coinStatic1.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coinStatic1.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_coinStatic1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coinStatic1.GetBody()->SetAngularDamping(0.1000f );
	_coinStatic1.GetBody()->SetLinearDamping(0.1000f );
	_coinStatic1.SetBodyType( b2_staticBody );

	// _piggyBank1
	_piggyBank1.GetBody()->GetFixtureList()->SetDensity( 7.5000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_piggyBank1.FlipX();
	_piggyBank1.SetBodyType( b2_dynamicBody );

	// _wallBox1
	_wallBox1.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox1.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox1.SetBodyType( b2_staticBody );

	// _chain1
	_chain1.SetChain( 22.0000f);
	_chain1.SetRotation( 345.0000f );
	_chain1.FixHook( false );

	// _chain2
	_chain2.SetChain( 22.0000f);
	_chain2.SetRotation( 195.0000f );
	_chain2.FixHook( false );


	//Set blocks positions
	_bombStatic1.SetPosition( 16.2502f, 7.5000f );
	_coinStatic1.SetPosition( 77.6000f, 55.1000f );
	_piggyBank1.SetPosition( 57.5083f, 49.7166f );
	_wallBox1.SetPosition( 16.2502f, 13.1501f );
	_chain1.SetPosition( 35.9998f, 55.1000f );
	_chain2.SetPosition( 77.6000f, 55.1000f );

	//Bombs Construction
	_bombStatic1.SetRange( 17.4000f );
	_bombStatic1.SetBombImpulse( 119400.0000f );

	//Set chains hooks
	_chain1.HookOnChain( &_piggyBank1 );
	_chain2.HookOnChain( &_piggyBank1 );

	ConstFinal();
}
//--------------------------------------------------------------------------
LevelLetsBomb_3::LevelLetsBomb_3( GameTypes::LevelEnum levelEnum, bool viewMode ): Level( levelEnum, viewMode )
{
    _levelTag = eLevelTag_Bombs;
    
	// _bomb1
	_bomb1.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_bomb1.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_bomb1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bomb1.GetBody()->SetAngularDamping(0.1000f );
	_bomb1.GetBody()->SetLinearDamping(0.2000f );
	_bomb1.SetBodyType( b2_dynamicBody );

	// _bomb2
	_bomb2.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_bomb2.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_bomb2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bomb2.GetBody()->SetAngularDamping(0.1000f );
	_bomb2.GetBody()->SetLinearDamping(0.2000f );
	_bomb2.SetBodyType( b2_dynamicBody );

	// _bomb3
	_bomb3.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_bomb3.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_bomb3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bomb3.GetBody()->SetAngularDamping(0.1000f );
	_bomb3.GetBody()->SetLinearDamping(0.2000f );
	_bomb3.SetBodyType( b2_dynamicBody );

	// _coin1
	_coin1.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_coin1.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_coin1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_coin1.GetBody()->SetAngularDamping(0.4000f );
	_coin1.GetBody()->SetLinearDamping(0.3000f );
	_coin1.SetBodyType( b2_dynamicBody );

	// _piggyBank1
	_piggyBank1.GetBody()->GetFixtureList()->SetDensity( 7.5000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_piggyBank1.FlipX();
	_piggyBank1.SetBodyType( b2_dynamicBody );

	// _wallBox1
	_wallBox1.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox1.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox1.SetBodyType( b2_staticBody );

	// _wallBox2
	_wallBox2.SetRotation( -5.4000f );
	_wallBox2.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox2.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox2.SetBodyType( b2_staticBody );


	//Set blocks positions
	_bomb1.SetPosition( 44.4441f, 34.7428f );
	_bomb2.SetPosition( 62.7307f, 32.7770f );
	_bomb3.SetPosition( 77.9644f, 31.3064f );
	_coin1.SetPosition( 20.7666f, 35.4514f );
	_piggyBank1.SetPosition( 9.0000f, 34.2499f );
	_wallBox1.SetPosition( 9.6000f, 25.4999f );
	_wallBox2.SetPosition( 54.4000f, 22.1000f );

	//Bombs Construction
	_bomb1.SetRange( 14.0000f );
	_bomb1.SetBombImpulse( 5300.0000f );
	_bomb2.SetRange( 20.0000f );
	_bomb2.SetBombImpulse( 13500.0000f );
	_bomb3.SetRange( 20.0000f );
	_bomb3.SetBombImpulse( 6300.0000f );

	ConstFinal();
}
//--------------------------------------------------------------------------
LevelLetsBomb_4::LevelLetsBomb_4( GameTypes::LevelEnum levelEnum, bool viewMode ) : Level( levelEnum, viewMode )
{
    _levelTag = eLevelTag_Bombs;
    
	// _bomb1
	_bomb1.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_bomb1.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_bomb1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bomb1.SetBodyType( b2_dynamicBody );

	// _coin1
	_coin1.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin1.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin1.SetBodyType( b2_dynamicBody );

	// _wallBox1
	_wallBox1.SetRotation( 248.0003f );
	_wallBox1.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox1.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox1.SetBodyType( b2_staticBody );

	// _wallBox2
	_wallBox2.SetRotation( 108.9474f );
	_wallBox2.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox2.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox2.SetBodyType( b2_staticBody );

	// _wallBox3
	_wallBox3.SetRotation( 127.8948f );
	_wallBox3.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox3.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox3.SetBodyType( b2_staticBody );

	// _wallBox4
	_wallBox4.SetRotation( 146.8422f );
	_wallBox4.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox4.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox4.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox4.SetBodyType( b2_staticBody );

	// _wallBox5
	_wallBox5.SetRotation( 165.7896f );
	_wallBox5.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox5.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox5.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox5.SetBodyType( b2_staticBody );

	// _wallBox6
	_wallBox6.SetRotation( 184.7369f );
	_wallBox6.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox6.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox6.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox6.SetBodyType( b2_staticBody );

	// _wallBox7
	_wallBox7.SetRotation( 203.6842f );
	_wallBox7.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox7.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox7.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox7.SetBodyType( b2_staticBody );

	// _wallBox8
	_wallBox8.SetRotation( 222.6316f );
	_wallBox8.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox8.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox8.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox8.SetBodyType( b2_staticBody );

	// _wallBox9
	_wallBox9.SetRotation( 241.5792f );
	_wallBox9.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox9.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox9.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox9.SetBodyType( b2_staticBody );

	// _wallBox10
	_wallBox10.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox10.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox10.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox10.SetBodyType( b2_staticBody );

	// _wallBox11
	_wallBox11.SetRotation( 241.5792f );
	_wallBox11.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox11.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox11.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox11.SetBodyType( b2_staticBody );

	// _wallBox12
	_wallBox12.SetRotation( 222.6316f );
	_wallBox12.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox12.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox12.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox12.SetBodyType( b2_staticBody );

	// _wallBox13
	_wallBox13.SetRotation( 203.6842f );
	_wallBox13.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox13.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox13.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox13.SetBodyType( b2_staticBody );

	// _wallBox14
	_wallBox14.SetRotation( 165.7896f );
	_wallBox14.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox14.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox14.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox14.SetBodyType( b2_staticBody );

	// _wallBox15
	_wallBox15.SetRotation( 184.7369f );
	_wallBox15.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox15.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox15.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox15.SetBodyType( b2_staticBody );

	// _wallBox16
	_wallBox16.SetRotation( 146.8422f );
	_wallBox16.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox16.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox16.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox16.SetBodyType( b2_staticBody );

	// _wallBox17
	_wallBox17.SetRotation( 108.9474f );
	_wallBox17.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox17.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox17.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox17.SetBodyType( b2_staticBody );

	// _wallBox18
	_wallBox18.SetRotation( 127.8948f );
	_wallBox18.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox18.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox18.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox18.SetBodyType( b2_staticBody );

	// _wallBox19
	_wallBox19.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox19.GetBody()->GetFixtureList()->SetFriction( 0.2500f );
	_wallBox19.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox19.SetBodyType( b2_staticBody );

	// _piggyBank1
	_piggyBank1.FlipX();
	_piggyBank1.GetBody()->GetFixtureList()->SetDensity( 7.5000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_piggyBank1.SetBodyType( b2_dynamicBody );

	// _bomb2
	_bomb2.SetRotation( -4.2000f );
	_bomb2.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_bomb2.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_bomb2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bomb2.SetBodyType( b2_dynamicBody );

	// _wallBox20
	_wallBox20.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox20.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox20.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox20.SetBodyType( b2_staticBody );

	// _wallBox21
	_wallBox21.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox21.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox21.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox21.SetBodyType( b2_staticBody );

	// _chain1
	_chain1.SetChain( 17.5000f);
	_chain1.SetRotation( 210.0001f );
	_chain1.FixHook( false );

	// _chain2
	_chain2.SetChain( 17.5000f);
	_chain2.SetRotation( 330.0001f );
	_chain2.FixHook( false );

	// _bomb3
	_bomb3.SetRotation( -4.2000f );
	_bomb3.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_bomb3.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_bomb3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bomb3.SetBodyType( b2_dynamicBody );

	// _wallBox22
	_wallBox22.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox22.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox22.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox22.SetBodyType( b2_staticBody );

	// _wallBox23
	_wallBox23.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox23.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox23.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox23.SetBodyType( b2_staticBody );


	//Set blocks positions
	_bomb1.SetPosition( 7.8500f, 57.3001f );
	_coin1.SetPosition( 88.5500f, 57.3001f );
	_wallBox1.SetPosition( -60.3744f, 46.4266f );
	_wallBox2.SetPosition( 7.1178f, 16.7581f );
	_wallBox3.SetPosition( 9.8988f, 11.6192f );
	_wallBox4.SetPosition( 14.1978f, 7.6618f );
	_wallBox5.SetPosition( 19.5488f, 5.3147f );
	_wallBox6.SetPosition( 25.3719f, 4.8322f );
	_wallBox7.SetPosition( 31.0363f, 6.2666f );
	_wallBox8.SetPosition( 35.9279f, 9.4624f );
	_wallBox9.SetPosition( 39.5168f, 14.0734f );
	_wallBox10.SetPosition( 15.0501f, 21.1000f );
	_wallBox11.SetPosition( 88.9674f, 14.1735f );
	_wallBox12.SetPosition( 85.3785f, 9.5624f );
	_wallBox13.SetPosition( 80.4868f, 6.2166f );
	_wallBox14.SetPosition( 68.9992f, 5.2647f );
	_wallBox15.SetPosition( 74.8225f, 4.7822f );
	_wallBox16.SetPosition( 63.6481f, 7.6118f );
	_wallBox17.SetPosition( 56.5679f, 16.7081f );
	_wallBox18.SetPosition( 59.3489f, 11.5693f );
	_wallBox19.SetPosition( 64.5004f, 21.1000f );
	_piggyBank1.SetPosition( 20.0000f, 30.1000f );
	_bomb2.SetPosition( 67.9499f, 31.4502f );
	_wallBox20.SetPosition( 67.9500f, 24.6500f );
	_wallBox21.SetPosition( 55.2000f, 24.6500f );
	_chain1.SetPosition( 60.7172f, 49.6001f );
	_chain2.SetPosition( 35.0828f, 49.6000f );
	_bomb3.SetPosition( 47.9000f, 42.2001f );
	_wallBox22.SetPosition( 19.4000f, 48.2001f );
	_wallBox23.SetPosition( 76.4500f, 48.2001f );

	//Bombs Construction
	_bomb1.SetRange( 19.4000f );
	_bomb1.SetBombImpulse( 56000.0000f );
	_bomb2.SetRange( 23.2000f );
	_bomb2.SetBombImpulse( 52100.0000f );
	_bomb3.SetRange( 19.3000f );
	_bomb3.SetBombImpulse( 40050.0000f );


	//Set chains hooks
	_chain1.HookOnChain( &_bomb3 );
	_chain2.HookOnChain( &_bomb3 );

	ConstFinal();
}
//--------------------------------------------------------------------------
LevelLetsBomb_5::LevelLetsBomb_5( GameTypes::LevelEnum levelEnum, bool viewMode ) : Level( levelEnum, viewMode )
{
    _levelTag = eLevelTag_Bombs;
	
    // _bomb1
	_bomb1.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_bomb1.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_bomb1.GetBody()->GetFixtureList()->SetRestitution( 0.0000f );
	_bomb1.GetBody()->SetAngularDamping(0.1000f );
	_bomb1.GetBody()->SetLinearDamping(0.1000f );
	_bomb1.SetBodyType( b2_dynamicBody );

	// _bomb2
	_bomb2.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_bomb2.GetBody()->GetFixtureList()->SetFriction( 0.9000f );
	_bomb2.GetBody()->GetFixtureList()->SetRestitution( 0.0000f );
	_bomb2.GetBody()->SetAngularDamping(0.1000f );
	_bomb2.GetBody()->SetLinearDamping(0.1000f );
	_bomb2.SetBodyType( b2_dynamicBody );

	// _bomb3
	_bomb3.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_bomb3.GetBody()->GetFixtureList()->SetFriction( 0.9000f );
	_bomb3.GetBody()->GetFixtureList()->SetRestitution( 0.0000f );
	_bomb3.GetBody()->SetAngularDamping(0.1000f );
	_bomb3.GetBody()->SetLinearDamping(0.1000f );
	_bomb3.SetBodyType( b2_dynamicBody );

	// _bomb4
	_bomb4.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_bomb4.GetBody()->GetFixtureList()->SetFriction( 0.9000f );
	_bomb4.GetBody()->GetFixtureList()->SetRestitution( 0.0000f );
	_bomb4.GetBody()->SetAngularDamping(0.1000f );
	_bomb4.GetBody()->SetLinearDamping(0.1000f );
	_bomb4.SetBodyType( b2_dynamicBody );

	// _bomb5
	_bomb5.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_bomb5.GetBody()->GetFixtureList()->SetFriction( 0.9000f );
	_bomb5.GetBody()->GetFixtureList()->SetRestitution( 0.0000f );
	_bomb5.GetBody()->SetAngularDamping(0.1000f );
	_bomb5.GetBody()->SetLinearDamping(0.1000f );
	_bomb5.SetBodyType( b2_dynamicBody );

	// _coin1
	_coin1.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin1.GetBody()->GetFixtureList()->SetFriction( 0.9000f );
	_coin1.GetBody()->GetFixtureList()->SetRestitution( 0.0000f );
	_coin1.GetBody()->SetAngularDamping(0.1000f );
	_coin1.GetBody()->SetLinearDamping(0.1000f );
	_coin1.SetBodyType( b2_dynamicBody );

	// _piggyBank1
	_piggyBank1.GetBody()->GetFixtureList()->SetDensity( 7.5000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetFriction( 0.9000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetRestitution( 0.0000f );
	_piggyBank1.GetBody()->SetAngularDamping(0.5000f );
	_piggyBank1.GetBody()->SetLinearDamping(0.5000f );
	_piggyBank1.FlipX();
	_piggyBank1.SetBodyType( b2_dynamicBody );

	// _wallBox1
	_wallBox1.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox1.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox1.SetBodyType( b2_staticBody );

	// _wallBox2
	_wallBox2.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox2.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox2.SetBodyType( b2_staticBody );

	// _wallBox3
	_wallBox3.SetRotation( 5.0000f );
	_wallBox3.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox3.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox3.GetBody()->GetFixtureList()->SetRestitution( 0.0000f );
	_wallBox3.SetBodyType( b2_staticBody );

	// _wallBox4
	_wallBox4.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox4.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox4.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox4.SetBodyType( b2_staticBody );

	// _wallBox5
	_wallBox5.SetRotation( 190.8000f );
	_wallBox5.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox5.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox5.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox5.SetBodyType( b2_staticBody );

	// _wallBox6
	_wallBox6.SetRotation( 180.4000f );
	_wallBox6.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox6.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox6.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox6.SetBodyType( b2_staticBody );

	// _wallBox7
	_wallBox7.SetRotation( 291.6000f );
	_wallBox7.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox7.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox7.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox7.SetBodyType( b2_staticBody );

	// _wallBox8
	_wallBox8.SetRotation( 306.0000f );
	_wallBox8.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox8.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox8.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox8.SetBodyType( b2_staticBody );

	// _wallBox9
	_wallBox9.SetRotation( 205.2000f );
	_wallBox9.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox9.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox9.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox9.SetBodyType( b2_staticBody );

	// _wallBox10
	_wallBox10.SetRotation( 219.6000f );
	_wallBox10.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox10.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox10.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox10.SetBodyType( b2_staticBody );

	// _wallBox11
	_wallBox11.SetRotation( 234.0000f );
	_wallBox11.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox11.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox11.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox11.SetBodyType( b2_staticBody );

	// _wallBox12
	_wallBox12.SetRotation( 248.4000f );
	_wallBox12.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox12.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox12.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox12.SetBodyType( b2_staticBody );

	// _wallBox13
	_wallBox13.SetRotation( 277.2000f );
	_wallBox13.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox13.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox13.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox13.SetBodyType( b2_staticBody );

	// _wallBox14
	_wallBox14.SetRotation( 262.8000f );
	_wallBox14.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox14.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox14.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox14.SetBodyType( b2_staticBody );

	// _wallBox15
	_wallBox15.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox15.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox15.GetBody()->GetFixtureList()->SetRestitution( 0.0000f );
	_wallBox15.SetBodyType( b2_staticBody );

	// _wallBox16
	_wallBox16.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox16.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox16.GetBody()->GetFixtureList()->SetRestitution( 0.0000f );
	_wallBox16.SetBodyType( b2_staticBody );

	// _wallBox17
	_wallBox17.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox17.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox17.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox17.SetBodyType( b2_staticBody );

	// _wallBox18
	_wallBox18.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox18.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox18.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox18.SetBodyType( b2_staticBody );

	// _wallBox19
	_wallBox19.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox19.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox19.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox19.SetBodyType( b2_staticBody );

	// _wallBox20
	_wallBox20.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox20.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox20.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox20.SetBodyType( b2_staticBody );

	// _wallBox21
	_wallBox21.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox21.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox21.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox21.SetBodyType( b2_staticBody );

	// _wallBox22
	_wallBox22.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox22.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox22.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox22.SetBodyType( b2_staticBody );

	// _wallBox23
	_wallBox23.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox23.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox23.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox23.SetBodyType( b2_staticBody );

	// _wallBox24
	_wallBox24.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox24.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox24.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox24.SetBodyType( b2_staticBody );

	// _wallBox25
	_wallBox25.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox25.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox25.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox25.SetBodyType( b2_staticBody );


	//Set blocks positions
	_bomb1.SetPosition( 72.3506f, 56.1002f );
	_bomb2.SetPosition( 60.6004f, 10.2001f );
	_bomb3.SetPosition( 60.6004f, 19.3001f );
	_bomb4.SetPosition( 60.6004f, 27.9006f );
	_bomb5.SetPosition( 60.6004f, 37.0997f );
	_coin1.SetPosition( 26.0506f, 35.2003f );
	_piggyBank1.SetPosition( 13.5504f, 34.5003f );
	_wallBox1.SetPosition( 72.3508f, 48.8499f );
	_wallBox2.SetPosition( 60.6004f, 4.7000f );
	_wallBox3.SetPosition( 40.3499f, 26.8500f );
	_wallBox4.SetPosition( 45.6999f, 48.8500f );
	_wallBox5.SetPosition( 75.5594f, 26.9409f );
	_wallBox6.SetPosition( 70.7436f, 26.6379f );
	_wallBox7.SetPosition( 89.8507f, 52.9362f );
	_wallBox8.SetPosition( 87.5261f, 57.1647f );
	_wallBox9.SetPosition( 80.1487f, 28.4319f );
	_wallBox10.SetPosition( 84.2229f, 31.0175f );
	_wallBox11.SetPosition( 87.5261f, 34.5350f );
	_wallBox12.SetPosition( 89.8507f, 38.7634f );
	_wallBox13.SetPosition( 91.0507f, 48.2625f );
	_wallBox14.SetPosition( 91.0507f, 43.4372f );
	_wallBox15.SetPosition( 31.9500f, 31.7500f );
	_wallBox16.SetPosition( 31.9500f, 37.7500f );
	_wallBox17.SetPosition( 31.9500f, 43.7500f );
	_wallBox18.SetPosition( 70.0501f, 31.7500f );
	_wallBox19.SetPosition( 70.0501f, 43.7500f );
	_wallBox20.SetPosition( 70.0501f, 37.7500f );
	_wallBox21.SetPosition( 4.9500f, 31.8000f );
	_wallBox22.SetPosition( 4.9500f, 43.8000f );
	_wallBox23.SetPosition( 4.9500f, 37.8000f );
	_wallBox24.SetPosition( 18.4500f, 26.6500f );
	_wallBox25.SetPosition( 18.4500f, 48.8500f );

	//Bombs Construction
	_bomb1.SetRange( 17.6000f );
	_bomb1.SetBombImpulse( 15200.0000f );
	_bomb2.SetRange( 16.0000f );
	_bomb2.SetBombImpulse( 10900.0000f );
	_bomb3.SetRange( 16.0000f );
	_bomb3.SetBombImpulse( 9625.0000f );
	_bomb4.SetRange( 16.0000f );
	_bomb4.SetBombImpulse( 9625.0000f );
	_bomb5.SetRange( 16.0000f );
	_bomb5.SetBombImpulse( 10200.0000f );

	ConstFinal();
}
//--------------------------------------------------------------------------
LevelLetsBomb_6::LevelLetsBomb_6( GameTypes::LevelEnum levelEnum, bool viewMode ) : Level( levelEnum, viewMode )
{
    _levelTag = eLevelTag_Bombs;
	
    // _bomb1
	_bomb1.SetRotation( -0.9000f );
	_bomb1.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_bomb1.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_bomb1.GetBody()->GetFixtureList()->SetRestitution( 0.0000f );
	_bomb1.SetBodyType( b2_dynamicBody );

	// _bomb2
	_bomb2.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_bomb2.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_bomb2.GetBody()->GetFixtureList()->SetRestitution( 0.0000f );
	_bomb2.SetBodyType( b2_dynamicBody );

	// _bomb3
	_bomb3.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_bomb3.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_bomb3.GetBody()->GetFixtureList()->SetRestitution( 0.0000f );
	_bomb3.SetBodyType( b2_dynamicBody );

	// _bomb4
	_bomb4.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_bomb4.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_bomb4.GetBody()->GetFixtureList()->SetRestitution( 0.0000f );
	_bomb4.SetBodyType( b2_dynamicBody );

	// _bomb5
	_bomb5.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_bomb5.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_bomb5.GetBody()->GetFixtureList()->SetRestitution( 0.0000f );
	_bomb5.SetBodyType( b2_dynamicBody );

	// _bomb6
	_bomb6.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_bomb6.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_bomb6.GetBody()->GetFixtureList()->SetRestitution( 0.0000f );
	_bomb6.SetBodyType( b2_dynamicBody );

	// _bomb7
	_bomb7.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_bomb7.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_bomb7.GetBody()->GetFixtureList()->SetRestitution( 0.0000f );
	_bomb7.SetBodyType( b2_dynamicBody );

	// _bomb8
	_bomb8.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_bomb8.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_bomb8.GetBody()->GetFixtureList()->SetRestitution( 0.0000f );
	_bomb8.SetBodyType( b2_dynamicBody );

	// _bomb9
	_bomb9.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_bomb9.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_bomb9.GetBody()->GetFixtureList()->SetRestitution( 0.0000f );
	_bomb9.SetBodyType( b2_dynamicBody );

	// _bomb10
	_bomb10.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_bomb10.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_bomb10.GetBody()->GetFixtureList()->SetRestitution( 0.0000f );
	_bomb10.SetBodyType( b2_dynamicBody );

	// _coin1
	_coin1.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin1.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin1.GetBody()->GetFixtureList()->SetRestitution( 0.0000f );
	_coin1.GetBody()->SetAngularDamping(0.6500f );
	_coin1.SetBodyType( b2_dynamicBody );

	// _piggyBankStatic1
	_piggyBankStatic1.GetBody()->GetFixtureList()->SetDensity( 7.5000f );
	_piggyBankStatic1.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_piggyBankStatic1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_piggyBankStatic1.FlipX();
	_piggyBankStatic1.SetBodyType( b2_staticBody );

	// _wallBox1
	_wallBox1.SetRotation( 90.0000f );
	_wallBox1.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox1.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox1.SetBodyType( b2_staticBody );

	// _wallBox2
	_wallBox2.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox2.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox2.SetBodyType( b2_staticBody );

	// _wallBox3
	_wallBox3.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox3.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox3.SetBodyType( b2_staticBody );


	// _wallBox4
	_wallBox4.SetRotation( 90.0000f );
	_wallBox4.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox4.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox4.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox4.SetBodyType( b2_staticBody );


	//Set blocks positions
	_bomb1.SetPosition( 31.9000f, 10.5502f );
	_bomb2.SetPosition( 88.0001f, 10.4502f );
	_bomb3.SetPosition( 76.0001f, 10.4502f );
	_bomb4.SetPosition( 82.0001f, 20.8502f );
	_bomb5.SetPosition( 70.0000f, 20.8502f );
	_bomb6.SetPosition( 64.0000f, 10.4502f );
	_bomb7.SetPosition( 25.9000f, 20.9502f );
	_bomb8.SetPosition( 7.9000f, 10.5502f );
	_bomb9.SetPosition( 19.9000f, 10.5502f );
	_bomb10.SetPosition( 13.9000f, 20.9502f );
	_coin1.SetPosition( 76.1001f, 31.7501f );
	_piggyBankStatic1.SetPosition( 19.9000f, 54.5501f );
	_wallBox1.SetPosition( 48.0000f, 23.9000f );
	_wallBox2.SetPosition( 19.9000f, 3.1500f );
	_wallBox3.SetPosition( 76.0004f, 3.1499f );
	_wallBox4.SetPosition( 48.0000f, 57.3500f );

	//Bombs Construction
	_bomb1.SetRange( 19.0000f );
	_bomb1.SetBombImpulse( 8750.0000f );
	_bomb2.SetRange( 19.0000f );
	_bomb2.SetBombImpulse( 10750.0000f );
	_bomb3.SetRange( 19.0000f );
	_bomb3.SetBombImpulse( 10750.0000f );
	_bomb4.SetRange( 19.0000f );
	_bomb4.SetBombImpulse( 10750.0000f );
	_bomb5.SetRange( 19.0000f );
	_bomb5.SetBombImpulse( 10750.0000f );
	_bomb6.SetRange( 19.0000f );
	_bomb6.SetBombImpulse( 10750.0000f );
	_bomb7.SetRange( 19.0000f );
	_bomb7.SetBombImpulse( 8900.0000f );
	_bomb8.SetRange( 19.0000f );
	_bomb8.SetBombImpulse( 8750.0000f );
	_bomb9.SetRange( 19.0000f );
	_bomb9.SetBombImpulse( 8750.0000f );
	_bomb10.SetRange( 19.0000f );
	_bomb10.SetBombImpulse( 8900.0000f );

	ConstFinal();
}
//--------------------------------------------------------------------------
LevelLetsBomb_Pile::LevelLetsBomb_Pile( GameTypes::LevelEnum levelEnum, bool viewMode ) : Level( levelEnum, viewMode )
{
    _levelTag = eLevelTag_Bombs;
	_coinCount = 5;
	_border.SetBorderSize( 30.0f );

	// _bomb1
	_bomb1.GetBody()->GetFixtureList()->SetDensity( 6.7000f );
	_bomb1.GetBody()->GetFixtureList()->SetFriction( 0.7500f );
	_bomb1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bomb1.SetBodyType( b2_dynamicBody );

	// _bomb2
	_bomb2.GetBody()->GetFixtureList()->SetDensity( 6.7000f );
	_bomb2.GetBody()->GetFixtureList()->SetFriction( 0.7500f );
	_bomb2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bomb2.SetBodyType( b2_dynamicBody );

	// _bomb3
	_bomb3.GetBody()->GetFixtureList()->SetDensity( 6.7000f );
	_bomb3.GetBody()->GetFixtureList()->SetFriction( 0.7500f );
	_bomb3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bomb3.SetBodyType( b2_dynamicBody );

	// _bomb4
	_bomb4.GetBody()->GetFixtureList()->SetDensity( 6.7000f );
	_bomb4.GetBody()->GetFixtureList()->SetFriction( 0.7500f );
	_bomb4.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bomb4.SetBodyType( b2_dynamicBody );

	// _bomb5
	_bomb5.GetBody()->GetFixtureList()->SetDensity( 6.7000f );
	_bomb5.GetBody()->GetFixtureList()->SetFriction( 0.7500f );
	_bomb5.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bomb5.SetBodyType( b2_dynamicBody );

	// _bomb6
	_bomb6.GetBody()->GetFixtureList()->SetDensity( 6.7000f );
	_bomb6.GetBody()->GetFixtureList()->SetFriction( 0.7500f );
	_bomb6.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bomb6.SetBodyType( b2_dynamicBody );

	// _bomb7
	_bomb7.GetBody()->GetFixtureList()->SetDensity( 6.7000f );
	_bomb7.GetBody()->GetFixtureList()->SetFriction( 0.7500f );
	_bomb7.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bomb7.SetBodyType( b2_dynamicBody );

	// _bomb8
	_bomb8.GetBody()->GetFixtureList()->SetDensity( 6.7000f );
	_bomb8.GetBody()->GetFixtureList()->SetFriction( 0.7500f );
	_bomb8.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bomb8.SetBodyType( b2_dynamicBody );

	// _bomb9
	_bomb9.GetBody()->GetFixtureList()->SetDensity( 6.7000f );
	_bomb9.GetBody()->GetFixtureList()->SetFriction( 0.7500f );
	_bomb9.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bomb9.SetBodyType( b2_dynamicBody );

	// _bomb10
	_bomb10.GetBody()->GetFixtureList()->SetDensity( 6.7000f );
	_bomb10.GetBody()->GetFixtureList()->SetFriction( 0.7500f );
	_bomb10.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bomb10.SetBodyType( b2_dynamicBody );

	// _bomb11
	_bomb11.GetBody()->GetFixtureList()->SetDensity( 6.7000f );
	_bomb11.GetBody()->GetFixtureList()->SetFriction( 0.7500f );
	_bomb11.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bomb11.SetBodyType( b2_dynamicBody );

	// _bomb12
	_bomb12.GetBody()->GetFixtureList()->SetDensity( 6.7000f );
	_bomb12.GetBody()->GetFixtureList()->SetFriction( 0.7500f );
	_bomb12.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bomb12.SetBodyType( b2_dynamicBody );

	// _bomb13
	_bomb13.GetBody()->GetFixtureList()->SetDensity( 6.7000f );
	_bomb13.GetBody()->GetFixtureList()->SetFriction( 0.7500f );
	_bomb13.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bomb13.GetBody()->SetAngularDamping( 0.075000f );
	_bomb13.GetBody()->SetLinearDamping(0.0500f );
	_bomb13.SetBodyType( b2_dynamicBody );

	// _bomb14
	_bomb14.GetBody()->GetFixtureList()->SetDensity( 6.7000f );
	_bomb14.GetBody()->GetFixtureList()->SetFriction( 0.7500f );
	_bomb14.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bomb14.GetBody()->SetAngularDamping( 0.075000f );
	_bomb14.GetBody()->SetLinearDamping(0.0500f );
	_bomb14.SetBodyType( b2_dynamicBody );

	// _bomb15
	_bomb15.GetBody()->GetFixtureList()->SetDensity( 6.7000f );
	_bomb15.GetBody()->GetFixtureList()->SetFriction( 0.7500f );
	_bomb15.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bomb15.SetBodyType( b2_dynamicBody );

	// _bomb16
	_bomb16.GetBody()->GetFixtureList()->SetDensity( 6.7000f );
	_bomb16.GetBody()->GetFixtureList()->SetFriction( 0.7500f );
	_bomb16.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bomb16.SetBodyType( b2_dynamicBody );

	// _bomb17
	_bomb17.GetBody()->GetFixtureList()->SetDensity( 6.7000f );
	_bomb17.GetBody()->GetFixtureList()->SetFriction( 0.7500f );
	_bomb17.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bomb17.SetBodyType( b2_dynamicBody );

	// _bomb18
	_bomb18.GetBody()->GetFixtureList()->SetDensity( 6.7000f );
	_bomb18.GetBody()->GetFixtureList()->SetFriction( 0.7500f );
	_bomb18.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bomb18.SetBodyType( b2_dynamicBody );

	// _bomb19
	_bomb19.GetBody()->GetFixtureList()->SetDensity( 6.7000f );
	_bomb19.GetBody()->GetFixtureList()->SetFriction( 0.7500f );
	_bomb19.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bomb19.SetBodyType( b2_dynamicBody );

	// _bomb20
	_bomb20.GetBody()->GetFixtureList()->SetDensity( 6.7000f );
	_bomb20.GetBody()->GetFixtureList()->SetFriction( 0.7500f );
	_bomb20.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bomb20.SetBodyType( b2_dynamicBody );

	// _bomb21
	_bomb21.GetBody()->GetFixtureList()->SetDensity( 6.7000f );
	_bomb21.GetBody()->GetFixtureList()->SetFriction( 0.7500f );
	_bomb21.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bomb21.SetBodyType( b2_dynamicBody );

	// _bomb22
	_bomb22.GetBody()->GetFixtureList()->SetDensity( 6.7000f );
	_bomb22.GetBody()->GetFixtureList()->SetFriction( 0.7500f );
	_bomb22.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bomb22.SetBodyType( b2_dynamicBody );

	// _coin1
	_coin1.GetBody()->GetFixtureList()->SetDensity( 6.7000f );
	_coin1.GetBody()->GetFixtureList()->SetFriction( 0.7500f );
	_coin1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_coin1.GetBody()->SetAngularDamping( 0.015f );
	_coin1.GetBody()->SetLinearDamping(0.015f );
	_coin1.SetBodyType( b2_dynamicBody );

	// _coin2
	_coin2.GetBody()->GetFixtureList()->SetDensity( 6.7000f );
	_coin2.GetBody()->GetFixtureList()->SetFriction( 0.7500f );
	_coin2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_coin2.GetBody()->SetAngularDamping( 0.015f );
	_coin2.GetBody()->SetLinearDamping(0.015f );
	_coin2.SetBodyType( b2_dynamicBody );

	// _coin3
	_coin3.GetBody()->GetFixtureList()->SetDensity( 6.7000f );
	_coin3.GetBody()->GetFixtureList()->SetFriction( 0.7500f );
	_coin3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_coin3.GetBody()->SetAngularDamping( 0.015f );
	_coin3.GetBody()->SetLinearDamping(0.015f );
	_coin3.SetBodyType( b2_dynamicBody );

	// _coin4
	_coin4.SetRotation( -0.4000f );
	_coin4.GetBody()->GetFixtureList()->SetDensity( 6.7000f );
	_coin4.GetBody()->GetFixtureList()->SetFriction( 0.7500f );
	_coin4.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_coin4.GetBody()->SetAngularDamping( 0.015f );
	_coin4.GetBody()->SetLinearDamping(0.015f );
	_coin4.SetBodyType( b2_dynamicBody );

	// _coin5
	_coin5.SetRotation( -0.4000f );
	_coin5.GetBody()->GetFixtureList()->SetDensity( 6.7000f );
	_coin5.GetBody()->GetFixtureList()->SetFriction( 0.7500f );
	_coin5.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_coin5.GetBody()->SetAngularDamping( 0.015f );
	_coin5.GetBody()->SetLinearDamping(0.015f );
	_coin5.SetBodyType( b2_dynamicBody );

	// _piggyBank1
	_piggyBank1.GetBody()->GetFixtureList()->SetDensity( 7.5000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_piggyBank1.GetBody()->SetAngularDamping( 0.015f );
	_piggyBank1.GetBody()->SetLinearDamping( 0.015f );
	_piggyBank1.SetBodyType( b2_dynamicBody );

	// _wallBox1
	_wallBox1.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox1.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox1.SetBodyType( b2_staticBody );

	// _wallBox2
	_wallBox2.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox2.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox2.SetBodyType( b2_staticBody );

	// _wallBox3
	_wallBox3.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox3.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox3.SetBodyType( b2_staticBody );


	//Set blocks positions
	_bomb1.SetPosition( 52.5002f, 17.4001f );
	_bomb2.SetPosition( 61.4002f, 17.4001f );
	_bomb3.SetPosition( 70.3005f, 17.4001f );
	_bomb4.SetPosition( 30.2002f, 24.9999f );
	_bomb5.SetPosition( 39.1002f, 24.9999f );
	_bomb6.SetPosition( 56.9002f, 24.9999f );
	_bomb7.SetPosition( 65.8003f, 24.9999f );
	_bomb8.SetPosition( 43.6002f, 32.5998f );
	_bomb9.SetPosition( 52.5002f, 32.5998f );
	_bomb10.SetPosition( 39.1002f, 40.1998f );
	_bomb11.SetPosition( 21.3002f, 9.8001f );
	_bomb12.SetPosition( 56.9002f, 40.1998f );
	_bomb13.SetPosition( 43.6002f, 47.7998f );
	_bomb14.SetPosition( 52.5002f, 47.7998f );
	_bomb15.SetPosition( 30.2002f, 9.8001f );
	_bomb16.SetPosition( 39.1002f, 9.8001f );
	_bomb17.SetPosition( 56.9002f, 9.8001f );
	_bomb18.SetPosition( 65.8003f, 9.8001f );
	_bomb19.SetPosition( 74.7006f, 9.8001f );
	_bomb20.SetPosition( 25.8002f, 17.4001f );
	_bomb21.SetPosition( 34.7002f, 17.4001f );
	_bomb22.SetPosition( 43.6002f, 17.4001f );
	_coin1.SetPosition( 47.9992f, 40.1998f );
	_coin2.SetPosition( 48.0007f, 24.9999f );
	_coin3.SetPosition( 48.0007f, 9.8000f );
	_coin4.SetPosition( 34.7020f, 32.5999f );
	_coin5.SetPosition( 61.4020f, 32.5999f );
	_piggyBank1.SetPosition( 48.0000f, 56.2499f );
	_wallBox1.SetPosition( 80.0003f, 6.5000f );
	_wallBox2.SetPosition( 16.0001f, 6.5000f );
	_wallBox3.SetPosition( 48.0001f, 3.5000f );

	//Bombs Construction
	_bomb1.SetRange( 14.7000f );
	_bomb1.SetBombImpulse( 6175.0000f );
	_bomb2.SetRange( 14.7000f );
	_bomb2.SetBombImpulse( 6175.0000f );
	_bomb3.SetRange( 14.7000f );
	_bomb3.SetBombImpulse( 6175.0000f );
	_bomb4.SetRange( 14.7000f );
	_bomb4.SetBombImpulse( 6175.0000f );
	_bomb5.SetRange( 14.7000f );
	_bomb5.SetBombImpulse( 6175.0000f );
	_bomb6.SetRange( 14.7000f );
	_bomb6.SetBombImpulse( 6175.0000f );
	_bomb7.SetRange( 14.7000f );
	_bomb7.SetBombImpulse( 6175.0000f );
	_bomb8.SetRange( 14.7000f );
	_bomb8.SetBombImpulse( 6175.0000f );
	_bomb9.SetRange( 14.7000f );
	_bomb9.SetBombImpulse( 6175.0000f );
	_bomb10.SetRange( 14.7000f );
	_bomb10.SetBombImpulse( 5275.0000f );
	_bomb11.SetRange( 14.7000f );
	_bomb11.SetBombImpulse( 6175.0000f );
	_bomb12.SetRange( 14.7000f );
	_bomb12.SetBombImpulse( 5275.0000f );
	_bomb13.SetRange( 14.7000f );
	_bomb13.SetBombImpulse( 6175.0000f );
	_bomb14.SetRange( 14.7000f );
	_bomb14.SetBombImpulse( 6175.0000f );
	_bomb15.SetRange( 14.7000f );
	_bomb15.SetBombImpulse( 6175.0000f );
	_bomb16.SetRange( 14.7000f );
	_bomb16.SetBombImpulse( 6175.0000f );
	_bomb17.SetRange( 14.7000f );
	_bomb17.SetBombImpulse( 6175.0000f );
	_bomb18.SetRange( 14.7000f );
	_bomb18.SetBombImpulse( 6175.0000f );
	_bomb19.SetRange( 14.7000f );
	_bomb19.SetBombImpulse( 6175.0000f );
	_bomb20.SetRange( 14.7000f );
	_bomb20.SetBombImpulse( 6175.0000f );
	_bomb21.SetRange( 14.7000f );
	_bomb21.SetBombImpulse( 6175.0000f );
	_bomb22.SetRange( 14.7000f );
	_bomb22.SetBombImpulse( 6175.0000f );

	_wallBox1.ResetSpriteZOrder( GameTypes::eSpritesWallAbove );
	_wallBox2.ResetSpriteZOrder( GameTypes::eSpritesWallAbove );

    _velocityIterations = 3;
	_positionIterations = 3;
    
	ConstFinal();
}
//--------------------------------------------------------------------------
LevelLetsBomb_8::LevelLetsBomb_8( GameTypes::LevelEnum levelEnum, bool viewMode ) : Level( levelEnum, viewMode )
{
    _levelTag = eLevelTag_Bombs;
	_coinCount = 4;

	// _bomb1
	_bomb1.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_bomb1.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_bomb1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bomb1.SetBodyType( b2_dynamicBody );

	// _bomb2
	_bomb2.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_bomb2.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_bomb2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bomb2.SetBodyType( b2_dynamicBody );

	// _bomb3
	_bomb3.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_bomb3.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_bomb3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bomb3.SetBodyType( b2_dynamicBody );

	// _bomb4
	_bomb4.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_bomb4.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_bomb4.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bomb4.SetBodyType( b2_dynamicBody );

	// _coin1
	_coin1.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin1.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin1.SetBodyType( b2_dynamicBody );

	// _coin2
	_coin2.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin2.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin2.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin2.SetBodyType( b2_dynamicBody );

	// _coin3
	_coin3.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin3.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin3.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin3.SetBodyType( b2_dynamicBody );

	// _coinStatic1
	_coinStatic1.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coinStatic1.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coinStatic1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coinStatic1.SetBodyType( b2_staticBody );

	// _piggyBank1
	_piggyBank1.GetBody()->GetFixtureList()->SetDensity( 7.5000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_piggyBank1.FlipX();
	_piggyBank1.SetBodyType( b2_dynamicBody );

	// _piggyBank2
	_piggyBank2.GetBody()->GetFixtureList()->SetDensity( 7.5000f );
	_piggyBank2.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_piggyBank2.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_piggyBank2.SetBodyType( b2_dynamicBody );

	// _piggyBank3
	_piggyBank3.GetBody()->GetFixtureList()->SetDensity( 7.5000f );
	_piggyBank3.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_piggyBank3.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_piggyBank3.SetBodyType( b2_dynamicBody );

	// _wallBox1
	_wallBox1.SetRotation( 5.0000f );
	_wallBox1.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox1.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox1.SetBodyType( b2_staticBody );

	// _wallBox2
	_wallBox2.SetRotation( -5.0000f );
	_wallBox2.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox2.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox2.SetBodyType( b2_staticBody );

	// _wallBox3
	_wallBox3.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox3.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox3.SetBodyType( b2_staticBody );

	// _wallBox4
	_wallBox4.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox4.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox4.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox4.SetBodyType( b2_staticBody );

	// _wallBox5
	_wallBox5.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox5.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox5.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox5.SetBodyType( b2_staticBody );

	// _wallBox6
	_wallBox6.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox6.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox6.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox6.SetBodyType( b2_staticBody );

	// _wallBox7
	_wallBox7.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox7.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox7.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox7.SetBodyType( b2_staticBody );

	// _wallBox8
	_wallBox8.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox8.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox8.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox8.SetBodyType( b2_staticBody );

	// _wallBox9
	_wallBox9.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox9.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox9.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox9.SetBodyType( b2_staticBody );


	//Set blocks positions
	_bomb1.SetPosition( 39.5002f, 14.9498f );
	_bomb2.SetPosition( 57.5005f, 14.9998f );
	_bomb3.SetPosition( 53.0004f, 24.3497f );
	_bomb4.SetPosition( 43.0002f, 24.3499f );
	_coin1.SetPosition( 20.0002f, 14.5000f );
	_coin2.SetPosition( 75.9999f, 14.5000f );
	_coin3.SetPosition( 48.0003f, 14.9998f );
	_coinStatic1.SetPosition( 48.0000f, 52.3000f );
	_piggyBank1.SetPosition( 7.8000f, 15.9000f );
	_piggyBank2.SetPosition( 48.0001f, 35.0001f );
	_piggyBank3.SetPosition( 88.1999f, 15.9000f );
	_wallBox1.SetPosition( 34.3498f, 9.0000f );
	_wallBox2.SetPosition( 61.6503f, 9.0001f );
	_wallBox3.SetPosition( 25.0000f, 25.0000f );
	_wallBox4.SetPosition( 25.0000f, 17.9999f );
	_wallBox5.SetPosition( 25.0000f, 32.0003f );
	_wallBox6.SetPosition( 71.0003f, 32.0003f );
	_wallBox7.SetPosition( 71.0003f, 25.0000f );
	_wallBox8.SetPosition( 71.0003f, 17.9999f );
	_wallBox9.SetPosition( 48.0000f, 7.7000f );

	//Bombs Construction
	_bomb1.SetRange( 16.0000f );
	_bomb1.SetBombImpulse( 11000.0000f );
	_bomb2.SetRange( 16.0000f );
	_bomb2.SetBombImpulse( 11000.0000f );
	_bomb3.SetRange( 15.9000f );
	_bomb3.SetBombImpulse( 15425.0000f );
	_bomb4.SetRange( 15.9000f );
	_bomb4.SetBombImpulse( 16325.0000f );

	_wallBox1.ResetSpriteZOrder( GameTypes::eSpritesWallAbove );
	_wallBox2.ResetSpriteZOrder( GameTypes::eSpritesWallAbove );

	ConstFinal();
}
//--------------------------------------------------------------------------
