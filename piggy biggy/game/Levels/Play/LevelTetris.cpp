#include "LevelTetris.h"

//--------------------------------------------------------------------------
// LevelTetris_1
//--------------------------------------------------------------------------
LevelTetris_1::LevelTetris_1( GameTypes::LevelEnum levelEnum, bool viewMode ) : LevelWithCallback( levelEnum, viewMode, 8 )
{
    _levelTag = eLevelTag_Tetris;
    
	// _piggyBank1
	_piggyBank1.GetBody()->GetFixtureList()->SetDensity( 7.5f );
	_piggyBank1.GetBody()->GetFixtureList()->SetFriction( 0.4f );
	_piggyBank1.GetBody()->GetFixtureList()->SetRestitution( 0.2f );
	_piggyBank1.SetBodyType( b2_dynamicBody );

	// _coin1
	_coin1.GetBody()->GetFixtureList()->SetDensity( 3.5f );
	_coin1.GetBody()->GetFixtureList()->SetFriction( 0.6f );
	_coin1.GetBody()->GetFixtureList()->SetRestitution( 0.2f );
	_coin1.SetBodyType( b2_dynamicBody );

	// _wall3
	_wall3.SetRotation( -8 );
	_wall3.GetBody()->GetFixtureList()->SetDensity( 8.0f );
	_wall3.GetBody()->GetFixtureList()->SetFriction( 0.8f );
	_wall3.GetBody()->GetFixtureList()->SetRestitution( 0.1f );
	_wall3.SetBodyType( b2_staticBody );

	// _wall4
	_wall4.GetBody()->GetFixtureList()->SetDensity( 8.0f );
	_wall4.GetBody()->GetFixtureList()->SetFriction( 0.8f );
	_wall4.GetBody()->GetFixtureList()->SetRestitution( 0.1f );
	_wall4.SetBodyType( b2_staticBody );

	// _fragileBox1
	_fragileBox1.GetBody()->GetFixtureList()->SetDensity( 9.0f );
	_fragileBox1.GetBody()->GetFixtureList()->SetFriction( 0.7f );
	_fragileBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1f );
	_fragileBox1.SetBodyType( b2_dynamicBody );


	//Floor 
	_floor11.GetBody()->GetFixtureList()->SetFriction( 0.1f );
	_floor11.GetBody()->GetFixtureList()->SetRestitution( 0.0f );

	// Set blocks container ( falling order )
	
	_blocks.push_back( &_tetrisLM1 );
	_blocks.push_back( &_fake1 );

	//_blocks.push_back( &_fake2 );
	_blocks.push_back( &_fake3 );
	_blocks.push_back( &_tetrisS1 );
	_blocks.push_back( &_fake4 );

	_blocks.push_back( &_tetrisSM1 );
	_blocks.push_back( &_fake5 );
	_blocks.push_back( &_fake6 );

	_blocks.push_back( &_fake7 );
	_blocks.push_back( &_tetrisSM2 );
	_blocks.push_back( &_tetrisT1 );
	
	_blocks.push_back( &_tetrisLM2 );
	_blocks.push_back( &_fake9 );
	_blocks.push_back( &_fake10 );
	_blocks.push_back( &_fake8 );
	_blocks.push_back( &_fake11 );
	_blocks.push_back( &_tetrisSquare1 );

	_blocks.push_back( &_tetrisSM3 );
	_blocks.push_back( &_fake12 );
	_blocks.push_back( &_tetrisL1 );
	_blocks.push_back( &_fake13 );
	_blocks.push_back( &_tetrisLM3 );
	_blocks.push_back( &_fake14 );

	_fallingBlock = _blocks.begin();


	// Set blocks rotation
	// 0, 90, 180, ...
	_tetrisLM1.SetRotation( eRot270 );
	_fake1.SetRotation( eRot0 );
	//_fake2.SetRotation( eRot90 );
	_fake3.SetRotation( eRot90 );
	_fake4.SetRotation( eRot270 );

	_fake8.SetRotation( eRot90 );
	_fake9.SetRotation( eRot180 );
	_fake11.SetRotation( eRot90 );

	_tetrisS1.SetRotation( eRot90 );
	_tetrisT1.SetRotation( eRot270 );
	_tetrisLM2.SetRotation( eRot270 );
	_tetrisLM3.SetRotation( eRot90 );
	_tetrisL1.SetRotation( eRot270 );

	// Set Tetris blocks position 
	float baseY = 72.0f;
	float baseX = 30.5f;

	_tetrisLM1.SetStartPos( baseX + 3 * puTetrisBase::_sBlockSizeTotal, baseY -2.0f	 );
	_fake1.SetStartPos( baseX + 0 * puTetrisBase::_sBlockSizeTotal, baseY  );
	//_fake2.SetStartPos( baseX + 4 * puTetrisBase::_sBlockSizeTotal, baseY );
	_fake3.SetStartPos( baseX + 5 * puTetrisBase::_sBlockSizeTotal, baseY );
	_tetrisS1.SetStartPos( baseX + 6 * puTetrisBase::_sBlockSizeTotal, baseY );
	_fake4.SetStartPos( baseX + 8 * puTetrisBase::_sBlockSizeTotal, baseY );

	_tetrisSM1.SetStartPos( baseX + puTetrisBase::_sBlockSizeTotal, baseY );
	_fake5.SetStartPos( baseX + 2 * puTetrisBase::_sBlockSizeTotal, baseY + 2.0f );
	_fake6.SetStartPos( baseX + 6 * puTetrisBase::_sBlockSizeTotal, baseY + 2.0f );
	
	_fake7.SetStartPos( baseX + 0 * puTetrisBase::_sBlockSizeTotal, baseY + 3.0f );
	_tetrisSM2.SetStartPos( baseX + 4 * puTetrisBase::_sBlockSizeTotal, baseY + 3.0f );
	_tetrisT1.SetStartPos( baseX + 8 * puTetrisBase::_sBlockSizeTotal, baseY + 0.0f );

	_fake8.SetStartPos( baseX + 0 * puTetrisBase::_sBlockSizeTotal, baseY + 8.0f );
	_tetrisLM2.SetStartPos( baseX + 2 * puTetrisBase::_sBlockSizeTotal, baseY + 12.0f );
	_fake9.SetStartPos( baseX + 5 * puTetrisBase::_sBlockSizeTotal, baseY + 8.0f );
	_fake10.SetStartPos( baseX + 6 * puTetrisBase::_sBlockSizeTotal, baseY );
	_fake11.SetStartPos( baseX + 3 * puTetrisBase::_sBlockSizeTotal, baseY + 8.0f );
	_tetrisSquare1.SetStartPos( baseX + 7 * puTetrisBase::_sBlockSizeTotal, baseY + 4.0f );
	
	_tetrisSM3.SetStartPos( baseX + 4 * puTetrisBase::_sBlockSizeTotal, baseY + 10.0f );
	_fake12.SetStartPos( baseX + 1 * puTetrisBase::_sBlockSizeTotal, baseY + 15.0f );
	_tetrisL1.SetStartPos( baseX + 6 * puTetrisBase::_sBlockSizeTotal, baseY + 15.0f );
	_fake13.SetStartPos( baseX + 7 * puTetrisBase::_sBlockSizeTotal, baseY + 18.0f );
	_tetrisLM3.SetStartPos( baseX + 0 * puTetrisBase::_sBlockSizeTotal, baseY + 18.0f );
	_fake14.SetStartPos( baseX + 4 * puTetrisBase::_sBlockSizeTotal, baseY + 18.0f );
	
	// Set normal blocks position
	_coin1.SetPosition( 3.7f, 22.4f );
	_piggyBank1.SetPosition( 41.7f, 20.2f );
	_wall3.SetPosition( 6.95f, 18.0f );
	_wall4.SetPosition( 41.7f, 16.6f );
	_fragileBox1.SetPosition( 8.5f, 21.8f );
	_floor11.SetPosition( 50.05f / 2.0f, 2.0f );

	// Set tetris block static 
	puTetrisBase *tetrisBlock;
	BlockContainer::iterator it;
	
	for ( it = _blocks.begin(); it != _blocks.end(); it ++ )
	{
		(*it)->SetBodyType( b2_staticBody );
		tetrisBlock = ( puTetrisBase *) (*it);
		tetrisBlock->SetGround( &_floor11 );
		tetrisBlock->SetPosition( -1000, -1000 );
	}

	ConstFinal();

	//RandomHandTap();
	_msgManager.push_back( eOTMsg_IntroTetirs, new LevelMsg_MovingPointingCircle( 
		Config::LevelFirstMsgTick + 190, 
		2.0f, 
		"", 
		b2Vec2( 0.0f, 0.0f ), 
		b2Vec2( 40.0f, 10.0f ), 
		b2Vec2( 20.0f, 45.0f ) 
		));

	AdjustOldScreen();
}
//--------------------------------------------------------------------------
LevelTetris_1::~LevelTetris_1()
{
	_blocks.clear();
}

//--------------------------------------------------------------------------
void LevelTetris_1::CounterCallback()
{
	if ( _fallingBlock != _blocks.end() )
	{
		((puTetrisBase *) (*_fallingBlock))->StartFalling();
		_fallingBlock++;
	}
}
//--------------------------------------------------------------------------
void LevelTetris_1::RandomHandTap()
{
	int initDelay;
	int deltaDelay;
	int count;
	
	float dur;
	b2Vec2 randomPos;
	b2Vec2 startPos;
	b2Vec2 posRange;

	dur = 0.5f;
	count = 3;
	initDelay = Config::LevelFirstMsgTick + 200;
	deltaDelay = 30;

	startPos.Set( 30.0f, 10.0f );
	posRange.Set( 10.0f, 10.0f );

	randomPos.x = ( float ) ( ( int ) startPos.x + rand() % ( int ) posRange.x );
	randomPos.y = ( float ) ( ( int ) startPos.y + rand() % ( int ) posRange.y );

	_msgManager.push_back( eOTMsg_IntroTetirs, new LevelMsg_PointingCircle( 
		initDelay, 
		2.0f, 
		"", 
		b2Vec2( 0.0f, 0.0f ), 
		randomPos
		));


	startPos.Set( 50.0f, 45.0f );
	posRange.Set( 5.0f, 5.0f );

	randomPos.x = ( float ) ( ( int ) startPos.x + rand() % ( int ) posRange.x );
	randomPos.y = ( float ) ( ( int ) startPos.y + rand() % ( int ) posRange.y );

	_msgManager.push_back( eOTMsg_IntroTetirs, new LevelMsg_PointingCircle( 
		initDelay + deltaDelay, 
		2.0f, 
		"", 
		b2Vec2( 0.0f, 0.0f ), 
		randomPos
		));


	startPos.Set( 60.0f, 20.0f );
	posRange.Set( 10.0f, 10.0f );

	randomPos.x = ( float ) ( ( int ) startPos.x + rand() % ( int ) posRange.x );
	randomPos.y = ( float ) ( ( int ) startPos.y + rand() % ( int ) posRange.y );

//	_msgManager.push_back( eOTMsg_IntroTetirs, new LevelMsg_PointingCircle( 
//		initDelay + 2 * deltaDelay, 
//		2.0f, 
//		"", 
//		b2Vec2( 0.0f, 0.0f ), 
//		randomPos
//		));
}

//--------------------------------------------------------------------------



//--------------------------------------------------------------------------
// LevelTetris_2
//--------------------------------------------------------------------------
LevelTetris_2::LevelTetris_2( GameTypes::LevelEnum levelEnum, bool viewMode ) : LevelWithCallback( levelEnum, viewMode, 8 )
{
    _levelTag = eLevelTag_Tetris;
    
	// _piggyBank1
	_piggyBank1.GetBody()->GetFixtureList()->SetDensity( 7.5f );
	_piggyBank1.GetBody()->GetFixtureList()->SetFriction( 0.4f );
	_piggyBank1.GetBody()->GetFixtureList()->SetRestitution( 0.2f );
	_piggyBank1.SetBodyType( b2_dynamicBody );

	// _coin1
	_coin1.GetBody()->GetFixtureList()->SetDensity( 3.5f );
	_coin1.GetBody()->GetFixtureList()->SetFriction( 0.6f );
	_coin1.GetBody()->GetFixtureList()->SetRestitution( 0.2f );
	_coin1.SetBodyType( b2_dynamicBody );

	// _wallLeftUp
	_wallLeftUp.SetRotation( -8 );
	_wallLeftUp.GetBody()->GetFixtureList()->SetDensity( 8.0f );
	_wallLeftUp.GetBody()->GetFixtureList()->SetFriction( 0.8f );
	_wallLeftUp.GetBody()->GetFixtureList()->SetRestitution( 0.1f );
	_wallLeftUp.SetBodyType( b2_staticBody );

	// _wallLeftDown
	_wallLeftDown.SetRotation( -8 );
	_wallLeftDown.GetBody()->GetFixtureList()->SetDensity( 8.0f );
	_wallLeftDown.GetBody()->GetFixtureList()->SetFriction( 0.8f );
	_wallLeftDown.GetBody()->GetFixtureList()->SetRestitution( 0.1f );
	_wallLeftDown.SetBodyType( b2_staticBody );

	// _wallRightUp
	_wallRightUp.GetBody()->GetFixtureList()->SetDensity( 8.0f );
	_wallRightUp.GetBody()->GetFixtureList()->SetFriction( 0.8f );
	_wallRightUp.GetBody()->GetFixtureList()->SetRestitution( 0.1f );
	_wallRightUp.SetBodyType( b2_staticBody );

	// _wallRightDown
	_wallRightDown.GetBody()->GetFixtureList()->SetDensity( 8.0f );
	_wallRightDown.GetBody()->GetFixtureList()->SetFriction( 0.8f );
	_wallRightDown.GetBody()->GetFixtureList()->SetRestitution( 0.1f );
	_wallRightDown.SetBodyType( b2_staticBody );

	// _fragileBox1
	_fragileBox1.GetBody()->GetFixtureList()->SetDensity( 9.0f );
	_fragileBox1.GetBody()->GetFixtureList()->SetFriction( 0.7f );
	_fragileBox1.GetBody()->GetFixtureList()->SetRestitution( 0.01f );
	_fragileBox1.SetBodyType( b2_dynamicBody );


	//Floor 
	_floor11.GetBody()->GetFixtureList()->SetFriction( 0.1f );
	_floor11.GetBody()->GetFixtureList()->SetRestitution( 0.0f );

	// Doors filters
	b2Filter filter;
	filter.groupIndex = 0;
	filter.categoryBits = Config::eFilterCategoryDoor;
	filter.maskBits = ~Config::eFilterCategoryDoor & 0xffff;

	_wallRightDown.GetBody()->GetFixtureList()->SetFilterData( filter );
	_wallRightUp.GetBody()->GetFixtureList()->SetFilterData( filter );
	//_wallBox4.GetBody()->GetFixtureList()->SetFilterData( filter );


	// Set blocks container ( falling order )
	_blocks.push_back( &_tetrisL1 );
	_blocks.push_back( &_fake1 );
	_blocks.push_back( &_tetrisSquare1 );
	_blocks.push_back( &_fake2 );
	_blocks.push_back( &_tetrisSM1 );
	_blocks.push_back( &_fake5 );
	_blocks.push_back( &_tetrisLM1 );
	_blocks.push_back( &_tetrisSM2 );
	_blocks.push_back( &_tetrisSquare2 );
	_blocks.push_back( &_fake3 );
	_blocks.push_back( &_tetrisT2 );
	_blocks.push_back( &_tetrisT1 );
	_blocks.push_back( &_tetrisI1 );
	_blocks.push_back( &_tetrisLM2 );
	_blocks.push_back( &_fake4 );
	_blocks.push_back( &_tetrisS1 );
	_blocks.push_back( &_fake6 );
	_blocks.push_back( &_fake8 );
	_blocks.push_back( &_tetrisSM3 );
	_blocks.push_back( &_fake7 );
	_blocks.push_back( &_fake9 );
	_blocks.push_back( &_fake10 );
	_blocks.push_back( &_tetrisI2 );
	_blocks.push_back( &_fake11 );
	_blocks.push_back( &_fake12 );

	_fallingBlock = _blocks.begin();


	// Set blocks rotation
	// 0, 90, 180, ...
	_tetrisT1.SetRotation( eRot90 );
	_tetrisI1.SetRotation( eRot90 );
	_tetrisLM2.SetRotation( eRot90 );
	_tetrisL1.SetRotation( eRot90 );
	_tetrisT2.SetRotation( eRot270 );
	_tetrisSM2.SetRotation( eRot180 );

	_fake1.SetRotation( eRot90 );
	_fake5.SetRotation( eRot180 );
	_fake6.SetRotation( eRot180 );
	_fake8.SetRotation( eRot90 );
	_fake9.SetRotation( eRot90 );
	_fake11.SetRotation( eRot270 );
	_fake12.SetRotation( eRot180 );


	// Set Tetris blocks position 
	float baseY = 73.0f;
	float baseX = 31.0f;
		float xAling = 0.5f;

	_tetrisSquare1.SetStartPos( baseX - xAling, baseY  );
	_tetrisL1.SetStartPos( baseX- xAling + 2 * puTetrisBase::_sBlockSizeTotal, baseY );
	_fake1.SetStartPos( baseX - xAling+ 3 * puTetrisBase::_sBlockSizeTotal, baseY );
	_fake2.SetStartPos( baseX - xAling + 6 * puTetrisBase::_sBlockSizeTotal, baseY );
	_tetrisSM1.SetStartPos( baseX - xAling + 4 * puTetrisBase::_sBlockSizeTotal, baseY + 6.0f );
	_fake5.SetStartPos( baseX - xAling + 7 * puTetrisBase::_sBlockSizeTotal, baseY );
	_tetrisLM1.SetStartPos( baseX - xAling + 7 * puTetrisBase::_sBlockSizeTotal, baseY + 2.0f );
	_tetrisSM2.SetStartPos( baseX - xAling + 7 * puTetrisBase::_sBlockSizeTotal, baseY + 6.0f );
	_tetrisSquare2.SetStartPos( baseX - xAling + 3 * puTetrisBase::_sBlockSizeTotal, baseY + 8.0f );
	_fake3.SetStartPos( baseX - xAling, baseY );

	_tetrisLM2.SetStartPos( baseX - xAling + 5 * puTetrisBase::_sBlockSizeTotal, baseY );
	_tetrisT1.SetStartPos( baseX - xAling + 1 * puTetrisBase::_sBlockSizeTotal, baseY + 9.0f );
	_tetrisT2.SetStartPos( baseX - xAling + 8 * puTetrisBase::_sBlockSizeTotal, baseY + 9.0f );
	_fake4.SetStartPos( baseX - xAling + 3 * puTetrisBase::_sBlockSizeTotal, baseY + 9.0f );
	_tetrisS1.SetStartPos( baseX - xAling + 6 * puTetrisBase::_sBlockSizeTotal, baseY + 7.0f );
	_tetrisSM3.SetStartPos( baseX - xAling + 2 * puTetrisBase::_sBlockSizeTotal, baseY + 5.0f );
	_fake8.SetStartPos( baseX - xAling + 0 * puTetrisBase::_sBlockSizeTotal, baseY + 8.0f );

	_tetrisI1.SetStartPos( baseX - xAling + 0 * puTetrisBase::_sBlockSizeTotal, baseY );
	_fake6.SetStartPos( baseX - xAling + 7 * puTetrisBase::_sBlockSizeTotal, baseY + 12.0f );
	_fake7.SetStartPos( baseX - xAling + 4 * puTetrisBase::_sBlockSizeTotal, baseY );

	_fake9.SetStartPos( baseX - xAling + 2 * puTetrisBase::_sBlockSizeTotal, baseY + 14.0f );
	_fake10.SetStartPos( baseX - xAling + 7 * puTetrisBase::_sBlockSizeTotal, baseY + 8.0f );
	
	_tetrisI2.SetStartPos( baseX - xAling + 4 * puTetrisBase::_sBlockSizeTotal, baseY + 14.5f );
	_fake11.SetStartPos( baseX  - xAling+ 1 * puTetrisBase::_sBlockSizeTotal, baseY + 12.0f );
	_fake12.SetStartPos( baseX - xAling + 7 * puTetrisBase::_sBlockSizeTotal, baseY + 14.5f );

	// Set normal blocks position
	_thief.FlipX();
	_thief.SetPosition( 4.2f, 23.0f );
	_coin1.SetPosition( 4.2f, 16.0f );
	_fragileBox1.SetPosition( 9.0f, 21.8f );
	_fragileBox2.SetPosition( 9.0f, 15.50f );
	
	_doorSwitch1.SetTarget( &_door1 );

	// Set tetris block static 
	puTetrisBase *tetrisBlock;
	BlockContainer::iterator it;

    D_SIZE( _blocks )
	for ( it = _blocks.begin(); it != _blocks.end(); it ++ )
	{
		(*it)->SetBodyType( b2_staticBody );
		tetrisBlock = ( puTetrisBase *) (*it);
		tetrisBlock->SetGround( &_floor11 );
		tetrisBlock->SetPosition( -1000, -1000 );
	}

	AdjustOldScreen();
	_door1.SetPosition( 76.8f, 27.75f );
	_doorSwitch1.SetPosition( 80.0f, 38.4f );

	_floor11.SetPosition( 50.4f, 4.0f );
	_fragileBox3.SetPosition( 88.25f, 40.0f );
	_piggyBank1.SetPosition( 88.25f, 28.3f );
	_wallLeftUp.SetPosition( 14.4f, 36.4f );
	_wallLeftDown.SetPosition( 14.4f, 25.3f );

	_wallRightUp.SetPosition( 83.0f, 33.0f );
	_wallRightDown.SetPosition( 83.0f, 22.5f );


	ConstFinal();
}
//--------------------------------------------------------------------------
LevelTetris_2::~LevelTetris_2()
{
	_blocks.clear();
	_contactSolver.Quiting();
}
//--------------------------------------------------------------------------
void LevelTetris_2::CounterCallback()
{
	if ( _fallingBlock != _blocks.end() )
	{
		((puTetrisBase *) (*_fallingBlock))->StartFalling();
		_fallingBlock++;
	}
}
