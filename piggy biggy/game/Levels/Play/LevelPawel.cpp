#include "LevelPawel.h"

//--------------------------------------------------------------------------
LevelPawel_1::LevelPawel_1( GameTypes::LevelEnum levelEnum, bool viewMode ) : LevelActivable( levelEnum, viewMode )
{
	// _coin1
	_coin1.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin1.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin1.SetBodyType( b2_dynamicBody );

	// _fragileBox1
	_fragileBox1.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox1.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox1.SetBodyType( b2_dynamicBody );

	// _fragileBox2
	_fragileBox2.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox2.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox2.SetBodyType( b2_dynamicBody );

	// _fragileBox3
	_fragileBox3.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox3.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox3.SetBodyType( b2_dynamicBody );

	// _fragileBox4
	_fragileBox4.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox4.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox4.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox4.SetBodyType( b2_dynamicBody );

	// _fragileBox5
	_fragileBox5.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox5.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox5.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox5.SetBodyType( b2_dynamicBody );

	// _fragileBox6
	_fragileBox6.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox6.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox6.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox6.SetBodyType( b2_dynamicBody );

	// _fragileBox7
	_fragileBox7.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox7.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox7.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox7.SetBodyType( b2_dynamicBody );

	// _piggyBank1
	_piggyBank1.GetBody()->GetFixtureList()->SetDensity( 6.2000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetFriction( 0.5500f );
	_piggyBank1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_piggyBank1.SetBodyType( b2_dynamicBody );

	// _wallBox1
	_wallBox1.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox1.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox1.SetBodyType( b2_staticBody );

	// _wallBox2
	_wallBox2.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox2.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox2.SetBodyType( b2_staticBody );

	// _wallBox3
	_wallBox3.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox3.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox3.SetBodyType( b2_staticBody );

	// _wallBox4
	_wallBox4.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox4.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox4.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox4.SetBodyType( b2_staticBody );

	// _wallBox5
	_wallBox5.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox5.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox5.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox5.SetBodyType( b2_staticBody );

	// _wallBox6
	_wallBox6.SetRotation( 10.0000f );
	_wallBox6.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox6.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox6.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox6.SetBodyType( b2_staticBody );

	// _wallBox7
	_wallBox7.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox7.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox7.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox7.SetBodyType( b2_staticBody );

	// _wallBox8
	_wallBox8.SetRotation( 30.0000f );
	_wallBox8.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox8.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox8.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox8.SetBodyType( b2_staticBody );

	// _wallBox9
	_wallBox9.SetRotation( 110.0000f );
	_wallBox9.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox9.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox9.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox9.SetBodyType( b2_staticBody );

	// _wallBox10
	_wallBox10.SetRotation( 170.0000f );
	_wallBox10.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox10.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox10.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox10.SetBodyType( b2_staticBody );

	// _wallBox11
	_wallBox11.SetRotation( 180.0000f );
	_wallBox11.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox11.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox11.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox11.SetBodyType( b2_staticBody );

	// _wallBox12
	_wallBox12.SetRotation( 130.0000f );
	_wallBox12.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox12.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox12.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox12.SetBodyType( b2_staticBody );

	// _wallBox13
	_wallBox13.SetRotation( 150.0000f );
	_wallBox13.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox13.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox13.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox13.SetBodyType( b2_staticBody );

	// _wallBox14
	_wallBox14.SetRotation( 50.0000f );
	_wallBox14.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox14.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox14.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox14.SetBodyType( b2_staticBody );

	// _wallBox15
	_wallBox15.SetRotation( 70.0000f );
	_wallBox15.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox15.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox15.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox15.SetBodyType( b2_staticBody );

	// _wallBox16
	_wallBox16.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox16.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox16.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox16.SetBodyType( b2_staticBody );

	// _woodBox1
	_woodBox1.SetRotation( 90.0000f );
	_woodBox1.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox1.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_woodBox1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_woodBox1.SetBodyType( b2_dynamicBody );


	//Set blocks positions
	_coin1.SetPosition( 19.4000f, 69.3500f );
	_fragileBox1.SetPosition( 47.9998f, 54.6501f );
	_fragileBox2.SetPosition( 55.3505f, 11.4500f );
	_fragileBox3.SetPosition( 80.2508f, 11.4500f );
	_fragileBox4.SetPosition( 73.7507f, 21.1500f );
	_fragileBox5.SetPosition( 61.1505f, 21.1500f );
	_fragileBox6.SetPosition( 67.8505f, 11.4500f );
	_fragileBox7.SetPosition( 47.9998f, 44.9001f );
	_piggyBank1.SetPosition( 67.2501f, 31.0001f );
	_wallBox1.SetPosition( 35.5502f, 48.7001f );
	_wallBox2.SetPosition( 60.4501f, 48.7000f );
	_wallBox3.SetPosition( 78.7501f, 39.2002f );
	_wallBox4.SetPosition( 19.2497f, 20.1500f );
	_wallBox5.SetPosition( 51.3001f, 39.0501f );
	_wallBox6.SetPosition( 72.4925f, 49.1714f );
	_wallBox7.SetPosition( 68.1948f, 48.7539f );
	_wallBox8.SetPosition( 76.5133f, 50.6907f );
	_wallBox9.SetPosition( 14.0890f, 57.2159f );
	_wallBox10.SetPosition( 23.5077f, 49.1715f );
	_wallBox11.SetPosition( 27.8055f, 48.7540f );
	_wallBox12.SetPosition( 16.2186f, 53.4823f );
	_wallBox13.SetPosition( 19.4870f, 50.6908f );
	_wallBox14.SetPosition( 79.7817f, 53.4822f );
	_wallBox15.SetPosition( 81.9112f, 57.2158f );
	_wallBox16.SetPosition( 62.9001f, 5.3500f );
	_woodBox1.SetPosition( 44.7501f, 20.7000f );

	_sleepers.push_back( Sleeper( &_coin1, 50 ));

	ConstFinal();
}
//--------------------------------------------------------------------------
LevelPawel_2::LevelPawel_2( GameTypes::LevelEnum levelEnum, bool viewMode ) : Level( levelEnum, viewMode )
{
	// _bomb1
	_bomb1.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_bomb1.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_bomb1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bomb1.SetBodyType( b2_dynamicBody );

	// _fragileBox1
	_fragileBox1.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox1.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox1.SetBodyType( b2_dynamicBody );

	// _fragileBox2
	_fragileBox2.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox2.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox2.SetBodyType( b2_dynamicBody );

	// _fragileBoxStatic1
	_fragileBoxStatic1.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBoxStatic1.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBoxStatic1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBoxStatic1.SetBodyType( b2_staticBody );

	// _fragileBoxStatic2
	_fragileBoxStatic2.SetRotation( -0.3000f );
	_fragileBoxStatic2.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBoxStatic2.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBoxStatic2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBoxStatic2.SetBodyType( b2_staticBody );

	// _wallBox1
	_wallBox1.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox1.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox1.SetBodyType( b2_staticBody );

	// _bomb2
	_bomb2.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_bomb2.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_bomb2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bomb2.SetBodyType( b2_dynamicBody );

	// _wallBox2
	_wallBox2.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox2.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox2.SetBodyType( b2_staticBody );

	// _wallBox3
	_wallBox3.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox3.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox3.SetBodyType( b2_staticBody );

	// _wallBox4
	_wallBox4.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox4.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox4.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox4.SetBodyType( b2_staticBody );

	// _bombStatic1
	_bombStatic1.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_bombStatic1.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_bombStatic1.GetBody()->GetFixtureList()->SetRestitution( 0.0000f );
	_bombStatic1.SetBodyType( b2_staticBody );

	// _wallBox5
	_wallBox5.SetRotation( 234.0000f );
	_wallBox5.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox5.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox5.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox5.SetBodyType( b2_staticBody );

	// _wallBox6
	_wallBox6.SetRotation( 126.0000f );
	_wallBox6.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox6.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox6.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox6.SetBodyType( b2_staticBody );

	// _wallBox7
	_wallBox7.SetRotation( 234.0000f );
	_wallBox7.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox7.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox7.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox7.SetBodyType( b2_staticBody );

	// _bombStatic2
	_bombStatic2.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_bombStatic2.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_bombStatic2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bombStatic2.SetBodyType( b2_staticBody );

	// _wallBox8
	_wallBox8.SetRotation( 126.0000f );
	_wallBox8.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox8.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox8.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox8.SetBodyType( b2_staticBody );

	// _woodBox1
	_woodBox1.SetRotation( 90.0000f );
	_woodBox1.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox1.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_woodBox1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_woodBox1.SetBodyType( b2_dynamicBody );

	// _bombStatic3
	_bombStatic3.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_bombStatic3.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_bombStatic3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bombStatic3.SetBodyType( b2_staticBody );

	// _wallBox9
	_wallBox9.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox9.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox9.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox9.SetBodyType( b2_staticBody );

	// _piggyBank1
	_piggyBank1.GetBody()->GetFixtureList()->SetDensity( 7.5000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_piggyBank1.FlipX();
	_piggyBank1.SetBodyType( b2_dynamicBody );

	// _wallBox10
	_wallBox10.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox10.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox10.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox10.SetBodyType( b2_staticBody );

	// _fragileBoxStatic3
	_fragileBoxStatic3.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBoxStatic3.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBoxStatic3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBoxStatic3.SetBodyType( b2_staticBody );

	// _fragileBoxStatic4
	_fragileBoxStatic4.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBoxStatic4.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBoxStatic4.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBoxStatic4.SetBodyType( b2_staticBody );

	// _fragileBoxStatic5
	_fragileBoxStatic5.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBoxStatic5.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBoxStatic5.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBoxStatic5.SetBodyType( b2_staticBody );

	// _coin1
	_coin1.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin1.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin1.GetBody()->GetFixtureList()->SetRestitution( 0.1500f );
	_coin1.GetBody()->SetAngularDamping(0.1000f );
	_coin1.GetBody()->SetLinearDamping(0.1000f );
	_coin1.SetBodyType( b2_dynamicBody );


	//Set blocks positions
	_bomb1.SetPosition( 51.7501f, 54.3500f );
	_fragileBox1.SetPosition( 79.3997f, 19.2000f );
	_fragileBox2.SetPosition( 61.9997f, 54.3501f );
	_fragileBoxStatic1.SetPosition( 51.7501f, 44.4501f );
	_fragileBoxStatic2.SetPosition( 61.9997f, 44.4501f );
	_wallBox1.SetPosition( 68.1499f, 32.3500f );
	_bomb2.SetPosition( 37.7996f, 40.4500f );
	_wallBox2.SetPosition( 78.8499f, 34.4999f );
	_wallBox3.SetPosition( 46.2998f, 13.4500f );
	_wallBox4.SetPosition( 42.0998f, 23.9500f );
	_bombStatic1.SetPosition( 84.5493f, 39.9999f );
	_wallBox5.SetPosition( 35.4468f, 12.9640f );
	_wallBox6.SetPosition( 26.9522f, 12.9640f );
	_wallBox7.SetPosition( 18.7971f, 12.9641f );
	_bombStatic2.SetPosition( 11.5492f, 7.2499f );
	_wallBox8.SetPosition( 10.3025f, 12.9641f );
	_woodBox1.SetPosition( 43.3498f, 46.7500f );
	_bombStatic3.SetPosition( 37.7993f, 7.2499f );
	_wallBox9.SetPosition( 27.7998f, 34.5000f );
	_piggyBank1.SetPosition( 12.6498f, 43.7500f );
	_wallBox10.SetPosition( 77.9500f, -18.1000f );
	_fragileBoxStatic3.SetPosition( 62.0001f, 9.5501f );
	_fragileBoxStatic4.SetPosition( 74.9501f, 9.5501f );
	_fragileBoxStatic5.SetPosition( 84.5501f, 9.5501f );
	_coin1.SetPosition( 79.3997f, 28.0000f );

	//Bombs Construction
	_bomb1.SetRange( 15.9000f );
	_bomb1.SetBombImpulse( 7825.0000f );
	_bomb2.SetRange( 9.1000f );
	_bomb2.SetBombImpulse( 3150.0000f );
	_bombStatic1.SetRange( 16.0000f );
	_bombStatic1.SetBombImpulse( 7525.0000f );
	_bombStatic2.SetRange( 16.0000f );
	_bombStatic2.SetBombImpulse( 6900.0000f );
	_bombStatic3.SetRange( 16.0000f );
	_bombStatic3.SetBombImpulse( 5700.0000f );

	ConstFinal();
}
//--------------------------------------------------------------------------
LevelPawel_3::LevelPawel_3( GameTypes::LevelEnum levelEnum, bool viewMode ) : Level( levelEnum, viewMode )
{
	// _bomb1
	_bomb1.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_bomb1.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_bomb1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bomb1.SetBodyType( b2_staticBody );

	// _coin1
	_coin1.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin1.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin1.SetBodyType( b2_dynamicBody );

	// _fragileBoxStatic1
	_fragileBoxStatic1.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBoxStatic1.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBoxStatic1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBoxStatic1.SetBodyType( b2_staticBody );

	// _piggyBank1
	_piggyBank1.GetBody()->GetFixtureList()->SetDensity( 7.5000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_piggyBank1.GetBody()->SetAngularDamping(0.0500f );
	_piggyBank1.GetBody()->SetLinearDamping(0.0500f );
	_piggyBank1.SetBodyType( b2_dynamicBody );

	// _bomb2
	_bomb2.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_bomb2.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_bomb2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bomb2.SetBodyType( b2_dynamicBody );

	// _fragileBox1
	_fragileBox1.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox1.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox1.SetBodyType( b2_dynamicBody );

	// _fragileBoxStatic2
	_fragileBoxStatic2.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBoxStatic2.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBoxStatic2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBoxStatic2.SetBodyType( b2_staticBody );

	// _bomb3
	_bomb3.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_bomb3.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_bomb3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bomb3.SetBodyType( b2_dynamicBody );

	// _wallBox1
	_wallBox1.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox1.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox1.SetBodyType( b2_staticBody );

	// _woodBox1
	_woodBox1.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox1.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_woodBox1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_woodBox1.SetBodyType( b2_dynamicBody );

	// _wallBox2
	_wallBox2.SetRotation( 15.0000f );
	_wallBox2.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox2.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox2.SetBodyType( b2_staticBody );

	// _fragileBox2
	_fragileBox2.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox2.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox2.SetBodyType( b2_dynamicBody );

	// _wallBox3
	_wallBox3.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox3.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox3.SetBodyType( b2_staticBody );

	// _woodBox2
	_woodBox2.SetRotation( 90.0000f );
	_woodBox2.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox2.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_woodBox2.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_woodBox2.SetBodyType( b2_dynamicBody );

	// _fragileBoxStatic3
	_fragileBoxStatic3.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBoxStatic3.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBoxStatic3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBoxStatic3.SetBodyType( b2_staticBody );


	//Set blocks positions
	_bomb1.SetPosition( 8.6000f, 16.5500f );
	_coin1.SetPosition( 11.6499f, 51.6502f );
	_fragileBoxStatic1.SetPosition( 11.6499f, 42.5502f );
	_piggyBank1.SetPosition( 85.4501f, 19.5000f );
	_bomb2.SetPosition( 84.4501f, 44.9994f );
	_fragileBox1.SetPosition( 59.2999f, 17.2999f );
	_fragileBoxStatic2.SetPosition( 84.4499f, 34.3003f );
	_bomb3.SetPosition( 40.1000f, 44.7003f );
	_wallBox1.SetPosition( 48.0000f, 10.9999f );
	_woodBox1.SetPosition( 66.0999f, 23.7999f );
	_wallBox2.SetPosition( 40.4500f, 37.9502f );
	_fragileBox2.SetPosition( 19.3501f, 17.0999f );
	_wallBox3.SetPosition( 72.9000f, 20.5999f );
	_woodBox2.SetPosition( 36.5500f, 22.3000f );
	_fragileBoxStatic3.SetPosition( 30.4000f, 42.5502f );

	//Bombs Construction
	_bomb1.SetRange( 14.1000f );
	_bomb1.SetBombImpulse( 5850.0000f );
	_bomb2.SetRange( 16.0000f );
	_bomb2.SetBombImpulse( 6750.0000f );
	_bomb3.SetRange( 16.0000f );
	_bomb3.SetBombImpulse( 6400.0000f );

	ConstFinal();
}
//--------------------------------------------------------------------------
LevelPawel_4::LevelPawel_4( GameTypes::LevelEnum levelEnum, bool viewMode ) : Level( levelEnum, viewMode )
{
	_coinCount = 2;

	// _piggyBank1
	_piggyBank1.GetBody()->GetFixtureList()->SetDensity( 7.5000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_piggyBank1.SetBodyType( b2_dynamicBody );

	// _wallBox1
	_wallBox1.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox1.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox1.SetBodyType( b2_staticBody );

	// _bomb1
	_bomb1.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_bomb1.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_bomb1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bomb1.SetBodyType( b2_dynamicBody );

	// _coin1
	_coin1.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin1.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin1.SetBodyType( b2_dynamicBody );

	// _coin2
	_coin2.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin2.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin2.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin2.SetBodyType( b2_dynamicBody );

	// _wallBox2
	_wallBox2.SetRotation( -7.0000f );
	_wallBox2.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox2.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox2.SetBodyType( b2_staticBody );

	// _wallBox3
	_wallBox3.SetRotation( 7.0000f );
	_wallBox3.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox3.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox3.SetBodyType( b2_staticBody );

	// _woodBox1
	_woodBox1.SetRotation( -90.3000f );
	_woodBox1.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox1.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_woodBox1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_woodBox1.SetBodyType( b2_dynamicBody );

	// _woodBox2
	_woodBox2.SetRotation( -89.7000f );
	_woodBox2.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox2.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_woodBox2.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_woodBox2.SetBodyType( b2_dynamicBody );

	// _bomb2
	_bomb2.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_bomb2.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_bomb2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bomb2.SetBodyType( b2_dynamicBody );

	// _wallBox4
	_wallBox4.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox4.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox4.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox4.SetBodyType( b2_staticBody );

	// _wallBox5
	_wallBox5.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox5.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox5.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox5.SetBodyType( b2_staticBody );

	// _wallBox6
	_wallBox6.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox6.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox6.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox6.SetBodyType( b2_staticBody );

	// _wallBox7
	_wallBox7.SetRotation( -0.3000f );
	_wallBox7.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox7.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox7.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox7.SetBodyType( b2_staticBody );

	// _wallBox8
	_wallBox8.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox8.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox8.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox8.SetBodyType( b2_staticBody );

	// _fragileBoxStatic1
	_fragileBoxStatic1.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBoxStatic1.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBoxStatic1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBoxStatic1.SetBodyType( b2_staticBody );

	// _fragileBoxStatic2
	_fragileBoxStatic2.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBoxStatic2.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBoxStatic2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBoxStatic2.SetBodyType( b2_staticBody );

	// _wallBox9
	_wallBox9.SetRotation( -90.0000f );
	_wallBox9.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox9.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox9.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox9.SetBodyType( b2_staticBody );

	// _wallBox10
	_wallBox10.SetRotation( -90.0000f );
	_wallBox10.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox10.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox10.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox10.SetBodyType( b2_staticBody );

	// _fragileBoxStatic3
	_fragileBoxStatic3.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBoxStatic3.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBoxStatic3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBoxStatic3.SetBodyType( b2_staticBody );

	// _bomb3
	_bomb3.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_bomb3.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_bomb3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bomb3.SetBodyType( b2_dynamicBody );

	// _wallBox11
	_wallBox11.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox11.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox11.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox11.SetBodyType( b2_staticBody );

	// _bomb4
	_bomb4.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_bomb4.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_bomb4.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bomb4.SetBodyType( b2_dynamicBody );

	// _bomb5
	_bomb5.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_bomb5.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_bomb5.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bomb5.SetBodyType( b2_dynamicBody );

	// _wallBox12
	_wallBox12.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox12.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox12.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox12.SetBodyType( b2_staticBody );

	// _wallBox13
	_wallBox13.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox13.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox13.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox13.SetBodyType( b2_staticBody );

	// _woodBox3
	_woodBox3.SetRotation( -90.0000f );
	_woodBox3.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox3.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_woodBox3.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_woodBox3.SetBodyType( b2_dynamicBody );

	// _woodBox4
	_woodBox4.SetRotation( -90.0000f );
	_woodBox4.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox4.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_woodBox4.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_woodBox4.SetBodyType( b2_dynamicBody );

	// _wallBox14
	_wallBox14.SetRotation( -90.0000f );
	_wallBox14.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox14.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox14.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox14.SetBodyType( b2_staticBody );

	// _wallBox15
	_wallBox15.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox15.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox15.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox15.SetBodyType( b2_staticBody );

	// _wallBox16
	_wallBox16.SetRotation( -89.4000f );
	_wallBox16.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox16.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox16.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox16.SetBodyType( b2_staticBody );

	// _wallBox17
	_wallBox17.SetRotation( -90.0000f );
	_wallBox17.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox17.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox17.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox17.SetBodyType( b2_staticBody );

	// _wallBox18
	_wallBox18.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox18.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox18.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox18.SetBodyType( b2_staticBody );

	// _wallBox19
	_wallBox19.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox19.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox19.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox19.SetBodyType( b2_staticBody );

	// _wallBox20
	_wallBox20.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox20.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox20.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox20.SetBodyType( b2_staticBody );

	// _wallBox21
	_wallBox21.SetRotation( -90.0000f );
	_wallBox21.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox21.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox21.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox21.SetBodyType( b2_staticBody );

	// _wallBox22
	_wallBox22.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox22.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox22.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox22.SetBodyType( b2_staticBody );

	// _wallBox23
	_wallBox23.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox23.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox23.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox23.SetBodyType( b2_staticBody );

	// _wallBox24
	_wallBox24.SetRotation( -90.0000f );
	_wallBox24.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox24.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox24.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox24.SetBodyType( b2_staticBody );

	// _wallBox25
	_wallBox25.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox25.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox25.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox25.SetBodyType( b2_staticBody );

	// _wallBox26
	_wallBox26.SetRotation( 0.3000f );
	_wallBox26.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox26.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox26.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox26.SetBodyType( b2_staticBody );

	// _fragileBoxStatic4
	_fragileBoxStatic4.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBoxStatic4.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBoxStatic4.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBoxStatic4.SetBodyType( b2_staticBody );


	//Set blocks positions
	_piggyBank1.SetPosition( 24.3000f, 14.0000f );
	_wallBox1.SetPosition( 24.2000f, 20.8000f );
	_bomb1.SetPosition( 24.4500f, 23.4000f );
	_coin1.SetPosition( 2.6500f, 29.9000f );
	_coin2.SetPosition( 45.1000f, 29.9000f );
	_wallBox2.SetPosition( 3.4500f, 21.3000f );
	_wallBox3.SetPosition( 44.9500f, 21.2500f );
	_woodBox1.SetPosition( 31.9500f, 25.1500f );
	_woodBox2.SetPosition( 17.0500f, 25.2500f );
	_bomb2.SetPosition( 3.3500f, 17.8000f );
	_wallBox4.SetPosition( 45.1000f, 15.0000f );
	_wallBox5.SetPosition( 16.3000f, 20.0000f );
	_wallBox6.SetPosition( 32.1000f, 19.9500f );
	_wallBox7.SetPosition( 44.0000f, 20.2500f );
	_wallBox8.SetPosition( 4.2000f, 20.4000f );
	_fragileBoxStatic1.SetPosition( 45.2000f, 25.4000f );
	_fragileBoxStatic2.SetPosition( 2.6500f, 25.4000f );
	_wallBox9.SetPosition( 21.8500f, 18.4500f );
	_wallBox10.SetPosition( 27.0500f, 18.3500f );
	_fragileBoxStatic3.SetPosition( 43.0000f, 8.2500f );
	_bomb3.SetPosition( 8.7500f, 3.2500f );
	_wallBox11.SetPosition( 3.0000f, 15.2500f );
	_bomb4.SetPosition( 45.2500f, 17.5500f );
	_bomb5.SetPosition( 39.7000f, 3.3500f );
	_wallBox12.SetPosition( 8.3000f, 0.6000f );
	_wallBox13.SetPosition( 39.5500f, 0.7500f );
	_woodBox3.SetPosition( 9.8500f, 9.6000f );
	_woodBox4.SetPosition( 40.1500f, 9.7000f );
	_wallBox14.SetPosition( 24.4500f, 8.2000f );
	_wallBox15.SetPosition( 30.9000f, 16.1000f );
	_wallBox16.SetPosition( 38.1500f, 7.8500f );
	_wallBox17.SetPosition( 33.6500f, 7.8000f );
	_wallBox18.SetPosition( 11.2000f, 3.2500f );
	_wallBox19.SetPosition( 42.2500f, 3.4000f );
	_wallBox20.SetPosition( 17.9500f, 16.1000f );
	_wallBox21.SetPosition( 15.4000f, 7.8500f );
	_wallBox22.SetPosition( 37.2500f, 3.4000f );
	_wallBox23.SetPosition( 6.2500f, 3.3000f );
	_wallBox24.SetPosition( 10.7500f, 7.8500f );
	_wallBox25.SetPosition( 9.1500f, 10.0000f );
	_wallBox26.SetPosition( 39.4500f, 10.1000f );
	_fragileBoxStatic4.SetPosition( 5.8000f, 8.2500f );

	//Bombs Construction
	_bomb1.SetRange( 16.0000f );
	_bomb1.SetBombImpulse( 6000.0000f );
	_bomb2.SetRange( 16.0000f );
	_bomb2.SetBombImpulse( 6000.0000f );
	_bomb3.SetRange( 16.0000f );
	_bomb3.SetBombImpulse( 6000.0000f );
	_bomb4.SetRange( 16.0000f );
	_bomb4.SetBombImpulse( 6000.0000f );
	_bomb5.SetRange( 16.0000f );
	_bomb5.SetBombImpulse( 6000.0000f );

	ConstFinal();
	AdjustOldScreen();
}
//--------------------------------------------------------------------------
LevelPawel_5::LevelPawel_5( GameTypes::LevelEnum levelEnum, bool viewMode ) : Level( levelEnum, viewMode )
{
	_coinCount = 3;
    _levelTag = eLevelTag_Hacker;
    
	// _bomb1
	_bomb1.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_bomb1.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_bomb1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bomb1.SetBodyType( b2_dynamicBody );

	// _bomb2
	_bomb2.GetBody()->GetFixtureList()->SetDensity( 6.5500f );
	_bomb2.GetBody()->GetFixtureList()->SetFriction( 0.4500f );
	_bomb2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bomb2.SetBodyType( b2_dynamicBody );

	// _bomb3
	_bomb3.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_bomb3.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_bomb3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bomb3.SetBodyType( b2_dynamicBody );

	// _bombStatic1
	_bombStatic1.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_bombStatic1.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_bombStatic1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bombStatic1.SetBodyType( b2_staticBody );

	// _bombStatic2
	_bombStatic2.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_bombStatic2.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_bombStatic2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bombStatic2.SetBodyType( b2_staticBody );

	// _bombStatic3
	_bombStatic3.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_bombStatic3.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_bombStatic3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bombStatic3.SetBodyType( b2_staticBody );

	// _coin1
	_coin1.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin1.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin1.SetBodyType( b2_dynamicBody );

	// _coin2
	_coin2.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin2.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin2.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin2.SetBodyType( b2_dynamicBody );

	// _coin3
	_coin3.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin3.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin3.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin3.SetBodyType( b2_dynamicBody );

	// _fragileBoxStatic1
	_fragileBoxStatic1.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBoxStatic1.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBoxStatic1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBoxStatic1.SetBodyType( b2_staticBody );

	// _fragileBoxStatic2
	_fragileBoxStatic2.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBoxStatic2.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBoxStatic2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBoxStatic2.SetBodyType( b2_staticBody );

	// _piggyBankStatic1
	_piggyBankStatic1.GetBody()->GetFixtureList()->SetDensity( 7.5000f );
	_piggyBankStatic1.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_piggyBankStatic1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_piggyBankStatic1.SetBodyType( b2_staticBody );

	// _wallBox1
	_wallBox1.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox1.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox1.SetBodyType( b2_staticBody );

	// _wallBox2
	_wallBox2.SetRotation( 33.0000f );
	_wallBox2.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox2.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox2.SetBodyType( b2_staticBody );

	// _wallBox3
	_wallBox3.SetRotation( 33.0000f );
	_wallBox3.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox3.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox3.SetBodyType( b2_staticBody );

	// _wallBox4
	_wallBox4.SetRotation( 33.0000f );
	_wallBox4.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox4.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox4.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox4.SetBodyType( b2_staticBody );

	// _wallBox5
	_wallBox5.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox5.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox5.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox5.SetBodyType( b2_staticBody );

	// _wallBox6
	_wallBox6.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox6.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox6.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox6.SetBodyType( b2_staticBody );

	// _wallBox7
	_wallBox7.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox7.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox7.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox7.SetBodyType( b2_staticBody );

	// _wallBox8
	_wallBox8.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox8.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox8.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox8.SetBodyType( b2_staticBody );

	// _woodBox1
	_woodBox1.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox1.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_woodBox1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_woodBox1.SetBodyType( b2_dynamicBody );


	//Set blocks positions
	_bomb1.SetPosition( 32.1497f, 54.3500f );
	_bomb2.SetPosition( 57.2495f, 13.6500f );
	_bomb3.SetPosition( 49.5496f, 54.3500f );
	_bombStatic1.SetPosition( 10.3000f, 13.6500f );
	_bombStatic2.SetPosition( 27.6998f, 13.6500f );
	_bombStatic3.SetPosition( 45.0998f, 13.6500f );
	_coin1.SetPosition( 14.8499f, 25.1001f );
	_coin2.SetPosition( 32.2498f, 25.1001f );
	_coin3.SetPosition( 49.5998f, 25.1001f );
	_fragileBoxStatic1.SetPosition( 32.1497f, 43.8001f );
	_fragileBoxStatic2.SetPosition( 49.5496f, 43.8003f );
	_piggyBankStatic1.SetPosition( 80.9001f, 13.6500f );
	_wallBox1.SetPosition( 57.9498f, 8.2499f );
	_wallBox2.SetPosition( 21.2999f, 21.0500f );
	_wallBox3.SetPosition( 38.6998f, 21.0500f );
	_wallBox4.SetPosition( 56.0998f, 21.0500f );
	_wallBox5.SetPosition( 87.4001f, 23.9499f );
	_wallBox6.SetPosition( 74.4001f, 23.9499f );
	_wallBox7.SetPosition( 8.8000f, 24.3999f );
	_wallBox8.SetPosition( 67.8997f, 23.9499f );
	_woodBox1.SetPosition( 80.9500f, 26.8000f );

	//Bombs Construction
	_bomb1.SetRange( 14.0000f );
	_bomb1.SetBombImpulse( 7875.0000f );
	_bomb2.SetRange( 20.7000f );
	_bomb2.SetBombImpulse( 10000.0000f );
	_bomb3.SetRange( 14.0000f );
	_bomb3.SetBombImpulse( 4950.0000f );
	_bombStatic1.SetRange( 16.0000f );
	_bombStatic1.SetBombImpulse( 5550.0000f );
	_bombStatic2.SetRange( 16.0000f );
	_bombStatic2.SetBombImpulse( 5550.0000f );
	_bombStatic3.SetRange( 18.6000f );
	_bombStatic3.SetBombImpulse( 6200.0000f );

	ConstFinal();
}
//--------------------------------------------------------------------------
LevelPawel_6::LevelPawel_6( GameTypes::LevelEnum levelEnum, bool viewMode ) : Level( levelEnum, viewMode )
{
	_coinCount = 3;

	// _wallBox1
	_wallBox1.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox1.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox1.SetBodyType( b2_staticBody );

	// _wallBox2
	_wallBox2.SetRotation( -90.0000f );
	_wallBox2.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox2.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox2.SetBodyType( b2_staticBody );

	// _wallBox3
	_wallBox3.SetRotation( -90.0000f );
	_wallBox3.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox3.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox3.SetBodyType( b2_staticBody );

	// _piggyBank1
	_piggyBank1.GetBody()->GetFixtureList()->SetDensity( 7.5000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_piggyBank1.SetBodyType( b2_dynamicBody );

	// _wallBox4
	_wallBox4.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox4.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox4.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox4.SetBodyType( b2_staticBody );

	// _wallBox5
	_wallBox5.SetRotation( 10.0000f );
	_wallBox5.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox5.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox5.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox5.SetBodyType( b2_staticBody );

	// _wallBox6
	_wallBox6.SetRotation( -90.0000f );
	_wallBox6.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox6.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox6.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox6.SetBodyType( b2_staticBody );

	// _coin1
	_coin1.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin1.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin1.SetBodyType( b2_dynamicBody );

	// _bomb1
	_bomb1.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_bomb1.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_bomb1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bomb1.SetBodyType( b2_dynamicBody );

	// _wallBox7
	_wallBox7.SetRotation( 10.0000f );
	_wallBox7.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox7.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox7.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox7.SetBodyType( b2_staticBody );

	// _coin2
	_coin2.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin2.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin2.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin2.SetBodyType( b2_dynamicBody );

	// _wallBox8
	_wallBox8.SetRotation( -90.0000f );
	_wallBox8.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox8.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox8.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox8.SetBodyType( b2_staticBody );

	// _bomb2
	_bomb2.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_bomb2.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_bomb2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bomb2.SetBodyType( b2_dynamicBody );

	// _wallBox9
	_wallBox9.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox9.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox9.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox9.SetBodyType( b2_staticBody );

	// _wallBox10
	_wallBox10.SetRotation( 15.0000f );
	_wallBox10.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox10.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox10.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox10.SetBodyType( b2_staticBody );

	// _coin3
	_coin3.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin3.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin3.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin3.SetBodyType( b2_dynamicBody );

	// _wallBox11
	_wallBox11.SetRotation( -90.0000f );
	_wallBox11.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox11.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox11.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox11.SetBodyType( b2_staticBody );

	// _bomb3
	_bomb3.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_bomb3.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_bomb3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bomb3.SetBodyType( b2_dynamicBody );

	// _wallBox12
	_wallBox12.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox12.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox12.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox12.SetBodyType( b2_staticBody );

	// _fragileBoxStatic1
	_fragileBoxStatic1.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBoxStatic1.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBoxStatic1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBoxStatic1.SetBodyType( b2_staticBody );

	// _fragileBoxStatic2
	_fragileBoxStatic2.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBoxStatic2.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBoxStatic2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBoxStatic2.SetBodyType( b2_staticBody );

	// _bomb4
	_bomb4.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_bomb4.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_bomb4.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bomb4.SetBodyType( b2_dynamicBody );

	// _bomb5
	_bomb5.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_bomb5.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_bomb5.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bomb5.SetBodyType( b2_dynamicBody );

	// _woodBox1
	_woodBox1.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox1.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_woodBox1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_woodBox1.SetBodyType( b2_dynamicBody );

	// _woodBox2
	_woodBox2.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox2.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_woodBox2.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_woodBox2.SetBodyType( b2_dynamicBody );

	// _fragileBoxStatic3
	_fragileBoxStatic3.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBoxStatic3.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBoxStatic3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBoxStatic3.SetBodyType( b2_staticBody );

	// _bomb6
	_bomb6.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_bomb6.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_bomb6.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bomb6.SetBodyType( b2_dynamicBody );


	//Set blocks positions
	_wallBox1.SetPosition( 43.9000f, 0.5500f );
	_wallBox2.SetPosition( 40.4500f, 1.2000f );
	_wallBox3.SetPosition( 47.5000f, 2.9000f );
	_piggyBank1.SetPosition( 43.9000f, 3.9000f );
	_wallBox4.SetPosition( 2.5500f, 10.5000f );
	_wallBox5.SetPosition( 8.8000f, 16.7500f );
	_wallBox6.SetPosition( 1.6000f, 18.1000f );
	_coin1.SetPosition( 4.0000f, 18.5500f );
	_bomb1.SetPosition( 2.5000f, 13.0500f );
	_wallBox7.SetPosition( 21.4500f, 12.7500f );
	_coin2.SetPosition( 16.8500f, 15.0000f );
	_wallBox8.SetPosition( 14.2500f, 14.4000f );
	_bomb2.SetPosition( 18.7000f, 29.1500f );
	_wallBox9.SetPosition( 14.8000f, 6.9500f );
	_wallBox10.SetPosition( 35.3500f, 8.8500f );
	_coin3.SetPosition( 29.9000f, 10.0501f );
	_wallBox11.SetPosition( 27.3500f, 10.4001f );
	_bomb3.SetPosition( 28.7000f, 5.8000f );
	_wallBox12.SetPosition( 28.4500f, 3.2500f );
	_fragileBoxStatic1.SetPosition( 30.5000f, 20.7500f );
	_fragileBoxStatic2.SetPosition( 18.6500f, 24.5000f );
	_bomb4.SetPosition( 14.8500f, 9.7000f );
	_bomb5.SetPosition( 30.5500f, 25.4000f );
	_woodBox1.SetPosition( 31.3500f, 13.3000f );
	_woodBox2.SetPosition( 5.2500f, 21.2000f );
	_fragileBoxStatic3.SetPosition( 10.0500f, 9.8000f );
	_bomb6.SetPosition( 11.6000f, 14.3500f );

	//Bombs Construction
	_bomb1.SetRange( 2.0f * 8.0000f );
	_bomb1.SetBombImpulse( 6.0f * 1000.0000f );
	_bomb2.SetRange( 2.0f * 8.0000f );
	_bomb2.SetBombImpulse( 6.0f * 1000.0000f );
	_bomb3.SetRange( 2.0f * 8.0000f );
	_bomb3.SetBombImpulse( 6.0f * 1000.0000f );
	_bomb4.SetRange( 2.0f * 8.0000f );
	_bomb4.SetBombImpulse( 6.0f * 1000.0000f );
	_bomb5.SetRange( 2.0f * 8.0000f );
	_bomb5.SetBombImpulse( 6.0f * 1000.0000f );
	_bomb6.SetRange( 2.0f * 8.0000f );
	_bomb6.SetBombImpulse( 6.0f * 1000.0000f );

	ConstFinal();
	AdjustOldScreen();
}
//--------------------------------------------------------------------------
LevelPawel_7::LevelPawel_7( GameTypes::LevelEnum levelEnum, bool viewMode ) : Level( levelEnum, viewMode )
{
	_coinCount = 2;
    _levelTag = eLevelTag_Bombs;
    
	// _bombStatic1
	_bombStatic1.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_bombStatic1.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_bombStatic1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bombStatic1.SetBodyType( b2_staticBody );

	// _bombStatic2
	_bombStatic2.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_bombStatic2.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_bombStatic2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bombStatic2.SetBodyType( b2_staticBody );

	// _bombStatic3
	_bombStatic3.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_bombStatic3.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_bombStatic3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bombStatic3.SetBodyType( b2_staticBody );

	// _bombStatic4
	_bombStatic4.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_bombStatic4.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_bombStatic4.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bombStatic4.SetBodyType( b2_staticBody );

	// _button1
	_button1.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_button1.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_button1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_button1.SetBodyType( b2_dynamicBody );

	// _coin1
	_coin1.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin1.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin1.SetBodyType( b2_dynamicBody );

	// _coin2
	_coin2.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin2.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin2.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin2.SetBodyType( b2_dynamicBody );

	// _fragileBox1
	_fragileBox1.SetRotation( -6.0000f );
	_fragileBox1.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox1.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox1.SetBodyType( b2_dynamicBody );

	// _fragileBoxStatic1
	_fragileBoxStatic1.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBoxStatic1.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBoxStatic1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBoxStatic1.SetBodyType( b2_staticBody );

	// _fragileBoxStatic2
	_fragileBoxStatic2.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBoxStatic2.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBoxStatic2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBoxStatic2.SetBodyType( b2_staticBody );

	// _piggyBankStatic1
	_piggyBankStatic1.GetBody()->GetFixtureList()->SetDensity( 7.5000f );
	_piggyBankStatic1.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_piggyBankStatic1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_piggyBankStatic1.FlipX();
	_piggyBankStatic1.SetBodyType( b2_staticBody );

	// _wallBox1
	_wallBox1.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox1.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox1.SetBodyType( b2_staticBody );

	// _wallBox2
	_wallBox2.SetRotation( 90.0000f );
	_wallBox2.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox2.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox2.SetBodyType( b2_staticBody );

	// _wallBox3
	_wallBox3.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox3.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox3.SetBodyType( b2_staticBody );

	// _wallBox4
	_wallBox4.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox4.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox4.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox4.SetBodyType( b2_staticBody );

	// _wallBox5
	_wallBox5.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox5.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox5.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox5.SetBodyType( b2_staticBody );

	// _wallBox6
	_wallBox6.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox6.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox6.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox6.SetBodyType( b2_staticBody );

	// _wallBox7
	_wallBox7.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox7.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox7.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox7.SetBodyType( b2_staticBody );

	// _wallBox8
	_wallBox8.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox8.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox8.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox8.SetBodyType( b2_staticBody );

	// _wallBox9
	_wallBox9.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox9.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox9.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox9.SetBodyType( b2_staticBody );

	// _wallBox10
	_wallBox10.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox10.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox10.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox10.SetBodyType( b2_staticBody );

	// _wallBox11
	_wallBox11.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox11.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox11.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox11.SetBodyType( b2_staticBody );

	// _wallBox12
	_wallBox12.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox12.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox12.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox12.SetBodyType( b2_staticBody );

	// _wallBox13
	_wallBox13.SetRotation( -6.2000f );
	_wallBox13.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox13.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox13.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox13.SetBodyType( b2_staticBody );

	// _wallBox14
	_wallBox14.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox14.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox14.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox14.SetBodyType( b2_staticBody );

	// _woodBox1
	_woodBox1.SetRotation( 270.0000f );
	_woodBox1.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox1.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_woodBox1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_woodBox1.SetBodyType( b2_dynamicBody );

	// _woodBox2
	_woodBox2.SetRotation( 90.0000f );
	_woodBox2.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox2.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_woodBox2.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_woodBox2.SetBodyType( b2_dynamicBody );

	// _fragileBox2
	_fragileBox2.SetRotation( -6.0000f );
	_fragileBox2.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox2.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox2.SetBodyType( b2_dynamicBody );


	//Set blocks positions
	_bombStatic1.SetPosition( 86.1007f, 10.4500f );
	_bombStatic2.SetPosition( 58.7502f, 22.6000f );
	_bombStatic3.SetPosition( 35.5502f, 31.7998f );
	_bombStatic4.SetPosition( 86.1005f, 32.6999f );
	_button1.SetPosition( 83.6501f, 19.1500f );
	_coin1.SetPosition( 33.6002f, 44.1499f );
	_coin2.SetPosition( 11.3500f, 51.8501f );
	_fragileBox1.SetPosition( 57.7502f, 53.6998f );
	_fragileBoxStatic1.SetPosition( 44.0502f, 42.4999f );
	_fragileBoxStatic2.SetPosition( 11.3500f, 42.5000f );
	_piggyBankStatic1.SetPosition( 11.3500f, 11.1000f );
	_wallBox1.SetPosition( 27.2501f, 18.6000f );
	_wallBox2.SetPosition( 23.0495f, 42.8000f );
	_wallBox3.SetPosition( 11.3500f, 24.3999f );
	_wallBox4.SetPosition( 56.2001f, 30.7499f );
	_wallBox5.SetPosition( 88.9507f, 20.9334f );
	_wallBox6.SetPosition( 79.8006f, 14.5000f );
	_wallBox7.SetPosition( 64.7502f, 26.5000f );
	_wallBox8.SetPosition( 81.0503f, 36.7832f );
	_wallBox9.SetPosition( 89.8004f, 42.5834f );
	_wallBox10.SetPosition( 50.9500f, 14.5000f );
	_wallBox11.SetPosition( 5.5001f, 24.3999f );
	_wallBox12.SetPosition( 32.2999f, 6.9000f );
	_wallBox13.SetPosition( 51.5502f, 47.6498f );
	_wallBox14.SetPosition( 33.6000f, 37.7000f );
	_woodBox1.SetPosition( 39.9501f, 15.8000f );
	_woodBox2.SetPosition( 25.9001f, 27.7999f );
	_fragileBox2.SetPosition( 43.3500f, 55.3000f );

	//Bombs Construction
	_bombStatic1.SetRange( 16.0000f );
	_bombStatic1.SetBombImpulse( 12950.0000f );
	_bombStatic2.SetRange( 13.2000f );
	_bombStatic2.SetBombImpulse( 16125.0000f );
	_bombStatic3.SetRange( 13.2000f );
	_bombStatic3.SetBombImpulse( 4700.0000f );
	_bombStatic4.SetRange( 16.0000f );
	_bombStatic4.SetBombImpulse( 16500.0000f );

	ConstFinal();
}
//--------------------------------------------------------------------------
LevelPawel_8::LevelPawel_8( GameTypes::LevelEnum levelEnum, bool viewMode ) : Level( levelEnum, viewMode )
{
    _levelTag = eLevelTag_Bombs;
    
	// _bomb1
	_bomb1.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_bomb1.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_bomb1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bomb1.SetBodyType( b2_dynamicBody );

	// _bombStatic1
	_bombStatic1.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_bombStatic1.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_bombStatic1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bombStatic1.FlipX();
	_bombStatic1.SetBodyType( b2_staticBody );

	// _bombStatic2
	_bombStatic2.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_bombStatic2.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_bombStatic2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bombStatic2.SetBodyType( b2_staticBody );

	// _bombStatic3
	_bombStatic3.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_bombStatic3.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_bombStatic3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bombStatic3.SetBodyType( b2_staticBody );

	// _bombStatic4
	_bombStatic4.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_bombStatic4.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_bombStatic4.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bombStatic4.SetBodyType( b2_staticBody );

	// _bombStatic5
	_bombStatic5.SetRotation( 0.6000f );
	_bombStatic5.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_bombStatic5.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_bombStatic5.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bombStatic5.SetBodyType( b2_staticBody );

	// _button1
	_button1.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_button1.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_button1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_button1.SetBodyType( b2_dynamicBody );

	// _coin1
	_coin1.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin1.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin1.SetBodyType( b2_dynamicBody );

	// _fragileBox1
	_fragileBox1.SetRotation( -5.0000f );
	_fragileBox1.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox1.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox1.SetBodyType( b2_dynamicBody );

	// _piggyBank1
	_piggyBank1.GetBody()->GetFixtureList()->SetDensity( 7.5000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_piggyBank1.SetBodyType( b2_dynamicBody );

	// _wallBox1
	_wallBox1.SetRotation( -7.0000f );
	_wallBox1.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox1.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox1.SetBodyType( b2_staticBody );

	// _wallBox2
	_wallBox2.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox2.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox2.SetBodyType( b2_staticBody );

	// _wallBox3
	_wallBox3.SetRotation( -7.0000f );
	_wallBox3.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox3.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox3.SetBodyType( b2_staticBody );

	// _wallBox4
	_wallBox4.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox4.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox4.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox4.SetBodyType( b2_staticBody );

	// _wallBox5
	_wallBox5.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox5.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox5.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox5.SetBodyType( b2_staticBody );

	// _wallBox6
	_wallBox6.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox6.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox6.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox6.SetBodyType( b2_staticBody );

	// _wallBox7
	_wallBox7.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox7.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox7.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox7.SetBodyType( b2_staticBody );

	// _wallBox8
	_wallBox8.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox8.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox8.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox8.SetBodyType( b2_staticBody );

	// _wallBox9
	_wallBox9.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox9.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox9.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox9.SetBodyType( b2_staticBody );

	// _wallBox10
	_wallBox10.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox10.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox10.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox10.SetBodyType( b2_staticBody );

	// _wallBox11
	_wallBox11.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox11.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox11.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox11.SetBodyType( b2_staticBody );

	// _wallBox12
	_wallBox12.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox12.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox12.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox12.SetBodyType( b2_staticBody );

	// _wallBox13
	_wallBox13.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox13.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox13.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox13.SetBodyType( b2_staticBody );

	// _wallBox14
	_wallBox14.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox14.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox14.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox14.SetBodyType( b2_staticBody );

	// _wallBox15
	_wallBox15.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox15.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox15.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox15.SetBodyType( b2_staticBody );

	// _woodBox1
	_woodBox1.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox1.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_woodBox1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_woodBox1.SetBodyType( b2_dynamicBody );


	//Set blocks positions
	_bomb1.SetPosition( 12.0001f, 46.6500f );
	_bombStatic1.SetPosition( 84.0998f, 6.9001f );
	_bombStatic2.SetPosition( 54.3493f, 6.9001f );
	_bombStatic3.SetPosition( 12.0000f, 6.9002f );
	_bombStatic4.SetPosition( 54.6496f, 44.7001f );
	_bombStatic5.SetPosition( 84.0998f, 25.6499f );
	_button1.SetPosition( 38.7500f, 18.5000f );
	_coin1.SetPosition( 80.7999f, 15.1001f );
	_fragileBox1.SetPosition( 20.8000f, 45.0500f );
	_piggyBank1.SetPosition( 84.0998f, 55.3999f );
	_wallBox1.SetPosition( 43.6499f, 12.1501f );
	_wallBox2.SetPosition( 34.4500f, 25.9501f );
	_wallBox3.SetPosition( 15.9000f, 39.3499f );
	_wallBox4.SetPosition( 71.3000f, 31.4000f );
	_wallBox5.SetPosition( 40.2376f, 48.8375f );
	_wallBox6.SetPosition( 58.2372f, 48.8375f );
	_wallBox7.SetPosition( 78.1501f, 48.8499f );
	_wallBox8.SetPosition( 90.0503f, 48.8499f );
	_wallBox9.SetPosition( 8.5500f, 14.2001f );
	_wallBox10.SetPosition( 42.1997f, 23.9001f );
	_wallBox11.SetPosition( 86.2498f, 13.1001f );
	_wallBox12.SetPosition( 61.1998f, 29.0998f );
	_wallBox13.SetPosition( 88.5999f, 31.4000f );
	_wallBox14.SetPosition( 68.7000f, 10.1000f );
	_wallBox15.SetPosition( 19.1000f, 11.8000f );
	_woodBox1.SetPosition( 48.9997f, 51.4001f );

	//Bombs Construction
	_bomb1.SetRange( 16.0000f );
	_bomb1.SetBombImpulse( 5000.0000f );
	_bombStatic1.SetRange( 16.0000f );
	_bombStatic1.SetBombImpulse( 5000.0000f );
	_bombStatic2.SetRange( 16.0000f );
	_bombStatic2.SetBombImpulse( 6700.0000f );
	_bombStatic3.SetRange( 16.0000f );
	_bombStatic3.SetBombImpulse( 6950.0000f );
	_bombStatic4.SetRange( 16.0000f );
	_bombStatic4.SetBombImpulse( 2700.0000f );
	_bombStatic5.SetRange( 16.0000f );
	_bombStatic5.SetBombImpulse( 6325.0000f );

	ConstFinal();
}
//--------------------------------------------------------------------------
LevelPawel_9::LevelPawel_9( GameTypes::LevelEnum levelEnum, bool viewMode ) : Level( levelEnum, viewMode )
{
	// _wallBox1
	_wallBox1.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox1.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox1.SetBodyType( b2_staticBody );

	// _wallBox2
	_wallBox2.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox2.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox2.SetBodyType( b2_staticBody );

	// _bomb1
	_bomb1.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_bomb1.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_bomb1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bomb1.SetBodyType( b2_dynamicBody );

	// _coin1
	_coin1.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin1.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin1.SetBodyType( b2_dynamicBody );

	// _wallBox3
	_wallBox3.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox3.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox3.SetBodyType( b2_staticBody );

	// _wallBox4
	_wallBox4.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox4.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox4.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox4.SetBodyType( b2_staticBody );

	// _wallBox5
	_wallBox5.SetRotation( -2.0000f );
	_wallBox5.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox5.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox5.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox5.SetBodyType( b2_staticBody );

	// _wallBox6
	_wallBox6.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox6.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox6.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox6.SetBodyType( b2_staticBody );

	// _bomb2
	_bomb2.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_bomb2.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_bomb2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bomb2.SetBodyType( b2_dynamicBody );

	// _wallBox7
	_wallBox7.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox7.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox7.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox7.SetBodyType( b2_staticBody );

	// _piggyBank1
	_piggyBank1.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_piggyBank1.SetBodyType( b2_dynamicBody );

	// _bomb3
	_bomb3.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_bomb3.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_bomb3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bomb3.SetBodyType( b2_dynamicBody );

	// _circleWood1
	_circleWood1.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_circleWood1.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_circleWood1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_circleWood1.SetBodyType( b2_dynamicBody );

	// _wallBox8
	_wallBox8.SetRotation( 20.0000f );
	_wallBox8.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox8.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox8.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox8.SetBodyType( b2_staticBody );

	// _wallBox9
	_wallBox9.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox9.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox9.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox9.SetBodyType( b2_staticBody );

	// _wallBox10
	_wallBox10.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox10.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox10.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox10.SetBodyType( b2_staticBody );

	// _bomb4
	_bomb4.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_bomb4.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_bomb4.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bomb4.SetBodyType( b2_dynamicBody );

	// _wallBox11
	_wallBox11.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox11.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox11.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox11.SetBodyType( b2_staticBody );

	// _fragileBoxStatic1
	_fragileBoxStatic1.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBoxStatic1.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBoxStatic1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBoxStatic1.SetBodyType( b2_staticBody );

	// _bomb5
	_bomb5.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_bomb5.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_bomb5.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bomb5.SetBodyType( b2_dynamicBody );

	// _wallBox12
	_wallBox12.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox12.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox12.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox12.SetBodyType( b2_staticBody );

	// _wallBox13
	_wallBox13.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox13.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox13.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox13.SetBodyType( b2_staticBody );

	// _bomb6
	_bomb6.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_bomb6.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_bomb6.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bomb6.SetBodyType( b2_dynamicBody );

	// _wallBox14
	_wallBox14.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox14.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox14.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox14.SetBodyType( b2_staticBody );

	// _fragileBoxStatic2
	_fragileBoxStatic2.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBoxStatic2.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBoxStatic2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBoxStatic2.SetBodyType( b2_staticBody );

	// _wallBox15
	_wallBox15.SetRotation( -40.0000f );
	_wallBox15.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox15.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox15.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox15.SetBodyType( b2_staticBody );

	// _wallBox16
	_wallBox16.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox16.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox16.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox16.SetBodyType( b2_staticBody );

	// _wallBox17
	_wallBox17.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox17.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox17.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox17.SetBodyType( b2_staticBody );

	// _fragileBoxStatic3
	_fragileBoxStatic3.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBoxStatic3.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBoxStatic3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBoxStatic3.SetBodyType( b2_staticBody );


	//Set blocks positions
	_wallBox1.SetPosition( 26.3500f, 20.1500f );
	_wallBox2.SetPosition( 46.7500f, 8.1500f );
	_bomb1.SetPosition( 2.0500f, 20.1500f );
	_coin1.SetPosition( 42.2000f, 8.2000f );
	_wallBox3.SetPosition( 45.6500f, 2.2500f );
	_wallBox4.SetPosition( 19.8500f, 2.9000f );
	_wallBox5.SetPosition( 12.8500f, 6.5500f );
	_wallBox6.SetPosition( 5.8500f, 1.9000f );
	_bomb2.SetPosition( 5.6000f, 4.5500f );
	_wallBox7.SetPosition( 31.9500f, 5.9000f );
	_piggyBank1.SetPosition( 42.6500f, 28.2500f );
	_bomb3.SetPosition( 38.6000f, 18.4500f );
	_circleWood1.SetPosition( 5.0000f, 25.6500f );
	_wallBox8.SetPosition( 5.8500f, 22.9500f );
	_wallBox9.SetPosition( 50.0000f, 4.9000f );
	_wallBox10.SetPosition( 1.2000f, 6.0000f );
	_bomb4.SetPosition( 46.8500f, 24.9500f );
	_wallBox11.SetPosition( 47.2000f, 22.3500f );
	_fragileBoxStatic1.SetPosition( 0.3000f, 25.1000f );
	_bomb5.SetPosition( 45.2500f, 4.9000f );
	_wallBox12.SetPosition( 0.6500f, 17.4000f );
	_wallBox13.SetPosition( 17.7500f, 25.7000f );
	_bomb6.SetPosition( 32.6500f, 27.4500f );
	_wallBox14.SetPosition( 40.4000f, 15.8000f );
	_fragileBoxStatic2.SetPosition( 32.7500f, 22.9500f );
	_wallBox15.SetPosition( 0.8500f, 13.1000f );
	_wallBox16.SetPosition( 37.6500f, 26.8000f );
	_wallBox17.SetPosition( 38.4500f, 22.6500f );
	_fragileBoxStatic3.SetPosition( 42.3000f, 23.0500f );

	//Bombs Construction
	_bomb1.SetRange( 2.0f * 8.0000f );
	_bomb1.SetBombImpulse( 6.0f * 800.0000f );
	_bomb2.SetRange( 2.0f * 8.0000f );
	_bomb2.SetBombImpulse( 6.0f * 1600.0000f );
	_bomb3.SetRange( 2.0f * 8.0000f );
	_bomb3.SetBombImpulse( 6.0f * 1450.0000f );
	_bomb4.SetRange( 2.0f * 8.5000f );
	_bomb4.SetBombImpulse( 6.0f * 6000.0000f );
	_bomb5.SetRange( 2.0f * 8.0000f );
	_bomb5.SetBombImpulse( 6.0f * 1100.0000f );
	_bomb6.SetRange( 2.0f * 8.0000f );
	_bomb6.SetBombImpulse( 6.0f * 2550.0000f );

	ConstFinal();
	AdjustOldScreen();
}
//--------------------------------------------------------------------------
LevelPawel_10::LevelPawel_10( GameTypes::LevelEnum levelEnum, bool viewMode ) : Level( levelEnum, viewMode )
{
	_coinCount = 2;
    _levelTag = eLevelTag_Bombs;

	// _bombStatic1
	_bombStatic1.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_bombStatic1.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_bombStatic1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bombStatic1.SetBodyType( b2_staticBody );

	// _bombStatic2
	_bombStatic2.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_bombStatic2.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_bombStatic2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bombStatic2.SetBodyType( b2_staticBody );

	// _bombStatic3
	_bombStatic3.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_bombStatic3.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_bombStatic3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bombStatic3.SetBodyType( b2_staticBody );

	// _bombStatic4
	_bombStatic4.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_bombStatic4.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_bombStatic4.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bombStatic4.SetBodyType( b2_staticBody );

	// _bombStatic5
	_bombStatic5.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_bombStatic5.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_bombStatic5.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bombStatic5.SetBodyType( b2_staticBody );

	// _coin1
	_coin1.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin1.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_coin1.SetBodyType( b2_dynamicBody );

	// _coin2
	_coin2.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin2.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin2.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin2.SetBodyType( b2_dynamicBody );

	// _fragileBoxStatic1
	_fragileBoxStatic1.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBoxStatic1.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBoxStatic1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBoxStatic1.SetBodyType( b2_staticBody );

	// _fragileBoxStatic2
	_fragileBoxStatic2.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBoxStatic2.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBoxStatic2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBoxStatic2.SetBodyType( b2_staticBody );

	// _piggyBankStatic1
	_piggyBankStatic1.GetBody()->GetFixtureList()->SetDensity( 7.5000f );
	_piggyBankStatic1.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_piggyBankStatic1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_piggyBankStatic1.FlipX();
	_piggyBankStatic1.SetBodyType( b2_staticBody );

	// _piggyBankStatic2
	_piggyBankStatic2.GetBody()->GetFixtureList()->SetDensity( 7.5000f );
	_piggyBankStatic2.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_piggyBankStatic2.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_piggyBankStatic2.SetBodyType( b2_staticBody );

	// _wallBox1
	_wallBox1.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox1.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox1.SetBodyType( b2_staticBody );

	// _wallBox2
	_wallBox2.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox2.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox2.SetBodyType( b2_staticBody );

	// _wallBox3
	_wallBox3.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox3.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox3.SetBodyType( b2_staticBody );

	// _wallBox4
	_wallBox4.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox4.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox4.GetBody()->GetFixtureList()->SetRestitution( 0.0500f );
	_wallBox4.SetBodyType( b2_staticBody );

	// _wallBox5
	_wallBox5.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox5.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox5.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox5.SetBodyType( b2_staticBody );

	// _wallBox6
	_wallBox6.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox6.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox6.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox6.SetBodyType( b2_staticBody );

	// _wallBox7
	_wallBox7.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox7.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox7.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox7.SetBodyType( b2_staticBody );

	// _wallBox8
	_wallBox8.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox8.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox8.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox8.SetBodyType( b2_staticBody );

	// _wallBox9
	_wallBox9.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox9.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox9.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox9.SetBodyType( b2_staticBody );

	// _wallBox10
	_wallBox10.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox10.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox10.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox10.SetBodyType( b2_staticBody );

	// _wallBox11
	_wallBox11.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox11.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox11.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox11.SetBodyType( b2_staticBody );

	// _woodBox1
	_woodBox1.SetRotation( -90.0000f );
	_woodBox1.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox1.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_woodBox1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_woodBox1.SetBodyType( b2_dynamicBody );

	// _woodBox2
	_woodBox2.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox2.GetBody()->GetFixtureList()->SetFriction( 0.7500f );
	_woodBox2.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_woodBox2.SetBodyType( b2_dynamicBody );

	// _woodBox3
	_woodBox3.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox3.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_woodBox3.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_woodBox3.SetBodyType( b2_dynamicBody );

	// _woodBox4
	_woodBox4.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox4.GetBody()->GetFixtureList()->SetFriction( 0.2500f );
	_woodBox4.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_woodBox4.SetBodyType( b2_dynamicBody );

	// _chain1
	_chain1.SetChain( 20.2500f);
	_chain1.SetRotation( 270.0000f );
	_chain1.FixHook( false );

	// _wallBox12
	_wallBox12.SetRotation( 120.0000f );
	_wallBox12.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox12.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox12.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox12.SetBodyType( b2_staticBody );

	// _wallBox13
	_wallBox13.SetRotation( 150.0000f );
	_wallBox13.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox13.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox13.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox13.SetBodyType( b2_staticBody );

	// _wallBox14
	_wallBox14.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox14.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox14.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox14.SetBodyType( b2_staticBody );

	// _wallBox15
	_wallBox15.SetRotation( 90.0000f );
	_wallBox15.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox15.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox15.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox15.SetBodyType( b2_staticBody );


	//Set blocks positions
	_bombStatic1.SetPosition( 85.4489f, 25.5002f );
	_bombStatic2.SetPosition( 58.6998f, 47.2999f );
	_bombStatic3.SetPosition( 85.4498f, 49.5503f );
	_bombStatic4.SetPosition( 58.6999f, 8.5500f );
	_bombStatic5.SetPosition( 36.9500f, 8.5500f );
	_coin1.SetPosition( 36.1498f, 39.3500f );
	_coin2.SetPosition( 84.4992f, 37.1001f );
	_fragileBoxStatic1.SetPosition( 21.5500f, 14.4000f );
	_fragileBoxStatic2.SetPosition( 71.7999f, 48.9999f );
	_piggyBankStatic1.SetPosition( 8.6001f, 54.0001f );
	_piggyBankStatic2.SetPosition( 85.0496f, 8.7500f );
	_wallBox1.SetPosition( 84.9009f, 31.3001f );
	_wallBox2.SetPosition( 71.2997f, 24.5498f );
	_wallBox3.SetPosition( 91.7507f, 31.3000f );
	_wallBox4.SetPosition( 32.4495f, 15.0000f );
	_wallBox5.SetPosition( 43.7998f, 47.9002f );
	_wallBox6.SetPosition( 5.7001f, 32.5497f );
	_wallBox7.SetPosition( 62.6498f, 12.7500f );
	_wallBox8.SetPosition( 22.4993f, 28.1500f );
	_wallBox9.SetPosition( 49.9993f, 13.7000f );
	_wallBox10.SetPosition( 71.2997f, 12.7500f );
	_wallBox11.SetPosition( 42.3497f, 15.0000f );
	_woodBox1.SetPosition( 45.7496f, 24.2000f );
	_woodBox2.SetPosition( 78.8997f, 56.5001f );
	_woodBox3.SetPosition( 52.6998f, 54.6999f );
	_woodBox4.SetPosition( 29.5996f, 20.3500f );
	_chain1.SetPosition( 36.1498f, 58.1500f );
	_wallBox12.SetPosition( 65.6398f, 36.6000f );
	_wallBox13.SetPosition( 69.5501f, 32.6397f );
	_wallBox14.SetPosition( 75.1000f, 31.3000f );
	_wallBox15.SetPosition( 64.1500f, 46.4000f );

	//Bombs Construction
	_bombStatic1.SetRange( 15.0000f );
	_bombStatic1.SetBombImpulse( 14425.0000f );
	_bombStatic2.SetRange( 15.7000f );
	_bombStatic2.SetBombImpulse( 6487.5000f );
	_bombStatic3.SetRange( 9.5000f );
	_bombStatic3.SetBombImpulse( 11087.5000f );
	_bombStatic4.SetRange( 16.0000f );
	_bombStatic4.SetBombImpulse( 4150.0000f );
	_bombStatic5.SetRange( 15.5000f );
	_bombStatic5.SetBombImpulse( 14300.0000f );

	//Set chains hooks
	_chain1.HookOnChain( &_coin1 );

	ConstFinal();
}
//--------------------------------------------------------------------------
LevelPawel_11::LevelPawel_11( GameTypes::LevelEnum levelEnum, bool viewMode ) : Level( levelEnum, viewMode )
{
    _levelTag = eLevelTag_Button;
    
	// _bomb1
	_bomb1.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_bomb1.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_bomb1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bomb1.SetBodyType( b2_dynamicBody );

	// _bombStatic1
	_bombStatic1.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_bombStatic1.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_bombStatic1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bombStatic1.SetBodyType( b2_staticBody );

	// _bombStatic2
	_bombStatic2.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_bombStatic2.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_bombStatic2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bombStatic2.SetBodyType( b2_staticBody );

	// _coin1
	_coin1.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin1.GetBody()->GetFixtureList()->SetFriction( 0.3500f );
	_coin1.GetBody()->GetFixtureList()->SetRestitution( 0.0000f );
	_coin1.SetBodyType( b2_dynamicBody );

	// _fragileBoxStatic1
	_fragileBoxStatic1.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBoxStatic1.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBoxStatic1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBoxStatic1.SetBodyType( b2_staticBody );

	// _fragileBoxStatic2
	_fragileBoxStatic2.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBoxStatic2.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBoxStatic2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBoxStatic2.SetBodyType( b2_staticBody );

	// _wallBox1
	_wallBox1.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox1.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox1.SetBodyType( b2_staticBody );

	// _wallBox2
	_wallBox2.SetRotation( 0.0000f );
	_wallBox2.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox2.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox2.SetBodyType( b2_staticBody );

	// _wallBox3
	_wallBox3.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox3.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox3.SetBodyType( b2_staticBody );

	// _wallBox4
	_wallBox4.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox4.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox4.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox4.SetBodyType( b2_staticBody );

	// _chain1
	_chain1.SetChain( 16.5000f);
	_chain1.SetRotation( 270.0000f );
	_chain1.FixHook( false );

	// _bomb2
	_bomb2.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_bomb2.GetBody()->GetFixtureList()->SetFriction( 0.2500f );
	_bomb2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bomb2.SetBodyType( b2_dynamicBody );

	// _bombStatic3
	_bombStatic3.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_bombStatic3.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_bombStatic3.GetBody()->GetFixtureList()->SetRestitution( 0.0000f );
	_bombStatic3.SetBodyType( b2_staticBody );

	// _wallBox5
	_wallBox5.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox5.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox5.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox5.SetBodyType( b2_staticBody );

	// _piggyBank1
	_piggyBank1.GetBody()->GetFixtureList()->SetDensity( 7.5000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_piggyBank1.FlipX();
	_piggyBank1.SetBodyType( b2_dynamicBody );

	// _woodBox1
	_woodBox1.SetRotation( 90.0000f );
	_woodBox1.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox1.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_woodBox1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_woodBox1.SetBodyType( b2_dynamicBody );

	// _wallBox6
	_wallBox6.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox6.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox6.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox6.SetBodyType( b2_staticBody );

	// _bombStatic4
	_bombStatic4.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_bombStatic4.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_bombStatic4.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bombStatic4.SetBodyType( b2_staticBody );

	// _wallBox7
	_wallBox7.SetRotation( -11.0000f );
	_wallBox7.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox7.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox7.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox7.SetBodyType( b2_staticBody );

	// _wallBox8
	_wallBox8.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox8.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox8.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox8.SetBodyType( b2_staticBody );

	// _button1
	_button1.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_button1.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_button1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_button1.SetBodyType( b2_dynamicBody );

	// _wallBox9
	_wallBox9.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox9.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox9.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox9.SetBodyType( b2_staticBody );


	//Set blocks positions
	_bomb1.SetPosition( 37.8516f, 35.7500f );
	_bombStatic1.SetPosition( 85.2016f, 8.6000f );
	_bombStatic2.SetPosition( 65.3500f, 51.1998f );
	_coin1.SetPosition( 83.2517f, 26.2499f );
	_fragileBoxStatic1.SetPosition( 85.2017f, 17.4000f );
	_fragileBoxStatic2.SetPosition( 85.2018f, 42.0002f );
	_wallBox1.SetPosition( 76.6518f, 5.3501f );
	_wallBox2.SetPosition( 9.0501f, 15.1000f );
	_wallBox3.SetPosition( 87.6018f, 32.8499f );
	_wallBox4.SetPosition( 59.0501f, 44.1501f );
	_chain1.SetPosition( 47.7812f, 52.2376f );
	_bomb2.SetPosition( 84.9017f, 51.7002f );
	_bombStatic3.SetPosition( 65.3500f, 35.4499f );
	_wallBox5.SetPosition( 51.4500f, 29.9500f );
	_piggyBank1.SetPosition( 12.5500f, 44.0498f );
	_woodBox1.SetPosition( 59.0000f, 53.2000f );
	_wallBox6.SetPosition( 14.5500f, 35.6000f );
	_bombStatic4.SetPosition( 10.2000f, 8.5999f );
	_wallBox7.SetPosition( 40.7499f, 10.0000f );
	_wallBox8.SetPosition( 19.5501f, 37.7000f );
	_button1.SetPosition( 47.7812f, 37.4376f );
	_wallBox9.SetPosition( 76.6502f, 16.4000f );

	//Bombs Construction
	_bomb1.SetRange( 16.0000f );
	_bomb1.SetBombImpulse( 8050.0000f );
	_bombStatic1.SetRange( 22.1000f );
	_bombStatic1.SetBombImpulse( 8350.0000f );
	_bombStatic2.SetRange( 11.0000f );
	_bombStatic2.SetBombImpulse( 4050.0000f );
	_bomb2.SetRange( 16.0000f );
	_bomb2.SetBombImpulse( 5350.0000f );
	_bombStatic3.SetRange( 16.0000f );
	_bombStatic3.SetBombImpulse( 10075.0000f );
	_bombStatic4.SetRange( 16.0000f );
	_bombStatic4.SetBombImpulse( 8750.0000f );

	//Set chains hooks
	_chain1.HookOnChain( &_button1 );
	
	ConstFinal();
}
//--------------------------------------------------------------------------
LevelPawel_12::LevelPawel_12( GameTypes::LevelEnum levelEnum, bool viewMode ) : Level( levelEnum, viewMode )
{
    _levelTag = eLevelTag_Button;
    
	// _bombStatic1
	_bombStatic1.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_bombStatic1.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_bombStatic1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bombStatic1.SetBodyType( b2_staticBody );

	// _bombStatic2
	_bombStatic2.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_bombStatic2.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_bombStatic2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bombStatic2.SetBodyType( b2_staticBody );

	// _bombStatic3
	_bombStatic3.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_bombStatic3.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_bombStatic3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bombStatic3.SetBodyType( b2_staticBody );

	// _bombStatic4
	_bombStatic4.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_bombStatic4.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_bombStatic4.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bombStatic4.SetBodyType( b2_staticBody );

	// _circleFragile1
	_circleFragile1.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_circleFragile1.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_circleFragile1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_circleFragile1.SetBodyType( b2_dynamicBody );

	// _coin1
	_coin1.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin1.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin1.SetBodyType( b2_dynamicBody );

	// _piggyBank1
	_piggyBank1.GetBody()->GetFixtureList()->SetDensity( 7.5000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_piggyBank1.SetBodyType( b2_dynamicBody );

	// _wallBox1
	_wallBox1.SetRotation( -45.0000f );
	_wallBox1.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox1.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox1.SetBodyType( b2_staticBody );

	// _wallBox2
	_wallBox2.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox2.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox2.SetBodyType( b2_staticBody );

	// _wallBox3
	_wallBox3.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox3.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox3.SetBodyType( b2_staticBody );

	// _wallBox4
	_wallBox4.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox4.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox4.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox4.SetBodyType( b2_staticBody );

	// _wallBox5
	_wallBox5.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox5.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox5.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox5.SetBodyType( b2_staticBody );

	// _wallBox6
	_wallBox6.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox6.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox6.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox6.SetBodyType( b2_staticBody );

	// _wallBox7
	_wallBox7.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox7.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox7.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox7.SetBodyType( b2_staticBody );

	// _wallBox8
	_wallBox8.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox8.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox8.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox8.SetBodyType( b2_staticBody );

	// _wallBox9
	_wallBox9.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox9.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox9.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox9.SetBodyType( b2_staticBody );

	// _wallBox10
	_wallBox10.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox10.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox10.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox10.SetBodyType( b2_staticBody );

	// _woodBox1
	_woodBox1.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox1.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_woodBox1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_woodBox1.SetBodyType( b2_dynamicBody );

	// _wallBox11
	_wallBox11.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox11.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox11.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox11.SetBodyType( b2_staticBody );

	// _chain1
	_chain1.SetChain( 16.0000f);
	_chain1.SetRotation( 240.0000f );
	_chain1.FixHook( false );

	// _chain2
	_chain2.SetChain( 16.0000f);
	_chain2.SetRotation( 360.0000f );
	_chain2.FixHook( false );

	// _woodBox2
	_woodBox2.SetRotation( 90.0000f );
	_woodBox2.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox2.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_woodBox2.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_woodBox2.SetBodyType( b2_dynamicBody );

	// _woodBox3
	_woodBox3.SetRotation( 90.0000f );
	_woodBox3.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox3.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_woodBox3.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_woodBox3.SetBodyType( b2_dynamicBody );

	// _wallBox12
	_wallBox12.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox12.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox12.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox12.SetBodyType( b2_staticBody );

	// _button1
	_button1.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_button1.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_button1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_button1.SetBodyType( b2_dynamicBody );


	//Set blocks positions
	_bombStatic1.SetPosition( 57.95f, 7.6500f );
	_bombStatic2.SetPosition( 29.0510f, 7.6500f );
	_bombStatic3.SetPosition( 11.0500f, 39.3501f );
	_bombStatic4.SetPosition( 11.0498f, 29.1000f );
	_circleFragile1.SetPosition( 82.5714f, 44.2394f );
	_coin1.SetPosition( 57.95f, 17.4000f );
	_piggyBank1.SetPosition( 57.95002f, 56.0500f );
	_wallBox1.SetPosition( 21.4505f, 18.6000f );
	_wallBox2.SetPosition( 63.6502f, 21.2500f );
	_wallBox3.SetPosition( 19.8002f, 25.9000f );
	_wallBox4.SetPosition( 61.6502f, 12.8500f );
	_wallBox5.SetPosition( 54.2502f, 12.8500f );
	_wallBox6.SetPosition( 7.8500f, 43.3500f );
	_wallBox7.SetPosition( 19.7999f, 43.3500f );
	_wallBox8.SetPosition( 52.2002f, 43.3500f );
	_wallBox9.SetPosition( 41.4503f, 43.3500f );
	_wallBox10.SetPosition( 52.2502f, 21.2500f );
	_woodBox1.SetPosition( 14.0001f, 45.8499f );
	_wallBox11.SetPosition( 57.95f, 47.2500f );
	_chain1.SetPosition( 90.0514f, 57.0251f );
	_chain2.SetPosition( 67.7713f, 44.2394f );
	_woodBox2.SetPosition( 52.2500f, 30.2500f );
	_woodBox3.SetPosition( 63.6500f, 30.2500f );
	_wallBox12.SetPosition( 37.4499f, 12.8500f );
	_button1.SetPosition( 19.7999f, 31.5500f );

	//Bombs Construction
	_bombStatic1.SetRange( 18.0000f );
	_bombStatic1.SetBombImpulse( 7350.0000f );
	_bombStatic2.SetRange( 16.0000f );
	_bombStatic2.SetBombImpulse( 10950.0000f );
	_bombStatic3.SetRange( 8.7000f );
	_bombStatic3.SetBombImpulse( 6825.0000f );
	_bombStatic4.SetRange( 15.8000f );
	_bombStatic4.SetBombImpulse( 15775.0000f );

	//Set chains hooks
	_chain1.HookOnChain( &_circleFragile1 );
	_chain2.HookOnChain( &_circleFragile1 );

	ConstFinal();
}
//--------------------------------------------------------------------------
LevelPawel_13::LevelPawel_13( GameTypes::LevelEnum levelEnum, bool viewMode ) : Level( levelEnum, viewMode )
{
    _levelTag = eLevelTag_Hacker;
    
	b2Filter filter;
	// _bombStatic1
	_bombStatic1.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_bombStatic1.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_bombStatic1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bombStatic1.SetBodyType( b2_staticBody );

	// _bombStatic2
	_bombStatic2.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_bombStatic2.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_bombStatic2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bombStatic2.SetBodyType( b2_staticBody );

	// _bombStatic3
	_bombStatic3.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_bombStatic3.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_bombStatic3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bombStatic3.SetBodyType( b2_staticBody );

	// _fragileBox1
	_fragileBox1.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox1.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox1.SetBodyType( b2_dynamicBody );

	// _fragileBox2
	_fragileBox2.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox2.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox2.SetBodyType( b2_dynamicBody );

	// _fragileBoxStatic1
	_fragileBoxStatic1.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBoxStatic1.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBoxStatic1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBoxStatic1.SetBodyType( b2_staticBody );

	// _piggyBank1
	_piggyBank1.GetBody()->GetFixtureList()->SetDensity( 7.5000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_piggyBank1.GetBody()->SetAngularDamping(0.5000f );
	_piggyBank1.GetBody()->SetLinearDamping(0.5000f );
	_piggyBank1.FlipX();
	_piggyBank1.SetBodyType( b2_dynamicBody );

	// _screw1
	_screw1.SetRotation( 29.4000f );
	_screw1.GetBody()->GetFixtureList()->SetDensity( 0.1000f );
	_screw1.GetBody()->GetFixtureList()->SetFriction( 0.1000f );
	_screw1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	filter.groupIndex = -1;
	filter.categoryBits = 0;
	filter.maskBits = 65535;
	_screw1.GetBody()->GetFixtureList()->SetFilterData( filter );
	_screw1.SetBodyType( b2_staticBody );

	// _wallBox1
	_wallBox1.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox1.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox1.SetBodyType( b2_staticBody );

	// _wallBox2
	_wallBox2.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox2.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox2.SetBodyType( b2_staticBody );

	// _wallBox3
	_wallBox3.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox3.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox3.SetBodyType( b2_staticBody );

	// _wallBox4
	_wallBox4.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox4.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox4.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox4.SetBodyType( b2_staticBody );

	// _wallBox5
	_wallBox5.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox5.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox5.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox5.SetBodyType( b2_staticBody );

	// _wallBox6
	_wallBox6.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox6.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox6.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox6.SetBodyType( b2_staticBody );

	// _wallBox7
	_wallBox7.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox7.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox7.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox7.SetBodyType( b2_staticBody );

	// _wallBox8
	_wallBox8.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox8.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox8.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox8.SetBodyType( b2_staticBody );

	// _wallBox9
	_wallBox9.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox9.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox9.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox9.SetBodyType( b2_staticBody );

	// _woodBox1
	_woodBox1.SetRotation( 90.00f );
	_woodBox1.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox1.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_woodBox1.GetBody()->GetFixtureList()->SetRestitution( 0.0500f );
	filter.groupIndex = -3;
	filter.categoryBits = 1;
	filter.maskBits = 65535;
	_woodBox1.GetBody()->GetFixtureList()->SetFilterData( filter );
	_woodBox1.SetBodyType( b2_dynamicBody );

	// _woodBox2
	_woodBox2.SetRotation( 90.0000f );
	_woodBox2.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox2.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_woodBox2.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_woodBox2.SetBodyType( b2_dynamicBody );

	// _wallBox10
	_wallBox10.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox10.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox10.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox10.SetBodyType( b2_staticBody );

	// _wallBox11
	_wallBox11.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox11.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox11.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox11.SetBodyType( b2_staticBody );

	// _woodBox3
	_woodBox3.SetRotation( 90.0000f );
	_woodBox3.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox3.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_woodBox3.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	filter.groupIndex = -2;
	filter.categoryBits = 1;
	filter.maskBits = 65535;
	_woodBox3.GetBody()->GetFixtureList()->SetFilterData( filter );
	_woodBox3.SetBodyType( b2_dynamicBody );

	// _woodBox4
	_woodBox4.SetRotation( 90.0000f );
	_woodBox4.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox4.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_woodBox4.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_woodBox4.SetBodyType( b2_dynamicBody );

	// _woodBox5
	_woodBox5.SetRotation( 90.0000f );
	_woodBox5.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox5.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_woodBox5.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_woodBox5.SetBodyType( b2_dynamicBody );

	// _woodBox6
	_woodBox6.SetRotation( 90.0000f );
	_woodBox6.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox6.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_woodBox6.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_woodBox6.SetBodyType( b2_dynamicBody );

	// _woodBox7
	_woodBox7.SetRotation( 90.0000f );
	_woodBox7.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox7.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_woodBox7.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_woodBox7.SetBodyType( b2_dynamicBody );

	// _wallBox12
	_wallBox12.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox12.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox12.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	filter.groupIndex = -2;
	filter.categoryBits = 1;
	filter.maskBits = 65535;
	_wallBox12.GetBody()->GetFixtureList()->SetFilterData( filter );
	_wallBox12.SetBodyType( b2_staticBody );

	// _wallBox13
	_wallBox13.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox13.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox13.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	filter.groupIndex = -2;
	filter.categoryBits = 1;
	filter.maskBits = 65535;
	_wallBox13.GetBody()->GetFixtureList()->SetFilterData( filter );
	_wallBox13.SetBodyType( b2_staticBody );

	// _woodBox8
	_woodBox8.SetRotation( 90.0000f );
	_woodBox8.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox8.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_woodBox8.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_woodBox8.SetBodyType( b2_dynamicBody );

	// _woodBox9
	_woodBox9.SetRotation( 90.0000f );
	_woodBox9.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox9.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_woodBox9.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_woodBox9.SetBodyType( b2_dynamicBody );

	// _woodBox10
	_woodBox10.SetRotation( 90.0000f );
	_woodBox10.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox10.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_woodBox10.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	filter.groupIndex = -2;
	filter.categoryBits = 1;
	filter.maskBits = 65535;
	_woodBox10.GetBody()->GetFixtureList()->SetFilterData( filter );
	_woodBox10.SetBodyType( b2_dynamicBody );

	// _woodBox11
	_woodBox11.SetRotation( 90.0000f );
	_woodBox11.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox11.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_woodBox11.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_woodBox11.SetBodyType( b2_dynamicBody );

	// _woodBox12
	_woodBox12.SetRotation( 90.0000f );
	_woodBox12.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox12.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_woodBox12.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_woodBox12.SetBodyType( b2_dynamicBody );

	// _coin1
	_coin1.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin1.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin1.GetBody()->GetFixtureList()->SetRestitution( 0.0500f );
	_coin1.SetBodyType( b2_dynamicBody );

	// _wallBox14
	_wallBox14.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox14.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox14.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox14.SetBodyType( b2_staticBody );

	// _screw2
	_screw2.GetBody()->GetFixtureList()->SetDensity( 0.1000f );
	_screw2.GetBody()->GetFixtureList()->SetFriction( 0.1000f );
	_screw2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	filter.groupIndex = -1;
	filter.categoryBits = 0;
	filter.maskBits = 65535;
	_screw2.GetBody()->GetFixtureList()->SetFilterData( filter );
	_screw2.SetBodyType( b2_staticBody );

	// _screw3
	_screw3.GetBody()->GetFixtureList()->SetDensity( 0.1000f );
	_screw3.GetBody()->GetFixtureList()->SetFriction( 0.1000f );
	_screw3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	filter.groupIndex = -1;
	filter.categoryBits = 0;
	filter.maskBits = 65535;
	_screw3.GetBody()->GetFixtureList()->SetFilterData( filter );
	_screw3.SetBodyType( b2_staticBody );

	// _wallBox15
	_wallBox15.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox15.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox15.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	filter.groupIndex = -3;
	filter.categoryBits = 1;
	filter.maskBits = 65535;
	_wallBox15.GetBody()->GetFixtureList()->SetFilterData( filter );
	_wallBox15.SetBodyType( b2_staticBody );

	// _wallBox16
	_wallBox16.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox16.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox16.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox16.SetBodyType( b2_staticBody );
	filter.groupIndex = -3;
	filter.categoryBits = 1;
	filter.maskBits = 65535;
	_wallBox16.GetBody()->GetFixtureList()->SetFilterData( filter );

	// _button1
	_button1.GetBody()->GetFixtureList()->SetDensity( 3.2500f );
	_button1.GetBody()->GetFixtureList()->SetFriction( 0.800f );
	_button1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_button1.SetBodyType( b2_dynamicBody );

	// _woodBox13
	_woodBox13.SetRotation( 90.0000f );
	_woodBox13.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox13.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_woodBox13.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_woodBox13.SetBodyType( b2_dynamicBody );

	// _screw4
	_screw4.SetRotation( -45.6000f );
	_screw4.GetBody()->GetFixtureList()->SetDensity( 0.1000f );
	_screw4.GetBody()->GetFixtureList()->SetFriction( 0.1000f );
	_screw4.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	filter.groupIndex = -1;
	filter.categoryBits = 0;
	filter.maskBits = 65535;
	_screw4.GetBody()->GetFixtureList()->SetFilterData( filter );
	_screw4.SetBodyType( b2_staticBody );

	// _woodBox14
	_woodBox14.SetRotation( 15.4000f );
	_woodBox14.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox14.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_woodBox14.GetBody()->GetFixtureList()->SetRestitution( 0.0000f );
	filter.groupIndex = -3;
	filter.categoryBits = 1;
	filter.maskBits = 65535;
	_woodBox14.GetBody()->GetFixtureList()->SetFilterData( filter );
	_woodBox14.SetBodyType( b2_dynamicBody );

	// _bomb1
	_bomb1.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_bomb1.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_bomb1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bomb1.SetBodyType( b2_dynamicBody );

	// _bomb2
	_bomb2.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_bomb2.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_bomb2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bomb2.SetBodyType( b2_dynamicBody );

	//Set blocks positions
	_bombStatic1.SetPosition( 7.5998f, 34.9992f );
	_bombStatic2.SetPosition( 7.5998f, 9.1000f );
	_bombStatic3.SetPosition( 39.05f, 34.9992f );
	_fragileBox1.SetPosition( 27.9f, 44.8993f );
	_fragileBox2.SetPosition( 50.4508f, 26.3499f );
	_fragileBoxStatic1.SetPosition( 27.9f, 35.2994f );
	_piggyBank1.SetPosition( 65.2515f, 25.4001f );
	_screw1.SetPosition( 81.00f, 3.43f );
	
	_wallBox1.SetPosition( 48.40f, 33.5f );
	_wallBox2.SetPosition( 52.50f, 33.5f );
	
	_wallBox8.SetPosition( 48.40f, 54.75f );
	_wallBox7.SetPosition( 52.50f, 54.75f );
	
	_wallBox4.SetPosition( 48.40f, 18.4f );
	_wallBox3.SetPosition( 52.50f, 18.4f );
	

	_wallBox5.SetPosition( 11.3500f, 14.5000f );
	_wallBox6.SetPosition( 38.3508f, 41.0990f );
	
	_woodBox1.SetPosition( 81.00f, 8.5f );
	_woodBox2.SetPosition( 38.2506f, 45.5990f );
	_wallBox10.SetPosition( 26.6501f, 14.5000f );
	_wallBox11.SetPosition( 11.3498f, 41.0995f );
	_woodBox3.SetPosition( 61.1503f, 48.6000f );
	_woodBox4.SetPosition( 67.1008f, 48.6000f );
	_woodBox5.SetPosition( 64.1507f, 58.9501f );
	_woodBox6.SetPosition( 70.1513f, 58.9501f );
	_woodBox7.SetPosition( 76.1515f, 58.9501f );
	_wallBox12.SetPosition( 74.7005f, 44.5500f );
	_wallBox13.SetPosition( 74.7005f, 54.750f );
	_woodBox8.SetPosition( 79.1017f, 48.6000f );
	_woodBox9.SetPosition( 73.1015f, 48.6000f );
	_woodBox10.SetPosition( 88.1516f, 58.8001f );
	_woodBox11.SetPosition( 85.1017f, 48.6000f );
	_woodBox12.SetPosition( 82.1515f, 58.9501f );
	_coin1.SetPosition( 66.7501f, 38.4999f );
	_wallBox14.SetPosition( 70.1501f, 33.3499f );
	_screw2.SetPosition( 88.1508f, 54.6999f );
	_screw3.SetPosition( 61.1508f, 44.5499f );
	_wallBox15.SetPosition( 70.1501f, 18.4000f );
	_woodBox13.SetPosition( 50.5501f, 50.2999f );
	_screw4.SetPosition( 81.0352f, 18.3385f );
	_woodBox14.SetPosition( 86.1227f, 19.6119f );
	
	_bomb1.SetPosition( 39.05f, 10.7500f );
	_bomb2.SetPosition( 66.75f, 10.7500f );
	_button1.SetPosition( 58.40f, 9.40f );

	_wallBox9.SetPosition( 17.05f, 3.4000f );
	_wallBox16.SetPosition( 52.950f, 3.4000f );

	//Bombs Construction
	_bombStatic1.SetRange( 13.7000f );
	_bombStatic1.SetBombImpulse( 3700.0000f );
	_bombStatic2.SetRange( 16.0000f );
	_bombStatic2.SetBombImpulse( 1250.0000f );
	_bombStatic3.SetRange( 11.0000f );
	_bombStatic3.SetBombImpulse( 5375.0000f );
	
	_bomb1.SetRange( 13.8000f );
	_bomb1.SetBombImpulse( 8800.0000f );
	_bomb2.SetRange( 13.8000f );
	_bomb2.SetBombImpulse( 15300.0000f );

	ConstFinal();
	CreateJoints();
}
//--------------------------------------------------------------------------
void LevelPawel_13::CreateJoints()
{
	b2RevoluteJointDef jointRevDef1;
	jointRevDef1.collideConnected = false;
	jointRevDef1.Initialize( _woodBox10.GetBody(), _screw2.GetBody(), _screw2.GetPosition() );
	_jointRev1 = (b2RevoluteJoint *) _world.CreateJoint( &jointRevDef1 );

	_jointRev1->EnableMotor( true );
	_jointRev1->SetMotorSpeed( 0.0000f );
	_jointRev1->SetMaxMotorTorque( 0.0000f );

	b2RevoluteJointDef jointRevDef2;
	jointRevDef2.collideConnected = false;
	jointRevDef2.Initialize( _screw3.GetBody(), _woodBox3.GetBody(), _screw3.GetPosition() );
	_jointRev2 = (b2RevoluteJoint *) _world.CreateJoint( &jointRevDef2 );

	_jointRev2->EnableMotor( true );
	_jointRev2->SetMotorSpeed( 0.0000f );
	_jointRev2->SetMaxMotorTorque( 0.0000f );

	b2RevoluteJointDef jointRevDef3;
	jointRevDef3.collideConnected = false;
	jointRevDef3.Initialize( _screw1.GetBody(), _woodBox1.GetBody(), _screw1.GetPosition() );
	_jointRev3 = (b2RevoluteJoint *) _world.CreateJoint( &jointRevDef3 );

	_jointRev3->EnableMotor( true );
	_jointRev3->SetMotorSpeed( -0.8000f );
	_jointRev3->SetMaxMotorTorque( 4450.0000f );

	b2RevoluteJointDef jointRevDef4;
	jointRevDef4.collideConnected = false;
	jointRevDef4.Initialize( _screw4.GetBody(), _woodBox14.GetBody(), _screw4.GetPosition() );
	_jointRev4 = (b2RevoluteJoint *) _world.CreateJoint( &jointRevDef4 );

	_jointRev4->SetLimits( 0.0000f, 1.3000f );
	_jointRev4->EnableLimit( true );
	_jointRev4->EnableMotor( true );
	_jointRev4->SetMotorSpeed( 0.0000f );
	_jointRev4->SetMaxMotorTorque( 0.0000f );

	b2GearJointDef jointGearDef1;
	jointGearDef1.bodyA = _woodBox1.GetBody();
	jointGearDef1.bodyB = _woodBox14.GetBody();
	jointGearDef1.joint1 = _jointRev3;
	jointGearDef1.joint2 = _jointRev4;
	jointGearDef1.ratio = 1.19f;
	_jointGear1 = (b2GearJoint *) _world.CreateJoint( &jointGearDef1 );
}
//--------------------------------------------------------------------------
LevelPawel_13::~LevelPawel_13()
{
	if ( _jointGear1 ) _world.DestroyJoint( _jointGear1 );
	
	if ( _jointRev1 ) _world.DestroyJoint( _jointRev1 );
	if ( _jointRev2 ) _world.DestroyJoint( _jointRev2 );
	if ( _jointRev3 ) _world.DestroyJoint( _jointRev3 );
	if ( _jointRev4 ) _world.DestroyJoint( _jointRev4 );
}
//--------------------------------------------------------------------------
LevelPawel_14::LevelPawel_14( GameTypes::LevelEnum levelEnum, bool viewMode ) : Level( levelEnum, viewMode )
{
	// _chain1
	_chain1.SetChain( 10.0000f);
	_chain1.SetRotation( 261.3002f );
	_chain1.FixHook( false );

	// _chain2
	_chain2.SetChain( 10.0000f);
	_chain2.SetRotation( 276.5999f );
	_chain2.FixHook( false );

	// _circleFragile1
	_circleFragile1.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_circleFragile1.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_circleFragile1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_circleFragile1.SetBodyType( b2_dynamicBody );

	// _circleFragile2
	_circleFragile2.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_circleFragile2.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_circleFragile2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_circleFragile2.SetBodyType( b2_dynamicBody );

	// _coin1
	_coin1.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_coin1.GetBody()->GetFixtureList()->SetFriction( 1.0000f );
	_coin1.GetBody()->GetFixtureList()->SetRestitution( 0.0000f );
	_coin1.SetBodyType( b2_dynamicBody );

	// _wallBox1
	_wallBox1.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox1.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox1.SetBodyType( b2_staticBody );

	// _wallBox2
	_wallBox2.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox2.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox2.SetBodyType( b2_staticBody );

	// _bomb1
	_bomb1.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_bomb1.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_bomb1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bomb1.SetBodyType( b2_staticBody );

	// _bomb2
	_bomb2.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_bomb2.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_bomb2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bomb2.SetBodyType( b2_staticBody );

	// _wallBox3
	_wallBox3.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox3.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox3.SetBodyType( b2_staticBody );

	// _woodBox1
	_woodBox1.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox1.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_woodBox1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_woodBox1.SetBodyType( b2_dynamicBody );

	// _wallBox4
	_wallBox4.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox4.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox4.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox4.SetBodyType( b2_staticBody );

	// _wallBox5
	_wallBox5.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox5.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox5.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox5.SetBodyType( b2_staticBody );

	// _bomb3
	_bomb3.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_bomb3.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_bomb3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bomb3.SetBodyType( b2_staticBody );

	// _wallBox6
	_wallBox6.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox6.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox6.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox6.SetBodyType( b2_staticBody );

	// _wallBox7
	_wallBox7.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox7.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox7.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox7.SetBodyType( b2_staticBody );

	// _wallBox8
	_wallBox8.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox8.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox8.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox8.SetBodyType( b2_staticBody );

	// _bomb4
	_bomb4.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_bomb4.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_bomb4.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bomb4.SetBodyType( b2_staticBody );

	// _wallBox9
	_wallBox9.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox9.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox9.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox9.SetBodyType( b2_staticBody );

	// _wallBox10
	_wallBox10.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox10.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox10.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox10.SetBodyType( b2_staticBody );

	// _wallBox11
	_wallBox11.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox11.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox11.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox11.SetBodyType( b2_staticBody );

	// _woodBox2
	_woodBox2.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox2.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_woodBox2.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_woodBox2.SetBodyType( b2_dynamicBody );

	// _piggyBank1
	_piggyBank1.GetBody()->GetFixtureList()->SetDensity( 7.5000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_piggyBank1.SetBodyType( b2_staticBody );

	// _woodBox3
	_woodBox3.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox3.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_woodBox3.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_woodBox3.SetBodyType( b2_dynamicBody );

	// _bomb5
	_bomb5.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_bomb5.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_bomb5.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bomb5.SetBodyType( b2_staticBody );

	// _bomb6
	_bomb6.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_bomb6.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_bomb6.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bomb6.SetBodyType( b2_staticBody );

	// _bomb7
	_bomb7.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_bomb7.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_bomb7.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bomb7.SetBodyType( b2_staticBody );

	// _bomb8
	_bomb8.SetRotation( -0.6000f );
	_bomb8.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_bomb8.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_bomb8.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bomb8.SetBodyType( b2_staticBody );

	// _wallBox12
	_wallBox12.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox12.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox12.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox12.SetBodyType( b2_staticBody );

	// _woodBox4
	_woodBox4.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox4.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_woodBox4.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_woodBox4.SetBodyType( b2_dynamicBody );

	// _wallBox13
	_wallBox13.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox13.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox13.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox13.SetBodyType( b2_staticBody );


	//Set blocks positions
	_chain1.SetPosition( 43.1500f, 31.5000f );
	_chain2.SetPosition( 45.1500f, 31.5000f );
	_circleFragile1.SetPosition( 41.7500f, 22.3500f );
	_circleFragile2.SetPosition( 46.2499f, 22.0000f );
	_coin1.SetPosition( 44.1000f, 25.6000f );
	_wallBox1.SetPosition( 41.8000f, 4.3500f );
	_wallBox2.SetPosition( 34.3000f, 4.4500f );
	_bomb1.SetPosition( 43.4000f, 2.2000f );
	_bomb2.SetPosition( 2.2000f, 20.3000f );
	_wallBox3.SetPosition( 12.0000f, 6.6500f );
	_woodBox1.SetPosition( 8.2500f, 7.4500f );
	_wallBox4.SetPosition( 3.8500f, 7.4000f );
	_wallBox5.SetPosition( 4.5500f, 6.7000f );
	_bomb3.SetPosition( 4.9500f, 16.6500f );
	_wallBox6.SetPosition( 12.6000f, 19.3000f );
	_wallBox7.SetPosition( 5.1500f, 19.3500f );
	_wallBox8.SetPosition( 42.3500f, 5.2000f );
	_bomb4.SetPosition( 23.9000f, 10.9500f );
	_wallBox9.SetPosition( 27.1500f, 14.5000f );
	_wallBox10.SetPosition( 26.6000f, 13.6500f );
	_wallBox11.SetPosition( 19.1500f, 13.7000f );
	_woodBox2.SetPosition( 22.8500f, 14.4500f );
	_piggyBank1.SetPosition( 10.1500f, 28.9000f );
	_woodBox3.SetPosition( 8.8500f, 20.1000f );
	_bomb5.SetPosition( 36.4500f, 2.2000f );
	_bomb6.SetPosition( 7.3500f, 4.7500f );
	_bomb7.SetPosition( 19.5000f, 11.1500f );
	_bomb8.SetPosition( 33.3500f, 27.2000f );
	_wallBox12.SetPosition( 36.7999f, 29.3000f );
	_woodBox4.SetPosition( 33.0500f, 30.0000f );
	_wallBox13.SetPosition( 29.3000f, 29.3000f );

	//Bombs Construction
	_bomb1.SetRange( 16.0000f );
	_bomb1.SetBombImpulse( 6.0f * 1500.0000f );
	_bomb2.SetRange( 16.0000f );
	_bomb2.SetBombImpulse( 6.0f * 800.0000f );
	_bomb3.SetRange( 16.0000f );
	_bomb3.SetBombImpulse( 6.0f * 900.0000f );
	_bomb4.SetRange( 16.0000f );
	_bomb4.SetBombImpulse( 6.0f * 875.0000f );
	_bomb5.SetRange( 16.0000f );
	_bomb5.SetBombImpulse( 6.0f * 1350.0000f );
	_bomb6.SetRange( 16.0000f );
	_bomb6.SetBombImpulse( 6.0f * 800.0000f );
	_bomb7.SetRange( 16.0000f );
	_bomb7.SetBombImpulse( 6.0f * 1250.0000f );
	_bomb8.SetRange( 16.0000f );
	_bomb8.SetBombImpulse( 6.0f * 875.0000f );

	ConstFinal();
	AdjustOldScreen();

	//Set chains hooks
	_chain1.HookOnChain( &_circleFragile1 );
	_chain2.HookOnChain( &_circleFragile2 );
}
//--------------------------------------------------------------------------
LevelPawel_15::LevelPawel_15( GameTypes::LevelEnum levelEnum, bool viewMode ) : Level( levelEnum, viewMode )
{
    _levelTag = eLevelTag_Bombs;
    
	// _bombStatic1
	_bombStatic1.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_bombStatic1.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_bombStatic1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bombStatic1.SetBodyType( b2_staticBody );

	// _bombStatic2
	_bombStatic2.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_bombStatic2.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_bombStatic2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bombStatic2.SetBodyType( b2_staticBody );

	// _bombStatic3
	_bombStatic3.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_bombStatic3.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_bombStatic3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bombStatic3.SetBodyType( b2_staticBody );

	// _bombStatic4
	_bombStatic4.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_bombStatic4.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_bombStatic4.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bombStatic4.SetBodyType( b2_staticBody );

	// _bombStatic5
	_bombStatic5.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_bombStatic5.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_bombStatic5.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bombStatic5.SetBodyType( b2_staticBody );

	// _bombStatic6
	_bombStatic6.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_bombStatic6.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_bombStatic6.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bombStatic6.SetBodyType( b2_staticBody );

	// _circleFragileStatic1
	_circleFragileStatic1.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_circleFragileStatic1.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_circleFragileStatic1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_circleFragileStatic1.SetBodyType( b2_staticBody );

	// _circleFragileStatic2
	_circleFragileStatic2.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_circleFragileStatic2.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_circleFragileStatic2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_circleFragileStatic2.SetBodyType( b2_staticBody );

	// _coin1
	_coin1.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_coin1.GetBody()->GetFixtureList()->SetFriction( 1.0000f );
	_coin1.GetBody()->GetFixtureList()->SetRestitution( 0.0000f );
	_coin1.SetBodyType( b2_dynamicBody );

	// _piggyBankStatic1
	_piggyBankStatic1.GetBody()->GetFixtureList()->SetDensity( 7.5000f );
	_piggyBankStatic1.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_piggyBankStatic1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_piggyBankStatic1.FlipX();
	_piggyBankStatic1.SetBodyType( b2_staticBody );

	// _wallBox1
	_wallBox1.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox1.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox1.SetBodyType( b2_staticBody );

	// _wallBox2
	_wallBox2.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox2.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox2.SetBodyType( b2_staticBody );

	// _wallBox3
	_wallBox3.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox3.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox3.SetBodyType( b2_staticBody );

	// _wallBox4
	_wallBox4.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox4.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox4.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox4.SetBodyType( b2_staticBody );

	// _wallBox5
	_wallBox5.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox5.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox5.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox5.SetBodyType( b2_staticBody );

	// _wallBox6
	_wallBox6.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox6.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox6.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox6.SetBodyType( b2_staticBody );

	// _wallBox7
	_wallBox7.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox7.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox7.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox7.SetBodyType( b2_staticBody );

	// _wallBox8
	_wallBox8.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox8.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox8.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox8.SetBodyType( b2_staticBody );

	// _wallBox9
	_wallBox9.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox9.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox9.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox9.SetBodyType( b2_staticBody );

	// _wallBox10
	_wallBox10.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox10.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox10.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox10.SetBodyType( b2_staticBody );

	// _woodBox1
	_woodBox1.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox1.GetBody()->GetFixtureList()->SetFriction( 0.8500f );
	_woodBox1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_woodBox1.SetBodyType( b2_dynamicBody );

	// _woodBox2
	_woodBox2.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox2.GetBody()->GetFixtureList()->SetFriction( 0.8500f );
	_woodBox2.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_woodBox2.SetBodyType( b2_dynamicBody );

	// _woodBox3
	_woodBox3.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox3.GetBody()->GetFixtureList()->SetFriction( 0.8500f );
	_woodBox3.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_woodBox3.SetBodyType( b2_dynamicBody );


	//Set blocks positions
	_bombStatic1.SetPosition( 38.8999f, 18.4000f );
	_bombStatic2.SetPosition( 14.4500f, 7.1000f );
	_bombStatic3.SetPosition( 47.3999f, 18.4000f );
	_bombStatic4.SetPosition( 16.5498f, 32.9499f );
	_bombStatic5.SetPosition( 8.0500f, 32.9499f );
	_bombStatic6.SetPosition( 70.4001f, 7.1000f );
	_circleFragileStatic1.SetPosition( 88.8506f, 32.9500f );
	_circleFragileStatic2.SetPosition( 79.7506f, 32.9500f );
	_coin1.SetPosition( 84.3030f, 41.9503f );
	_piggyBankStatic1.SetPosition( 8.0501f, 55.7000f );
	_wallBox1.SetPosition( 23.2000f, 53.3005f );
	_wallBox2.SetPosition( 23.2000f, 59.5004f );
	_wallBox3.SetPosition( 23.2003f, 10.5500f );
	_wallBox4.SetPosition( 9.2504f, 10.5500f );
	_wallBox5.SetPosition( 23.2003f, 38.6499f );
	_wallBox6.SetPosition( 9.2504f, 38.6499f );
	_wallBox7.SetPosition( 38.1001f, 24.1997f );
	_wallBox8.SetPosition( 52.0502f, 24.1997f );
	_wallBox9.SetPosition( 80.8007f, 10.5498f );
	_wallBox10.SetPosition( 66.8504f, 10.5498f );
	_woodBox1.SetPosition( 16.2505f, 12.5000f );
	_woodBox2.SetPosition( 16.3005f, 40.6000f );
	_woodBox3.SetPosition( 45.1004f, 26.2498f );

	//Bombs Construction
	_bombStatic1.SetRange( 20.6000f );
	_bombStatic1.SetBombImpulse( 7125.0000f );
	_bombStatic2.SetRange( 12.6000f );
	_bombStatic2.SetBombImpulse( 4650.0000f );
	_bombStatic3.SetRange( 16.6000f );
	_bombStatic3.SetBombImpulse( 5075.0000f );
	_bombStatic4.SetRange( 14.9000f );
	_bombStatic4.SetBombImpulse( 3075.0000f );
	_bombStatic5.SetRange( 16.5000f );
	_bombStatic5.SetBombImpulse( 4975.0000f );
	_bombStatic6.SetRange( 17.0000f );
	_bombStatic6.SetBombImpulse( 6200.0000f );
	_border.SetBorderSize( 5.0f );
	ConstFinal();
}
//--------------------------------------------------------------------------
LevelPawel_16::LevelPawel_16( GameTypes::LevelEnum levelEnum, bool viewMode ) : Level( levelEnum, viewMode )
{
	// _wallBox1
	_wallBox1.SetRotation( -90.0000f );
	_wallBox1.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox1.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox1.SetBodyType( b2_staticBody );

	// _wallBox2
	_wallBox2.SetRotation( -90.0000f );
	_wallBox2.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox2.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox2.SetBodyType( b2_staticBody );

	// _bomb1
	_bomb1.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_bomb1.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_bomb1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bomb1.SetBodyType( b2_dynamicBody );

	// _floor21
	_floor21.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_floor21.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_floor21.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_floor21.SetBodyType( b2_staticBody );

	// _coin1
	_coin1.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin1.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin1.SetBodyType( b2_dynamicBody );

	// _wallBox3
	_wallBox3.SetRotation( -90.0000f );
	_wallBox3.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox3.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox3.SetBodyType( b2_staticBody );

	// _wallBox4
	_wallBox4.SetRotation( -90.0000f );
	_wallBox4.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox4.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox4.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox4.SetBodyType( b2_staticBody );

	// _wallBox5
	_wallBox5.SetRotation( -10.0000f );
	_wallBox5.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox5.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox5.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox5.SetBodyType( b2_staticBody );

	// _wallBox6
	_wallBox6.SetRotation( 10.0000f );
	_wallBox6.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox6.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox6.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox6.SetBodyType( b2_staticBody );

	// _fragileBoxStatic1
	_fragileBoxStatic1.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBoxStatic1.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBoxStatic1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBoxStatic1.SetBodyType( b2_staticBody );

	// _fragileBoxStatic2
	_fragileBoxStatic2.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBoxStatic2.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBoxStatic2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBoxStatic2.SetBodyType( b2_staticBody );

	// _tennisBall1
	_tennisBall1.GetBody()->GetFixtureList()->SetDensity( 2.5000f );
	_tennisBall1.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_tennisBall1.GetBody()->GetFixtureList()->SetRestitution( 0.8000f );
	_tennisBall1.SetBodyType( b2_dynamicBody );

	// _tennisBall2
	_tennisBall2.GetBody()->GetFixtureList()->SetDensity( 2.5000f );
	_tennisBall2.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_tennisBall2.GetBody()->GetFixtureList()->SetRestitution( 0.8000f );
	_tennisBall2.SetBodyType( b2_dynamicBody );

	// _wallBox7
	_wallBox7.SetRotation( -90.0000f );
	_wallBox7.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox7.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox7.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox7.SetBodyType( b2_staticBody );

	// _wallBox8
	_wallBox8.SetRotation( -90.0000f );
	_wallBox8.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox8.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox8.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox8.SetBodyType( b2_staticBody );

	// _wallBox9
	_wallBox9.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox9.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox9.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox9.SetBodyType( b2_staticBody );

	// _wallBox10
	_wallBox10.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox10.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox10.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox10.SetBodyType( b2_staticBody );

	// _bomb2
	_bomb2.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_bomb2.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_bomb2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bomb2.SetBodyType( b2_dynamicBody );

	// _bomb3
	_bomb3.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_bomb3.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_bomb3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bomb3.SetBodyType( b2_dynamicBody );

	// _bomb4
	_bomb4.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_bomb4.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_bomb4.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bomb4.SetBodyType( b2_dynamicBody );

	// _wallBox11
	_wallBox11.SetRotation( 10.0000f );
	_wallBox11.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox11.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox11.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox11.SetBodyType( b2_staticBody );

	// _fragileBoxStatic3
	_fragileBoxStatic3.SetRotation( 0.3000f );
	_fragileBoxStatic3.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBoxStatic3.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBoxStatic3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBoxStatic3.SetBodyType( b2_staticBody );

	// _tennisBall3
	_tennisBall3.GetBody()->GetFixtureList()->SetDensity( 2.5000f );
	_tennisBall3.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_tennisBall3.GetBody()->GetFixtureList()->SetRestitution( 0.8000f );
	_tennisBall3.SetBodyType( b2_dynamicBody );

	// _piggyBank1
	_piggyBank1.GetBody()->GetFixtureList()->SetDensity( 7.5000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_piggyBank1.SetBodyType( b2_dynamicBody );

	// _wallBox12
	_wallBox12.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox12.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox12.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox12.SetBodyType( b2_staticBody );

	// _wallBox13
	_wallBox13.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox13.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox13.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox13.SetBodyType( b2_staticBody );

	// _woodBox1
	_woodBox1.SetRotation( 0.6000f );
	_woodBox1.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox1.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_woodBox1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_woodBox1.SetBodyType( b2_dynamicBody );

	// _fragileBoxStatic4
	_fragileBoxStatic4.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBoxStatic4.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBoxStatic4.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBoxStatic4.SetBodyType( b2_staticBody );

	// _bomb5
	_bomb5.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_bomb5.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_bomb5.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bomb5.SetBodyType( b2_dynamicBody );

	// _wallBox14
	_wallBox14.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox14.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox14.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox14.SetBodyType( b2_staticBody );

	// _wallBox15
	_wallBox15.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox15.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox15.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox15.SetBodyType( b2_staticBody );

	// _wallBox16
	_wallBox16.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox16.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox16.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox16.SetBodyType( b2_staticBody );

	// _wallBox17
	_wallBox17.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox17.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox17.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox17.SetBodyType( b2_staticBody );


	//Set blocks positions
	_wallBox1.SetPosition( 47.0500f, 2.9000f );
	_wallBox2.SetPosition( 41.6000f, 2.9000f );
	_bomb1.SetPosition( 44.3500f, 3.4000f );
	_floor21.SetPosition( 24.0000f, 0.4500f );
	_coin1.SetPosition( 42.9500f, 7.5500f );
	_wallBox3.SetPosition( 35.2000f, 5.6500f );
	_wallBox4.SetPosition( 29.8000f, 5.4000f );
	_wallBox5.SetPosition( 10.0000f, 22.2000f );
	_wallBox6.SetPosition( 39.6500f, 24.7500f );
	_fragileBoxStatic1.SetPosition( 7.8500f, 25.6000f );
	_fragileBoxStatic2.SetPosition( 40.7000f, 28.3000f );
	_tennisBall1.SetPosition( 3.4000f, 25.8000f );
	_tennisBall2.SetPosition( 45.1000f, 28.1500f );
	_wallBox7.SetPosition( 23.4500f, 3.5500f );
	_wallBox8.SetPosition( 18.2000f, 3.9000f );
	_wallBox9.SetPosition( 4.0000f, 6.3500f );
	_wallBox10.SetPosition( 9.5000f, 4.7000f );
	_bomb2.SetPosition( 7.2500f, 8.4500f );
	_bomb3.SetPosition( 6.2500f, 3.0500f );
	_bomb4.SetPosition( 44.8500f, 22.9500f );
	_wallBox11.SetPosition( 47.4000f, 20.8000f );
	_fragileBoxStatic3.SetPosition( 36.1500f, 20.9000f );
	_tennisBall3.SetPosition( 40.6000f, 22.1000f );
	_piggyBank1.SetPosition( 16.3000f, 29.1500f );
	_wallBox12.SetPosition( 17.7000f, 26.1500f );
	_wallBox13.SetPosition( 14.7500f, 26.1500f );
	_woodBox1.SetPosition( 24.7500f, 31.6000f );
	_fragileBoxStatic4.SetPosition( 23.5000f, 28.7500f );
	_bomb5.SetPosition( 11.0000f, 29.9500f );
	_wallBox14.SetPosition( 8.9500f, 28.3000f );
	_wallBox15.SetPosition( 13.0500f, 28.4000f );
	_wallBox16.SetPosition( 19.8500f, 25.3000f );
	_wallBox17.SetPosition( 24.3000f, 25.2000f );

	//Bombs Construction
	_bomb1.SetRange( 2.0f * 8.0000f );
	_bomb1.SetBombImpulse( 6.0f * 1000.0000f );
	_bomb2.SetRange( 2.0f * 8.0000f );
	_bomb2.SetBombImpulse( 6.0f * 1500.0000f );
	_bomb3.SetRange( 2.0f * 10.0000f );
	_bomb3.SetBombImpulse( 6.0f * 2700.0000f );
	_bomb4.SetRange( 2.0f * 8.0000f );
	_bomb4.SetBombImpulse( 6.0f * 1300.0000f );
	_bomb5.SetRange( 2.0f * 10.0000f );
	_bomb5.SetBombImpulse( 6.0f * 462.5000f );

	ConstFinal();
	AdjustOldScreen();
}
//--------------------------------------------------------------------------
