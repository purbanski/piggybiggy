#include "LevelTraps.h"

//--------------------------------------------------------------------------
LevelTraps_1::LevelTraps_1( GameTypes::LevelEnum levelEnum, bool viewMode ) : Level( levelEnum, viewMode )
{
	_coinCount = 4;
    _levelTag = eLevelTag_Thief;
    
	// _thief1
	_thief1.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_thief1.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_thief1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_thief1.GetBody()->SetAngularDamping( 0.8f );
	_thief1.GetBody()->SetLinearDamping( 0.8f );
	_thief1.SetBodyType( b2_dynamicBody );

	// _trap1
	_trap1.SetRotation( 50.0000f );
	_trap1.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_trap1.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_trap1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_trap1.SetBodyType( b2_dynamicBody );

	// _trap2
	_trap2.SetRotation( 50.0000f );
	_trap2.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_trap2.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_trap2.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_trap2.SetBodyType( b2_dynamicBody );

	// _wallBox1
	_wallBox1.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox1.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox1.SetBodyType( b2_staticBody );

	// _wallBox2
	_wallBox2.SetRotation( -10.0000f );
	_wallBox2.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox2.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox2.SetBodyType( b2_staticBody );

	// _fragileBox1
	_fragileBox1.SetRotation( -10.0000f );
	_fragileBox1.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox1.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox1.SetBodyType( b2_dynamicBody );

	// _fragileBox2
	_fragileBox2.SetRotation( -10.0000f );
	_fragileBox2.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox2.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox2.SetBodyType( b2_dynamicBody );

	// _fragileBox3
	_fragileBox3.SetRotation( -10.0000f );
	_fragileBox3.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox3.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox3.SetBodyType( b2_dynamicBody );

	// _fragileBox4
	_fragileBox4.SetRotation( -10.0000f );
	_fragileBox4.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox4.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox4.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox4.SetBodyType( b2_dynamicBody );

	// _wallBox6
	_wallBox6.SetRotation( -10.0000f );
	_wallBox6.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox6.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox6.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox6.SetBodyType( b2_staticBody );

	// _wallBox7
	_wallBox7.SetRotation( -10.0000f );
	_wallBox7.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox7.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox7.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox7.SetBodyType( b2_staticBody );

	// _wallBox8
	_wallBox8.SetRotation( -10.0000f );
	_wallBox8.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox8.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox8.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox8.SetBodyType( b2_staticBody );

	// _wallBox9
	_wallBox9.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox9.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox9.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox9.SetBodyType( b2_staticBody );

	// _coin1
	_coin1.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin1.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin1.SetBodyType( b2_dynamicBody );

	// _coin2
	_coin2.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin2.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin2.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin2.SetBodyType( b2_dynamicBody );

	// _coin3
	_coin3.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin3.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin3.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin3.SetBodyType( b2_dynamicBody );

	// _piggyBank1
	_piggyBank1.GetBody()->GetFixtureList()->SetDensity( 7.5000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_piggyBank1.SetBodyType( b2_dynamicBody );
	_piggyBank1.FlipX();

	// _coin4
	_coin4.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin4.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin4.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin4.SetBodyType( b2_dynamicBody );

	// _wallBox10
	_wallBox10.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox10.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox10.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox10.SetBodyType( b2_staticBody );

	// _wallBox11
	_wallBox11.SetRotation( -2.0000f );
	_wallBox11.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox11.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox11.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox11.SetBodyType( b2_staticBody );


	//Set blocks positions
	_thief1.SetPosition( 71.8502f, 23.5999f );
	_trap1.SetPosition( 41.2005f, 40.1999f );
	_trap2.SetPosition( 57.8007f, 24.1000f );
	_wallBox1.SetPosition( 87.7505f, 8.0499f );
	_wallBox2.SetPosition( 13.8000f, 52.9499f );
	_fragileBox1.SetPosition( 17.4500f, 19.3992f );
	_fragileBox2.SetPosition( 17.4500f, 32.3991f );
	_fragileBox3.SetPosition( 17.4500f, 45.3991f );
	_fragileBox4.SetPosition( 17.4500f, 58.3991f );
	_wallBox6.SetPosition( 22.2001f, 38.4499f );
	_wallBox7.SetPosition( 30.5001f, 24.0000f );
	_wallBox8.SetPosition( 38.9501f, 9.5500f );
	_wallBox9.SetPosition( 62.8501f, 33.5999f );
	_coin1.SetPosition( 8.0001f, 21.0500f );
	_coin2.SetPosition( 8.0001f, 34.0499f );
	_coin3.SetPosition( 8.0001f, 60.0500f );
	_piggyBank1.SetPosition( 8.0005f, 47.0499f );
	_coin4.SetPosition( 71.8503f, 38.9499f );
	_wallBox10.SetPosition( 71.8500f, 17.4000f );
	_wallBox11.SetPosition( 80.5501f, 3.3000f );

	ConstFinal();
}
//--------------------------------------------------------------------------
LevelTraps_2::LevelTraps_2( GameTypes::LevelEnum levelEnum, bool viewMode ) : Level( levelEnum, viewMode )
{
    _levelTag = eLevelTag_Hacker;
	_coinCount = 8;

	// _coinSilver1
	_coinSilver1.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coinSilver1.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coinSilver1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coinSilver1.SetBodyType( b2_dynamicBody );

	// _tFlap1
	_tFlap1.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_tFlap1.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_tFlap1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_tFlap1.SetBodyType( b2_dynamicBody );

	// _tFlap2
	_tFlap2.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_tFlap2.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_tFlap2.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_tFlap2.SetBodyType( b2_dynamicBody );

	// _tFlap3
	_tFlap3.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_tFlap3.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_tFlap3.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_tFlap3.FlipX();
	_tFlap3.SetBodyType( b2_dynamicBody );

	// _tFlap4
	_tFlap4.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_tFlap4.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_tFlap4.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_tFlap4.FlipX();
	_tFlap4.SetBodyType( b2_dynamicBody );

	// _wallBox1
	_wallBox1.SetRotation( -2.0000f );
	_wallBox1.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox1.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox1.SetBodyType( b2_staticBody );

	// _wallBox2
	_wallBox2.SetRotation( 7.0000f );
	_wallBox2.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox2.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox2.SetBodyType( b2_staticBody );

	// _wallBox3
	_wallBox3.SetRotation( -7.0000f );
	_wallBox3.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox3.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox3.SetBodyType( b2_staticBody );

	// _wallBox4
	_wallBox4.SetRotation( 7.0000f );
	_wallBox4.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox4.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox4.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox4.SetBodyType( b2_staticBody );

	// _wallBox5
	_wallBox5.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox5.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox5.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox5.SetBodyType( b2_staticBody );

	// _wallBox6
	_wallBox6.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox6.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox6.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox6.SetBodyType( b2_staticBody );

	// _wallBox7
	_wallBox7.SetRotation( 7.0000f );
	_wallBox7.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox7.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox7.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox7.SetBodyType( b2_staticBody );

	// _wallBox8
	_wallBox8.SetRotation( -7.0000f );
	_wallBox8.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox8.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox8.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox8.SetBodyType( b2_staticBody );

	// _motherPiggyStatic1
	_motherPiggyStatic1.GetBody()->GetFixtureList()->SetDensity( 7.5000f );
	_motherPiggyStatic1.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_motherPiggyStatic1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_motherPiggyStatic1.SetBodyType( b2_staticBody );
	_motherPiggyStatic1.FlipX();

	
	// _cashCowStatic1
	_cashCowStatic1.GetBody()->GetFixtureList()->SetDensity( 7.5000f );
	_cashCowStatic1.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_cashCowStatic1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_cashCowStatic1.SetBodyType( b2_staticBody );

	// _prison1
	_prison1.GetBody()->GetFixtureList()->SetDensity( 7.5000f );
	_prison1.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_prison1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_prison1.SetBodyType( b2_staticBody );

	//Set blocks positions
	_coinSilver1.SetPosition( 31.8000f, 8.1500f );
	_tFlap1.SetPosition( 14.3000f, 14.9500f );
	_tFlap2.SetPosition( 45.9000f, 25.7499f );
	_tFlap3.SetPosition( 80.7000f, 30.2499f );
	_tFlap4.SetPosition( 62.6000f, 39.1499f );
	_wallBox1.SetPosition( 35.3001f, 2.8000f );
	_wallBox2.SetPosition( 58.1000f, 27.0499f );
	_wallBox3.SetPosition( 22.2001f, 3.7000f );
	_wallBox4.SetPosition( 26.4000f, 15.9500f );
	_wallBox5.SetPosition( 46.8500f, 13.8500f );
	_wallBox6.SetPosition( 40.3000f, 7.5500f );
	_wallBox7.SetPosition( 65.1999f, 10.8000f );
	_wallBox8.SetPosition( 30.7000f, 42.3500f );
	_motherPiggyStatic1.SetPosition( 12.7000f, 57.0000f );
	_cashCowStatic1.SetPosition( 45.6000f, 57.1000f );
	_prison1.SetPosition( 28.4000f, 56.7000f );
	
	_motherPiggyStatic1.SetKidsFlipX( puBlock::eAxisFlipped );
	_prison1.SetKidsFlipX( puBlock::eAxisFlipped );

	float x = 12.5f; 
	float y = 57.0f;
	float xDelta = 16.0f; 
	//Set blocks positions
	//
	_motherPiggyStatic1.SetPosition( x, y );
	_prison1.SetPosition( x + xDelta, y );
	_cashCowStatic1.SetPosition( x + 2.0f * xDelta, y );
	//
	ConstFinal();
}
//--------------------------------------------------------------------------
LevelTraps_3::LevelTraps_3( GameTypes::LevelEnum levelEnum, bool viewMode ) : Level( levelEnum, viewMode )
{
	b2Filter filter;
	_coinCount = 3;
    _levelTag = eLevelTag_Frigle;

	// _bomb1
	_bomb1.SetRotation( 6.3000f );
	_bomb1.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_bomb1.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_bomb1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bomb1.GetBody()->SetLinearDamping(0.1000f );
	_bomb1.SetBodyType( b2_dynamicBody );

	// _coin1
	_coin1.SetRotation( 6.3000f );
	_coin1.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin1.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin1.SetBodyType( b2_dynamicBody );

	// _coin2
	_coin2.SetRotation( 6.3000f );
	_coin2.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin2.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin2.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin2.SetBodyType( b2_dynamicBody );

	// _coin3
	_coin3.SetRotation( 6.3000f );
	_coin3.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin3.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin3.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin3.SetBodyType( b2_dynamicBody );

	// _fragileBox1
	_fragileBox1.SetRotation( 12.3000f );
	_fragileBox1.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox1.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox1.SetBodyType( b2_dynamicBody );

	// _fragileBox2
	_fragileBox2.SetRotation( 12.3000f );
	_fragileBox2.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox2.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox2.SetBodyType( b2_dynamicBody );

	// _fragileBox3
	_fragileBox3.SetRotation( 12.3000f );
	_fragileBox3.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox3.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox3.SetBodyType( b2_dynamicBody );

	// _fragileBox4
	_fragileBox4.SetRotation( 12.3000f );
	_fragileBox4.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox4.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox4.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox4.SetBodyType( b2_dynamicBody );

	// _fragileBox5
	_fragileBox5.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox5.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox5.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox5.SetBodyType( b2_dynamicBody );

	// _piggyBank1
	_piggyBank1.FlipX();
	_piggyBank1.GetBody()->GetFixtureList()->SetDensity( 7.5000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );

	// _screw1
	_screw1.GetBody()->GetFixtureList()->SetDensity( 0.1000f );
	_screw1.GetBody()->GetFixtureList()->SetFriction( 0.1000f );
	_screw1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	filter.groupIndex = -1;
	filter.categoryBits = 0;
	filter.maskBits = 65535;
	_screw1.GetBody()->GetFixtureList()->SetFilterData( filter );
	_screw1.SetBodyType( b2_staticBody );

	// _screw2
	_screw2.GetBody()->GetFixtureList()->SetDensity( 0.1000f );
	_screw2.GetBody()->GetFixtureList()->SetFriction( 0.1000f );
	_screw2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	filter.groupIndex = -1;
	filter.categoryBits = 0;
	filter.maskBits = 65535;
	_screw2.GetBody()->GetFixtureList()->SetFilterData( filter );
	_screw2.SetBodyType( b2_staticBody );

	// _screw3
	_screw3.SetRotation( -90.0000f );
	_screw3.GetBody()->GetFixtureList()->SetDensity( 0.1000f );
	_screw3.GetBody()->GetFixtureList()->SetFriction( 0.1000f );
	_screw3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	filter.groupIndex = -1;
	filter.categoryBits = 0;
	filter.maskBits = 65535;
	_screw3.GetBody()->GetFixtureList()->SetFilterData( filter );
	_screw3.SetBodyType( b2_staticBody );

	// _screw4
	_screw4.SetRotation( -90.0000f );
	_screw4.GetBody()->GetFixtureList()->SetDensity( 0.1000f );
	_screw4.GetBody()->GetFixtureList()->SetFriction( 0.1000f );
	_screw4.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	filter.groupIndex = -1;
	filter.categoryBits = 0;
	filter.maskBits = 65535;
	_screw4.GetBody()->GetFixtureList()->SetFilterData( filter );
	_screw4.SetBodyType( b2_staticBody );

	// _thief1
	_thief1.SetRotation( 6.3000f );
	_thief1.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_thief1.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_thief1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_thief1.SetBodyType( b2_dynamicBody );

	// _wallBox1
	_wallBox1.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox1.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	filter.groupIndex = -1;
	filter.categoryBits = 1;
	filter.maskBits = 65535;
	_wallBox1.GetBody()->GetFixtureList()->SetFilterData( filter );
	_wallBox1.SetBodyType( b2_staticBody );

	// _wallBox2
	_wallBox2.SetRotation( 140.4601f );
	_wallBox2.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox2.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	filter.groupIndex = -1;
	filter.categoryBits = 1;
	filter.maskBits = 65535;
	_wallBox2.GetBody()->GetFixtureList()->SetFilterData( filter );
	_wallBox2.SetBodyType( b2_staticBody );

	// _wallBox3
	_wallBox3.SetRotation( 178.6200f );
	_wallBox3.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox3.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	filter.groupIndex = -1;
	filter.categoryBits = 1;
	filter.maskBits = 65535;
	_wallBox3.GetBody()->GetFixtureList()->SetFilterData( filter );
	_wallBox3.SetBodyType( b2_staticBody );

	// _wallBox4
	_wallBox4.SetRotation( 216.7801f );
	_wallBox4.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox4.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox4.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	filter.groupIndex = -1;
	filter.categoryBits = 1;
	filter.maskBits = 65535;
	_wallBox4.GetBody()->GetFixtureList()->SetFilterData( filter );
	_wallBox4.SetBodyType( b2_staticBody );

	// _wallBox11
	_wallBox11.SetRotation( 13.3000f );
	_wallBox11.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox11.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox11.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox11.SetBodyType( b2_staticBody );

	// _wallBox12
	_wallBox12.SetRotation( 13.3000f );
	_wallBox12.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox12.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox12.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox12.SetBodyType( b2_staticBody );

	// _wallBox13
	_wallBox13.SetRotation( 13.0000f );
	_wallBox13.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox13.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox13.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox13.SetBodyType( b2_staticBody );

	// _woodBox1
	_woodBox1.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox1.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_woodBox1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	filter.groupIndex = -1;
	filter.categoryBits = 1;
	filter.maskBits = 65535;
	_woodBox1.GetBody()->GetFixtureList()->SetFilterData( filter );
	_woodBox1.SetBodyType( b2_dynamicBody );

	// _woodBox2
	_woodBox2.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox2.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_woodBox2.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	filter.groupIndex = -1;
	filter.categoryBits = 1;
	filter.maskBits = 65535;
	_woodBox2.GetBody()->GetFixtureList()->SetFilterData( filter );
	_woodBox2.SetBodyType( b2_dynamicBody );

	// _woodBox3
	_woodBox3.SetRotation( -90.0000f );
	_woodBox3.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox3.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_woodBox3.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	filter.groupIndex = -1;
	filter.categoryBits = 1;
	filter.maskBits = 65535;
	_woodBox3.GetBody()->GetFixtureList()->SetFilterData( filter );
	_woodBox3.SetBodyType( b2_dynamicBody );

	// _woodBox4
	_woodBox4.SetRotation( -90.0000f );
	_woodBox4.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox4.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_woodBox4.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	filter.groupIndex = -1;
	filter.categoryBits = 1;
	filter.maskBits = 65535;
	_woodBox4.GetBody()->GetFixtureList()->SetFilterData( filter );
	_woodBox4.SetBodyType( b2_dynamicBody );


	//Set blocks positions
	_bomb1.SetPosition( 67.6000f, 48.4000f );
	_coin1.SetPosition( 63.3642f, 59.5182f );
	_coin2.SetPosition( 89.0662f, 41.3470f );
	_coin3.SetPosition( 52.6350f, 32.6627f );
	_fragileBox1.SetPosition( 53.1502f, 57.3323f );
	_fragileBox2.SetPosition( 57.2000f, 45.7500f );
	_fragileBox3.SetPosition( 61.5484f, 34.6321f );
	_fragileBox4.SetPosition( 80.3382f, 39.4228f );
	_fragileBox5.SetPosition( 44.0000f, 32.0f );
	_piggyBank1.SetPosition( 9.2500f, 30.50f );
	_screw1.SetPosition( 33.5500f, 25.3000f );
	_screw2.SetPosition( 19.1500f, 25.3000f );
	_screw3.SetPosition( 20.4252f, 12.2252f );
	_screw4.SetPosition( 32.2753f, 12.0251f );
	_thief1.SetPosition( 71.3613f, 37.1288f );
	_wallBox1.SetPosition( 40.9899f, 25.3429f );
	_wallBox11.SetPosition( 58.8331f, 52.3367f );
	_wallBox12.SetPosition( 64.5f, 41.600f );
	_wallBox13.SetPosition( 70.3000f, 30.6000f );

	_wallBox2.SetPosition( 22.5516f, 3.9275f );
	_wallBox3.SetPosition( 26.2268f, 2.5563f );
	_wallBox4.SetPosition( 29.9638f, 3.7489f );

	_woodBox1.SetPosition( 30.4002f, 25.3000f );
	_woodBox2.SetPosition( 22.3006f, 25.3000f );
	_woodBox3.SetPosition( 20.4253f, 9.0746f );
	_woodBox4.SetPosition( 32.2753f, 8.8745f );

	//Bombs Construction
	_bomb1.SetRange( 16.0000f );
	_bomb1.SetBombImpulse( 6000.0000f );

	ConstFinal();
	CreateJoints();
}
//--------------------------------------------------------------------------
LevelTraps_3::~LevelTraps_3()
{
	DestroyJoints();
}
//--------------------------------------------------------------------------
void LevelTraps_3::CreateJoints()
{
	b2RevoluteJointDef jointRevDef1;
	jointRevDef1.collideConnected = false;
	jointRevDef1.Initialize( _screw1.GetBody(), _woodBox1.GetBody(), _screw1.GetPosition());
	_jointRev1 = (b2RevoluteJoint *) _world.CreateJoint( &jointRevDef1 );

	_jointRev1->EnableMotor( true );
	_jointRev1->SetMotorSpeed( 0.0000f );
	_jointRev1->SetMaxMotorTorque( 2900.0000f );

	b2RevoluteJointDef jointRevDef2;
	jointRevDef2.collideConnected = false;
	jointRevDef2.Initialize( _screw2.GetBody(), _woodBox2.GetBody(), _screw2.GetPosition() );
	_jointRev2 = (b2RevoluteJoint *) _world.CreateJoint( &jointRevDef2 );

	_jointRev2->SetLimits( -1.5500f, -0.0500f );
	_jointRev2->EnableLimit( true );
	_jointRev2->EnableMotor( false );
	_jointRev2->SetMotorSpeed( 0.0000f );
	_jointRev2->SetMaxMotorTorque( 3100.0000f );

	b2RevoluteJointDef jointRevDef3;
	jointRevDef3.collideConnected = false;
	jointRevDef3.Initialize( _screw3.GetBody(), _woodBox3.GetBody(), _screw3.GetPosition() );
	_jointRev3 = (b2RevoluteJoint *) _world.CreateJoint( &jointRevDef3 );

	_jointRev3->EnableMotor( true );
	_jointRev3->SetMotorSpeed( 0.0000f );
	_jointRev3->SetMaxMotorTorque( 1500.0000f );

	b2RevoluteJointDef jointRevDef4;
	jointRevDef4.collideConnected = false;
	jointRevDef4.Initialize( _screw4.GetBody(), _woodBox4.GetBody(), _screw4.GetPosition() );
	_jointRev4 = (b2RevoluteJoint *) _world.CreateJoint( &jointRevDef4 );

	_jointRev4->EnableMotor( true );
	_jointRev4->SetMotorSpeed( 0.0000f );
	_jointRev4->SetMaxMotorTorque( 1500.0000f );

	b2GearJointDef jointGearDef1;
	jointGearDef1.bodyA = _woodBox1.GetBody();
	jointGearDef1.bodyB = _woodBox2.GetBody();
	jointGearDef1.joint1 = _jointRev1;
	jointGearDef1.joint2 = _jointRev2;
	jointGearDef1.ratio = 1;
	_jointGear1 = (b2GearJoint *) _world.CreateJoint( &jointGearDef1 );
	
	b2GearJointDef jointGearDef2;
	jointGearDef2.bodyA = _woodBox1.GetBody();
	jointGearDef2.bodyB = _woodBox4.GetBody();
	jointGearDef2.joint1 = _jointRev1;
	jointGearDef2.joint2 = _jointRev4;
	jointGearDef2.ratio = 1;
	_jointGear2 = (b2GearJoint *) _world.CreateJoint( &jointGearDef2 );
	
	b2GearJointDef jointGearDef3;
	jointGearDef3.bodyA = _woodBox3.GetBody();
	jointGearDef3.bodyB = _woodBox2.GetBody();
	jointGearDef3.joint1 = _jointRev3;
	jointGearDef3.joint2 = _jointRev2;
	jointGearDef3.ratio = 1;
	_jointGear3 = (b2GearJoint *) _world.CreateJoint( &jointGearDef3 );
	
	b2GearJointDef jointGearDef4;
	jointGearDef4.bodyA = _woodBox4.GetBody();
	jointGearDef4.bodyB = _woodBox3.GetBody();
	jointGearDef4.joint1 = _jointRev4;
	jointGearDef4.joint2 = _jointRev3;
	jointGearDef4.ratio = 1;
	_jointGear4 = (b2GearJoint *) _world.CreateJoint( &jointGearDef4 );

}
//--------------------------------------------------------------------------
void LevelTraps_3::DestroyJoints()
{
	_world.DestroyJoint( _jointGear1 );
	_world.DestroyJoint( _jointGear2 );
	_world.DestroyJoint( _jointGear3 );
	_world.DestroyJoint( _jointGear4 );

	_world.DestroyJoint( _jointRev1 );
	_world.DestroyJoint( _jointRev2 );
	_world.DestroyJoint( _jointRev3 );
	_world.DestroyJoint( _jointRev4 );
}
//--------------------------------------------------------------------------
