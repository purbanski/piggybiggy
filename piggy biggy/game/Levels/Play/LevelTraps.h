#ifndef __LEVELTRAPS_H_
#define __LEVELTRAPS_H_

#include <Box2D/Box2D.h>
#include "Levels/Level.h"
#include "Blocks/puBox.h"
#include "Blocks/puCircle.h"
#include "Blocks/puTrap.h"

//-----------------------------------------------------------------------------------
class LevelTraps_1 : public Level
{
public:
	LevelTraps_1( GameTypes::LevelEnum levelEnum, bool viewMode );

private:
	puCoin< 40 >	_coin1;
	puCoin< 40 >	_coin2;
	puCoin< 40 >	_coin3;
	puCoin< 40 >	_coin4;
	puFragileBox<96,96,eBlockScaled>	_fragileBox1;
	puFragileBox<96,96,eBlockScaled>	_fragileBox2;
	puFragileBox<96,96,eBlockScaled>	_fragileBox3;
	puFragileBox<96,96,eBlockScaled>	_fragileBox4;
	puPiggyBank< 44 >	_piggyBank1;
	puThief< 44 >	_thief1;
	puTrap	_trap1;
	puTrap	_trap2;
	puWallBox<160,20,eBlockScaled>	_wallBox11;
	puWallBox<160,20,eBlockScaled>	_wallBox2;
	puWallBox<20,20,eBlockScaled>	_wallBox1;
	puWallBox<250,20,eBlockScaled>	_wallBox9;
	puWallBox<330,20,eBlockScaled>	_wallBox6;
	puWallBox<500,20,eBlockScaled>	_wallBox7;
	puWallBox<670,20,eBlockScaled>	_wallBox8;
	puWallBox<70,20,eBlockScaled>	_wallBox10;
};
//-----------------------------------------------------------------------------------
class LevelTraps_2: public Level
{
public:
	LevelTraps_2( GameTypes::LevelEnum levelEnum, bool viewMode );

private:
	puCashCowStatic< 55, 28, 6 > 	_cashCowStatic1;
	puCoinSilver< 28 >	_coinSilver1;
	puMotherPiggyStatic< 55, 28, 2 > 	_motherPiggyStatic1;
	puPrison< 55, 28, 3 > 	_prison1;
	puTFlap	_tFlap1;
	puTFlap	_tFlap2;
	puTFlap	_tFlap3;
	puTFlap	_tFlap4;
	puWallBox<120,15,eBlockScaled>	_wallBox1;
	puWallBox<120,15,eBlockScaled>	_wallBox2;
	puWallBox<120,15,eBlockScaled>	_wallBox3;
	puWallBox<130,15,eBlockScaled>	_wallBox4;
	puWallBox<20,20,eBlockScaled>	_wallBox5;
	puWallBox<20,20,eBlockScaled>	_wallBox6;
	puWallBox<380,20,eBlockScaled>	_wallBox7;
	puWallBox<510,15,eBlockScaled>	_wallBox8;
};
//-----------------------------------------------------------------------------------
class LevelTraps_3: public Level
{
public:
	LevelTraps_3( GameTypes::LevelEnum levelEnum, bool viewMode );
	~LevelTraps_3();

private:
	void CreateJoints();
	void DestroyJoints();

private:
	puBomb< 44 >	_bomb1;
	puCoin< 40 >	_coin1;
	puCoin< 40 >	_coin2;
	puCoin< 40 >	_coin3;
	puFragileBox<96,96,eBlockScaled>	_fragileBox1;
	puFragileBox<96,96,eBlockScaled>	_fragileBox2;
	puFragileBox<96,96,eBlockScaled>	_fragileBox3;
	puFragileBox<96,96,eBlockScaled>	_fragileBox4;
	puFragileBox<96,96,eBlockScaled>	_fragileBox5;
	puPiggyBankStatic< 60 >	_piggyBank1;
	puScrew< 10 >	_screw1;
	puScrew< 10 >	_screw2;
	puScrew< 10 >	_screw3;
	puScrew< 10 >	_screw4;
	puThief< 44 >	_thief1;
	puWallBox<120,20,eBlockScaled>	_wallBox1;
	puWallBox<230,20,eBlockScaled>	_wallBox11;
	puWallBox<35,20,eBlockScaled>	_wallBox2;
	puWallBox<35,20,eBlockScaled>	_wallBox3;
	puWallBox<35,20,eBlockScaled>	_wallBox4;
	puWallBox<330,20,eBlockScaled>	_wallBox12;
	puWallBox<480,20,eBlockScaled>	_wallBox13;
	puWood2Box<80,20,eBlockScaled>	_woodBox1;
	puWood2Box<80,20,eBlockScaled>	_woodBox2;
	puWood2Box<80,20,eBlockScaled>	_woodBox3;
	puWood2Box<80,20,eBlockScaled>	_woodBox4;


	b2RevoluteJoint	*_jointRev1;
	b2RevoluteJoint	*_jointRev2;
	b2RevoluteJoint	*_jointRev3;
	b2RevoluteJoint	*_jointRev4;
	b2GearJoint	*_jointGear1;
	b2GearJoint	*_jointGear2;
	b2GearJoint	*_jointGear3;
	b2GearJoint	*_jointGear4;
};
//-----------------------------------------------------------------------------------

#endif
