#include "LevelTomekW.h"
//--------------------------------------------------------------------------
LevelTomekW_Mapa3::LevelTomekW_Mapa3( GameTypes::LevelEnum levelEnum, bool viewMode ) : Level( levelEnum, viewMode )
{
    _levelTag = eLevelTag_Frigle;
    
	// _coin1
	_coin1.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin1.GetBody()->GetFixtureList()->SetFriction( 1.0000f );
	_coin1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin1.SetBodyType( b2_dynamicBody );

	// _fragileBox1
	_fragileBox1.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox1.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox1.SetBodyType( b2_dynamicBody );

	// _fragileBox2
	_fragileBox2.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox2.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox2.SetBodyType( b2_dynamicBody );

	// _fragileBox3
	_fragileBox3.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox3.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox3.SetBodyType( b2_dynamicBody );

	// _fragileBox4
	_fragileBox4.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox4.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox4.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox4.SetBodyType( b2_dynamicBody );

	// _fragileBox5
	_fragileBox5.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox5.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox5.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox5.SetBodyType( b2_dynamicBody );

	// _fragileBox6
	_fragileBox6.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_fragileBox6.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_fragileBox6.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_fragileBox6.SetBodyType( b2_dynamicBody );

	// _fragileBox7
	_fragileBox7.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_fragileBox7.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_fragileBox7.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_fragileBox7.SetBodyType( b2_dynamicBody );

	// _fragileBox8
	_fragileBox8.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_fragileBox8.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_fragileBox8.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_fragileBox8.SetBodyType( b2_dynamicBody );

	// _piggyBankStatic1
	_piggyBankStatic1.GetBody()->GetFixtureList()->SetDensity( 7.5000f );
	_piggyBankStatic1.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_piggyBankStatic1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_piggyBankStatic1.FlipX();
	_piggyBankStatic1.SetBodyType( b2_staticBody );

	// _wallBox1
	_wallBox1.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox1.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox1.SetBodyType( b2_staticBody );

	// _woodBox1
	_woodBox1.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox1.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_woodBox1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_woodBox1.SetBodyType( b2_dynamicBody );


	//Set blocks positions
	_coin1.SetPosition( 68.6516f, 50.1001f );
	_fragileBox1.SetPosition( 58.8511f, 39.1001f );
	_fragileBox2.SetPosition( 54.3511f, 19.8001f );
	_fragileBox3.SetPosition( 83.1515f, 19.8001f );
	_fragileBox4.SetPosition( 58.8511f, 29.5002f );
	_fragileBox5.SetPosition( 78.1514f, 29.5002f );
	_fragileBox6.SetPosition( 73.5516f, 19.8001f );
	_fragileBox7.SetPosition( 63.9511f, 19.8001f );
	_fragileBox8.SetPosition( 78.1514f, 39.1001f );
	_piggyBankStatic1.SetPosition( 14.3499f, 17.3500f );
	_wallBox1.SetPosition( 68.7507f, 13.8000f );
	_woodBox1.SetPosition( 68.4007f, 44.9501f );

	ConstFinal();
}
//--------------------------------------------------------------------------
LevelTomekW_Prosty4::LevelTomekW_Prosty4( GameTypes::LevelEnum levelEnum, bool viewMode ) : Level( levelEnum, viewMode )
{
	// _bomb1
	_bomb1.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_bomb1.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_bomb1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bomb1.SetBodyType( b2_dynamicBody );

	// _bomb2
	_bomb2.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_bomb2.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_bomb2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bomb2.SetBodyType( b2_dynamicBody );

	// _borderAllClose1
	_borderAllClose1.SetRotation( -90.0000f );
	_borderAllClose1.GetBody()->GetFixtureList()->SetDensity( 5.0000f );
	_borderAllClose1.GetBody()->GetFixtureList()->SetFriction( 0.1000f );
	_borderAllClose1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_borderAllClose1.SetBodyType( b2_staticBody );

	// _coin1
	_coin1.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin1.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin1.SetBodyType( b2_dynamicBody );

	// _fragileBoxStatic1
	_fragileBoxStatic1.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBoxStatic1.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBoxStatic1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBoxStatic1.SetBodyType( b2_staticBody );

	// _fragileBoxStatic2
	_fragileBoxStatic2.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBoxStatic2.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBoxStatic2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBoxStatic2.SetBodyType( b2_staticBody );

	// _fragileBoxStatic3
	_fragileBoxStatic3.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBoxStatic3.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBoxStatic3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBoxStatic3.SetBodyType( b2_staticBody );

	// _fragileBoxStatic4
	_fragileBoxStatic4.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBoxStatic4.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBoxStatic4.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBoxStatic4.SetBodyType( b2_staticBody );

	// _piggyBank1
	_piggyBank1.GetBody()->GetFixtureList()->SetDensity( 7.5000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_piggyBank1.SetBodyType( b2_dynamicBody );

	// _wallBox1
	_wallBox1.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox1.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox1.SetBodyType( b2_staticBody );

	// _wallBox2
	_wallBox2.SetRotation( 20.4000f );
	_wallBox2.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox2.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox2.SetBodyType( b2_staticBody );

	// _wallBox3
	_wallBox3.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox3.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox3.SetBodyType( b2_staticBody );

	// _wallBox4
	_wallBox4.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox4.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox4.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox4.SetBodyType( b2_staticBody );

	// _wallBox5
	_wallBox5.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox5.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox5.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox5.SetBodyType( b2_staticBody );

	// _wallBox6
	_wallBox6.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox6.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox6.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox6.SetBodyType( b2_staticBody );

	// _wallBox7
	_wallBox7.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox7.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox7.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox7.SetBodyType( b2_staticBody );

	// _wallBox8
	_wallBox8.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox8.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox8.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox8.SetBodyType( b2_staticBody );

	// _wallBox9
	_wallBox9.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox9.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox9.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox9.SetBodyType( b2_staticBody );

	// _wallBox10
	_wallBox10.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox10.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox10.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox10.SetBodyType( b2_staticBody );

	// _wallBox11
	_wallBox11.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox11.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox11.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox11.SetBodyType( b2_staticBody );

	// _woodBox1
	_woodBox1.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox1.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_woodBox1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_woodBox1.SetBodyType( b2_dynamicBody );

	// _woodBox2
	_woodBox2.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox2.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_woodBox2.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_woodBox2.SetBodyType( b2_dynamicBody );

	// _wallBox12
	_wallBox12.SetRotation( 27.9000f );
	_wallBox12.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox12.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox12.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox12.SetBodyType( b2_staticBody );


	//Set blocks positions
	_bomb1.SetPosition( 44.9000f, 29.4000f );
	_bomb2.SetPosition( 26.3000f, 57.4000f );
	_borderAllClose1.SetPosition( 47.0000f, 63.0000f );
	_coin1.SetPosition( 16.2000f, 47.3000f );
	_fragileBoxStatic1.SetPosition( 26.1000f, 47.9000f );
	_fragileBoxStatic2.SetPosition( 45.0000f, 20.5000f );
	_fragileBoxStatic3.SetPosition( 6.9000f, 38.3000f );
	_fragileBoxStatic4.SetPosition( 69.7000f, 37.8000f );
	_piggyBank1.SetPosition( 88.1000f, 28.1000f );
	_wallBox1.SetPosition( 70.6000f, 2.3000f );
	_wallBox2.SetPosition( 45.7000f, 13.1000f );
	_wallBox3.SetPosition( 85.7000f, 21.3000f );
	_wallBox4.SetPosition( 21.8000f, 42.2000f );
	_wallBox5.SetPosition( 20.9000f, 21.9000f );
	_wallBox6.SetPosition( 21.7000f, 11.4000f );
	_wallBox7.SetPosition( 60.3000f, 29.5000f );
	_wallBox8.SetPosition( 35.9000f, 38.2000f );
	_wallBox9.SetPosition( 44.1000f, 34.8000f );
	_wallBox10.SetPosition( 52.0000f, 32.0000f );
	_wallBox11.SetPosition( 9.6000f, 20.9000f );
	_woodBox1.SetPosition( 69.9000f, 52.3000f );
	_woodBox2.SetPosition( 6.5000f, 22.3000f );
	_wallBox12.SetPosition( 3.3000f, 61.2000f );

	//Bombs Construction
	_bomb1.SetRange( 16.0000f );
	_bomb1.SetBombImpulse( 15000.0000f );
	_bomb2.SetRange( 16.0000f );
	_bomb2.SetBombImpulse( 6000.0000f );

	ConstFinal();
}
//--------------------------------------------------------------------------
LevelTomekW_CalmWood::LevelTomekW_CalmWood( GameTypes::LevelEnum levelEnum, bool viewMode ) : Level( levelEnum, viewMode )
{
    _levelTag = eLevelTag_Button;
    
	// _bomb2
	_bomb2.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_bomb2.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_bomb2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bomb2.SetBodyType( b2_dynamicBody );

	// _coin1
	_coin1.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin1.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin1.SetBodyType( b2_dynamicBody );

	// _fragileBoxStatic1
	_fragileBoxStatic1.SetRotation( 90.0000f );
	_fragileBoxStatic1.GetBody()->GetFixtureList()->SetDensity( 7.5500f );
	_fragileBoxStatic1.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBoxStatic1.GetBody()->GetFixtureList()->SetRestitution( 0.0500f );
	_fragileBoxStatic1.SetBodyType( b2_staticBody );

	// _fragileBoxStatic2
	_fragileBoxStatic2.SetRotation( 90.0000f );
	_fragileBoxStatic2.GetBody()->GetFixtureList()->SetDensity( 7.5500f );
	_fragileBoxStatic2.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBoxStatic2.GetBody()->GetFixtureList()->SetRestitution( 0.0500f );
	_fragileBoxStatic2.SetBodyType( b2_staticBody );

	// _fragileBoxStatic3
	_fragileBoxStatic3.SetRotation( 90.0000f );
	_fragileBoxStatic3.GetBody()->GetFixtureList()->SetDensity( 7.5500f );
	_fragileBoxStatic3.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBoxStatic3.GetBody()->GetFixtureList()->SetRestitution( 0.0500f );
	_fragileBoxStatic3.SetBodyType( b2_staticBody );

	// _piggyBank1
	_piggyBank1.GetBody()->GetFixtureList()->SetDensity( 7.5000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_piggyBank1.GetBody()->SetAngularDamping(0.3000f );
	_piggyBank1.GetBody()->SetLinearDamping(0.3000f );
	_piggyBank1.SetBodyType( b2_dynamicBody );

	// _wallBox1
	_wallBox1.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox1.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox1.SetBodyType( b2_staticBody );

	// _wallBox2
	_wallBox2.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox2.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox2.SetBodyType( b2_staticBody );

	// _wallBox3
	_wallBox3.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox3.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox3.SetBodyType( b2_staticBody );

	// _wallBox4
	_wallBox4.SetRotation( 165.0000f );
	_wallBox4.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox4.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox4.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox4.SetBodyType( b2_staticBody );

	// _wallBox5
	_wallBox5.SetRotation( 90.0000f );
	_wallBox5.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox5.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox5.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox5.SetBodyType( b2_staticBody );

	// _wallBox6
	_wallBox6.SetRotation( 105.0000f );
	_wallBox6.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox6.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox6.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox6.SetBodyType( b2_staticBody );

	// _wallBox7
	_wallBox7.SetRotation( 120.0000f );
	_wallBox7.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox7.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox7.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox7.SetBodyType( b2_staticBody );

	// _wallBox8
	_wallBox8.SetRotation( 135.0000f );
	_wallBox8.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox8.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox8.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox8.SetBodyType( b2_staticBody );

	// _wallBox9
	_wallBox9.SetRotation( 150.0000f );
	_wallBox9.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox9.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox9.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox9.SetBodyType( b2_staticBody );

	// _woodBox1
	_woodBox1.GetBody()->GetFixtureList()->SetDensity( 7.5500f );
	_woodBox1.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_woodBox1.GetBody()->GetFixtureList()->SetRestitution( 0.0500f );
	_woodBox1.SetBodyType( b2_dynamicBody );

	// _wallBox10
	_wallBox10.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox10.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox10.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox10.SetBodyType( b2_staticBody );

	// _button1
	_button1.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_button1.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_button1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_button1.SetBodyType( b2_dynamicBody );

	// _wallBox11
	_wallBox11.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox11.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox11.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox11.SetBodyType( b2_staticBody );


	//Set blocks positions
	_bomb2.SetPosition( 88.7001f, 43.0001f );
	_coin1.SetPosition( 30.1496f, 41.5501f );
	_fragileBoxStatic1.SetPosition( 54.1501f, 30.4500f );
	_fragileBoxStatic2.SetPosition( 54.1501f, 52.4499f );
	_fragileBoxStatic3.SetPosition( 54.1501f, 8.4500f );
	_piggyBank1.SetPosition( 86.7500f, 23.2000f );
	_wallBox1.SetPosition( 84.2000f, 14.1001f );
	_wallBox2.SetPosition( 84.2000f, 36.1000f );
	_wallBox3.SetPosition( 77.1501f, 16.2500f );
	_wallBox4.SetPosition( 19.9059f, 14.7996f );
	_wallBox5.SetPosition( 3.6000f, 36.0503f );
	_wallBox6.SetPosition( 4.3496f, 30.3563f );
	_wallBox7.SetPosition( 6.4474f, 25.0499f );
	_wallBox8.SetPosition( 9.9436f, 20.4936f );
	_wallBox9.SetPosition( 14.4999f, 16.9974f );
	_woodBox1.SetPosition( 54.7501f, 60.1000f );
	_wallBox10.SetPosition( 23.3000f, 36.1000f );
	_button1.SetPosition( 79.5500f, 43.0001f );
	_wallBox11.SetPosition( 28.1500f, 14.1000f );

	//Bombs Construction
	_bomb2.SetRange( 16.3000f );
	_bomb2.SetBombImpulse( 19187.5000f );

	ConstFinal();
}
//--------------------------------------------------------------------------
LevelTomekW_SelfMadeCannon::LevelTomekW_SelfMadeCannon( GameTypes::LevelEnum levelEnum, bool viewMode ) : Level( levelEnum, viewMode )
{
    _levelTag = eLevelTag_Button;
    
	// _bomb1
	_bomb1.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_bomb1.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_bomb1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bomb1.SetBodyType( b2_dynamicBody );

	// _coin1
	_coin1.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin1.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin1.SetBodyType( b2_dynamicBody );

	// _fragileBox1
	_fragileBox1.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox1.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox1.SetBodyType( b2_dynamicBody );

	// _fragileBox2
	_fragileBox2.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox2.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox2.SetBodyType( b2_dynamicBody );

	// _fragileBox3
	_fragileBox3.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox3.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox3.SetBodyType( b2_dynamicBody );

	// _fragileBoxStatic1
	_fragileBoxStatic1.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBoxStatic1.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBoxStatic1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBoxStatic1.SetBodyType( b2_staticBody );

	// _wallBox1
	_wallBox1.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox1.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox1.SetBodyType( b2_staticBody );

	// _wallBox2
	_wallBox2.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox2.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox2.SetBodyType( b2_staticBody );

	// _wallBox3
	_wallBox3.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox3.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox3.SetBodyType( b2_staticBody );

	// _wallBox4
	_wallBox4.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox4.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox4.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox4.SetBodyType( b2_staticBody );

	// _button1
	_button1.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_button1.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_button1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_button1.SetBodyType( b2_dynamicBody );

	// _wallBox5
	_wallBox5.SetRotation( 182.0000f );
	_wallBox5.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox5.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox5.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox5.SetBodyType( b2_staticBody );

	// _wallBox6
	_wallBox6.SetRotation( 198.0000f );
	_wallBox6.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox6.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox6.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox6.SetBodyType( b2_staticBody );

	// _wallBox7
	_wallBox7.SetRotation( 216.0000f );
	_wallBox7.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox7.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox7.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox7.SetBodyType( b2_staticBody );

	// _wallBox8
	_wallBox8.SetRotation( 234.0000f );
	_wallBox8.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox8.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox8.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox8.SetBodyType( b2_staticBody );

	// _wallBox9
	_wallBox9.SetRotation( 252.0000f );
	_wallBox9.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox9.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox9.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox9.SetBodyType( b2_staticBody );

	// _wallBox10
	_wallBox10.SetRotation( 265.9999f );
	_wallBox10.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox10.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox10.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox10.SetBodyType( b2_staticBody );

	// _bomb2
	_bomb2.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_bomb2.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_bomb2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bomb2.SetBodyType( b2_dynamicBody );

	// _woodBox1
	_woodBox1.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox1.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_woodBox1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_woodBox1.SetBodyType( b2_dynamicBody );

	// _wallBox11
	_wallBox11.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox11.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox11.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox11.SetBodyType( b2_staticBody );

	// _wallBox12
	_wallBox12.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox12.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox12.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox12.SetBodyType( b2_staticBody );

	// _wallBox13
	_wallBox13.SetRotation( 104.0000f );
	_wallBox13.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox13.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox13.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox13.SetBodyType( b2_staticBody );

	// _wallBox14
	_wallBox14.SetRotation( 117.0000f );
	_wallBox14.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox14.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox14.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox14.SetBodyType( b2_staticBody );

	// _wallBox15
	_wallBox15.SetRotation( 180.0000f );
	_wallBox15.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox15.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox15.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox15.SetBodyType( b2_staticBody );

	// _wallBox16
	_wallBox16.SetRotation( 167.0000f );
	_wallBox16.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox16.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox16.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox16.SetBodyType( b2_staticBody );

	// _wallBox17
	_wallBox17.SetRotation( 134.0000f );
	_wallBox17.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox17.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox17.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox17.SetBodyType( b2_staticBody );

	// _wallBox18
	_wallBox18.SetRotation( 149.0000f );
	_wallBox18.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox18.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox18.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox18.SetBodyType( b2_staticBody );

	// _piggyBank1
	_piggyBank1.GetBody()->GetFixtureList()->SetDensity( 7.5000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_piggyBank1.SetBodyType( b2_dynamicBody );

	// _wallBox19
	_wallBox19.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox19.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox19.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox19.SetBodyType( b2_staticBody );

	// _woodBox2
	_woodBox2.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox2.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_woodBox2.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_woodBox2.SetBodyType( b2_dynamicBody );

	// _wallBox20
	_wallBox20.SetRotation( 180.0000f );
	_wallBox20.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox20.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox20.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox20.SetBodyType( b2_staticBody );

	// _wallBox21
	_wallBox21.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox21.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox21.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox21.SetBodyType( b2_staticBody );

	// _wallBox22
	_wallBox22.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox22.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox22.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox22.SetBodyType( b2_staticBody );


	//Set blocks positions
	_bomb1.SetPosition( 7.8000f, 39.1508f );
	_coin1.SetPosition( 29.0499f, 26.9500f );
	_fragileBox1.SetPosition( 29.2501f, 38.1007f );
	_fragileBox2.SetPosition( 49.6501f, 38.0007f );
	_fragileBox3.SetPosition( 64.1501f, 50.1508f );
	_fragileBoxStatic1.SetPosition( 64.1508f, 40.4006f );
	_wallBox1.SetPosition( 17.1000f, 32.3501f );
	_wallBox2.SetPosition( 77.7507f, 51.5502f );
	_wallBox3.SetPosition( 29.0999f, 21.2999f );
	_wallBox4.SetPosition( 7.8000f, 32.3501f );
	_button1.SetPosition( 17.1000f, 39.1508f );
	_wallBox5.SetPosition( 72.4005f, 23.9999f );
	_wallBox6.SetPosition( 78.1174f, 24.9053f );
	_wallBox7.SetPosition( 83.2747f, 27.5332f );
	_wallBox8.SetPosition( 87.3675f, 31.6263f );
	_wallBox9.SetPosition( 89.9952f, 36.7835f );
	_wallBox10.SetPosition( 91.1007f, 42.5004f );
	_bomb2.SetPosition( 77.7507f, 57.8509f );
	_woodBox1.SetPosition( 63.1751f, 56.1000f );
	_wallBox11.SetPosition( 53.6496f, 24.7501f );
	_wallBox12.SetPosition( 64.3002f, 3.2501f );
	_wallBox13.SetPosition( 8.5667f, 19.9265f );
	_wallBox14.SetPosition( 10.7144f, 14.4499f );
	_wallBox15.SetPosition( 29.5000f, 3.2499f );
	_wallBox16.SetPosition( 24.0766f, 3.9666f );
	_wallBox17.SetPosition( 14.2401f, 9.5900f );
	_wallBox18.SetPosition( 18.7500f, 6.0643f );
	_piggyBank1.SetPosition( 83.00f, 11.5500f );
	_wallBox19.SetPosition( 39.4501f, 32.0000f );
	_woodBox2.SetPosition( 39.4501f, 44.1500f );
	_wallBox20.SetPosition( 34.5003f, 3.2499f );
	_wallBox21.SetPosition( 71.1006f, 29.2501f );
	_wallBox22.SetPosition( 48.0000f, 17.5497f );

	//Bombs Construction
	_bomb1.SetRange( 14.5000f );
	_bomb1.SetBombImpulse( 43125.0000f );
	_bomb2.SetRange( 17.0000f );
	_bomb2.SetBombImpulse( 18775.0000f );

	ConstFinal();
}
//--------------------------------------------------------------------------
