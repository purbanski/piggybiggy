#ifndef __LEVELLETSROTATE_H__
#define __LEVELLETSROTATE_H__

#include <Box2D/Box2D.h>
#include "Levels/LevelAccelerometr.h"
#include "Blocks/AllBlocks.h"

//--------------------------------------------------------------------------------------------
class LevelLetsRotate_1 : public LevelAccelerometr
{
public:
	LevelLetsRotate_1( GameTypes::LevelEnum levelEnum, bool viewMode );

private:
	puCircleWall< 20 >	_circleWall1;
	puCircleWall< 20 >	_circleWall2;
	puCoin< 40 >	_coin1;
	puPiggyBankRolled< 60 >	_piggyBank1;
	puWallBox<140,20,eBlockScaled>	_wallBox1;
	puWallBox<140,20,eBlockScaled>	_wallBox2;
	puWallBox<250,20,eBlockScaled>	_wallBox3;
	puWallBox<250,20,eBlockScaled>	_wallBox4;
	puWallBox<310,20,eBlockScaled>	_wallBox5;
	puWallBox<380,20,eBlockScaled>	_wallBox6;
	puWallBox<540,20,eBlockScaled>	_wallBox7;
	puWallBox<910,20,eBlockScaled>	_wallBox8;
	puWallBox<910,20,eBlockScaled>	_wallBox9;
};
//--------------------------------------------------------------------------------------------
class LevelLetsRotate_2 : public LevelAccelerometr
{
public:
	LevelLetsRotate_2( GameTypes::LevelEnum levelEnum, bool viewMode );

private:
	puCoin< 40 >	_coin1;
	puPiggyBankRolled< 60 >	_piggyBank1;
	puWallBox<380,20,eBlockScaled>	_wallBox16;
	puWallBox<380,20,eBlockScaled>	_wallBox2;
	puWallBox<380,20,eBlockScaled>	_wallBox3;
	puWallBox<380,20,eBlockScaled>	_wallBox4;
	puWallBox<380,20,eBlockScaled>	_wallBox8;
	puWallBox<50,20,eBlockScaled>	_wallBox10;
	puWallBox<50,20,eBlockScaled>	_wallBox11;
	puWallBox<50,20,eBlockScaled>	_wallBox12;
	puWallBox<50,20,eBlockScaled>	_wallBox13;
	puWallBox<50,20,eBlockScaled>	_wallBox14;
	puWallBox<50,20,eBlockScaled>	_wallBox15;
	puWallBox<50,20,eBlockScaled>	_wallBox5;
	puWallBox<50,20,eBlockScaled>	_wallBox6;
	puWallBox<50,20,eBlockScaled>	_wallBox7;
	puWallBox<50,20,eBlockScaled>	_wallBox9;
	puWallBox<600,20,eBlockScaled>	_wallBox1;
};
//--------------------------------------------------------------------------------------------
class LevelLetsRotate_3 : public LevelAccelerometr
{
public:
	LevelLetsRotate_3( GameTypes::LevelEnum levelEnum, bool viewMode );

private:
	puCoin< 40 >	_coin1;
	puPiggyBankRolled< 60 >	_piggyBank1;
	puWallBox<380,20,eBlockScaled>	_wallBox2;
	puWallBox<380,20,eBlockScaled>	_wallBox3;
	puWallBox<380,20,eBlockScaled>	_wallBox4;
	puWallBox<380,20,eBlockScaled>	_wallBox8;
	puWallBox<380,20,eBlockScaled>	_wallBox9;
	puWallBox<50,20,eBlockScaled>	_wallBox5;
	puWallBox<50,20,eBlockScaled>	_wallBox6;
	puWallBox<50,20,eBlockScaled>	_wallBox7;
	puWallBox<600,20,eBlockScaled>	_wallBox1;
};
//--------------------------------------------------------------------------------------------
class LevelLetsRotate_4 : public LevelAccelerometr
{
public:
	LevelLetsRotate_4( GameTypes::LevelEnum levelEnum, bool viewMode );

private:
	puCoin<40>					_coin1;
	puPiggyBankRolled<60>				_piggyBank1;
	puWallBox<100,20,eBlockScaled>	_wallBox1;
};
//--------------------------------------------------------------------------------------------
class LevelLetsRotate_5 : public LevelAccelerometr
{
public:
	LevelLetsRotate_5( GameTypes::LevelEnum levelEnum, bool viewMode );

private:
	puCoin< 40 >	_coin1;
	puPiggyBankRolled< 50 >	_piggyBank1;
	puThiefRolled< 44 >	_thief1;
	puThiefRolled< 44 >	_thief2;
	puThiefRolled< 44 >	_thief3;
	puThiefRolled< 44 >	_thief4;
	puThiefRolled< 44 >	_thief5;
	puThiefRolled< 44 >	_thief6;
	puWallBox<100,20,eBlockScaled>	_wallBox1;
};
//--------------------------------------------------------------------------------------------
class LevelLetsRotate_6 : public LevelAccelerometr
{
public:
	LevelLetsRotate_6( GameTypes::LevelEnum levelEnum, bool viewMode );

private:
	puCoin< 40 >	_coin1;
	puPiggyBankRolled< 50 >	_piggyBank1;
	puThiefRolled< 44 >	_thief1;
	puThiefRolled< 44 >	_thief2;
	puThiefRolled< 44 >	_thief3;
	puThiefRolled< 44 >	_thief4;
	puThiefRolled< 44 >	_thief5;
	puThiefRolled< 44 >	_thief6;
	puThiefRolled< 44 >	_thief7;
	puThiefRolled< 44 >	_thief8;
	puWallBox<100,20,eBlockScaled>	_wallBox1;
};
//--------------------------------------------------------------------------------------------
class LevelLetsRotate_7 : public LevelAccelerometr
{
public:
	LevelLetsRotate_7( GameTypes::LevelEnum levelEnum, bool viewMode );

private:
	puCoin< 30 >	_coin1;
	puCoin< 30 >	_coin2;
	puCoin< 30 >	_coin3;
	puPiggyBankRolled< 44 >	_piggyBank1;
	puThiefRolled< 40 >	_thief1;
	puThiefRolled< 40 >	_thief2;
	puThiefRolled< 40 >	_thief3;
	puWallBox<100,20,eBlockScaled>	_wallBox1;
	puWallBox<100,20,eBlockScaled>	_wallBox2;
	puWallBox<100,20,eBlockScaled>	_wallBox3;
	puWallBox<100,20,eBlockScaled>	_wallBox4;
	puWallBox<35,20,eBlockScaled>	_wallBox10;
	puWallBox<35,20,eBlockScaled>	_wallBox11;
	puWallBox<35,20,eBlockScaled>	_wallBox12;
	puWallBox<35,20,eBlockScaled>	_wallBox5;
	puWallBox<35,20,eBlockScaled>	_wallBox6;
	puWallBox<35,20,eBlockScaled>	_wallBox7;
	puWallBox<35,20,eBlockScaled>	_wallBox8;
	puWallBox<35,20,eBlockScaled>	_wallBox9;
	puWallBox<670,20,eBlockScaled>	_wallBox13;
	puWallBox<670,20,eBlockScaled>	_wallBox14;
	puWallBox<70,20,eBlockScaled>	_wallBox15;
	puWallBox<70,20,eBlockScaled>	_wallBox16;
	puWallBox<70,20,eBlockScaled>	_wallBox17;
	puWallBox<70,20,eBlockScaled>	_wallBox18;
};
//--------------------------------------------------------------------------------------------
class LevelLetsRotate_Circle : public LevelAccelerometr
{
public:
	LevelLetsRotate_Circle( GameTypes::LevelEnum levelEnum, bool viewMode );

private:
	puCoin< 35 >	_coin1;
	puCoin< 35 >	_coin2;
	puPiggyBankRolled< 44 >	_piggyBank1;
	puWallBox<55,20,eBlockScaled>	_wallBox10;
	puWallBox<55,20,eBlockScaled>	_wallBox11;
	puWallBox<55,20,eBlockScaled>	_wallBox12;
	puWallBox<55,20,eBlockScaled>	_wallBox13;
	puWallBox<55,20,eBlockScaled>	_wallBox14;
	puWallBox<55,20,eBlockScaled>	_wallBox15;
	puWallBox<55,20,eBlockScaled>	_wallBox16;
	puWallBox<55,20,eBlockScaled>	_wallBox17;
	puWallBox<55,20,eBlockScaled>	_wallBox18;
	puWallBox<55,20,eBlockScaled>	_wallBox19;
	puWallBox<55,20,eBlockScaled>	_wallBox1;
	puWallBox<55,20,eBlockScaled>	_wallBox20;
	puWallBox<55,20,eBlockScaled>	_wallBox21;
	puWallBox<55,20,eBlockScaled>	_wallBox22;
	puWallBox<55,20,eBlockScaled>	_wallBox23;
	puWallBox<55,20,eBlockScaled>	_wallBox24;
	puWallBox<55,20,eBlockScaled>	_wallBox25;
	puWallBox<55,20,eBlockScaled>	_wallBox26;
	puWallBox<55,20,eBlockScaled>	_wallBox27;
	puWallBox<55,20,eBlockScaled>	_wallBox28;
	puWallBox<55,20,eBlockScaled>	_wallBox29;
	puWallBox<55,20,eBlockScaled>	_wallBox2;
	puWallBox<55,20,eBlockScaled>	_wallBox30;
	puWallBox<55,20,eBlockScaled>	_wallBox31;
	puWallBox<55,20,eBlockScaled>	_wallBox32;
	puWallBox<55,20,eBlockScaled>	_wallBox33;
	puWallBox<55,20,eBlockScaled>	_wallBox3;
	puWallBox<55,20,eBlockScaled>	_wallBox4;
	puWallBox<55,20,eBlockScaled>	_wallBox5;
	puWallBox<55,20,eBlockScaled>	_wallBox6;
	puWallBox<55,20,eBlockScaled>	_wallBox7;
	puWallBox<55,20,eBlockScaled>	_wallBox8;
	puWallBox<55,20,eBlockScaled>	_wallBox9;
};

#endif
