#include "LevelChain.h"

//--------------------------------------------------------------------------
LevelChain_1::LevelChain_1( GameTypes::LevelEnum levelEnum, bool viewMode ) : Level( levelEnum, viewMode )
{
	_coinCount = 2;
	_levelTag = eLevelTag_Flies;
    
	// _coin1
	_coin1.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin1.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin1.SetBodyType( b2_dynamicBody );

	// _coin2
	_coin2.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin2.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin2.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin2.GetBody()->SetAngularDamping(0.0500f );
	_coin2.SetBodyType( b2_dynamicBody );

	// _piggyBank1
	_piggyBank1.GetBody()->GetFixtureList()->SetDensity( 7.5000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_piggyBank1.GetBody()->SetAngularDamping(0.3500f );
	_piggyBank1.GetBody()->SetLinearDamping(0.3500f );
	_piggyBank1.SetBodyType( b2_dynamicBody );

	b2Filter filter;
	// _screw1
	_screw1.SetRotation( -89.4000f );
	_screw1.GetBody()->GetFixtureList()->SetDensity( 0.1000f );
	_screw1.GetBody()->GetFixtureList()->SetFriction( 0.1000f );
	_screw1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	filter.groupIndex = -1;
	filter.categoryBits = 0;
	filter.maskBits = 65535;
	_screw1.GetBody()->GetFixtureList()->SetFilterData( filter );
	_screw1.SetBodyType( b2_staticBody );

	// _wallBox1
	_wallBox1.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox1.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox1.SetBodyType( b2_staticBody );

	// _wallBox2
	_wallBox2.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox2.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox2.SetBodyType( b2_staticBody );

	// _wallBox3
	_wallBox3.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox3.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox3.SetBodyType( b2_staticBody );

	// _woodBox1
	_woodBox1.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox1.GetBody()->GetFixtureList()->SetFriction( 0.0000f );
	_woodBox1.GetBody()->GetFixtureList()->SetRestitution( 0.0000f );
	_woodBox1.SetBodyType( b2_dynamicBody );

	// _chain1
	_chain1.SetChain( 14.0000f);
	_chain1.SetRotation( -45.0000f );
	_chain1.FixHook( false );

	// _chain2
	_chain2.SetChain( 14.0000f);
	_chain2.SetRotation( 225.0000f );
	_chain2.FixHook( false );

	// _chain3
	_chain3.SetChain( 14.0000f);
	_chain3.SetRotation( -45.0000f );
	_chain3.FixHook( false );

	// _chain4
	_chain4.SetChain( 14.0000f);
	_chain4.SetRotation( 225.0000f );
	_chain4.FixHook( false );

	// _button1
	_button1.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_button1.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_button1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_button1.SetBodyType( b2_dynamicBody );

	// _woodBox2
	_woodBox2.SetRotation( -90.0000f );
	_woodBox2.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox2.GetBody()->GetFixtureList()->SetFriction( 0.0000f );
	_woodBox2.GetBody()->GetFixtureList()->SetRestitution( 0.0000f );
	_woodBox2.SetBodyType( b2_dynamicBody );

	// _wallBox4
	_wallBox4.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox4.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox4.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox4.SetBodyType( b2_staticBody );

	// _wallBox5
	_wallBox5.SetRotation( 90.0000f );
	_wallBox5.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox5.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox5.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox5.SetBodyType( b2_staticBody );

	// _wallBox6
	_wallBox6.SetRotation( 107.1429f );
	_wallBox6.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox6.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox6.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox6.SetBodyType( b2_staticBody );

	// _wallBox7
	_wallBox7.SetRotation( 124.2857f );
	_wallBox7.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox7.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox7.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox7.SetBodyType( b2_staticBody );

	// _wallBox8
	_wallBox8.SetRotation( 141.4286f );
	_wallBox8.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox8.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox8.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox8.SetBodyType( b2_staticBody );

	// _wallBox9
	_wallBox9.SetRotation( 158.5714f );
	_wallBox9.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox9.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox9.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox9.SetBodyType( b2_staticBody );

	// _wallBox10
	_wallBox10.SetRotation( 175.7143f );
	_wallBox10.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox10.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox10.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox10.SetBodyType( b2_staticBody );

	// _wallBox11
	_wallBox11.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox11.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox11.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox11.SetBodyType( b2_staticBody );


	//Set blocks positions
	_coin1.SetPosition( 73.8519f, 37.8504f );
	_coin2.SetPosition( 40.4001f, 57.3506f );
	_piggyBank1.SetPosition( 73.8011f, 13.1000f );
	_screw1.SetPosition( 49.9002f, 43.6505f );
	_wallBox1.SetPosition( 35.4000f, 57.0506f );
	_wallBox2.SetPosition( 45.4001f, 56.8506f );
	_wallBox3.SetPosition( 58.5001f, 37.6005f );
	_woodBox1.SetPosition( 40.3502f, 51.9506f );
	_chain1.SetPosition( 64.7992f, 46.9014f );
	_chain2.SetPosition( 82.9029f, 46.9014f );
	_chain3.SetPosition( 12.2990f, 46.8015f );
	_chain4.SetPosition( 30.4011f, 46.8015f );
	_button1.SetPosition( 21.3501f, 37.7505f );
	_woodBox2.SetPosition( 49.9002f, 39.9508f );
	_wallBox4.SetPosition( 46.9500f, 27.8502f );
	_wallBox5.SetPosition( 21.8000f, 26.3001f );
	_wallBox6.SetPosition( 22.6885f, 20.4049f );
	_wallBox7.SetPosition( 25.2752f, 15.0336f );
	_wallBox8.SetPosition( 29.3302f, 10.6634f );
	_wallBox9.SetPosition( 34.4931f, 7.6825f );
	_wallBox10.SetPosition( 40.3054f, 6.3559f );
	_wallBox11.SetPosition( 61.3000f, 6.0000f );

	//Set chains hooks
	_chain1.HookOnChain( &_coin1 );
	_chain2.HookOnChain( &_coin1 );
	_chain3.HookOnChain( &_button1 );
	_chain4.HookOnChain( &_button1 );

	ConstFinal();
	CreateJoints();
}
//--------------------------------------------------------------------------
void LevelChain_1::CreateJoints()
{
	b2PrismaticJointDef jointPrisDef1;
	jointPrisDef1.collideConnected = false;
	jointPrisDef1.Initialize( _screw1.GetBody(), _woodBox1.GetBody(), _screw1.GetPosition(), b2Vec2( -1.0f, 0.0f ) );
	_jointPris1 = (b2PrismaticJoint *) _world.CreateJoint( &jointPrisDef1 );

	_jointPris1->EnableMotor( true );
	_jointPris1->SetMotorSpeed( 0.0000f );
	_jointPris1->SetMaxMotorForce( 0.0000f );

	b2RevoluteJointDef jointRevDef1;
	jointRevDef1.collideConnected = false;
	jointRevDef1.Initialize( _screw1.GetBody(), _woodBox2.GetBody(), _screw1.GetPosition() );
	_jointRev1 = (b2RevoluteJoint *) _world.CreateJoint( &jointRevDef1 );

	_jointRev1->SetLimits( 0.0000f, 1.8500f );
	_jointRev1->EnableLimit( true );
	_jointRev1->EnableMotor( false );
	_jointRev1->SetMotorSpeed( 1.5000f );
	_jointRev1->SetMaxMotorTorque( 350.0000f );

	b2GearJointDef jointGearDef1;
	jointGearDef1.bodyA = _woodBox2.GetBody();
	jointGearDef1.bodyB = _woodBox1.GetBody();
	jointGearDef1.joint1 = _jointRev1;
	jointGearDef1.joint2 = _jointPris1;
	jointGearDef1.ratio = 0.08f;
	_jointGear1 = (b2GearJoint *) _world.CreateJoint( &jointGearDef1 );

}
//--------------------------------------------------------------------------
//void LevelChain_1::CreateJoints()

//--------------------------------------------------------------------------
LevelChain_1::~LevelChain_1()
{
	if ( _jointGear1 ) _world.DestroyJoint( _jointGear1 );
	if ( _jointRev1 ) _world.DestroyJoint( _jointRev1 );
	if ( _jointPris1 ) _world.DestroyJoint( _jointPris1 );
}
//--------------------------------------------------------------------------
LevelChain_2a::LevelChain_2a( GameTypes::LevelEnum levelEnum, bool viewMode ) : Level( levelEnum, viewMode )
{
	_coinCount = 2;
    _levelTag = eLevelTag_Flies;
    
	// _coinStatic1
	_coinStatic1.SetRotation( 0.0000f );
	_coinStatic1.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coinStatic1.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coinStatic1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coinStatic1.SetBodyType( b2_staticBody );

	// _coinStatic2
	_coinStatic2.SetRotation( 0.0000f );
	_coinStatic2.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coinStatic2.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coinStatic2.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coinStatic2.SetBodyType( b2_staticBody );

	// _chain1
	_chain1.SetChain( 20.5000f);
	_chain1.SetRotation( 225.0000f );
	_chain1.FixHook( false );

	// _chain2
	_chain2.SetChain( 20.5000f);
	_chain2.SetRotation( 494.9999f );
	_chain2.FixHook( false );

	// _chain3
	_chain3.SetChain( 20.5000f);
	_chain3.SetRotation( 360.0000f );
	_chain3.FixHook( false );

	// _piggyBank1
	_piggyBank1.GetBody()->GetFixtureList()->SetDensity( 7.5000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_piggyBank1.SetBodyType( b2_dynamicBody );


	//Set blocks positions
	_coinStatic1.SetPosition( 65.7512f, 11.8000f );
	_coinStatic2.SetPosition( 25.9497f, 45.3501f );
	_chain1.SetPosition( 78.9445f, 60.1437f );
	_chain2.SetPosition( 78.9445f, 33.5564f );
	_chain3.SetPosition( 46.8497f, 46.8500f );
	_piggyBank1.SetPosition( 65.6497f, 46.8500f );

	//Set chains hooks
	_chain1.HookOnChain( &_piggyBank1 );
	_chain2.HookOnChain( &_piggyBank1 );
	_chain3.HookOnChain( &_piggyBank1 );

	ConstFinal();
}
//--------------------------------------------------------------------------
LevelChain_2b::LevelChain_2b( GameTypes::LevelEnum levelEnum, bool viewMode ) : Level( levelEnum, viewMode )
{
	_coinCount = 2;
    _levelTag = eLevelTag_Flies;

	// _coinStatic1
	_coinStatic1.SetRotation( 0.0000f );
	_coinStatic1.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coinStatic1.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coinStatic1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coinStatic1.SetBodyType( b2_staticBody );

	// _coinStatic2
	_coinStatic2.SetRotation( 0.0000f );
	_coinStatic2.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coinStatic2.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coinStatic2.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coinStatic2.SetBodyType( b2_staticBody );

	// _piggyBank1
	_piggyBank1.GetBody()->GetFixtureList()->SetDensity( 7.5000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_piggyBank1.SetBodyType( b2_dynamicBody );

	// _wallBox1
	_wallBox1.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox1.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox1.SetBodyType( b2_staticBody );

	// _wallBox2
	_wallBox2.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox2.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox2.SetBodyType( b2_staticBody );

	// _chain1
	_chain1.SetChain( 20.5000f);
	_chain1.SetRotation( 225.0000f );
	_chain1.FixHook( false );

	// _chain2
	_chain2.SetChain( 20.5000f);
	_chain2.SetRotation( 494.9999f );
	_chain2.FixHook( false );

	// _chain3
	_chain3.SetChain( 20.5000f);
	_chain3.SetRotation( 360.0000f );
	_chain3.FixHook( false );

	// _thief1
	_thief1.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_thief1.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_thief1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_thief1.SetBodyType( b2_dynamicBody );
	_thief1.FlipX();

	// _thief2
	_thief2.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_thief2.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_thief2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_thief2.SetBodyType( b2_dynamicBody );


	//Set blocks positions
	_coinStatic1.SetPosition( 59.7513f, 6.8502f );
	_coinStatic2.SetPosition( 20.4997f, 46.6002f );
	_piggyBank1.SetPosition( 60.1997f, 48.1002f );
	_wallBox1.SetPosition( 46.1499f, 7.1000f );
	_wallBox2.SetPosition( 73.3501f, 7.1000f );
	_chain1.SetPosition( 73.4946f, 61.3935f );
	_chain2.SetPosition( 73.4946f, 34.8066f );
	_chain3.SetPosition( 41.3997f, 48.1002f );
	_thief1.SetPosition( 46.1499f, 14.2503f );
	_thief2.SetPosition( 73.3501f, 14.2503f );


	//Set chains hooks
	_chain1.HookOnChain( &_piggyBank1 );
	_chain2.HookOnChain( &_piggyBank1 );
	_chain3.HookOnChain( &_piggyBank1 );

	ConstFinal();
}
//--------------------------------------------------------------------------
LevelChain_2c::LevelChain_2c( GameTypes::LevelEnum levelEnum, bool viewMode ) : Level( levelEnum, viewMode )
{
	_coinCount = 2;
    _levelTag = eLevelTag_Hacker;

	// _coinStatic1
	_coinStatic1.SetRotation( 90.0000f );
	_coinStatic1.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coinStatic1.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coinStatic1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coinStatic1.SetBodyType( b2_staticBody );

	// _coinStatic2
	_coinStatic2.SetRotation( 90.0000f );
	_coinStatic2.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coinStatic2.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coinStatic2.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coinStatic2.SetBodyType( b2_staticBody );

	// _piggyBank1
	_piggyBank1.GetBody()->GetFixtureList()->SetDensity( 7.5000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_piggyBank1.SetBodyType( b2_dynamicBody );

	// _chain1
	_chain1.SetChain( 20.5000f);
	_chain1.SetRotation( 225.0000f );
	_chain1.FixHook( false );

	// _chain2
	_chain2.SetChain( 20.5000f);
	_chain2.SetRotation( 494.9999f );
	_chain2.FixHook( false );

	// _chain3
	_chain3.SetChain( 20.5000f);
	_chain3.SetRotation( 360.0000f );
	_chain3.FixHook( false );

	// _thiefStatic1
	_thiefStatic1.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_thiefStatic1.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_thiefStatic1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_thiefStatic1.FlipX();
	_thiefStatic1.SetBodyType( b2_staticBody );

	//Set blocks positions
	_coinStatic1.SetPosition( 59.3511f, 9.9501f );
	_coinStatic2.SetPosition( 20.1496f, 9.9501f );
	_piggyBank1.SetPosition( 59.3497f, 45.1000f );
	_chain1.SetPosition( 72.6448f, 58.3937f );
	_chain2.SetPosition( 72.6448f, 31.8064f );
	_chain3.SetPosition( 40.5496f, 45.1000f );
	_thiefStatic1.SetPosition( 39.7499f, 12.1001f );

	//Set chains hooks
	_chain1.HookOnChain( &_piggyBank1 );
	_chain2.HookOnChain( &_piggyBank1 );
	_chain3.HookOnChain( &_piggyBank1 );

	ConstFinal();
}
//--------------------------------------------------------------------------
LevelChain_3::LevelChain_3( GameTypes::LevelEnum levelEnum, bool viewMode ) : LevelVertical( levelEnum, viewMode )
{
	_coinCount = 5;
    _levelTag = eLevelTag_Thief;

	// _coin1
	_coin1.SetRotation( 90.0000f );
	_coin1.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin1.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin1.SetBodyType( b2_dynamicBody );

	// _coin2
	_coin2.SetRotation( 90.0000f );
	_coin2.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin2.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin2.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin2.SetBodyType( b2_dynamicBody );

	// _coin3
	_coin3.SetRotation( 90.0000f );
	_coin3.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin3.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin3.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin3.SetBodyType( b2_dynamicBody );

	// _coin4
	_coin4.SetRotation( 90.0000f );
	_coin4.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin4.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin4.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin4.SetBodyType( b2_dynamicBody );

	// _coin5
	_coin5.SetRotation( 90.0000f );
	_coin5.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin5.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin5.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin5.SetBodyType( b2_dynamicBody );

	// _piggyBank1
	_piggyBank1.SetRotation( 90.0000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetDensity( 7.5000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_piggyBank1.GetBody()->SetAngularDamping(0.2500f );
	_piggyBank1.GetBody()->SetLinearDamping(0.2500f );
	_piggyBank1.SetBodyType( b2_dynamicBody );

	// _thief1
	_thief1.FlipX();
	_thief1.SetRotation( 90.0000f );
	_thief1.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_thief1.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_thief1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_thief1.SetBodyType( b2_dynamicBody );

	// _thief2
	_thief2.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_thief2.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_thief2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_thief2.FlipX();
	_thief2.SetRotation( 90.0000f );
	_thief2.SetBodyType( b2_dynamicBody );

	// _thief3
	_thief3.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_thief3.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_thief3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_thief3.FlipX();
	_thief3.SetRotation( 90.0000f );
	_thief3.SetBodyType( b2_dynamicBody );

	// _chain1
	_chain1.SetChain( 24.0000f);
	_chain1.SetRotation( 270.0000f );
	_chain1.FixHook( false );

	// _chain2
	_chain2.SetChain( 24.0000f);
	_chain2.SetRotation( 449.9999f );
	_chain2.FixHook( false );

	// _wallBox1
	_wallBox1.SetRotation( 90.0000f );
	_wallBox1.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox1.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox1.SetBodyType( b2_staticBody );

	_thief4.SetRotation( 90.0f );

	//Set blocks positions
	_coin1.SetPosition( 85.0998f, 32.0000f );
	_coin2.SetPosition( 77.0998f, 32.0000f );
	_coin3.SetPosition( 69.0998f, 32.0000f );
	_coin4.SetPosition( 53.1000f, 32.0000f );
	_coin5.SetPosition( 61.1000f, 32.0000f );
	_piggyBank1.SetPosition( 16.8501f, 32.0000f );
	_thief1.SetPosition( 58.9500f, 13.5000f );
	_thief2.SetPosition( 71.5503f, 13.5000f );
	_thief3.SetPosition( 84.1504f, 13.5000f );
	_thief4.SetPosition( 84.1504f, 32.0f + (32.0f - 13.5000f ));
	_chain1.SetPosition( 16.8501f, 54.8000f );
	_chain2.SetPosition( 16.8501f, 9.2000f );
	_wallBox1.SetPosition( 92.6000f, 32.00f );

	_chain1.HookOnChain( &_piggyBank1 );
	_chain2.HookOnChain( &_piggyBank1 );

	ConstFinal();
}
//--------------------------------------------------------------------------
LevelChain_4::LevelChain_4( GameTypes::LevelEnum levelEnum, bool viewMode ) : Level( levelEnum, viewMode )
{
	_coinCount = 6;
    _levelTag = eLevelTag_Flies;

	// _chain1
	_chain1.SetChain( 20.0000f);
	_chain1.SetRotation( 180.000f );

	// _chain2
	_chain2.SetChain( 36.0000f);
	_chain2.SetRotation( 180.000f );

	// _chain3
	_chain3.SetChain( 52.0000f);
	_chain3.SetRotation( 180.000f );

	// _piggyBank1
	_piggyBank1.GetBody()->GetFixtureList()->SetDensity( 7.5000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_piggyBank1.SetBodyType( b2_dynamicBody );

	// _coin1
	_coin1.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin1.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin1.SetBodyType( b2_staticBody );

	// _coin2
	_coin2.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin2.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin2.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin2.SetBodyType( b2_staticBody );

	// _coin3
	_coin3.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin3.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin3.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin3.SetBodyType( b2_staticBody );

	// _coin4
	_coin4.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin4.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin4.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin4.SetBodyType( b2_staticBody );

	// _coin5
	_coin5.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin5.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin5.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin5.SetBodyType( b2_staticBody );

	// _coin5
	_coin6.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin6.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin6.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin6.SetBodyType( b2_staticBody );


	//Set blocks positions
	_coin1.SetPosition( 10.2000f, 25.1000f );
	_coin2.SetPosition( 69.0000f, 41.9000f );
	_coin3.SetPosition( 36.2000f, 24.9000f );
	_coin4.SetPosition( 88.4000f, 22.7000f );
	_coin5.SetPosition( 50.5000f, 6.3000f );
	_coin6.SetPosition( 6.3000f, 6.3000f );
	_piggyBank1.SetPosition( 5.9000f, 62.5004f );
	_chain1.SetPosition( 24.7000f, 62.5004f );
	_chain2.SetPosition( 40.7000f, 62.5002f );
	_chain3.SetPosition( 56.7000f, 62.5002f );

	ConstFinal();

	//Set chains hooks
	_chain1.HookOnChain( &_piggyBank1 );
	_chain2.HookOnChain( &_piggyBank1 );
	_chain3.HookOnChain( &_piggyBank1 );
}
//--------------------------------------------------------------------------
LevelChain_5::LevelChain_5( GameTypes::LevelEnum levelEnum, bool viewMode ) : LevelVertical( levelEnum, viewMode )
{
	_coinCount = 3;
    _levelTag = eLevelTag_Flies;

	// _coin1
	_coin1.SetRotation( 90.0000f );
	_coin1.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin1.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_coin1.SetBodyType( b2_dynamicBody );

	// _coin2
	_coin2.SetRotation( 90.0000f );
	_coin2.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin2.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_coin2.SetBodyType( b2_dynamicBody );

	// _coin3
	_coin3.SetRotation( 90.0000f );
	_coin3.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin3.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_coin3.SetBodyType( b2_dynamicBody );

	// _chain1
	_chain1.SetChain( 22.0000f);
	_chain1.SetRotation( 449.9999f );
	_chain1.FixHook( false );

	// _chain2
	_chain2.SetChain( 22.0000f);
	_chain2.SetRotation( 270.0000f );
	_chain2.FixHook( false );

	// _chain3
	_chain3.SetChain( 22.0000f);
	_chain3.SetRotation( 449.9999f );
	_chain3.FixHook( false );

	// _chain4
	_chain4.SetChain( 22.0000f);
	_chain4.SetRotation( 270.0000f );
	_chain4.FixHook( false );

	// _chain5
	_chain5.SetChain( 22.0000f);
	_chain5.SetRotation( 449.9999f );
	_chain5.FixHook( false );

	// _chain6
	_chain6.SetChain( 22.0000f);
	_chain6.SetRotation( 270.0000f );
	_chain6.FixHook( false );

	// _thiefStatic1
	_thiefStatic1.FlipX();
	_thiefStatic1.SetRotation( 90.0000f );
	_thiefStatic1.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_thiefStatic1.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_thiefStatic1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_thiefStatic1.SetBodyType( b2_staticBody );

	// _piggyBankStatic1
	_piggyBankStatic1.FlipX();
	_piggyBankStatic1.SetRotation( 90.0000f );
	_piggyBankStatic1.GetBody()->GetFixtureList()->SetDensity( 7.5000f );
	_piggyBankStatic1.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_piggyBankStatic1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_piggyBankStatic1.SetBodyType( b2_staticBody );

	// _thiefStatic2
	_thiefStatic2.FlipX();
	_thiefStatic2.SetRotation( 90.0000f );
	_thiefStatic2.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_thiefStatic2.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_thiefStatic2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_thiefStatic2.SetBodyType( b2_staticBody );

	// _thiefStatic3
	_thiefStatic3.SetRotation( 90.0000f );
	_thiefStatic3.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_thiefStatic3.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_thiefStatic3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_thiefStatic3.SetBodyType( b2_staticBody );

	// _thiefStatic4
	_thiefStatic4.SetRotation( 90.0000f );
	_thiefStatic4.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_thiefStatic4.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_thiefStatic4.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_thiefStatic4.SetBodyType( b2_staticBody );

	// _thiefStatic5
	_thiefStatic5.SetRotation( 90.0000f );
	_thiefStatic5.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_thiefStatic5.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_thiefStatic5.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_thiefStatic5.SetBodyType( b2_staticBody );


	//Set blocks positions
	_coin1.SetPosition( 39.0501f, 32.0000f );
	_coin2.SetPosition( 9.0500f, 32.0000f );
	_coin3.SetPosition( 69.0501f, 32.0000f );
	_chain1.SetPosition( 39.0501f, 11.2000f );
	_chain2.SetPosition( 39.0501f, 52.8000f );
	_chain3.SetPosition( 9.0500f, 11.2000f );
	_chain4.SetPosition( 9.0500f, 52.8000f );
	_chain5.SetPosition( 69.0501f, 11.2000f );
	_chain6.SetPosition( 69.0501f, 52.8000f );
	
	_piggyBankStatic1.SetPosition( 87.1001f, 8.2000f );
	_thiefStatic1.SetPosition( 57.1003f, 8.2000f );
	_thiefStatic2.SetPosition( 27.1004f, 8.2000f );
	_thiefStatic3.SetPosition( 27.1005f, 55.8002f );
	_thiefStatic4.SetPosition( 57.1004f, 55.8002f );
	_thiefStatic5.SetPosition( 87.1004f, 55.8002f );

	//Set chains hooks
	_chain1.HookOnChain( &_coin1 );
	_chain2.HookOnChain( &_coin1 );
	_chain3.HookOnChain( &_coin2 );
	_chain4.HookOnChain( &_coin2 );
	_chain5.HookOnChain( &_coin3 );
	_chain6.HookOnChain( &_coin3 );

	ConstFinal();
}
//--------------------------------------------------------------------------
LevelChain_6::LevelChain_6( GameTypes::LevelEnum levelEnum, bool viewMode ) : LevelVertical( levelEnum, viewMode )
{
	_coinCount = 3;
    _levelTag = eLevelTag_Flies;

	// _coin1
	_coin1.SetRotation( 90.0000f );
	_coin1.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin1.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_coin1.SetBodyType( b2_dynamicBody );

	// _piggyBank1
	_piggyBank1.FlipX();
	_piggyBank1.SetRotation( 90.0f );
	_piggyBank1.GetBody()->GetFixtureList()->SetDensity( 7.5000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_piggyBank1.SetBodyType( b2_dynamicBody );

	// _piggyBank2
	_piggyBank2.SetRotation( 90.0000f );
	_piggyBank2.GetBody()->GetFixtureList()->SetDensity( 7.5000f );
	_piggyBank2.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_piggyBank2.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_piggyBank2.SetBodyType( b2_dynamicBody );

	// _thief1
	_thief1.SetRotation( 90.0000f );
	_thief1.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_thief1.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_thief1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_thief1.SetBodyType( b2_dynamicBody );

	// _thief2
	_thief2.SetRotation( 90.0000f );
	_thief2.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_thief2.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_thief2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_thief2.SetBodyType( b2_dynamicBody );

	// _thief3
	_thief3.FlipX();
	_thief3.SetRotation( 90.0f );
	_thief3.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_thief3.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_thief3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_thief3.SetBodyType( b2_dynamicBody );

	// _thief4
	_thief4.SetRotation( 90.0000f );
	_thief4.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_thief4.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_thief4.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_thief4.SetBodyType( b2_dynamicBody );

	// _wallBox1
	_wallBox1.SetRotation( 90.0000f );
	_wallBox1.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox1.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox1.SetBodyType( b2_staticBody );

	// _wallBox2
	_wallBox2.SetRotation( 90.0000f );
	_wallBox2.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox2.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox2.SetBodyType( b2_staticBody );

	// _wallBox3
	_wallBox3.SetRotation( 90.0000f );
	_wallBox3.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox3.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox3.SetBodyType( b2_staticBody );

	// _thief5
	_thief5.FlipX();
	_thief5.SetRotation( 90.0f );
	_thief5.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_thief5.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_thief5.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_thief5.SetBodyType( b2_dynamicBody );

	// _wallBox4
	_wallBox4.SetRotation( 90.0000f );
	_wallBox4.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox4.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox4.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox4.SetBodyType( b2_staticBody );

	// _wallBox5
	_wallBox5.SetRotation( 90.0000f );
	_wallBox5.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox5.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox5.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox5.SetBodyType( b2_staticBody );

	// _wallBox6
	_wallBox6.SetRotation( 90.0000f );
	_wallBox6.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox6.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox6.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox6.SetBodyType( b2_staticBody );

	// _thief6
	_thief6.FlipX();
	_thief6.SetRotation( 90.0f );
	_thief6.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_thief6.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_thief6.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_thief6.SetBodyType( b2_dynamicBody );

	// _piggyBank3
	_piggyBank3.FlipX();
	_piggyBank3.SetRotation( 90.0f );
	_piggyBank3.GetBody()->GetFixtureList()->SetDensity( 7.5000f );
	_piggyBank3.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_piggyBank3.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_piggyBank3.SetBodyType( b2_dynamicBody );

	// _chain1
	_chain1.SetChain( 18.5000f);
	_chain1.SetRotation( 270.0000f );
	_chain1.FixHook( false );

	// _chain2
	_chain2.SetChain( 18.5000f);
	_chain2.SetRotation( 449.9999f );
	_chain2.FixHook( false );

	// _chain3
	_chain3.SetChain( 18.5000f);
	_chain3.SetRotation( 270.0000f );
	_chain3.FixHook( false );

	// _chain4
	_chain4.SetChain( 18.5000f);
	_chain4.SetRotation( 449.9999f );
	_chain4.FixHook( false );

	// _coin2
	_coin2.SetRotation( 90.0000f );
	_coin2.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin2.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_coin2.SetBodyType( b2_dynamicBody );

	// _chain5
	_chain5.SetChain( 18.5000f);
	_chain5.SetRotation( 270.0000f );
	_chain5.FixHook( false );

	// _chain6
	_chain6.SetChain( 18.5000f);
	_chain6.SetRotation( 449.9999f );
	_chain6.FixHook( false );

	// _coin3
	_coin3.SetRotation( 90.0000f );
	_coin3.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin3.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_coin3.SetBodyType( b2_dynamicBody );


	//Set blocks positions
	_coin1.SetPosition( 37.7492f, 32.0000f );
	_piggyBank1.SetPosition( 24.4501f, 16.7500f );
	_piggyBank2.SetPosition( 54.5501f, 47.2499f );
	_thief1.SetPosition( 54.5501f, 56.9499f );
	_thief2.SetPosition( 24.4501f, 56.9499f );
	_thief3.SetPosition( 24.4501f, 7.0500f );
	_thief4.SetPosition( 84.5000f, 56.9499f );
	_wallBox1.SetPosition( 30.9502f, 11.3000f );
	_wallBox2.SetPosition( 60.9501f, 11.3000f );
	_wallBox3.SetPosition( 90.9500f, 11.3000f );
	_thief5.SetPosition( 54.5501f, 7.0500f );
	_wallBox4.SetPosition( 90.9504f, 52.6999f );
	_wallBox5.SetPosition( 60.9501f, 52.6999f );
	_wallBox6.SetPosition( 30.9502f, 52.6999f );
	_thief6.SetPosition( 84.5000f, 7.0500f );
	_piggyBank3.SetPosition( 84.5000f, 16.7500f );
	_chain1.SetPosition( 37.8492f, 48.8000f );
	_chain2.SetPosition( 37.8492f, 15.2000f );
	_chain3.SetPosition( 67.8492f, 48.8000f );
	_chain4.SetPosition( 67.8492f, 15.2000f );
	_coin2.SetPosition( 67.8492f, 32.0000f );
	_chain5.SetPosition( 7.8494f, 48.8000f );
	_chain6.SetPosition( 7.8494f, 15.2000f );
	_coin3.SetPosition( 7.8494f, 32.0000f );

	//Set chains hooks
	_chain1.HookOnChain( &_coin1 );
	_chain2.HookOnChain( &_coin1 );
	_chain3.HookOnChain( &_coin2 );
	_chain4.HookOnChain( &_coin2 );
	_chain5.HookOnChain( &_coin3 );
	_chain6.HookOnChain( &_coin3 );

	ConstFinal();
}
//--------------------------------------------------------------------------
LevelChain_7a::LevelChain_7a( GameTypes::LevelEnum levelEnum, bool viewMode ) : Level( levelEnum, viewMode )
{
    _levelTag = eLevelTag_Flies;

	// _coin1
	_coin1.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin1.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_coin1.SetBodyType( b2_dynamicBody );

	// _piggyBank1
	_piggyBank1.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_piggyBank1.SetBodyType( b2_dynamicBody );

	// _wallBox1
	_wallBox1.SetRotation( 90.0000f );
	_wallBox1.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox1.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox1.SetBodyType( b2_staticBody );

	// _chain1
	_chain1.SetChain( 20.0f );
	_chain1.SetRotation( 240.5589f );
	_chain1.FixHook( false );

	// _chain2
	_chain2.SetChain( 20.0f );
	_chain2.SetRotation( 300.5589f );
	_chain2.FixHook( false );

	// _chain3
	_chain3.SetChain( 18.0f );
	_chain3.SetRotation( 0.0000f );
	_chain3.FixHook( false );

	// _chain4
	_chain4.SetChain( 18.0f );
	_chain4.SetRotation( 180.0000f );
	_chain4.FixHook( false );


	//Set blocks positions
	_coin1.SetPosition( 38.4000f, 38.4000f );
	_piggyBank1.SetPosition( 57.2000f, 38.7000f );
	_wallBox1.SetPosition( 47.3000f, 36.8202f );
	_chain1.SetPosition( 47.6407f, 54.7722f );
	_chain2.SetPosition( 47.6416f, 54.8888f );
	_chain3.SetPosition( 21.6000f, 38.4000f );
	_chain4.SetPosition( 74.0001f, 38.7000f );

	ConstFinal();

	//Set chains hooks
	_chain1.HookOnChain( &_coin1 );
	_chain2.HookOnChain( &_piggyBank1 );
	_chain3.HookOnChain( &_coin1 );
	_chain4.HookOnChain( &_piggyBank1 );
}
//--------------------------------------------------------------------------
LevelChain_7b::LevelChain_7b( GameTypes::LevelEnum levelEnum, bool viewMode ) : Level( levelEnum, viewMode )
{
    _levelTag = eLevelTag_Flies;

	// _coin1
	_coin1.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin1.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_coin1.SetBodyType( b2_dynamicBody );

	// _piggyBank1
	_piggyBank1.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_piggyBank1.SetBodyType( b2_dynamicBody );

	// _chain1
	_chain1.SetChain( 18.0000f);
	_chain1.SetRotation( 360.0000f );
	_chain1.FixHook( false );

	// _chain2
	_chain2.SetChain( 18.0000f);
	_chain2.SetRotation( 540.0000f );
	_chain2.FixHook( false );

	// _wallBox2
	_wallBox2.SetRotation( 90.0000f );
	_wallBox2.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox2.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox2.SetBodyType( b2_staticBody );

	// _wallBox3
	_wallBox3.SetRotation( 90.0000f );
	_wallBox3.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox3.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox3.SetBodyType( b2_staticBody );

	// _chain3
	_chain3.SetChain( 37.7191f );
	_chain3.SetRotation( 209.8956f );
	_chain3.FixHook( false );

	// _chain4
	_chain4.SetChain( 37.7191f );
	_chain4.SetRotation( 328.8956f );
	_chain4.FixHook( false );


	//Set blocks positions
	_coin1.SetPosition( 40.1520f, 42.6295f );
	_piggyBank1.SetPosition( 56.1991f, 42.6295f );
	_chain1.SetPosition( 23.3520f, 42.6295f );
	_chain2.SetPosition( 72.9995f, 42.6295f );
	_wallBox2.SetPosition( 46.4001f, 39.3999f );
	_wallBox3.SetPosition( 46.3001f, 13.9000f );
	_chain3.SetPosition( 72.9819f, 61.5636f );
	_chain4.SetPosition( 23.3818f, 61.1182f );

	//Set chains hooks
	_chain1.HookOnChain( &_coin1 );
	_chain2.HookOnChain( &_piggyBank1 );
	_chain3.HookOnChain( &_coin1 );
	_chain4.HookOnChain( &_piggyBank1 );

	ConstFinal();
}
//--------------------------------------------------------------------------
