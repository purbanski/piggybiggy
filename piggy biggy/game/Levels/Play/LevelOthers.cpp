#include "LevelOthers.h"
#include "Skins.h"

//--------------------------------------------------------------------------
LevelOthers_1::LevelOthers_1( GameTypes::LevelEnum levelEnum, bool viewMode ) : Level( levelEnum, viewMode )
{
	_coinCount = 3;
    _levelTag = eLevelTag_Hacker;
    
	// _bombStatic1
	_bombStatic1.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_bombStatic1.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_bombStatic1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bombStatic1.SetBodyType( b2_staticBody );

	// _coin1
	_coin1.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin1.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_coin1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin1.GetBody()->SetAngularDamping(0.1000f );
	_coin1.GetBody()->SetLinearDamping(0.1000f );
	_coin1.SetBodyType( b2_dynamicBody );

	// _coin2
	_coin2.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin2.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_coin2.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin2.GetBody()->SetAngularDamping(0.1000f );
	_coin2.GetBody()->SetLinearDamping(0.1000f );
	_coin2.SetBodyType( b2_dynamicBody );

	// _coin3
	_coin3.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin3.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin3.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin3.GetBody()->SetAngularDamping(0.1000f );
	_coin3.GetBody()->SetLinearDamping(0.1000f );
	_coin3.SetBodyType( b2_dynamicBody );

	// _fragileBox1
	_fragileBox1.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox1.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox1.GetBody()->SetAngularDamping(-0.1000f );
	_fragileBox1.GetBody()->SetLinearDamping(-0.1000f );
	_fragileBox1.SetBodyType( b2_dynamicBody );

	// _piggyBank1
	_piggyBank1.GetBody()->GetFixtureList()->SetDensity( 7.5000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_piggyBank1.SetBodyType( b2_dynamicBody );

	// _piggyBank2
	_piggyBank2.GetBody()->GetFixtureList()->SetDensity( 7.5000f );
	_piggyBank2.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_piggyBank2.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_piggyBank2.GetBody()->SetAngularDamping(0.1000f );
	_piggyBank2.GetBody()->SetLinearDamping(0.1000f );
	_piggyBank2.SetBodyType( b2_dynamicBody );

	// _wallBox1
	_wallBox1.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox1.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox1.GetBody()->SetAngularDamping(-0.1000f );
	_wallBox1.GetBody()->SetLinearDamping(-0.1000f );
	_wallBox1.SetBodyType( b2_staticBody );

	// _wallBox2
	_wallBox2.SetRotation( 21.6001f );
	_wallBox2.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox2.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox2.SetBodyType( b2_staticBody );

	// _wallBox3
	_wallBox3.SetRotation( 44.1001f );
	_wallBox3.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox3.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox3.SetBodyType( b2_staticBody );

	// _wallBox4
	_wallBox4.SetRotation( 74.6000f );
	_wallBox4.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox4.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox4.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox4.SetBodyType( b2_staticBody );

	// _wallBox5
	_wallBox5.SetRotation( 104.6001f );
	_wallBox5.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox5.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox5.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox5.SetBodyType( b2_staticBody );

	// _wallBox6
	_wallBox6.SetRotation( 134.1002f );
	_wallBox6.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox6.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox6.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox6.SetBodyType( b2_staticBody );

	// _wallBox7
	_wallBox7.SetRotation( 0.1000f );
	_wallBox7.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox7.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox7.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox7.SetBodyType( b2_staticBody );

	// _wallBox8
	_wallBox8.SetRotation( 156.6001f );
	_wallBox8.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox8.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox8.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox8.SetBodyType( b2_staticBody );

	// _wallBox9
	_wallBox9.SetRotation( 179.1001f );
	_wallBox9.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox9.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox9.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox9.SetBodyType( b2_staticBody );

	// _wallBox10
	_wallBox10.SetRotation( 180.6001f );
	_wallBox10.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox10.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox10.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox10.SetBodyType( b2_staticBody );

	// _wallBox11
	_wallBox11.SetRotation( 206.1000f );
	_wallBox11.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox11.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox11.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox11.SetBodyType( b2_staticBody );

	// _wallBox12
	_wallBox12.SetRotation( 0.1000f );
	_wallBox12.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox12.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox12.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox12.SetBodyType( b2_staticBody );

	// _wallBox13
	_wallBox13.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox13.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox13.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox13.SetBodyType( b2_staticBody );

	// _chain1
	_chain1.SetChain( 22.0000f);
	_chain1.SetRotation( 345.0000f );
	_chain1.FixHook( false );

	// _chain2
	_chain2.SetChain( 22.0000f);
	_chain2.SetRotation( 195.0000f );
	_chain2.FixHook( false );


	//Set blocks positions
	_bombStatic1.SetPosition( 7.1500f, 20.0000f );
	_coin1.SetPosition( 90.0501f, 10.1501f );
	_coin2.SetPosition( 10.3000f, 34.1497f );
	_coin3.SetPosition( 33.0500f, 8.9000f );
	_fragileBox1.SetPosition( 78.6501f, 11.0501f );
	_piggyBank1.SetPosition( 48.7082f, 53.5666f );
	_piggyBank2.SetPosition( 78.7501f, 23.8498f );
	_wallBox1.SetPosition( 77.5499f, 3.9501f );
	_wallBox2.SetPosition( 26.1304f, 27.4818f );
	_wallBox3.SetPosition( 21.3776f, 24.4131f );
	_wallBox4.SetPosition( 18.2600f, 19.4500f );
	_wallBox5.SetPosition( 18.0800f, 13.6100f );
	_wallBox6.SetPosition( 21.0554f, 8.3596f );
	_wallBox7.SetPosition( 37.9460f, 28.4985f );
	_wallBox8.SetPosition( 25.7096f, 5.1430f );
	_wallBox9.SetPosition( 31.2404f, 3.9522f );
	_wallBox10.SetPosition( 37.2180f, 3.9576f );
	_wallBox11.SetPosition( 42.5672f, 5.1974f );
	_wallBox12.SetPosition( 31.6960f, 28.4985f );
	_wallBox13.SetPosition( 10.3000f, 28.4998f );
	_chain1.SetPosition( 27.2001f, 58.9500f );
	_chain2.SetPosition( 68.7995f, 58.9500f );

	//Bombs Construction
	_bombStatic1.SetRange( 17.8000f );
	_bombStatic1.SetBombImpulse( 15050.0000f );


	//Set chains hooks
	_chain1.HookOnChain( &_piggyBank1 );
	_chain2.HookOnChain( &_piggyBank1 );

	ConstFinal();
}
//--------------------------------------------------------------------------
LevelOthers_2::LevelOthers_2( GameTypes::LevelEnum levelEnum, bool viewMode ) : Level( levelEnum, viewMode )
{
	// _bomb1
	_bomb1.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_bomb1.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_bomb1.GetBody()->GetFixtureList()->SetRestitution( 0.0000f );
	_bomb1.SetBodyType( b2_dynamicBody );

	// _bombStatic1
	_bombStatic1.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_bombStatic1.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_bombStatic1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bombStatic1.SetBodyType( b2_staticBody );

	// _coin1
	_coin1.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin1.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin1.GetBody()->GetFixtureList()->SetRestitution( 0.0000f );
	_coin1.SetBodyType( b2_dynamicBody );

	// _fragileBoxStatic1
	_fragileBoxStatic1.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBoxStatic1.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBoxStatic1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBoxStatic1.SetBodyType( b2_staticBody );

	// _fragileBoxStatic2
	_fragileBoxStatic2.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBoxStatic2.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBoxStatic2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBoxStatic2.SetBodyType( b2_staticBody );

	// _piggyBankStatic1
	_piggyBankStatic1.GetBody()->GetFixtureList()->SetDensity( 7.5000f );
	_piggyBankStatic1.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_piggyBankStatic1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_piggyBankStatic1.SetBodyType( b2_staticBody );
	_piggyBankStatic1.FlipX();
	//_piggyBankStatic1.FlipX();

	// _woodBox1
	_woodBox1.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox1.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_woodBox1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_woodBox1.SetBodyType( b2_dynamicBody );

	// _chain1
	_chain1.SetChain( 22.2500f);
	_chain1.SetRotation( 270.0000f );
	_chain1.FixHook( false );

	// _chain2
	_chain2.SetChain( 22.2500f);
	_chain2.SetRotation( 270.0000f );
	_chain2.FixHook( false );

	// _chain3
	_chain3.SetChainCubic( 20.5000f, 10.0000f, 2.5000f);
	_chain3.SetRotation( 0.0000f );
	_chain3.FixHook( false );

	// _wallBox1
	_wallBox1.SetRotation( 4.0000f );
	_wallBox1.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox1.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox1.SetBodyType( b2_staticBody );

	// _wallBox2
	_wallBox2.SetRotation( 4.0000f );
	_wallBox2.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox2.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox2.SetBodyType( b2_staticBody );

	// _wallBox3
	_wallBox3.SetRotation( 90.0000f );
	_wallBox3.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox3.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox3.SetBodyType( b2_staticBody );

	// _wallBox4
	_wallBox4.SetRotation( 90.0000f );
	_wallBox4.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox4.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox4.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox4.SetBodyType( b2_staticBody );


	//Set blocks positions
	_bomb1.SetPosition( 70.4503f, 60.2497f );
	_bombStatic1.SetPosition( 37.400f, 46.7497f );
	_piggyBankStatic1.SetPosition( 8.6f, 46.7493f );
	
	_coin1.SetPosition( 80.9501f, 60.2497f );
	_fragileBoxStatic1.SetPosition( 70.4503f, 46.7497f );
	_fragileBoxStatic2.SetPosition( 80.9501f, 46.7497f );
	
	_woodBox1.SetPosition( 75.5500f, 29.8496f );
	_chain1.SetPosition( 62.1999f, 50.7028f );
	_chain2.SetPosition( 89.0285f, 50.7028f );
	_chain3.SetPosition( 52.45f, 50.8501f );
	_wallBox1.SetPosition( 46.5999f, 6.8500f );
	_wallBox2.SetPosition( 36.65f, 18.10f );
	_wallBox3.SetPosition( 17.6499f, 10.7000f );
	_wallBox4.SetPosition( 17.6499f, 22.7500f );

	//Bombs Construction
	_bomb1.SetRange( 16.5000f );
	_bomb1.SetBombImpulse( 17250.0000f );
	_bombStatic1.SetRange( 19.1000f );
	_bombStatic1.SetBombImpulse( 9475.0000f );

	//Set chains hooks
	_chain1.HookOnChain( &_woodBox1 );
	_chain2.HookOnChain( &_woodBox1 );
	_chain3.HookOnChain( &_woodBox1 );

	ConstFinal();
}
//--------------------------------------------------------------------------
LevelOthers_ControlWheel::LevelOthers_ControlWheel( GameTypes::LevelEnum levelEnum, bool viewMode ) : LevelActivable( levelEnum, viewMode )
{
	_coinCount = 3;

	b2Filter filter;

	// _coin1
	_coin1.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin1.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin1.SetBodyType( b2_dynamicBody );

	// _coin2
	_coin2.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin2.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin2.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin2.SetBodyType( b2_staticBody );

	// _coin3
	_coin3.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin3.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin3.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin3.SetBodyType( b2_dynamicBody );

	// _piggyBank1
	_piggyBank1.GetBody()->GetFixtureList()->SetDensity( 7.5000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_piggyBank1.SetBodyType( b2_dynamicBody );

	// _wallBox1
	_wallBox1.SetRotation( 62.2800f );
	_wallBox1.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox1.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox1.SetBodyType( b2_staticBody );

	// _wallBox2
	_wallBox2.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox2.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox2.SetBodyType( b2_staticBody );

	// _wallBox3
	_wallBox3.SetRotation( 38.8800f );
	_wallBox3.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox3.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox3.SetBodyType( b2_staticBody );

	// _wallBox4
	_wallBox4.SetRotation( 77.7601f );
	_wallBox4.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox4.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox4.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox4.SetBodyType( b2_staticBody );

	// _wallBox5
	_wallBox5.SetRotation( 116.6401f );
	_wallBox5.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox5.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox5.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox5.SetBodyType( b2_staticBody );

	// _wallBox7
	_wallBox7.SetRotation( 101.1601f );
	_wallBox7.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox7.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox7.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox7.SetBodyType( b2_staticBody );

	// _wallBox8
	_wallBox8.SetRotation( 178.9201f );
	_wallBox8.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox8.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox8.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox8.SetBodyType( b2_staticBody );

	// _wallBox9
	_wallBox9.SetRotation( 140.0400f );
	_wallBox9.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox9.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox9.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox9.SetBodyType( b2_staticBody );

	// _wallBox10
	_wallBox10.GetBody()->GetFixtureList()->SetDensity( 2.0000f );
	_wallBox10.GetBody()->GetFixtureList()->SetFriction( 0.3000f );
	_wallBox10.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox10.SetBodyType( b2_staticBody );

	// _screw2
	_screw2.GetBody()->GetFixtureList()->SetDensity( 0.1000f );
	_screw2.GetBody()->GetFixtureList()->SetFriction( 0.1000f );
	_screw2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	filter.groupIndex = -1;
	filter.categoryBits = 0;
	filter.maskBits = 65535;
	_screw2.GetBody()->GetFixtureList()->SetFilterData( filter );
	_screw2.SetBodyType( b2_staticBody );

	// _woodBox1
	_woodBox1.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox1.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_woodBox1.GetBody()->GetFixtureList()->SetRestitution( 0.0f );
	_woodBox1.SetRotation( 90.0f );
	_woodBox1.SetBodyType( b2_dynamicBody );
	_woodBox1.SetSpriteZOrder( GameTypes::eSpritesAbove );

	// _circleWood1
	_circleWood1.GetBody()->GetFixtureList()->SetDensity( 0.1000f );
	_circleWood1.GetBody()->GetFixtureList()->SetFriction( 0.1000f );
	_circleWood1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_circleWood1.SetBodyType( b2_dynamicBody );
	_circleWood1.SetBlockType( GameTypes::eOther );
	
	filter.groupIndex = -1;
	filter.categoryBits = 0;
	filter.maskBits = 65535;
	_circleWood1.GetBody()->GetFixtureList()->SetFilterData( filter );
	

	_coin1.GetBody()->GetFixtureList()->SetRestitution( 0.0f );
	_coin2.GetBody()->GetFixtureList()->SetRestitution( 0.0f );
	_coin3.GetBody()->GetFixtureList()->SetRestitution( 0.0f );
	_piggyBank1.GetBody()->GetFixtureList()->SetRestitution( 0.0f );
	_piggyBank2.GetBody()->GetFixtureList()->SetRestitution( 0.0f );

	//Set blocks positions
	_coin1.SetPosition( 44.5000f, 3.2000f );
	_piggyBank1.SetPosition( 2.9500f, 3.4500f );
	_wallBox1.SetPosition( 43.2339f, 0.7854f );
	_wallBox2.SetPosition( 0.4000f, 3.1000f );
	_wallBox3.SetPosition( 0.9538f, 1.5308f );
	_wallBox4.SetPosition( 2.3700f, 0.6568f );
	_wallBox5.SetPosition( 4.0210f, 0.8654f );
	_wallBox7.SetPosition( 44.8807f, 0.5457f );
	_wallBox8.SetPosition( 46.8964f, 2.9513f );
	_wallBox9.SetPosition( 46.3130f, 1.3928f );
	_screw2.SetPosition( 21.6500f, 15.6500f );
	_woodBox1.SetPosition( 21.6500f, 9.6500f );
	
	_circleWood1.SetPosition( 21.6500f, 15.6500f );

	_circleWood1.GetBody()->GetFixtureList()->SetDensity( 1.0f );
	if ( _circleWood1.GetSprite() && _circleWood1.GetSprite()->getParent() ) 
	{
		_circleWood1.GetSprite()->removeFromParentAndCleanup( true );
		_circleWood1.SetSprite( CCSprite::spriteWithFile( Skins::GetSkinName( "Images/Blocks/ControlWheel.png" ) ) );
	}

	_world.SetGravity( b2Vec2( 0.0f, -6.0f ));

	//-----------------
	// thiefs
	_thief1.SetPosition( 8.00f, 23.3f +20);
	_thief2.SetPosition( 11.6f, 20.8f +20);
	_thief3.SetPosition( 15.2f, 18.3f +20);
	_thief4.SetPosition( 18.80f, 15.8f +20);
	
	_thief5.SetPosition( 24.5f, 17.85f + 17 );
	_thief6.SetPosition( 27.5f, 21.05f + 17 );
	_thief7.SetPosition( 30.5f, 24.25f + 17 );
	_thief8.SetPosition( 33.5f, 27.45f + 17 );
	_thief9.SetPosition( 36.5f, 30.65f + 17 );
	_thief10.SetPosition( 39.5f, 33.85f + 17 );

	BlockContainer thiefs;
	thiefs.push_back( &_thief1 );
	thiefs.push_back( &_thief2 );
	thiefs.push_back( &_thief3 );
	thiefs.push_back( &_thief4 );
	thiefs.push_back( &_thief5 );
	thiefs.push_back( &_thief6 );
	thiefs.push_back( &_thief7 );
	thiefs.push_back( &_thief8 );
	thiefs.push_back( &_thief9 );
	thiefs.push_back( &_thief10);

	float res = 0.4f;
	int falloutTime = 700;

	for ( BlockContainer::iterator it = thiefs.begin(); it != thiefs.end(); it++ )
	{
		(*it)->GetBody()->GetFixtureList()->SetRestitution( res );
		_sleepers.push_back( Sleeper( (*it), falloutTime ));
	}

	//-----------------------------
	// other fallouts
	_coin2.SetPosition( 17.5f, 34.0f );
	_coin3.SetPosition( 30.5f, 34.0f );
	_piggyBank2.SetPosition( 13.5f, 34.0f );

	_sleepers.push_back( Sleeper( &_coin2, 120 ));
	_sleepers.push_back( Sleeper( &_coin3, 410 ));
	_sleepers.push_back( Sleeper( &_piggyBank2, 970 ));


	filter.groupIndex = -1;
	filter.categoryBits = 0;
	filter.maskBits = 65535;

	_circleWood2.SetPosition( 21.6500f, 2.500f );
	_circleWood2.GetBody()->GetFixtureList()->SetFilterData( filter );
	_circleWood2.GetSprite()->removeFromParentAndCleanup( true );
	_circleWood2.SetSprite( CCSprite::spriteWithFile( Skins::GetSkinName( "Images/Blocks/TouchIcon.png" ) ));
	_circleWood2.SetSpriteZOrder( GameTypes::eSpritesBeloweX2 );

	ConstFinal();
	AdjustOldScreen();
	CreateJoints();
}
//--------------------------------------------------------------------------
void LevelOthers_ControlWheel::CreateJoints()
{
	b2RevoluteJoint *jointRev1;
	b2RevoluteJoint *jointRev2;
	b2PrismaticJoint *jointPris;
	
	b2RevoluteJointDef jointRevDef;
	b2PrismaticJointDef jointPrisDef;
	b2GearJointDef jointGearDef;


	//--------
	// rev1
	//--------
	jointRevDef.collideConnected = false;
	jointRevDef.Initialize( _screw2.GetBody(), _woodBox1.GetBody(),  _screw2.GetPosition());
	
	jointRev1 = (b2RevoluteJoint *) _world.CreateJoint( &jointRevDef );
	jointRev1->EnableMotor( true );
	jointRev1->SetMotorSpeed( 0.0000f );
	jointRev1->SetMaxMotorTorque( 2500.0000f );
	jointRev1->SetLimits( -b2_pi / 1.5f , b2_pi / 1.5f );
	jointRev1->EnableLimit( true );


	//--------
	// rev2
	//--------
	jointRevDef.Initialize( _screw2.GetBody(), _circleWood1.GetBody(),  _screw2.GetPosition());
	
	jointRev2 = (b2RevoluteJoint *) _world.CreateJoint( &jointRevDef );
	jointRev2->EnableMotor( false );

	
	//--------
	// pris1
	//--------
	jointPrisDef.collideConnected = false;
	jointPrisDef.Initialize( _screw2.GetBody(), _circleWood2.GetBody(), _circleWood2.GetPosition(), b2Vec2( 1.0f, 0.0f ));
	
	jointPris = (b2PrismaticJoint *) _world.CreateJoint( &jointPrisDef );
	jointPris->EnableMotor( true );
	jointPris->SetMotorSpeed( 0.0000f );
	jointPris->SetMaxMotorForce( 1500.0000f );


	//--------
	// gear 1
	//--------
	jointGearDef.bodyA = _circleWood2.GetBody();
	jointGearDef.bodyB = _woodBox1.GetBody();
	jointGearDef.joint1 = jointPris;
	jointGearDef.joint2 = jointRev1;
	jointGearDef.ratio = -7.0f;
	_jointGear1 = (b2GearJoint *) _world.CreateJoint( &jointGearDef );


	//--------
	// gear 2
	//--------
	jointGearDef.bodyB = _circleWood1.GetBody();
	jointGearDef.bodyA = _circleWood2.GetBody();
	jointGearDef.joint2 = jointRev2;
	jointGearDef.joint1 = jointPris;
	jointGearDef.ratio = -7.0f;
	_jointGear2 = (b2GearJoint *) _world.CreateJoint( &jointGearDef );

}
//--------------------------------------------------------------------------
LevelOthers_ControlWheel::~LevelOthers_ControlWheel()
{
	_world.DestroyJoint( _jointGear1 );
	_world.DestroyJoint( _jointGear2 );
}
//--------------------------------------------------------------------------
LevelOthers_4::LevelOthers_4( GameTypes::LevelEnum levelEnum, bool viewMode ) : Level( levelEnum, viewMode )
{
	float r;
	r = 15.5f;

	// big triangles
//	_polygonFragile1.Construct(
//		&b2Vec2( 0.0f, 0.0f ),
//		&b2Vec2( r, 0.0f ),
//		&b2Vec2( r / 2.0f, r / 2.0f ), NULL ); 
//
//	_polygonFragile2.Construct(
//		&b2Vec2( 0.0f, 0.0f ),
//		&b2Vec2( r, 0.0f ),
//		&b2Vec2( r / 2.0f, r / 2.0f ), NULL ); 
//
//	//small triangles
//	_polygonFragile3.Construct(
//		&b2Vec2( 0.0f, 0.0f ),
//		&b2Vec2( r / 2.0f, 0.0f ),
//		&b2Vec2( r / 4.0f, r / 4.0f ), NULL ); 
//
//	_polygonFragile4.Construct(
//		&b2Vec2( 0.0f, 0.0f ),
//		&b2Vec2( r / 2.0f, 0.0f ),
//		&b2Vec2( r / 4.0f, r / 4.0f ), NULL ); 
//
//	// middle triangle
//	_polygonFragile5.Construct(
//		&b2Vec2( 0.0f, 0.0f ),
//		&b2Vec2( r / 2.0f, 0.0f ),
//		&b2Vec2( r / 2.0f, r / 2.0f ), NULL ); 
//
//	// box
//	float boxSize = r / 4.0f * sqrt( 2.0f );
//	_polygonFragile6.Construct(
//		&b2Vec2( 0.0f, 0.0f ),
//		&b2Vec2( boxSize, 0.0f ),
//		&b2Vec2( boxSize, boxSize ), 
//		&b2Vec2( 0.0f, boxSize ), 
//		NULL ); 
//
//	// box
//	_polygonFragile7.Construct(
//		&b2Vec2( 0.0f, 0.0f ),
//		&b2Vec2( r / 2.0f, 0.0f ),
//		&b2Vec2( r / 2.0f + r / 4.0f, r / 4.0f ),
//		&b2Vec2( r / 4.0f, r / 4.0f ), 
//		NULL ); 
//
//
//	// _polygonFragile1
//	_polygonFragile1.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
//	_polygonFragile1.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
//	_polygonFragile1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
//	_polygonFragile1.SetBodyType( b2_dynamicBody );
//
//	// _polygonFragile2
//	_polygonFragile2.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
//	_polygonFragile2.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
//	_polygonFragile2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
//	_polygonFragile2.SetBodyType( b2_dynamicBody );
//
//	//Set blocks positions
//	_polygonFragile1.SetPosition( 24.2000f, 16.2000f );
//	_polygonFragile2.SetPosition( 24.2000f, 16.2000f );
//	_polygonFragile3.SetPosition( 24.2000f, 16.2000f );
//	_polygonFragile4.SetPosition( 24.2000f, 16.2000f );
//	_polygonFragile5.SetPosition( 24.2000f, 16.2000f );
//	_polygonFragile6.SetPosition( 24.2000f, 16.2000f );
//	_polygonFragile7.SetPosition( 24.2000f, 16.2000f );
//
//	_floor.SetPosition( 23.5f, 31.5f );

	ConstFinal();
}
//--------------------------------------------------------------------------
LevelSwingAndBomb::LevelSwingAndBomb( GameTypes::LevelEnum levelEnum, bool viewMode )  : Level( levelEnum, viewMode )
{
	_coinCount = 2;
    _levelTag = eLevelTag_Boxing;
    
	// _bomb1
	_bomb1.GetBody()->GetFixtureList()->SetDensity( 6.0500f );
	_bomb1.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_bomb1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bomb1.SetBodyType( b2_dynamicBody );

	// _boxerGlove1
	_boxerGlove1.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_boxerGlove1.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_boxerGlove1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_boxerGlove1.SetBodyType( b2_dynamicBody );

	// _coin1
	_coin1.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin1.GetBody()->GetFixtureList()->SetFriction( 0.7500f );
	_coin1.GetBody()->GetFixtureList()->SetRestitution( 0.0500f );
	_coin1.SetBodyType( b2_dynamicBody );

	// _coin2
	_coin2.GetBody()->GetFixtureList()->SetDensity( 3.0500f );
	_coin2.GetBody()->GetFixtureList()->SetFriction( 0.9500f );
	_coin2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_coin2.SetBodyType( b2_dynamicBody );

	// _fragileBox1
	_fragileBox1.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox1.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox1.GetBody()->GetFixtureList()->SetRestitution( 0.0500f );
	_fragileBox1.SetBodyType( b2_dynamicBody );

	// _fragileBox2
	_fragileBox2.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox2.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox2.SetBodyType( b2_dynamicBody );

	// _fragileBox3
	_fragileBox3.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox3.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBox3.SetBodyType( b2_dynamicBody );

	// _piggyBank1
	_piggyBank1.GetBody()->GetFixtureList()->SetDensity( 7.5000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetFriction( 0.9000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_piggyBank1.GetBody()->SetAngularDamping(0.2000f );
	_piggyBank1.GetBody()->SetLinearDamping(0.2000f );
	_piggyBank1.FlipX();
	_piggyBank1.SetBodyType( b2_dynamicBody );

	// _wallBox1
	_wallBox1.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox1.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox1.SetBodyType( b2_staticBody );

	// _wallBox2
	_wallBox2.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox2.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox2.SetBodyType( b2_staticBody );

	// _wallBox3
	_wallBox3.SetRotation( 203.2910f );
	_wallBox3.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox3.GetBody()->GetFixtureList()->SetFriction( 0.9500f );
	_wallBox3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox3.SetBodyType( b2_staticBody );

	// _wallBox4
	_wallBox4.SetRotation( 212.7821f );
	_wallBox4.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox4.GetBody()->GetFixtureList()->SetFriction( 0.9500f );
	_wallBox4.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox4.SetBodyType( b2_staticBody );

	// _wallBox5
	_wallBox5.SetRotation( 101.0000f );
	_wallBox5.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox5.GetBody()->GetFixtureList()->SetFriction( 0.9000f );
	_wallBox5.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox5.SetBodyType( b2_staticBody );

	// _wallBox6
	_wallBox6.SetRotation( 217.2998f );
	_wallBox6.GetBody()->GetFixtureList()->SetDensity( 7.5500f );
	_wallBox6.GetBody()->GetFixtureList()->SetFriction( 0.9500f );
	_wallBox6.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox6.SetBodyType( b2_staticBody );

	// _wallBox7
	_wallBox7.SetRotation( 228.2998f );
	_wallBox7.GetBody()->GetFixtureList()->SetDensity( 7.5500f );
	_wallBox7.GetBody()->GetFixtureList()->SetFriction( 0.9500f );
	_wallBox7.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox7.SetBodyType( b2_staticBody );

	// _wallBox8
	_wallBox8.SetRotation( 239.3002f );
	_wallBox8.GetBody()->GetFixtureList()->SetDensity( 7.5500f );
	_wallBox8.GetBody()->GetFixtureList()->SetFriction( 0.9000f );
	_wallBox8.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox8.SetBodyType( b2_staticBody );

	// _wallBox9
	_wallBox9.SetRotation( 250.3001f );
	_wallBox9.GetBody()->GetFixtureList()->SetDensity( 7.5500f );
	_wallBox9.GetBody()->GetFixtureList()->SetFriction( 0.9000f );
	_wallBox9.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox9.SetBodyType( b2_staticBody );

	// _wallBox10
	_wallBox10.SetRotation( 112.0000f );
	_wallBox10.GetBody()->GetFixtureList()->SetDensity( 7.5500f );
	_wallBox10.GetBody()->GetFixtureList()->SetFriction( 0.9000f );
	_wallBox10.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox10.SetBodyType( b2_staticBody );

	// _wallBox11
	_wallBox11.SetRotation( 123.0000f );
	_wallBox11.GetBody()->GetFixtureList()->SetDensity( 7.5500f );
	_wallBox11.GetBody()->GetFixtureList()->SetFriction( 0.9000f );
	_wallBox11.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox11.SetBodyType( b2_staticBody );

	// _wallBox12
	_wallBox12.SetRotation( 134.0000f );
	_wallBox12.GetBody()->GetFixtureList()->SetDensity( 7.5500f );
	_wallBox12.GetBody()->GetFixtureList()->SetFriction( 0.9000f );
	_wallBox12.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox12.SetBodyType( b2_staticBody );

	// _wallBox13
	_wallBox13.SetRotation( 145.0000f );
	_wallBox13.GetBody()->GetFixtureList()->SetDensity( 7.5500f );
	_wallBox13.GetBody()->GetFixtureList()->SetFriction( 0.9000f );
	_wallBox13.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox13.SetBodyType( b2_staticBody );

	// _wallBox14
	_wallBox14.SetRotation( 156.0001f );
	_wallBox14.GetBody()->GetFixtureList()->SetDensity( 7.5500f );
	_wallBox14.GetBody()->GetFixtureList()->SetFriction( 0.9000f );
	_wallBox14.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox14.SetBodyType( b2_staticBody );

	// _wallBox15
	_wallBox15.SetRotation( 167.0001f );
	_wallBox15.GetBody()->GetFixtureList()->SetDensity( 7.5500f );
	_wallBox15.GetBody()->GetFixtureList()->SetFriction( 0.9000f );
	_wallBox15.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox15.SetBodyType( b2_staticBody );

	// _wallBox16
	_wallBox16.SetRotation( 178.0001f );
	_wallBox16.GetBody()->GetFixtureList()->SetDensity( 7.5500f );
	_wallBox16.GetBody()->GetFixtureList()->SetFriction( 0.9000f );
	_wallBox16.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox16.SetBodyType( b2_staticBody );

	// _wallBox17
	_wallBox17.SetRotation( 189.0001f );
	_wallBox17.GetBody()->GetFixtureList()->SetDensity( 7.5500f );
	_wallBox17.GetBody()->GetFixtureList()->SetFriction( 0.9000f );
	_wallBox17.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox17.SetBodyType( b2_staticBody );

	// _wallBox18
	_wallBox18.SetRotation( 193.8000f );
	_wallBox18.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox18.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox18.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox18.SetBodyType( b2_staticBody );


	//Set blocks positions
	_bomb1.SetPosition( 67.3011f, 34.1998f );
	_boxerGlove1.SetPosition( 77.6004f, 49.4500f );
	_coin1.SetPosition( 67.3009f, 49.4500f );
	_coin2.SetPosition( 45.3006f, 10.6499f );
	_fragileBox1.SetPosition( 40.8001f, 44.2499f );
	_fragileBox2.SetPosition( 35.4001f, 34.3499f );
	_fragileBox3.SetPosition( 46.0001f, 34.2499f );
	_piggyBank1.SetPosition( 40.6001f, 55.3497f );
	_wallBox1.SetPosition( 67.3009f, 44.4999f );
	_wallBox2.SetPosition( 47.7000f, 28.0000f );
	_wallBox3.SetPosition( 59.5849f, 7.1022f );
	_wallBox4.SetPosition( 64.5468f, 9.7243f );
	_wallBox5.SetPosition( 16.0282f, 27.0644f );
	_wallBox6.SetPosition( 69.0045f, 12.9004f );
	_wallBox7.SetPosition( 73.0486f, 16.6448f );
	_wallBox8.SetPosition( 76.3039f, 21.0924f );
	_wallBox9.SetPosition( 78.6504f, 26.0791f );
	_wallBox10.SetPosition( 17.5934f, 21.7801f );
	_wallBox11.SetPosition( 20.1382f, 16.8917f );
	_wallBox12.SetPosition( 23.5691f, 12.5786f );
	_wallBox13.SetPosition( 27.7599f, 8.9994f );
	_wallBox14.SetPosition( 32.5565f, 6.2856f );
	_wallBox15.SetPosition( 37.7829f, 4.5369f );
	_wallBox16.SetPosition( 43.2469f, 3.8176f );
	_wallBox17.SetPosition( 48.7477f, 4.1540f );
	_wallBox18.SetPosition( 54.0942f, 5.2603f );

	//Bombs Construction
	_bomb1.SetRange( 16.0000f );
	_bomb1.SetBombImpulse( 8000.0000f );

	//Boxer Gloves Construction
	_boxerGlove1.SetBoxerForce( 41000.0000f );

	ConstFinal();
}
//--------------------------------------------------------------------------
LevelRotateL::LevelRotateL( GameTypes::LevelEnum levelEnum, bool viewMode ) : LevelVertical( levelEnum, viewMode )
{
    _levelTag = eLevelTag_Thief;
	_coinCount = 2;

	// _coin1
	_coin1.SetRotation( 90.0000f );
	_coin1.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin1.GetBody()->GetFixtureList()->SetFriction( 0.9000f );
	_coin1.GetBody()->GetFixtureList()->SetRestitution( 0.0500f );
	_coin1.GetBody()->SetAngularDamping(0.2500f );
	_coin1.GetBody()->SetLinearDamping(0.2500f );
	_coin1.SetBodyType( b2_dynamicBody );

	// _coin2
	_coin2.SetRotation( 90.0000f );
	_coin2.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin2.GetBody()->GetFixtureList()->SetFriction( 0.9000f );
	_coin2.GetBody()->GetFixtureList()->SetRestitution( 0.0500f );
	_coin2.GetBody()->SetAngularDamping(0.2500f );
	_coin2.GetBody()->SetLinearDamping(0.2500f );
	_coin2.SetBodyType( b2_dynamicBody );

	// _fragileBox1
	_fragileBox1.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox1.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox1.GetBody()->GetFixtureList()->SetRestitution( 0.0500f );
	_fragileBox1.SetBodyType( b2_dynamicBody );

	// _fragileBox2
	_fragileBox2.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox2.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox2.GetBody()->GetFixtureList()->SetRestitution( 0.0500f );
	_fragileBox2.SetBodyType( b2_dynamicBody );

	// _fragileBox3
	_fragileBox3.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox3.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox3.GetBody()->GetFixtureList()->SetRestitution( 0.0500f );
	_fragileBox3.SetBodyType( b2_dynamicBody );

	// _fragileBox4
	_fragileBox4.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox4.GetBody()->GetFixtureList()->SetFriction( 0.9000f );
	_fragileBox4.GetBody()->GetFixtureList()->SetRestitution( 0.0500f );
	_fragileBox4.SetBodyType( b2_staticBody );

	// _fragileBox5
	_fragileBox5.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox5.GetBody()->GetFixtureList()->SetFriction( 0.9000f );
	_fragileBox5.GetBody()->GetFixtureList()->SetRestitution( 0.0500f );
	_fragileBox5.SetBodyType( b2_staticBody );

	// _fragileBox6
	_fragileBox6.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox6.GetBody()->GetFixtureList()->SetFriction( 0.9000f );
	_fragileBox6.GetBody()->GetFixtureList()->SetRestitution( 0.0500f );
	_fragileBox6.SetBodyType( b2_staticBody );

	// _fragileBox7
	_fragileBox7.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox7.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox7.GetBody()->GetFixtureList()->SetRestitution( 0.0500f );
	_fragileBox7.SetBodyType( b2_dynamicBody );

	// _fragileBox8
	_fragileBox8.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox8.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox8.GetBody()->GetFixtureList()->SetRestitution( 0.0500f );
	_fragileBox8.SetBodyType( b2_dynamicBody );

	// _fragileBox9
	_fragileBox9.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox9.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox9.GetBody()->GetFixtureList()->SetRestitution( 0.0500f );
	_fragileBox9.SetBodyType( b2_dynamicBody );

	// _fragileBox10
	_fragileBox10.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox10.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox10.GetBody()->GetFixtureList()->SetRestitution( 0.0500f );
	_fragileBox10.SetBodyType( b2_dynamicBody );

	// _fragileBox11
	_fragileBox11.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox11.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox11.GetBody()->GetFixtureList()->SetRestitution( 0.0500f );
	_fragileBox11.SetBodyType( b2_dynamicBody );

	// _fragileBox12
	_fragileBox12.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBox12.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBox12.GetBody()->GetFixtureList()->SetRestitution( 0.0500f );
	_fragileBox12.SetBodyType( b2_dynamicBody );

	// _l1
	_l1.SetRotation( 0.0f );
	_l1.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_l1.GetBody()->GetFixtureList()->SetFriction( 0.9000f );
	_l1.GetBody()->GetFixtureList()->SetRestitution( 0.0500f );
	_l1.SetBodyType( b2_dynamicBody );

	// _piggyBank1
	_piggyBank1.FlipX();
	_piggyBank1.SetRotation( 90.0f );
	_piggyBank1.GetBody()->GetFixtureList()->SetDensity( 7.5000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_piggyBank1.GetBody()->SetAngularDamping(0.8500f );
	_piggyBank1.GetBody()->SetLinearDamping(0.0500f );
	_piggyBank1.SetBodyType( b2_dynamicBody );

	// _thief1
	_thief1.SetRotation( 90.0000f );
	_thief1.GetBody()->GetFixtureList()->SetDensity( 3.3000f );
	_thief1.GetBody()->GetFixtureList()->SetFriction( 0.9000f );
	_thief1.GetBody()->GetFixtureList()->SetRestitution( 0.0500f );
	_thief1.SetBodyType( b2_dynamicBody );

	// _wallBox1
	_wallBox1.SetRotation( 90.0000f );
	_wallBox1.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox1.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox1.SetBodyType( b2_staticBody );


	//Set blocks positions
	_coin1.SetPosition( 4.8399f, 32.0000f );
	_coin2.SetPosition( 41.0898f, 32.0000f );
	_fragileBox1.SetPosition( 69.2502f, 30.1001f );
	_fragileBox2.SetPosition( 78.4001f, 34.1002f );
	_fragileBox3.SetPosition( 69.2502f, 39.3501f );
	_fragileBox4.SetPosition( 13.5500f, 32.0000f );
	_fragileBox5.SetPosition( 32.5500f, 32.0000f );
	_fragileBox6.SetPosition( 49.6999f, 32.0000f );
	_fragileBox7.SetPosition( 87.5501f, 21.6000f );
	_fragileBox8.SetPosition( 87.5501f, 49.0001f );
	_fragileBox9.SetPosition( 78.4001f, 25.1000f );
	_fragileBox10.SetPosition( 87.5501f, 30.6001f );
	_fragileBox11.SetPosition( 87.5501f, 39.8001f );
	_fragileBox12.SetPosition( 78.4001f, 43.0001f );
	_l1.SetPosition( 59.5500f, 32.0000f );
	_piggyBank1.SetPosition( 86.15000f, 9.1000f );
	_thief1.SetPosition( 22.8999f, 31.8000f );
	_wallBox1.SetPosition( 93.1500f, 32.0001f );


	ConstFinal();
}
//--------------------------------------------------------------------------
LevelOthers_6::LevelOthers_6( GameTypes::LevelEnum levelEnum, bool viewMode ) : Level( levelEnum, viewMode )
{
    _levelTag = eLevelTag_Bombs;
	_coinCount = 2;

	// _wallBox1
	_wallBox1.SetRotation( 30.0000f );
	_wallBox1.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox1.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox1.SetBodyType( b2_staticBody );

	// _coin1
	_coin1.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin1.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin1.SetBodyType( b2_dynamicBody );

	// _bombStatic1
	_bombStatic1.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_bombStatic1.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_bombStatic1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bombStatic1.SetBodyType( b2_staticBody );

	// _wallBox2
	_wallBox2.SetRotation( 30.0000f );
	_wallBox2.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox2.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox2.SetBodyType( b2_staticBody );

	// _bombStatic2
	_bombStatic2.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_bombStatic2.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_bombStatic2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bombStatic2.SetBodyType( b2_staticBody );

	// _wallBox3
	_wallBox3.SetRotation( 30.0000f );
	_wallBox3.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox3.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox3.SetBodyType( b2_staticBody );

	// _coin2
	_coin2.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin2.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin2.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin2.SetBodyType( b2_dynamicBody );

	// _bombStatic3
	_bombStatic3.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_bombStatic3.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_bombStatic3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_bombStatic3.SetBodyType( b2_staticBody );

	// _button1
	_button1.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_button1.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_button1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_button1.SetBodyType( b2_dynamicBody );

	// _piggyBank1
	_piggyBank1.FlipX();
	_piggyBank1.GetBody()->GetFixtureList()->SetDensity( 7.5000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_piggyBank1.GetBody()->SetAngularDamping(0.0500f );
	_piggyBank1.GetBody()->SetLinearDamping(0.0500f );
	_piggyBank1.SetBodyType( b2_dynamicBody );

	// _wallBox4
	_wallBox4.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox4.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox4.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox4.SetBodyType( b2_staticBody );


	//Set blocks positions
	_wallBox1.SetPosition( 33.3501f, 43.7502f );
	_coin1.SetPosition( 27.4501f, 47.3004f );
	_bombStatic1.SetPosition( 20.6001f, 39.8502f );
	_wallBox2.SetPosition( 54.0502f, 43.7502f );
	_bombStatic2.SetPosition( 41.3001f, 39.8502f );
	_wallBox3.SetPosition( 74.7507f, 43.7502f );
	_coin2.SetPosition( 68.8505f, 47.3004f );
	_bombStatic3.SetPosition( 62.0002f, 39.8502f );
	_button1.SetPosition( 48.4001f, 47.3003f );
	_piggyBank1.SetPosition( 21.9501f, 19.0499f );
	_wallBox4.SetPosition( 48.0000f, 10.3500f );

	//Bombs Construction
	_bombStatic1.SetRange( 12.6000f );
	_bombStatic1.SetBombImpulse( 5700.0000f );
	_bombStatic2.SetRange( 12.6000f );
	_bombStatic2.SetBombImpulse( 8850.0000f );
	_bombStatic3.SetRange( 12.6000f );
	_bombStatic3.SetBombImpulse( 5700.0000f );

	ConstFinal();
}
//--------------------------------------------------------------------------
LevelOthers_7::LevelOthers_7( GameTypes::LevelEnum levelEnum, bool viewMode ) : LevelVertical( levelEnum, viewMode )
{
    _levelTag = eLevelTag_Thief;
    
	// _coin1
	_coin1.SetRotation( 90.0000f );
	_coin1.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin1.GetBody()->GetFixtureList()->SetFriction( 0.2500f );
	_coin1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin1.GetBody()->SetAngularDamping(0.1000f );
	_coin1.GetBody()->SetLinearDamping(0.1000f );
	_coin1.SetBodyType( b2_dynamicBody );

	// _fragileBoxStatic1
	_fragileBoxStatic1.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBoxStatic1.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBoxStatic1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBoxStatic1.SetBodyType( b2_staticBody );

	// _fragileBoxStatic2
	_fragileBoxStatic2.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBoxStatic2.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBoxStatic2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBoxStatic2.SetBodyType( b2_staticBody );

	// _fragileBoxStatic3
	_fragileBoxStatic3.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBoxStatic3.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBoxStatic3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBoxStatic3.SetBodyType( b2_staticBody );

	// _fragileBoxStatic4
	_fragileBoxStatic4.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBoxStatic4.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBoxStatic4.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBoxStatic4.SetBodyType( b2_staticBody );

	// _fragileBoxStatic5
	_fragileBoxStatic5.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBoxStatic5.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBoxStatic5.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBoxStatic5.SetBodyType( b2_staticBody );

	// _fragileBoxStatic6
	_fragileBoxStatic6.GetBody()->GetFixtureList()->SetDensity( 9.0000f );
	_fragileBoxStatic6.GetBody()->GetFixtureList()->SetFriction( 0.7000f );
	_fragileBoxStatic6.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_fragileBoxStatic6.SetBodyType( b2_staticBody );

	// _piggyBank1
	_piggyBank1.SetRotation( 90.0000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetDensity( 7.5000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_piggyBank1.SetBodyType( b2_dynamicBody );

	// _thief1
	_thief1.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_thief1.GetBody()->GetFixtureList()->SetFriction( 0.2500f );
	_thief1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_thief1.GetBody()->SetAngularDamping(0.1000f );
	_thief1.GetBody()->SetLinearDamping(0.1000f );
	_thief1.FlipX();
	_thief1.SetRotation( 90.0000f );
	_thief1.SetBodyType( b2_dynamicBody );

	// _thief2
	_thief2.SetRotation( 90.0000f );
	_thief2.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_thief2.GetBody()->GetFixtureList()->SetFriction( 0.2500f );
	_thief2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_thief2.GetBody()->SetAngularDamping(0.1000f );
	_thief2.GetBody()->SetLinearDamping(0.1000f );
	_thief2.SetBodyType( b2_dynamicBody );

	// _wallBox1
	_wallBox1.SetRotation( 90.0000f );
	_wallBox1.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox1.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox1.SetBodyType( b2_staticBody );

	// _woodBox1
	_woodBox1.SetRotation( 90.0000f );
	_woodBox1.GetBody()->GetFixtureList()->SetDensity( 5.5000f );
	_woodBox1.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_woodBox1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_woodBox1.SetBodyType( b2_dynamicBody );


	//Set blocks positions
	_coin1.SetPosition( 15.1002f, 32.0001f );
	_fragileBoxStatic1.SetPosition( 46.7003f, 14.0001f );
	_fragileBoxStatic2.SetPosition( 27.5003f, 14.0001f );
	_fragileBoxStatic3.SetPosition( 37.1002f, 14.0001f );
	_fragileBoxStatic4.SetPosition( 46.7003f, 50.0003f );
	_fragileBoxStatic5.SetPosition( 37.1004f, 50.0003f );
	_fragileBoxStatic6.SetPosition( 27.5003f, 50.0003f );
	_piggyBank1.SetPosition( 78.7506f, 32.0000f );
	_thief1.SetPosition( 15.1002f, 21.4999f );
	_thief2.SetPosition( 15.1002f, 42.4998f );
	_wallBox1.SetPosition( 86.9503f, 32.0000f );
	_woodBox1.SetPosition( 21.3499f, 32.0000f );

	ConstFinal();
}
//--------------------------------------------------------------------------
