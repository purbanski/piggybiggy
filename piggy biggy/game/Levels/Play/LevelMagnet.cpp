#include "LevelMagnet.h"

//--------------------------------------------------------------------------
LevelMagnet_2::LevelMagnet_2( GameTypes::LevelEnum levelEnum, bool viewMode ) : LevelVertical( levelEnum, viewMode )
{
    _levelTag = eLevelTag_MagnetBomb;
	_coinCount = 8;

	// _floor11
	_floor11.SetRotation( 90.0000f );
	_floor11.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_floor11.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_floor11.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_floor11.SetBodyType( b2_staticBody );

	// _magnetBombPlus1
	_magnetBombPlus1.SetRotation( 90.0000f );
	_magnetBombPlus1.GetBody()->GetFixtureList()->SetDensity( 1.1000f );
	_magnetBombPlus1.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_magnetBombPlus1.GetBody()->GetFixtureList()->SetRestitution( 0.0000f );
	_magnetBombPlus1.GetBody()->SetAngularDamping(0.1000f );
	_magnetBombPlus1.GetBody()->SetLinearDamping(0.1000f );
	_magnetBombPlus1.SetBodyType( b2_dynamicBody );

	// _magnetBombMinus1
	_magnetBombMinus1.SetRotation( 90.0000f );
	_magnetBombMinus1.GetBody()->GetFixtureList()->SetDensity( 1.1000f );
	_magnetBombMinus1.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_magnetBombMinus1.GetBody()->GetFixtureList()->SetRestitution( 0.0000f );
	_magnetBombMinus1.GetBody()->SetAngularDamping(0.1000f );
	_magnetBombMinus1.GetBody()->SetLinearDamping(0.1000f );
	_magnetBombMinus1.SetBodyType( b2_dynamicBody );

	// _magnetBombPlus2
	_magnetBombPlus2.SetRotation( 90.0000f );
	_magnetBombPlus2.GetBody()->GetFixtureList()->SetDensity( 1.1000f );
	_magnetBombPlus2.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_magnetBombPlus2.GetBody()->GetFixtureList()->SetRestitution( 0.0000f );
	_magnetBombPlus2.GetBody()->SetAngularDamping(0.1000f );
	_magnetBombPlus2.GetBody()->SetLinearDamping(0.1000f );
	_magnetBombPlus2.SetBodyType( b2_dynamicBody );

	// _piggyBank1
	_piggyBank1.SetRotation( 90.0000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetDensity( 0.3000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetRestitution( 0.0000f );
	_piggyBank1.SetBodyType( b2_dynamicBody );

	// _magnetBomb1
	_magnetBomb1.SetRotation( 90.0000f );
	_magnetBomb1.GetBody()->GetFixtureList()->SetDensity( 1.1000f );
	_magnetBomb1.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_magnetBomb1.GetBody()->GetFixtureList()->SetRestitution( 0.0000f );
	_magnetBomb1.GetBody()->SetAngularDamping(0.1000f );
	_magnetBomb1.GetBody()->SetLinearDamping(0.1000f );
	_magnetBomb1.SetBodyType( b2_dynamicBody );

	// _coin1
	_coin1.SetRotation( 88.5000f );
	_coin1.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin1.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin1.SetBodyType( b2_dynamicBody );

	// _coin2
	_coin2.SetRotation( 88.5000f );
	_coin2.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin2.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin2.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin2.SetBodyType( b2_dynamicBody );

	// _coin3
	_coin3.SetRotation( 88.5000f );
	_coin3.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin3.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin3.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin3.SetBodyType( b2_dynamicBody );

	// _coin4
	_coin4.SetRotation( 88.5000f );
	_coin4.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin4.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin4.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin4.SetBodyType( b2_dynamicBody );

	// _coin5
	_coin5.SetRotation( 90.0000f );
	_coin5.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin5.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin5.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin5.SetBodyType( b2_dynamicBody );

	// _coin6
	_coin6.SetRotation( 90.000f );
	_coin6.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin6.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin6.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin6.SetBodyType( b2_dynamicBody );

	// _coin7
	_coin7.SetRotation( 90.000f );
	_coin7.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin7.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin7.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin7.SetBodyType( b2_dynamicBody );

	// _coin8
	_coin8.SetRotation( 90.000f );
	_coin8.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin8.GetBody()->GetFixtureList()->SetFriction( 0.6000f );
	_coin8.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_coin8.SetBodyType( b2_dynamicBody );

	// _circleWall1
	_circleWall1.SetRotation( 88.5000f );
	_circleWall1.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_circleWall1.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_circleWall1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_circleWall1.SetBodyType( b2_staticBody );

	// _circleWall2
	_circleWall2.SetRotation( 88.5000f );
	_circleWall2.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_circleWall2.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_circleWall2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_circleWall2.SetBodyType( b2_staticBody );

	// _magnetBombPlus3
	_magnetBombPlus3.SetRotation( 90.0000f );
	_magnetBombPlus3.GetBody()->GetFixtureList()->SetDensity( 1.1000f );
	_magnetBombPlus3.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_magnetBombPlus3.GetBody()->GetFixtureList()->SetRestitution( 0.0000f );
	_magnetBombPlus3.GetBody()->SetAngularDamping(0.1000f );
	_magnetBombPlus3.GetBody()->SetLinearDamping(0.1000f );
	_magnetBombPlus3.SetBodyType( b2_dynamicBody );

	// _magnetBomb2
	_magnetBomb2.SetRotation( 90.0000f );
	_magnetBomb2.GetBody()->GetFixtureList()->SetDensity( 1.1000f );
	_magnetBomb2.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_magnetBomb2.GetBody()->GetFixtureList()->SetRestitution( 0.0000f );
	_magnetBomb2.GetBody()->SetAngularDamping(0.1000f );
	_magnetBomb2.GetBody()->SetLinearDamping(0.1000f );
	_magnetBomb2.SetBodyType( b2_dynamicBody );


	//Set blocks positions
	_floor11.SetPosition( 45.5000f, 16.0000f );
	_magnetBombPlus1.SetPosition( 33.6998f, 16.0000f );
	_magnetBombMinus1.SetPosition( 38.3497f, 16.0000f );
	_magnetBombPlus2.SetPosition( 24.5501f, 16.0000f );
	_piggyBank1.SetPosition( 14.5501f, 16.0000f );
	_magnetBomb1.SetPosition( 29.1501f, 16.0000f );
	
	_coin8.SetPosition( -18.5f, 16.0f );
	_coin7.SetPosition( -14.5f, 16.0000f );
	_coin5.SetPosition( -10.5f, 16.0000f );
	_coin1.SetPosition( -6.5f, 16.0000f );
	_coin3.SetPosition( -2.5f, 16.0000f );
	_coin2.SetPosition( 1.5f, 16.0000f );
	_coin4.SetPosition( 5.5f, 16.0000f );
	_coin6.SetPosition( 09.5f, 16.0000f );

	_magnetBombPlus3.SetPosition( 42.8000f, 16.0000f );
	_magnetBomb2.SetPosition( 20.1501f, 16.0000f );

	//Bombs Construction
	_magnetBombPlus1.SetRange( 2.0f * 9.0000f );
	_magnetBombPlus1.SetBombImpulse( 6.0f * 7500.0000f );
	_magnetBombMinus1.SetRange( 2.0f * 9.0000f );
	_magnetBombMinus1.SetBombImpulse( 6.0f * 7500.0000f );
	_magnetBombPlus2.SetRange( 2.0f * 9.0000f );
	_magnetBombPlus2.SetBombImpulse( 6.0f * 7500.0000f );
	_magnetBomb1.SetRange( 2.0f * 9.0000f );
	_magnetBomb1.SetBombImpulse( 6.0f * 7500.0000f );
	_magnetBombPlus3.SetRange( 2.0f * 9.0000f );
	_magnetBombPlus3.SetBombImpulse( 6.0f * 7500.0000f );
	_magnetBomb2.SetRange( 2.0f * 9.0000f );
	_magnetBomb2.SetBombImpulse( 6.0f * 7500.0000f );



	//Magnet Construction
	_magnetBombPlus1.SetRange( 2.0f * 9.0000f );
	_magnetBombPlus1.SetMagnetImpulse( 6.0f * 2050.0000f );
	_magnetBombMinus1.SetRange( 2.0f * 9.0000f );
	_magnetBombMinus1.SetMagnetImpulse( 6.0f * 2050.0000f );
	_magnetBombPlus2.SetRange( 2.0f * 9.0000f );
	_magnetBombPlus2.SetMagnetImpulse( 6.0f * 2050.0000f );
	_magnetBomb1.SetRange( 2.0f * 9.0000f );
	_magnetBomb1.SetMagnetImpulse( 6.0f * 2050.0000f );
	_magnetBombPlus3.SetRange( 2.0f * 9.0000f );
	_magnetBombPlus3.SetMagnetImpulse( 6.0f * 2050.0000f );
	_magnetBomb2.SetRange( 2.0f * 9.0000f );
	_magnetBomb2.SetMagnetImpulse( 6.0f * 2050.0000f );

	ConstFinal();
	AdjustOldScreen();

	_circleWall1.SetPosition( 21.15f, 37.35f );
	_circleWall2.SetPosition( 21.15f, 26.65f );

	CreateJoints();

	_border.DestroyBody();
}
//--------------------------------------------------------------------------
void LevelMagnet_2::CreateJoints()
{
	SetJoint( _magnetBomb1 );
	SetJoint( _magnetBomb2 );
	SetJoint( _magnetBombPlus1 );
	SetJoint( _magnetBombPlus2 );
	SetJoint( _magnetBombPlus3 );
	SetJoint( _magnetBombMinus1 );

	SetJoint( _piggyBank1 );
	SetJoint( _coin6 );
	SetJoint( _coin7 );
	SetJoint( _coin4 );
	SetJoint( _coin2 );
	SetJoint( _coin3 );
	SetJoint( _coin1 );
	SetJoint( _coin5 );
	SetJoint( _coin8 );
}
//--------------------------------------------------------------------------
void LevelMagnet_2::SetJoint( puBlock &block )
{
	b2PrismaticJointDef jointDef;
	jointDef.collideConnected = true;
	jointDef.Initialize( block.GetBody(), _floor11.GetBody(), block.GetPosition(), b2Vec2( -1.0f, 0.0f ) );
	
	b2PrismaticJoint *joint = (b2PrismaticJoint *) _world.CreateJoint( &jointDef );
	joint->EnableMotor( true );
	joint->SetMotorSpeed( 0.0000f );
	joint->SetMaxMotorForce( 0.0000f );
}
//--------------------------------------------------------------------------





//--------------------------------------------------------------------------
LevelMagnet_1::LevelMagnet_1( GameTypes::LevelEnum levelEnum, bool viewMode ) : Level( levelEnum, viewMode )
{
    _levelTag = eLevelTag_MagnetBomb;
    
	// _coin1
	_coin1.GetBody()->GetFixtureList()->SetDensity( 3.5000f );
	_coin1.GetBody()->GetFixtureList()->SetFriction( 0.3500f );
	_coin1.GetBody()->GetFixtureList()->SetRestitution( 0.0500f );
	_coin1.SetBodyType( b2_dynamicBody );

	// _magnetBomb1
	_magnetBomb1.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_magnetBomb1.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_magnetBomb1.GetBody()->GetFixtureList()->SetRestitution( 0.0500f );
	_magnetBomb1.SetBodyType( b2_dynamicBody );

	// _magnetBomb2
	_magnetBomb2.GetBody()->GetFixtureList()->SetDensity( 6.5000f );
	_magnetBomb2.GetBody()->GetFixtureList()->SetFriction( 0.5000f );
	_magnetBomb2.GetBody()->GetFixtureList()->SetRestitution( 0.0500f );
	_magnetBomb2.SetBodyType( b2_dynamicBody );

	// _wallBox1
	_wallBox1.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox1.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox1.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox1.SetBodyType( b2_staticBody );

	// _chain1
	_chain1.SetChain( 16.0000f);
	_chain1.SetRotation( 330.0001f );
	_chain1.FixHook( false );

	// _chain2
	_chain2.SetChain( 16.0000f);
	_chain2.SetRotation( 210.0001f );
	_chain2.FixHook( false );

	// _chain3
	_chain3.SetChain( 16.0000f);
	_chain3.SetRotation( 270.0000f );
	_chain3.FixHook( false );

	// _wallBox2
	_wallBox2.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox2.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox2.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox2.SetBodyType( b2_staticBody );

	// _wallBox3
	_wallBox3.SetRotation( 183.0000f );
	_wallBox3.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox3.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox3.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox3.SetBodyType( b2_staticBody );

	// _wallBox4
	_wallBox4.SetRotation( 205.4999f );
	_wallBox4.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox4.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox4.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox4.SetBodyType( b2_staticBody );

	// _wallBox5
	_wallBox5.SetRotation( 194.2501f );
	_wallBox5.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox5.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox5.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox5.SetBodyType( b2_staticBody );

	// _wallBox6
	_wallBox6.SetRotation( 228.0000f );
	_wallBox6.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox6.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox6.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox6.SetBodyType( b2_staticBody );

	// _wallBox7
	_wallBox7.SetRotation( 216.7500f );
	_wallBox7.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox7.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox7.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox7.SetBodyType( b2_staticBody );

	// _wallBox8
	_wallBox8.SetRotation( 166.7499f );
	_wallBox8.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox8.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox8.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox8.SetBodyType( b2_staticBody );

	// _wallBox9
	_wallBox9.SetRotation( 204.6000f );
	_wallBox9.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox9.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox9.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox9.SetBodyType( b2_staticBody );

	// _wallBox10
	_wallBox10.SetRotation( 6.9000f );
	_wallBox10.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox10.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox10.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox10.SetBodyType( b2_staticBody );

	// _wallBox11
	_wallBox11.SetRotation( 221.2500f );
	_wallBox11.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox11.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox11.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox11.SetBodyType( b2_staticBody );

	// _wallBox12
	_wallBox12.SetRotation( 209.9999f );
	_wallBox12.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox12.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox12.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox12.SetBodyType( b2_staticBody );

	// _wallBox13
	_wallBox13.SetRotation( 187.4999f );
	_wallBox13.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox13.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox13.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox13.SetBodyType( b2_staticBody );

	// _wallBox14
	_wallBox14.SetRotation( 198.7500f );
	_wallBox14.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox14.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox14.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox14.SetBodyType( b2_staticBody );

	// _wallBox15
	_wallBox15.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox15.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox15.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox15.SetBodyType( b2_staticBody );

	// _wallBox16
	_wallBox16.SetRotation( 367.6498f );
	_wallBox16.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox16.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox16.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox16.SetBodyType( b2_staticBody );

	// _wallBox17
	_wallBox17.SetRotation( 378.8999f );
	_wallBox17.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox17.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox17.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox17.SetBodyType( b2_staticBody );

	// _wallBox18
	_wallBox18.SetRotation( 356.3998f );
	_wallBox18.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox18.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox18.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox18.SetBodyType( b2_staticBody );

	// _wallBox19
	_wallBox19.SetRotation( 345.1498f );
	_wallBox19.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox19.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox19.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox19.SetBodyType( b2_staticBody );

	// _wallBox20
	_wallBox20.SetRotation( -18.0000f );
	_wallBox20.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox20.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox20.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox20.SetBodyType( b2_staticBody );

	// _wallBox21
	_wallBox21.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox21.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox21.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox21.SetBodyType( b2_staticBody );

	// _wallBox22
	_wallBox22.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox22.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox22.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox22.SetBodyType( b2_staticBody );

	// _wallBox23
	_wallBox23.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox23.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox23.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox23.SetBodyType( b2_staticBody );

	// _piggyBank1
	_piggyBank1.GetBody()->GetFixtureList()->SetDensity( 7.5000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetFriction( 0.4000f );
	_piggyBank1.GetBody()->GetFixtureList()->SetRestitution( 0.2000f );
	_piggyBank1.SetBodyType( b2_dynamicBody );

	// _wallBox24
	_wallBox24.SetRotation( 204.6000f );
	_wallBox24.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox24.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox24.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox24.SetBodyType( b2_staticBody );

	// _wallBox25
	_wallBox25.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox25.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox25.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox25.SetBodyType( b2_staticBody );

	// _wallBox26
	_wallBox26.GetBody()->GetFixtureList()->SetDensity( 8.0000f );
	_wallBox26.GetBody()->GetFixtureList()->SetFriction( 0.8000f );
	_wallBox26.GetBody()->GetFixtureList()->SetRestitution( 0.1000f );
	_wallBox26.SetBodyType( b2_staticBody );


	//Set blocks positions
	_coin1.SetPosition( 86.0510f, 46.2506f );
	_magnetBomb1.SetPosition( 50.0505f, 46.4510f );
	_magnetBomb2.SetPosition( 14.0501f, 46.4507f );
	_wallBox1.SetPosition( 86.0510f, 41.3507f );
	_chain1.SetPosition( 37.2328f, 53.8509f );
	_chain2.SetPosition( 62.8680f, 53.8510f );
	_chain3.SetPosition( 14.0501f, 61.2507f );
	_wallBox2.SetPosition( 6.5501f, 39.1001f );
	_wallBox3.SetPosition( 50.5350f, 16.2993f );
	_wallBox4.SetPosition( 61.8806f, 19.1807f );
	_wallBox5.SetPosition( 56.3496f, 17.1813f );
	_wallBox6.SetPosition( 71.2599f, 26.1843f );
	_wallBox7.SetPosition( 66.9152f, 22.2206f );
	_wallBox8.SetPosition( 44.6601f, 16.7687f );
	_wallBox9.SetPosition( 76.0673f, 29.7286f );
	_wallBox10.SetPosition( 81.5179f, 31.3260f );
	_wallBox11.SetPosition( 74.1591f, 10.4042f );
	_wallBox12.SetPosition( 69.3780f, 6.9785f );
	_wallBox13.SetPosition( 58.2926f, 3.2158f );
	_wallBox14.SetPosition( 64.0205f, 4.5513f );
	_wallBox15.SetPosition( 29.0499f, 25.3001f );
	_wallBox16.SetPosition( 26.9556f, 19.0664f );
	_wallBox17.SetPosition( 21.1814f, 17.7160f );
	_wallBox18.SetPosition( 32.8330f, 19.2742f );
	_wallBox19.SetPosition( 38.6381f, 18.3313f );
	_wallBox20.SetPosition( 7.0500f, 17.7000f );
	_wallBox21.SetPosition( 29.9998f, 2.8000f );
	_wallBox22.SetPosition( 6.5501f, 45.5001f );
	_wallBox23.SetPosition( 6.5501f, 51.9500f );
	_piggyBank1.SetPosition( 86.0501f, 23.1000f );
	_wallBox24.SetPosition( 79.0676f, 13.5286f );
	_wallBox25.SetPosition( 86.5499f, 14.6000f );
	_wallBox26.SetPosition( 88.1499f, 31.7000f );

	//Bombs Construction
	_magnetBomb1.SetRange( 17.3000f );
	_magnetBomb1.SetBombImpulse( 9500.0000f );
	_magnetBomb2.SetRange( 17.3000f );
	_magnetBomb2.SetBombImpulse( 17750.0000f );


	//Magnet Construction
	_magnetBomb1.SetRange( 17.3000f );
	_magnetBomb1.SetMagnetImpulse( 800.0000f );
	_magnetBomb2.SetRange( 17.3000f );
	_magnetBomb2.SetMagnetImpulse( 1000.0000f );


	//Set chains hooks
	_chain1.HookOnChain( &_magnetBomb1 );
	_chain2.HookOnChain( &_magnetBomb1 );
	_chain3.HookOnChain( &_magnetBomb2 );


	ConstFinal();
}
//--------------------------------------------------------------------------
