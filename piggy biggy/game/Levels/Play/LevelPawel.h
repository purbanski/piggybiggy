#ifndef __LEVELPAWEL_H__
#define __LEVELPAWEL_H__

#include <Box2D/Box2D.h>
#include "Levels/LevelWithCallback.h"
#include "Blocks/AllBlocks.h"

//------------------------------------------------------------------
class LevelPawel_1 : public LevelActivable
{
public:
	LevelPawel_1( GameTypes::LevelEnum levelEnum, bool viewMode );

private:
	puCoin< 35 >	_coin1;
	puFragileBox<96,96,eBlockScaled>	_fragileBox1;
	puFragileBox<96,96,eBlockScaled>	_fragileBox2;
	puFragileBox<96,96,eBlockScaled>	_fragileBox3;
	puFragileBox<96,96,eBlockScaled>	_fragileBox4;
	puFragileBox<96,96,eBlockScaled>	_fragileBox5;
	puFragileBox<96,96,eBlockScaled>	_fragileBox6;
	puFragileBox<96,96,eBlockScaled>	_fragileBox7;
	puPiggyBank< 50 >	_piggyBank1;
	puWallBox<100,20,eBlockScaled>	_wallBox1;
	puWallBox<100,20,eBlockScaled>	_wallBox2;
	puWallBox<160,20,eBlockScaled>	_wallBox3;
	puWallBox<20,20,eBlockScaled>	_wallBox4;
	puWallBox<250,20,eBlockScaled>	_wallBox5;
	puWallBox<35,20,eBlockScaled>	_wallBox10;
	puWallBox<35,20,eBlockScaled>	_wallBox11;
	puWallBox<35,20,eBlockScaled>	_wallBox12;
	puWallBox<35,20,eBlockScaled>	_wallBox13;
	puWallBox<35,20,eBlockScaled>	_wallBox14;
	puWallBox<35,20,eBlockScaled>	_wallBox15;
	puWallBox<35,20,eBlockScaled>	_wallBox6;
	puWallBox<35,20,eBlockScaled>	_wallBox7;
	puWallBox<35,20,eBlockScaled>	_wallBox8;
	puWallBox<35,20,eBlockScaled>	_wallBox9;
	puWallBox<480,20,eBlockScaled>	_wallBox16;
	puWoodBox<280,20,eBlockScaled>	_woodBox1;
};
//------------------------------------------------------------------
class LevelPawel_2 : public Level
{
public:
	LevelPawel_2( GameTypes::LevelEnum levelEnum, bool viewMode );

private:
	puBomb< 44 >	_bomb1;
	puBomb< 44 >	_bomb2;
	puBombStatic< 44 >	_bombStatic1;
	puBombStatic< 44 >	_bombStatic2;
	puBombStatic< 44 >	_bombStatic3;
	puCoin< 40 >	_coin1;
	puFragileBox<192,96,eBlockScaled>	_fragileBox1;
	puFragileBox<96,96,eBlockScaled>	_fragileBox2;
	puFragileBoxStatic<96,96,eBlockScaled>	_fragileBoxStatic1;
	puFragileBoxStatic<96,96,eBlockScaled>	_fragileBoxStatic2;
	puFragileBoxStatic<96,96,eBlockScaled>	_fragileBoxStatic3;
	puFragileBoxStatic<96,96,eBlockScaled>	_fragileBoxStatic4;
	puFragileBoxStatic<96,96,eBlockScaled>	_fragileBoxStatic5;
	puPiggyBank< 60 >	_piggyBank1;
	puWallBox<190,20,eBlockScaled>	_wallBox2;
	puWallBox<190,20,eBlockScaled>	_wallBox3;
	puWallBox<190,20,eBlockScaled>	_wallBox4;
	puWallBox<20,20,eBlockScaled>	_wallBox1;
	puWallBox<20,20,eBlockScaled>	_wallBox5;
	puWallBox<20,20,eBlockScaled>	_wallBox6;
	puWallBox<20,20,eBlockScaled>	_wallBox7;
	puWallBox<20,20,eBlockScaled>	_wallBox8;
	puWallBox<330,20,eBlockScaled>	_wallBox10;
	puWallBox<400,20,eBlockScaled>	_wallBox9;
	puWoodBox<220,20,eBlockScaled>	_woodBox1;
};
//------------------------------------------------------------------
class LevelPawel_3 : public Level
{
public:
	LevelPawel_3( GameTypes::LevelEnum levelEnum, bool viewMode );

private:
	puBomb< 44 >	_bomb1;
	puBomb< 44 >	_bomb2;
	puBomb< 44 >	_bomb3;
	puCoin< 40 >	_coin1;
	puFragileBox<96,96,eBlockScaled>	_fragileBox1;
	puFragileBox<96,96,eBlockScaled>	_fragileBox2;
	puFragileBoxStatic<96,96,eBlockScaled>	_fragileBoxStatic1;
	puFragileBoxStatic<96,96,eBlockScaled>	_fragileBoxStatic2;
	puFragileBoxStatic<96,96,eBlockScaled>	_fragileBoxStatic3;
	puPiggyBank< 60 >	_piggyBank1;
	puWallBox<100,20,eBlockScaled>	_wallBox2;
	puWallBox<70,20,eBlockScaled>	_wallBox3;
	puWallBox<860,20,eBlockScaled>	_wallBox1;
	puWoodBox<200,20,eBlockScaled>	_woodBox2;
	puWoodBox<240,20,eBlockScaled>	_woodBox1;
};
//------------------------------------------------------------------
class LevelPawel_7 : public Level
{
public:
	LevelPawel_7( GameTypes::LevelEnum levelEnum, bool viewMode );

private:
	puBombStatic< 44 >	_bombStatic1;
	puBombStatic< 44 >	_bombStatic2;
	puBombStatic< 44 >	_bombStatic3;
	puBombStatic< 44 >	_bombStatic4;
	puButton< 44 >	_button1;
	puCoin< 40 >	_coin1;
	puCoin< 40 >	_coin2;
	puFragileBox<192,96,eBlockScaled>	_fragileBox1;
	puFragileBox<96,96,eBlockScaled>	_fragileBox2;
	puFragileBoxStatic<96,96,eBlockScaled>	_fragileBoxStatic1;
	puFragileBoxStatic<96,96,eBlockScaled>	_fragileBoxStatic2;
	puPiggyBankStatic< 60 >	_piggyBankStatic1;
	puWallBox<100,20,eBlockScaled>	_wallBox1;
	puWallBox<120,20,eBlockScaled>	_wallBox2;
	puWallBox<20,20,eBlockScaled>	_wallBox10;
	puWallBox<20,20,eBlockScaled>	_wallBox11;
	puWallBox<20,20,eBlockScaled>	_wallBox3;
	puWallBox<20,20,eBlockScaled>	_wallBox4;
	puWallBox<20,20,eBlockScaled>	_wallBox5;
	puWallBox<20,20,eBlockScaled>	_wallBox6;
	puWallBox<20,20,eBlockScaled>	_wallBox7;
	puWallBox<20,20,eBlockScaled>	_wallBox8;
	puWallBox<20,20,eBlockScaled>	_wallBox9;
	puWallBox<200,20,eBlockScaled>	_wallBox12;
	puWallBox<300,20,eBlockScaled>	_wallBox13;
	puWallBox<50,20,eBlockScaled>	_wallBox14;
	puWoodBox<140,20,eBlockScaled>	_woodBox1;
	puWoodBox<160,20,eBlockScaled>	_woodBox2;
};
//------------------------------------------------------------------
class LevelPawel_4 : public Level
{
public:
	LevelPawel_4( GameTypes::LevelEnum levelEnum, bool viewMode );

private:
	puBomb< 44 >	_bomb1;
	puBomb< 44 >	_bomb2;
	puBomb< 44 >	_bomb3;
	puBomb< 44 >	_bomb4;
	puBomb< 44 >	_bomb5;
	puCoin< 40 >	_coin1;
	puCoin< 40 >	_coin2;
	puFragileBoxStatic<96,96,eBlockScaled>	_fragileBoxStatic1;
	puFragileBoxStatic<96,96,eBlockScaled>	_fragileBoxStatic2;
	puFragileBoxStatic<96,96,eBlockScaled>	_fragileBoxStatic3;
	puFragileBoxStatic<96,96,eBlockScaled>	_fragileBoxStatic4;
	puPiggyBank< 60 >	_piggyBank1;
	puWallBox<400,20,eBlockScaled>	_wallBox1;
	puWallBox<20,20,eBlockScaled>	_wallBox18;
	puWallBox<20,20,eBlockScaled>	_wallBox19;
	puWallBox<20,20,eBlockScaled>	_wallBox22;
	puWallBox<20,20,eBlockScaled>	_wallBox23;
	puWallBox<20,20,eBlockScaled>	_wallBox25;
	puWallBox<20,20,eBlockScaled>	_wallBox26;
	puWallBox<100,20,eBlockScaled>	_wallBox10;
	puWallBox<100,20,eBlockScaled>	_wallBox11;
	puWallBox<100,20,eBlockScaled>	_wallBox12;
	puWallBox<100,20,eBlockScaled>	_wallBox13;
	puWallBox<100,20,eBlockScaled>	_wallBox14;
	puWallBox<100,20,eBlockScaled>	_wallBox15;
	puWallBox<100,20,eBlockScaled>	_wallBox16;
	puWallBox<100,20,eBlockScaled>	_wallBox17;
	puWallBox<100,20,eBlockScaled>	_wallBox20;
	puWallBox<100,20,eBlockScaled>	_wallBox21;
	puWallBox<100,20,eBlockScaled>	_wallBox24;
	puWallBox<100,20,eBlockScaled>	_wallBox2;
	puWallBox<100,20,eBlockScaled>	_wallBox3;
	puWallBox<100,20,eBlockScaled>	_wallBox4;
	puWallBox<100,20,eBlockScaled>	_wallBox5;
	puWallBox<100,20,eBlockScaled>	_wallBox6;
	puWallBox<100,20,eBlockScaled>	_wallBox7;
	puWallBox<100,20,eBlockScaled>	_wallBox8;
	puWallBox<100,20,eBlockScaled>	_wallBox9;
	puWoodBox<160,20,eBlockScaled>	_woodBox1;
	puWoodBox<160,20,eBlockScaled>	_woodBox2;
	puWoodBox<160,20,eBlockScaled>	_woodBox3;
	puWoodBox<160,20,eBlockScaled>	_woodBox4;
};
//------------------------------------------------------------------
class LevelPawel_5 : public Level
{
public:
	LevelPawel_5( GameTypes::LevelEnum levelEnum, bool viewMode );

private:
	puBomb< 44 >	_bomb1;
	puBomb< 44 >	_bomb2;
	puBomb< 44 >	_bomb3;
	puBombStatic< 44 >	_bombStatic1;
	puBombStatic< 44 >	_bombStatic2;
	puBombStatic< 44 >	_bombStatic3;
	puCoin< 40 >	_coin1;
	puCoin< 40 >	_coin2;
	puCoin< 40 >	_coin3;
	puFragileBoxStatic<96,96,eBlockScaled>	_fragileBoxStatic1;
	puFragileBoxStatic<96,96,eBlockScaled>	_fragileBoxStatic2;
	puPiggyBankStatic< 60 >	_piggyBankStatic1;
	puWallBox<120,20,eBlockScaled>	_wallBox1;
	puWallBox<140,20,eBlockScaled>	_wallBox2;
	puWallBox<140,20,eBlockScaled>	_wallBox3;
	puWallBox<140,20,eBlockScaled>	_wallBox4;
	puWallBox<20,20,eBlockScaled>	_wallBox5;
	puWallBox<20,20,eBlockScaled>	_wallBox6;
	puWallBox<20,20,eBlockScaled>	_wallBox7;
	puWallBox<20,20,eBlockScaled>	_wallBox8;
	puWoodBox<160,20,eBlockScaled>	_woodBox1;
};
//------------------------------------------------------------------
class LevelPawel_6 : public Level
{
public:
	LevelPawel_6( GameTypes::LevelEnum levelEnum, bool viewMode );

private:
	puBomb< 44 >	_bomb1;
	puBomb< 44 >	_bomb2;
	puBomb< 44 >	_bomb3;
	puBomb< 44 >	_bomb4;
	puBomb< 44 >	_bomb5;
	puBomb< 44 >	_bomb6;
	puCoin< 40 >	_coin1;
	puCoin< 40 >	_coin2;
	puCoin< 40 >	_coin3;
	puFragileBoxStatic<96,96,eBlockScaled>	_fragileBoxStatic1;
	puFragileBoxStatic<96,96,eBlockScaled>	_fragileBoxStatic2;
	puFragileBoxStatic<96,96,eBlockScaled>	_fragileBoxStatic3;
	puPiggyBank< 60 >	_piggyBank1;
	puWallBox<200,20,eBlockScaled>	_wallBox10;
	puWallBox<200,20,eBlockScaled>	_wallBox2;
	puWallBox<200,20,eBlockScaled>	_wallBox3;
	puWallBox<200,20,eBlockScaled>	_wallBox5;
	puWallBox<200,20,eBlockScaled>	_wallBox7;
	puWallBox<100,20,eBlockScaled>	_wallBox11;
	puWallBox<100,20,eBlockScaled>	_wallBox12;
	puWallBox<100,20,eBlockScaled>	_wallBox1;
	puWallBox<100,20,eBlockScaled>	_wallBox4;
	puWallBox<100,20,eBlockScaled>	_wallBox6;
	puWallBox<100,20,eBlockScaled>	_wallBox8;
	puWallBox<100,20,eBlockScaled>	_wallBox9;
	puWoodBox<160,20,eBlockScaled>	_woodBox1;
	puWoodBox<160,20,eBlockScaled>	_woodBox2;
};

//------------------------------------------------------------------
class LevelPawel_8 : public Level
{
public:
	LevelPawel_8( GameTypes::LevelEnum levelEnum, bool viewMode );

private:
	puBomb< 44 >	_bomb1;
	puBombStatic< 44 >	_bombStatic1;
	puBombStatic< 44 >	_bombStatic2;
	puBombStatic< 44 >	_bombStatic3;
	puBombStatic< 44 >	_bombStatic4;
	puBombStatic< 44 >	_bombStatic5;
	puButton< 38 >	_button1;
	puCoin< 40 >	_coin1;
	puFragileBox<96,96,eBlockScaled>	_fragileBox1;
	puPiggyBank< 60 >	_piggyBank1;
	puWallBox<140,20,eBlockScaled>	_wallBox1;
	puWallBox<140,20,eBlockScaled>	_wallBox2;
	puWallBox<160,20,eBlockScaled>	_wallBox3;
	puWallBox<190,20,eBlockScaled>	_wallBox4;
	puWallBox<20,20,eBlockScaled>	_wallBox10;
	puWallBox<20,20,eBlockScaled>	_wallBox11;
	puWallBox<20,20,eBlockScaled>	_wallBox12;
	puWallBox<20,20,eBlockScaled>	_wallBox13;
	puWallBox<20,20,eBlockScaled>	_wallBox5;
	puWallBox<20,20,eBlockScaled>	_wallBox6;
	puWallBox<20,20,eBlockScaled>	_wallBox7;
	puWallBox<20,20,eBlockScaled>	_wallBox8;
	puWallBox<20,20,eBlockScaled>	_wallBox9;
	puWallBox<200,20,eBlockScaled>	_wallBox14;
	puWallBox<50,20,eBlockScaled>	_wallBox15;
	puWoodBox<200,20,eBlockScaled>	_woodBox1;
};
//------------------------------------------------------------------
class LevelPawel_9 : public Level
{
public:
	LevelPawel_9( GameTypes::LevelEnum levelEnum, bool viewMode );

private:
	puBomb< 44 >	_bomb1;
	puBomb< 44 >	_bomb2;
	puBomb< 44 >	_bomb3;
	puBomb< 44 >	_bomb4;
	puBomb< 44 >	_bomb5;
	puBomb< 44 >	_bomb6;
	puCircleWood< 40 >	_circleWood1;
	puCoin< 40 >	_coin1;
	puFragileBoxStatic<96,96,eBlockScaled>	_fragileBoxStatic1;
	puFragileBoxStatic<96,96,eBlockScaled>	_fragileBoxStatic2;
	puFragileBoxStatic<96,96,eBlockScaled>	_fragileBoxStatic3;
	puPiggyBank< 60 >	_piggyBank1;
	puWallBox<200,15,eBlockScaled>	_wallBox13;
	puWallBox<200,15,eBlockScaled>	_wallBox15;
	puWallBox<200,15,eBlockScaled>	_wallBox5;
	puWallBox<400,15,eBlockScaled>	_wallBox1;
	puWallBox<400,15,eBlockScaled>	_wallBox7;
	puWallBox<10,10,eBlockScaled>	_wallBox16;
	puWallBox<10,10,eBlockScaled>	_wallBox17;
	puWallBox<100,15,eBlockScaled>	_wallBox10;
	puWallBox<100,15,eBlockScaled>	_wallBox11;
	puWallBox<100,15,eBlockScaled>	_wallBox12;
	puWallBox<100,15,eBlockScaled>	_wallBox14;
	puWallBox<100,15,eBlockScaled>	_wallBox2;
	puWallBox<100,15,eBlockScaled>	_wallBox3;
	puWallBox<100,15,eBlockScaled>	_wallBox4;
	puWallBox<100,15,eBlockScaled>	_wallBox6;
	puWallBox<100,15,eBlockScaled>	_wallBox8;
	puWallBox<100,15,eBlockScaled>	_wallBox9;
};
//------------------------------------------------------------------
class LevelPawel_10 : public Level
{
public:
	LevelPawel_10( GameTypes::LevelEnum levelEnum, bool viewMode );

private:
	puBombStatic< 44 >	_bombStatic1;
	puBombStatic< 44 >	_bombStatic2;
	puBombStatic< 44 >	_bombStatic3;
	puBombStatic< 44 >	_bombStatic4;
	puBombStatic< 44 >	_bombStatic5;
	puChainCutable	_chain1;
	puCoin< 40 >	_coin1;
	puCoin< 40 >	_coin2;
	puFragileBoxStatic<96,96,eBlockScaled>	_fragileBoxStatic1;
	puFragileBoxStatic<96,96,eBlockScaled>	_fragileBoxStatic2;
	puPiggyBankStatic< 60 >	_piggyBankStatic1;
	puPiggyBankStatic< 60 >	_piggyBankStatic2;
	puWallBox<140,20,eBlockScaled>	_wallBox15;
	puWallBox<20,20,eBlockScaled>	_wallBox10;
	puWallBox<20,20,eBlockScaled>	_wallBox1;
	puWallBox<20,20,eBlockScaled>	_wallBox2;
	puWallBox<20,20,eBlockScaled>	_wallBox3;
	puWallBox<20,20,eBlockScaled>	_wallBox4;
	puWallBox<20,20,eBlockScaled>	_wallBox5;
	puWallBox<20,20,eBlockScaled>	_wallBox6;
	puWallBox<20,20,eBlockScaled>	_wallBox7;
	puWallBox<20,20,eBlockScaled>	_wallBox8;
	puWallBox<20,20,eBlockScaled>	_wallBox9;
	puWallBox<55,20,eBlockScaled>	_wallBox12;
	puWallBox<55,20,eBlockScaled>	_wallBox13;
	puWallBox<70,20,eBlockScaled>	_wallBox11;
	puWallBox<70,20,eBlockScaled>	_wallBox14;
	puWoodBox<160,20,eBlockScaled>	_woodBox1;
	puWoodBox<240,20,eBlockScaled>	_woodBox2;
	puWoodBox<240,20,eBlockScaled>	_woodBox3;
	puWoodBox<240,20,eBlockScaled>	_woodBox4;
};
//------------------------------------------------------------------
class LevelPawel_11 : public Level
{
public:
	LevelPawel_11( GameTypes::LevelEnum levelEnum, bool viewMode );

private:
	puBomb< 44 >	_bomb1;
	puBomb< 44 >	_bomb2;
	puBombStatic< 44 >	_bombStatic1;
	puBombStatic< 44 >	_bombStatic2;
	puBombStatic< 44 >	_bombStatic3;
	puBombStatic< 44 >	_bombStatic4;
	puButton< 55 >	_button1;
	puChainCutable	_chain1;
	puCoin< 40 >	_coin1;
	puFragileBoxStatic<96,96,eBlockScaled>	_fragileBoxStatic1;
	puFragileBoxStatic<96,96,eBlockScaled>	_fragileBoxStatic2;
	puPiggyBank< 60 >	_piggyBank1;
	puWallBox<140,20,eBlockScaled>	_wallBox6;
	puWallBox<20,20,eBlockScaled>	_wallBox1;
	puWallBox<20,20,eBlockScaled>	_wallBox2;
	puWallBox<20,20,eBlockScaled>	_wallBox3;
	puWallBox<20,20,eBlockScaled>	_wallBox4;
	puWallBox<20,20,eBlockScaled>	_wallBox8;
	puWallBox<20,20,eBlockScaled>	_wallBox9;
	puWallBox<330,20,eBlockScaled>	_wallBox5;
	puWallBox<500,20,eBlockScaled>	_wallBox7;
	puWoodBox<160,20,eBlockScaled>	_woodBox1;
};
//------------------------------------------------------------------
class LevelPawel_12 : public Level
{
public:
	LevelPawel_12( GameTypes::LevelEnum levelEnum, bool viewMode );

private:
	puBombStatic< 44 >	_bombStatic1;
	puBombStatic< 44 >	_bombStatic2;
	puBombStatic< 44 >	_bombStatic3;
	puBombStatic< 44 >	_bombStatic4;
	puButton< 38 >	_button1;
	puChainCutable	_chain1;
	puChainCutable	_chain2;
	puButton< 38 >	_circleFragile1;
	puCoin< 40 >	_coin1;
	puPiggyBank< 60 >	_piggyBank1;
	puWallBox<100,20,eBlockScaled>	_wallBox12;
	puWallBox<100,20,eBlockScaled>	_wallBox1;
	puWallBox<20,20,eBlockScaled>	_wallBox10;
	puWallBox<20,20,eBlockScaled>	_wallBox2;
	puWallBox<20,20,eBlockScaled>	_wallBox3;
	puWallBox<20,20,eBlockScaled>	_wallBox4;
	puWallBox<20,20,eBlockScaled>	_wallBox5;
	puWallBox<20,20,eBlockScaled>	_wallBox6;
	puWallBox<20,20,eBlockScaled>	_wallBox7;
	puWallBox<20,20,eBlockScaled>	_wallBox8;
	puWallBox<20,20,eBlockScaled>	_wallBox9;
	puWallBox<70,20,eBlockScaled>	_wallBox11;
	puWoodBox<140,20,eBlockScaled>	_woodBox1;
	puWoodBox<160,20,eBlockScaled>	_woodBox2;
	puWoodBox<160,20,eBlockScaled>	_woodBox3;
};
//------------------------------------------------------------------
class LevelPawel_13 : public Level
{
public:
	LevelPawel_13( GameTypes::LevelEnum levelEnum, bool viewMode );
	~LevelPawel_13();

private:
	void CreateJoints();

	puBomb< 44 >	_bomb1;
	puBomb< 44 >	_bomb2;
	puBombStatic< 44 >	_bombStatic1;
	puBombStatic< 44 >	_bombStatic2;
	puBombStatic< 44 >	_bombStatic3;
	puButton< 38 >	_button1;
	puCoin< 40 >	_coin1;
	puFragileBox<96,96,eBlockScaled>	_fragileBox1;
	puFragileBox<96,96,eBlockScaled>	_fragileBox2;
	puFragileBoxStatic<96,96,eBlockScaled>	_fragileBoxStatic1;
	puPiggyBank< 60 >	_piggyBank1;
	puScrew< 12 >	_screw2;
	puScrew< 12 >	_screw3;
	puScrew< 12 >	_screw1;
	puScrew< 12 >	_screw4;
	puWallBox<120,20,eBlockScaled>	_wallBox10;
	puWallBox<20,20,eBlockScaled>	_wallBox11;
	puWallBox<20,20,eBlockScaled>	_wallBox1;
	puWallBox<20,20,eBlockScaled>	_wallBox2;
	puWallBox<20,20,eBlockScaled>	_wallBox3;
	puWallBox<20,20,eBlockScaled>	_wallBox4;
	puWallBox<20,20,eBlockScaled>	_wallBox5;
	puWallBox<20,20,eBlockScaled>	_wallBox6;
	puWallBox<20,20,eBlockScaled>	_wallBox7;
	puWallBox<20,20,eBlockScaled>	_wallBox8;
	puWallBox<20,20,eBlockScaled>	_wallBox9;
	puWallBox<200,20,eBlockScaled>	_wallBox14;
	puWallBox<200,20,eBlockScaled>	_wallBox15;
	puWallBox<290,20,eBlockScaled>	_wallBox12;
	puWallBox<290,20,eBlockScaled>	_wallBox13;
	puWallBox<550,20,eBlockScaled>	_wallBox16;
	puWood2Box<100,20,eBlockScaled>	_woodBox14;
	puWood2Box<100,20,eBlockScaled>	_woodBox1;
	puWoodBox<340,20,eBlockScaled>	_woodBox13;
	puWoodBox<60,15,eBlockScaled>	_woodBox10;
	puWoodBox<60,15,eBlockScaled>	_woodBox11;
	puWoodBox<60,15,eBlockScaled>	_woodBox12;
	puWoodBox<60,15,eBlockScaled>	_woodBox2;
	puWoodBox<60,15,eBlockScaled>	_woodBox3;
	puWoodBox<60,15,eBlockScaled>	_woodBox4;
	puWoodBox<60,15,eBlockScaled>	_woodBox5;
	puWoodBox<60,15,eBlockScaled>	_woodBox6;
	puWoodBox<60,15,eBlockScaled>	_woodBox7;
	puWoodBox<60,15,eBlockScaled>	_woodBox8;
	puWoodBox<60,15,eBlockScaled>	_woodBox9;


	b2RevoluteJoint	*_jointRev1;
	b2RevoluteJoint	*_jointRev2;
	b2RevoluteJoint	*_jointRev3;
	b2RevoluteJoint	*_jointRev4;
	b2GearJoint	*_jointGear1;
};
//------------------------------------------------------------------
class LevelPawel_14 : public Level
{
public:
	LevelPawel_14( GameTypes::LevelEnum levelEnum, bool viewMode );

private:
	puBomb< 44 >	_bomb1;
	puBomb< 44 >	_bomb2;
	puBomb< 44 >	_bomb3;
	puBomb< 44 >	_bomb4;
	puBomb< 44 >	_bomb5;
	puBomb< 44 >	_bomb6;
	puBomb< 44 >	_bomb7;
	puBomb< 44 >	_bomb8;
	puChainCutable	_chain1;
	puChainCutable	_chain2;
	puCircleFragile< 40 >	_circleFragile1;
	puCircleFragile< 40 >	_circleFragile2;
	puCoin< 40 >	_coin1;
	puPiggyBank< 60 >	_piggyBank1;
	puWallBox<10,10,eBlockScaled>	_wallBox10;
	puWallBox<10,10,eBlockScaled>	_wallBox11;
	puWallBox<10,10,eBlockScaled>	_wallBox12;
	puWallBox<10,10,eBlockScaled>	_wallBox13;
	puWallBox<10,10,eBlockScaled>	_wallBox1;
	puWallBox<10,10,eBlockScaled>	_wallBox2;
	puWallBox<10,10,eBlockScaled>	_wallBox3;
	puWallBox<10,10,eBlockScaled>	_wallBox4;
	puWallBox<10,10,eBlockScaled>	_wallBox5;
	puWallBox<10,10,eBlockScaled>	_wallBox6;
	puWallBox<10,10,eBlockScaled>	_wallBox7;
	puWallBox<10,10,eBlockScaled>	_wallBox8;
	puWallBox<10,10,eBlockScaled>	_wallBox9;
	puWoodBox<160,15,eBlockScaled>	_woodBox1;
	puWoodBox<160,15,eBlockScaled>	_woodBox2;
	puWoodBox<160,15,eBlockScaled>	_woodBox3;
	puWoodBox<160,15,eBlockScaled>	_woodBox4;
};
//------------------------------------------------------------------
class LevelPawel_15 : public Level
{
public:
	LevelPawel_15( GameTypes::LevelEnum levelEnum, bool viewMode );

private:
	puBombStatic< 44 >	_bombStatic1;
	puBombStatic< 44 >	_bombStatic2;
	puBombStatic< 44 >	_bombStatic3;
	puBombStatic< 44 >	_bombStatic4;
	puBombStatic< 44 >	_bombStatic5;
	puBombStatic< 44 >	_bombStatic6;
	puCircleFragileStatic< 45 >	_circleFragileStatic1;
	puCircleFragileStatic< 45 >	_circleFragileStatic2;
	puCoin< 40 >	_coin1;
	puPiggyBankStatic< 50 >	_piggyBankStatic1;
	puWallBox<20,20,eBlockScaled>	_wallBox10;
	puWallBox<20,20,eBlockScaled>	_wallBox1;
	puWallBox<20,20,eBlockScaled>	_wallBox2;
	puWallBox<20,20,eBlockScaled>	_wallBox3;
	puWallBox<20,20,eBlockScaled>	_wallBox4;
	puWallBox<20,20,eBlockScaled>	_wallBox5;
	puWallBox<20,20,eBlockScaled>	_wallBox6;
	puWallBox<20,20,eBlockScaled>	_wallBox7;
	puWallBox<20,20,eBlockScaled>	_wallBox8;
	puWallBox<20,20,eBlockScaled>	_wallBox9;
	puWoodBox<160,15,eBlockScaled>	_woodBox1;
	puWoodBox<160,15,eBlockScaled>	_woodBox2;
	puWoodBox<160,15,eBlockScaled>	_woodBox3;
};
//------------------------------------------------------------------
class LevelPawel_16 : public Level
{
public:
	LevelPawel_16( GameTypes::LevelEnum levelEnum, bool viewMode );

private:
	puBomb< 44 >	_bomb1;
	puBomb< 44 >	_bomb2;
	puBomb< 44 >	_bomb3;
	puBomb< 44 >	_bomb4;
	puBomb< 44 >	_bomb5;
	puCoin< 40 >	_coin1;
	puFloor2	_floor21;
	puFragileBoxStatic<96,96,eBlockScaled>	_fragileBoxStatic1;
	puFragileBoxStatic<96,96,eBlockScaled>	_fragileBoxStatic2;
	puFragileBoxStatic<96,96,eBlockScaled>	_fragileBoxStatic3;
	puFragileBoxStatic<96,96,eBlockScaled>	_fragileBoxStatic4;
	puPiggyBank< 60 >	_piggyBank1;
	puGumBall< >	_tennisBall1;
	puGumBall< >	_tennisBall2;
	puGumBall< >	_tennisBall3;
	puWallBox<200,15,eBlockScaled>	_wallBox3;
	puWallBox<200,15,eBlockScaled>	_wallBox4;
	puWallBox<40,40,eBlockScaled>	_wallBox10;
	puWallBox<40,40,eBlockScaled>	_wallBox9;
	puWallBox<400,15,eBlockScaled>	_wallBox11;
	puWallBox<400,15,eBlockScaled>	_wallBox5;
	puWallBox<400,15,eBlockScaled>	_wallBox6;
	puWallBox<400,15,eBlockScaled>	_wallBox7;
	puWallBox<400,15,eBlockScaled>	_wallBox8;
	puWallBox<10,10,eBlockScaled>	_wallBox12;
	puWallBox<10,10,eBlockScaled>	_wallBox13;
	puWallBox<10,10,eBlockScaled>	_wallBox14;
	puWallBox<10,10,eBlockScaled>	_wallBox15;
	puWallBox<10,10,eBlockScaled>	_wallBox16;
	puWallBox<10,10,eBlockScaled>	_wallBox17;
	puWallBox<100,15,eBlockScaled>	_wallBox1;
	puWallBox<100,15,eBlockScaled>	_wallBox2;
	puWoodBox<240,15,eBlockScaled>	_woodBox1;
};
//------------------------------------------------------------------
#endif
