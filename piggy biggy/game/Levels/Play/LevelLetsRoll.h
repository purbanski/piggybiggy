#ifndef __LEVELLETSROLL_H__
#define __LEVELLETSROLL_H__

#include <Box2D/Box2D.h>
#include "Levels/Level.h"
#include "Blocks/puBox.h"
#include "Blocks/puCircle.h"

class LevelLetsRoll_1 : public Level
{
public:
	LevelLetsRoll_1( GameTypes::LevelEnum levelEnum, bool viewMode );

private:
	puCoin< 40 >	_coin1;
	puCoin< 40 >	_coin2;
	puFragileBox<96,96,eBlockScaled>	_fragileBox1;
	puFragileBox<96,96,eBlockScaled>	_fragileBox2;
	puFragileBox<96,96,eBlockScaled>	_fragileBox3;
	puFragileBox<96,96,eBlockScaled>	_fragileBox4;
	puPiggyBank< 60 >	_piggyBank1;
	puWallBox<160,20,eBlockScaled>	_wallBox1;
	puWallBox<290,20,eBlockScaled>	_wallBox2;
	puWallBox<360,20,eBlockScaled>	_wallBox3;
	puWallBox<360,20,eBlockScaled>	_wallBox4;
};
//--------------------------------------------------------------------------------------------------------
class LevelLetsRoll_2 : public Level
{
public:
	LevelLetsRoll_2( GameTypes::LevelEnum levelEnum, bool viewMode );

private:
	puCoin< 40 >	_coin1;
	puCoin< 40 >	_coin2;
	puFragileBox<96,96,eBlockScaled>	_fragileBox1;
	puFragileBox<96,96,eBlockScaled>	_fragileBox2;
	puFragileBox<96,96,eBlockScaled>	_fragileBox3;
	puFragileBox<96,96,eBlockScaled>	_fragileBox4;
	puPiggyBank< 40 >	_piggyBank1;
	puPiggyBank< 60 >	_piggyBank2;
	puWallBox<360,20,eBlockScaled>	_wallBox2;
	puWallBox<360,20,eBlockScaled>	_wallBox3;
	puWallBox<620,20,eBlockScaled>	_wallBox4;
	puWallBox<160,20,eBlockScaled>		_wallBox5;
};
//--------------------------------------------------------------------------------------------------------

#endif
