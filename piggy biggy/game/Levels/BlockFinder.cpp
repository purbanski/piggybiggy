#include "BlockFinder.h"
#include "Game.h"

BlockFinderQuery::BlockFinderQuery( const b2Vec2& point )
{
	_point = point;
	_fixtures.clear();
}

bool BlockFinderQuery::ReportFixture( b2Fixture* fixture )
{
	//b2Body* body = fixture->GetBody();
	bool inside = fixture->TestPoint( _point );

	if ( inside )
	{
		_fixtures.push_back( fixture );
		return true; // continue
	}

	// Continue the query.
	return true;
}
//-------------------------------------------------------------------------------------------
puBlock* BlockFinderBase::BlockTouched( const b2Vec2& position )
{
	//--------------------
	// Make a small box.
	//--------------------
	b2AABB aabb;
	b2Vec2 d;

	d.Set(0.001f, 0.001f);
	aabb.lowerBound = position - d;
	aabb.upperBound = position + d;


	//----------------------------------------
	// Query the world for overlapping shapes.
	//----------------------------------------
	puBlock *block;
	BlockFinderQuery query( position );
	Game::Get()->GetRuningLevel()->GetWorld()->QueryAABB( &query, aabb);

	//-------------------------------
	// Process Vehical buttons first
	if ( query._fixtures.size() > 1 )
	{
		for ( FixtureContainer::iterator it = query._fixtures.begin(); it != query._fixtures.end(); it++ )
		{
			block = ( puBlock * ) (*it)->GetBody()->GetUserData();
			if  ( block && 
				( block->GetBlockEnum() == 	eBlockVehicalButtonLeft || block->GetBlockEnum() == eBlockVehicalButtonRight ))
					return block;
		}
	}

	//-------------------------------
	// Process rest
	for ( FixtureContainer::iterator it = query._fixtures.begin(); it != query._fixtures.end(); it++ )
	{
		block = ( puBlock * ) (*it)->GetBody()->GetUserData();
		
		if  ( ! block ) 
			continue;
		 
		if	( block->GetBlockEnum() == 	eBlockBoxWall || block->GetBlockEnum() == eBlockCircleWall )
			continue;

		if ( ! (*it)->IsSensor() )
			return block;
	}

	return NULL;
}
//-------------------------------------------------------------------------------------------
bool BlockFinderBase::MouseLongMove( const b2Vec2 &starPos, const b2Vec2 &endPos, void *userData )
{
	float prec = 0.1f;

	// Move to long split in few moves
	float delta;
	delta = (float) ( sqrt(pow (( starPos.x - endPos.x ),2) + pow (( starPos.y - endPos.y ),2))  ); 

	if ( delta > 2.0f * prec )
	{
		float countf;
		countf = delta * 1.0f / prec;

		int count;
		count = (int)countf;

		float dx = ( endPos.x - starPos.x ) / count;
		float dy = ( endPos.y - starPos.y ) / count;

		b2Vec2 touchIt;
		for ( int i  = 1 ; i <= count ; i ++)
		{
			touchIt.Set( starPos.x + dx * i , starPos.y + dy * i );

			puBlock *block;
			block =  BlockTouched( touchIt );
			if (  block != NULL )
			{	
				_doBlockTouch( block, touchIt, userData );
			}
			else
				_doEmptyTouch( touchIt, userData );
		}
		return true;
	}
	return false;
}
