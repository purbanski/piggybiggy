#ifndef __LEVELANIMATOR_H__
#define __LEVELANIMATOR_H__

#include "cocos2d.h"
#include <vector>
#include <list>
#include <map>
#include "GameTypes.h"
#include "Blocks/puBlock.h"

USING_NS_CC;
using namespace std;

class Level;

//------------------------------------------------------------------
// Slow Speed Blocks
//------------------------------------------------------------------
class SlowSpeedBlockMap : public map<puBlock*, float>
{
public:
	SlowSpeedBlockMap();
	void AddTime( puBlock *block, ccTime dTime );
	void ClearTime( puBlock *block );
	float GetTime( puBlock *block );

private:
//	BlockTimeMap	_blockTimeMap;
};
//------------------------------------------------------------------



//------------------------------------------------------------------
// Idle Blocks Animator
//------------------------------------------------------------------
class LevelAnimatorIdleBlocks 
{
	typedef vector<GameTypes::CharactersAnim>	AnimVector;
	typedef map<BlockEnum,AnimVector>			AnimListMap;

	typedef map<puBlock*, float>			IdleBlockToProcess;
	typedef pair<int, int>					TimeRange;
	typedef map<BlockEnum, TimeRange>		BlockTimeRangeMap;

public:
	LevelAnimatorIdleBlocks();
	~LevelAnimatorIdleBlocks();

	void Process( ccTime dtime );
	void SetLevel( Level *level );

	void Enable()	{ _enabled = true; }
	void Disable()	{ _enabled = false; }

private:
	void SetIdleBlocksTimers( ccTime dTime );
	void ProcessIdleBlocks();
	float GetRandomTime( puBlock *block );
	void PlayRandomAnim( puBlock *block );

	AnimListMap			_animListMap;
	IdleBlockToProcess	_idleBlocksTimer;
	BlockTimeRangeMap	_blockTimeRange;
	bool				_enabled;
	Level				*_level;
};


//------------------------------------------------------------------
// Active Blocks Animator
//------------------------------------------------------------------
class LevelAnimatorActiveBlocks 
{
	//typedef vector<GameTypes::CharactersAnim>	AnimVector;
	typedef map<GameTypes::BlockEnum, GameTypes::CharactersAnim>		AnimListMap;

	typedef pair< float, float >	Speed; // linear and angular
	typedef map<puBlock*, Speed >	BlockSpeedMap;
	typedef map< GameTypes::BlockEnum, Speed >	BlockSpeedRangeMap;
	typedef set< puBlock *>			BlockSet;

public:
	LevelAnimatorActiveBlocks();
	~LevelAnimatorActiveBlocks();

	void Process( ccTime dtime );
	void LevelFailed();
	void SetLevel( Level *level );

	void Enable()	{ _enabled = true; }
	void Disable()	{ _enabled = false; }

private:
	void SetBlockSpeedMap( ccTime dTime );
	void ProcessBlocks();
	void PlayRandomAnim( puBlock *block );

	AnimListMap			_animsMoveMap;
	AnimListMap			_animsStopMap;
	BlockSpeedMap		_blockSpeedMap;
	BlockSpeedRangeMap	_blockSpeedLinearLimitMap;
	BlockSpeedRangeMap	_blockSpeedAngularLimitMap;
	
	b2Vec2				_slowMovingSpeedLimit;
	float				_slowMovingTimeLimit;
	SlowSpeedBlockMap	_blockSlowSpeedTimeMap;

	BlockSet			_animatedBlocks;
	bool				_enabled;
	Level				*_level;
};
//------------------------------------------------------------------



//------------------------------------------------------------------
// Events Animator
//------------------------------------------------------------------
class LevelAnimatorEvents
{
public:
	LevelAnimatorEvents();
	void LevelFailed();
	void LevelDone();
	void CoinCollected( puBlock *piggy );
	void ThiefSmiles( puBlock *thief );
	void ThiefCaught( puBlock *policeman );
	void PiggyStolen( puBlock *piggy );
	void SetLevel( Level *level );

	void Enable()	{ _enabled = true; }
	void Disable()	{ _enabled = false; }

private:
	bool	_enabled;
	Level	*_level;
};
//------------------------------------------------------------------
#endif
