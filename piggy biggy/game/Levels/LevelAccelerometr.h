#ifndef __LEVELACCELEROMETR_H__
#define __LEVELACCELEROMETR_H__

#include "Level.h"
#include "Blocks/puAccelerometer.h"
#include "cocos2d.h"
//-----------------------------------------------------------
class LevelAccelerometrStarter : public CCNode
{
public:
	LevelAccelerometrStarter();
	void GoAway();

	CCSprite	*_spriteBg;
	CCSprite	*_spriteHoldFlat;
	CCSprite	*_spriteHoldFlatShadow;
    CCSprite	*_spriteiPhone;
    CCSprite	*_spriteToStart;
    
};
//-----------------------------------------------------------
class LevelAccelerometr : public Level
{
public:
	virtual ~LevelAccelerometr();
	virtual bool init();

	virtual void didAccelerate(CCAcceleration* pAccelerationValue);


	virtual void Step( cocos2d::ccTime dt );
	virtual void StartCheck( cocos2d::ccTime dt );
	virtual void LevelRun();
	virtual void LevelPause();

	virtual void Quiting();

protected:
	LevelAccelerometr( GameTypes::LevelEnum levelEnum, bool viewMode );

	//void SetAccelNotifierPosition();
	virtual void AdjustOldScreen();

private:
	typedef enum
	{
		eModeStoped,
		eModeStarting,
		eModeStarted
	} Mode;

	puAccelIcon		_accelIcon;
	Mode		_mode;
	
	LevelAccelerometrStarter	*_startMsg;
	

#ifdef BUILD_EDITOR
protected:
	CCLabelBMFont	*_acceLabel;
	CCLabelBMFont	*_gravityLabel;
#endif
};
//-----------------------------------------------------------

#endif
