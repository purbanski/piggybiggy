#ifdef BUILD_TEST

#include "LevelTestNode.h"
#include "GameConfig.h"
#include "Game.h"
#include "Levels/Level.h"
#include "Animation/AnimManager.h"
#include "Debug/DebugLogger.h"
#include "Facebook/FBTool.h"
#include "Platform/ToolBox.h"
#include "Promotion/FBPoster.h"

//-------------------------------------------------------------------------------------
// Level Test
//-------------------------------------------------------------------------------------
float	LevelTest::_sTestTime				= 7.0f;
int		LevelTest::_sTestRun				= 0;
int		LevelTest::_sCurrentLevelTestRun	= 0;
int		LevelTest::_sResetCount				= 3;
time_t	LevelTestNode::_sStartTime			= time( 0 );
//-------------------------------------------------------------------------------------
LevelTest* LevelTest::Create( Level *level )
{
	LevelTest* node = new LevelTest( level );
	if ( node )
	{
		node->autorelease();
		node->Init();
		return node;
	}
	CC_SAFE_DELETE( node );
	return NULL;
}
//-------------------------------------------------------------------------------------
LevelTest::LevelTest( Level *level )
{
	_counter = 0.0f;
	_heartbeat = 0.0f;
	_sceneChange = false;
	_levelComplitionForced = false;
	_level = level;
	_sTestRun++;
	_sCurrentLevelTestRun++;
	
	if ( _sCurrentLevelTestRun == LevelTest::_sResetCount )
		_level->Unfailable();

	float rand;
	for ( int i= 0 ; i < 10; i++ )
	{
		rand = (float)( Tools::Rand( 1000, (int)( _sTestTime - 1.0f ) * 1000 )) / 1000.0f;
		_actionTime[rand] = false;
	}
}
//-------------------------------------------------------------------------------------
void LevelTest::Init()
{
	D_SIZE(_blocks);
	UpdateContainer();

	CCLabelBMFont *label;
	stringstream ss;

	ss << _sTestRun;
	label = CCLabelBMFont::labelWithString( ss.str().c_str(), Config::LevelDebugMenuFont );
	label->setPosition( ccp( 100.0f, 245.0f ));
	addChild( label, 10 );

	schedule( schedule_selector( LevelTest::Step ));
}
//-------------------------------------------------------------------------------------
LevelTest::~LevelTest()
{
}
//-------------------------------------------------------------------------------------
void LevelTest::Step( ccTime dTime )
{
	stringstream ss;
	_counter += dTime;
	_heartbeat += dTime;

	
	if ( _heartbeat > 1.0f )
	{
		ss << "Heartbeat: counter: " << _counter << ", ";
		ss << "Scene change: ";
		if ( _sceneChange )
			ss << " True";
		else
			ss << " False";

		_heartbeat = 0.0f;
	}

	if ( _sceneChange )
		return;
		
	if ( _counter > _sTestTime )
	{
		if ( _sCurrentLevelTestRun == LevelTest::_sResetCount )
		{
			ForceLevelCompletion();

			if ( _counter > ( _sTestTime + 7.0f ))
			{
				ImDone();
				_sCurrentLevelTestRun = 0;
                runAction(CCSequence::actions(CCDelayTime::actionWithDuration(6.5f),
                                              CCCallFunc::actionWithTarget(this, callfunc_selector(LevelTest::PlayNextLevel)),
                                              NULL));
            }
		}
		else
		{
			ImDone();
            removeFromParentAndCleanup( true );
			Game::Get()->RestartLevel();
		}	
	}

	for ( FloatSet::iterator it = _actionTime.begin(); it != _actionTime.end(); it++ )
	{
		if ( ! it->second && _counter > it->first )
		{
			ExecuteTouch();
			it->second = true;
		}
	}
}
//-------------------------------------------------------------------------------------
void LevelTest::PlayNextLevel()
{
    removeFromParentAndCleanup( true );
    Game::Get()->PlayNextLevel();
}
//-------------------------------------------------------------------------------------
void LevelTest::UpdateContainer()
{
	b2World *world;
	puBlock *block;
	b2Body *body;

	world = _level->GetWorld();
	body = world->GetBodyList();

	while( body )
	{
		block = (puBlock*) body->GetUserData();
		if ( block )
		{
			if (
				( block->GetBlockType() == GameTypes::eDestroyable ) ||
				( block->GetBlockType() == GameTypes::eActivable ) ||
				( block->GetBlockType() == GameTypes::eMoveable ) ||
				( block->GetBlockType() == GameTypes::eCutable ) 
				)
			{
				_blocks.insert( block );
			}

			if ( block->GetBlockType() == GameTypes::eCoin )
			{
				_coins.insert( block );
			}

			if ( block->GetBlockType() == GameTypes::ePiggyBank )
			{
				_piggys.insert( block );
			}
		}
		body = body->GetNext();
	}
}
//-------------------------------------------------------------------------------------
void LevelTest::ExecuteTouch()
{
	if (  _blocks.size() <= 0 )
		return;

	puBlock *block;
	BlockSet::iterator it;

	if ( _blocks.size() == 1 )
	{
		it = _blocks.begin();
	}
	else
	{
		int rand;
		rand = Tools::Rand( 0, _blocks.size() - 1 );
	
		it = _blocks.begin();
		for ( int i = 0; i < rand; i++ )
		{
			it++;
		}
	}

	block = *it;
	_blocks.erase( it );
	D_SEPERATOR
	D_PTR( block )

	if ( ! block || ! block->GetBody() )
		return;

	switch ( block->GetBlockType() )
	{
		case eDestroyable :
			block->DestroyFragile();
			break;

		case eActivable :
			block->Activate();
			break;

		case eMoveable :
			block->GetBody()->SetAngularVelocity((float) Tools::Rand( -40, 40 ));
			break;

		case eCutable :
			block->Cut();
			break;

		default:
			;
	}
}
//-------------------------------------------------------------------------------------
void LevelTest::WriteLog()
{
	stringstream ss;
	
	//---------------------------------------------
	// Test run log
	//---------------------------------------------
	ss.str("");
	ss << "Test nr: " << _sTestRun;
	D_WRITE( ss.str().c_str() );

	ss.str("");
	ss << "Current Level: " << Game::Get()->GetGameStatus()->GetCurrentLevel();
	D_WRITE( ss.str().c_str() );

	ss.str("");
	ss << "Next Level: " << Game::Get()->GetGameStatus()->GetNextUndoneLevel();
	D_WRITE( ss.str().c_str() );

	D_WRITE("");


	//---------------------------------------------
	// Memory log
	//---------------------------------------------
	int freeMem;
	int lfbMem;
	int usedMem;
	int sizeMem;

	//freeMem		= s3eMemoryGetInt( S3E_MEMORY_FREE );
	//lfbMem		= s3eMemoryGetInt( S3E_MEMORY_LFB );
	//usedMem		= s3eMemoryGetInt( S3E_MEMORY_USED );
	//

	freeMem		= 0;//IwMemBucketGetFree( GameTypes::MEMBUCKET_FIXED );
	lfbMem		= 0;//IwMemBucketGetLargestFreeBlock( GameTypes::MEMBUCKET_FIXED );
	usedMem		= 0;//IwMemBucketGetUsed( GameTypes::MEMBUCKET_FIXED );
	sizeMem		= 0;//s3eMemoryGetInt( S3E_MEMORY_SIZE );

	string strFree	= Tools::IntToDottedString( freeMem );
	string strLbf	= Tools::IntToDottedString( lfbMem );
	string strUsed	= Tools::IntToDottedString( usedMem );
	string strSize	= Tools::IntToDottedString( sizeMem );

	ss.str("");
	ss << "Test: " << _sTestRun;

	string msg;
	msg.append( ss.str() );
	msg.append("\tUsed: ");
	msg.append( strUsed );
	msg.append("  Free: ");
	msg.append( strFree );
	msg.append("  LBF: ");
	msg.append( strLbf );
	//msg.append("  Size: ");
	//msg.append( strSize );

	DebugLogger::Get( DebugLogger::LogMemory )->Write( msg.c_str() );


	//---------------------------------------------
	// Log Run
	//---------------------------------------------
	stringstream timeDetails;
	SecondsHolder seconds;

	time_t now;
	time( &now );

	seconds._seconds =  now - LevelTestNode::_sStartTime;

	timeDetails << "Start time: ";
	timeDetails << Tools::GetTimeStamp( &LevelTestNode::_sStartTime ) << "\tLaps: ";
	timeDetails << seconds.GetHoursCount() / 10 << seconds.GetHoursCount() - ( seconds.GetHoursCount() / 10 ) * 10 ;
	timeDetails << ":" << seconds.GetMinutesCount() / 10 << seconds.GetMinutesCount() - ( seconds.GetMinutesCount() / 10 ) * 10 ;
	timeDetails << ":" << seconds.GetSecondsCount() / 10 << seconds.GetSecondsCount() - ( seconds.GetSecondsCount() / 10 ) * 10 ;

	DebugLogger::Get( DebugLogger::LogTestDuration )->Write( timeDetails.str().c_str() );


}
//-------------------------------------------------------------------------------------
void LevelTest::ImDone()
{
	WriteLog();
	_sceneChange = true;
	unschedule( schedule_selector( LevelTest::Step ));
}
//-------------------------------------------------------------------------------------
void LevelTest::ForceLevelCompletion()
{
	if ( ! _piggys.size() ||  ! _coins.size() || _levelComplitionForced )
		return;

	_levelComplitionForced = true;

	puBlock *piggy;

	piggy = *(_piggys.begin());

	for ( int i = 0; i < 40; i++ )
	{
		_level->GameCoinCollected( piggy );
	}
}



//-------------------------------------------------------------------------------------
// Level Test Node
//-------------------------------------------------------------------------------------
bool LevelTestNode::_sTesting = false;

LevelTestNode* LevelTestNode::Create( Level *level )
{
	LevelTestNode* node = new LevelTestNode( level );
	if ( node )
	{
		node->autorelease();
		node->Init();
		return node;
	}
	CC_SAFE_DELETE( node );
	return NULL;
}
//-------------------------------------------------------------------------------------
LevelTestNode::LevelTestNode( Level *level )
{
	_level = level;
}
//-------------------------------------------------------------------------------------
void LevelTestNode::Init()
{
	//-----------------------------------
	// Tests, memory, reset... menu
	CCLabelBMFont *labelMem = CCLabelBMFont::labelWithString("Anim", Config::LevelDebugMenuFont );
	CCMenuItemLabel *itemAnim = CCMenuItemLabel::itemWithLabel( labelMem, this, menu_selector( LevelTestNode::Menu_ShowAnimMenu ));

	CCLabelBMFont *labelTest = CCLabelBMFont::labelWithString("Test", Config::LevelDebugMenuFont );
	CCMenuItemLabel *itemTest = CCMenuItemLabel::itemWithLabel( labelTest, this, menu_selector( LevelTestNode::Menu_TestToggle ));

	CCLabelBMFont *labelResetSnd = CCLabelBMFont::labelWithString( "Facebook", Config::LevelDebugMenuFont );
	CCMenuItemLabel *itemFacebook = CCMenuItemLabel::itemWithLabel( labelResetSnd, this, menu_selector( LevelTestNode::Menu_ShowFacebookMenu ));

	CCLabelBMFont *labelResetAll = CCLabelBMFont::labelWithString( "ResetAll", Config::LevelDebugMenuFont );
	CCMenuItemLabel *itemResetAll = CCMenuItemLabel::itemWithLabel( labelResetAll, this, menu_selector( LevelTestNode::Menu_ResetAll ));

	_menu = CCMenu::menuWithItems( itemResetAll, itemFacebook, itemAnim, itemTest, NULL );
	_menu->alignItemsHorizontallyWithPadding( 25.0f );
	_menu->setPosition( ccp( -150.0f, 280.0f ));
	addChild( _menu, 10 );
	_menu->setIsVisible( false );


	//-----------------------------------
	// Debug menu
	CCMenu *menu;

	CCLabelBMFont *labelDebug = CCLabelBMFont::labelWithString("Debug", Config::LevelDebugMenuFont );
	CCMenuItemLabel *debugItem = CCMenuItemLabel::itemWithLabel( labelDebug, this, menu_selector( LevelTestNode::Menu_Debug ));

	menu = CCMenu::menuWithItem( debugItem );
	menu->setPosition( ccp( 220.0f, 280.0f ));
	addChild( menu, 10 );


	//-----------------------------------
	// Anim Right Menu
	_animRightMenu = AnimRightMenu::Create( _level );
	_animRightMenu->setPosition( ccp( 380.0f, -25.0f ));
	_animRightMenu->setIsVisible( false );
	addChild( _animRightMenu, 10 );

    
	//-----------------------------------
	// Anim Left Menu
	_animLeftMenu = AnimLeftMenu::Create( _level );
	_animLeftMenu->setPosition( ccp( -380.0f, -25.0f ));
	_animLeftMenu->setIsVisible( false );
	addChild( _animLeftMenu, 10 );


   	//-----------------------------------
	// Facebook Menu
	_fbMenuLeft = FBMenuLeft::Create( _level );
	_fbMenuLeft->setPosition( ccp( -80.0f, 10.0f ));
	_fbMenuLeft->setIsVisible( false );
	addChild( _fbMenuLeft, 10 );

   	_fbMenuRight = FBMenuRight::Create( _level );
	_fbMenuRight->setPosition( ccp( 110.0f, 10.0f ));
	_fbMenuRight->setIsVisible( false );
	addChild( _fbMenuRight, 10 );

	//-----------------------------------
	// Test Menu
	_testMenu = TestMenu::Create( this );
	_testMenu->setPosition( ccp( 105.0f, 50.0f ));
	_testMenu->setIsVisible( false );
	addChild( _testMenu, 10 );


	//-----------------------------------
	// Memory node
//	_memDebug = MemoryDebug::Create();
//	_memDebug->setIsVisible( false );
//	_memDebug->setPosition( ccp( 0.0f, -305.0f ));
//	addChild( _memDebug, 10 );


	//-----------------------------------
	// Testing
	if ( _sTesting )
	{
		_test = LevelTest::Create( _level );
		addChild( _test, 10 );
//		_memDebug->setIsVisible( true );
		_menu->setIsVisible( true );
	}
	else
	{
		_test = NULL;
	}
}
//-------------------------------------------------------------------------------------
LevelTestNode::~LevelTestNode()
{
	removeAllChildrenWithCleanup( true );
}
//-------------------------------------------------------------------------------------
void LevelTestNode::Menu_Debug( CCObject *sender )
{
	if ( _menu->getIsVisible() )
	{
		_menu->setIsVisible( false );
        _animRightMenu->setIsVisible( false );
        _animLeftMenu->setIsVisible( false );
        _fbMenuLeft->setIsVisible( false );
        _fbMenuRight->setIsVisible( false );
	}
	else
	{
		_menu->setIsVisible( true );
	}
	//_animRightMenu->setIsVisible( _menu->getIsVisible() );
	//_animLeftMenu->setIsVisible( _menu->getIsVisible() );
}
//-------------------------------------------------------------------------------------
void LevelTestNode::Menu_ShowAnimMenu( CCObject* pSender )
{
    if ( _animLeftMenu->getIsVisible() )
    {
        _animLeftMenu->setIsVisible( false );
        _animLeftMenu->setIsTouchEnabled( false );

        _animRightMenu->setIsVisible( false );
        _animRightMenu->setIsTouchEnabled( false );
    }
    else
    {
        _animLeftMenu->setIsVisible( true );
        _animLeftMenu->setIsTouchEnabled( true );

        _animRightMenu->setIsVisible( true );
        _animRightMenu->setIsTouchEnabled( true );
    }
}
//-------------------------------------------------------------------------------------
void LevelTestNode::Menu_ShowFacebookMenu( CCObject* pSender )
{
    if ( _fbMenuLeft->getIsVisible() )
    {
        _fbMenuRight->setIsVisible( false );
        _fbMenuRight->setIsTouchEnabled( false );

        _fbMenuLeft->setIsVisible( false );
        _fbMenuLeft->setIsTouchEnabled( false );
    }
    else
    {
        _fbMenuRight->setIsVisible( true );
        _fbMenuRight->setIsTouchEnabled( true );

        _fbMenuLeft->setIsVisible( true );
        _fbMenuLeft->setIsTouchEnabled( true );
    }
}
//-------------------------------------------------------------------------------------
void LevelTestNode::StartTesting()
{
	if ( ! _sTesting && ! _test)
	{
		time( &_sStartTime );
		_test = LevelTest::Create( _level );
		addChild( _test, 10 );
		_sTesting = true;
	}
}
//-------------------------------------------------------------------------------------
void LevelTestNode::StopTesting()
{
	_sTesting = false;
	if ( _test && _test->getParent() )
		_test->removeFromParentAndCleanup( true );
}
//-------------------------------------------------------------------------------------
void LevelTestNode::Menu_TestToggle( CCObject* pSender )
{
	if ( _testMenu->getIsVisible() )
		_testMenu->setIsVisible( false );
	else
		_testMenu->setIsVisible( true );

}
//-------------------------------------------------------------------------------------
void LevelTestNode::Menu_ResetAll( CCObject* pSender )
{
	Preloader::Get()->FreeResources();
}
//-------------------------------------------------------------------------------------
void LevelTestNode::Menu_ResetSnd( CCObject* pSender )
{
	SoundEngine::Get()->Reset();
}
//-------------------------------------------------------------------------------------






//-------------------------------------------------------------------------------------
// Anim Menu
//-------------------------------------------------------------------------------------
AnimRightMenu* AnimRightMenu::Create( Level *level )
{
	AnimRightMenu* menu = new AnimRightMenu( level );
	if ( menu )
	{
		menu->autorelease();
		menu->Init();
		return menu;
	}
	CC_SAFE_DELETE(menu);
	return NULL;
}
//-------------------------------------------------------------------------------------
AnimRightMenu::AnimRightMenu( Level *level )
{
	_level = level;
}
//-------------------------------------------------------------------------------------
void AnimRightMenu::Init()
{
	CCLabelBMFont *a1 = CCLabelBMFont::labelWithString( "Uuuaa",		Config::LevelDebugMenuFont );
	CCLabelBMFont *a2 = CCLabelBMFont::labelWithString( "NoseBubble",	Config::LevelDebugMenuFont );
	CCLabelBMFont *a3 = CCLabelBMFont::labelWithString( "FiuFiu",		Config::LevelDebugMenuFont );
	CCLabelBMFont *a4 = CCLabelBMFont::labelWithString( "Salto",		Config::LevelDebugMenuFont );
	CCLabelBMFont *a5 = CCLabelBMFont::labelWithString( "Eeeooo",		Config::LevelDebugMenuFont );
	CCLabelBMFont *a6 = CCLabelBMFont::labelWithString( "HappyJump",	Config::LevelDebugMenuFont );
	CCLabelBMFont *a7 = CCLabelBMFont::labelWithString( "LookAround",	Config::LevelDebugMenuFont );
	CCLabelBMFont *a8 = CCLabelBMFont::labelWithString( "Cop Happy",	Config::LevelDebugMenuFont );
	CCLabelBMFont *a9 = CCLabelBMFont::labelWithString( "Rotation Fix",	Config::LevelDebugMenuFont );

	FXMenuItemLabel *ai1 = FXMenuItemLabel::itemWithLabel( a1, this, menu_selector( AnimRightMenu::Menu_A1 ));
	FXMenuItemLabel *ai2 = FXMenuItemLabel::itemWithLabel( a2, this, menu_selector( AnimRightMenu::Menu_A2 ));
	FXMenuItemLabel *ai3 = FXMenuItemLabel::itemWithLabel( a3, this, menu_selector( AnimRightMenu::Menu_A3 ));
	FXMenuItemLabel *ai4 = FXMenuItemLabel::itemWithLabel( a4, this, menu_selector( AnimRightMenu::Menu_A4 ));
	FXMenuItemLabel *ai5 = FXMenuItemLabel::itemWithLabel( a5, this, menu_selector( AnimRightMenu::Menu_A5 ));
	FXMenuItemLabel *ai6 = FXMenuItemLabel::itemWithLabel( a6, this, menu_selector( AnimRightMenu::Menu_A6 ));
	FXMenuItemLabel *ai7 = FXMenuItemLabel::itemWithLabel( a7, this, menu_selector( AnimRightMenu::Menu_A7 ));
	FXMenuItemLabel *ai8 = FXMenuItemLabel::itemWithLabel( a8, this, menu_selector( AnimRightMenu::Menu_A8 ));
	FXMenuItemLabel *ai9 = FXMenuItemLabel::itemWithLabel( a9, this, menu_selector( AnimRightMenu::Menu_A9 ));

	MyInitWithItems( ai1, ai2, ai3, ai4, ai5, ai6, ai7, ai8, ai9, NULL );
	alignItemsVerticallyWithPadding( 25.0f );

    setIsVisible( false );
    setIsTouchEnabled( false );
}
//-------------------------------------------------------------------------------------
void AnimRightMenu::Menu_A1( CCObject *sender )
{
	AnimManager::Get()->PlayAnim( GameTypes::eAnim_PiggyBankUaa, GameTypes::eAnimPiority_High, GetPiggy() );
}
//-------------------------------------------------------------------------------------
void AnimRightMenu::Menu_A2( CCObject *sender )
{
	AnimManager::Get()->PlayAnim( GameTypes::eAnim_PiggyBankNoseBubble, GameTypes::eAnimPiority_High, GetPiggy() );
}
//-------------------------------------------------------------------------------------
void AnimRightMenu::Menu_A3( CCObject *sender )
{
	AnimManager::Get()->PlayAnim( GameTypes::eAnim_PiggyBankFiuFiu, GameTypes::eAnimPiority_High, GetPiggy() );
}
//-------------------------------------------------------------------------------------
void AnimRightMenu::Menu_A4( CCObject *sender )
{
	AnimManager::Get()->PlayAnim( GameTypes::eAnim_PiggyBankSalto, GameTypes::eAnimPiority_High, GetPiggy() );
}
//-------------------------------------------------------------------------------------
void AnimRightMenu::Menu_A5( CCObject *sender )
{
	AnimManager::Get()->PlayAnim( GameTypes::eAnim_PiggyBankEeeOoo, GameTypes::eAnimPiority_High, GetPiggy() );
}
//-------------------------------------------------------------------------------------
void AnimRightMenu::Menu_A6( CCObject *sender )
{
	AnimManager::Get()->PlayAnim( GameTypes::eAnim_PiggyBankHappyJump, GameTypes::eAnimPiority_High, GetPiggy() );
}
//-------------------------------------------------------------------------------------
void AnimRightMenu::Menu_A7( CCObject *sender )
{
	AnimManager::Get()->PlayAnim( GameTypes::eAnim_PiggyBankLookAround, GameTypes::eAnimPiority_High, GetPiggy() );
}
//-------------------------------------------------------------------------------------
void AnimRightMenu::Menu_A8( CCObject *sender )
{
	puBlock *piggy;
	GameTypes::AnimSpriteState state;

	piggy = GetPiggy();
	state = AnimManager::Get()->GetSpriteState( piggy );

	if ( state == eSpriteState_Sit || state == eSpriteState_Roll  )
	{
		AnimManager::Get()->SetSpriteState( piggy, eSpriteState_Roll );
		AnimManager::Get()->PlayAnim( GameTypes::eAnim_CopSmiles, GameTypes::eAnimPiority_High, GetPiggy() );
	}
}//-------------------------------------------------------------------------------------
void AnimRightMenu::Menu_A9( CCObject *sender )
{
	puBlock *piggy;
	piggy = GetPiggy();
	piggy->SetRotation( piggy->GetRotation() + Tools::Rand( 120, 120 ));
	//piggy->SetRotation( piggy->GetRotation() - 160 );
}
//-------------------------------------------------------------------------------------
void AnimRightMenu::MyInitWithItems( CCMenuItem* item, ... )
{
	va_list args;
	va_start( args, item );

	CCMenu::initWithItems( item, args );
	va_end(args);
}
//-------------------------------------------------------------------------------------
puBlock* AnimRightMenu::GetPiggy()
{
	puBlock *block;
	b2World *world;
	b2Body *body;

	world = _level->GetWorld();
	body = world->GetBodyList();

	while ( body )
	{
		block = (puBlock*)( body->GetUserData() );
		if ( block && block->GetBlockType() == ePiggyBank )
			return block;

		body = body->GetNext();
	}

	return NULL;
}




//-------------------------------------------------------------------------------------
// Anim Left Menu
//-------------------------------------------------------------------------------------
AnimLeftMenu* AnimLeftMenu::Create( Level *level )
{
	AnimLeftMenu* menu = new AnimLeftMenu( level );
	if ( menu )
	{
		menu->autorelease();
		menu->Init();
		return menu;
	}
	CC_SAFE_DELETE(menu);
	return NULL;
}
//-------------------------------------------------------------------------------------
AnimLeftMenu::AnimLeftMenu( Level *level )
{
	_level = level;
}
//-------------------------------------------------------------------------------------
void AnimLeftMenu::Init()
{
	CCLabelBMFont *a1 = CCLabelBMFont::labelWithString( "Failed Sit",		Config::LevelDebugMenuFont );
	CCLabelBMFont *a2 = CCLabelBMFont::labelWithString( "Failed Roll",		Config::LevelDebugMenuFont );
	CCLabelBMFont *a3 = CCLabelBMFont::labelWithString( "Done Sit",			Config::LevelDebugMenuFont );
	CCLabelBMFont *a4 = CCLabelBMFont::labelWithString( "Done Roll",		Config::LevelDebugMenuFont );
	CCLabelBMFont *a5 = CCLabelBMFont::labelWithString( "Coin Get Sit",		Config::LevelDebugMenuFont );
	CCLabelBMFont *a6 = CCLabelBMFont::labelWithString( "Coin Get Roll",	Config::LevelDebugMenuFont );
	CCLabelBMFont *a7 = CCLabelBMFont::labelWithString( "Thief Sit",		Config::LevelDebugMenuFont );
	CCLabelBMFont *a8 = CCLabelBMFont::labelWithString( "Thief Roll",		Config::LevelDebugMenuFont );
	CCLabelBMFont *a9 = CCLabelBMFont::labelWithString( "Pocket Unlock",	Config::LevelDebugMenuFont );

	FXMenuItemLabel *ai1 = FXMenuItemLabel::itemWithLabel( a1, this, menu_selector( AnimLeftMenu::Menu_A1 ));
	FXMenuItemLabel *ai2 = FXMenuItemLabel::itemWithLabel( a2, this, menu_selector( AnimLeftMenu::Menu_A2 ));
	FXMenuItemLabel *ai3 = FXMenuItemLabel::itemWithLabel( a3, this, menu_selector( AnimLeftMenu::Menu_A3 ));
	FXMenuItemLabel *ai4 = FXMenuItemLabel::itemWithLabel( a4, this, menu_selector( AnimLeftMenu::Menu_A4 ));
	FXMenuItemLabel *ai5 = FXMenuItemLabel::itemWithLabel( a5, this, menu_selector( AnimLeftMenu::Menu_A5 ));
	FXMenuItemLabel *ai6 = FXMenuItemLabel::itemWithLabel( a6, this, menu_selector( AnimLeftMenu::Menu_A6 ));
	FXMenuItemLabel *ai7 = FXMenuItemLabel::itemWithLabel( a7, this, menu_selector( AnimLeftMenu::Menu_A7 ));
	FXMenuItemLabel *ai8 = FXMenuItemLabel::itemWithLabel( a8, this, menu_selector( AnimLeftMenu::Menu_A8 ));
	FXMenuItemLabel *ai9 = FXMenuItemLabel::itemWithLabel( a9, this, menu_selector( AnimLeftMenu::Menu_A9 ));

	MyInitWithItems( ai1, ai2, ai3, ai4, ai5, ai6, ai7, ai8, ai9, NULL );
	alignItemsVerticallyWithPadding( 25.0f );
    
    setIsVisible( false );
    setIsTouchEnabled( false );
}
//-------------------------------------------------------------------------------------
void AnimLeftMenu::Menu_A1( CCObject *sender )
{
	puBlock *piggy;
	GameTypes::AnimSpriteState state;

	piggy = GetPiggy();
	state = AnimManager::Get()->GetSpriteState( piggy );

	if ( state == eSpriteState_Sit || state == eSpriteState_Roll  )
	{
		AnimManager::Get()->SetSpriteState( piggy, eSpriteState_Sit );
		AnimManager::Get()->PlayAnim( GameTypes::eAnim_PiggyBankLevelFailed, GameTypes::eAnimPiority_Highest, GetPiggy() );
	}
}
//-------------------------------------------------------------------------------------
void AnimLeftMenu::Menu_A2( CCObject *sender )
{
	puBlock *piggy;
	GameTypes::AnimSpriteState state;

	piggy = GetPiggy();
	state = AnimManager::Get()->GetSpriteState( piggy );

	if ( state == eSpriteState_Sit || state == eSpriteState_Roll  )
	{
		AnimManager::Get()->SetSpriteState( piggy, eSpriteState_Roll );
		AnimManager::Get()->PlayAnim( GameTypes::eAnim_PiggyBankLevelFailed, GameTypes::eAnimPiority_Highest, GetPiggy() );
	}
}
//-------------------------------------------------------------------------------------
void AnimLeftMenu::Menu_A3( CCObject *sender )
{
	puBlock *piggy;
	GameTypes::AnimSpriteState state;

	piggy = GetPiggy();
	state = AnimManager::Get()->GetSpriteState( piggy );

	if ( state == eSpriteState_Sit || state == eSpriteState_Roll  )
	{
		AnimManager::Get()->SetSpriteState( piggy, eSpriteState_Sit );
		AnimManager::Get()->PlayAnim( GameTypes::eAnim_PiggyBankLevelDone, GameTypes::eAnimPiority_High, GetPiggy() );
	}
}
//-------------------------------------------------------------------------------------
void AnimLeftMenu::Menu_A4( CCObject *sender )
{
	puBlock *piggy;
	GameTypes::AnimSpriteState state;

	piggy = GetPiggy();
	state = AnimManager::Get()->GetSpriteState( piggy );

	if ( state == eSpriteState_Sit || state == eSpriteState_Roll  )
	{
		AnimManager::Get()->SetSpriteState( piggy, eSpriteState_Roll );
		AnimManager::Get()->PlayAnim( GameTypes::eAnim_PiggyBankLevelDone, GameTypes::eAnimPiority_High, GetPiggy() );
	}
}
//-------------------------------------------------------------------------------------
void AnimLeftMenu::Menu_A5( CCObject *sender )
{
	puBlock *piggy;
	GameTypes::AnimSpriteState state;

	piggy = GetPiggy();
	state = AnimManager::Get()->GetSpriteState( piggy );

	if ( state == eSpriteState_Sit || state == eSpriteState_Roll  )
	{
		AnimManager::Get()->SetSpriteState( piggy, eSpriteState_Sit );
		AnimManager::Get()->PlayAnim( GameTypes::eAnim_PiggyBankCoinCollected, GameTypes::eAnimPiority_High, GetPiggy() );
	}
}
//-------------------------------------------------------------------------------------
void AnimLeftMenu::Menu_A6( CCObject *sender )
{
	puBlock *piggy;
	GameTypes::AnimSpriteState state;

	piggy = GetPiggy();
	state = AnimManager::Get()->GetSpriteState( piggy );

	if ( state == eSpriteState_Sit || state == eSpriteState_Roll  )
	{
		AnimManager::Get()->SetSpriteState( piggy, eSpriteState_Roll );
		AnimManager::Get()->PlayAnim( GameTypes::eAnim_PiggyBankCoinCollected, GameTypes::eAnimPiority_High, GetPiggy() );
	}
}
//-------------------------------------------------------------------------------------
void AnimLeftMenu::Menu_A7( CCObject *sender )
{
	puBlock *piggy;
	GameTypes::AnimSpriteState state;

	piggy = GetPiggy();
	state = AnimManager::Get()->GetSpriteState( piggy );

	if ( state == eSpriteState_Sit || state == eSpriteState_Roll  )
	{
		AnimManager::Get()->SetSpriteState( piggy, eSpriteState_Sit );
		AnimManager::Get()->PlayAnim( GameTypes::eAnim_ThiefSmiles, GameTypes::eAnimPiority_High, GetPiggy() );
	}
}
//-------------------------------------------------------------------------------------
void AnimLeftMenu::Menu_A8( CCObject *sender )
{
	puBlock *piggy;
	GameTypes::AnimSpriteState state;

	piggy = GetPiggy();
	state = AnimManager::Get()->GetSpriteState( piggy );

	if ( state == eSpriteState_Sit || state == eSpriteState_Roll  )
	{
		AnimManager::Get()->SetSpriteState( piggy, eSpriteState_Roll );
		AnimManager::Get()->PlayAnim( GameTypes::eAnim_ThiefSmiles, GameTypes::eAnimPiority_High, GetPiggy() );
	}
}
//-------------------------------------------------------------------------------------
void AnimLeftMenu::Menu_A9( CCObject *sender )
{
	UnlockPocketNode *unlockAnim;
	unlockAnim = UnlockPocketNode::node( GameTypes::ePocketRasta );
	unlockAnim->setPosition( ccp( 00.0f, 0.0f ));
	_level->addChild( unlockAnim, 255 );
	unlockAnim->StartAnim();
}
//-------------------------------------------------------------------------------------
void AnimLeftMenu::MyInitWithItems( CCMenuItem* item, ... )
{
	va_list args;
	va_start( args, item );

	CCMenu::initWithItems( item, args );
	va_end(args);
}
//-------------------------------------------------------------------------------------
puBlock* AnimLeftMenu::GetPiggy()
{
	puBlock *block;
	b2World *world;
	b2Body *body;

	world = _level->GetWorld();
	body = world->GetBodyList();

	while ( body )
	{
		block = (puBlock*)( body->GetUserData() );
		if ( block && block->GetBlockType() == ePiggyBank )
			return block;

		body = body->GetNext();
	}

	return NULL;
}





//-------------------------------------------------------------------------------------
// Facebook Menu
//-------------------------------------------------------------------------------------
FBTestMenuBase::FBTestMenuBase( Level *level )
{
	_level = level;
}
//-------------------------------------------------------------------------------------
void FBTestMenuBase::Init()
{   
    setIsVisible( false );
    setIsTouchEnabled( false );
}
//-------------------------------------------------------------------------------------
void FBTestMenuBase::Menu_FBLogin( CCObject *sender )
{
    FBTool::Get()->Login();
}
//-------------------------------------------------------------------------------------
void FBTestMenuBase::Menu_FBLogout( CCObject *sender )
{
    FBTool::Get()->Logout();
}
//-------------------------------------------------------------------------------------
void FBTestMenuBase::Menu_FBStatusUpdate( CCObject *sender )
{
    FBTool::Get()->StatusUpdate( "test" );
}
//-------------------------------------------------------------------------------------
void FBTestMenuBase::Menu_FBScreenshotUpload( CCObject *sender )
{
     CCDirector::sharedDirector()->makeScreenshot();
}
//-------------------------------------------------------------------------------------
void FBTestMenuBase::Menu_FBActivity_LevelCompleted( CCObject *sender )
{
    FBPoster::Get()->Post_LevelCompleted( Game::Get()->GetRuningLevel()->GetLevelEnum(), Game::Get()->GetRuningLevel()->GetLevelTag() );
}
//-------------------------------------------------------------------------------------
void FBTestMenuBase::Menu_FBActivity_LevelSkipped( CCObject *sender )
{
    FBPoster::Get()->Post_LevelSkipped( Game::Get()->GetRuningLevel()->GetLevelEnum(), Game::Get()->GetRuningLevel()->GetLevelTag() );
}
//-------------------------------------------------------------------------------------
void FBTestMenuBase::Menu_FBTestPost( CCObject *sender )
{
    FBTool::Get()->Post_test();
}
//-------------------------------------------------------------------------------------
void FBTestMenuBase::Menu_FBAchievement( CCObject *sender )
{
    FBTool::Get()->PostUserAchievement();
}
//-------------------------------------------------------------------------------------
void FBTestMenuBase::Menu_FBCustomPost( CCObject *sender )
{
    FBPoster::Get()->RandomPost();
}
//-------------------------------------------------------------------------------------
void FBTestMenuBase::Menu_FBIsLogged( CCObject *sender )
{
    if ( FBTool::Get()->IsLogged() )
    {
        ToolBox::DebugMsgBox( "debug", "Facebook logged in");
    }
    else
    {
        ToolBox::DebugMsgBox( "debug", "Facebook not logged");       
    }
}
//-------------------------------------------------------------------------------------
void FBTestMenuBase::Menu_FBGetUsername( CCObject *sender )
{
    char buffer[32];
    snprintf( buffer, 32, "Username: %s", FBTool::Get()->GetUsername().c_str() );
    
    ToolBox::DebugMsgBox( "debug", buffer );
}
//-------------------------------------------------------------------------------------
void FBTestMenuBase::Menu_FBReadScore( CCObject *sender )
{
    FBTool::Get()->ReadScore( NULL );
}
//-------------------------------------------------------------------------------------
void FBTestMenuBase::Menu_FBPostScore( CCObject *sender )
{
    FBTool::Get()->PostScore( Tools::Rand(50, 1000));
}
//-------------------------------------------------------------------------------------
void FBTestMenuBase::Menu_FBReadFriends( CCObject *sender )
{
    FBTool::Get()->Read_FriendsList( NULL );
}
//-------------------------------------------------------------------------------------
void FBTestMenuBase::MyInitWithItems( CCMenuItem* item, ... )
{
	va_list args;
	va_start( args, item );

	CCMenu::initWithItems( item, args );
	va_end(args);
}


//-------------------------------------------------------------------------------------
// FB Menu Left
//-------------------------------------------------------------------------------------
FBMenuLeft* FBMenuLeft::Create( Level *level )
{
	FBMenuLeft* menu = new FBMenuLeft( level );
	if ( menu )
	{
		menu->autorelease();
		menu->Init();
		return menu;
	}
	CC_SAFE_DELETE(menu);
	return NULL;
}
//-------------------------------------------------------------------------------------
FBMenuLeft::FBMenuLeft( Level *level ) : FBTestMenuBase( level )
{
}
//-------------------------------------------------------------------------------------
void FBMenuLeft::Init()
{
    FBTestMenuBase::Init();
    
	CCLabelBMFont *a1 = CCLabelBMFont::labelWithString( "Login",            Config::LevelDebugMenuFont );
	CCLabelBMFont *a2 = CCLabelBMFont::labelWithString( "Logou",            Config::LevelDebugMenuFont );
	
    CCLabelBMFont *a3 = CCLabelBMFont::labelWithString( "Status",    Config::LevelDebugMenuFont );
	CCLabelBMFont *a4 = CCLabelBMFont::labelWithString( "Screenshot",Config::LevelDebugMenuFont );
	CCLabelBMFont *a5 = CCLabelBMFont::labelWithString( "level compl",	Config::LevelDebugMenuFont );
	CCLabelBMFont *a6 = CCLabelBMFont::labelWithString( "Random",      Config::LevelDebugMenuFont );
	
    CCLabelBMFont *a7 = CCLabelBMFont::labelWithString( "Is logged",        Config::LevelDebugMenuFont );
  	CCLabelBMFont *a8 = CCLabelBMFont::labelWithString( "Username",     Config::LevelDebugMenuFont );
    
	FXMenuItemLabel *ai1 = FXMenuItemLabel::itemWithLabel( a1, this, menu_selector( FBTestMenuBase::Menu_FBLogin ));
	FXMenuItemLabel *ai2 = FXMenuItemLabel::itemWithLabel( a2, this, menu_selector( FBTestMenuBase::Menu_FBLogout ));
	FXMenuItemLabel *ai3 = FXMenuItemLabel::itemWithLabel( a3, this, menu_selector( FBTestMenuBase::Menu_FBStatusUpdate ));
	FXMenuItemLabel *ai4 = FXMenuItemLabel::itemWithLabel( a4, this, menu_selector( FBTestMenuBase::Menu_FBScreenshotUpload ));
	FXMenuItemLabel *ai5 = FXMenuItemLabel::itemWithLabel( a5, this, menu_selector( FBTestMenuBase::Menu_FBActivity_LevelCompleted ));
	FXMenuItemLabel *ai6 = FXMenuItemLabel::itemWithLabel( a6, this, menu_selector( FBTestMenuBase::Menu_FBCustomPost ));
	FXMenuItemLabel *ai7 = FXMenuItemLabel::itemWithLabel( a7, this, menu_selector( FBTestMenuBase::Menu_FBIsLogged ));
	FXMenuItemLabel *ai8 = FXMenuItemLabel::itemWithLabel( a8, this, menu_selector( FBTestMenuBase::Menu_FBGetUsername ));

	MyInitWithItems( ai1, ai2, ai7, ai8, ai3, ai4, ai5, ai6, NULL );
	alignItemsVerticallyWithPadding( 25.0f );
}


//-------------------------------------------------------------------------------------
// FB Menu Left
//-------------------------------------------------------------------------------------
FBMenuRight* FBMenuRight::Create( Level *level )
{
	FBMenuRight* menu = new FBMenuRight( level );
	if ( menu )
	{
		menu->autorelease();
		menu->Init();
		return menu;
	}
	CC_SAFE_DELETE(menu);
	return NULL;
}
//-------------------------------------------------------------------------------------
FBMenuRight::FBMenuRight( Level *level ) : FBTestMenuBase( level )
{
    
}
//-------------------------------------------------------------------------------------
void FBMenuRight::Init()
{
    FBTestMenuBase::Init();
    
 	CCLabelBMFont *a1 = CCLabelBMFont::labelWithString( "Read score",       Config::LevelDebugMenuFont );
	CCLabelBMFont *a2 = CCLabelBMFont::labelWithString( "Post score",       Config::LevelDebugMenuFont );
 	CCLabelBMFont *a3 = CCLabelBMFont::labelWithString( "achievement",      Config::LevelDebugMenuFont );
	CCLabelBMFont *a4 = CCLabelBMFont::labelWithString( "friends",          Config::LevelDebugMenuFont );
   	CCLabelBMFont *a5 = CCLabelBMFont::labelWithString( "level skip",       Config::LevelDebugMenuFont );
    CCLabelBMFont *a6 = CCLabelBMFont::labelWithString( "post test",        Config::LevelDebugMenuFont );
   
	FXMenuItemLabel *ai1 = FXMenuItemLabel::itemWithLabel( a1, this, menu_selector( FBTestMenuBase::Menu_FBReadScore ));
	FXMenuItemLabel *ai2 = FXMenuItemLabel::itemWithLabel( a2, this, menu_selector( FBTestMenuBase::Menu_FBPostScore ));
	FXMenuItemLabel *ai3 = FXMenuItemLabel::itemWithLabel( a3, this, menu_selector( FBTestMenuBase::Menu_FBAchievement ));
	FXMenuItemLabel *ai4 = FXMenuItemLabel::itemWithLabel( a4, this, menu_selector( FBTestMenuBase::Menu_FBReadFriends ));
	FXMenuItemLabel *ai5 = FXMenuItemLabel::itemWithLabel( a5, this, menu_selector( FBTestMenuBase::Menu_FBActivity_LevelSkipped ));
	FXMenuItemLabel *ai6 = FXMenuItemLabel::itemWithLabel( a6, this, menu_selector( FBTestMenuBase::Menu_FBTestPost ));

	MyInitWithItems( ai1, ai2, ai3, ai4, ai5, ai6, NULL );
	alignItemsVerticallyWithPadding( 25.0f );
}


//-------------------------------------------------------------------------------------
// Test Menu
//-------------------------------------------------------------------------------------
TestMenu* TestMenu::Create( LevelTestNode *node )
{
	TestMenu* menu = new TestMenu( node );
	if ( menu )
	{
		menu->autorelease();
		menu->Init();
		return menu;
	}
	CC_SAFE_DELETE(menu);
	return NULL;
}
//-------------------------------------------------------------------------------------
TestMenu::TestMenu( LevelTestNode *node )
{
	_testNode = node;
}
//-------------------------------------------------------------------------------------
void TestMenu::Init()
{
	CCLabelBMFont *startLabel		= CCLabelBMFont::labelWithString( "Start",		Config::LevelDebugMenuFont );
	CCLabelBMFont *stopLabel		= CCLabelBMFont::labelWithString( "Stop",		Config::LevelDebugMenuFont );
	CCLabelBMFont *plusLabel		= CCLabelBMFont::labelWithString( "Count +",	Config::LevelDebugMenuFont );
	CCLabelBMFont *minusLabel		= CCLabelBMFont::labelWithString( "Count -",	Config::LevelDebugMenuFont );
	CCLabelBMFont *timePlusLabel	= CCLabelBMFont::labelWithString( "Time +",		Config::LevelDebugMenuFont );
	CCLabelBMFont *timeMinusLabel	= CCLabelBMFont::labelWithString( "Time -",		Config::LevelDebugMenuFont );

	FXMenuItemLabel *startItem		= FXMenuItemLabel::itemWithLabel( startLabel,		this, menu_selector( TestMenu::Menu_StartTest ));
	FXMenuItemLabel *stoptItem		= FXMenuItemLabel::itemWithLabel( stopLabel,		this, menu_selector( TestMenu::Menu_StopTest ));
	FXMenuItemLabel *plusItem		= FXMenuItemLabel::itemWithLabel( plusLabel,		this, menu_selector( TestMenu::Menu_ResetCountPlus ));
	FXMenuItemLabel *minusItem		= FXMenuItemLabel::itemWithLabel( minusLabel,		this, menu_selector( TestMenu::Menu_ResetCountMinus ));
	FXMenuItemLabel *timePlusItem	= FXMenuItemLabel::itemWithLabel( timePlusLabel,	this, menu_selector( TestMenu::Menu_DurationPlus ));
	FXMenuItemLabel *timeMinusItem	= FXMenuItemLabel::itemWithLabel( timeMinusLabel,	this, menu_selector( TestMenu::Menu_DurationMinus ));

	CCMenu *menu = CCMenu::menuWithItems( startItem, stoptItem, plusItem, minusItem, timePlusItem, timeMinusItem, NULL );
	menu->alignItemsVerticallyWithPadding( 25.0f );
	menu->setPosition( ccp( 0.0f, 0.0f ));

	addChild( menu, 10 );
	
	_resetCountLabel = CCLabelBMFont::labelWithString( "", Config::LevelDebugMenuFont );
	_resetCountLabel->setPosition( ccp( 100.0f, -10.0f ));
	addChild( _resetCountLabel , 10 );
	UpdateResetCount();

	_durationLabel = CCLabelBMFont::labelWithString( "", Config::LevelDebugMenuFont );
	_durationLabel->setPosition( ccp( 100.0f, -130.0f ));
	addChild( _durationLabel, 10 );
	UpdateDuration();
}
//-------------------------------------------------------------------------------------
void TestMenu::Menu_StartTest( CCObject *sender )
{
	_testNode->StartTesting();
}
//-------------------------------------------------------------------------------------
void TestMenu::Menu_StopTest( CCObject *sender )
{
	_testNode->StopTesting();
}
//-------------------------------------------------------------------------------------
void TestMenu::Menu_ResetCountPlus( CCObject *sender )
{
	if ( LevelTest::_sResetCount >= 10 )
		LevelTest::_sResetCount += 5;
	else
		LevelTest::_sResetCount++;

	UpdateResetCount();
}
//-------------------------------------------------------------------------------------
void TestMenu::Menu_ResetCountMinus( CCObject *sender )
{
	if ( LevelTest::_sResetCount > 1 )
	{
		if ( LevelTest::_sResetCount <= 10 )
			LevelTest::_sResetCount--;
		else
			LevelTest::_sResetCount -= 5;
		UpdateResetCount();
	}
}
//-------------------------------------------------------------------------------------
void TestMenu::Menu_DurationMinus( CCObject *sender )
{
	if ( LevelTest::_sTestTime <= 2 )
	{}
	else if ( LevelTest::_sTestTime <= 10 )
		LevelTest::_sTestTime -= 1;
	else if ( LevelTest::_sTestTime <= 60 )
		LevelTest::_sTestTime -= 5;
	else 
		LevelTest::_sTestTime -= 60;

	UpdateDuration();
}
//-------------------------------------------------------------------------------------
void TestMenu::Menu_DurationPlus( CCObject *sender )
{
	if ( LevelTest::_sTestTime < 10 )
		LevelTest::_sTestTime += 1;
	else if ( LevelTest::_sTestTime < 60 )
		LevelTest::_sTestTime += 5;
	else 
		LevelTest::_sTestTime += 60;

	UpdateDuration();
}
//-------------------------------------------------------------------------------------
void TestMenu::UpdateResetCount()
{
	if ( ! _resetCountLabel )
		return;

	stringstream ss;
	ss << LevelTest::_sResetCount;

	_resetCountLabel->setString( ss.str().c_str() );
}
//-------------------------------------------------------------------------------------
void TestMenu::UpdateDuration()
{
	if ( ! _durationLabel )
		return;

	stringstream ss;
	ss << LevelTest::_sTestTime;

	_durationLabel->setString( ss.str().c_str() );
}
//-------------------------------------------------------------------------------------

#endif
