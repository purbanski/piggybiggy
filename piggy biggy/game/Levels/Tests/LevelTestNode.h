#ifdef BUILD_TEST

#ifndef __LEVELTESTNODE_H__
#define __LEVELTESTNODE_H__

#include <set>
#include <map>
#include "cocos2d.h"
#include "Debug/MemoryDebug.h"

USING_NS_CC;
using namespace std;

class Level;
class puBlock;
class LevelTestNode;
//--------------------------------------------------------------------------------
class TestMenu : public CCNode
{
public:
	static TestMenu* Create( LevelTestNode *level );

	void Menu_ResetCountPlus( CCObject *sender );
	void Menu_ResetCountMinus( CCObject *sender );
	void Menu_DurationPlus( CCObject *sender );
	void Menu_DurationMinus( CCObject *sender );
	void Menu_StartTest( CCObject *sender );
	void Menu_StopTest( CCObject *sender );

private:
	TestMenu( LevelTestNode *level );
	void Init();
	void UpdateResetCount();
	void UpdateDuration();

private:
	LevelTestNode	*_testNode;
	CCLabelBMFont	*_resetCountLabel;
	CCLabelBMFont	*_durationLabel;
};
//--------------------------------------------------------------------------------
class AnimRightMenu : public CCMenu
{
public:
	static AnimRightMenu* Create( Level *level );

	void Menu_A1( CCObject *sender );
	void Menu_A2( CCObject *sender );
	void Menu_A3( CCObject *sender );
	void Menu_A4( CCObject *sender );
	void Menu_A5( CCObject *sender );
	void Menu_A6( CCObject *sender );
	void Menu_A7( CCObject *sender );
	void Menu_A8( CCObject *sender );
	void Menu_A9( CCObject *sender );

private:
	AnimRightMenu( Level *level );
	void Init();

	puBlock* GetPiggy();
	void MyInitWithItems( CCMenuItem* item, ...);

private:
	Level			*_level;
};
//--------------------------------------------------------------------------------
class AnimLeftMenu : public CCMenu
{
public:
	static AnimLeftMenu* Create( Level *level );

	void Menu_A1( CCObject *sender );
	void Menu_A2( CCObject *sender );
	void Menu_A3( CCObject *sender );
	void Menu_A4( CCObject *sender );
	void Menu_A5( CCObject *sender );
	void Menu_A6( CCObject *sender );
	void Menu_A7( CCObject *sender );
	void Menu_A8( CCObject *sender );
	void Menu_A9( CCObject *sender );

private:
	AnimLeftMenu( Level *level );
	void Init();

	puBlock* GetPiggy();
	void MyInitWithItems( CCMenuItem* item, ...);

private:
	Level			*_level;
};


//--------------------------------------------------------------------------------
// FB Menu Base
//--------------------------------------------------------------------------------
class FBTestMenuBase : public CCMenu
{
public:
	void Menu_FBLogin( CCObject *sender );
	void Menu_FBLogout( CCObject *sender );
	void Menu_FBStatusUpdate( CCObject *sender );
	void Menu_FBScreenshotUpload( CCObject *sender );
	
    void Menu_FBActivity_LevelCompleted( CCObject *sender );
    void Menu_FBActivity_LevelSkipped( CCObject *sender );
    void Menu_FBTestPost( CCObject *sender );
    
 	void Menu_FBAchievement( CCObject *sender );
    void Menu_FBCustomPost( CCObject *sender );
    void Menu_FBIsLogged( CCObject *sender );
    void Menu_FBGetUsername( CCObject *sender );
    void Menu_FBReadScore( CCObject *sender );
    void Menu_FBPostScore( CCObject *sender );
    void Menu_FBReadFriends( CCObject *sender );
    
protected:
	FBTestMenuBase( Level *level );
	void Init();
	void MyInitWithItems( CCMenuItem* item, ...);

protected:
	Level			*_level;
};
//--------------------------------------------------------------------------------
class FBMenuLeft : public FBTestMenuBase
{
public:
	static FBMenuLeft* Create( Level *level );

private:
	FBMenuLeft( Level *level );
   	void Init();
};
//--------------------------------------------------------------------------------
class FBMenuRight : public FBTestMenuBase
{
public:
	static FBMenuRight* Create( Level *level );

private:
	FBMenuRight( Level *level );
   	void Init();
};


//--------------------------------------------------------------------------------
class LevelTest : public CCNode
{
public:
	static float	_sTestTime;
	static int		_sTestRun;
	static int		_sResetCount;
	static int		_sCurrentLevelTestRun;

	static LevelTest* Create( Level *level );
	~LevelTest();
	void Step( ccTime dTime );

private:
	LevelTest( Level *level );
	void Init();
	void UpdateContainer();
	void ExecuteTouch();
	void WriteLog();
	void ImDone();
	void ForceLevelCompletion();
    void PlayNextLevel();

private:
	Level		*_level;

	float	_counter;
	float	_heartbeat;
	typedef set<puBlock*>	BlockSet;
	typedef map<float,bool>	FloatSet;

	BlockSet	_blocks;
	BlockSet	_piggys;
	BlockSet	_coins;
	FloatSet	_actionTime;
	bool		_sceneChange;
	bool		_levelComplitionForced;
};
//--------------------------------------------------------------------------------
class LevelTestNode : public CCNode
{
public:
	static bool		_sTesting;
	static time_t	_sStartTime;

	static LevelTestNode* Create( Level *level );
	~LevelTestNode();

	void StartTesting();
	void StopTesting();

	void Menu_Debug( CCObject* pSender );
	void Menu_MemoryToggle( CCObject* pSender );
	void Menu_TestToggle( CCObject* pSender );
	void Menu_ResetAll( CCObject* pSender );
	void Menu_ResetSnd( CCObject* pSender );
    
	void Menu_ShowAnimMenu( CCObject* pSender );
    void Menu_ShowFacebookMenu( CCObject* pSender );
    
private:
	LevelTestNode( Level *level );
	void Init();

private:
	Level			*_level;
	CCMenu			*_menu;
	MemoryDebug		*_memDebug;
    
	AnimLeftMenu	*_animLeftMenu;
	AnimRightMenu	*_animRightMenu;
    FBMenuLeft      *_fbMenuLeft;
    FBMenuRight     *_fbMenuRight;
    
	TestMenu		*_testMenu;
	LevelTest		*_test;
};
//--------------------------------------------------------------------------------

//--------------------------------------------------------------------------------
#endif

#endif
