#ifndef __LEVELORIENTMSG_H__
#define __LEVELORIENTMSG_H__

#include "cocos2d.h"
#include "GameTypes.h"

USING_NS_CC;

//-------------------------------------------------------------------------
class LevelOrientMsg : public CCNode
{
public:
	static LevelOrientMsg* Create( GameTypes::LevelOrientation orient );
	~LevelOrientMsg();

	void Init();
	void AcceratorNotify( CCAcceleration* acceleration );

	void Enable();
	void Disable();

protected:
	typedef enum
	{
		eState_Shown = 1,
		eState_Showing,
		eState_Hidden,
		eState_Hiding
	} States;

	LevelOrientMsg(  GameTypes::LevelOrientation orient );

	void OrientSpriteAnimStart();
	void OrientSpriteAnimStop();
	void OrientSpriteAnimStopImediate();
	void SetState( CCNode *node, void *data );

	void CleanAction( CCNode *parent, int actionTag );
	void Step(ccTime dTime );

private:
	GameTypes::LevelOrientation		_orientMode;
	CCSprite						*_iphoneSprite;
    CCSprite						*_holdThisWaySprite;

	double							_timestampInit;
	States							_state;
	bool							_enabled;
	bool							_enableInitialized;
	ccTime							_initDelay;

	static float _sFadeDur;
    static float _sDisplayDuration;
};
//-------------------------------------------------------------------------
#endif
