#ifndef __POCKETS_H__
#define __POCKETS_H__

#include <map>
#include <set>
#include <list>
#include "cocos2d.h"
#include <pu/ui/SlidingNode.h>
#include "GameTypes.h"
#include "Components/TrippleSprite.h"

using namespace std;
USING_NS_CC;

//------------------------------------------------------------------------------
class PocketStats 
{
public:
	PocketStats();

	int _levelsCompletedCount;
	int	_levelsTotalCount;
	int _levelsLockCount;
	int _levelsSkipCount;

	int _totalTime;
};
//------------------------------------------------------------------------------
class PocketSpritesNames
{
public:
	//PocketSprites();

	//string _pocketSpriteBigUnlocked;
	//string _pocketSpriteBigLocked;
	string _pocketSpriteUnlockAnim;
	string _pocketSpriteSelectMenu;
	string _selectPocketBg;
   	string _pocketAnimBg;
};
//------------------------------------------------------------------------------
class PocketManager : public CCNode
{
public:
	//--------------
	// Type defs

	//--------------
	// Pocket to Level
	typedef list<GameTypes::LevelEnum> PocketLevels;
	typedef map<GameTypes::PocketType, PocketLevels> PocketToLevelMap;

	//--------------
	// Pocket Sprites
	typedef map<GameTypes::PocketType, PocketSpritesNames> PocketSpritesMap;


	//--------------
	// Pocket releated
	typedef list<GameTypes::PocketType> PocketList;

	static PocketManager* Get();
	~PocketManager();

	//--------------
	// Sprites Getters
	//CCSprite* GetSpritePocketBigUnlocked( GameTypes::PocketType pocket );
	//CCSprite* GetSpritePocketBigLocked( GameTypes::PocketType pocket );
	CCSprite* GetSpritePocketUnlockAnim( GameTypes::PocketType pocket );
	CCSprite* GetSpritePocketSelect( GameTypes::PocketType pocket );
	CCSprite* GetSpritePocketSelectLocked( GameTypes::PocketType pocket );
	CCSprite* GetSelectPocketBackground( GameTypes::PocketType pocket );
   	CCSprite* GetPocketAnimBackground( GameTypes::PocketType pocket );
	//CCSprite* GetSelectLevelBackground( GameTypes::PocketType pocket );


	//--------------
	// Pocket Getters
	PocketLevels GetLevelsFromPocket( GameTypes::PocketType pocket );
	PocketStats GetPocketStats( GameTypes::PocketType pocket );
	PocketList GetPocketList();
	GameTypes::PocketType LevelInWhichPocket( GameTypes::LevelEnum levelEnum );

	//--------------
	// Other Getters
	bool IsLevelInPocket( GameTypes::LevelEnum level, GameTypes::PocketType pocket );
    bool IsLevelFirstInPocket( GameTypes::LevelEnum level, GameTypes::PocketType pocket );
    
	CCNode* GetLevelListNode( GameTypes::PocketType pocket, SlidingNode *slidingNode );
	void SelectLevel( CCObject* sender );

private:
	PocketManager();

	void SetPocketsOrder();
	void SetBluePocket();
	void SetMoroPocket();
	void SetRastaPocket();
	void SetJapanPocket();
	void SetSciencePocket();

	CCMenuItem* GetMenuItem( GameTypes::LevelEnum level );
	CCNode* GetCoinsScore( GameTypes::LevelEnum level );
	CCNode* GetTimeLabel( GameTypes::LevelEnum level );
	CCNode* GetLevelLabel( GameTypes::LevelEnum level );

	TrippleSprite GetLevelSkippedSprite();
	TrippleSprite GetLevelCompletedSprite( GameTypes::LevelEnum level );

private:
	static PocketManager*	_sInstance;

	PocketList			_pocketList;
	PocketToLevelMap	_pocketToLevelMap;
	PocketSpritesMap	_pocketSpritesMap;
};
//------------------------------------------------------------------------------
#endif
