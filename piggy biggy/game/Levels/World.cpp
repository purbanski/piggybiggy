#include <Box2D/Box2D.h>
#include "cocos2d.h"

#include "World.h"
#include "GLES-Render.h"
#include "GameConfig.h"


World *World::_sGWorld = NULL;
//-------------------------------------------------------------------------------------
World *World::Get()
{
	assert ( _sGWorld != NULL );
	return ( _sGWorld ) ;
}

//-------------------------------------------------------------------------------------
World::World( bool doSleep ) : _world( b2Vec2( 0.0f, -10.0f ), doSleep )
{
	_world.SetDebugDraw( GLESDebugDraw::Get() );

	b2BodyDef groundBodyDef;
	groundBodyDef.position.Set( 0.0f, 0.0f );
	
	b2PolygonShape	groundBox;
	groundBox.SetAsEdge(b2Vec2(-40.0f, 10.0f), b2Vec2(40.0f, 0.0f));
	_groundBody = _world.CreateBody( &groundBodyDef );

	_timeStep = 1.0f / 30.0f;
	_velocityIterations = Config::WorldVelocityIterations;
	_positionIterations = Config::WorldPositionIterations;

	_sGWorld = this;
}
//-------------------------------------------------------------------------------------
World::~World()
{
	_world.DestroyBody( _groundBody );
	_world.SetContactListener( NULL );
	_world.SetDestructionListener( NULL );
}
//-------------------------------------------------------------------------------------
void World::Step()
{
	_world.Step( _timeStep, _velocityIterations, _positionIterations );
	_world.ClearForces();
}
//-------------------------------------------------------------------------------------
void World::DrawDebugData()
{
	GLESDebugDraw::Get()->LockGL();
	_world.DrawDebugData();
	GLESDebugDraw::Get()->UnlockGL();
}
//-------------------------------------------------------------------------------------
void World::SetIterations( int32 velocity, int position )
{
	_velocityIterations = velocity;
	_positionIterations = position;
}
