#ifndef __SCORECOUNTER_H__
#define __SCORECOUNTER_H__

#include "GameTypes.h"
#include "cocos2d.h"
#include <stdio.h>
#include <time.h>

USING_NS_CC;

class FinalTimeAnimation;
class Level;
//---------------------------------------------------------------------------
class NodeAndAction 
{
public:
	NodeAndAction();
	NodeAndAction( CCNode* node, CCAction* action );

	CCNode		*_node;
	CCAction	*_action;
};
//---------------------------------------------------------------------------
class NodeAndActions : public vector<NodeAndAction>
{
public:
	void push_back( const NodeAndAction &node );
	void clear();
	void releaseAll();
};
//---------------------------------------------------------------------------

class ScoreCounter : public CCNode
{
public:
    static float _yCoins;
    
	static ScoreCounter* Create( Level *level );

	~ScoreCounter();

	void Step( cocos2d::ccTime dt );

	void Start();
	void Pause();
	void Stop();
	void StopQuiet();
    void ShowAnim();

	void DisplayCoinScore();
	void DisplayTimeScore();
	void ShowLevelMenu();
	void PlayCoinScore();
	void PlayCoinScoreReverse();
	CCNode* GetCoinsRemain();

	void SetOrientation( GameTypes::LevelOrientation orient );
	void SetMute( bool mute ) { _mute = mute ;}

private:
	ScoreCounter( Level *level );
	void Init();
	void Update();
	bool ProcessTime();
	void ProcessRemainCoins();
	void CleanUpRemainBigCoins();
	void HideRemainCoinsNode();
    void ReleaseAllActions();

    void AddCoinEffect(CCNode*, void *data);
    
	void StartStopperAnim();
	void StopStopperAnim();

	bool IsLevelDone();
	CCNode* GetCoinScoreNode();

private:
	typedef vector<CCSprite *>	Sprites;

	Sprites			_coinsRemainSprites;
	NodeAndActions	_remainCoinsContainer;

	Level		*_level;
//	CCSprite	*_coinsRemainBg;
//	CCAction	*_coinsRemainBgA1;
//	CCAction	*_coinsRemainBgA2;
    
    CCAction    *_pointerFadeOutAction;
    CCAction    *_pointerFastFadeOutAction;
    CCAction    *_stopperMiddleDotFadeOutAction;
    CCAction    *_counterSpriteFadeOutAction;
    CCAction    *_runDisplayTimeAction;
    
    typedef     vector<CCAction *>  ActionVector;
    ActionVector    _coinsFadeOutActions;
    
    
	CCNode		*_remainSmallCoinsNode;
	CCNode		*_remainBigCoinsNode;

	CCNode				*_finalScore;
	FinalTimeAnimation	*_finalTime;

	CCPoint		_finalScorePos;
	CCPoint		_stopperFinalPos;

	CCSprite	*_pointer;
	CCSprite	*_pointerFast;
	CCAction	*_pointerAction;
	CCAction	*_pointerFastAction;
	CCSprite	*_counterSprite;
	CCSprite	*_stopperMiddleDot;

	time_t		_startTime;
	bool		_stopCalled;

	bool		_mute;
};
//---------------------------------------------------------------------------
class FinalTimeSingleAnimData 
{
public:
	FinalTimeSingleAnimData();
	FinalTimeSingleAnimData( CCNode *node, int finalDigit, int index, int *frameWaitTotal );

	int		_index;
	int		_finalDigit;
	int		*_frameWaitTotal;
	CCNode	*_node;
};
//---------------------------------------------------------------------------
class FinalTimeAnimation : public CCNode
{
public:
	static FinalTimeAnimation* Get( ScoreCounter *scoreConter );
	~FinalTimeAnimation();
	void Start();
	void SingleDigitAnimCallback();
	void PlayTick();
	void PlayFinalTick();
	void ShowColon( CCObject *sender, void *data );


private:
	FinalTimeAnimation( ScoreCounter *scoreConter );
	void Init();

	void GetRandomDigitAnim( CCNode *node, int finalDigit, int index );

	string GetRandomDigit();
	void PlayTickTimeSound();

private:
	typedef vector<FinalTimeSingleAnimData>	DigitDataContainer;
	DigitDataContainer				_digitsContainer;
	DigitDataContainer::iterator	_currentDigit;
	ScoreCounter					*_scoreCounter;

	CCNode *_hoursNode;
	CCNode *_minutesNodeA;
	CCNode *_minutesNodeB;
	CCNode *_secondsNodeA;
	CCNode *_secondsNodeB;
	CCSprite *_colon1;
	CCSprite *_colon2;

};
//---------------------------------------------------------------------------
#endif
