#include <sstream>
#include "ClueManager.h"
#include "GameConfig.h"
#include "Debug/MyDebug.h"
#include "Tools.h"
#include "Game.h"
#include "SoundEngine.h"
#include "pu/ui/SlidingNode.h"

//------------------------------------------------------------------------------------------------
// ClueManager
//------------------------------------------------------------------------------------------------
ClueManager::ClueManager()
{
	_timerLabel = CCLabelBMFont::labelWithString( "", Config::LevelClueTimerFont );
	_timerLabel->setPosition( ccp ( -180.0f, 130.0f ));
	addChild( _timerLabel );

	_timeout = Config::ClueManagerTickDelay;

	CCLabelBMFont *label = CCLabelBMFont::labelWithString("clue", Config::LevelSubMenuFont );
	CCMenuItemLabel *showClueButton = CCMenuItemLabel::itemWithLabel( label, this, menu_selector( ClueManager::Menu_ShowClue ));
	_menu = CCMenu::menuWithItem( showClueButton );

	_menu->setIsTouchEnabled( false );
	_menu->setIsVisible( false );
	_menu->setOpacity( Config::LevelButtonOpacity );
	_menu->setPosition( ccp ( 120.0f, 135.0f ));
	addChild( _menu );

	_clueScreen = new ClueScreen();
	_clueScreen->autorelease();
	_clueScreen->retain();
}
//------------------------------------------------------------------------------------------------
ClueManager::~ClueManager()
{
	_clueScreen->release();
}
//------------------------------------------------------------------------------------------------
void ClueManager::SetTimer( int timer )
{
	stringstream ss;

	ss << "clue in: ";
	ss << timer;
	_timerLabel->setString( ss.str().c_str() );
}
//------------------------------------------------------------------------------------------------
void ClueManager::SetClues( ClueList clues )
{
	// this logic belowe is so convoluted
	// would be nice to sort it out
	GameStatus *gameStatus;
	GameStatus::LevelClueStatus clueStatus;
	
	gameStatus = Game::Get()->GetGameStatus();
	clueStatus = gameStatus->GetCurrentLevelClueStatus();

	//----------------------------------------------------------------------------//
	unsigned int clueIndex = 0 ;
	unsigned int cluesSize = clues.size();  // it can change while processing

	for ( clueIndex = 0; ( clueIndex < clueStatus._clueProcessed && clueIndex < cluesSize ); clueIndex++ )
	{
		_cluesGot.push_back( clues.front() );
		clues.pop_front();
	}
	_clues = clues;


	// Are there any clues processed already
	if ( _cluesGot.size() )	
		ShowClueButton();

	// Are there any clues to be processed
	if ( _clues.size() )
	{
		if (( _cluesGot.size() == 0 ) || ( gameStatus->GetCurrentLevelClueStatus()._running ))
		{
			if ( gameStatus->GetCurrentLevelClueStatus()._timeout != -1 )
				_clues.front()->_timeout = gameStatus->GetCurrentLevelClueStatus()._timeout;

			_isTimerRunning = true;

			SetTimer( _clues.front()->_timeout );
			schedule( schedule_selector( ClueManager::Tick ));
		}
		else
			_isTimerRunning = false;
	}
}
//------------------------------------------------------------------------------------------------
void ClueManager::Tick( cocos2d::ccTime dt )
{
	return;
	if ( _timeout > 0 )
	{
		_timeout--;
		return;
	}
	_timeout = Config::ClueManagerTickDelay;

	SetTimer( _clues.front()->_timeout );
	_clues.front()->_timeout--;

	if ( ! _clues.front()->_timeout )
	{
		ProcessClue();
	}
}
//------------------------------------------------------------------------------------------------
void ClueManager::ProcessClue()
{
	_isTimerRunning = false;

	SoundEngine::Get()->PlayEffect( SoundEngine::eSoundNewClue );
	Game::Get()->GetRuningLevel()->GetParticleEngine()->Effect_NewClue( Game::Get()->GetRuningLevel(), b2Vec2( 63.0f, 52.0f ) );

	ShowClueButton();

	_cluesGot.push_back( _clues.front() );
	_clues.pop_front();
	UpdateGameStatus();

	HideClueTimer();
	unschedule( schedule_selector( ClueManager::Tick ));
}
//------------------------------------------------------------------------------------------------
void ClueManager::Menu_ShowClue( CCObject *sender )
{
	Level *level;
	level = Game::Get()->GetRuningLevel();
	
	level->LevelPause();
//	level->EnableMenuButton( false );
	
	_clueScreen->Show( _cluesGot );
	_isTimerRunning = true;
}
//------------------------------------------------------------------------------------------------
bool ClueManager::IsBusy()
{
	if ( _clues.size() || _cluesGot.size() )
		return true;
	else 
		return false;
}
//------------------------------------------------------------------------------------------------
void ClueManager::PauseTimer()
{
	_menu->setIsTouchEnabled( false );
	unschedule( schedule_selector( ClueManager::Tick ));
}
//------------------------------------------------------------------------------------------------
void ClueManager::ResumeTimer()
{
	if ( _menu->getIsVisible() )
		_menu->setIsTouchEnabled( true );

	if ( ! ( _clues.size() ))
		return;

	if ( ! ( _isTimerRunning ))
		return;

	SetTimer( _clues.front()->_timeout );
	_timerLabel->setIsVisible( true );
	_timerLabel->setOpacity( Config::LevelButtonOpacity  );
	schedule( schedule_selector( ClueManager::Tick ));
}
//------------------------------------------------------------------------------------------------
void ClueManager::Reset()
{
	_clues.release();
	_cluesGot.release();
}
//------------------------------------------------------------------------------------------------
void ClueManager::ShowClueButton()
{
	// do nothing if it is already visible
	if ( _menu->getIsVisible() )
		return;

	// if there already any clues processed, skip animation
	if ( _cluesGot.size() )
	{
		_menu->setIsVisible( true );
		_menu->setIsTouchEnabled( true );
		return; 
	}

	CCFadeTo *fadeIn = new CCFadeTo();
	fadeIn->autorelease();
	fadeIn->initWithDuration( 1.0f, Config::LevelButtonOpacity );
	
	_menu->setOpacity( 0 );
	_menu->setIsVisible( true );
	_menu->setIsTouchEnabled( true );
	_menu->runAction( fadeIn );
}
//------------------------------------------------------------------------------------------------
void ClueManager::UpdateGameStatus()
{
	GameStatus *gameStatus;
	GameStatus::LevelClueStatus clueStatus;
	
	clueStatus._clueProcessed = _cluesGot.size();
	clueStatus._running = _isTimerRunning;

	if ( _clues.size() )
		clueStatus._timeout = _clues.front()->_timeout;
	
	gameStatus = Game::Get()->GetGameStatus();
	gameStatus->SetCurrentLevelClueStatus( clueStatus );
}
//------------------------------------------------------------------------------------------------
void ClueManager::HideClueTimer()
{
	CCFadeTo *fadeOut = new CCFadeTo();
	fadeOut->autorelease();
	fadeOut->initWithDuration( 0.6f, 0 );
	_timerLabel->runAction( fadeOut );
}
//------------------------------------------------------------------------------------------------





//------------------------------------------------------------------------------------------------
// Clue
//------------------------------------------------------------------------------------------------
Clue* Clue::node( int timeout, const char *clue )
{
	Clue *ret = new Clue( timeout, clue );
	if ( clue )
	{
		ret->autorelease();
		ret->retain();
		return ret;
	}

	return NULL;
}
//------------------------------------------------------------------------------------------------
Clue::Clue( int timeout, const char *clue )
{
	_timeout = timeout;
	_clue.append( clue );

	CCLabelBMFont *label = CCLabelBMFont::labelWithString( clue, Config::DefaultClueFont );
	addChild( label, 10 );
}
//------------------------------------------------------------------------------------------------




//------------------------------------------------------------------------------------------------
// ClueScreen
//------------------------------------------------------------------------------------------------
ClueScreen::ClueScreen()
{
	CCLabelBMFont *exitLabel = CCLabelBMFont::labelWithString( "exit", Config::LevelSubMenuFont );
	CCMenuItemLabel *exitButton = CCMenuItemLabel::itemWithLabel( exitLabel, this, menu_selector( ClueScreen::Menu_Exit ));
	CCMenu *menu = CCMenu::menuWithItem( exitButton );

	menu->setPosition( ccp( 180.0f, -120.0f ));
	addChild( menu, 15 );

	_background = CCSprite::spriteWithFile(  Skins::GetSkinName( "Images/Other/bgShowClue.png" ) );
	_background->retain();
	_background->setOpacity( 240 );
	addChild( _background, 5);

	setPosition( ccp( 0.0f, 320.0f ));
}
//------------------------------------------------------------------------------------------------
ClueScreen::~ClueScreen()
{
	_background->release();
	removeAllChildrenWithCleanup( true );
}
//------------------------------------------------------------------------------------------------
void ClueScreen::Show( ClueList clues )
{
	stopActionByTag( eAction_Hide );

	CleanUp();
	_clueList = clues;

	_slidingNode = SlidingNode::node( _clueList.size() );
	_slidingNode->SetInitPosition( ccp ( 0.0f, 0.0f ));
	_slidingNode->SetIndex( _clueList.size() - 1 );

	_clueList.AddToNode( _slidingNode );
	addChild( _slidingNode, 10 );

	Game::Get()->GetRuningLevel()->addChild( this, GameTypes::eLevelClueScreen );
	
 	CCMoveTo *moveTo = new CCMoveTo();
	moveTo->autorelease();
	moveTo->initWithDuration( 0.6f, ccp( 0.0f, 0.0f ) );
	moveTo->setTag( eAction_Show );
	
	runAction( moveTo );
}
//------------------------------------------------------------------------------------------------
void ClueScreen::Hide()
{
	stopActionByTag( eAction_Show );

	_slidingNode->removeFromTouchDispatcher();

	CCMoveTo *moveTo = new CCMoveTo();
	moveTo->autorelease();
	moveTo->initWithDuration( 0.6f, ccp( 0.0f, 320.0f ));
	moveTo->setTag( eAction_Hide );

	runAction( moveTo );

	Game::Get()->GetRuningLevel()->LevelRun();
}
//------------------------------------------------------------------------------------------------
void ClueScreen::Menu_Exit( CCObject *sender )
{
	Hide();
}
//------------------------------------------------------------------------------------------------
void ClueScreen::CleanUp()
{
	if ( getParent() )
	{
		_clueList.removeFromParentAndCleanup( false );
		_slidingNode->removeFromParentAndCleanup( true );
		removeFromParentAndCleanup( false );
	}
}
//------------------------------------------------------------------------------------------------




//------------------------------------------------------------------------------------------------
// ClueList
//------------------------------------------------------------------------------------------------
void ClueList::Debug()
{
	D_LOG("size: %d", (int)size() );
	
	int count =1;
	for ( ClueList::iterator it = begin(); it != end(); it++ )
	{
		D_LOG("%d %d %s", count, (*it)->_timeout, (*it)->_clue.c_str() );
	}
}
//------------------------------------------------------------------------------------------------
void ClueList::removeFromParentAndCleanup( bool cleanup )
{
	for ( iterator it = begin(); it != end(); it++ )
	{
		(*it)->removeFromParentAndCleanup( cleanup );
	}
}
//------------------------------------------------------------------------------------------------
void ClueList::AddToNode( CCNode *node )
{
	//iphone
	float dx = 480.0f;
	int countx = 0;

	for ( iterator it = begin(); it != end(); it++ )
	{
		node->addChild( *it );
		(*it)->setPosition (ccp ( dx * countx, 0.0f ));
		countx++;
	}
}
//------------------------------------------------------------------------------------------------
void ClueList::release()
{
	for ( iterator it = begin(); it != end(); it++ )
	{
		(*it)->release();
	}

	clear();
}

//------------------------------------------------------------------------------------------------
