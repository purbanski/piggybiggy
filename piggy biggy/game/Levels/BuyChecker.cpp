#include "BuyChecker.h"
#include "Game.h"
#include "GameStatus.h"
#include "GameConfig.h"
#include "Scenes/BuyScene.h"
//-----------------------------------------------------------------------------------
BuyChecker::BuyChecker()
{
}
//-----------------------------------------------------------------------------------
bool BuyChecker::IsPurchaseNeeded( GameTypes::LevelEnum levelEnum )
{
	int index;
	GameLevelSequence &sequnce = Game::Get()->GetGameStatus()->GetLevelSequence();
	//sequnce.Dump();
	index = sequnce.GetLevelIndex( levelEnum ) + 1;

	if (( index > Config::FreeLevelCount ) && ( ! Game::Get()->GetGameStatus()->HasBeenBought() ))
		return true;
	else
		return false;
}
//-----------------------------------------------------------------------------------
bool BuyChecker::BuyIfNeeded( GameTypes::LevelEnum level )
{
	if ( IsPurchaseNeeded( level ))
	{
		BuyScene::ShowScene();
		return true;
	}
	return false;
}

//-----------------------------------------------------------------------------------
