#include "ParticleEngine.h"
#include "GameConfig.h"
#include "Level.h"
#include "Debug/MyDebug.h"
#include "Game.h"

//----------------------------------------------------------------------------------------------
ParticleEngine::ParticleEngine()
{
	_effectsList.clear();
}
//----------------------------------------------------------------------------------------------
ParticleEngine::~ParticleEngine()
{

}
//----------------------------------------------------------------------------------------------
void ParticleEngine::Update()
{
	EffectsList::iterator it;
	for ( it = _effectsList.begin(); it != _effectsList.end(); )
	{
		if ( it->tick-- == 0 )
		{
			if ( it->node && it->node->getParent() )
			{	
				it->node->removeFromParentAndCleanup( true );
				it = _effectsList.erase( it );
			}
		}
		else it++;
	}
}
//----------------------------------------------------------------------------------------------
void ParticleEngine::Effect_PocketOpen( CCNode *level, b2Vec2 pos )
{
	CCParticleSystemQuad* emitter = new CCParticleSystemQuad();
	emitter->initWithTotalParticles(50);
	emitter->autorelease();

	emitter->setTexture( CCTextureCache::sharedTextureCache()->addImage(  Skins::GetSkinName( "Images/Particles/stars.png" )  ));
	emitter->setDuration( 0.5f );

	emitter->setGravity(CCPointZero);

	emitter->setAngle(90);
	emitter->setAngleVar(360);

	emitter->setSpeed(160);
	emitter->setSpeedVar(120);

	emitter->setRadialAccel(-120);
	emitter->setRadialAccelVar(0);

	emitter->setTangentialAccel(30);
	emitter->setTangentialAccelVar(0);

	emitter->setLife(1.5f);
	emitter->setLifeVar(1);

	emitter->setStartSpin(0);
	emitter->setStartSizeVar(0);
	emitter->setEndSpin(0);
	emitter->setEndSpinVar(0);

	// color of particles
	ccColor4F startColor = {0.5f, 0.5f, 0.5f, 1.0f};
	emitter->setStartColor(startColor);

	ccColor4F startColorVar = {0.5f, 0.5f, 0.5f, 1.0f};
	emitter->setStartColorVar(startColorVar);

	ccColor4F endColor = {0.1f, 0.1f, 0.1f, 1.0f};
	emitter->setEndColor(endColor);

	ccColor4F endColorVar = {0.1f, 0.1f, 0.1f, 1.0f};	
	emitter->setEndColorVar(endColorVar);

	// size, in pixels
	emitter->setStartSize(60.0f);
	emitter->setStartSizeVar(30.0f);
	emitter->setEndSize(kParticleStartSizeEqualToEndSize);

	// emits per second
	//m_emitter->setEmissionRate(m_emitter->getTotalParticles()/m_emitter->getLife());
	emitter->setEmissionRate(60);

	// additive
	emitter->setIsBlendAdditive(false);

	emitter->setPosVar(CCPointZero);
	//emitter->setPosition( ccpSub( ccp( pos.x * RATIO, pos.y * RATIO ), level->convertToWorldSpace(CCPointZero)));
	emitter->setPosition( ccpSub( ccp( pos.x * RATIO, pos.y * RATIO ), level->convertToWorldSpace(CCPointZero)));

	level->addChild( emitter, GameTypes::eSpritesEffects );

	_effectsList.push_back( EffectRecord( emitter, 80 ) );
}
//----------------------------------------------------------------------------------------------
void ParticleEngine::Effect_Stars( CCNode *level, b2Vec2 pos)
{
#ifdef BUILD_EDITOR4ALL
	return;
#endif

	CCParticleSystemQuad* emitter = new CCParticleSystemQuad();
	emitter->initWithTotalParticles(50);
	emitter->autorelease();

	emitter->setTexture( CCTextureCache::sharedTextureCache()->addImage(  Skins::GetSkinName( "Images/Particles/Star.png" )  ));
	emitter->setDuration( 0.5f );

	emitter->setGravity(CCPointZero);

	emitter->setAngle(90);
	emitter->setAngleVar(360);

	emitter->setSpeed(160);
	emitter->setSpeedVar(120);

	emitter->setRadialAccel(-120);
	emitter->setRadialAccelVar(0);

	emitter->setTangentialAccel(30);
	emitter->setTangentialAccelVar(0);

	emitter->setLife(1.5f);
	emitter->setLifeVar(1);

	emitter->setStartSpin(0);
	emitter->setStartSizeVar(0);
	emitter->setEndSpin(0);
	emitter->setEndSpinVar(0);

	// color of particles
	ccColor4F startColor = {0.5f, 0.5f, 0.5f, 1.0f};
	emitter->setStartColor(startColor);

	ccColor4F startColorVar = {0.5f, 0.5f, 0.5f, 0.0f};
	emitter->setStartColorVar(startColorVar);

	ccColor4F endColor = {0.1f, 0.1f, 0.1f, 0.0f};
	emitter->setEndColor(endColor);

	ccColor4F endColorVar = {0.1f, 0.1f, 0.1f, 1.0f};	
	emitter->setEndColorVar(endColorVar);

	// size, in pixels
	emitter->setStartSize(40.0f);
	emitter->setStartSizeVar(15.0f);
	emitter->setEndSize(kParticleStartSizeEqualToEndSize);

	// emits per second
	//m_emitter->setEmissionRate(m_emitter->getTotalParticles()/m_emitter->getLife());
	emitter->setEmissionRate(60);

	// additive
	emitter->setIsBlendAdditive(false);

	emitter->setPosVar(CCPointZero);
	//emitter->setPosition( ccpSub( ccp( pos.x * RATIO, pos.y * RATIO ), level->convertToWorldSpace(CCPointZero)));
	emitter->setPosition( ccpSub( ccp( pos.x * RATIO, pos.y * RATIO ), level->convertToWorldSpace(CCPointZero)));

	level->addChild( emitter, GameTypes::eSpritesEffects );

	_effectsList.push_back( EffectRecord( emitter, 80 ) );
}
//----------------------------------------------------------------------------------------------
void ParticleEngine::Effect_CoinCollected( Level *level, puBlock *block  )
{
	string imageFile;
	CCParticleSystemQuad* emitter = new CCParticleSystemQuad();

	imageFile.append( "Images/Particles/" );
	imageFile.append( block->GetClassName2() );
	imageFile.append( ".png" );

	emitter->initWithTotalParticles(30);
	emitter->autorelease();

	emitter->setTexture( CCTextureCache::sharedTextureCache()->addImage( Skins::GetSkinName( imageFile.c_str() ) ));

	emitter->setDuration( 0.2f );
    emitter->setGravity(CCPointZero);

	emitter->setAngle(90);
	emitter->setAngleVar(360);

	emitter->setSpeed(160);
	emitter->setSpeedVar(120);

	emitter->setRadialAccel(-120);
	emitter->setRadialAccelVar(0);

	emitter->setTangentialAccel(30);
	emitter->setTangentialAccelVar(0);

	emitter->setLife(0.5f);
	emitter->setLifeVar( 0.5f );

	emitter->setStartSpin(0);
	emitter->setStartSizeVar(0);
	emitter->setEndSpin(0);
	emitter->setEndSpinVar(0);

	// color of particles
	ccColor4F startColor = {1.0f, 1.0f, 1.0f, 1.0f};
	emitter->setStartColor(startColor);

	ccColor4F startColorVar = {0.0f, 0.0f, 0.0f, 0.0f};
	emitter->setStartColorVar(startColorVar);

	ccColor4F endColor = {1.0f, 1.0f, 1.0f, 1.0f};
	emitter->setEndColor(endColor);

	ccColor4F endColorVar = {0.0f, 0.0f, 0.0f, 0.0f};	
	emitter->setEndColorVar(endColorVar);

	// size, in pixels
	emitter->setStartSize(30.0f);
	emitter->setStartSizeVar(10.0f);
	emitter->setEndSize(kParticleStartSizeEqualToEndSize);

	// emits per second
	emitter->setEmissionRate( 60 );

	// additive
	ccBlendFunc blend;
	blend.src = 0;
	blend.dst = 770;

	emitter->setBlendFunc( blend );
	emitter->setIsBlendAdditive(false);
	//emitter->setGravity( CCPoint( -1000.0f, 0.0f ));

	emitter->setPosVar(CCPointZero);
	emitter->setPosition( ccpSub( ccp( block->GetPosition().x * RATIO, block->GetPosition().y * RATIO ), level->convertToWorldSpace(CCPointZero)));

	level->addChild( emitter, GameTypes::eSpritesEffects );

	if ( level->GetOrientation() == GameTypes::eLevel_Vertical )
	{
		emitter->setRotation( -90.0f );
	}
	_effectsList.push_back( EffectRecord( emitter, 80 ) );
}
//----------------------------------------------------------------------------------------------
CCParticleSystemQuad* ParticleEngine::Effect_CoinOnScore()
{
    CCParticleSystemQuad *emitter1 = new CCParticleSystemQuad();
	emitter1->initWithFile( Skins::GetSkinName( "Images/Particles/Fireworks.xml" ));
	emitter1->autorelease();
	ccColor4F c;
	c.r = 1;
	c.g = 0;
	c.b = 0;
	c.a = 1;
	
	emitter1->setStartColor( c );
    emitter1->setPosition( ccp(0.0f, 0.0f ));
    return emitter1;
//	string imageFile;
//	CCParticleSystemQuad* emitter = new CCParticleSystemQuad();
//
//	imageFile.append( "Images/Particles/Coin.png" );
//
//	emitter->initWithTotalParticles(10);
//	emitter->autorelease();
//
//	emitter->setTexture( CCTextureCache::sharedTextureCache()->addImage( Skins::GetSkinName( imageFile.c_str() ) ));
//	emitter->setDuration( 0.5f );
//
//	emitter->setGravity(CCPointZero);
//
//	emitter->setAngle(90);
//	emitter->setAngleVar(360);
//
//	emitter->setSpeed(160);
//	emitter->setSpeedVar(120);
//
//	emitter->setRadialAccel(-120);
//	emitter->setRadialAccelVar(0);
//
//	emitter->setTangentialAccel(30);
//	emitter->setTangentialAccelVar(0);
//
//	emitter->setLife(0.5f);
//	emitter->setLifeVar( 0.5f );
//
//	emitter->setStartSpin(0);
//	emitter->setStartSizeVar(0);
//	emitter->setEndSpin(0);
//	emitter->setEndSpinVar(0);
//
//	// color of particles
//	ccColor4F startColor = {1.0f, 1.0f, 1.0f, 1.0f};
//	emitter->setStartColor(startColor);
//
//	ccColor4F startColorVar = {0.0f, 0.0f, 0.0f, 0.0f};
//	emitter->setStartColorVar(startColorVar);
//
//	ccColor4F endColor = {1.0f, 1.0f, 1.0f, 1.0f};
//	emitter->setEndColor(endColor);
//
//	ccColor4F endColorVar = {0.0f, 0.0f, 0.0f, 0.0f};	
//	emitter->setEndColorVar(endColorVar);
//
//	// size, in pixels
//	emitter->setStartSize(30.0f);
//	emitter->setStartSizeVar(10.0f);
//	emitter->setEndSize(kParticleStartSizeEqualToEndSize);
//
//	// emits per second
//	emitter->setEmissionRate( 60 );
//
//	// additive
//	ccBlendFunc blend;
//	blend.src = 0;
//	blend.dst = 770;
//
//	emitter->setBlendFunc( blend );
//	emitter->setIsBlendAdditive(false);
//	//emitter->setGravity( CCPoint( -1000.0f, 0.0f ));
//
//    return emitter;
}
//----------------------------------------------------------------------------------------------
void ParticleEngine::Effect_Burn( CCNode *level, b2Vec2 pos )
{
	CCParticleFire *emitter = CCParticleFire::node();

	emitter->setPosVar(CCPointZero);
	emitter->setPosition( ccpSub( ccp( pos.x * RATIO, pos.y * RATIO ), level->convertToWorldSpace(CCPointZero)));

	emitter->setTexture( CCTextureCache::sharedTextureCache()->addImage(  Skins::GetSkinName( "Images/Particles/fire.png" )  ));
	emitter->setDuration( 0.35f );

	emitter->setGravity(CCPointZero);

	emitter->setAngle(180);
	emitter->setAngleVar(360);

	emitter->setSpeed(60);
	emitter->setSpeedVar(20);

	emitter->setRadialAccel(0);
	emitter->setRadialAccelVar(0);

	emitter->setLife(0.80f);
	emitter->setLifeVar( 0.3f );

	level->addChild( emitter, GameTypes::eSpritesEffects );
	_effectsList.push_back( EffectRecord( emitter, 80 ) );
}
//----------------------------------------------------------------------------------------------
void ParticleEngine::Effect_BombExloded( CCNode *level, b2Vec2 pos, float range )
{

	CCParticleFire *emitter = CCParticleFire::node();
	
	emitter->setEmitterMode( kCCParticleModeRadius );

	emitter->setPosVar( ccp ( range, range ));
	emitter->setPosition( ccpSub( ccp( pos.x * RATIO, pos.y * RATIO ), level->convertToWorldSpace(CCPointZero)));

	emitter->setTexture( CCTextureCache::sharedTextureCache()->addImage(  Skins::GetSkinName( "Images/Particles/fire.png" )  ));
	emitter->setDuration( 0.3f );

	emitter->setAngle(360);
	emitter->setAngleVar(360);

	emitter->setEmissionRate( 2000 );
	emitter->setLife(1.0f);
	emitter->setLifeVar( 0.5f );
	emitter->setEndRadius( range * RATIO );
	
	level->addChild( emitter, GameTypes::eSpritesEffects );
	_effectsList.push_back( EffectRecord( emitter, 80 ) );
}
//----------------------------------------------------------------------------------------------
void ParticleEngine::Effect_ChainCut( CCNode *level, b2Vec2 pos )
{

	CCParticleFire *emitter = CCParticleFire::node();

	emitter->setPosVar(CCPointZero);
	emitter->setPosition( ccpSub( ccp( pos.x * RATIO, pos.y * RATIO ), level->convertToWorldSpace(CCPointZero)));

	emitter->setTexture( CCTextureCache::sharedTextureCache()->addImage(  Skins::GetSkinName( "Images/Particles/fire.png" )  ));
	emitter->setDuration( 0.2f );

	emitter->setGravity(CCPointZero);

	emitter->setAngle(180);
	emitter->setAngleVar(360);

	emitter->setSpeed(60);
	emitter->setSpeedVar(20);

	emitter->setRadialAccel(0);
	emitter->setRadialAccelVar(0);

	emitter->setLife(0.30f);
	emitter->setLifeVar( 0.2f );

	level->addChild( emitter, GameTypes::eSpritesEffects );
	_effectsList.push_back( EffectRecord( emitter, 20 ) );
}
//----------------------------------------------------------------------------------------------
void ParticleEngine::Effect_Smoke( CCNode *level, b2Vec2 pos )
{

	CCParticleSmoke *emitter = CCParticleSmoke::node();

	emitter->setPosVar(CCPointZero);
	emitter->setPosition( ccpSub( ccp( pos.x * RATIO, pos.y * RATIO ), level->convertToWorldSpace(CCPointZero)));

	emitter->setTexture( CCTextureCache::sharedTextureCache()->addImage( Skins::GetSkinName( "Images/Particles/fire.png" ) ) );
	emitter->setDuration( 0.15f );

	emitter->setGravity(CCPointZero);

	emitter->setAngleVar(30);

	emitter->setSpeed(100);
	emitter->setSpeedVar(100);

	emitter->setLife(0.5f);
	emitter->setLifeVar( 0.5f );

	emitter->setEmissionRate(50);

	level->addChild( emitter, GameTypes::eSpritesEffects );
	_effectsList.push_back( EffectRecord( emitter, 130 ) );
}
//----------------------------------------------------------------------------------------------
void ParticleEngine::Effect_Fireworks( CCNode *level, b2Vec2 pos )
{
	CCParticleSystemQuad *emitter1 = new CCParticleSystemQuad();
	emitter1->initWithFile( Skins::GetSkinName( "Images/Particles/Fireworks.xml" ));
	emitter1->autorelease();
	ccColor4F c;
	c.r = 1;
	c.g = 0;
	c.b = 0;
	c.a = 1;
	
	emitter1->setStartColor( c );
	emitter1->setPosition( ccpSub( ccp( pos.x * RATIO, pos.y * RATIO ), level->convertToWorldSpace(CCPointZero)));

	level->addChild( emitter1, GameTypes::eSpritesEffects );
	_effectsList.push_back( EffectRecord( emitter1, 130 ) );
}
//----------------------------------------------------------------------------------------------
void ParticleEngine::Effect_LevelFinished( CCNode *level, b2Vec2 pos )
{
	return;
	D_LOG("pos %f %f", pos.x, pos.y );
	CCParticleSystemQuad *emitter = new CCParticleSystemQuad();
	emitter->initWithFile(  Skins::GetSkinName( "Images/Particles/LavaFlow.plist" ) );
	emitter->autorelease();

	emitter->setPosVar(CCPointZero);
	emitter->setPosition( ccpSub( ccp( pos.x * RATIO, pos.y * RATIO ), level->convertToWorldSpace(CCPointZero)));

	emitter->setTexture( CCTextureCache::sharedTextureCache()->addImage( Skins::GetSkinName( "Images/Particles/fire.png" ) ) );
	emitter->setDuration( 0.5f );

	CCMoveBy *move1 = CCMoveBy::actionWithDuration( 0.6f, ccp( -200, 0 ));

	emitter->runAction(  move1 );
	level->addChild( emitter, GameTypes::eSpritesEffects );
	_effectsList.push_back( EffectRecord( emitter, 22530 ) );
}
//----------------------------------------------------------------------------------------------
void ParticleEngine::Effect_NewClue( CCNode *level, b2Vec2 pos )
{

	CCParticleFlower *emitter = CCParticleFlower::node();
	emitter->setPosition( ccpSub( ccp( pos.x * RATIO, pos.y * RATIO ), level->convertToWorldSpace(CCPointZero)));
	emitter->setAngleVar(360);

	emitter->setSpeed(160);
	emitter->setSpeedVar(120);

	emitter->setRadialAccel(-120);
	emitter->setRadialAccelVar(0);

	emitter->setTangentialAccel(30);
	emitter->setTangentialAccelVar(0);

	emitter->setLife(1.5f);
	emitter->setLifeVar(1);

	emitter->setStartSpin(0);
	emitter->setStartSizeVar(0);
	emitter->setEndSpin(0);
	emitter->setEndSpinVar(0);

	// size, in pixels
	emitter->setStartSize(60.0f);
	emitter->setStartSizeVar(30.0f);
	emitter->setEndSize( kParticleStartSizeEqualToEndSize );

	// emits per second
	emitter->setEmissionRate(60);

	emitter->setTexture( CCTextureCache::sharedTextureCache()->addImage( Skins::GetSkinName( "Images/Particles/clue.png" ) ) );
	emitter->setDuration( 1.0f );

	level->addChild( emitter, GameTypes::eSpritesEffects );
	_effectsList.push_back( EffectRecord( emitter, 180 ) );
}
//----------------------------------------------------------------------------------------------
void ParticleEngine::Effect_Snow( CCNode *node )
{

	CCParticleSnow* emitter = CCParticleSnow::node();
	emitter->setPosition( CCPointMake( 0.0f, 200.0f ));
	emitter->setLife(3);
	emitter->setLifeVar(1);
	emitter->setGravity( CCPointMake( Game::Get()->GetRuningLevel()->GetWorld()->GetGravity().x, Game::Get()->GetRuningLevel()->GetWorld()->GetGravity().y ));

	// speed of particles
	emitter->setSpeed(90);
	emitter->setSpeedVar(20);

	ccColor4F startColor = emitter->getStartColor();
	startColor.r = 0.9f;
	startColor.g = 0.9f;
	startColor.b = 0.9f;
	emitter->setStartColor(startColor);

	ccColor4F startColorVar = emitter->getStartColorVar();
	startColorVar.b = 0.1f;
	emitter->setStartColorVar(startColorVar);

	float rate = (float)( 150 + ( rand() % 440 ));
	emitter->setEmissionRate(emitter->getTotalParticles()/emitter->getLife() / 2);
	emitter->setEmissionRate( rate );

	emitter->setTexture( CCTextureCache::sharedTextureCache()->addImage( Skins::GetSkinName( "Images/Particles/snow.png" ) ));
	node->addChild( emitter, GameTypes::eSpritesRain );
}
//----------------------------------------------------------------------------------------------
void ParticleEngine::Effect_HotSun( CCNode *node )
{
	//float rate = (float) ( 50 + ( rand() % 300 ));
	//D_FLOAT( rate )
	{
		CCParticleSystemQuad *emitter = new CCParticleSystemQuad();
		emitter->initWithFile(  Skins::GetSkinName( "Images/Particles/BoilingFoam.plist" ) );
		emitter->autorelease();
		emitter->setPosition( CCPointMake( -100.0f, 20.0f ));
		emitter->setPosVar( CCPointMake( 60.0f, 30.0f ));

		emitter->setEmissionRate( 30 );
		node->addChild( emitter, GameTypes::eSpritesBeloweX3 );
	}
}
//----------------------------------------------------------------------------------------------
void ParticleEngine::Effect_Rain( CCNode *node )
{

	float rate = (float) ( 100 + ( rand() % 500 ));
	
	D_FLOAT( rate )
	{
		CCParticleSystemQuad *emitter = new CCParticleSystemQuad();
		emitter->initWithFile(  Skins::GetSkinName( "Images/Particles/BurstPipe.plist" ) );
		emitter->autorelease();
		emitter->setPosition( CCPointMake( 0.0f, 250.0f ));
		emitter->setEmissionRate( rate );
		node->addChild( emitter, GameTypes::eSpritesRain );

	}
	{
		CCParticleSystemQuad *emitter = new CCParticleSystemQuad();
		emitter->initWithFile(  Skins::GetSkinName( "Images/Particles/BurstPipe.plist" ) );
		emitter->autorelease();
		emitter->setPosition( CCPointMake( -225.0f, 250.0f ));
		emitter->setEmissionRate( rate );
		node->addChild( emitter, GameTypes::eSpritesRain );
	}
	{
		CCParticleSystemQuad *emitter = new CCParticleSystemQuad();
		emitter->initWithFile(  Skins::GetSkinName( "Images/Particles/BurstPipe.plist" ) );
		emitter->autorelease();
		emitter->setPosition( CCPointMake( -150.0f, 250.0f ));
		emitter->setEmissionRate( rate );
		node->addChild( emitter, GameTypes::eSpritesRain );
	}
	{
		CCParticleSystemQuad *emitter = new CCParticleSystemQuad();
		emitter->initWithFile(  Skins::GetSkinName( "Images/Particles/BurstPipe.plist" ) );
		emitter->autorelease();
		emitter->setPosition( CCPointMake( -75.0f, 250.0f ));
		emitter->setEmissionRate( rate );
		node->addChild( emitter, GameTypes::eSpritesRain );
	}
	{
		CCParticleSystemQuad *emitter = new CCParticleSystemQuad();
		emitter->initWithFile(  Skins::GetSkinName( "Images/Particles/BurstPipe.plist" ) );
		emitter->autorelease();
		emitter->setPosition( CCPointMake( 225.0f, 250.0f ));
		emitter->setEmissionRate( rate );
		node->addChild( emitter, GameTypes::eSpritesRain );
	}
	{
		CCParticleSystemQuad *emitter = new CCParticleSystemQuad();
		emitter->initWithFile(  Skins::GetSkinName( "Images/Particles/BurstPipe.plist" ) );
		emitter->autorelease();
		emitter->setPosition( CCPointMake( 150.0f, 250.0f ));
		emitter->setEmissionRate( rate );
		node->addChild( emitter, GameTypes::eSpritesRain );
	}
	{
		CCParticleSystemQuad *emitter = new CCParticleSystemQuad();
		emitter->initWithFile(  Skins::GetSkinName( "Images/Particles/BurstPipe.plist" ) );
		emitter->autorelease();
		emitter->setPosition( CCPointMake( 75.0f, 250.0f ));
		emitter->setEmissionRate( rate );
		node->addChild( emitter, GameTypes::eSpritesRain );
	}
}
//----------------------------------------------------------------------------------------------
CCNode* ParticleEngine::Effect_Boobles()
{

	float rate = (float) ( 100 + ( rand() % 500 ));

	CCParticleSystemQuad *emitter = new CCParticleSystemQuad();
	emitter->initWithFile(  Skins::GetSkinName( "Images/Particles/BurstPipe.plist" ) );
	emitter->autorelease();
	emitter->setPosition( CCPointMake( 0.0f, 250.0f ));
	emitter->setEmissionRate( rate );

	//emitter->setRotatePerSecond( 5 );
	emitter->setRotation( 180.0f );
	return emitter;
}
//----------------------------------------------------------------------------------------------
void ParticleEngine::Effect_FragileDestroyed( CCNode *level, b2Vec2 pos )
{

	Effect_FragileDestroyed( level, pos, "" );
}
//----------------------------------------------------------------------------------------------
void ParticleEngine::Effect_FragileDestroyed( CCNode *level, b2Vec2 pos, const char *color )
{

	char file[128];
	const static int sParticlesCount = 100;
	
	snprintf( file, 128, "Images/Particles/Fragile%s.png", color );

	CCParticleSystemQuad* emitter = new CCParticleSystemQuad();
	emitter->initWithTotalParticles(sParticlesCount);
	emitter->autorelease();

	emitter->setTexture( CCTextureCache::sharedTextureCache()->addImage(  Skins::GetSkinName( file )  ));
	emitter->setDuration( 0.2f );

	emitter->setGravity(ccp(0.0f, -500.0f ));

	emitter->setAngle(90);
	emitter->setAngleVar(360);

	emitter->setSpeed(160);
	emitter->setSpeedVar(120);

	emitter->setRadialAccel(-120);
	emitter->setRadialAccelVar(0);

	emitter->setTangentialAccel(30);
	emitter->setTangentialAccelVar(0);

	emitter->setLife(0.5f);
	emitter->setLifeVar( 0.5f );

	emitter->setStartSpin(00);
	emitter->setStartSizeVar(00);
	emitter->setEndSpin(00);
	emitter->setEndSpinVar(00);

	// color of particles
	ccColor4F startColor = {1.0f, 1.0f, 1.0f, 1.0f};
	emitter->setStartColor(startColor);

	ccColor4F startColorVar = {0.0f, 0.0f, 0.0f, 0.0f};
	emitter->setStartColorVar(startColorVar);

	ccColor4F endColor = {1.0f, 1.0f, 1.0f, 1.0f};
	emitter->setEndColor(endColor);

	ccColor4F endColorVar = {0.0f, 0.0f, 0.0f, 0.0f};	
	emitter->setEndColorVar(endColorVar);

	// size, in pixels
	emitter->setStartSize(16.0f);
	emitter->setStartSizeVar(0.0f);
	emitter->setEndSize(kParticleStartSizeEqualToEndSize);

	// emits per second
	emitter->setEmissionRate( (float) sParticlesCount );

	// additive
	ccBlendFunc blend;
	blend.src = 0;
	blend.dst = 770;

	emitter->setBlendFunc( blend );
	emitter->setIsBlendAdditive(false);

	emitter->setPosVar(ccp( 50.0f, 50.0f ));
	//emitter->setPosition( ccpSub( ccp( pos.x * RATIO, pos.y * RATIO ), level->convertToWorldSpace(CCPointZero)));
	emitter->setPosition( ccpSub( ccp( pos.x * RATIO, pos.y * RATIO ), level->convertToWorldSpace(CCPointZero)));

	level->addChild( emitter, GameTypes::eSpritesEffects );

	_effectsList.push_back( EffectRecord( emitter, 80 ) );
}
//----------------------------------------------------------------------------------------------
void ParticleEngine::PauseEngine()
{

	EffectsList::iterator it;
	for ( it = _effectsList.begin(); it != _effectsList.end(); it++ )
		it->node->pauseSchedulerAndActions();
}
//----------------------------------------------------------------------------------------------
void ParticleEngine::ResumeEngine()
{

	EffectsList::iterator it;
	for ( it = _effectsList.begin(); it != _effectsList.end(); it++ )
		it->node->resumeSchedulerAndActions();
}
//----------------------------------------------------------------------------------------------
void ParticleEngine::PreloadGraphic()
{

	CCSprite::spriteWithFile( Skins::GetSkinName( "Images/Particles/Fragile.png") );
	CCSprite::spriteWithFile( Skins::GetSkinName( "Images/Particles/Coin.png") );
}


