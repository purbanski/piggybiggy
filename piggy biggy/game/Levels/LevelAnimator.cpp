#include <set>
#include "LevelAnimator.h"
#include "Animation/AnimManager.h"
#include "GameConfig.h"
#include "Blocks/puBlock.h"
#include "Game.h"
#include "SoundEngine.h"

//-----------------------------------------------------------------------------------------------
// Slow Speed Blocks Map
//-----------------------------------------------------------------------------------------------
SlowSpeedBlockMap::SlowSpeedBlockMap()
{
	clear();
}
//-----------------------------------------------------------------------------------------------
void SlowSpeedBlockMap::AddTime( puBlock *block, ccTime dTime )
{
	operator[]( block ) = operator[]( block ) + dTime;
}
//-----------------------------------------------------------------------------------------------
void SlowSpeedBlockMap::ClearTime( puBlock *block )
{
	operator[]( block ) = 0.0f;
}
//-----------------------------------------------------------------------------------------------
float SlowSpeedBlockMap::GetTime( puBlock *block )
{
	return operator[]( block );
}
//-----------------------------------------------------------------------------------------------



//-----------------------------------------------------------------------------------------------
// Level Animator Idle Blocks
//-----------------------------------------------------------------------------------------------
LevelAnimatorIdleBlocks::LevelAnimatorIdleBlocks()
{

	_level = NULL;

	AnimVector animList;

	//-------------------------
	// Piggy Bank
	//-------------------------
	animList.clear();
	animList.push_back( GameTypes::eAnim_PiggyBankUaa );
	animList.push_back( GameTypes::eAnim_PiggyBankFiuFiu );
	animList.push_back( GameTypes::eAnim_PiggyBankEeeOoo );
	animList.push_back( GameTypes::eAnim_PiggyBankSalto );
	animList.push_back( GameTypes::eAnim_PiggyBankLookAround );
	animList.push_back( GameTypes::eAnim_PiggyBankEyesBlink );
	animList.push_back( GameTypes::eAnim_PiggyBankHappyJump );
	animList.push_back( GameTypes::eAnim_PiggyBankNoseBubble );

	_animListMap[ eBlockPiggyBank ] = animList;
	_blockTimeRange[ eBlockPiggyBank ] = TimeRange( 10, 110 );


	//-------------------------
	// Piggy Bank Static
	//-------------------------
	animList.clear();
	animList.push_back( GameTypes::eAnim_PiggyBankUaa );
	animList.push_back( GameTypes::eAnim_PiggyBankFiuFiu );
	animList.push_back( GameTypes::eAnim_PiggyBankEeeOoo );
	animList.push_back( GameTypes::eAnim_PiggyBankLookAround );
	animList.push_back( GameTypes::eAnim_PiggyBankEyesBlink );
	animList.push_back( GameTypes::eAnim_PiggyBankNoseBubble );

	_animListMap[ eBlockPiggyBankStatic ] = animList;
	_blockTimeRange[ eBlockPiggyBankStatic ] = TimeRange( 10, 110 ); // 10, 110


	//-------------------------
	// Cop
	//-------------------------
	animList.clear();
	animList.push_back( GameTypes::eAnim_CopJump );
	_animListMap[ eBlockCop ] = animList;
	//_animListMap[ eBlockCopStat ] = animList;

	_blockTimeRange[ eBlockCop ] = TimeRange( 40, 150 );
	//_blockTimeRange[ string("CopStatic") ] = TimeRange( 40, 150 );


	//-------------------------
	// Thief
	//-------------------------
	animList.clear();
	animList.push_back( GameTypes::eAnim_ThiefLookAround );
	_animListMap[ eBlockThief ]				= animList;
	_animListMap[ eBlockThiefStatic ]		= animList;

	_blockTimeRange[ eBlockThief ]			= TimeRange( 50, 180 );
	_blockTimeRange[ eBlockThiefStatic ]	= TimeRange( 50, 180 );


	//-------------------------
	// Coin
	//-------------------------
	animList.clear();
	animList.push_back( GameTypes::eAnim_CoinFlash1 );
	_animListMap[ eBlockCoin ]			= animList;
	_animListMap[ eBlockCoinStatic ]	= animList;

	_blockTimeRange[ eBlockCoin ]		= TimeRange( 10, 80 );
	_blockTimeRange[ eBlockCoinStatic ] = TimeRange( 10, 80 );
	
	_enabled = true;	


	////-------------------------
	//// Cash cow
	////-------------------------
	//animList.clear();
	//animList.push_back( GameTypes::eAnim_CashCowEyesBlink );
	//_animListMap[ string("CashCow") ] = animList;
	//_animListMap[ string("CashCowStatic") ] = animList;

	//_blockTimeRange[ string("CashCow") ] = TimeRange( 10, 80 );
	//_blockTimeRange[ string("CashCowStatic") ] = TimeRange( 10, 80 );

	//_enabled = true;	
}
//-----------------------------------------------------------------------------------------------
LevelAnimatorIdleBlocks::~LevelAnimatorIdleBlocks()
{
	_animListMap.clear();
	_blockTimeRange.clear();
	_idleBlocksTimer.clear();
}
//-----------------------------------------------------------------------------------------------
void LevelAnimatorIdleBlocks::Process( ccTime time)
{	

	if ( ! _enabled || ! _level )
		return;

	SetIdleBlocksTimers( time );
	ProcessIdleBlocks();
	return;
}
//-----------------------------------------------------------------------------------------------
void LevelAnimatorIdleBlocks::SetIdleBlocksTimers( ccTime dTime )
{

	puBlock *block;
	BlockSet blocks;

	blocks = _level->GetAnimableBlocks();

	//------------------------------
	// add new idle blocks
	for ( BlockSet::iterator it = blocks.begin(); it != blocks.end(); it++ )
	{
		block = *it;

		// no Body
		if ( ! block || ! block->GetBody() || ! block->GetSprite() || ! block->GetSprite()->getIsVisible() )
			continue;
		

		// awake
//		if ( block->GetBody()->IsAwake() && block->GetBody()->GetType() != b2_staticBody ) 
	//		continue;

		double lineVel;
		b2Vec2 lineVec;
		lineVec = block->GetBody()->GetLinearVelocity();
		lineVel = sqrt( pow(lineVec.x, 2) + pow(lineVec.y,2 ));

		//D_FLOAT( block->GetBody()->GetAngularVelocity() )
		//D_FLOAT( lineVel )

		float div;
		div = 0.85f;

		if ( lineVel  > Config::AnimFastSpeedLinearLimits.x * div )
			continue;

		if ( abs( block->GetBody()->GetAngularVelocity() ) > Config::AnimFastSpeedAngularLimits.x * div )
			continue;

		// anim running
		if ( AnimManager::Get()->GetAnimState( block ) == GameTypes::eAnimState_Running )
			continue;

		// add if not exists
		if ( ! _idleBlocksTimer.count( block ))
		{
			_idleBlocksTimer[ block ] = GetRandomTime( block );
		}
		// decreses time if > 0
		else if ( _idleBlocksTimer[ block ] > 0.0f )
		{
			_idleBlocksTimer[ block ] -= dTime;
		}
		else if ( _idleBlocksTimer[ block ] <= 0.0f )
		{
			_idleBlocksTimer[ block ] = GetRandomTime( block );
		}
	}
}
//-----------------------------------------------------------------------------------------------
void LevelAnimatorIdleBlocks::ProcessIdleBlocks()
{
	puBlock *block;
	float time;

	for ( IdleBlockToProcess::iterator it = _idleBlocksTimer.begin(); it != _idleBlocksTimer.end(); it++ )
	{
		block = it->first;
		time = it->second;

		//------------------------------
		// checks
		if ( ! block || ! block->GetBody() || ! block->GetSprite() || ! block->GetSprite()->getIsVisible() )
			continue;

		if ( block->GetBody()->IsAwake() && block->GetBody()->GetType() == b2_dynamicBody )
			continue;

		 if ( AnimManager::Get()->GetAnimState( block ) == GameTypes::eAnimState_Running )
			continue;

		float rotation;
		rotation = block->GetRotation();
		rotation = Tools::RotationNormalize( rotation );
		rotation += block->GetSpriteDeltaRotation();

		//-----------------------------
		// Not vertical
		if ( 
			
			! ( rotation > -Config::AnimRotationStartLimit && rotation < Config::AnimRotationStartLimit ) 
			&& block->GetBody()->GetType() == b2_dynamicBody 
			&& _level->GetOrientation() == GameTypes::eLevel_Horizontal
			)
		{
			it->second = 0.0f;
			AnimManager::Get()->PlayAnim( GameTypes::eAnim_RotationFix, GameTypes::eAnimPiority_Low, block );
		}
		else if ( 
			! ( rotation - 90.0f > -Config::AnimRotationStartLimit && rotation - 90.0f < Config::AnimRotationStartLimit ) 
			&& block->GetBody()->GetType() == b2_dynamicBody 
			&& _level->GetOrientation() == GameTypes::eLevel_Vertical

			)
		{
			it->second = 0.0f;
			AnimManager::Get()->PlayAnim( GameTypes::eAnim_RotationFix, GameTypes::eAnimPiority_Low, block );
		}
		//--------------------------
		// Adjusted
		else if ( time <= 0.0f )
		{
			PlayRandomAnim( block );
		}
	}
}
//-----------------------------------------------------------------------------------------------
void LevelAnimatorIdleBlocks::PlayRandomAnim( puBlock * block )
{
	int randIndex;
	AnimVector animList;
	animList = _animListMap[ block->GetBlockEnum() ];

	if ( ! animList.size() )
		return;

	randIndex = Tools::Rand( 0, animList.size() - 1 );
	AnimManager::Get()->PlayAnim( animList.at( randIndex ), GameTypes::eAnimPiority_Normal, block );
}
//-----------------------------------------------------------------------------------------------
float LevelAnimatorIdleBlocks::GetRandomTime( puBlock *block )
{
	if ( ! _blockTimeRange.count( block->GetBlockEnum() ))
	{
		unAssertMsg( Game, false, ("Undefide animation rand time for: %s", block->GetClassName2() ));
		return 2.5f;
	}

	float ret;
	int a;
	int b;

	a = _blockTimeRange[ block->GetBlockEnum() ].first;
	b = _blockTimeRange[ block->GetBlockEnum() ].second;

	ret = ( (float) Tools::Rand( a, b ) ) / 10.0f;
	return ret;
}
//-----------------------------------------------------------------------------------------------
void LevelAnimatorIdleBlocks::SetLevel( Level *level )
{
	_level = level;
}


//-----------------------------------------------------------------------------------------------
// Level Animator Active Blocks
//-----------------------------------------------------------------------------------------------
LevelAnimatorActiveBlocks::LevelAnimatorActiveBlocks()
{

	_level = NULL;

	_animsMoveMap[ eBlockPiggyBank	] = GameTypes::eAnim_PiggyBankToRolling;
	_animsStopMap[ eBlockPiggyBank	] = GameTypes::eAnim_PiggyBankToSitting;

	_animsMoveMap[ eBlockCop	] = GameTypes::eAnim_CopToRolling;
	_animsStopMap[ eBlockCop	] = GameTypes::eAnim_CopToSitting;

	_animsMoveMap[ eBlockThief	] = GameTypes::eAnim_ThiefToRolling;
	_animsStopMap[ eBlockThief	] = GameTypes::eAnim_ThiefToSitting;

	float linearRangeA, linearRangeB;
	float angularRangeA, angularRangeB;

	linearRangeA = Config::AnimFastSpeedLinearLimits.x;
	linearRangeB = Config::AnimFastSpeedLinearLimits.y;

	angularRangeA = Config::AnimFastSpeedAngularLimits.x;
	angularRangeB = Config::AnimFastSpeedAngularLimits.y;

	_blockSpeedLinearLimitMap[ eBlockPiggyBank	]	= Speed( linearRangeA, linearRangeB );
	_blockSpeedAngularLimitMap[ eBlockPiggyBank	]	= Speed( angularRangeA, angularRangeB );
	
	_blockSpeedLinearLimitMap[ eBlockCop ]	= Speed( linearRangeA, linearRangeB );
	_blockSpeedAngularLimitMap[ eBlockCop ]	= Speed( angularRangeA, angularRangeB );

	_blockSpeedLinearLimitMap[ eBlockThief ]	= Speed( linearRangeA, linearRangeB );
	_blockSpeedAngularLimitMap[ eBlockThief ]	= Speed( angularRangeA, angularRangeB );

	_slowMovingSpeedLimit = Config::AnimSlowSpeedLimits; // linear, angular
	_slowMovingTimeLimit = Config::AnimSlowSpeedTimeLimit;

	_enabled = true;
}
//-----------------------------------------------------------------------------------------------
LevelAnimatorActiveBlocks::~LevelAnimatorActiveBlocks()
{
	_animsMoveMap.clear();
	_animsStopMap.clear();
	_blockSpeedMap.clear();
	_blockSpeedLinearLimitMap.clear();
	_blockSpeedAngularLimitMap.clear();

	_blockSlowSpeedTimeMap.clear();
	_animatedBlocks.clear();
}
//-----------------------------------------------------------------------------------------------
void LevelAnimatorActiveBlocks::Process( ccTime time)
{	

	if ( ! _enabled || ! _level )
		return;

	SetBlockSpeedMap( time );
	ProcessBlocks();
	return;
}
//-----------------------------------------------------------------------------------------------
void LevelAnimatorActiveBlocks::SetBlockSpeedMap( ccTime dTime )
{

	puBlock *block;
	BlockSet blocks;

	blocks = Game::Get()->GetRuningLevel()->GetAnimableBlocks();

	_blockSpeedMap.clear();
	//------------------------------
	// add new idle blocks
	for ( BlockSet::iterator it = blocks.begin(); it != blocks.end(); it++ )
	{
		block = *it;

		// no Body
		if ( ! block->GetBody() )
			continue;

		// awake
		if ( ! block->GetBody()->IsAwake() && block->GetBody()->GetType() == b2_staticBody ) 
			continue;

		// anim running
		if ( AnimManager::Get()->GetAnimState( block ) == GameTypes::eAnimState_Running )
			continue;

		// add if not exists
		float linearSpeed;
		float angularSpeed;

		linearSpeed = sqrt( pow ( block->GetBody()->GetLinearVelocity().x , 2 ) + pow( block->GetBody()->GetLinearVelocity().y, 2 ));
		angularSpeed = abs( block->GetBody()->GetAngularVelocity() );

		_blockSpeedMap[ block ] = Speed( linearSpeed, angularSpeed );

		if ( linearSpeed <= _slowMovingSpeedLimit.x && angularSpeed <= _slowMovingSpeedLimit.y )
			_blockSlowSpeedTimeMap.AddTime( block, dTime );
		else
			_blockSlowSpeedTimeMap.ClearTime( block );
	}
}
//-----------------------------------------------------------------------------------------------
void LevelAnimatorActiveBlocks::ProcessBlocks()
{

	puBlock *block;
	
	float angSpeed;
	float linSpeed;

	float angSpeedUpLimit;
	float angSpeedDownLimit;

	float linSpeedUpLimit;
	float linSpeedDownLimit;


	//---------------------------
	// Fast moving blocks
	for ( BlockSpeedMap::iterator it = _blockSpeedMap.begin(); it != _blockSpeedMap.end(); it++ )
	{
		block = it->first;

		// no Body
		if ( ! block->GetBody() )
			continue;
			
		linSpeed = (*it).second.first;
		angSpeed = (*it).second.second;

		linSpeedUpLimit		= _blockSpeedLinearLimitMap[ block->GetBlockEnum() ].second;
		linSpeedDownLimit	= _blockSpeedLinearLimitMap[ block->GetBlockEnum() ].first;

		angSpeedUpLimit		= _blockSpeedAngularLimitMap[ block->GetBlockEnum() ].second;
		angSpeedDownLimit	= _blockSpeedAngularLimitMap[ block->GetBlockEnum() ].first;

		//----------------------------
		// block animated but not unaimated 
		// sprite in roll in sit pose
		if ( _animatedBlocks.count( block ) )
		{
			if ( linSpeed < linSpeedDownLimit && angSpeed < angSpeedDownLimit )
			{
				_animatedBlocks.erase( block );
				AnimManager::Get()->PlayAnim( _animsStopMap[ block->GetBlockEnum() ], GameTypes::eAnimPiority_Normal, block );
				return;
			}
		}
		

		//------------------------
		// Animated block
		// Get into roll pose
		if (( linSpeed > linSpeedUpLimit || angSpeed > angSpeedUpLimit ) && ! _animatedBlocks.count( block ))
		{
			_animatedBlocks.insert( block );
			AnimManager::Get()->PlayAnim( _animsMoveMap[ block->GetBlockEnum() ], GameTypes::eAnimPiority_Normal, block );
		}
	}



	//---------------------------
	// Slow moving blocks
	for ( SlowSpeedBlockMap::iterator it = _blockSlowSpeedTimeMap.begin(); it != _blockSlowSpeedTimeMap.end(); it++ )
	{
		puBlock *block;
		float rotation;

		block = it->first;
		// no Body
		if ( ! block->GetBody() )
			continue;

		if ( it->second < _slowMovingTimeLimit )
			continue;

		rotation = block->GetRotation();
		rotation = Tools::RotationNormalize( rotation );
		rotation += block->GetSpriteDeltaRotation();

		//-----------------------------
		// Check for vertical level
		//------------------------------
		if ( 

			! ( rotation > -Config::AnimRotationStartLimit && rotation < Config::AnimRotationStartLimit ) 
			&& block->GetBody()->GetType() == b2_dynamicBody 
			&& _level->GetOrientation() == GameTypes::eLevel_Horizontal
			)
		{
			it->second = 0.0f;
			AnimManager::Get()->PlayAnim( GameTypes::eAnim_RotationFix, GameTypes::eAnimPiority_Low, block );
		}
		else if ( 
			! ( rotation - 90.0f > -Config::AnimRotationStartLimit && rotation - 90.0f < Config::AnimRotationStartLimit ) 
			&& block->GetBody()->GetType() == b2_dynamicBody 
			&& _level->GetOrientation() == GameTypes::eLevel_Vertical
			)
		{
			it->second = 0.0f;
			AnimManager::Get()->PlayAnim( GameTypes::eAnim_RotationFix, GameTypes::eAnimPiority_Low, block );
		}

	}

}
//-----------------------------------------------------------------------------------------------
void LevelAnimatorActiveBlocks::PlayRandomAnim( puBlock * block )
{
	
	
}
//-----------------------------------------------------------------------------------------------
void LevelAnimatorActiveBlocks::SetLevel( Level *level )
{
	_level = level;
}




//-----------------------------------------------------------------------------------------------
// Level Animator Events
//-----------------------------------------------------------------------------------------------
LevelAnimatorEvents::LevelAnimatorEvents()
{
	_enabled = true;
	_level = NULL;
}
//-----------------------------------------------------------------------------------------------
void LevelAnimatorEvents::LevelFailed()
{

	if ( ! _enabled && ! _level )
		return;

	puBlock *block;
	BlockSet blocks;
	BlockSet blocksTmp;

	blocks = _level->GetAnimableBlocks();
	blocksTmp = _level->GetRolledAnimableBlocks();

	blocks.insert( blocksTmp.begin(), blocksTmp.end() );

	for ( BlockSet::iterator it = blocks.begin(); it != blocks.end(); it++ )
	{
		block = *it;

		if ( block && block->GetBody() )
			if ( 
				( block->GetBlockEnum() == GameTypes::eBlockPiggyBank ) || 
				( block->GetBlockEnum() == GameTypes::eBlockPiggyBankStatic ) || 
				( block->GetBlockEnum() == GameTypes::eBlockPiggyBankRolled ) )
			{
				AnimManager::Get()->PlayAnim( GameTypes::eAnim_PiggyBankLevelFailed, GameTypes::eAnimPiority_Highest, block );
			}
	}
}
//-----------------------------------------------------------------------------------------------
void LevelAnimatorEvents::LevelDone()
{
	if ( ! _enabled || ! _level )
		return;

	puBlock *block;
	BlockSet blocks;
	BlockSet blocksTmp;

	blocks = _level->GetAnimableBlocks();
	blocksTmp = _level->GetRolledAnimableBlocks();

	blocks.insert( blocksTmp.begin(), blocksTmp.end() );

	for ( BlockSet::iterator it = blocks.begin(); it != blocks.end(); it++ )
	{
		block = *it;

		if ( block && block->GetBody() )
			if ( ( block->GetBlockEnum() == eBlockPiggyBank ) || ( block->GetBlockEnum() == eBlockPiggyBankStatic ) || ( block->GetBlockEnum() == eBlockPiggyBankRolled ) )
			{
				AnimManager::Get()->PlayAnim( GameTypes::eAnim_PiggyBankLevelDone, GameTypes::eAnimPiority_High, block );
			}
	}
}
//-----------------------------------------------------------------------------------------------
void LevelAnimatorEvents::CoinCollected( puBlock * piggy )
{

	if ( ! _enabled )
		return;

	AnimManager::Get()->PlayAnim( GameTypes::eAnim_PiggyBankCoinCollected, eAnimPiority_High, piggy );
}
//-----------------------------------------------------------------------------------------------
void LevelAnimatorEvents::ThiefCaught( puBlock *policeman )
{

	if ( ! _enabled )
		return;

	AnimManager::Get()->PlayAnim( GameTypes::eAnim_CopSmiles, eAnimPiority_High, policeman );

}
//-----------------------------------------------------------------------------------------------
void LevelAnimatorEvents::ThiefSmiles( puBlock *thief )
{

	if ( ! _enabled )
		return;

	AnimManager::Get()->PlayAnim( GameTypes::eAnim_ThiefSmiles, eAnimPiority_High, thief );
}
//-----------------------------------------------------------------------------------------------
void LevelAnimatorEvents::PiggyStolen( puBlock *piggy )
{

	if ( ! _enabled )
		return;

	b2Filter filter;
	filter.groupIndex = -1;

	if ( piggy && piggy->GetBody() && piggy->GetBody()->GetFixtureList() )
	{
		b2Vec2 move;
		move.x = (float) Tools::Rand( -50, 50 );
		move.y = (float) Tools::Rand( 50, -1200 );

		move.y = 100000;
		move.x = 100000;

		piggy->GetBody()->GetFixtureList()->SetFilterData( filter );
		piggy->GetBody()->SetLinearDamping( 0.5f );
		piggy->GetBody()->SetType( b2_dynamicBody );
		//piggy->GetBody()->ApplyLinearImpulse( move, piggy->GetPosition() );
		
	}
	AnimManager::Get()->PlayAnim( GameTypes::eAnim_PiggyBankStolen, eAnimPiority_High, piggy );
}
//-----------------------------------------------------------------------------------------------
void LevelAnimatorEvents::SetLevel( Level *level )
{

	_level = level;
}
//-----------------------------------------------------------------------------------------------
