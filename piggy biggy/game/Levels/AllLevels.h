#ifndef __ALLLEVELS_H__
#define __ALLLEVELS_H__

#include "Play/LevelIntro.h"
#include "Play/LevelLetsBomb.h"
#include "Play/LevelLetsRotate.h"
#include "Play/LevelLabyrinth.h"
#include "Play/LevelLetsRoll.h"
#include "Play/LevelLetsRell.h"
#include "Play/LevelLetsSwing.h"
#include "Play/LevelBank.h"
#include "Play/LevelLetsShoot.h"
#include "Play/LevelTraps.h"
#include "Play/LevelConstruction.h"
#include "Play/LevelTetris.h"
#include "Play/LevelDomino.h"
#include "Play/LevelScale.h"
#include "Play/LevelChain.h"
#include "Play/LevelOthers.h"
#include "Play/LevelMagnet.h"
#include "Play/LevelLetsSynchronize.h"

#include "Play/LevelVehical.h"
#include "Play/LevelLetsTest.h"

#include "Play/LevelAdam.h"
#include "Play/LevelRafal.h"
#include "Play/LevelPawel.h"
#include "Play/LevelTomekW.h"
#include "Play/LevelBogusia.h"
#include "Play/LevelJacekZ.h"
#include "Play/LevelMarzena.h"
#include "Play/LevelLukasz.h"

#include "Play/LevelDemo.h"

#endif
