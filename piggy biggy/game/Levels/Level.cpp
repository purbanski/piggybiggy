#include <vector>
#include <set>
#include "cocos2d.h"
#include "Game.h"
#include "GameConfig.h"
#include "Level.h"
#include "Tools.h"
#include "Debug/MyDebug.h"
#include "DebugDraw.h"
#include "SoundEngine.h"
#include "Levels/LevelMenu.h"
#include "Levels/BlockOperator.h"
#include "GameTypes.h"
#include "GameGlobals.h"
#include "Preloader.h"
#include "LoadingBar.h"
#include "Pockets.h"
#include "Scenes/UnlockPocketScene.h"
#include "Scenes/BuyScene.h"
#include "LevelOrientMsg.h"
#include "Animation/AnimManager.h"
#include "Debug/DebugLogger.h"
#include "Scenes/NextLevelScene.h"
#include "RunLogger/RunLogger.h"
#include "GLogger.h"
#include "Platform/Lang.h"
#include "GameWatcher.h"
#include "Appirater/cppAppirater.h"
#include "Platform/ScoreTool.h"
#include "Facebook/FBTool.h"
#include "Facebook/Menus/FBAsk4HelpMenu.h"

#ifdef BUILD_TEST
#include "Tests/LevelTestNode.h"
#include "Debug/DebugTimer.h"
#include "GameGlobals.h"
#endif

#include "TestFlight/Report.h"

using namespace GameTypes;

//--------------------------------------------------------------------------------------
// Level
//--------------------------------------------------------------------------------------
Level::Level( GameTypes::LevelEnum levelEnum, bool viewMode, bool sleepAllowed ) : World( sleepAllowed )
{
    Preloader::Get()->Reset();
    
    _levelType = eLevelTypeNormal;
    _levelTag = eLevelTag_Unknown;
    
    gAppDisplayRotateMode = eDisplay_LandscapeRotating;
    
#ifdef _DEBUG
	Debug::Init();
#endif

#ifdef BUILD_TEST
	_unfailable = false;
#endif
	
	_levelEnum = levelEnum;
    _levelIndex = Game::Get()->GetGameStatus()->GetLevelSequence().GetLevelIndex( _levelEnum );

	_menu = NULL;
	_levelFinishMenu = NULL;
	_quiting = false; 
	_background = NULL;
	_levelOrientMsg = NULL;
	_scoreCounter = NULL;
    
    _showingSkipIntro = false;
	_styleSet = false;
    _isScheduled = false;
	_demoRuning = false;
    _viewMode = viewMode;
    _subMenuShown = false;
    
	_demoCounter = 0;
	_msgCounter = 0;
	_coinCount = 1;
	_delayCounter = 0;
	_currentState = eLevelState_Running;
	_levelOrient = GameTypes::eLevel_Horizontal;

	_world.SetContactListener( &_contactSolver );
	_blockOperator = new LevelBlockOperator();
	
#ifndef BUILD_EDITOR
	//-----------------
	// Scale Layer
	//-----------------
	//if ( Tools::GetScreenSize().width != 480 )
	{
		_scaleScreenLayer = ScaleScreenLayer::node();
		_scaleScreenLayer->setPosition( ccp( 0.0f, 0.0f ));
		addChild( _scaleScreenLayer, GameTypes::eLevelScaleLayer );
	}
#else
	AddLevelEnum();
#endif


#ifdef BUILD_EDITOR
	//----------------
	// Debug draw
	_debugDraw = new DebugDraw( this );
	addChild( _debugDraw, GameTypes::eLevelDebug );
	_debugDraw->EnableDraw( true );
	_debugDraw->EnableDraw( false );
	_debugDraw->setPosition( ccp( -Tools::GetScreenMiddleX(), -Tools::GetScreenMiddleY() ));
#endif


	//----------------
	// Game events receiver
	_contactSolver.SetRecv( this );
	

	//----------------
	// Set Level position
	CCSize size;
	size = CCDirector::sharedDirector()->getWinSize();
	setPosition( CCPoint( size.width / 2.0f, size.height / 2.0f ));

	SetPocketSkin();

	AnimManager::Get()->Disable();
	_animatorIdleBlocks.SetLevel( this );
	_animatorActiveBlocks.SetLevel( this );
	_animatorEvents.SetLevel( this );
}
//--------------------------------------------------------------------------------------
void Level::ConstFinal()
{
    if ( _viewMode )
    {
        ConstFinalMinimal();
        return;
    }
    
    int secondsCount;
    secondsCount = Game::Get()->GetGameStatus()->GetLevelTime( _levelEnum );
    
    RLOG_II("LEVEL_CONST", _levelIndex, secondsCount );

    stringstream ss;
    ss << "Level " << _levelIndex;
    GSendView( ss.str().c_str(), NULL );
    GLogLevelEvent(_levelIndex, "Construct");
    
	CCSize size;
	size = CCDirector::sharedDirector()->getWinSize();

#ifndef BUILD_EDITOR
	//----------------
	// Finish Level Menu
	_levelFinishMenu = LevelFinishMenuNode::node( this );
	addChild( _levelFinishMenu, GameTypes::eLevelMenu );


	//----------------
	// Level Menu
	_menu = LevelTopRightMenu::Create( this );
	_menu->setPosition( ccp( 0.0f, 0.0f ));
	addChild( _menu, GameTypes::eLevelMenu );
	addChild( _menu->GetMenuButtons(), GameTypes::eLevelMenuButtons );
#endif

	SetOrient(); // for gravity etc

	//----------------
	// Orient Msg
	if ( ! _levelOrientMsg && _levelOrient == eLevel_Vertical )
    {
		_levelOrientMsg = LevelOrientMsg::Create( _levelOrient );
        addChild( _levelOrientMsg, GameTypes::eSpritesOrientMsg );
    }
	
	_border.SetPosition( b2Vec2( 0.0f, 0.0f ));
	AddSprites();
	UpdateBlockContainers();

#ifdef  INTRO_POINTER
    SetUpScoreCounter();
#else
	if ( ! IsDone() )
        SetUpScoreCounter();
#endif
    
	AnimManager::Get()->Reset();

	//-------------------
	// Preloader

	//-------------------
	// If level name wasn't shown yet - preload level name stuff
	if ( ! Game::Get()->GetGameStatus()->GetLevelNameShowed( _levelEnum ))
	{
		Preloader::Get()->AddFont( Lang::Get()->GetLang( Config::LevelNamePlayFont ));
		Preloader::Get()->AddSound( SoundEngine::eSoundLevelNameShow );
		Preloader::Get()->AddSound( SoundEngine::eSoundLevelNameHide );
	}


	Preloader::Get()->AddSound( SoundEngine::eSoundLevelFinished );
	Preloader::Get()->AddSound( SoundEngine::eSoundLevelFailed );
	Preloader::Get()->AddSound( SoundEngine::eSoundLevelSkipped );
	Preloader::Get()->AddSound( SoundEngine::eSoundLevelRestart );
	Preloader::Get()->AddSound( SoundEngine::eSoundChangeScene );
	Preloader::Get()->AddSound( SoundEngine::eSoundButtonClick );
	Preloader::Get()->AddSound( SoundEngine::eSoundCoinCollected );
	Preloader::Get()->AddSound( SoundEngine::eSoundCoinScored );
	Preloader::Get()->AddSound( SoundEngine::eSoundCoinScoredReversed );
	Preloader::Get()->AddSound( SoundEngine::eSoundPocketUnlock );

	
	Preloader::Get()->AddImageDir( "Images/Menus/LevelMenu/" );
	Preloader::Get()->AddImageDir( "Images/Menus/SoundMenu/" );
	Preloader::Get()->AddImageDir( "Images/OtherPreload/" );
	
	if ( Game::Get()->GetGameStatus()->GetLevelStatus( _levelEnum ) != GameStatus::eLevelStatus_Done )
	{
		Preloader::Get()->AddSound( SoundEngine::eSoundScoreTimeTick );
		Preloader::Get()->AddSound( SoundEngine::eSoundScoreTimeTickFinal );
		Preloader::Get()->AddAnim( "ScoreCounter" );
	}

#ifdef BUILD_TEST

	string levelName;
	levelName = GameGlobals::Get()->LevelNames()[_levelEnum ];

	GameLevelSequence &sequnce = Game::Get()->GetGameStatus()->GetLevelSequence();

	std::replace( levelName.begin(), levelName.end(), '\n','.');
	ss << "Preload Level: " << levelName ;
	ss << "(enum:"<< _levelEnum << "/" ;
	ss << "index:" << ( sequnce.GetLevelIndex( _levelEnum ) + 1 ) << ")";

	DebugTimer::Get()->SetLabel( 2, ss.str().c_str() );
	DebugTimer::Get()->Start( 2 );
#endif
	
	if ( _levelEnum != Game::Get()->GetGameStatus()->GetPreviouslyPlayedLevel() )
	{
		Preloader::Get()->FreeResources();
		Preloader::Get()->Preload();
	}

#ifdef BUILD_TEST
	DebugTimer::Get()->Stop( 2 );
	DebugTimer::Get()->InsertEmptyLine();


	LevelTestNode *tests = LevelTestNode::Create( this );
	tests->setPosition( ccp( 0.0f, 0.0f ));
	addChild( tests, 255 );
#endif 
    

//    _fbPromoNode = FBPromoNode::Create( _levelEnum );
//    _fbPromoNode->Hide();
//    addChild( _fbPromoNode, eSpritesFBHighScore );
//    _fbPromoNode->setPosition( CCPoint( -105.0f, 0.0f ));

	LevelRun();

}
//--------------------------------------------------------------------------------------
void Level::ConstFinalMinimal()
{
    _scaleScreenLayer = ScaleScreenLayer::node();
	_scaleScreenLayer->setPosition( ccp( 0.0f, 0.0f ));
		addChild( _scaleScreenLayer, GameTypes::eLevelScaleLayer );
    
   	AddSprites();
	UpdateBlockContainers();
    SetOrient(); // for gravity etc
    LevelRun();
}
//--------------------------------------------------------------------------------------
void Level::UpdateBlockContainers()
{
	ResetContainers();

	b2Body *body;
	puBlock *block;
	
	body = GetWorld()->GetBodyList();
	while ( body != NULL )
	{
		block = (puBlock *) body->GetUserData();
		if ( ! block )
		{
			body = body->GetNext();
			continue;
		}

		if ( GameGlobals::Get()->FullAnimableBlockNames().count( block->GetBlockEnum() ))
			_animableBlocks.insert( block );

		if ( GameGlobals::Get()->RolledAnimableBlockNames().count( block->GetBlockEnum() ))
			_rolledAnimableBlocks.insert( block );

		if ( GameGlobals::Get()->StepableBlockNames().count( block->GetBlockEnum() ))
			_stepableBlocks.insert( block );

		if ( GameGlobals::Get()->QuitableBlockNames().count( block->GetBlockEnum() ))
			_quitableBlocks.insert( block );

   		if ( GameGlobals::Get()->ExitableBlockNames().count( block->GetBlockEnum() ))
			_exitableBlocks.insert( block );
        
		if ( GameGlobals::Get()->FinishableBlockNames().count( block->GetBlockEnum() ))
			_finishableBlocks.insert( block );

        if ( block->GetBlockEnum() == eBlockPiggyBank )
			_piggyBankDynamicBlocks.insert( block );

		if ( body->GetType() != b2_staticBody )
			_bouyentBlocks.insert( block );

		body = body->GetNext();
	}
}
//--------------------------------------------------------------------------------------
Level::~Level()
{
    ResetContainers();
    delete _blockOperator;
	
    if ( _scoreCounter )
		_scoreCounter->StopQuiet();

	removeAllChildrenWithCleanup( true );
	_msgManager.clear();
   
   NextLevelScene::Unlock();
}
//--------------------------------------------------------------------------------------
void Level::Step( cocos2d::ccTime dt )
{	
	World::Step();
    SoundEngine::Get()->Update();
    
    UpdateSprites();
	BlocksStep();

	_contactSolver.Process();
	_particleEngine.Update();

	CheckDelayedStateChange();
	MsgTick();
	
	if ( ! _demoRuning && ! _viewMode )
	{
		_animatorIdleBlocks.Process( dt );
		_animatorActiveBlocks.Process( dt );
	}
}
//--------------------------------------------------------------------------------------
void Level::UpdateSprites()
{

	b2Body *body;
	puBlock *block;
	
	body = _world.GetBodyList();
	while (  body != NULL )
	{
		block = (puBlock *) body->GetUserData();
		if ( block != NULL)
			block->UpdateSprite( false );

		body = body->GetNext();
	}
}
//--------------------------------------------------------------------------------------
void Level::AddSprites()
{

	b2Body *body;
	puBlock *block;
	CCSprite *sprite;

	body = _world.GetBodyList();
	while (  body != NULL )
	{
		block = (puBlock *) body->GetUserData();
		if ( block != NULL )
		{
			sprite = block->GetSprite();
			if ( sprite != NULL )
			{
				// FIX ME 
				// No one should be added here 

				if ( ! sprite->getParent() ) 
				{
//					unAssertMsg( Level, false, ("PROBLEM!!! day and night anim wont start on the sprite"));
					addChild( sprite, block->GetSpriteZOrder() );
				}
				block->UpdateSprite();
			}
		}
		body = body->GetNext();
	}
}
//--------------------------------------------------------------------------------------
bool Level::IsEnding()
{
	return ( _quiting || ( _currentState != eLevelState_Running ));
}
//--------------------------------------------------------------------------------------
void Level::GameCoinCollected( puBlock *piggy )
{
	if ( IsEnding() )
		return;
	
#ifndef BUILD_EDITOR4ALL
	if ( ! _demoRuning && ! _viewMode )
		SoundEngine::Get()->PlayEffect( SoundEngine::eSoundCoinCollected );
#endif

	_coinCount--;
    if ( ! _demoRuning && ! _viewMode )
        Game::Get()->GetGameStatus()->IncCoinCollectedCount();
    
	if ( ! _coinCount && ! _demoRuning && ! _viewMode )
	{
		LevelCompletedPre();
	}

	if ( ! _coinCount )
	{
		_animatorEvents.LevelDone();
        if ( _levelOrient )
            _levelOrientMsg->Disable();
	}
	else
		_animatorEvents.CoinCollected( piggy );
}
//--------------------------------------------------------------------------------------
void Level::GameThiefCaught( puBlock *policeman )
{
	if ( IsEnding() )
		return;
	
	if ( ! _demoRuning && ! _viewMode )
	{
		_animatorEvents.ThiefCaught( policeman );
	}
}
//--------------------------------------------------------------------------------------
void Level::GameBlockExplode( b2Vec2 pos )
{
	_particleEngine.Effect_Fireworks( this, pos );

	if ( IsEnding() )
		return;

	if ( ! _demoRuning && ! _viewMode )
		SoundEngine::Get()->PlayRandomEffect( 
			10.0f,
			SoundEngine::eSoundFireworks1, 
			SoundEngine::eSoundFireworks2, 
			SoundEngine::eSoundNone 
			);
}
//--------------------------------------------------------------------------------------
void Level::GameMoneyExplode( b2Vec2 pos )
{

	_particleEngine.Effect_Fireworks( this, pos );

	if ( IsEnding() )
		return;
}
//--------------------------------------------------------------------------------------
void Level::GameCoinStolen( puBlock *thief )
{
	_animatorEvents.ThiefSmiles( thief );

	if ( IsEnding() )
		return;

	if ( ! _demoRuning && ! _viewMode )
	{
		LevelFailedPre();
	}
}
//--------------------------------------------------------------------------------------
void Level::GamePiggyStolen( puBlock *piggyBank, puBlock *thief )
{

	_animatorEvents.ThiefSmiles( thief );
	//_animatorEvents.PiggyStolen( piggyBank );

	if ( IsEnding() )
		return;

	if ( ! _demoRuning && ! _viewMode)
	{
		LevelFailedPre( false );
		SoundEngine::Get()->PlayEffect( SoundEngine::eSoundAnim_PiggyStolen );
	}
}
//--------------------------------------------------------------------------------------
void Level::GameCoinSilverCollected( b2Vec2 pos )
{
	if ( IsEnding() )
		return;

	if ( ! _demoRuning && ! _viewMode)
	{
		SoundEngine::Get()->PlayEffect( SoundEngine::eSoundCoinCollected );
		AnimerSimpleEffect::Get()->ShowMsg( this, LocalString("BadInvestmentMsg"), GameTypes::eLevelMsg, 2.0f, false );
		LevelFailedPre();
	}
}
//--------------------------------------------------------------------------------------
void Level::GameCoinExplode()
{

	if ( IsEnding() )
		return;

	if ( ! _demoRuning && !_viewMode )
	{
		LevelFailedPre();
	}
}
//--------------------------------------------------------------------------------------
void Level::GameOutOfBorder()
{

#ifdef BUILD_EDITOR
	return;
#endif

	if ( IsEnding() )
		return;

	if ( ! _demoRuning && ! _viewMode )
	{
		LevelFailedPre();
	}
}
//--------------------------------------------------------------------------------------
void Level::GameCustomAction_ContactBegin( puBlock *aBlock, puBlock *bBlock, b2Contact *contact )
{
}
//--------------------------------------------------------------------------------------
void Level::GameCustomAction_ContactEnd( puBlock *aBlock, puBlock *bBlock, b2Contact *contact )
{
}
//--------------------------------------------------------------------------------------
void Level::CheckDelayedStateChange()
{
	if ( ! _delayCounter || _demoRuning || _quiting || _viewMode )
		return;

	if ( ! ( --_delayCounter ))
	{
		switch ( _currentState )
		{
			case GameTypes::eLevelState_Failed :
				LevelFailed();
				break;

			case GameTypes::eLevelState_Finished :
				LevelCompleted();
				break;

			default:
				unAssertMsg( Game, 0, ( "Unknown level state: %d", _currentState ) );
		}
		_menu->EnableRestartButton( false );
	}
}
//--------------------------------------------------------------------------------------
void Level::SetDelayedStateChange( LevelState state, int delay )
{	
#ifdef BUILD_EDITOR
	return;
#endif

	// something already schedule
	if ( _delayCounter != 0 )
		return;

	//_menu->EnableMenuButton( false );

	_delayCounter = delay;
	_currentState = state;
}
//-----------------------------------------------------------------------------------
float Level::GetScreenMiddleX()
{
	CCSize size;
	size = CCDirector::sharedDirector()->getWinSize();
	return ( size.width / 2 / RATIO );
}
//-----------------------------------------------------------------------------------
float Level::GetScreenMiddleY()
{
	CCSize size;
	size = CCDirector::sharedDirector()->getWinSize();
	return ( size.height / 2 / RATIO );
}
//-----------------------------------------------------------------------------------
bool Level::init()
{
	if ( ! CCLayer::init() )
		return false;

    if ( ! _viewMode )
    {
        setIsTouchEnabled( true );
    }
    
	return true;
}
//-----------------------------------------------------------------------------------
void Level::registerWithTouchDispatcher()
{
	CCTouchDispatcher::sharedDispatcher()->addStandardDelegate( this, Config::eMousePriority_GameBlockOperator );
}
//-----------------------------------------------------------------------------------
void Level::ccTouchesBegan( CCSet *touches, CCEvent *eventt )
{
#ifdef INTRO_POINTER
//	CCTouch *touch;
    int i = 0;
	for ( CCSetIterator it = touches->begin(); it != touches->end(); it++ )
	{
        //	touch = ;
		_introPointer = IntroPointer::Create();
        addChild( _introPointer, 255 );
	}
#endif
    
  	if ( _demoRuning || _viewMode )
		return;

	if ( ! ( _isScheduled ))
		return;

	if ( IsEnding() )
		return;

	CCSetIterator it;
	CCTouch *touch;
	for ( it = touches->begin(); it != touches->end(); it++ )
	{
		touch = (CCTouch *) ( *it );
		_blockOperator->MouseDown( touch );
	}
}
//-----------------------------------------------------------------------------------
void Level::ccTouchesMoved( CCSet *touches, CCEvent *pEvent )
{
#ifdef INTRO_POINTER
//	CCTouch *touch;
	for ( CCSetIterator it = touches->begin(); it != touches->end(); it++ )
	{
        //	touch = ;
		_introPointer->SetPosition( (CCTouch *) ( *it ) );
	}
#endif
    
	if ( _demoRuning || _viewMode )
		return;

	if ( ! ( _isScheduled ))
		return;

	if ( IsEnding() )
		return;

	CCSetIterator it;
	CCTouch *touch;
	for ( it = touches->begin(); it != touches->end(); it++ )
	{
		touch = (CCTouch *) ( *it );
		_blockOperator->MouseMove( touch );
	}
}
//-----------------------------------------------------------------------------------
void Level::ccTouchesEnded( CCSet *touches, CCEvent *pEvent )
{
    #ifdef INTRO_POINTER
//	CCTouch *touch;
	for ( CCSetIterator it = touches->begin(); it != touches->end(); it++ )
	{
        //	touch = ;
		_introPointer->SetPosition( (CCTouch *) ( *it ) );
        _introPointer->Hide();
	}
#endif

	if ( _demoRuning || _viewMode )
		return;

	if ( ! ( _isScheduled ))
		return;

	if ( IsEnding() )
		return;

	CCSetIterator it;
	CCTouch *touch;
	for ( it = touches->begin(); it != touches->end(); it++ )
	{
		touch = (CCTouch *) ( *it );
		_blockOperator->MouseUp( touch );
	}
}
//-----------------------------------------------------------------------------------
void Level::ccTouchesCancelled( CCSet *pTouches, CCEvent *pEvent )
{
}
//-----------------------------------------------------------------------------------
void Level::ToggleDebugDraw()
{
	_debugDraw->EnableDraw( (_debugDraw->GetEnable() ? false : true ));
}
//-----------------------------------------------------------------------------------
void Level::LevelRun()
{
    if ( _isScheduled || _subMenuShown )
        return;
    
	if ( ! _demoRuning && !_viewMode )
	{
		DisplayLevelName();
        
		SoundEngine::Get()->GetBackgroundMusic().SetMusic_Game();

		_scoreCounter->Start();
		
		//------------------------
		// Open current pocket
		//if ( _openCurrentPocketNode )
		//	_openCurrentPocketNode->StartAnim();
		

		//------------------------
		// Animator
		AnimManager::Get()->Enable();
		AnimManager::Get()->ResumeAllActions( this );

        if ( _levelOrient )
            _levelOrientMsg->Enable();
        
        setIsTouchEnabled(true);
	}

	_isScheduled = true;
	schedule( schedule_selector( Level::Step ));
	
	_particleEngine.ResumeEngine();
	//SoundEngine::Get()->ResumeAllEffects();

	DisplayLevelStartMsg();
	UpdateSprites();
}
//-----------------------------------------------------------------------------------
void Level::LevelPause()
{
	AnimManager::Get()->PauseAllActions( this );
	_isScheduled = false;
	unschedule( schedule_selector( Level::Step ));
	_particleEngine.PauseEngine();
	SoundEngine::Get()->PauseAllEffects();
	//SoundEngine::Get()->PauseBackgroundMusic();
    
	if ( _scoreCounter )
		_scoreCounter->Pause();
    
    if ( _levelOrient )
        _levelOrientMsg->Disable();
}
//-----------------------------------------------------------------------------------
void Level::LevelCompletedPre()
{
#ifdef INTRO_POINTER
    BlocksLevelPlayFinished();
    return;
#endif
    
    if ( _showingSkipIntro )
        return;

	_menu->EnableMenuButton( false );
    _levelFinishMenu->Show( eLevelState_Finished );

	DisplayLevelFinishedMsg();
    GameWatcher::Get()->RecordEvent( GameTypes::eGameEvent_FBLevelCompleted, NULL );

	if ( !IsDone() && _scoreCounter )
	{
        _scoreCounter->Stop();
		SetDelayedStateChange( GameTypes::eLevelState_Finished, (float) Config::LevelStateChangeDelay * 2.5f );
        ScoreTool::Get()->PushLevelScore( FBTool::Get()->GetFBUserId().c_str(), _levelEnum );

        int solvingTime;
        solvingTime = Game::Get()->GetGameStatus()->GetLevelTime( _levelEnum );

        D_INT( solvingTime );
        
        RLOG_II("LEVEL_COMPLETED", _levelIndex, solvingTime );
        GLogLevelEvent( _levelIndex, "Completed");
    }
	else
    {
        int solvingTime;
        solvingTime = Game::Get()->GetGameStatus()->GetLevelTime( _levelEnum );
        
        RLOG_SII("LEVEL_COMPLETED", "AGAIN", _levelIndex, solvingTime );
        GLogLevelEvent( _levelIndex, "Completed again");
        
		SetDelayedStateChange( GameTypes::eLevelState_Finished, (float) Config::LevelStateChangeDelay * 4.5f );
    }
	
#ifdef INTRO_POINTER
		SetDelayedStateChange( GameTypes::eLevelState_Finished, Config::LevelStateChangeDelayIntroPointer );
#endif
    
    Game::Get()->LevelFinished();    
	BlocksLevelPlayFinished();
    SlowDownPiggyBank();
}
//-----------------------------------------------------------------------------------
void Level::LevelCompleted()
{
    if ( _scoreCounter )
    {
        _scoreCounter->ShowAnim();
        
        switch (_levelType)
        {
            case GameTypes::eLevelTypeNormal :
            case GameTypes::eLevelTypeCar :
            case GameTypes::eLevelTypeAccelerometr :
                ccpAppirater::UserDidSignificantEvent();
                break;
            
            case GameTypes::eLevelTypeVertical :
                ccpAppirater::UserDidSignificantEvent( false );
                break;

            case GameTypes::eLevelTypeDemo :
                break;
            
            default :
                ;
        }
    }
    else if ( _levelOrient == eLevel_Horizontal )
    {
//        _fbPromoNode->SetViewMode( ePromoNodeCmd_ViewHighscore, true );
//        _fbPromoNode->Show();
    }
}
//-----------------------------------------------------------------------------------
void Level::LevelFailed()
{
    if ( IsDone() && _levelOrient == eLevel_Horizontal )
    {
//        _fbPromoNode->SetViewMode( ePromoNodeCmd_ViewHighscore, true );
//        _fbPromoNode->Show();
    }
}
//-----------------------------------------------------------------------------------
void Level::LevelFailedPre( bool startPiggyAnim )
{
#ifdef INTRO_POINTER
  	BlocksLevelPlayFinished();
    return;
#endif
    
#ifdef BUILD_TEST
	if ( _unfailable )
		return;
#endif
    
    if ( _showingSkipIntro )
        return;
    
    int secondsCount;
    secondsCount = Game::Get()->GetGameStatus()->GetLevelTime( _levelEnum );
    
    RLOG_II("LEVEL_FAILED", _levelIndex, secondsCount );
    GLogLevelEvent(_levelIndex, "Failed");
    

#ifdef INTRO_POINTER
	SetDelayedStateChange( GameTypes::eLevelState_Failed, Config::LevelStateChangeDelayIntroPointer );
#else
    _menu->EnableMenuButton( false );
    SetDelayedStateChange( GameTypes::eLevelState_Failed, Config::LevelStateChangeDelay * 4 );
#endif

	if ( _scoreCounter )
		_scoreCounter->Pause();

	if ( startPiggyAnim )
		_animatorEvents.LevelFailed();

	BlocksLevelPlayFinished();
    
    if ( _levelOrient )
        _levelOrientMsg->Disable();

#ifndef INTRO_POINTER
    if ( Game::Get()->CheckForSkipLevelIntro() )
    {
        ShowSkipLevelIntro();
        _levelFinishMenu->Show( _currentState );
    }
    else
    {
        _levelFinishMenu->Show( _currentState );
    }
#endif
}
//-----------------------------------------------------------------------------------
void Level::MsgTick()
{
	if ( ! _msgManager.size() )
		return;

	_msgCounter++;
    
    LevelMsgManager::iterator it = _msgManager.begin();

    while( it != _msgManager.end() )
    {
//        D_INT((*it)->GetTime())
//        D_INT(_msgCounter)
        
		if ((*it)->GetTime() == _msgCounter )
		{
			addChild( (*it) , (*it)->GetZOrder() );
			(*it)->DisplayMsg();
            _msgManager.erase( it++ );
		}
        else
            ++it;
    }
}
//-----------------------------------------------------------------------------------
bool Level::IsDebugEnabled()
{
	return _debugDraw->GetEnable();
}
//-----------------------------------------------------------------------------------
void Level::EnableDebugDraw( bool enable )
{

	_debugDraw->EnableDraw( enable );
}
//-----------------------------------------------------------------------------------
void Level::onExit()
{
  	BlocksLevelExit();
    
	CCTouchDispatcher::sharedDispatcher()->removeDelegate( this );
	CCNode::onExit();
}
//-----------------------------------------------------------------------------------
void Level::onEnter()
{
	CCLayer::onEnter();
}
//-----------------------------------------------------------------------------------
void Level::onEnterTransitionDidFinish()
{
	CCLayer::onEnterTransitionDidFinish();
}
//-----------------------------------------------------------------------------------
void Level::Quiting()
{
	if ( _demoRuning || _viewMode )
		return;
    
    SoundEngine::Get()->FadeOutAllEffects();
	_menu->EnableMenuButton( false );
	_menu->EnableRestartButton( false );
	
	AnimManager::Get()->Disable();
   	AnimManager::Get()->Reset();
    //	Preloader::Get()->Reset();

	_animatorActiveBlocks.Disable();
	_animatorIdleBlocks.Disable();
	_animatorEvents.Disable();
	
	_quiting = true;
	_blockOperator->RelaseAllMouseJoints();
	if ( _scoreCounter )
		_scoreCounter->Pause();
		
	CCAccelerometer::sharedAccelerometer()->setDelegate( NULL ); 
    BlocksLevelQuit();
	Game::Get()->ResetRuningLevel();
}
//-----------------------------------------------------------------------------------
void Level::Center()
{
	setPosition( ccp( Tools::GetScreenMiddleX() * getScale(), Tools::GetScreenMiddleY() * getScale() ));
}
//-----------------------------------------------------------------------------------
//void Level::EnableMenuButton( bool enable )
//{
//
//	_menu->EnableRestartButton( enable );
//}
//-----------------------------------------------------------------------------------
void Level::DisplayMsg( const char* str, const b2Vec2& pos )
{
	CCLabelBMFont *label = CCLabelBMFont::labelWithString( str, Config::LevelMsgFont );

	float dur;
	dur = strlen(str) / 3.0f;

	D_FLOAT( dur )

	label->setPosition( ccp( 2000, 0 ) );
	label->setScale( 1.0f / getScale() );
	addChild( label, GameTypes::eLevelEditorMsg );

	CCMoveTo *move = CCMoveTo::actionWithDuration( 15.0f, ccp( -2000, 0 ));
	label->runAction( CCSequence::actions( move, NULL ));
}
//-----------------------------------------------------------------------------------
void Level::DisplayMsg( const char* str )
{
	DisplayMsg( str, b2Vec2( 0.0f, 0.0f ));
}
//-----------------------------------------------------------------------------------
void Level::DisplayLevelStartMsg()
{
	if ( _msgStart.size() > 0 )
		DisplayMsg( _msgStart.c_str() );
}
//-----------------------------------------------------------------------------------
void Level::DisplayLevelFinishedMsg()
{
	if ( _msgFinish.size() > 0 )
		DisplayMsg( _msgFinish.c_str() );
}
//-----------------------------------------------------------------------------------
void Level::BlocksStep()
{
	puBlock *block;
	BlockSet::iterator it;

	for ( it = _stepableBlocks.begin(); it != _stepableBlocks.end(); it++ )
	{
		block = *it;
		if ( block != NULL && block->GetBody() && block->GetBody()->GetFixtureList() )
			block->Step();
	}
}
//-----------------------------------------------------------------------------------
void Level::BlocksLevelExit()
{
#ifdef BUILD_EDITOR
	return;
#endif

	puBlock *block;
	BlockSet::iterator it;

	for ( it = _exitableBlocks.begin(); it != _exitableBlocks.end(); it++ )
	{
		block = *it;
		if ( block != NULL && block->GetBody() && block->GetBody()->GetFixtureList() )
			block->LevelExiting();
	}
}
//-----------------------------------------------------------------------------------
void Level::BlocksLevelQuit()
{
#ifdef BUILD_EDITOR
	return;
#endif

	puBlock *block;
	BlockSet::iterator it;

	for ( it = _quitableBlocks.begin(); it != _quitableBlocks.end(); it++ )
	{
		block = *it;
		if ( block != NULL && block->GetBody() && block->GetBody()->GetFixtureList() )
			block->LevelQuiting();
	}
}
//-----------------------------------------------------------------------------------
void Level::RemoveFromContainers( puBlock *block )
{

	D_SIZE( _animableBlocks )
    
	
	if ( _animableBlocks.count(block) )
		_animableBlocks.erase( block );

	D_SIZE( _animableBlocks )
}
//-----------------------------------------------------------------------------------
void Level::AdjustOldScreen()
{

	puBlock *block;
	b2Body *body;
	b2Vec2 pos;

	body = _world.GetBodyList();

	while( body )
	{
		block = (puBlock *) body->GetUserData();
		if ( block )
		{
			pos = block->GetReversedPosition();
			pos *= 2.0f;

			block->SetPosition( pos );
		}

		body = body->GetNext();
	}
}
//-----------------------------------------------------------------------------------
bool Level::IsDone()
{
	return ( Game::Get()->GetGameStatus()->GetCurrentLevelStatus() == GameStatus::eLevelStatus_Done );
}
//-----------------------------------------------------------------------------------
void Level::SetPocketSkin()
{
	GameTypes::PocketType pocket;
 	pocket = PocketManager::Get()->LevelInWhichPocket( _levelEnum );
    Skins::SetSkinName( pocket );
}
//-----------------------------------------------------------------------------------
void Level::SetOrient()
{
	if ( ! _background )
    {
		if ( _levelOrient == GameTypes::eLevel_Horizontal )
			_background = CCSprite::spriteWithFile( Skins::GetSkinName( "Images/Levels/Backgrounds/horizontal.jpg") );
		else
			_background = CCSprite::spriteWithFile( Skins::GetSkinName( "Images/Levels/Backgrounds/vertical.jpg") );
    }

	addChild( _background, GameTypes::eBackground );


#ifndef BUILD_EDITOR
	if ( _menu )
		_menu->SetOrientation( _levelOrient );
	
	if ( _levelFinishMenu )
	_levelFinishMenu->SetOrient( _levelOrient );

#endif
}
//-----------------------------------------------------------------------------------
//void Level::didAccelerate( CCAcceleration* pAccelerationValue )
//{
//	if ( _isScheduled && _levelOrientMsg )
//		_levelOrientMsg->AcceratorNotify( pAccelerationValue );
//}
//-----------------------------------------------------------------------------------
GameTypes::LevelOrientation Level::GetOrientation()
{
	return _levelOrient;
}
//-----------------------------------------------------------------------------------
void Level::BlocksLevelPlayFinished()
{
#ifdef BUILD_EDITOR
	return;
#endif

	puBlock *block;
	BlockSet::iterator it;

	for ( it = _finishableBlocks.begin(); it != _finishableBlocks.end(); it++ )
	{
		block = *it;
		if ( block != NULL && block->GetBody() && block->GetBody()->GetFixtureList() )
			block->LevelFinished();
	}
}
//-----------------------------------------------------------------------------------
void Level::ResetContainers()
{
	_animableBlocks.clear();
	_rolledAnimableBlocks.clear();
	_bouyentBlocks.clear();
	_stepableBlocks.clear();
	_quitableBlocks.clear();
	_finishableBlocks.clear();
    _exitableBlocks.clear();
    _piggyBankDynamicBlocks.clear();
}
//-----------------------------------------------------------------------------------
void Level::SetUpScoreCounter()
{
	if ( _scoreCounter )
		return;

	//------------------------
	// Score Counter
	_scoreCounter = ScoreCounter::Create( this );
#ifndef BUILD_EDITOR
	if ( ! _scoreCounter->getParent() )
	{
		CCNode *remainCoins = _scoreCounter->GetCoinsRemain();
		if ( remainCoins )
		{
			remainCoins->setPosition( CCPointZero );
			addChild( remainCoins, GameTypes::eLevelScoreCoins );
		}
		addChild( _scoreCounter, GameTypes::eLevelScoreStopper );
		_scoreCounter->SetOrientation( _levelOrient );
	}
#endif
}
//-----------------------------------------------------------------------------------
void Level::DisplayLevelName()
{
#ifdef INTRO_POINTER
    return;
#endif
    
	if ( Game::Get()->GetGameStatus()->GetLevelNameShowed( _levelEnum ))
		return;

	Game::Get()->GetGameStatus()->SetLevelNameShowed( _levelEnum, true );

    CCNode *levelName;
    
    if ( Lang::Get()->IsChinese() )
    {
        CCLabelTTF *levelNameT;
        // This should not work
        // and therefore it works  :)
        // because it does not work
        // it goes into some default - system font
        // which acctually works :)
        levelNameT = CCLabelTTF::labelWithString( GameGlobals::Get()->LevelNames()[ _levelEnum ].c_str(),  Config::LevelNamePlayFont, 110 );

        levelNameT->setOpacity( 0 );
        levelName = levelNameT;
    }
    else if ( Lang::Get()->IsJapanese() )
    {
        CCLabelTTF *levelNameT;
        // This should not work
        // and therefore it works  :)
        // because it does not work
        // it goes into some default - system font
        // which acctually works :)
        levelNameT = CCLabelTTF::labelWithString( GameGlobals::Get()->LevelNames()[ _levelEnum ].c_str(),  Config::LevelNamePlayFont, 70 );

        levelNameT->setOpacity( 0 );
        levelName = levelNameT;
    }
    else
    {
       	CCLabelBMFont *levelNameB;
        levelNameB = CCLabelBMFont::labelWithString( GameGlobals::Get()->LevelNames()[ _levelEnum ].c_str(), Lang::Get()->GetLang( Config::LevelNamePlayFont ));
        levelNameB->setOpacity( 0 );
      	levelNameB->setAlignment( CCTextAlignmentCenter );
        
        levelName = levelNameB;
    }
	
	CCAction* animSeq = (CCActionInterval*)(CCSequence::actions( 
		CCDelayTime::actionWithDuration( 1.65f ),
		CCCallFuncND::actionWithTarget( this, callfuncND_selector( SoundEngine::PlayEffectCallback ), (void*)(int) SoundEngine::eSoundLevelNameShow ), 
		CCFadeTo::actionWithDuration( 0.4f, 255 ),
		CCDelayTime::actionWithDuration( 3.2f ),
		CCCallFuncND::actionWithTarget( this, callfuncND_selector( SoundEngine::PlayEffectCallback ), (void*)(int) SoundEngine::eSoundLevelNameHide ), 
		CCFadeTo::actionWithDuration( 0.4f, 0 ),
		NULL ));

	levelName->setPosition( ccp( 0.0f, 0.0f ));

	if ( _levelOrient == eLevel_Vertical )
		levelName->setRotation( -90.0f );

	addChild( levelName, GameTypes::eLevelMsgAbove );
	levelName->runAction( animSeq );

}
//-----------------------------------------------------------------------------------
void Level::SlowDownPiggyBank()
{
    float dampingLimit;
    puBlock *block;
    
    dampingLimit = 0.9f;
    
    for ( BlockSet::iterator it = _piggyBankDynamicBlocks.begin(); it != _piggyBankDynamicBlocks.end(); it++ )
    {
        block = *it;
        if ( block && block->GetBody() )
        {
            if ( block->GetBody()->GetAngularDamping() < dampingLimit )
                block->GetBody()->SetAngularDamping( dampingLimit );
            
            //  if ( block->GetBody()->GetLinearDamping() < dampingLimit )
            //  block->GetBody()->SetLinearDamping( dampingLimit );
            
        }
    }
}
//-----------------------------------------------------------------------------------
void Level::ScoreShown()
{
    if ( _levelOrient == eLevel_Horizontal )
    {
//        _fbPromoNode->SetViewMode( ePromoNodeCmd_ViewHighscore, true );
//        _fbPromoNode->Show();
    }
}
//-----------------------------------------------------------------------------------
void Level::ScreenshotFlash()
{
    float dur;
    dur = 0.2f;
    
    _screenshotFlash->runAction( CCSequence::actions(
                                                     CCFadeTo::actionWithDuration( dur, 255 ),
                                                     CCDelayTime::actionWithDuration( dur/ 5.0f ),
                                                     CCFadeTo::actionWithDuration( dur, 0 )
                                                     )
                                );
}
//-----------------------------------------------------------------------------------
void Level::SubMenuShow( bool shown )
{
    _subMenuShown = shown;
}
//-----------------------------------------------------------------------------------
void Level::SetViewMode( bool enabled )
{
    _viewMode = true;
    if ( _scaleScreenLayer )
    {
        _scaleScreenLayer->removeFromParentAndCleanup( true );
        _scaleScreenLayer=NULL;
    }
}
//-----------------------------------------------------------------------------------
void Level::ShowSkipLevelIntro()
{
    D_INT(_msgCounter)
    _showingSkipIntro = true;
    
    _menu->EnableMenu(false);
    
    int size;
    float dur;
    string rlog;
    
    size = _msgManager.size();
    
    if ( _currentState == eLevelState_Running )
    {
        _msgManager.push_back( eOTMsg_SkipLevelIntro,
                              new LevelMsg_SkipLevelIntro( _msgCounter + 70.0f ));
        setIsTouchEnabled(false);
        dur = 6.5f;
        rlog.append("LEVEL_RUNNING");
    }
    else if ( _currentState == eLevelState_Failed )
    {
        _msgManager.push_back( eOTMsg_SkipLevelIntro, new LevelMsg_SkipLevelIntro_LevelFailed(_msgCounter + 200.0f ));
        dur = 6.0f;
        rlog.append("LEVEL_FAILED");
    }
    else
        return;

    //-------
    // Check if it was added
    if ( _msgManager.size() - size == 1 )
    {
        AnimerSimpleEffect::Get()->ShowMsg(
                                           this,
                                           LocalString("SkipIntroMsg"),
                                           GameTypes::eSpritesSkipLevelIntro + 1,
                                           dur,
                                           false );
        RLOG_S("SKIP_INTRO", rlog.c_str());
    }
    
    D_SIZE(_msgManager)
}
//-----------------------------------------------------------------------------------
void Level::ShowSubMenu(bool show)
{
    if ( show )
    {
        _menu->Menu_ShowSubMenu(NULL);
     //   _menu->EnableSubMenu( false );
    }
    else
        _menu->HideSubMenu();
}
//-----------------------------------------------------------------------------------
void Level::SkipLevelIntroFinished()
{
    _showingSkipIntro = false;
}


#ifdef BUILD_EDITOR
//-----------------------------------------------------------------------------------

//-----------------------------------------------------------------------------------
b2Vec2 Level::AdjustWithLevelPosition( const b2Vec2 &touch )
{

	CCPoint levelPos = Game::Get()->GetRuningLevel()->getPosition();
	float scale = Game::Get()->GetRuningLevel()->getScale();

	b2Vec2 temp;
	temp.x = touch.x - ( levelPos.x / scale - Tools::GetScreenMiddleX() ) / RATIO;
	temp.y = touch.y - ( levelPos.y / scale - Tools::GetScreenMiddleY() ) / RATIO;

	return temp;
}
//-----------------------------------------------------------------------------------
void Level::AddLevelEnum()
{

	stringstream ss;
	CCLabelBMFont *label;

	ss << "Level: " << _levelEnum << "\n";
	ss << GameGlobals::Get()->LevelNames()[ _levelEnum ];
	label = CCLabelBMFont::labelWithString( ss.str().c_str(), Config::LevelNameFont );
	label->setPosition( CCPoint( 400.0f, 280.0f ));
	
	addChild( label, 250 );
}
//-----------------------------------------------------------------------------------
#endif








DemoStep::DemoStep( int time, StepType type, puBlock* block )
{
	_time = time;
	_type = type;
	_block = block;
}
//-----------------------------------------------------------------------------------
DemoStep::DemoStep( int time, StepType type, puBlock* block, b2Vec2 force )
{
	_time = time;
	_type = type;
	_block = block;
	_force = force;
}
//-----------------------------------------------------------------------------------
void DemoStep::RunStep()
{
	switch ( _type )
	{
	case eDestroyBlock:
		_block->Destroy();
		break;
	
	case eApplyForce :
		//_block->GetBody()->ApplyForce( _r)
		//_block->Destroy();
		break;
	}
}
//-----------------------------------------------------------------------------------
LevelVertical::LevelVertical( GameTypes::LevelEnum levelEnum, bool viewMode ) : Level( levelEnum, viewMode )
{
    _levelType = eLevelTypeVertical;
    gAppDisplayRotateMode = eDisplay_PortraitFixed;

	_levelOrient = GameTypes::eLevel_Vertical;
	_world.SetGravity( b2Vec2( 10.0f, 0.0f ));
}
//-----------------------------------------------------------------------------------
bool LevelVertical::init()
{
	if ( ! Level::init() )
		return false;

    if ( ! _viewMode )
        setIsAccelerometerEnabled( true );

    return true;
}
//-----------------------------------------------------------------------------------
void LevelVertical::didAccelerate( CCAcceleration* pAccelerationValue )
{
	if ( _isScheduled && _levelOrientMsg )
		_levelOrientMsg->AcceratorNotify( pAccelerationValue );
}
//-----------------------------------------------------------------------------------
LevelDemo::LevelDemo( GameTypes::LevelEnum levelEnum, bool viewMode )  : Level( levelEnum, viewMode )
{
    _levelType = eLevelTypeDemo;
    
	_demoRuning = true;
	_coinCount = 999;
	Skins::SetSkinName("GreenPocket");

#ifndef BUILD_EDITOR
//	_menu->removeFromParentAndCleanup( true );
#endif
}
//-----------------------------------------------------------------------------------
void LevelDemo::ConstFinal()
{
	CCSize size;
	size = CCDirector::sharedDirector()->getWinSize();

	_levelFinishMenu = NULL;
	_menu = NULL;

	SetOrient(); // for gravity etc

	_border.SetPosition( b2Vec2( 0.0f, 0.0f ));
	AddSprites();
	UpdateBlockContainers();

	LevelRun();
}
//-----------------------------------------------------------------------------------
void LevelDemo::Step( cocos2d::ccTime dt )
{
	Level::Step( dt );
	DemoTick();
}
//-----------------------------------------------------------------------------------
void LevelDemo::DemoTick()
{
	if ( ! _demoList.size() )
		return;

	_demoCounter++;

	for ( DemoStepList::iterator it = _demoList.begin(); it != _demoList.end(); it++ )
	{
		if ((*it).GetTime() == _demoCounter )
		{
			(*it).RunStep();
		}
	}
}
//-----------------------------------------------------------------------------------
bool LevelDemo::init()
{
	if ( ! Level::init() )
		return false;

	setIsAccelerometerEnabled( false );
	return true;
}
//-----------------------------------------------------------------------------------
