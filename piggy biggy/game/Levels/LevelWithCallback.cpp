#include "LevelWithCallback.h"


//----------------------------------------------------------------------------------------------
LevelActivable::LevelActivable( GameTypes::LevelEnum levelEnum, bool viewMode ) : Level( levelEnum, viewMode )
{
	_stepCounter = 0;
}
//----------------------------------------------------------------------------------------------
void LevelActivable::ConstFinal()
{
	Sleepers::iterator it;
	Sleeper sleeper;

	for ( it = _sleepers.begin(); it != _sleepers.end(); it++ )
	{
		sleeper = (*it);

		sleeper._block->GetSprite()->setIsVisible( false );
		sleeper._block->GetSpriteShadow()->setIsVisible( false );
		sleeper._block->SetBodyType( b2_staticBody );
		sleeper._block->GetBody()->SetActive( false );
		sleeper._block->GetBody()->SetAwake( false );
	}
	Level::ConstFinal();
}
//----------------------------------------------------------------------------------------------
void LevelActivable::Step( cocos2d::ccTime dt )
{
	Sleepers::iterator it;
	Sleeper sleeper;

	for ( it = _sleepers.begin(); it != _sleepers.end(); it++ )
	{
		sleeper = (*it);

		if ( sleeper._time == _stepCounter )
		{
			sleeper._block->GetSprite()->setIsVisible( true );
			sleeper._block->GetSpriteShadow()->setIsVisible( true );
			sleeper._block->SetBodyType( b2_dynamicBody);
			sleeper._block->GetBody()->SetActive( true );
			sleeper._block->GetBody()->SetAwake( true );
		}
	}
	_stepCounter++;
	Level::Step( dt );
}


//----------------------------------------------------------------------------------------------
LevelWithCallback::LevelWithCallback( GameTypes::LevelEnum levelEnum, bool viewMode,  int stepMax ) : Level( levelEnum, viewMode )
{
	_stepMax = stepMax;
	_stepCounter = 0;
}
//----------------------------------------------------------------------------------------------
void LevelWithCallback::Step( cocos2d::ccTime dt )
{
	_stepCounter++;
	if ( _stepCounter == _stepMax )
	{
		_stepCounter = 0;
		CounterCallback();
	}
	Level::Step( dt );
}
//----------------------------------------------------------------------------------------------
