#ifndef __WORLD_H__
#define __WORLD_H__

#include "cocos2d.h"
#include <Box2D/Box2D.h>
#include "GLES-Render.h"

class World
{
public:
	static World* Get();

public:
	World( bool doSleep );
	virtual ~World();
	
	virtual void Step();
	
	void		DrawDebugData();
	b2Body*		GetGroundBody() { return _groundBody; }
	b2World*	GetWorld()		{ return &_world; }

	void		SetIterations( int32 velocity, int position );

protected:
	static World	*_sGWorld;

	b2World			_world;		
	b2Body			*_groundBody;

	float32			_timeStep;
	int32			_velocityIterations;
	int32			_positionIterations;
};

#endif
