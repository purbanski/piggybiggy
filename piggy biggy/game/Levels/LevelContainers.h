#ifndef __LEVELCONTAINERS_H__
#define __LEVELCONTAINERS_H__

#include "Levels/Level.h"
#include "GameTypes.h"

class LevelContainers
{
public:

private:
	BlockContainer			_animableBlocks;
	BlockContainer			_bouyentBlocks;

	friend class Level;
};

#endif
