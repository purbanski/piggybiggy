#ifndef __GAMEEVENTS_H__
#define __GAMEEVENTS_H__

class GameEventsListener
{
public:
	virtual void GameCoinCollected( puBlock *block ) = 0;
	virtual void GameBlockExplode( b2Vec2 pos ) = 0;
	virtual void GameMoneyExplode( b2Vec2 pos ) = 0;
	virtual void GameCustomAction_ContactBegin( puBlock *aBlock, puBlock *bBlock, b2Contact *contact ) = 0;
	virtual void GameCustomAction_ContactEnd( puBlock *aBlock, puBlock *bBlock, b2Contact *contact ) = 0;
	virtual void GameThiefCaught( puBlock *policeman ) = 0;
	virtual void GameCoinStolen( puBlock *thief ) = 0;
	virtual void GamePiggyStolen( puBlock *piggyBank, puBlock *thief ) = 0;
	virtual void GameCoinSilverCollected( b2Vec2 pos ) = 0;
	virtual void GameOutOfBorder() = 0;
};

#endif
