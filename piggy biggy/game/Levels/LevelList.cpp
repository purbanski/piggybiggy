#include <sstream>

#include "LevelList.h"
#include "Game.h"
#include "GameConfig.h"
#include "Tools.h"
#include "unDebug.h"
#include "Levels/Pockets.h"

//----------------------------------------------------------------------------
LevelList::LevelList()
{
}
//----------------------------------------------------------------------------
LevelList::~LevelList()
{
	_list.clear();
}
//----------------------------------------------------------------------------
void LevelList::SelectLevel( CCObject* pSender )
{
	long levelEnum;

	CCMenuItem *menuItem;
	menuItem = (CCMenuItem*) pSender;

	levelEnum = (long) menuItem->getUserData();

#ifdef BUILD_EDITOR
	CCDirector::sharedDirector()->popScene();
#endif
	Game::Get()->CreateAndPlayLevel( (GameTypes::LevelEnum) levelEnum, Game::eLevelToLevel );
}
//----------------------------------------------------------------------------
bool LevelList::IsInList( GameTypes::LevelEnum level )
{
	LevelListType::iterator it;

	for ( it = _list.begin(); it != _list.end(); it++ )
	{
		if ( (*it) == level )
			return true;
	}

	return false;
}
//----------------------------------------------------------------------------
void LevelList::CreateList()
{
	_menu = CCMenu::menuWithItems( NULL, NULL );

	//int	levelNr = 1;
	stringstream ss;

	for ( LevelListType::iterator it = _list.begin(); it != _list.end(); it++ )
	{
		ss.str("");
		ss << GameGlobals::Get()->LevelNames()[ *it ];

		float xLabel = 60.0f;
		float yLabel = -10.0f;

		CCLabelBMFont	*label;
		label = CCLabelBMFont::labelWithString( ss.str().c_str(),  Config::LevelNameFont );
		label->setPosition( ccp( xLabel, yLabel ));

		CCLabelBMFont	*labelSelected;
		labelSelected = CCLabelBMFont::labelWithString( ss.str().c_str(),  Config::LevelNameFont );
		labelSelected->setPosition( ccp( xLabel, yLabel ));

		CCLabelBMFont	*labelDisabled;
		labelDisabled = CCLabelBMFont::labelWithString( ss.str().c_str(),  Config::LevelNameFont );
		labelDisabled->setPosition( ccp( xLabel, yLabel ));

		CCSprite *spriteNormal;
		CCSprite *spriteSelected;
		CCSprite *spriteDisabled;

		switch ( Game::Get()->GetGameStatus()->GetLevelStatus( *it ))
		{
		case GameStatus::eLevelStatus_LastOpen :
		case GameStatus::eLevelStatus_Locked :
			spriteNormal   = CCSprite::spriteWithFile(  "Images/Menus/SelectLevel/ButtonUndone.png" );
			spriteSelected = CCSprite::spriteWithFile(  "Images/Menus/SelectLevel/ButtonUndone.png" );
			spriteDisabled = CCSprite::spriteWithFile(  "Images/Menus/SelectLevel/ButtonUndone_Disabled.png" );
			break;

		case GameStatus::eLevelStatus_Done :
			spriteNormal   = CCSprite::spriteWithFile(  "Images/Menus/SelectLevel/ButtonDone.png" );
			spriteSelected = CCSprite::spriteWithFile(  "Images/Menus/SelectLevel/ButtonDone.png" );
			spriteDisabled = CCSprite::spriteWithFile(  "Images/Menus/SelectLevel/ButtonDone.png" );
			break;

		case GameStatus::eLevelStatus_Skipped :
			spriteNormal   = CCSprite::spriteWithFile(  "Images/Menus/SelectLevel/ButtonSkipped.png" );
			spriteSelected = CCSprite::spriteWithFile(  "Images/Menus/SelectLevel/ButtonSkipped.png" );
			spriteDisabled = CCSprite::spriteWithFile(  "Images/Menus/SelectLevel/ButtonSkipped.png" );
			break;

		default:
			unAssertMsg( LevelList, false, ("Unknown level status %d", Game::Get()->GetGameStatus()->GetLevelStatus( *it )));
			spriteNormal	= NULL;
			spriteSelected  = NULL;
			spriteDisabled  = NULL;
			continue;

		}
		spriteNormal->addChild( label );
		spriteSelected->addChild( labelSelected );
		spriteDisabled->addChild( labelDisabled );


		CCMenuItemSprite *menuItem;

		menuItem = CCMenuItemSprite::itemFromNormalSprite( spriteNormal, spriteSelected, spriteDisabled, this, menu_selector( LevelList::SelectLevel ));
		menuItem->setUserData( (void *) (*it));
		if ( Game::Get()->GetGameStatus()->GetLevelStatus( *it ) == GameStatus::eLevelStatus_Locked )
			menuItem->setIsEnabled( false );

		_menu->addChild( menuItem, 5 );
	}

	_menu->alignItemsHorizontallyWithPadding( 30.0f );
	_menu->setPosition( ccp( 0.0f, 0.0f ));
	addChild( _menu );

	CCLabelBMFont *levelName;
	levelName = CCLabelBMFont::labelWithString( GetName().c_str(),  Config::LevelNameFont );
	levelName->setPosition( ccp( -120.0f, 50.0f ));
	addChild( levelName );
}
//----------------------------------------------------------------------------
//PocketStats LevelList::GetListStats()

//----------------------------------------------------------------------------




//----------------------------------------------------------------------------
// LevelList_Intro
//----------------------------------------------------------------------------
LevelList_Intro::LevelList_Intro()
{
	_list.push_back( GameTypes::eLevelIntro_Welcome );
	_list.push_back( GameTypes::eLevelIntro_DestroyBlock );
	_list.push_back( GameTypes::eLevelIntro_Gun );
	_list.push_back( GameTypes::eLevelIntro_Rope );
	_list.push_back( GameTypes::eLevelIntro_MoneyMakeMoney );

	CreateList();
}



//----------------------------------------------------------------------------
// LevelList_Roll
//----------------------------------------------------------------------------
LevelList_Roll::LevelList_Roll()
{
	_list.push_back( GameTypes::eLevelRoll_1 );
	_list.push_back( GameTypes::eLevelRoll_2 );
	//_list.push_back( GameTypes::eLevelRoll_3 );
	_list.push_back( GameTypes::eLevelRotateL );

	CreateList();
}



//----------------------------------------------------------------------------
// LevelList_Swing
//----------------------------------------------------------------------------
LevelList_Swing::LevelList_Swing()
{
	_list.push_back( GameTypes::eLevelSwing_1 );
	_list.push_back( GameTypes::eLevelSwing_2 );
	_list.push_back( GameTypes::eLevelSwing_Impossible );

	CreateList();
}




//----------------------------------------------------------------------------
// LevelList_Other
//----------------------------------------------------------------------------
LevelList_Other::LevelList_Other()
{
	_list.push_back( GameTypes::eLevelTraps_1 );
	_list.push_back( GameTypes::eLevelTraps_2 );
	_list.push_back( GameTypes::eLevelScale_1 );

	CreateList();
}



//----------------------------------------------------------------------------
// LevelList_Tetris
//----------------------------------------------------------------------------
LevelList_Tetris::LevelList_Tetris()
{
	_list.push_back( GameTypes::eLevelTetris_1 );
	_list.push_back( GameTypes::eLevelTetris_2 );

	CreateList();
}



//----------------------------------------------------------------------------
// LevelList_Domino
//----------------------------------------------------------------------------
LevelList_Domino::LevelList_Domino()
{
	_list.push_back( GameTypes::eLevelDomino_1 );

	CreateList();
}



//----------------------------------------------------------------------------
// LevelList_Construction
//----------------------------------------------------------------------------
LevelList_Construction_1::LevelList_Construction_1()
{
	_list.push_back( GameTypes::eLevelConstruction_1a );
	_list.push_back( GameTypes::eLevelConstruction_1b );
	_list.push_back( GameTypes::eLevelConstruction_1c );
	_list.push_back( GameTypes::eLevelConstruction_1d );

	_list.push_back( GameTypes::eLevelConstruction_2a );

	CreateList();
}



//----------------------------------------------------------------------------
// LevelList_Construction 2
//----------------------------------------------------------------------------
LevelList_Construction_2::LevelList_Construction_2()
{
	_list.push_back( GameTypes::eLevelConstruction_3a );
	_list.push_back( GameTypes::eLevelConstruction_4a );
	_list.push_back( GameTypes::eLevelConstruction_4b );

	CreateList();
}



//----------------------------------------------------------------------------
// LevelList_Gun
//----------------------------------------------------------------------------
LevelList_Gun::LevelList_Gun()
{
	_list.push_back( GameTypes::eLevelGun_1 );
	_list.push_back( GameTypes::eLevelGun_2 );
	_list.push_back( GameTypes::eLevelGun_3 );
	_list.push_back( GameTypes::eLevelGun_4 );

	CreateList();
}



//----------------------------------------------------------------------------
// LevelList_Reel
//----------------------------------------------------------------------------
LevelList_Reel::LevelList_Reel()
{
	_list.push_back( GameTypes::eLevelReel_1 );
	_list.push_back( GameTypes::eLevelReel_2 );
	_list.push_back( GameTypes::eLevelReel_3 );

	CreateList();
}



//----------------------------------------------------------------------------
// LevelList_Chain1
//----------------------------------------------------------------------------
LevelList_Chain1::LevelList_Chain1()
{
	_list.push_back( GameTypes::eLevelChain_1 );
	_list.push_back( GameTypes::eLevelChain_2a );
	_list.push_back( GameTypes::eLevelChain_3 );
	_list.push_back( GameTypes::eLevelChain_4 );
	_list.push_back( GameTypes::eLevelChain_5 );
	_list.push_back( GameTypes::eLevelChain_6 );

	CreateList();
}



//----------------------------------------------------------------------------
// LevelList_Chain2
//----------------------------------------------------------------------------
LevelList_Chain2::LevelList_Chain2()
{
	_list.push_back( GameTypes::eLevelChain_7a );
	CreateList();
}



//----------------------------------------------------------------------------
// LevelList_Bank
//----------------------------------------------------------------------------
LevelList_Bank::LevelList_Bank()
{
	_list.push_back( GameTypes::eLevelBank_1 );
	_list.push_back( GameTypes::eLevelBank_2 );
	_list.push_back( GameTypes::eLevelBank_3 );
	_list.push_back( GameTypes::eLevelBank_Impossible );
	_list.push_back( GameTypes::eLevelBank_Impossible_bak );

	CreateList();
}



//----------------------------------------------------------------------------
// LevelList_Rotate1
//----------------------------------------------------------------------------
LevelList_Rotate1::LevelList_Rotate1()
{
	_list.push_back( GameTypes::eLevelRotate_1 );
	_list.push_back( GameTypes::eLevelRotate_2 );
	_list.push_back( GameTypes::eLevelRotate_3 );
	_list.push_back( GameTypes::eLevelRotate_4 );
	_list.push_back( GameTypes::eLevelRotate_5 );
	_list.push_back( GameTypes::eLevelRotate_6 );

	CreateList();
}


//----------------------------------------------------------------------------
// LevelList_Rotate2
//----------------------------------------------------------------------------
LevelList_Rotate2::LevelList_Rotate2()
{
	_list.push_back( GameTypes::eLevelRotate_7 );

	CreateList();
}


//----------------------------------------------------------------------------
// LevelList_Labyrinth
//----------------------------------------------------------------------------
LevelList_Labyrinth::LevelList_Labyrinth()
{
	_list.push_back( GameTypes::eLevelLabyrinth_1 );
	_list.push_back( GameTypes::eLevelLabyrinth_Imposible );

	CreateList();
}



//----------------------------------------------------------------------------
// LevelList_Vehical
//----------------------------------------------------------------------------
LevelList_Vehical::LevelList_Vehical()
{
	_list.push_back( GameTypes::eLevelVehical_1 );
	_list.push_back( GameTypes::eLevelVehical_2 );
	_list.push_back( GameTypes::eLevelVehical_3 );

	CreateList();
}



//----------------------------------------------------------------------------
// LevelList_Bombs1
//----------------------------------------------------------------------------
LevelList_Bombs1::LevelList_Bombs1()
{
	_list.push_back( GameTypes::eLevelBomb_1 );
	_list.push_back( GameTypes::eLevelBomb_1b );
	_list.push_back( GameTypes::eLevelBomb_2 );
	_list.push_back( GameTypes::eLevelBomb_3 );
	_list.push_back( GameTypes::eLevelBomb_4 );

	CreateList();
}



//----------------------------------------------------------------------------
// LevelList_Bombs2
//----------------------------------------------------------------------------
LevelList_Bombs2::LevelList_Bombs2()
{
	_list.push_back( GameTypes::eLevelBomb_5 );
	_list.push_back( GameTypes::eLevelBomb_6 );
	_list.push_back( GameTypes::eLevelBomb_7 );
	_list.push_back( GameTypes::eLevelBomb_8 );

	CreateList();
}



//----------------------------------------------------------------------------
// LevelList_Others
//----------------------------------------------------------------------------
LevelList_Others::LevelList_Others()
{
	_list.push_back( GameTypes::eLevelOthers_1 );
	_list.push_back( GameTypes::eLevelOthers_2 );
	_list.push_back( GameTypes::eLevelOthers_3 );
	_list.push_back( GameTypes::eLevelOthers_4 );

	CreateList();
}



//----------------------------------------------------------------------------
// LevelList_Magnets
//----------------------------------------------------------------------------
LevelList_Magnets::LevelList_Magnets()
{
	_list.push_back( GameTypes::eLevelMagnet_1 );

	CreateList();
}



//----------------------------------------------------------------------------
// LevelList_Synchronize
//----------------------------------------------------------------------------
LevelList_Synchronize::LevelList_Synchronize()
{
	_list.push_back( GameTypes::eLevelSyncronize_1 );
	_list.push_back( GameTypes::eLevelSyncronize_2 );
	_list.push_back( GameTypes::eLevelSyncronize_3 );

	CreateList();
}



//----------------------------------------------------------------------------
// LevelList_Rafal1
//----------------------------------------------------------------------------
LevelList_Rafal1::LevelList_Rafal1()
{
	_list.push_back( GameTypes::eLevelScale_2 );
	_list.push_back( GameTypes::eLevelScale_3 );
	_list.push_back( GameTypes::eLevelPathfinder_1 );
	_list.push_back( GameTypes::eLevelBombAndRoll_1 );
	_list.push_back( GameTypes::eLevelExplosivePath );
	_list.push_back( GameTypes::eLevelBouncingCoin_1 );

	CreateList();
}



//----------------------------------------------------------------------------
// LevelList_Rafal2
//----------------------------------------------------------------------------
LevelList_Rafal2::LevelList_Rafal2()
{
	_list.push_back( GameTypes::eLevelTunnel_1 );
	_list.push_back( GameTypes::eLevelSpiderWeb );

	CreateList();
}



//----------------------------------------------------------------------------
// LevelList_Pawel1
//----------------------------------------------------------------------------
LevelList_Pawel1::LevelList_Pawel1()
{
	_list.push_back( GameTypes::eLevelPawel_1 );
	_list.push_back( GameTypes::eLevelPawel_2 );
	_list.push_back( GameTypes::eLevelPawel_3 );
	_list.push_back( GameTypes::eLevelPawel_4 );
	_list.push_back( GameTypes::eLevelPawel_5 );

	CreateList();
}



//----------------------------------------------------------------------------
// LevelList_Pawel2
//----------------------------------------------------------------------------
LevelList_Pawel2::LevelList_Pawel2()
{
	_list.push_back( GameTypes::eLevelPawel_6 );
	_list.push_back( GameTypes::eLevelPawel_7 );
	_list.push_back( GameTypes::eLevelPawel_8 );
	_list.push_back( GameTypes::eLevelPawel_9 );
	_list.push_back( GameTypes::eLevelPawel_10 );

	CreateList();
}



//----------------------------------------------------------------------------
// LevelList_Pawel3
//----------------------------------------------------------------------------
LevelList_Pawel3::LevelList_Pawel3()
{
	_list.push_back( GameTypes::eLevelPawel_11 );
	_list.push_back( GameTypes::eLevelPawel_12 );
	_list.push_back( GameTypes::eLevelPawel_13 );
	_list.push_back( GameTypes::eLevelPawel_14 );
	_list.push_back( GameTypes::eLevelPawel_15 );
	_list.push_back( GameTypes::eLevelPawel_16 );

	CreateList();
}



//----------------------------------------------------------------------------
// LevelList_Test
//----------------------------------------------------------------------------
LevelList_Test::LevelList_Test()
{

	_list.push_back( GameTypes::eLevelTestUser );
	_list.push_back( GameTypes::eLevelKonkurs );
	_list.push_back( GameTypes::eLevelTestAquarium );
	_list.push_back( GameTypes::eLevelTestSea );

	CreateList();
}



//----------------------------------------------------------------------------
// LevelList_Bogusia
//----------------------------------------------------------------------------
LevelList_Bogusia::LevelList_Bogusia()
{
	_list.push_back( GameTypes::eLevelBogusia_GoUnder );
	_list.push_back( GameTypes::eLevelBogusia_2 );
	_list.push_back( GameTypes::eLevelBogusia_3 );
	_list.push_back( GameTypes::eLevelBogusia_4 );

	CreateList();
}




//----------------------------------------------------------------------------
// LevelList_TomekW
//----------------------------------------------------------------------------
LevelList_TomekW::LevelList_TomekW()
{
	_list.push_back( GameTypes::eLevelTomek_1 );
	_list.push_back( GameTypes::eLevelTomek_2 );
	//_list.push_back( GameTypes::eLevelTomek_3 );
	//_list.push_back( GameTypes::eLevelTomek_4 );

	CreateList();
}


//----------------------------------------------------------------------------
// LevelList_TomekW
//----------------------------------------------------------------------------
LevelList_JacekZ::LevelList_JacekZ()
{
	_list.push_back( GameTypes::eLevelJacekZ_1 );

	CreateList();
}



//----------------------------------------------------------------------------
// LevelList_Marzena
//----------------------------------------------------------------------------
LevelList_Marzena::LevelList_Marzena()
{
	_list.push_back( GameTypes::eLevelMarzena_1 );

	CreateList();
}
