#ifndef __CONTACTSOLVER_H__
#define __CONTACTSOLVER_H__

#include <Box2D/Box2D.h>
#include <map>
#include <vector>
#include <set>
#include "Blocks/puBlock.h"
#include "Blocks/puCircle.h"
#include "Blocks/puAirBubble.h"
#include "Blocks/Switch/puSwitchBase.h"
#include "GameEvents.h"
#include "CommonDefs.h"


using namespace std;
//------------------------------------------------------------------------------------------------
class RuleFuncSettings
{
public:
	RuleFuncSettings ( GameEventsListener *recv, b2Contact *contact, puBlock *block1, puBlock *block2 ) 
		:	_recv( recv), _contact( contact ), _block1( block1 ), _block2( block2 ) {};

	GameEventsListener	*_recv;
	b2Contact			*_contact;
	puBlock				*_block1;
	puBlock				*_block2;
};
//------------------------------------------------------------------------------------------------
class ContactSolver : public b2ContactListener
{
public:
	ContactSolver();
	~ContactSolver();

	void Process();
	void SetRecv( GameEventsListener *recv ) { _recv = recv; }

	// Contact Solver
	virtual void BeginContact( b2Contact* contact );
	virtual void EndContact(b2Contact* contact);
	virtual void PreSolve( b2Contact* contact, const b2Manifold* oldManifold );

	void Quiting() { _isQuiting = true; }

private:
	typedef void ( *RuleFnPtr )( RuleFuncSettings * );
	std::map< int, RuleFnPtr > _rules;
	
	GameEventsListener	*_recv;


	// static because of static callbacks
	struct  AirBubblePair
	{
		AirBubblePair( puAirBubble_ *airBubble, puBlock *block )
		{
			_bubble = airBubble;
			_block = block;
		}

		puAirBubble_	*_bubble;
		puBlock			*_block;
	} ;

	//------------------------------------------
	// Air bubble collision container
	typedef vector<AirBubblePair> AirBubbleCollisions;
	static AirBubbleCollisions	_airBubbleCollisions;

	typedef set<puBlock*>		BlockSet;
	static BlockSet				_deadBlocks;
	static BlockSet				_glassCollisions;


	struct Rules
	{
		static void PiggyBank_Thief( RuleFuncSettings *param );
		static void Coin_Thief( RuleFuncSettings *param );
		static void CoinSilver_Coin( RuleFuncSettings *param );
		static void PiggyBank_Bomb( RuleFuncSettings *param );
		static void Coin_Bomb( RuleFuncSettings *param );
		static void Coin_PiggyBank( RuleFuncSettings *param );
		static void CoinSilver_PiggyBank( RuleFuncSettings *param );
		static void Cop_Thief( RuleFuncSettings *param );
		static void Swat_Thief( RuleFuncSettings *param );
		static void BulletExplosive_Other( RuleFuncSettings *param );
		static void BulletExplosive_Money( RuleFuncSettings *param );
		static void BlockRange_All_ContactBegin( RuleFuncSettings *param );
		static void BlockRange_All_ContactEnd( RuleFuncSettings *param );
		static void AirBubble_Others( RuleFuncSettings *param );
		static void AirBubble_AirBubble( RuleFuncSettings *param );
		static void Out_Of_Border( RuleFuncSettings *param );
		static void Custom_All_ContactBegin( RuleFuncSettings *param );
		static void Custom_All_ContactEnd( RuleFuncSettings *param );
		static void Glass_Others( RuleFuncSettings *param );
	};

	bool	_isQuiting;
};
//------------------------------------------------------------------------------------------------
#endif
