#include "unTypes.h"
#include "Pockets.h"
#include "GameConfig.h"
#include <sstream>
#include "Game.h"
#include "Tools.h"
#include "Platform/Lang.h"

//------------------------------------------------------------------------------
// Pocket Stats
//------------------------------------------------------------------------------
PocketStats::PocketStats()
{
	_levelsCompletedCount = 0;
	_levelsTotalCount = 0;
	_levelsLockCount = 0;
	_levelsSkipCount = 0;

	_totalTime = 0;
}


//------------------------------------------------------------------------------
// Pocket Manager
//------------------------------------------------------------------------------
PocketManager* PocketManager::_sInstance = NULL;
PocketManager* PocketManager::Get()
{
	if ( ! _sInstance )
	{
		_sInstance = new PocketManager();
		_sInstance->autorelease();
		_sInstance->retain();
	}
	return _sInstance;
}
//------------------------------------------------------------------------------
PocketManager::PocketManager()
{
	SetPocketsOrder();
	
	SetBluePocket();
	SetMoroPocket();
	SetRastaPocket();
	SetJapanPocket();
	SetSciencePocket();
}
//------------------------------------------------------------------------------
PocketManager::~PocketManager()
{
	_pocketList.clear();
	_pocketSpritesMap.clear();

	for ( PocketToLevelMap::iterator it = _pocketToLevelMap.begin(); it != _pocketToLevelMap.end(); it++ )
		it->second.clear();
    
	_pocketToLevelMap.clear();;
}
//------------------------------------------------------------------------------
CCSprite* PocketManager::GetSpritePocketUnlockAnim( GameTypes::PocketType pocket )
{
	return CCSprite::spriteWithFile( _pocketSpritesMap[ pocket ]._pocketSpriteUnlockAnim.c_str() );
}
//------------------------------------------------------------------------------
CCSprite* PocketManager::GetSpritePocketSelect( GameTypes::PocketType pocket )
{		
	return CCSprite::spriteWithFile( _pocketSpritesMap[ pocket ]._pocketSpriteSelectMenu.c_str() );		
}
//------------------------------------------------------------------------------
CCSprite* PocketManager::GetSpritePocketSelectLocked( GameTypes::PocketType pocket )
{		
	CCSprite *sprite;
	CCSprite *padlockSprite;

	sprite = GetSpritePocketSelect( pocket );
	padlockSprite = CCSprite::spriteWithFile( "Images/Pockets/PadlockMenu.png" );

	padlockSprite->setPosition( CCPoint( sprite->getContentSizeInPixels().width / 2.0f, sprite->getContentSizeInPixels().height / 2.0f ));
	sprite->addChild( padlockSprite );

	return sprite;		
}
//------------------------------------------------------------------------------
CCSprite* PocketManager::GetSelectPocketBackground( GameTypes::PocketType pocket )
{
	if ( pocket == GameTypes::ePocketNone )
	{
		unAssertMsg(PocketManager, false, ("Pocket None -> changing to Pocket Blue"));
		return CCSprite::spriteWithFile( _pocketSpritesMap[ GameTypes::ePocketBlue ]._selectPocketBg.c_str() );
	}

	return CCSprite::spriteWithFile( _pocketSpritesMap[ pocket ]._selectPocketBg.c_str() );
}
//------------------------------------------------------------------------------
CCSprite* PocketManager::GetPocketAnimBackground( GameTypes::PocketType pocket )
{
	if ( pocket == GameTypes::ePocketNone )
	{
		unAssertMsg(PocketManager, false, ("Pocket None -> changing to Pocket Blue"));
		return CCSprite::spriteWithFile( _pocketSpritesMap[ GameTypes::ePocketBlue ]._pocketAnimBg.c_str() );
	}

    D_LOG("%s", _pocketSpritesMap[ pocket ]._pocketAnimBg.c_str() );
    
	return CCSprite::spriteWithFile( _pocketSpritesMap[ pocket ]._pocketAnimBg.c_str() );
}
//------------------------------------------------------------------------------
void PocketManager::SetPocketsOrder()
{
	_pocketList.push_back( GameTypes::ePocketBlue );
	_pocketList.push_back( GameTypes::ePocketMoro );
	_pocketList.push_back( GameTypes::ePocketRasta );
	_pocketList.push_back( GameTypes::ePocketJapan );
	_pocketList.push_back( GameTypes::ePocketScience );

}
//------------------------------------------------------------------------------
void PocketManager::SetBluePocket()
{
	PocketLevels levels;
// levels.push_back( GameTypes::eLevelRotateL );

	levels.push_back( GameTypes::eLevelIntro_DestroyBlock );
	levels.push_back( GameTypes::eLevelConstruction_1a );
	levels.push_back( GameTypes::eLevelBogusia_GoUnder );
	levels.push_back( GameTypes::eLevelIntro_Gun );
	levels.push_back( GameTypes::eLevelRotate_1 );
	levels.push_back( GameTypes::eLevelExplosivePath );
    levels.push_back( GameTypes::eLevelPawel_1 );
    levels.push_back( GameTypes::eLevelReel_1 );
	levels.push_back( GameTypes::eLevelScale_2 );
    levels.push_back( GameTypes::eLevelRotateL );
	levels.push_back( GameTypes::eLevelIntro_Rope );
	levels.push_back( GameTypes::eLevelChain_7a );
	levels.push_back( GameTypes::eLevelBomb_3 );
	levels.push_back( GameTypes::eLevelGun_3 );
	levels.push_back( GameTypes::eLevelChain_2a );
	levels.push_back( GameTypes::eLevelRoll_1 );
    levels.push_back( GameTypes::eLevelRotate_4 );
   	levels.push_back( GameTypes::eLevelSyncronize_1 ); 
   	levels.push_back( GameTypes::eLevelLukasz_1a );
	levels.push_back( GameTypes::eLevelVehical_2 );
	levels.push_back( GameTypes::eLevelBomb_2 );
	levels.push_back( GameTypes::eLevelBomb_1b );
	levels.push_back( GameTypes::eLevelOthers_1 );
	levels.push_back( GameTypes::eLevelBank_1 );
    
	levels.unique();
	
	_pocketToLevelMap[ GameTypes::ePocketBlue ] = levels;
	
	//-----------------------------------------------------

	PocketSpritesNames sprites;
	sprites._pocketSpriteUnlockAnim.append	( "Images/Pockets/JeansPocketMenu.png" );
	sprites._pocketSpriteSelectMenu.append	( "Images/Pockets/JeansPocketMenu.png" );
	sprites._selectPocketBg.append			( "Skins/BluePocket/Images/Levels/Backgrounds/horizontal.jpg" );
	sprites._pocketAnimBg.append			( "Skins/BluePocket/Images/Levels/Backgrounds/pocketAnim.jpg" );

	_pocketSpritesMap[ GameTypes::ePocketBlue ] = sprites; 
}
//------------------------------------------------------------------------------
void PocketManager::SetMoroPocket()
{
	PocketLevels levels;

	levels.push_back( GameTypes::eLevelConstruction_3a );
	levels.push_back( GameTypes::eLevelBogusia_2 );
	levels.push_back( GameTypes::eLevelRotate_2 );
	levels.push_back( GameTypes::eLevelChain_3 );
	levels.push_back( GameTypes::eLevelGun_2 );
	levels.push_back( GameTypes::eLevelScale_1 ); //ssypie
	levels.push_back( GameTypes::eLevelPawel_12 );
	levels.push_back( GameTypes::eLevelOthers_2 );
	levels.push_back( GameTypes::eLevelOthers_6 );
	levels.push_back( GameTypes::eLevelTomek_1 );
	levels.push_back( GameTypes::eLevelBank_2 );
	levels.push_back( GameTypes::eLevelBogusia_4 );
	levels.push_back( GameTypes::eLevelSwing_1 );
	levels.push_back( GameTypes::eLevelSwing_2 );
	levels.push_back( GameTypes::eLevelTetris_2 );
	levels.push_back( GameTypes::eLevelVehical_3 );
	levels.push_back( GameTypes::eLevelPawel_15 );
	levels.push_back( GameTypes::eLevelRotate_7 );
	
	levels.unique();

	_pocketToLevelMap[ GameTypes::ePocketMoro ] = levels;

	//--------------------------------------------		

	PocketSpritesNames sprites;
	sprites._pocketSpriteUnlockAnim.append	( "Images/Pockets/MoroPocketMenu.png" );
	sprites._pocketSpriteSelectMenu.append	( "Images/Pockets/MoroPocketMenu.png" );
	sprites._selectPocketBg.append			( "Skins/MoroPocket/Images/Levels/Backgrounds/horizontal.jpg" );
   	sprites._pocketAnimBg.append			( "Skins/MoroPocket/Images/Levels/Backgrounds/pocketAnim.jpg" );

	_pocketSpritesMap[ GameTypes::ePocketMoro ] = sprites; 
}
//------------------------------------------------------------------------------
void PocketManager::SetRastaPocket()
{
	PocketLevels levels;
		
	levels.push_back( GameTypes::eLevelScale_3 );
	levels.push_back( GameTypes::eLevelMagnet_1 );
	levels.push_back( GameTypes::eLevelPawel_3 );
	levels.push_back( GameTypes::eLevelIntro_MoneyMakeMoney );
	levels.push_back( GameTypes::eLevelLukasz_2 );
	levels.push_back( GameTypes::eLevelRotate_5 );
	levels.push_back( GameTypes::eLevelTraps_2 );
	levels.push_back( GameTypes::eLevelChain_5 );
	levels.push_back( GameTypes::eLevelConstruction_1d );
	levels.push_back( GameTypes::eLevelOthers_5 );
	levels.push_back( GameTypes::eLevelReel_2 );
	levels.push_back( GameTypes::eLevelDomino_1 );
	levels.push_back( GameTypes::eLevelTomek_CalmWood );
	levels.push_back( GameTypes::eLevelChain_2b );
	levels.push_back( GameTypes::eLevelBombAndRoll_1 );
    levels.push_back( GameTypes::eLevelTraps_1 );
	levels.push_back( GameTypes::eLevelPawel_10 );
	levels.push_back( GameTypes::eLevelBank_3 );
    
	levels.unique();

	_pocketToLevelMap[ GameTypes::ePocketRasta ] = levels;

	//-----------------------------------------------------

	PocketSpritesNames sprites;
	sprites._pocketSpriteUnlockAnim.append	( "Images/Pockets/RastaPocketMenu.png" );
	sprites._pocketSpriteSelectMenu.append	( "Images/Pockets/RastaPocketMenu.png" );
	sprites._selectPocketBg.append			( "Skins/RastaPocket/Images/Levels/Backgrounds/horizontal.jpg" );
	sprites._pocketAnimBg.append			( "Skins/RastaPocket/Images/Levels/Backgrounds/pocketAnim.jpg" );
    
	_pocketSpritesMap[ GameTypes::ePocketRasta ] = sprites; 
}
//------------------------------------------------------------------------------
void PocketManager::SetJapanPocket()
{
	PocketLevels levels;

	levels.push_back( GameTypes::eLevelPawel_8 );
	levels.push_back( GameTypes::eLevelChain_7b );
	levels.push_back( GameTypes::eLevelSyncronize_2 );
	levels.push_back( GameTypes::eLevelTraps_3 );
	levels.push_back( GameTypes::eLevelBomb_5 );
	levels.push_back( GameTypes::eLevelVehical_1 );
	levels.push_back( GameTypes::eLevelBomb_8 );
	levels.push_back( GameTypes::eLevelConstruction_1b );
	levels.push_back( GameTypes::eLevelConstruction_4c );
	levels.push_back( GameTypes::eLevelBomb_6 );
	levels.push_back( GameTypes::eLevelRotate_3 );
	levels.push_back( GameTypes::eLevelMagnet_2 );
	levels.push_back( GameTypes::eLevelPawel_5 );
	levels.push_back( GameTypes::eLevelTomek_SelfMadeCannon );
	levels.push_back( GameTypes::eLevelGun_1 );
	levels.push_back( GameTypes::eLevelPawel_7 );
	levels.push_back( GameTypes::eLevelConstruction_4b );
	levels.push_back( GameTypes::eLevelChain_4 );
	levels.push_back( GameTypes::eLevelAdamS_PlainLogic );
	levels.push_back( GameTypes::eLevelPawel_13 );
	levels.push_back( GameTypes::eLevelRotate_Circle );
	levels.push_back( GameTypes::eLevelChain_1 );
	levels.push_back( GameTypes::eLevelPawel_2 );
	levels.push_back( GameTypes::eLevelPawel_11 );

	levels.unique();

	_pocketToLevelMap[ GameTypes::ePocketJapan ] = levels;

	//-----------------------------------------------------

	PocketSpritesNames sprites;
	sprites._pocketSpriteUnlockAnim.append	( "Images/Pockets/JapanPocketMenu.png" );
	sprites._pocketSpriteSelectMenu.append	( "Images/Pockets/JapanPocketMenu.png" );
	sprites._selectPocketBg.append			( "Skins/JapanPocket/Images/Levels/Backgrounds/horizontal.jpg" );
   	sprites._pocketAnimBg.append			( "Skins/JapanPocket/Images/Levels/Backgrounds/pocketAnim.jpg" );

	_pocketSpritesMap[ GameTypes::ePocketJapan ] = sprites;
}
//----------------------------------------------------------------------------
void PocketManager::SetSciencePocket()
{
	PocketLevels levels;

	levels.push_back( GameTypes::eLevelLukasz_1b );
	levels.push_back( GameTypes::eLevelConstruction_2a );
	levels.push_back( GameTypes::eLevelSyncronize_3 );
	levels.push_back( GameTypes::eLevelBomb_4 );
	levels.push_back( GameTypes::eLevelBomb_7 );
	levels.push_back( GameTypes::eLevelOthers_7 );
	levels.push_back( GameTypes::eLevelRotate_6 );
	levels.push_back( GameTypes::eLevelChain_6 );
	levels.push_back( GameTypes::eLevelSwing_Impossible );
	levels.push_back( GameTypes::eLevelReel_3 );
	levels.push_back( GameTypes::eLevelChain_2c );
	levels.push_back( GameTypes::eLevelConstruction_4a );
	levels.push_back( GameTypes::eLevelPathfinder_1 );
	levels.push_back( GameTypes::eLevelGun_4 );
	levels.push_back( GameTypes::eLevelTetris_1 );
	levels.push_back( GameTypes::eLevelConstruction_1c );
	levels.push_back( GameTypes::eLevelLabyrinth_Imposible );
	levels.push_back( GameTypes::eLevelBank_Impossible );

	levels.unique();

	_pocketToLevelMap[ GameTypes::ePocketScience ] = levels;

	//-----------------------------------------------------

	PocketSpritesNames sprites;
	sprites._pocketSpriteUnlockAnim.append	( "Images/Pockets/SciencePocketMenu.png" );
	sprites._pocketSpriteSelectMenu.append	( "Images/Pockets/SciencePocketMenu.png" );
	sprites._selectPocketBg.append			( "Skins/SciencePocket/Images/Levels/Backgrounds/horizontal.jpg" );
   	sprites._pocketAnimBg.append			( "Skins/SciencePocket/Images/Levels/Backgrounds/pocketAnim.jpg" );

	_pocketSpritesMap[ GameTypes::ePocketScience ] = sprites;
}
//------------------------------------------------------------------------------
bool PocketManager::IsLevelInPocket( GameTypes::LevelEnum level, GameTypes::PocketType pocket )
{
	PocketLevels levels;
	levels = _pocketToLevelMap[ pocket ];

	for ( PocketLevels::iterator it = levels.begin(); it != levels.end(); it++ )
		if ( (*it) == level ) 
			return true;

	return false;
}
//------------------------------------------------------------------------------
bool PocketManager::IsLevelFirstInPocket(GameTypes::LevelEnum level, GameTypes::PocketType pocket )
{
	PocketLevels levels;
    int count;
    
	levels = _pocketToLevelMap[ pocket ];
    count = 0;
    
	for ( PocketLevels::iterator it = levels.begin(); it != levels.end(); it++ )
    {
        count++;
		if ( (*it) == level )
        {
            if ( count == 1 )
                return true;
            else
                return false;
        }
    }

	return false;
}
//------------------------------------------------------------------------------
PocketManager::PocketLevels PocketManager::GetLevelsFromPocket( GameTypes::PocketType pocket )
{
	return _pocketToLevelMap[pocket];
}
//------------------------------------------------------------------------------
CCNode* PocketManager::GetLevelListNode( GameTypes::PocketType pocket, SlidingNode *slidingNode )
{
	int count;
	int countY;
	float deltaX;
	float deltaY;
	int breakRow;
	int screenCount;

	float x;
	float y;

	SlidingNodeMenu *menu;
	CCMenuItem *item;

	count = 0;
	countY = 1;

	breakRow = 3;

	x = 0.0f;
	y = 0.0f;
	screenCount = 0;

	deltaY = 280.0f;
	deltaX = 300.0f;

	menu = SlidingNodeMenu::menuWithItems( kCCMenuTouchPriority, slidingNode, NULL, NULL );

	PocketManager::PocketLevels levels;
	PocketManager::PocketLevels::iterator it;

	levels = PocketManager::Get()->GetLevelsFromPocket( pocket );
	D_SIZE(levels)
	////for ( LevelListType::iterator it = _list.begin(); it != _list.end(); it++ )
	for ( it = levels.begin(); it != levels.end(); it++ )
	{
		item = GetMenuItem( *it );
		item->setPosition( ccp( x, y ));
		menu->addChild( item, 10 );

		if ( (count % breakRow ) == ( breakRow - 1 ))
		{
			if ( y == 0.0f )
			{
				y -= deltaY;
				x = (float) screenCount * Config::GameSize.width / 1.0f;
			}
			else
			{
				y = 0.0f;
				screenCount++;
				x = (float) screenCount * Config::GameSize.width / 1.0f;
			}
		}
		else
			x += deltaX;

		count++;
	}

	menu->setPosition( ccp( -310.0f, 185.0f ));
	return menu;
}
//------------------------------------------------------------------------------
CCMenuItem* PocketManager::GetMenuItem( GameTypes::LevelEnum level )
{
	stringstream levelName;
	GameStatus *status;

	status =  Game::Get()->GetGameStatus();
	levelName << GameGlobals::Get()->LevelNames()[ level ];

	float xLevelName;
	float yLevelName;

	TrippleSprite triSprite;


	switch ( Game::Get()->GetGameStatus()->GetLevelStatus( level ))
	{
	case GameStatus::eLevelStatus_LastOpen :
	{
		CCSprite *piggy			= CCSprite::spriteWithFile( "Images/Menus/SelectLevel/PiggyReady2Rock.png" );
		CCSprite *piggySelected	= CCSprite::spriteWithFile( "Images/Menus/SelectLevel/PiggyReady2Rock.png" );

		triSprite._normal	= CCSprite::spriteWithFile( "Images/Menus/SelectLevel/PocketBg.png" );
		triSprite._selected = CCSprite::spriteWithFile( "Images/Menus/SelectLevel/PocketBg.png" );
		triSprite._disable	= CCSprite::spriteWithFile( "Images/Menus/SelectLevel/PiggyReady2Rock.png" );
		
		piggy->setPositionX( triSprite._normal->getContentSizeInPixels().width / 2.0f );
		piggy->setPositionY( triSprite._normal->getContentSizeInPixels().height / 2.0f );
		piggySelected->setPosition( piggy->getPosition() );

		triSprite._normal->addChild( piggy, 5 );
		triSprite._selected->addChild( piggySelected, 5 );

		AnimerSimpleEffect::Get()->Anim_PiggyReadyToRock( piggy );
		AnimerSimpleEffect::Get()->Anim_PiggyReadyToRock( piggySelected );
	}
	break;

	case GameStatus::eLevelStatus_Locked :
	{
		triSprite._normal   = CCSprite::spriteWithFile( "Images/Menus/SelectLevel/PiggyUndone.png" );
		triSprite._selected = CCSprite::spriteWithFile( "Images/Menus/SelectLevel/PiggyUndone.png" );
		triSprite._disable  = CCSprite::spriteWithFile( "Images/Menus/SelectLevel/PiggyUndone.png" );
	}
	break;

	case GameStatus::eLevelStatus_Done :
	{
		triSprite = GetLevelCompletedSprite( level );
	}
	break;

	case GameStatus::eLevelStatus_Skipped :
	{
		triSprite = GetLevelSkippedSprite();
	}
	break;

	default:
		unAssertMsg( LevelList, false, ("Unknown level status %d", Game::Get()->GetGameStatus()->GetLevelStatus( level )));
	}

	//	label			= CCLabelTTF::labelWithString( levelName.str().c_str(),  Config::LevelNameFont, Config::LevelNameFontSize );
//		labelSelected	= CCLabelTTF::labelWithString( levelName.str().c_str(),  Config::LevelNameFont, Config::LevelNameFontSize );
	//	labelDisabled	= CCLabelTTF::labelWithString( levelName.str().c_str(),  Config::LevelNameFont, Config::LevelNameFontSize );

	CCNode* label			= GetLevelLabel( level );
	CCNode* labelSelected	= GetLevelLabel( level );
	CCNode *labelDisabled	= GetLevelLabel( level );
		
	xLevelName = triSprite._normal->getContentSizeInPixels().width / 2.0f + 2.5f;
	yLevelName = triSprite._normal->getContentSizeInPixels().height / 2.0f - 130.0f;

	label->setPosition( ccp( xLevelName, yLevelName ));
	labelSelected->setPosition( ccp( xLevelName, yLevelName ));
	labelDisabled->setPosition( ccp( xLevelName, yLevelName ));

	triSprite._normal->addChild( label, 30 );
	triSprite._selected->addChild( labelSelected, 30 );
	triSprite._disable->addChild( labelDisabled, 30 );


	FXMenuItemSprite *menuItem;
	menuItem = FXMenuItemSprite::itemFromNormalSprite( 
			triSprite._normal, 
			triSprite._selected, 
			triSprite._disable, 
			this, 
			menu_selector( PocketManager::SelectLevel ));

	menuItem->setUserData( (void *) level );
	if ( Game::Get()->GetGameStatus()->GetLevelStatus( level ) == GameStatus::eLevelStatus_Locked )
		menuItem->setIsEnabled( false );

	return menuItem;
}
//-----------------------------------------------------------------------------------




//-----------------------------------------------------------------------------------
CCNode* PocketManager::GetTimeLabel( GameTypes::LevelEnum level )
{
	GameStatus *status;
	string completeTime;

	status =  Game::Get()->GetGameStatus();

	//int days	= status->GetStatus()->_scoreTable[ level ].GetDaysCount() ;
	int hours	= status->GetStatus()->_scoreTable[ level ].GetHoursCount() ;
	int minutes	= status->GetStatus()->_scoreTable[ level ].GetMinutesCount() ;
	int seconds	= status->GetStatus()->_scoreTable[ level ].GetSecondsCount() ;

	stringstream ss;
	ss << hours ;

	ss << ":";

	if ( minutes < 10 )
		ss << 0;
	ss << minutes ;

	ss << ":";

	if ( seconds < 10 )
		ss << 0;
	ss << seconds ;


	CCLabelBMFont	*scoreLabel;
	scoreLabel = CCLabelBMFont::labelWithString( ss.str().c_str(), Config::SelectLevelScoreTime );
	scoreLabel->setPosition( ccp( 90.0f, 20.0f ));

	//CCSprite *stopper;
	//stopper = CCSprite::spriteWithFile( Skins::GetSkinName( "Images/Stopper/Stopper.png") );
	//stopper->setPosition( ccp( -70.0f, 0.0f ));

	CCNode *node;
	node = CCNode::node();
	node->addChild( scoreLabel );
	//node->addChild( stopper );

	return node;
}
//----------------------------------------------------------------------------
CCNode* PocketManager::GetLevelLabel( GameTypes::LevelEnum level )
{
	CCNode *node;
	
	string levelName;
	string strTemp1, strTemp2;
	//string::iterator strIt;

	levelName = GameGlobals::Get()->LevelNames()[ level ];
	node = CCNode::node();
    
    D_LOG("Name %s:", levelName.c_str() )
    
	int strIt = levelName.find_first_of("\n");
    if ( Lang::Get()->IsChinese() )
    {
        // chinese
        // ttf labels
        
        CCLabelTTF* label1 =  CCLabelTTF::labelWithString( levelName.c_str(), Config::LevelNamePlayFont, 46 );
//      CCLabelTTF* label1 =  CCLabelTTF::labelWithString( levelName.c_str(), Config::HansSFont, 36 );
        label1->setPosition( CCPoint( 0.0f, -7.5f ));
        node->addChild( label1 );
    }
    else if ( Lang::Get()->IsJapanese() )
    {
        CCLabelTTF* label1 =  CCLabelTTF::labelWithString( levelName.c_str(), Config::LevelNamePlayFont, 36 );
        label1->setPosition( CCPoint( 0.0f, -4.5f ));
        node->addChild( label1 );
    }
    else
    {
        // english, polish, etc
        // fnt labels
        
            CCLabelBMFont *label1 = NULL;
            CCLabelBMFont *label2 = NULL;

        if ( strIt > 0 )
        {
            strTemp1.append( levelName.substr( 0, strIt ));
            strTemp2.append( levelName.substr( strIt + 1 ));
        
            D_LOG("Level1: -%s-", strTemp1.c_str() )
            D_LOG("Level2: -%s-", strTemp2.c_str() )
            
            label1 = CCLabelBMFont::labelWithString( strTemp1.c_str(), Lang::Get()->GetLang( Config::LevelNameFont ));
            label2 = CCLabelBMFont::labelWithString( strTemp2.c_str(), Lang::Get()->GetLang( Config::LevelNameFont ));
		
            label1->setPosition( CCPoint( 0.0f, 0.0f ));
            label2->setPosition( CCPoint( 0.0f, -25.0f ));

            node->addChild( label1 );
            node->addChild( label2 );
        }
        else
        {
            CCLabelBMFont *label1;

            label1 = CCLabelBMFont::labelWithString( levelName.c_str(), Lang::Get()->GetLang( Config::LevelNameFont ));
            label1->setPosition( CCPoint( 0.0f, 0.0f ));
            node->addChild( label1 );
        }
        
//        GameStatus *status;
//        status = Game::Get()->GetGameStatus();
//        
//        if ( status->GetLevelStatus( level ) == GameStatus::eLevelStatus_Done || 
    }
    
    
	return node;
}
//----------------------------------------------------------------------------
CCNode* PocketManager::GetCoinsScore( GameTypes::LevelEnum level )
{
	GameStatus *status;
	status = Game::Get()->GetGameStatus();

	int cointCount;
	cointCount = status->GetCoinCount( level );

	float x, y, xDelta;

	x = 0.0f;
	y = 0.0f;
	xDelta = 23.5f;

	float dur;
	float delay;

	dur = 0.4f;
	delay = 0.4f;

	CCNode *node;
	CCSprite *coinSprite;

	node = CCNode::node();

	for ( int i = 0; i < cointCount; i++ )
	{
		coinSprite = CCSprite::spriteWithFile( Skins::GetSkinName( "Images/OtherPreload/ScoreCoinSelectLevel.png") );
		coinSprite->setPosition( ccp( x, y ));
		node->addChild( coinSprite, i + 10 );

		x += xDelta;
	}

	return node;
}
//----------------------------------------------------------------------------
void PocketManager::SelectLevel( CCObject* pSender )
{  
	long levelEnum;

	CCMenuItem *menuItem;
	menuItem = (CCMenuItem*) pSender;

	levelEnum = (long) menuItem->getUserData();

#ifdef BUILD_EDITOR
	CCDirector::sharedDirector()->popScene();
#endif
	Game::Get()->GetGameStatus()->ResetPreviouslyPlayedLevel();
	Game::Get()->CreateAndPlayLevel( (GameTypes::LevelEnum) levelEnum, Game::eLevelToLevel );
}
//----------------------------------------------------------------------------
PocketStats PocketManager::GetPocketStats( GameTypes::PocketType pocket )
{
	GameStatus::LevelStatus levelStatus;
	PocketLevels levels;
	PocketStats stats;
	GameStatus *status;

	levels = _pocketToLevelMap[ pocket ];
	stats._levelsTotalCount = levels.size();
	status = Game::Get()->GetGameStatus();
	
	for ( PocketLevels::iterator it = levels.begin(); it != levels.end(); it++ )
	{
		levelStatus = status->GetLevelStatus( *it );

		if ( levelStatus == GameStatus::eLevelStatus_Done )
			stats._levelsCompletedCount++;

		if ( levelStatus == GameStatus::eLevelStatus_Locked || levelStatus == GameStatus::eLevelStatus_LastOpen )
			stats._levelsLockCount++;

		if ( levelStatus == GameStatus::eLevelStatus_Skipped )
			stats._levelsSkipCount++;
	}

	return stats;
}
//----------------------------------------------------------------------------
PocketManager::PocketList PocketManager::GetPocketList()
{
	return _pocketList;
}
//----------------------------------------------------------------------------
GameTypes::PocketType PocketManager::LevelInWhichPocket( GameTypes::LevelEnum levelEnum )
{
	PocketList::iterator it;

	for ( it = _pocketList.begin(); it != _pocketList.end(); it++ )
	{
		if ( IsLevelInPocket( levelEnum, *it ))
			return *it;
	}

	return GameTypes::ePocketNone;
}
//----------------------------------------------------------------------------
TrippleSprite PocketManager::GetLevelSkippedSprite()
{
	TrippleSprite triSprite;
	CCSprite *piggySprite;
	CCSprite *skippedLabel;
	CCPoint piggyPos;
	CCPoint skippedLabelPos;
	
	//-------------------------
	// Normal sprite
	//-------------------------
	triSprite._normal	= CCSprite::spriteWithFile( "Images/Menus/SelectLevel/PocketBg.png" );
	piggySprite = CCSprite::spriteWithFile( "Images/Menus/SelectLevel/PiggySkipped.png" );
	skippedLabel = CCSprite::spriteWithFile( Lang::Get()->GetLang( "Images/Menus/SelectLevel/SkippedLabel.png" ));
	
	piggyPos.x = triSprite._normal->getContentSizeInPixels().width / 2.0f;
	piggyPos.y = triSprite._normal->getContentSizeInPixels().height / 2.0f;

	skippedLabelPos = piggyPos;
	skippedLabelPos.y -= 55.0f;

	piggySprite->setPosition( piggyPos );
	skippedLabel->setPosition( skippedLabelPos );

	triSprite._normal->addChild( piggySprite, 5 );
	triSprite._normal->addChild( skippedLabel, 10);


	//-------------------------
	// Selected sprite
	//-------------------------
	triSprite._selected	= CCSprite::spriteWithFile( "Images/Menus/SelectLevel/PocketBg.png" );
	piggySprite = CCSprite::spriteWithFile( "Images/Menus/SelectLevel/PiggySkipped.png" );
	skippedLabel = CCSprite::spriteWithFile( Lang::Get()->GetLang( "Images/Menus/SelectLevel/SkippedLabel.png" ));

	piggySprite->setPosition( piggyPos );
	skippedLabel->setPosition( skippedLabelPos );

	AnimerSimpleEffect::Get()->Anim_ButtonPulse( piggySprite );
	triSprite._selected->addChild( piggySprite, 5 );
	triSprite._selected->addChild( skippedLabel, 10);

	
	//-------------------------
	// Disable sprite
	//-------------------------
	triSprite._disable	= CCSprite::spriteWithFile( "Images/Other/none.png" );

	return triSprite;
}
//----------------------------------------------------------------------------
TrippleSprite PocketManager::GetLevelCompletedSprite( GameTypes::LevelEnum level )
{
	TrippleSprite triSprite;
	CCSprite *piggySprite;
	CCSprite *scoreBg;
	CCNode *completionTime;
	CCNode *coinsScore;
	CCPoint piggyPos;
	CCPoint scoreBgPos;
	CCPoint completionTimePos;
	CCPoint coinsScorePos;

	
	//-------------------------
	// Normal sprite
	//-------------------------
	triSprite._normal	= CCSprite::spriteWithFile( "Images/Menus/SelectLevel/PocketBg.png" );
	piggySprite = CCSprite::spriteWithFile( "Images/Menus/SelectLevel/PiggyCompleted.png" );
	scoreBg = CCSprite::spriteWithFile( "Images/Menus/SelectLevel/ScoreBg.png" );
	completionTime = GetTimeLabel( level );
	coinsScore = GetCoinsScore( level );

	piggyPos.x = triSprite._normal->getContentSizeInPixels().width / 2.0f + 20.0f;
	piggyPos.y = triSprite._normal->getContentSizeInPixels().height / 2.0f + 38.0f;

	scoreBgPos.x = triSprite._normal->getContentSizeInPixels().width / 2.0f + 5.0f;
	scoreBgPos.y = triSprite._normal->getContentSizeInPixels().height / 2.0f - 68.0f;

	completionTimePos.x = triSprite._normal->getContentSizeInPixels().width / 2.0f - 63.5f;
	completionTimePos.y = triSprite._normal->getContentSizeInPixels().height / 2.0f - 65.0f;

	coinsScorePos.x = triSprite._normal->getContentSizeInPixels().width / 2.0f - 42.25f;
	coinsScorePos.y = triSprite._normal->getContentSizeInPixels().height / 2.0f - 89.25f;

	piggySprite->setPosition( piggyPos );
	scoreBg->setPosition( scoreBgPos );
	completionTime->setPosition( completionTimePos );
	coinsScore->setPosition( coinsScorePos );

	triSprite._normal->addChild( scoreBg, 5 );
	triSprite._normal->addChild( piggySprite, 10 );
	triSprite._normal->addChild( completionTime, 20 );
	triSprite._normal->addChild( coinsScore, 20 );


	//-------------------------
	// Selected sprite
	//-------------------------
	triSprite._selected	= CCSprite::spriteWithFile( "Images/Menus/SelectLevel/PocketBg.png" );
	piggySprite = CCSprite::spriteWithFile( "Images/Menus/SelectLevel/PiggyCompleted.png" );
	scoreBg = CCSprite::spriteWithFile( "Images/Menus/SelectLevel/ScoreBg.png" );
	completionTime = GetTimeLabel( level );
	coinsScore = GetCoinsScore( level );

	piggySprite->setPosition( piggyPos );
	scoreBg->setPosition( scoreBgPos );
	completionTime->setPosition( completionTimePos );
	coinsScore->setPosition( coinsScorePos );

	AnimerSimpleEffect::Get()->Anim_ButtonPulse( piggySprite );
	triSprite._selected->addChild( scoreBg, 5 );
	triSprite._selected->addChild( piggySprite, 10 );
	triSprite._selected->addChild( completionTime, 20 );
	triSprite._selected->addChild( coinsScore, 20 );


	//-------------------------
	// Disable sprite
	//-------------------------
	triSprite._disable	= CCSprite::spriteWithFile( "Images/Other/none.png" );

	return triSprite;
}
//----------------------------------------------------------------------------
