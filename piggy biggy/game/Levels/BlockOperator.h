#ifndef __LEVELBLOCKOPERATOR_H__
#define __LEVELBLOCKOPERATOR_H__

#include <map>
#include <Box2d/Box2D.h>
#include "Levels/BlockFinder.h"

using namespace std;

//-------------------------------------------------------------------------------------
class LevelBlockOperator : public BlockFinderBase
{
public:
	LevelBlockOperator();
	~LevelBlockOperator();

	void MouseDown( CCTouch *touch );
	void MouseUp( CCTouch *touch );
	void MouseMove( CCTouch *touch );

	void RelaseAllMouseJoints();

private:
	class TouchInfo
	{
	public:
		TouchInfo()
		{
			_block = NULL;
			_mouseJoint = NULL;
			_touchPosition.SetZero();
		}

		puBlock			*_block;
		b2Vec2			_touchPosition;
		b2MouseJoint	*_mouseJoint;
	};


private:
	virtual void _doBlockTouch( puBlock *block, const b2Vec2 &pos, void *userData );
	virtual void _doEmptyTouch( const b2Vec2 &pos, void *userData );

	void OperateTouch( CCTouch *touch );
	void OperateMove( CCTouch *touch );
	void OperateFinish( CCTouch *touch );
	void SetUpMouseJoint( CCTouch *touch );
	void ProcessActiveBlocks( CCTouch * touch, puBlock * block );
	

    //	b2Vec2 TouchAdjust( CCTouch * touch ); // similar to tools::ConvertToScaled  - but we do not scale with Game // not with layer


private:
	typedef map<CCTouch*, TouchInfo>	TouchedBlockMap;
	typedef map<CCTouch*, puBlock *>	ActiveBlockMap;
	TouchedBlockMap	_touchedMap;
	ActiveBlockMap  _activeBlockMap;
};

#endif
