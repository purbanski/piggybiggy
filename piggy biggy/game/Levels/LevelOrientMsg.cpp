#include "LevelOrientMsg.h"
#include "GameConfig.h"
#include "Skins.h"
#include "GameTypes.h"
#include "Debug/MyDebug.h"
#include "unDebug.h"
#include "Platform/Lang.h"

float LevelOrientMsg::_sFadeDur = 0.5f;
float LevelOrientMsg::_sDisplayDuration = 5.0f;

//-------------------------------------------------------------------------
LevelOrientMsg* LevelOrientMsg::Create(  GameTypes::LevelOrientation orient )
{

	LevelOrientMsg* ret;
	ret = new LevelOrientMsg( orient );

	if ( ret )
	{
		ret->autorelease();
		ret->Init();
		return ret;
	}
	return NULL;
}
//-------------------------------------------------------------------------
LevelOrientMsg::LevelOrientMsg( GameTypes::LevelOrientation orient )
{

	_iphoneSprite = NULL;
    _holdThisWaySprite = NULL;
	_orientMode = orient;
	_timestampInit = -1.0f;
	_enabled = false;
	_enableInitialized = false;
	_initDelay = 0.0f;
	_state = eState_Hidden;
}
//-------------------------------------------------------------------------
void LevelOrientMsg::Init()
{

	switch ( _orientMode )
	{
	case GameTypes::eLevel_Horizontal :
            _holdThisWaySprite =  CCSprite::spriteWithFile( Lang::Get()->GetLang( "Images/TurniPhone/HoldThisWayLandscape.png" ));
            _iphoneSprite =  CCSprite::spriteWithFile( "Images/TurniPhone/iPhoneLandscape.png" );
            
            _holdThisWaySprite->setPosition( CCPoint( 0.0f, -130.0f ));
            _iphoneSprite->setPosition( CCPoint( 0.0f, 50.0f ));
		break;

	case GameTypes::eLevel_Vertical :
            _holdThisWaySprite =  CCSprite::spriteWithFile(  Lang::Get()->GetLang( "Images/TurniPhone/HoldThisWayPortait.png" ));
            _iphoneSprite =  CCSprite::spriteWithFile( "Images/TurniPhone/iPhonePortait.png" );
            
            _holdThisWaySprite->setPosition( CCPoint( 250.0f, 0.0f ));
            _iphoneSprite->setPosition( CCPoint( -50.0f, 0.0f ));
		break;

	default:
		unAssertMsg(Level, false, ("bad orient mode: %d", _orientMode));
	}

	_holdThisWaySprite->setOpacity( 0 );
    _iphoneSprite->setOpacity( 0 );
    
	addChild( _holdThisWaySprite, 20 );
    addChild( _iphoneSprite, 10 );
}
//-------------------------------------------------------------------------
LevelOrientMsg::~LevelOrientMsg()
{
	removeAllChildrenWithCleanup( true );
}
//-------------------------------------------------------------------------
void LevelOrientMsg::AcceratorNotify( CCAcceleration* acceleration )
{ 

	if ( ! _enabled )
		return;

	if ( _state != eState_Hidden && _state != eState_Shown )
		return;

	if ( _timestampInit == -1.0 )
		_timestampInit = acceleration->timestamp;

	
	//------------------------------
	// Disable after first 15seconds
	//------------------------------
	double duration;
	duration = acceleration->timestamp - _timestampInit;
	
	if ( duration >= _sDisplayDuration )
	{
		Disable();
		return;
	}

	//------------------------------
	// Update orientation sprite
	//------------------------------
	double x = acceleration->x ;
	double y = acceleration->y ;
	double range = 1.0f / abs(acceleration->z);

	if ( _orientMode == GameTypes::eLevel_Horizontal &&
		( abs( x/y ) >= range && abs(x) > 0.2f  ))
	{
		OrientSpriteAnimStart();
	}
	else if ( _orientMode == GameTypes::eLevel_Vertical &&
		( abs( y/x ) >= range && abs(y) > 0.2f  )) 
	{
		OrientSpriteAnimStart();
	}
	else
	{
		OrientSpriteAnimStop();
	}
}
//-------------------------------------------------------------------------
void LevelOrientMsg::OrientSpriteAnimStart()
{

	if ( _state != eState_Hidden )
		return;

	_state = eState_Showing;

	CCActionInterval *seq;

	seq = (CCActionInterval*)(CCSequence::actions( 
		CCFadeTo::actionWithDuration( _sFadeDur, 255 ),
		CCCallFuncND::actionWithTarget( this, callfuncND_selector( LevelOrientMsg::SetState ), (void*) eState_Shown ), 
		NULL) );

	_iphoneSprite->stopAllActions();
	_iphoneSprite->runAction( seq );
    
    _holdThisWaySprite->stopAllActions();
    _holdThisWaySprite->runAction( CCFadeTo::actionWithDuration( _sFadeDur, 255 ));
    
}
//-------------------------------------------------------------------------
void LevelOrientMsg::OrientSpriteAnimStop()
{
	if ( _state != eState_Shown )
		return;

	if ( ! _iphoneSprite->getOpacity() )
	{
		_state = eState_Hidden;
		return;
	}

	_state = eState_Hiding;

	float oneOpacPerdur;
	float opacityDelta;
	float durAdjust;
	float durAdjust2;
    
	CCActionInterval* seq;

	opacityDelta = _iphoneSprite->getOpacity();
	oneOpacPerdur = _sFadeDur / 255.0f;
    
	durAdjust = oneOpacPerdur * opacityDelta;
	durAdjust2 = oneOpacPerdur * _holdThisWaySprite->getOpacity();
    
	seq = (CCActionInterval*)(CCSequence::actions( 
		CCFadeTo::actionWithDuration( durAdjust, 0 ),
		CCCallFuncND::actionWithTarget( this, callfuncND_selector( LevelOrientMsg::SetState ), (void*) eState_Hidden ), 
		NULL));

	_iphoneSprite->stopAllActions();
	_iphoneSprite->runAction(seq );
    
    _holdThisWaySprite->stopAllActions();
    _holdThisWaySprite->runAction( CCFadeTo::actionWithDuration( durAdjust2, 0 ));
}
//-------------------------------------------------------------------------
void LevelOrientMsg::SetState( CCNode *sender, void *data )
{
	_state = (States)((long)(data));
}
//-------------------------------------------------------------------------
void LevelOrientMsg::OrientSpriteAnimStopImediate()
{
	if ( ! _iphoneSprite->getOpacity() )
	{
		_state = eState_Hidden;
		return;
	}

	_state = eState_Hiding;

	float oneOpacPerdur;
	float opacityDelta;
	float durAdjust;
  	float durAdjust2;
    
	CCActionInterval* seq;

	opacityDelta = _iphoneSprite->getOpacity();
	oneOpacPerdur = _sFadeDur / 255.0f;
	durAdjust = oneOpacPerdur * opacityDelta;
	durAdjust2 = oneOpacPerdur * _holdThisWaySprite->getOpacity();

	seq = (CCActionInterval*)(CCSequence::actions( 
		CCFadeTo::actionWithDuration( durAdjust, 0 ),
		CCCallFuncND::actionWithTarget( this, callfuncND_selector( LevelOrientMsg::SetState ), (void*) eState_Hidden ), 
		NULL));

	_iphoneSprite->stopAllActions();
	_iphoneSprite->runAction(seq );
    
   	_holdThisWaySprite->stopAllActions();
	_holdThisWaySprite->runAction( CCFadeTo::actionWithDuration( durAdjust2, 0 ));
}
//-------------------------------------------------------------------------
void LevelOrientMsg::Enable()
{

	if ( ! _enableInitialized  )
	{
		_enableInitialized = true;
		schedule( schedule_selector( LevelOrientMsg::Step ));
	}
	else
		_enabled = true;
}
//-------------------------------------------------------------------------
void LevelOrientMsg::Disable()
{

	OrientSpriteAnimStopImediate();
	_enabled = false;
}
//-------------------------------------------------------------------------
void LevelOrientMsg::Step( ccTime dTime )
{
	_initDelay += dTime;
	if ( _initDelay > 1.0f )
	{
		unschedule( schedule_selector( LevelOrientMsg::Step ));
		Enable();
	}
}
//-------------------------------------------------------------------------
