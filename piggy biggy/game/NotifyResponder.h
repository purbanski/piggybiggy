#ifndef __NOTIFYRESPONDER_H__
#define __NOTIFYRESPONDER_H__
//--------------------------------------------------------------
#include "s3eTypes.h"
//--------------------------------------------------------------
class NotifyResponder
{
public:
	static NotifyResponder* Get();
	static void Destroy();

	~NotifyResponder();

	static int32 NotifyMemoryWarning( void* systemData, void* userData );
	static int32 NotifyBecomeActive( void* systemData, void* userData );
	static int32 NotifyLostActive( void* systemData, void* userData );
	static int32 NotifyEnterBackground( void* systemData, void* userData );
	static int32 NotifyEnterForeground( void* systemData, void* userData );
	static int32 NotifyTerminate( void* systemData, void* userData );

	void TestAll();

private:
	NotifyResponder();
	
private:
	static NotifyResponder *_sInstance;
	static int _countMemWarn;
	static int _countActive;
	static int _countLostActive;
	static int _countEnterBackground;
	static int _countEnterForeground;


};
//--------------------------------------------------------------
#endif
