#include <sstream>
#include "GameConfig.h"
#include "Preloader.h"
#include "Tools.h"
#include "Skins.h"
#include "SoundEngine.h"
#include "Skins.h"
#include "Animation/AnimManager.h"
#include "Blocks/puBlock.h"
#include "Debug/DebugLogger.h"
#include "Debug/MyDebug.h"
#include "Platform/ToolBox.h"

//------------------------------------------------------------------------------------
Preloader	* Preloader::_sInstance = NULL;
//------------------------------------------------------------------------------------
Preloader* Preloader::Get()
{
	if ( ! _sInstance )
		_sInstance = new Preloader();
	return _sInstance;
}
//------------------------------------------------------------------------------------
void Preloader::Destroy()
{
	if ( _sInstance )
		delete _sInstance;
	_sInstance = NULL;
}
//------------------------------------------------------------------------------------
void Preloader::PreloadGraphicsInDir( const char *dir, bool skinned )
{
	Filenames files;
	Filenames::iterator file;

	string ext;
	string fileToLoad;

	if ( skinned ) 
		Tools::GetFilesInDir( Skins::GetSkinName( dir ), files );

	for ( file = files.begin(); file != files.end(); file++ )
	{
		ext = file->substr( file->length() - 3, 3 );
		if ( ! ext.compare( string("png")) )
		{
			fileToLoad.clear();
			fileToLoad.append( Skins::GetSkinName( dir ) );
			fileToLoad.append( file->c_str() );
			D_LOG( "Preloading %s" , fileToLoad.c_str() );

			CCSprite::spriteWithFile( fileToLoad.c_str() );
		}
	}
}
//------------------------------------------------------------------------------------
void Preloader::Preload()
{
#ifdef BUILD_EDITOR
	Reset();
	return;
#endif

    //--------------------------
    // Do not preload for shitty
    // devices
    if ( ToolBox::LowMemDevice( gDevice ))
    {
        Reset();
        return;
    }
    
	FilesSet::iterator itFile;
	SoundEngine::SoundSet::iterator itSound;

	//-----------
	// Image
	for ( itFile = _contImage.begin(); itFile != _contImage.end(); itFile++ )
	{
		CCSprite::spriteWithFile( itFile->c_str() );
		D_LOG("Loading image: %s", itFile->c_str() );
	}
	
	//-----------
	// Image Dir
	for ( itFile = _contDirImage.begin(); itFile != _contDirImage.end(); itFile++ )
	{
		PreloadGraphicsInDir( itFile->c_str(), false );
		D_LOG("Loading Dir: %s", itFile->c_str() );
	}

	
	//-----------
	// Fonts
	for ( itFile = _contFont.begin(); itFile != _contFont.end(); itFile++ )
	{
		CCLabelBMFont::labelWithString( "", itFile->c_str() );
		D_LOG("Logding Font: %s", itFile->c_str() );
	}
	
	//-----------
	// Sounds
	D_SIZE( _contSounds )
	for ( itSound = _contSounds.begin(); itSound != _contSounds.end(); itSound++ )
	{
		SoundEngine::Get()->LoadSound( *itSound );
		D_LOG("Loading sound: %d", *itSound );
	}

	//-----------
	// Anim
	//_contAnim.Dump();
	for ( AnimSizeSet::iterator it = _contAnim.begin(); it != _contAnim.end(); it++ )
	{
		for ( RadiusSet::iterator rit = it->second.begin(); rit != it->second.end(); rit++ )
		{
			D_LOG("Loading anim:%d radius:%f", it->first, *rit )
			AnimManager::Get()->PreloadAnim( it->first, *rit );
		}
	}

	//-----------
	// Anim names
	for ( itFile = _contAnimNames.begin(); itFile != _contAnimNames.end(); itFile++ )
	{
		D_LOG("Loading anim: %s", (*itFile).c_str() );
		AnimTools::Get()->GetAnimFrames( (*itFile).c_str() );
	}

	Reset();
}
//------------------------------------------------------------------------------------
void Preloader::AddSound( SoundEngine::Sound sound )
{
	_contSounds.insert( sound );
}
//------------------------------------------------------------------------------------
void Preloader::AddImage( const char *filename )
{
	_contImage.insert( filename );
}
//------------------------------------------------------------------------------------
void Preloader::AddSkinImage( const char *filename )
{
	_contImage.insert( Skins::GetSkinName( filename ));
}
//------------------------------------------------------------------------------------
void Preloader::AddAnim( GameTypes::CharactersAnim anim, puBlock *block )
{
	_contAnim.Add( anim, block->GetWidth() / 2.0f * RATIO );
}
//------------------------------------------------------------------------------------
void Preloader::AddAnim( GameTypes::CharactersAnim anim, float radius )
{
	_contAnim.Add( anim, radius );
}
//------------------------------------------------------------------------------------
void Preloader::AddAnim( const char *filename )
{
	_contAnimNames.insert( filename );
}
//------------------------------------------------------------------------------------
void Preloader::AddFont( const char *filename )
{
	_contFont.insert( filename );
}
//------------------------------------------------------------------------------------
void Preloader::Reset()
{
	_contFont.clear();
	_contImage.clear();
	_contAnim.Reset();
	_contAnimNames.clear();
	_contSounds.clear();
	_contDirImage.clear();
}
//------------------------------------------------------------------------------------
void Preloader::AddImageDir( const char *filename )
{
	_contDirImage.insert( filename );
}
//------------------------------------------------------------------------------------
void Preloader::FreeResources()
{
	SoundEngine::Get()->Reset();
	
	CCSpriteFrameCache::sharedSpriteFrameCache()->removeSpriteFrames();
	//CCSpriteFrameCache::sharedSpriteFrameCache()->purgeSharedSpriteFrameCache();
	CCTextureCache::sharedTextureCache()->removeUnusedTextures();

	CCLabelBMFont::purgeCachedData();
}
//------------------------------------------------------------------------------------
void Preloader::AddPiggyAnimSet( puBlock *block )
{
	float radius;
	radius = block->GetWidth() / 2.0f * RATIO ;

	AddPiggyAnimSet( radius );
}
//------------------------------------------------------------------------------------
void Preloader::AddPiggyAnimSet( float radius )
{
	AddSound( SoundEngine::eSoundCoinCollected );

	AddAnim( GameTypes::eAnim_PiggyBankBounce,				radius );
	AddAnim( GameTypes::eAnim_PiggyBankBounce_Sit,			radius );
	AddAnim( GameTypes::eAnim_PiggyBankBounce_Roll,			radius );

	AddAnim( GameTypes::eAnim_PiggyBankLevelFailed,			radius );
	AddAnim( GameTypes::eAnim_PiggyBankLevelFailed_Sit,		radius );
	AddAnim( GameTypes::eAnim_PiggyBankLevelFailed_Roll,	radius );

	AddAnim( GameTypes::eAnim_PiggyBankLevelDone,			radius );
	AddAnim( GameTypes::eAnim_PiggyBankLevelDone_Sit,		radius );
	AddAnim( GameTypes::eAnim_PiggyBankLevelDone_Roll,		radius );

	AddAnim( GameTypes::eAnim_PiggyBankCoinCollected,		radius );
	AddAnim( GameTypes::eAnim_PiggyBankCoinCollected_Sit,	radius );
	AddAnim( GameTypes::eAnim_PiggyBankCoinCollected_Roll,	radius );

	AddAnim( GameTypes::eAnim_PiggyBankUaa,					radius );
	AddAnim( GameTypes::eAnim_PiggyBankFiuFiu,				radius );
	AddAnim( GameTypes::eAnim_PiggyBankSalto,				radius );
	AddAnim( GameTypes::eAnim_PiggyBankEeeOoo,				radius );
	AddAnim( GameTypes::eAnim_PiggyBankHappyJump,			radius );
	AddAnim( GameTypes::eAnim_PiggyBankLookAround,			radius );
	AddAnim( GameTypes::eAnim_PiggyBankEyesBlink,			radius );
	AddAnim( GameTypes::eAnim_PiggyBankNoseBubble,			radius );

	AddAnim( GameTypes::eAnim_PiggyBankToRolling,			radius );
	AddAnim( GameTypes::eAnim_PiggyBankToSitting,			radius );
	AddAnim( GameTypes::eAnim_PiggyBankRotationFix,			radius );
}
//------------------------------------------------------------------------------------
void Preloader::AddPiggyRolledAnimSet( float radius )
{
	AddSound( SoundEngine::eSoundCoinCollected );

	AddAnim( GameTypes::eAnim_PiggyBankBounce_Roll,			radius );
	AddAnim( GameTypes::eAnim_PiggyBankLevelFailed_Roll,	radius );
	AddAnim( GameTypes::eAnim_PiggyBankLevelDone_Roll,		radius );
	AddAnim( GameTypes::eAnim_PiggyBankCoinCollected_Roll,	radius );
}
//------------------------------------------------------------------------------------
void Preloader::AddPiggyStaticAnimSet( float radius )
{
	AddSound( SoundEngine::eSoundCoinCollected );

	AddAnim( GameTypes::eAnim_PiggyBankBounce,				radius );
	AddAnim( GameTypes::eAnim_PiggyBankBounce_Sit,			radius );

	AddAnim( GameTypes::eAnim_PiggyBankLevelFailed,			radius );
	AddAnim( GameTypes::eAnim_PiggyBankLevelFailed_Sit,		radius );

	AddAnim( GameTypes::eAnim_PiggyBankLevelDone,			radius );
	AddAnim( GameTypes::eAnim_PiggyBankLevelDone_Sit,		radius );

	AddAnim( GameTypes::eAnim_PiggyBankCoinCollected,		radius );
	AddAnim( GameTypes::eAnim_PiggyBankCoinCollected_Sit,	radius );

	AddAnim( GameTypes::eAnim_PiggyBankUaa,					radius );
	AddAnim( GameTypes::eAnim_PiggyBankFiuFiu,				radius );
	AddAnim( GameTypes::eAnim_PiggyBankEeeOoo,				radius );
	AddAnim( GameTypes::eAnim_PiggyBankLookAround,			radius );
	AddAnim( GameTypes::eAnim_PiggyBankEyesBlink,			radius );
	AddAnim( GameTypes::eAnim_PiggyBankNoseBubble,			radius );
}
//------------------------------------------------------------------------------------
void Preloader::AddThiefAnimSet( float radius )
{
	AddSound( SoundEngine::eSoundAnim_PiggyStolen );

	Preloader::Get()->AddAnim( GameTypes::eAnim_ThiefSitBounce,			radius );
	Preloader::Get()->AddAnim( GameTypes::eAnim_ThiefRollBounce,		radius );

	Preloader::Get()->AddAnim( GameTypes::eAnim_ThiefSmiles_Sit,		radius );
	Preloader::Get()->AddAnim( GameTypes::eAnim_ThiefSmiles_Roll,		radius );
	Preloader::Get()->AddAnim( GameTypes::eAnim_ThiefLookAround,		radius );

	Preloader::Get()->AddAnim( GameTypes::eAnim_ThiefToRolling,			radius );
	Preloader::Get()->AddAnim( GameTypes::eAnim_ThiefToSitting,			radius );
	Preloader::Get()->AddAnim( GameTypes::eAnim_ThiefRotationFix,		radius );
}
//------------------------------------------------------------------------------------
void Preloader::AddThiefRolledAnimSet( float radius )
{
	AddSound( SoundEngine::eSoundAnim_PiggyStolen );

	Preloader::Get()->AddAnim( GameTypes::eAnim_ThiefRollBounce,		radius );
	Preloader::Get()->AddAnim( GameTypes::eAnim_ThiefSmiles_Roll,	radius );
}
//------------------------------------------------------------------------------------
void Preloader::AddCopAnimSet( float radius )
{
	Preloader::Get()->AddSound( SoundEngine::eSoundThiefCaught );

	Preloader::Get()->AddAnim( GameTypes::eAnim_CopSitBounce,		radius );
	Preloader::Get()->AddAnim( GameTypes::eAnim_CopRollBounce,		radius );

	Preloader::Get()->AddAnim( GameTypes::eAnim_CopSmiles,			radius );
	Preloader::Get()->AddAnim( GameTypes::eAnim_CopSmiles_Sit,		radius );
	Preloader::Get()->AddAnim( GameTypes::eAnim_CopSmiles_Roll,		radius );
	Preloader::Get()->AddAnim( GameTypes::eAnim_CopJump,			radius );

	Preloader::Get()->AddAnim( GameTypes::eAnim_CopToRolling,		radius );
	Preloader::Get()->AddAnim( GameTypes::eAnim_CopToSitting,		radius );
	Preloader::Get()->AddAnim( GameTypes::eAnim_CopRotationFix,		radius );
}
//------------------------------------------------------------------------------------
Preloader::~Preloader()
{
	//FreeResources();
	Reset();
}
//------------------------------------------------------------------------------------
int32 Preloader::MemoryWarningRecieved( void* systemData, void* userData )
{
	Preloader::Get()->FreeResources();

#ifdef BUILD_TEST
	stringstream ss;
	static int callCount = 0;

	callCount++;
	ss << "Warning received: " << callCount;
	DebugLogger::Get( DebugLogger::LogNotifications )->Write( ss.str().c_str() );
#endif

	return 0;
}
//------------------------------------------------------------------------------------




//------------------------------------------------------------------------------------
// Anim Size set
//------------------------------------------------------------------------------------
void AnimSizeSet::Dump()
{
	D_LOG("-----------------" );
	D_LOG("-----------------" );
	D_LOG("Size: %d", (int)size() );
	D_LOG("-----------------" );
	for ( iterator it = begin(); it != end(); it++ )
	{
		D_LOG("Anim %d Size: %d", it->first, (int)it->second.size() );
		D_LOG("-----------------" );

		for ( RadiusSet::iterator it2 = it->second.begin(); it2 != it->second.end(); it2++ )
		{
			D_LOG("Radius: %f", *it2 );
		}
	}
}
//------------------------------------------------------------------------------------
void AnimSizeSet::Add( CharactersAnim anim, float radius )
{
	RadiusSet &rset = operator[]( anim );
	rset.insert( radius );
}
//------------------------------------------------------------------------------------
void AnimSizeSet::Reset()
{
	for ( iterator it = begin(); it != end(); it++ )
		it->second.clear();

	clear();
}
//------------------------------------------------------------------------------------
