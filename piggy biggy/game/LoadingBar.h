#ifndef __LOADINGBAR_H__
#define __LOADINGBAR_H__

#include "cocos2d.h"

USING_NS_CC;
//-----------------------------------------------------------------------------------
class LoadingSprite : public CCSprite
{
public:
    static LoadingSprite* Get();
    static LoadingSprite* GetL();
    static LoadingSprite* GetP();
    
    ~LoadingSprite();
    virtual void setOpacity(GLubyte var);
    
private:
    LoadingSprite();
    
    void Init( const char *bgFilename, const char *txtFilename );

private:
    CCSprite *_txtSprite;
    CCSprite *_background;
};

//-----------------------------------------------------------------------------------
class LoadingBar : public CCLayer
{
public:
	static LoadingBar* Get();
	static LoadingBar* GetAdjusted();
    
	static LoadingBar* Get( const char* bgFilename );

	virtual bool init( const char *filename );
	~LoadingBar();

	typedef void (*FuncPtr)( void * );
	void AddToScene( FuncPtr func, void *data );

private:
	LoadingBar();
	void StartScheduler();
	virtual void Step( cocos2d::ccTime dt );

private:
	bool		_isSchedule;
	int			_delayCounter;

	FuncPtr		_func;
	void		*_funcData;
};
//-----------------------------------------------------------------------------------
	
#endif
