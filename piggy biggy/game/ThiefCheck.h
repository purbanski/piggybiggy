#ifndef __THIEFCHECK_H__
#define __THIEFCHECK_H__

class ThiefCheck
{
public:

	bool IsStolen();

private:
	bool CheckFileSize( const char *filename, unsigned int len );
	bool CheckFileCode( const char *filename, unsigned int code );

private:
	int	GetFileCode( const char *filename );

};
#endif
