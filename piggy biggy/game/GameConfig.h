#ifndef __SETTINGS_H__
#define __SETTINGS_H__

extern float RATIO;

#include <Box2D/Box2D.h>
#include "cocos2d.h"
#include "GameTypes.h"

USING_NS_CC;
using namespace GameTypes;
using namespace std;

class Config
{
public:
    //-------------------------------------
    // URLs
    //-------------------------------------
    static const char *URLFacebook;
    static const char *URLTwitter;
    static const char *URLYouTube;
    static const char *URLAppStore;
    static const char *URLMarmaladeAppStore;
    
    //-------------------------------------
    // Appirater
    //-------------------------------------
    static const int AppiraterDaysUntilPrompt;
    static const int AppiraterUsesUntilPrompt;
    static const int AppiraterSignificantEventsUntilPrompt;
    static const int AppiraterDaysBeforeReminding;
    static const bool AppiraterDebug;

    //-------------------------------------
    // Game Watcher
    //-------------------------------------
    static const RangeInt Watcher_Activate_LevelCompleted;
    static const RangeInt Watcher_Activate_LevelSkipped;
    
    
	//-------------------------------------
	// stats
	//-------------------------------------
    static const char *StatsUrl;
    
    static const char *GameName;
    static const char *AppStoreAppId;
    static const char *AppStoreProductName;
    static const char *AppVersion;


  	//-------------------------------------
	// Facebook
	//-------------------------------------
    static const char *FBAppId;
    static const char *FBOpenGraphURL;
    static const char *FBCustomPostImagesURL;
    
    static const char *FBScoreURL;
    static const char *FBRequestsURL;
    static const char *FBHighScoreFont;
    static const char *FBHighScoreFontPlain;
    static const char *FBHighScorePositionFont;
    static const char *FBRequestMsgFont;
    static const float FBRequesterCheckEverySec;
    static const float FBNotifyRequestCheckEverySec;
    static const unsigned int FBFriendPickerFriendCount;
    static const unsigned int FBLevelChallengeFrinedCount;
    
    static const char *HansSFont;

    static const ccColor3B FBMenuItemColor;
    static const ccColor3B FBMenuTitleColor;
    static const ccColor3B FBRequestLabelsColor;
    static const ccColor3B FBRequestHelpMsgColor;
    static const unsigned int FBRequestPiggyBgOpacity;
    
    static const ccColor3B ColorWhite;
    static const ccColor3B ColorBlack;
    

	//-------------------------------------
	// Fonts
	//-------------------------------------
	static const char *LevelNumberFont;
	static const char *LevelNameFont;
	static const char *LevelNamePlayFont;
	static const char *LevelScaleFont;
	static const char *LevelMsgFont;
	static const char *LevelSubMenuFont;
	static const char *LevelShowClueButton;
	static const char *LevelClueTimerFont;
	static const char *LevelSkipCountVioletFont;
	static const char *LevelSkipCountRedFont;
	static const char *LevelSkipCountDisabledVioletFont;
	static const char *LevelSkipCountDisabledRedFont;
	static const char *LevelScoreFont;
	static const char *LevelStatsFont;
	static const char *LevelStatsDisabledFont;
	static const char *LevelFinishTimerFont;

	static const char *LevelDebugMenuFont;
	static const char *MemoryViewFont;


	static const char *SelectLevelMenuFont;
	static const char *SelectLevelScoreTime;
	static const char *MainMenuFont;
	static const char *DefaultClueFont;
	static const char *MotherCounterFont;
	static const char *DebugNodeFont;

	static const char *ConfigFileExt;

	//-------------------------------------
	// Filenames
	//-------------------------------------
	static const char *GameStatusFilename;


	//-------------------------------------
	// GameCenter
	//-------------------------------------
    static const char *GameCenter_LeaderBoard_TotalScore;
    static const char *GameCenter_LeaderBoard_JeansScore;
    static const char *GameCenter_LeaderBoard_MoroScore;
    static const char *GameCenter_LeaderBoard_RastaScore;
    static const char *GameCenter_LeaderBoard_KimonoScore;
    static const char *GameCenter_LeaderBoard_ProfessorScore;

    static const char *GameCenter_Achievement_GameProgress;
    static const char *GameCenter_Achievement_Hacker;

    static const char *GameCenter_Achievement_Bank;
    static const char *GameCenter_Achievement_Car;
    static const char *GameCenter_Achievement_Reel;
    static const char *GameCenter_Achievement_Rotate;
    static const char *GameCenter_Achievement_Bomb;
    static const char *GameCenter_Achievement_Spitter;
    
    static const char *GameCenter_Achievement_JeansPocket;
    static const char *GameCenter_Achievement_MoroPocket;
    static const char *GameCenter_Achievement_RastaPocket;
    static const char *GameCenter_Achievement_KimonoPocket;
    static const char *GameCenter_Achievement_ProfessorPocket;

    
	//-------------------------------------
	// Anim
	//-------------------------------------
	static const float AnimDayLong;
	static const int AnimCircleDefaultSize;
	static const float OrientationArrowsAnimDuration;
	static const float OrientationArrowsAnimRotation;
	static const float AnimRotationStartLimit;
	static const float AnimRotationSlowMovingStartLimit;

	// this is for setting limits 
	// for speed active animation for blocks
	static const b2Vec2 AnimFastSpeedLinearLimits;
	static const b2Vec2 AnimFastSpeedAngularLimits;
	static const b2Vec2 AnimSlowSpeedLimits;
	static const float AnimSlowSpeedTimeLimit;
    
    static const float PageTurnDur;
    
    static const int AnimPiggy360FrameCount;
    static const int AnimThief360FrameCount;
    static const int AnimCop360FrameCount;

	//-------------------------------------
	// Game
	//-------------------------------------
	static const float ShowBuySceneSkipTime;
	static const int FreeLevelCount;
	static const int GameSkipMaxCount;
	static const int GameDemoChangeCounter;
    static const unsigned int ShowSkipIntro_LevelRestarCount;
	static const CCSize GameSize;

	static const int PocketLevelOpenToFinish;
    static const int GameTotalLevelCount;

    static const char *TestFlightId;
    static const char *GoogleAnalyticsID;
    static const int   GoogleAnalyticsDelay;
    
	//-------------------------------------
	// Score limits
	//-------------------------------------
	static const int ScoreTime5Coins;
	static const int ScoreTime4Coins;
	static const int ScoreTime3Coins;
	static const int ScoreTime2Coins;
	static const int ScoreTime1Coins;
    static const int MaxScoreLevelCoins;

	//-------------------------------------
	// Shadows, Glue, etc
	//-------------------------------------
	static const int ShadowOpacity;
	static const b2Vec2 ShadowPosition;
	
	static const float SpriteGlueOutBorder;
	static const float SpriteGlueDestroyDelay;
	static const float SpriteGlueDestroyFadeDur;
	


	//-------------------------------------
	// World
	//-------------------------------------
	static const int WorldVelocityIterations;
	static const int WorldPositionIterations;

	static const int WorldVelocityIterationsHigh;
	static const int WorldPositionIterationsHigh;

	static const int WorldGunVelocityIterations;
	static const int WorldGunPositionIterations;
	

	//-------------------------------------
	// Level
	//-------------------------------------
	static const float LevelShotChangeDelay;
	static const int LevelStateChangeDelay;
    static const int LevelStateChangeDelayIntroPointer;
	static const int LevelStateChangeToCompletedDelay;
	static const int LevelIconAnimateInitDelay;
	static const int LevelIconAnimateCountDown;
	static const int LevelButtonOpacity;
	static const b2Vec2 LevelAccelerometrBallPosition;
	static const int LevelFirstMsgTick;
	static const int LevelNotifiersOpacity;

	static const float	WaterBouyancy;
	static const float	WaterWaveFrequency;
	static const b2Vec2 WaterWaveForce;
		
	static const int	BlockTint;
	//-------------------------------------
	// Sound
	//-------------------------------------
	static const float SoundEffectVol;
	static const float SoundMusicVol;
	static const int SoundChannelRate;
	static const int SoundStereoEnabled;
	static const int SoundWavHeaderSize;


	//-------------------------------------
	// Chain 
	//-------------------------------------
	static const b2Color ChainCutableColor;
	static const b2Color ChainStrongColor;
	

	//-------------------------------------
	// to be sorted
	//-------------------------------------
	static const int PolygonMaxVertex = 32; // must be init here, as it is used in other headers
	static const int GunClipSize = 32; // must be init here, as it is used in other headers
	static const int MotherMaxKidCount = 8; // must be init here, as it is used in other headers 
	static const int ClueManagerTickDelay;

	static const int EditorMenuOpacity;
	static const int MotherCounterEndOpacity;

	//-------------------------------------
	// Collide Category
	//-------------------------------------
	typedef enum 
	{
		eFilterCategorySlider		= 0x0001,
		eFilterCategorySwing		= 0x0002,
		eFilterCategoryDoor			= 0x0004,
		eFilterCategoryBullet		= 0x0008,
		eFilterCategoryGun			= 0x0010,
		eFilterCategoryPiggy		= 0x0020,
		eFilterCategoryCoin			= 0x0040,
		eFilterCategoryCoinBorder	= 0x0080,
		eFilterCategoryBomb			= 0x0100,
		eFilterCategoryAccelIcon	= 0x0200,

		eFilterCategoryGroupAccelIcon= 0x1000,
		eFilterCategoryGroup2       = 0x2000,
		eFilterCategoryGroup3       = 0x4000,
		eFilterCategoryGroup4       = 0x8000
	} CollideCategory;


	//-------------------------------------
	// Mouse touch priority
	//-------------------------------------
	typedef enum
	{
		eMousePriority_SelectLevelScreen    = 50,
		eMousePriority_GameBlockOperator    = 45,
		eMousePriority_EditorBlockMover     = 40,
		eMousePriority_MultiBlockSelector   = 35,
		eMousePriority_EditorMovebleLayer   = 30,
		eMousePriority_CreateBlock          = 20,
		eMousePriority_LevelSubMenu         = 15,
		eMousePriority_MainMenu             = 10,
		eMousePriority_LevelClue            = 5,
   		eMousePriority_FBHighScore          = 2,

        eMousePriority_FBIgnoreLayerBelowX2 = -148,
        eMousePriority_FBIgnoreLayerBelow   = -149,
        eMousePriority_FBIgnoreLayer        = -150,
        eMousePriority_FBIgnoreLayerAbove   = -151,
        eMousePriority_FBIgnoreLayerAboveX2 = -152,
        eMousePriority_FBMenuLayer          = -155,
        eMousePriority_FBMenuLayerVIPBelowX2= -180,
        eMousePriority_FBMenuLayerVIPBelow  = -190,
        eMousePriority_FBMenuLayerVIP       = -200,
        eMousePriority_FBMenuLayerVIPAbove  = -210,
        eMousePriority_FBMenuLayerVIPAboveX2= -220,
        eMousePriority_FBMenuLayerVIPAboveX3= -220,
        
        eMousePriority_FBMenuLayerSuperVIP         = -230,
        eMousePriority_FBMenuLayerSuperVIPAbove    = -231,
        eMousePriority_FBMenuLayerSuperVIPAboveX2  = -232,

	} MousePriority;



};

// fix me blowe do i need it?
//-------------------------------------
// BlockMaterial
//-------------------------------------

typedef enum
{
	eMaterialFragile = 1,
	eMaterialFragileStatic,
	eMaterialIron,
	eMaterialWall,
	eMaterialStone,
	eMaterialWood,
	eMaterialBouncer,
	eMaterialIce
} BlockMaterial;

#endif

