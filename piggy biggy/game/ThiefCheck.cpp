#include <string>
#include "unFile.h"
#include "ThiefCheck.h"
#include "Debug/MyDebug.h"

//---------------------------------------------------------------------------------------
bool ThiefCheck::IsStolen()
{
#ifndef BUILD_EDITOR4ALL
	return false;
#endif

	if ( ! CheckFileSize( "Fonts/DebugNode.fnt", 17225 ))	return true;
	if ( ! CheckFileSize( "Fonts/DebugNode_0.png", 13416 ))	return true;
	
	if ( ! CheckFileCode( "Fonts/DebugNode.fnt", 7845 ))	return true;
	if ( ! CheckFileCode( "Fonts/DebugNode_0.png", 25834 ))	return true;

	return false;
}
//---------------------------------------------------------------------------------------
bool ThiefCheck::CheckFileSize( const char *filename, unsigned int size )
{
	if ( ! unResourceFileCheckExists( filename ))
		return false;

	unFile *file;
	file = unResourceFileOpen( filename, "rb" );

	if ( ! file )
		return false;

	unsigned int filesize;
	filesize = unFileGetSize( file );

	unFileClose( file );

	if ( filesize != size )
		return false;	

	return true;
}
//---------------------------------------------------------------------------------------
bool ThiefCheck::CheckFileCode( const char *filename, unsigned int code )
{
	if ( GetFileCode( filename) != code  ) return false;
	return true;
}
//---------------------------------------------------------------------------------------
int ThiefCheck::GetFileCode( const char *filename )
{
	int code;
	code = 0;

	unFile* file;
	file = unResourceFileOpen( filename, "rb" );

	if ( ! file )
		return -1;

	//-----------------------
	const int bufSize = 24 * 1024;
	char buff[bufSize];
	unsigned int bytesRead;

	memset( buff, 0, bufSize );
	bytesRead = unFileRead( buff, sizeof( char ), 256, file);
	
	unFileClose( file );

	if ( bytesRead < 1 )
		return -1;

	for ( unsigned int i = 1; i < bytesRead; i++ )
	{
		//D_LOG
		code += (( buff[i-1] ^ buff[i] ) % 1000 );
			
	}
	D_INT(code);
	return code;
}
