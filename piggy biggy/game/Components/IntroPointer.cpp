#include "IntroPointer.h"
#include <Skins.h>
#include <string>
#include "GameConfig.h"
#include "Tools.h"
#include "Game.h"

//--------------------------------------------------------
IntroPointer* IntroPointer::Create()
{
    IntroPointer *pointer;
    pointer = new IntroPointer();
    
    if ( pointer )
    {
        pointer->autorelease();
        return pointer;
    }
    CC_SAFE_DELETE( pointer );
    return NULL;
}
//--------------------------------------------------------
IntroPointer::IntroPointer()
{
    _emitter = CCParticleSun::node();
    _emitter->setLife( 0.75f );
    _emitter->setLifeVar( 0.0f );
    _emitter->setEmissionRate( 600 );
    _emitter->setSpeed( 0.0f );
    _emitter->setSpeedVar( 0.0f );
    _emitter->setAngle( 0.0f );
    _emitter->setAngleVar( 0.0f );

    
    //string filename = "Images/Particles/Comet.plist";
    
    _emitter->setTexture( CCTextureCache::sharedTextureCache()->addImage( Skins::GetSkinName("Images/Particles/Comet.png")));
    _emitter->setPosition( CCPoint( -1000.0f, -1000.0f ));
    addChild( _emitter, 10);
    
	// moving background
	_background = CCSprite::spriteWithFile( Skins::GetSkinName( "Images/Levels/Backgrounds/horizontal.jpg"));
    _background->setOpacity( 0 );
	addChild(_background, 5 );
    
    _background->setContentSize( CCSize( 960, 640 ));
	_background->setPosition( CCPointMake( 0, 0) );

}
//--------------------------------------------------------
IntroPointer::~IntroPointer()
{
    
}
//--------------------------------------------------------
void IntroPointer::SetPosition( CCTouch *touch)
{
    CCPoint location = touch->locationInView();
	CCPoint convertedLocation = CCDirector::sharedDirector()->convertToGL(location);

    CCPoint pos = CCPointZero;
    if (_background)
    {
	    pos = _background->convertToWorldSpace(CCPointZero);
    }
    pos.x += Config::GameSize.width / 2.0f * Game::Get()->GetScale();
    pos.y += Config::GameSize.height / 2.0f * Game::Get()->GetScale();
	_emitter->setPosition( ccpSub(convertedLocation, pos) );
}
//------------------------------------------------------------
void IntroPointer::Hide()
{
    CCFiniteTimeAction *sequence;
   
    sequence = CCSequence::actions(
                                   CCDelayTime::actionWithDuration( 1.5f ),
                                   CCCallFunc::actionWithTarget( this, callfunc_selector( IntroPointer::EndingUp )),
                                   NULL );
    _emitter->setDuration( 0.5f );
    _emitter->runAction( sequence );
    
}
//--------------------------------------------------------
void IntroPointer::EndingUp()
{
    _emitter->removeFromParentAndCleanup( true );   
}

