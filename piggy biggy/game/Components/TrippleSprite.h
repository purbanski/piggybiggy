#ifndef __TRIPPLESPRITE_H__
#define __TRIPPLESPRITE_H__

#include "cocos2d.h"
USING_NS_CC;

//-----------------------------------------------------------------------------
class TrippleSprite 
{
public:
	TrippleSprite();
	TrippleSprite( const char *filename );
	TrippleSprite( const char *normal, const char *selected, const char *disable );

	CCSprite *_normal;
	CCSprite *_selected;
	CCSprite *_disable;
};	
//-----------------------------------------------------------------------------
#endif
