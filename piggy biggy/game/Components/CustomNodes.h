#ifndef __PU_CUSTOMNODES_H__
#define __PU_CUSTOMNODES_H__
//---------------------------------------------------------------
#include <cocos2d.h>
#include <vector>
//---------------------------------------------------------------
USING_NS_CC;
using namespace std;
//---------------------------------------------------------------
class NodeAlign : public CCNode
{
public:
    static NodeAlign* Create();
    ~NodeAlign();

    void AlignVertical( float padding );
    void AlignHorizontal( float padding );
    float GetPadding();
    
    void AddNode( CCNode *node );
    void RemoveAllNodes();
    
private:
    NodeAlign();
    void Init();
    
private:
    typedef vector<CCNode*> Nodes;
    Nodes   _nodes;
    float   _padding;
};
//---------------------------------------------------------------
class LanguageMenu : public CCNode
{
public:
    static LanguageMenu* Create();
    ~LanguageMenu();
    void Menu_SelectLang( CCObject *sender );
    void Menu_MainMenu( CCObject *sender );
    void Menu_ResetGame( CCObject *pSender );
    void Menu_SendEmail(CCObject *sender );
    
private:
    typedef pair<LanguageType,string> LangData;
    typedef vector<LangData> LangCodes;

private:
    LanguageMenu();
    void Init();
    CCMenuItemToggle* GetToggleButton();
    CCMenu* ConstructMainMenu();
    CCMenu* ConstructEmailMenu();
    void SortLangaugaes();
    void LogLanguage( LanguageType lang );
    
    typedef void ( LanguageMenu::*menuFnPtr )( CCObject *sender );
    CCMenuItemSprite* GetMenuItem( const char *buttonName, menuFnPtr fn, bool anim );
    
    void Refresh();
    void Refresh2Step();
    
private:
    LangCodes _langCodes;
    CCMenuItemSprite  *_resetButton;
};
//---------------------------------------------------------------


#endif