#ifndef __CUSTOMSPRITES_H__
#define __CUSTOMSPRITES_H__

#include "cocos2d.h"
#include <Box2D/Box2D.h>
#include "Facebook/FBTool.h"
#include "Platform/WWWTool.h"
#include "Blocks/puBlock.h"

USING_NS_CC;

//--------------------------------------------------------------------------------
class BankSaveWheelSprite : public CCSprite
{
public:
	static BankSaveWheelSprite* Create( const char *filename );
	virtual ~BankSaveWheelSprite();

	virtual void setPosition(const CCPoint& pos);
	virtual void setRotation(float fRotation);
	void Init();

private: 
	BankSaveWheelSprite();

private:
	CCSprite*	_raySprite;
};
//--------------------------------------------------------------------------------
class ReelSprite : public CCSprite
{
public:
	static ReelSprite* Create( const char *filename );
	virtual ~ReelSprite();

	virtual void setPosition( const CCPoint& pos );
	virtual void setRotation( float rotation );
	void Init();

private: 
	ReelSprite();

private:
	CCSprite*	_raySprite;
};
//--------------------------------------------------------------------------------
class SpriteWithRay : public CCSprite
{
public:
	static SpriteWithRay* Create( CCSprite *sprite, float radius, const char *rayFile );
	virtual ~SpriteWithRay();

	virtual void setPosition( const CCPoint& pos );
	virtual void setRotation( float rotation );
	virtual void setOpacity( GLubyte var );

	void Init( const char *rayFile, float radius );

	CCSprite *GetRaySprite();

private: 
	SpriteWithRay();

private:
	CCSprite*	_raySprite;
};
//--------------------------------------------------------------------------------
class CoinSprite : public CCSprite
{
public:
	static CCSprite* Create( puBlock *block );
	virtual ~CoinSprite();

	virtual void setPosition( const CCPoint& pos );
	virtual void setRotation( float rotation );
	virtual void setOpacity( GLubyte var );

	void Init( puBlock *block );

private: 
	CoinSprite();
	void RunFlashAnim();

private:
	CCSprite		*_flashSprite;
	SpriteWithRay	*_coinSprite;
};
//--------------------------------------------------------------------------------
class MovedSprite : public CCSprite
{
public:
	static MovedSprite* Create( float x, float y, float dur, const char *filename );
	virtual ~MovedSprite();

	virtual void setPosition( const CCPoint& pos );
	virtual void setRotation( float rotation );
	void Init();

private: 
	MovedSprite( float x, float y, float duration );

private:
	float	_xMove;
	float	_yMove;
	float	_rotation;
};
//--------------------------------------------------------------------------------
class LiftDoorSprite : public CCSprite
{
public:
	static LiftDoorSprite* Create();
	virtual ~LiftDoorSprite();
	virtual void setPosition( const CCPoint& pos );
	virtual void setRotation( float rotation );
	void Init();

	void SetRed();
	void SetGreen();

private: 
	LiftDoorSprite();

private:
	CCSprite *_spriteRed;
	CCSprite *_spriteGreen;

};
//--------------------------------------------------------------------------------
class puBank;
class BankSaveSprite : public CCSprite
{
public:
	typedef void ( *FuncPtr )( void *data );

	static BankSaveSprite* Create();
	virtual ~BankSaveSprite();
	void Init();

	virtual void setPosition( const CCPoint& pos );
	void StartAnim( FuncPtr fn, puBank *bank );

private: 
	BankSaveSprite();
	void PlaySmallWheelSound();
	void PlayBigWheelSound();
	void PlaySaveSlideUpSound();

	void AnimFinished();

private:
	CCSprite	*_openWheelSprite;
	CCSprite	*_lockWheelSprite;
	CCSprite	*_bankSaveSprite;

	puBank		*_bank;
	FuncPtr		_callbackFn;

};
//--------------------------------------------------------------------------------
class GlueCircleSprite : public CCSprite
{
public:
	static GlueCircleSprite* Create( float radius );
	virtual ~GlueCircleSprite();

private: 
	GlueCircleSprite();
	void ApplyScale( float radius );
	void ApplyRandRotation();
};
//--------------------------------------------------------------------------------
class GlueBoxSprite : public CCSprite
{
public:
	static GlueBoxSprite* Create( float width, float height );
	virtual ~GlueBoxSprite();

private: 
	GlueBoxSprite();
	void RandRotation();
	void Init( float width, float height );
};
//--------------------------------------------------------------------------------
class FBProfileSprite : public CCSprite, public WWWToolListener
{
public:
	static FBProfileSprite* Create();
	static FBProfileSprite* Create( const char *userFbID );
	static FBProfileSprite* CreateBold( const char *userFbID );
    
	virtual ~FBProfileSprite();

    virtual void Http_FinishedLoading( void *connection, const void *data, int len );
    virtual void Http_FailedWithError( void *connection, int error );

    virtual void setPosition( CCPoint pos );

private: 
	FBProfileSprite();

	void Init( const char *fbId, const char *fileframe );
    void Init();
    void ProfileSpriteSetup();
    void AddFrame( const char *framefile );
    void RescaleProfilePicture();

private:
    CCSprite    *_fbProfileSprite;
    CCSprite    *_fbProfileFrameSprite;
    bool        _processing;
};
//--------------------------------------------------------------------------------
class FBProfileBigSprite : public CCSprite, public WWWToolListener
{
public:
	static FBProfileBigSprite* Create();
	static FBProfileBigSprite* Create( const char *userFbID );
	static FBProfileBigSprite* Create( const char *userFbID, const char *filename );
  	static FBProfileBigSprite* Create( const char *userFbID,  GameLevelUsersType userType );
	static FBProfileBigSprite* CreateBold( const char *userFbID );
    
	virtual ~FBProfileBigSprite();

    virtual void Http_FinishedLoading( void *connection, const void *data, int len );
    virtual void Http_FailedWithError( void *connection, int error );

    virtual void setPosition( CCPoint pos );

private: 
	FBProfileBigSprite();

	void Init( const char *fbId, const char *fileframe );
    void Init( const char *fbId, GameLevelUsersType userType );
    void Init();
    
    void ProfileSpriteSetup();
    void AddFrame( const char *framefile );
    void AddDummyProfile();
    void RescaleProfilePicture();

private:
    CCSprite    *_fbProfileSprite;
    CCSprite    *_fbProfileFrameSprite;
    bool        _processing;
    
    static float _xFixDelta;
    static float _yFixDelta;
};
//--------------------------------------------------------------------------------
class FBUserFirstNameNode : public CCNode, public FBToolListener
{
public:
    static FBUserFirstNameNode* Create( const char *fbId );
    static FBUserFirstNameNode* CreateCenter( const char *fbId );
    static FBUserFirstNameNode* Create( const char *fbId, const CCSize &dimision, CCTextAlignment textAligment );
    
    ~FBUserFirstNameNode();
    
    virtual void FBRequestCompleted( FBTool::Request requestType, void *data );
    virtual void FBRequestError( int error );
    void SetColor( const ccColor3B& color );
    
private:
    FBUserFirstNameNode();
    void Init( const char *fbId );
    void Init( const char *fbId, const CCSize &dimision, CCTextAlignment textAligment );
    
    void InitCommon( const char *fbId );
    
private:
    CCLabelTTF   *_label;
    bool        _processing;
};
//--------------------------------------------------------------------------------
class FBCoinScoreNode : public CCNode
{
public:
    static FBCoinScoreNode* Create( int totalSeconds );
    ~FBCoinScoreNode();
    
private:
    FBCoinScoreNode();
    void Init( int totalSeconds );
    
private:
    
};
//--------------------------------------------------------------------------------
class LoadingAnim : public CCSprite
{
public:
	static LoadingAnim* Create();
	virtual ~LoadingAnim();

	void Init();
    void Stop();
    void ImediateCleanUp();
    void SetPositionForFBRequestMenu();
    
private: 
	LoadingAnim();
	void RunAnim();
    void StopFinal();

private:
	CCSprite		*_flashSprite;
	SpriteWithRay	*_coinSprite;
};
//--------------------------------------------------------------------------------
class FBLoginLabel : public CCSprite
{
public:
    static FBLoginLabel* Create();
    ~FBLoginLabel();

private:
    FBLoginLabel();
    void Init();
    
    void ShowLabel1();
    void ShowLabel2();
    
private:
    static float FadeDur;
    CCSprite    *_arrowSprite;
    CCLabelTTF  *_label1;
    CCLabelTTF  *_label2;
};
//--------------------------------------------------------------------------------
class FadeInLabel : public CCNode
{
public:
    static FadeInLabel *Create( float dur, const char *text, const char *font, float fontSize, float delay = 0.0f );
    ~FadeInLabel();
    CCLabelTTF *GetLabel() { return _label; }
    void SetColor( const ccColor3B &color );
    
private:
    FadeInLabel();
    void Init( float dur, const char *text, const char *font, float fontSize, float delay );

private:
    CCLabelTTF  *_label;
};
//--------------------------------------------------------------------------------
class FadeInLabelBMP : public CCNode
{
public:
    static FadeInLabelBMP *Create( float dur, const char *text, const char *font, float delay = 0.0f );
    ~FadeInLabelBMP();
    CCLabelBMFont *GetLabel() { return _label; }
    void SetColor( const ccColor3B &color );
    
private:
    FadeInLabelBMP();
    void Init( float dur, const char *text, const char *font, float delay );

private:
    CCLabelBMFont  *_label;
};
//--------------------------------------------------------------------------------
class FadeInFadeOutLabel : public CCNode
{
public:
    static FadeInFadeOutLabel *Create( float fadeInDur,
                                      float fadeOutDur,
                                      float shownDur,
                                      float showDelay,
                                      const char *text, const char *font, float fontSize );
    ~FadeInFadeOutLabel();
    CCLabelTTF *GetLabel() { return _label; }
    void SetColor( const ccColor3B &color );
    
private:
    FadeInFadeOutLabel();
    void Init( float fadeInDur,
              float fadeOutDur,
              float shownDur,
              float showDelay,
              const char *text, const char *font, float fontSize );
    
    void CleanUpNode( CCNode *sender, void *data );

private:
    CCLabelTTF  *_label;
};
//--------------------------------------------------------------------------------
class FadeInSprite : public CCNode
{
public:
    static FadeInSprite *Create( float dur, const char *filename, float delay = 0.0f, unsigned int finalOpacity = 255 );
    ~FadeInSprite();
    CCSprite *GetSprite() { return _sprite; }
//    void SetOpacity( unsigned int opacity );
    void Show();
    void Hide();
    
private:
    FadeInSprite();
    void Init( float dur, const char *filename, float delay, unsigned int finalOpacity );

private:
    CCSprite    *_sprite;
};
//--------------------------------------------------------------------------------
class NotificationSprite : public CCNode
{
public:
    static NotificationSprite *Create();
    ~NotificationSprite();

    virtual void Show();
    virtual void Hide();
    
    CCSprite *GetSprite() { return _sprite; }
    
protected:
    NotificationSprite();
    void Init();
    void Init( const char *filename );
    void CleanUpNode();
    
protected:
    CCSprite    *_sprite;
};
//--------------------------------------------------------------------------------
class RequestNotifySprite : public NotificationSprite, public WWWToolListener
{
public:
    static RequestNotifySprite *Create( const char *url, bool recheckEnable = true );
    ~RequestNotifySprite();
    CCSprite *GetSprite() { return _sprite; }
    
    virtual void Http_FailedWithError( void *connection, int error );
    virtual void Http_FinishedLoading( void *connection, const void *data, int len );
    void SetRecheckEnabled( bool enabled );
    
    virtual void Show();
    virtual void Hide();
    
private:
    typedef enum
    {
        eState_Idle = 0,
        eState_HTTPGet,
        eState_Exiting
    } State;
    
    typedef enum
    {
        eHidden = 0,
        eShown
    } ShownState;

protected:
    RequestNotifySprite( const char *url, bool recheckEnable );
    
    void Init();
    void ScheduleCheck();
    void Check();
    void Update( ShownState shown );
    
protected:
    State       _state;
    ShownState  _showState;
    string      _checkUrl;
    bool        _recheckEnabled;
};
//--------------------------------------------------------------------------------
class AnyRequestNotify : public RequestNotifySprite
{
public:
    static AnyRequestNotify *Create();
    void Step(ccTime time);
    
private:
    void Init();
    AnyRequestNotify( const char *url );
    
private:
    bool _initialized;
    ccTime  _dTime;
};

#endif
