#include "CustomNodes.h"
#include "CustomSprites.h"
#include "TrippleSprite.h"
#include "FXMenuItem.h"
#include "Debug/MyDebug.h"
#include "Platform/Lang.h"
#include "Levels/LevelMenu.h"
#include "Scenes/MainMenuScene.h"
#include "RunLogger/RunLogger.h"
#include "RunLogger/GLogger.h"
#include "MessageBox/MessageBox.h"
#include "Game.h"
#include "GameStatus.h"
#include "Platform/ToolBox.h"
#include "Facebook/FBLoginNode.h"

//---------------------------------------------------------------
// Node Align
//---------------------------------------------------------------
NodeAlign* NodeAlign::Create()
{
    NodeAlign *node;
    node = new NodeAlign();
    if ( node )
    {
        node->autorelease();
        node->Init();
        return node;
    }
    return NULL;
}
//---------------------------------------------------------------
NodeAlign::NodeAlign()
{
}
//---------------------------------------------------------------
NodeAlign::~NodeAlign()
{
}
//---------------------------------------------------------------
void NodeAlign::Init()
{
    
}
//---------------------------------------------------------------
void NodeAlign::AddNode( CCNode* node )
{
    _nodes.push_back( node );
    addChild( node );
}
//---------------------------------------------------------------
void NodeAlign::RemoveAllNodes()
{
    _nodes.clear();
}
//---------------------------------------------------------------
void NodeAlign::AlignHorizontal( float padding )
{
    _padding = padding;
}
//---------------------------------------------------------------
void NodeAlign::AlignVertical( float padding )
{
    _padding = padding;
    
    float x;
    float y;
    int index;
    
    x = 0.0f;
    y = 0.0f;
    index = 0;
    
    for ( Nodes::iterator it = _nodes.begin(); it != _nodes.end(); it++ )
    {
//        (*it)->setPosition( CCPoint( x, y - index * padding  - _componentSize.height / 2.0f + 320.0f ));
        (*it)->setPosition( CCPoint( x, y - index * padding  ));
        index++;
    }
}
//---------------------------------------------------------------
float NodeAlign::GetPadding()
{
    return _padding;
}



//---------------------------------------------------------------
// Language Menu
//---------------------------------------------------------------
LanguageMenu* LanguageMenu::Create()
{
    LanguageMenu *node;
    node = new LanguageMenu();
    if ( node )
    {
        node->autorelease();
        node->Init();
        return node;
    }
    return NULL;
}
//---------------------------------------------------------------
LanguageMenu::LanguageMenu()
{
}
//---------------------------------------------------------------
LanguageMenu::~LanguageMenu()
{
}
//---------------------------------------------------------------
void LanguageMenu::Init()
{
    float y;
    y = 0;
    
    _langCodes.push_back( LangData( kLanguageEnglish, "us" ));
    _langCodes.push_back( LangData( kLanguageRussian, "ru" ));
    _langCodes.push_back( LangData( kLanguageSpanish, "es" ));
    _langCodes.push_back( LangData( kLanguageItalian, "it" ));
    _langCodes.push_back( LangData( kLanguageGerman,  "de" ));
    _langCodes.push_back( LangData( kLanguageChineseS,"zn" ));
//    _langCodes.push_back( LangData( kLanguageJapanese,"jp" ));
    
    SortLangaugaes();

    _resetButton = GetMenuItem("reset game", &LanguageMenu::Menu_ResetGame, true );
    
    CCMenu *menu;
    menu = CCMenu::menuWithItems( NULL, NULL );
    menu->addChild( GetToggleButton(), 10 );
    menu->addChild( _resetButton , 10 );
    menu->setPosition( CCPoint( 0.0f, 60.0f ));
    menu->alignItemsVerticallyWithPadding( 15.0f );
    addChild( menu, 40 );
    
    MusicMenu *musicMenu;
    musicMenu = MusicMenu::Create();
    musicMenu->setPosition( CCPoint( 0.0f, -180.0f ));
    addChild( musicMenu, 40 );
    
    CCMenu *backToMain;
    backToMain = ConstructMainMenu();
    addChild( backToMain, 40 );

//    CCMenu *emailMenu;
//    emailMenu = ConstructEmailMenu();
//    emailMenu->setPosition( CCPoint( -120.0f, -180.0f ));
//    addChild( emailMenu, 40 );
    
    //-----------------------
    // Facebook menu
//    FBLoginNode *fbLogin;
//    
//    fbLogin = FBLoginNode::Create();
//    fbLogin->setPosition(CCPoint( -270.0f, -180.0f ));
//    addChild( fbLogin, 10 );
    
    CCSprite *background;
    background = CCSprite::spriteWithFile( "Images/Menus/SettingsMenu/background.jpg");
    addChild( background, 5 );
}
//---------------------------------------------------------------
void LanguageMenu::Menu_SelectLang( CCObject *sender )
{
    LanguageType lang;
    CCMenuItemToggle *item;
    CCMenuItem *subitem;
    
    item = (CCMenuItemToggle*)sender;
    subitem = item->getSubItems()->getObjectAtIndex( item->getSelectedIndex() );
    lang = (LanguageType)(long) subitem->getUserData();
    
    Game::Get()->GetGameStatus()->SetLang( lang );
       
    Refresh();
}
//---------------------------------------------------------------
void LanguageMenu::Menu_MainMenu( CCObject* sender )
{
	MainMenuScene::ShowScene();
}
//--------------------------------------------------------
void LanguageMenu::Menu_SendEmail( CCObject *sender )
{
    GLogEvent("Game", "Send email");
    ToolBox::ShowSendEmailView( "", "" );
}
//---------------------------------------------------------------
void LanguageMenu::Menu_ResetGame( CCObject *pSender )
{ 
    GLogEvent("Game", "Reset game");
    RLOG_S("RESET_GAME", "REQUEST");
    
	if ( MessageBox::CreateYesNoBox( LocalString( "ResetMsg"), LocalString( "ResetQuestion" )))
	{
        RLOG_S("RESET_GAME", "YES");
        
		GameStatus *gameStatus;
		gameStatus = Game::Get()->GetGameStatus();
		gameStatus->ResetGameStatus();
        MessageBox::CreateInfoBox( LocalString( "ResetDoneMsg" ), "");
	}
    else
    {
        RLOG_S("RESET_GAME", "NO");
    }
}
//---------------------------------------------------------------
CCMenuItemToggle* LanguageMenu::GetToggleButton()
{
	FXMenuItemToggle *toggleButton;
    toggleButton = NULL;
    
    for ( LangCodes::iterator it = _langCodes.begin(); it != _langCodes.end(); it++ )
    {
        D_LOG("%d %s", it->first, it->second.c_str() );
        
        //----------------
        string flagFile;
        TrippleSprite flagSprite;     
        CCMenuItemSprite *menuItem;
        
        //-----------------
        // Main menu button
        flagFile.append( "Images/Menus/SettingsMenu/Flags/" );
        flagFile.append( it->second );
        flagFile.append( "Flag.png" );


        flagSprite = TrippleSprite(flagFile.c_str());

        menuItem = FXMenuItemSpriteWithAnim::Create(
                                                          flagSprite,
                                                          this,
                                                          menu_selector( LanguageMenu::Menu_SelectLang ));
        menuItem->setUserData((void*) it->first );
        
        if ( ! toggleButton )
            toggleButton = FXMenuItemToggle::itemWithTarget(this,
                                                            menu_selector( LanguageMenu::Menu_SelectLang ),
                                                            menuItem,
                                                            NULL );
        else
            toggleButton->addSubItem( menuItem );
    }

    return toggleButton;
}
//---------------------------------------------------------------
CCMenuItemSprite* LanguageMenu::GetMenuItem( const char *buttonName, menuFnPtr fn, bool anim )
{
    string buttonFile;
    string buttonSelectedFile;

    buttonFile.append( "Images/Menus/MainMenu/" );
    buttonFile.append( buttonName );
    buttonFile.append( "Bt.png" );

    buttonSelectedFile.append( "Images/Menus/MainMenu/" );
    buttonSelectedFile.append( buttonName );
    buttonSelectedFile.append( "Bt.png" );

    buttonFile = string( Lang::Get()->GetLang( buttonFile.c_str() ));
    buttonSelectedFile = string( Lang::Get()->GetLang( buttonSelectedFile.c_str() ));
    D_LOG("%s button", buttonFile.c_str() );
    
    CCMenuItemSprite *menuItem;
    CCSprite* spriteNormal   = CCSprite::spriteWithFile(  buttonFile.c_str() );
    CCSprite* spriteSelected = CCSprite::spriteWithFile(  buttonSelectedFile.c_str() );
    CCSprite* spriteDisabled = CCSprite::spriteWithFile(  buttonFile.c_str() );

    if ( anim )
        menuItem = FXMenuItemSpriteWithAnim::Create( spriteNormal, spriteSelected, spriteDisabled, this, (SEL_MenuHandler)( fn ));
    else
        menuItem = FXMenuItemSprite::itemFromNormalSprite( spriteNormal, spriteSelected, spriteDisabled, this, (SEL_MenuHandler)( fn ));


    return menuItem;
}
//---------------------------------------------------------------
CCMenu* LanguageMenu::ConstructMainMenu()
{
	CCMenu* menu;
	TrippleSprite triSprite;
	string spriteFile;

	spriteFile.append( "Images/Menus/SettingsMenu/mainMenu.png" );
	triSprite = TrippleSprite( spriteFile.c_str());

	FXMenuItemSpriteWithAnim *menuItem;
	menuItem = FXMenuItemSpriteWithAnim::Create( triSprite,
                                                              this,
                                                              menu_selector( LanguageMenu::Menu_MainMenu ));

	menu = CCMenu::menuWithItems( menuItem, NULL );
	menu->alignItemsHorizontallyWithPadding( 0.0f );
	menu->setPosition( Config::GameSize.width / 2.0f - 50.0f, Config::GameSize.height / 2.0f - 60.0f );
	return menu;
}
//---------------------------------------------------------------
CCMenu* LanguageMenu::ConstructEmailMenu()
{
    CCMenu* menu;
	TrippleSprite triSprite;
	string spriteFile;

	spriteFile.append( "Images/Menus/SettingsMenu/btEmail.png" );
	triSprite = TrippleSprite( spriteFile.c_str());

	FXMenuItemSpriteWithAnim *menuItem;
	menuItem = FXMenuItemSpriteWithAnim::Create( triSprite,
                                                              this,
                                                              menu_selector( LanguageMenu::Menu_SendEmail ));

	menu = CCMenu::menuWithItems( menuItem, NULL );
	menu->alignItemsHorizontallyWithPadding( 0.0f );
	return menu;
}
//---------------------------------------------------------------
void LanguageMenu::Refresh()
{
    float dur;
    dur = 0.2f;
    dur = dur * ((float) _resetButton->getOpacity()) / 255.0f;

    _resetButton->stopAllActions();
    CCActionInterval* refreshSeq;
	refreshSeq = (CCActionInterval*)(CCSequence::actions(
                                                         CCFadeTo::actionWithDuration( dur, 0 ),
                                                         CCCallFunc::actionWithTarget( this, callfunc_selector( LanguageMenu::Refresh2Step )),
    
                                                         NULL ));
    _resetButton->runAction(refreshSeq);
}
//---------------------------------------------------------------
void LanguageMenu::Refresh2Step()
{
    CCPoint pos;
    CCMenu *parent;
    
    pos = _resetButton->getPosition();
    parent = (CCMenu*)_resetButton->getParent();
    
    _resetButton->removeFromParentAndCleanup( true );
    _resetButton = GetMenuItem("reset game", &LanguageMenu::Menu_ResetGame, true );
    _resetButton->setPosition(pos);
    _resetButton->setOpacity( 0 );
    parent->addChild(_resetButton, 10);
    
    _resetButton->runAction( CCFadeTo::actionWithDuration( 0.2f, 255 ));
}
//---------------------------------------------------------------
void LanguageMenu::SortLangaugaes()
{
    LangData langData;
    LanguageType lang;
    bool langFound;
    
    langFound = false;
    lang = Game::Get()->GetGameStatus()->GetLang();
    
    for ( LangCodes::iterator it = _langCodes.begin(); it != _langCodes.end(); it++ )
    {
        if ( it->first == lang )
        {
            langData = *it;
            _langCodes.erase( it );
            langFound = true;
            break;
        }
    }
    
    if ( langFound )
        _langCodes.insert( _langCodes.begin(), langData );
}
//---------------------------------------------------------------
