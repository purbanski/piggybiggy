#ifndef __piggy_biggy__IntroPointer__
#define __piggy_biggy__IntroPointer__
//---------------------------------------------------------------
#include <cocos2d.h>
//---------------------------------------------------------------
USING_NS_CC;
//---------------------------------------------------------------
class IntroPointer : public CCNode
{
public:
    static IntroPointer* Create();
    ~IntroPointer();
    void SetPosition( CCTouch* touch );
    void Hide();
    
private:
    IntroPointer();
    void EndingUp();
    
private:
    CCParticleSystemQuad *_emitter;
    CCSprite            *_background;
};
//---------------------------------------------------------------
#endif

