#include "CustomSprites.h"
#include "Skins.h"
#include "Debug/MyDebug.h"
#include <algorithm>
#include "Platform/ToolBox.h"
#include "Platform/ScoreTool.h"
#include "Facebook/FBTool.h"
#include "GameConfig.h"
#include "Game.h"
#include "Animation/AnimTools.h"
#include "Platform/Lang.h"
#include "Tools.h"

//----------------------------------------------------------------------------------
// Bank Save Wheel Sprite
//----------------------------------------------------------------------------------
BankSaveWheelSprite* BankSaveWheelSprite::Create( const char *filename )
{
	BankSaveWheelSprite *sprite = new BankSaveWheelSprite();
	if ( sprite && sprite->initWithFile( filename ))
	{
		sprite->autorelease();
		sprite->Init();
		return sprite;
	}
	CC_SAFE_DELETE(sprite);
	return NULL;
}
//----------------------------------------------------------------------------------
BankSaveWheelSprite::BankSaveWheelSprite()
{
}
//----------------------------------------------------------------------------------
BankSaveWheelSprite::~BankSaveWheelSprite()
{
	stopAllActions();
	removeAllChildrenWithCleanup( true );
}
//----------------------------------------------------------------------------------
void BankSaveWheelSprite::setPosition( const CCPoint& pos )
{
	CCSprite::setPosition( pos );
	CCPoint newPos;

	newPos.x = getContentSizeInPixels().width / 2.0f ;
	newPos.y = getContentSizeInPixels().height / 2.0f ;

	_raySprite->setPosition( newPos );
}
//----------------------------------------------------------------------------------
void BankSaveWheelSprite::setRotation( float rotation )
{
	CCSprite::setRotation( rotation );
	_raySprite->setRotation( -rotation );
}
//----------------------------------------------------------------------------------
void BankSaveWheelSprite::Init()
{
	_raySprite = CCSprite::spriteWithFile( Skins::GetSkinName( "Images/Blocks/BankSaveWheelRay.png" ) );
	addChild( _raySprite, 10 );
}

//----------------------------------------------------------------------------------



//----------------------------------------------------------------------------------
// Reel Sprite
//----------------------------------------------------------------------------------
ReelSprite* ReelSprite::Create( const char *filename )
{
	ReelSprite *sprite = new ReelSprite();
	if ( sprite && sprite->initWithFile( filename ))
	{
		sprite->autorelease();
		sprite->Init();
		return sprite;
	}
	CC_SAFE_DELETE(sprite);
	return NULL;
}
//----------------------------------------------------------------------------------
ReelSprite::ReelSprite()
{
}
//----------------------------------------------------------------------------------
void ReelSprite::setPosition( const CCPoint& pos )
{
	CCSprite::setPosition( pos );
	CCPoint newPos;

	newPos.x = getContentSizeInPixels().width / 2.0f ;
	newPos.y = getContentSizeInPixels().height / 2.0f ;

	_raySprite->setPosition( newPos );
}
//----------------------------------------------------------------------------------
void ReelSprite::setRotation( float rotation )
{
	CCSprite::setRotation( rotation );
	_raySprite->setRotation( -rotation );
}
//----------------------------------------------------------------------------------
void ReelSprite::Init()
{
	_raySprite = CCSprite::spriteWithFile( Skins::GetSkinName( "Images/Blocks/ReelRay150.png" ) );
	addChild( _raySprite, 10 );
}
//----------------------------------------------------------------------------------
ReelSprite::~ReelSprite()
{
	stopAllActions();
	removeAllChildrenWithCleanup( true );
}
//----------------------------------------------------------------------------------




//----------------------------------------------------------------------------------
// Sprite With Ray
//----------------------------------------------------------------------------------
SpriteWithRay* SpriteWithRay::Create( CCSprite *sprite, float radius, const char *rayFile )
{

	SpriteWithRay *spriteWithRay = new SpriteWithRay();
	if ( spriteWithRay && spriteWithRay->initWithTexture( sprite->getTexture() ))
	{
		spriteWithRay->autorelease();
		spriteWithRay->Init( rayFile, radius );
		spriteWithRay->setScale( sprite->getScale() );
		return spriteWithRay;
	}
	CC_SAFE_DELETE( spriteWithRay );
	return NULL;
}
//----------------------------------------------------------------------------------
SpriteWithRay::SpriteWithRay()
{
}
//----------------------------------------------------------------------------------
SpriteWithRay::~SpriteWithRay()
{
	stopAllActions();
	removeAllChildrenWithCleanup( true );
}
//----------------------------------------------------------------------------------
void SpriteWithRay::setPosition( const CCPoint& pos )
{
	CCSprite::setPosition( pos );
	CCPoint newPos;

	newPos.x = getContentSizeInPixels().width / 2.0f ;
	newPos.y = getContentSizeInPixels().height / 2.0f ;

	_raySprite->setPosition( newPos );
}
//----------------------------------------------------------------------------------
void SpriteWithRay::setRotation( float rotation )
{
	CCSprite::setRotation( rotation );
	_raySprite->setRotation( -rotation );
}
//----------------------------------------------------------------------------------
void SpriteWithRay::Init( const char *rayFile, float radius )
{
	string filename;
	string filenameDefault;
	stringstream ss;
	float i;
	
	i =  ( radius * 10.0f );

	ss << "Images/Blocks/" << rayFile << i << ".png";
	filename.append( Skins::GetSkinName( + ss.str().c_str() ));

	ss.str("");
	ss << "Images/Blocks/" << rayFile << ".png";
	filenameDefault.append( Skins::GetSkinName( ss.str().c_str() ));

	D_LOG( "Looking for: %s", filename.c_str() )
	if ( unResourceFileCheckExists( filename.c_str() ) )
	{
		DS_LOG_MISSING_PNG( "+ %s%d.png", rayFile, (int) ( radius * 10.0f ))
		_raySprite = CCSprite::spriteWithFile( filename.c_str() );
	}
	else
	{
		D_LOG( "Missing: %s", filename.c_str() )
		DS_LOG_MISSING_PNG( "- %s%d.png", rayFile, (int) ( radius * 10.0f ))
		_raySprite = CCSprite::spriteWithFile( filenameDefault.c_str() );

#ifdef DEBUG_MEGA
		unAssertMsg(CircleBase, false, ("Missing: %s", filename.c_str() ));
#endif
	}
	if ( _raySprite )
		addChild( _raySprite, 10 );
}
//----------------------------------------------------------------------------------
CCSprite * SpriteWithRay::GetRaySprite()
{
	return _raySprite;
}
//----------------------------------------------------------------------------------
void SpriteWithRay::setOpacity( GLubyte var )
{

	CCSprite::setOpacity( var );
	_raySprite->setOpacity( var );
}

//----------------------------------------------------------------------------------



//----------------------------------------------------------------------------------
// CoinSprite
//----------------------------------------------------------------------------------
CCSprite* CoinSprite::Create( puBlock *block )
{

	CoinSprite *coinSprite = new CoinSprite();
	
	// CCSprite *coinSprite = SpriteWithRay::Create( sprite, radius, "CoinRay" );
	if ( coinSprite && coinSprite->initWithFile( "Images/Other/none.png" ) )
	{
		coinSprite->Init( block );
		return coinSprite;
	}
	CC_SAFE_DELETE( coinSprite );
	return NULL;
}
//----------------------------------------------------------------------------------
CoinSprite::CoinSprite()
{
}
//----------------------------------------------------------------------------------
CoinSprite::~CoinSprite()
{

	if ( _flashSprite )
		_flashSprite->stopAllActions();

	stopAllActions();
	removeAllChildrenWithCleanup( true );
}
//----------------------------------------------------------------------------------
void CoinSprite::setPosition( const CCPoint& pos )
{

	CCSprite::setPosition( pos );
	CCPoint newPos;

	newPos.x = getContentSizeInPixels().width / 2.0f ;
	newPos.y = getContentSizeInPixels().height / 2.0f ;

	_flashSprite->setPosition( newPos );
	_coinSprite->setPosition( newPos );
}
//----------------------------------------------------------------------------------
void CoinSprite::setRotation( float rotation )
{

	CCSprite::setRotation( rotation );
	_flashSprite->setRotation( -rotation );
}
//----------------------------------------------------------------------------------
void CoinSprite::Init( puBlock *block  )
{

	float scale;
	string rayFile;

	rayFile.append( block->GetClassName2() );
	rayFile.append( "Ray" );

	scale = block->GetWidth() / 2.0f / 6.0f; /* CoinFlash anim frame size */

	_flashSprite = CCSprite::spriteWithFile("Images/Other/none.png");
	_flashSprite->setScale( scale );
	RunFlashAnim();

	_coinSprite = SpriteWithRay::Create( block->GetSprite(), block->GetWidth() / 2.0f, rayFile.c_str() );

	addChild( _coinSprite, 10 );
	addChild( _flashSprite, 10 );
}
//----------------------------------------------------------------------------------
void CoinSprite::RunFlashAnim()
{

	CCAnimate *animation;
	CCActionInterval* animSeq;
	float randDelay;

	randDelay = ((float) Tools::Rand( 300, 1000 )) / 100.0F;
	animation = AnimTools::Get()->GetAnimation( "CoinFlash", 1.0f / 24.0f );

	animSeq = (CCActionInterval*)(
		CCSequence::actions( 
		CCDelayTime::actionWithDuration( randDelay ),
		animation,
		CCCallFunc::actionWithTarget( this, callfunc_selector( CoinSprite::RunFlashAnim )),
		NULL ));

	_flashSprite->runAction( animSeq );
}
//----------------------------------------------------------------------------------
void CoinSprite::setOpacity( GLubyte var )
{
	CCSprite::setOpacity( var );
	_flashSprite->setOpacity( var );
	_coinSprite->setOpacity( var );
}

//----------------------------------------------------------------------------------


//----------------------------------------------------------------------------------
// Pocket Lock Sprite
//----------------------------------------------------------------------------------
BankSaveSprite* BankSaveSprite::Create()
{

	BankSaveSprite *ret = new BankSaveSprite();
	if ( ret && ret->initWithFile( "Images/Other/none.png" ))
	{
		ret->autorelease();
		ret->Init();
		return ret;
	}
	CC_SAFE_DELETE(ret);
	return NULL;
}
//----------------------------------------------------------------------------------
BankSaveSprite::BankSaveSprite()
{
	_callbackFn = NULL;
	_bank = NULL;
}
//----------------------------------------------------------------------------------
void BankSaveSprite::setPosition( const CCPoint& pos )
{
	CCSprite::setPosition( pos );
}
//----------------------------------------------------------------------------------
void BankSaveSprite::Init()
{

	_openWheelSprite = CCSprite::spriteWithFile( Skins::GetSkinName( "Images/Blocks/BankSaveOpenWheel.png" ) );
	_lockWheelSprite = CCSprite::spriteWithFile( Skins::GetSkinName( "Images/Blocks/BankSaveLockWheel.png" ) );
	_bankSaveSprite = CCSprite::spriteWithFile( Skins::GetSkinName( "Images/Blocks/BankSave.png" ) );

	float x;
	float y;

	x = _bankSaveSprite->getContentSizeInPixels().width / 2.0f;
	y = _bankSaveSprite->getContentSizeInPixels().height / 2.0f;

	_openWheelSprite->setPosition( CCPoint( x, y ));
	_lockWheelSprite->setPosition( CCPoint( x + 55.0f, y + 0.0f ));
	_bankSaveSprite->setPosition( CCPoint( 0.0f, 0.0f ));

	_bankSaveSprite->addChild( _lockWheelSprite, 10 );
	_bankSaveSprite->addChild( _openWheelSprite, 15 );
	addChild( _bankSaveSprite, 5 );
}
//----------------------------------------------------------------------------------
void BankSaveSprite::StartAnim( FuncPtr fn, puBank *bank )
{

	_callbackFn = fn;
	_bank = bank;

	CCFiniteTimeAction*  actionLockWheel = CCSequence::actions(
		CCMoveBy::actionWithDuration( 0.5f, CCPointZero ),
		CCCallFunc::actionWithTarget( this, callfunc_selector( BankSaveSprite::PlaySmallWheelSound )), 
		CCRotateBy::actionWithDuration( 1.2f, 360.0f ),
		CCCallFunc::actionWithTarget( this, callfunc_selector( BankSaveSprite::PlaySmallWheelSound )), 
		CCMoveBy::actionWithDuration( 0.2f, CCPointZero ),
		CCRotateBy::actionWithDuration( 0.65f, -150.0f ),
		CCCallFunc::actionWithTarget( this, callfunc_selector( BankSaveSprite::PlaySmallWheelSound )), 
		CCMoveBy::actionWithDuration( 0.2f, CCPointZero ),
		CCRotateBy::actionWithDuration( 0.9f, 220.0f ),
		NULL );


	CCFiniteTimeAction*  actionOpenWheel = CCSequence::actions(
		CCMoveBy::actionWithDuration( 0.5f + 1.2f + 0.2f + 0.65f + 0.2f + 0.9f, CCPointZero ),
		CCCallFunc::actionWithTarget( this, callfunc_selector( BankSaveSprite::PlayBigWheelSound )), 
		CCRotateBy::actionWithDuration( 0.1f, 5.0f ),
		CCRotateBy::actionWithDuration( 0.1f, 10.0f ),
		CCRotateBy::actionWithDuration( 0.1f, 20.0f ),
		CCRotateBy::actionWithDuration( 0.1f, 30.0f ),
		CCRotateBy::actionWithDuration( 2.0f, 720.0f ),
		CCRotateBy::actionWithDuration( 2.0f, 720.0f ),
		NULL );

	CCFiniteTimeAction*  actionBankSave = CCSequence::actions(
		CCMoveBy::actionWithDuration( 0.5f + 1.2f + 0.2f + 0.65f + 0.2f + 0.9f + 0.5f, CCPointZero ),
		CCCallFunc::actionWithTarget( this, callfunc_selector( BankSaveSprite::PlaySaveSlideUpSound )), 
		CCMoveBy::actionWithDuration( 3.0f, CCPoint( 0.0f, 280.0f )),
		NULL );

	CCFiniteTimeAction*  actionMe = CCSequence::actions(
		CCMoveBy::actionWithDuration( 0.5f + 1.2f + 0.2f + 0.65f + 0.2f + 0.9f + 0.5f, CCPointZero ),
		CCMoveBy::actionWithDuration( 0.20f, CCPointZero ),
		CCCallFunc::actionWithTarget( this, callfunc_selector( BankSaveSprite::AnimFinished )), 
		NULL );

	stopAllActions();
	_lockWheelSprite->stopAllActions();
	_openWheelSprite->stopAllActions();
	_bankSaveSprite->stopAllActions();

	runAction( actionMe );
	_lockWheelSprite->runAction( actionLockWheel );
	_openWheelSprite->runAction( actionOpenWheel );
	_bankSaveSprite->runAction( actionBankSave );
}
//----------------------------------------------------------------------------------
void BankSaveSprite::PlaySmallWheelSound()
{
	SoundEngine::Get()->PlayEffect( SoundEngine::eSoundBankSaveSmallWheel );
}
//----------------------------------------------------------------------------------
void BankSaveSprite::AnimFinished()
{
	_callbackFn( _bank );
}
//----------------------------------------------------------------------------------
void BankSaveSprite::PlayBigWheelSound()
{
	SoundEngine::Get()->PlayEffect( SoundEngine::eSoundBankSaveBigWheel );
}
//----------------------------------------------------------------------------------
void BankSaveSprite::PlaySaveSlideUpSound()
{
	SoundEngine::Get()->PlayEffect( SoundEngine::eSoundBankSaveSlideUp );
}
//----------------------------------------------------------------------------------
BankSaveSprite::~BankSaveSprite()
{


	stopAllActions();
	removeAllChildrenWithCleanup( true );
}
//----------------------------------------------------------------------------------




//----------------------------------------------------------------------------------
// Glued Circle sprite
//----------------------------------------------------------------------------------
GlueCircleSprite* GlueCircleSprite::Create( float radius )
{

	stringstream filename;
	GlueCircleSprite *glueSprite;
	
	filename << "Images/Blocks/Glued" ;
	filename << (int)( radius * 10.0f ) ;
	filename << ".png" ;

	glueSprite = new GlueCircleSprite( );
	if ( unResourceFileCheckExists( Skins::GetSkinName( filename.str().c_str() ) ))
	{
		if ( glueSprite && glueSprite->initWithFile( Skins::GetSkinName( filename.str().c_str() ) )) 
		{
			glueSprite->autorelease();
			glueSprite->ApplyRandRotation();

			return glueSprite;
		}
	}
	else if ( glueSprite && glueSprite->initWithFile( Skins::GetSkinName( "Images/Blocks/Glued.png" ) )) 
	{
		DS_LOG_MISSING_PNG( "- Glued%d.png", (int) ( radius * 10.0f ))
		D_LOG( "Missing: Glued%d.png", (int) ( radius * 10.0f ))

		glueSprite->autorelease();
		glueSprite->ApplyRandRotation();
		glueSprite->ApplyScale( radius );

		return glueSprite;
	}
	
	CC_SAFE_DELETE( glueSprite );
	return NULL;
}

//----------------------------------------------------------------------------------
GlueCircleSprite::GlueCircleSprite()
{
}
//----------------------------------------------------------------------------------
GlueCircleSprite::~GlueCircleSprite()
{
	stopAllActions();
	removeAllChildrenWithCleanup( true );
}
//----------------------------------------------------------------------------------
void GlueCircleSprite::ApplyScale( float radius )
{

	float scale;
	scale = ( radius * 2.0f * RATIO + Config::SpriteGlueOutBorder ) / getContentSizeInPixels().width ;
	setScale( scale );
}
//----------------------------------------------------------------------------------
void GlueCircleSprite::ApplyRandRotation()
{
	setRotation( (float) ( rand() % 360 ));
}

//----------------------------------------------------------------------------------




//----------------------------------------------------------------------------------
// Glued Box sprite
//----------------------------------------------------------------------------------
GlueBoxSprite* GlueBoxSprite::Create( float width, float height )
{
	stringstream file;
	file << "Images/Blocks/Glued" ;
	file << (int) ( width * RATIO ) << "x" << (int)( height * RATIO ) ;
	file << ".png";
	
	GlueBoxSprite *glueSprite;

	if ( ! unResourceFileCheckExists( Skins::GetSkinName( file.str().c_str() ) ))
	{
		DS_LOG_MISSING_PNG( "- Glued%dx%d.png", (int) ( width * RATIO ), (int)( height * RATIO ))
		D_LOG("Missing glue: %s", file.str().c_str() )

		glueSprite = new GlueBoxSprite();
		if ( glueSprite && glueSprite->initWithFile( Skins::GetSkinName( "Images/Blocks/Glued.png" ) )) 
		{
			glueSprite->autorelease();
			glueSprite->Init( width, height );
			glueSprite->RandRotation();
			return glueSprite;
		}
	}
	else
	{
		DS_LOG_MISSING_PNG( "+ Glued%dx%d.png", (int) ( width * RATIO ), (int)( height * RATIO ))

		glueSprite = new GlueBoxSprite();
		if ( glueSprite && glueSprite->initWithFile( Skins::GetSkinName( file.str().c_str() ) )) 
		{
			glueSprite->autorelease();
			glueSprite->RandRotation();
			return glueSprite;
		}
	}
	
	CC_SAFE_DELETE( glueSprite );
	return NULL;
}

//----------------------------------------------------------------------------------
GlueBoxSprite::GlueBoxSprite()
{
}
//----------------------------------------------------------------------------------
void GlueBoxSprite::Init( float width, float height )
{
	float scale;
	float min1;
	float min2;

	min1 = min( width, height );
	min2 = min( getContentSizeInPixels().width, getContentSizeInPixels().height );

	min1  = ( width + height ) /2.0f;
	min2 = (getContentSizeInPixels().width + getContentSizeInPixels().height ) / 2.0f;

	scale = ( min1 * RATIO + Config::SpriteGlueOutBorder ) / min2;
	setScale( scale );
}
//----------------------------------------------------------------------------------
GlueBoxSprite::~GlueBoxSprite()
{
	stopAllActions();
	removeAllChildrenWithCleanup( true );
}
//----------------------------------------------------------------------------------
void GlueBoxSprite::RandRotation()
{
	setRotation( (float) ( rand() % 360 ));
}
//----------------------------------------------------------------------------------





//----------------------------------------------------------------------------------
// Moved Sprite
//----------------------------------------------------------------------------------
MovedSprite* MovedSprite::Create( float x, float y, float rotation, const char *filename )
{
	MovedSprite *sprite = new MovedSprite( x, y, rotation );
	if ( sprite && sprite->initWithFile( Skins::GetSkinName( filename ) ))
	{
		sprite->autorelease();
		sprite->Init();
		return sprite;
	}
	CC_SAFE_DELETE(sprite);
	return NULL;
}
//----------------------------------------------------------------------------------
MovedSprite::MovedSprite( float x, float y, float rotation )
{
	_xMove = x;
	_yMove = y;
	_rotation = rotation;
}
//----------------------------------------------------------------------------------
void MovedSprite::Init()
{
}
//----------------------------------------------------------------------------------
void MovedSprite::setPosition( const CCPoint& pos )
{
	CCPoint newPos;
	newPos.x = pos.x + _xMove;
	newPos.y = pos.y + _yMove;

	CCSprite::setPosition( newPos );
}
//----------------------------------------------------------------------------------
void MovedSprite::setRotation( float rotation )
{
	CCSprite::setRotation( rotation );
}
//----------------------------------------------------------------------------------
MovedSprite::~MovedSprite()
{
	stopAllActions();
	//removeAllChildrenWithCleanup( true );
}

//----------------------------------------------------------------------------------




//----------------------------------------------------------------------------------
// Lift Door Sprite
//----------------------------------------------------------------------------------
LiftDoorSprite* LiftDoorSprite::Create()
{
	LiftDoorSprite *sprite = new LiftDoorSprite();
	if ( sprite && sprite->initWithFile( "Images/Other/none.png" ) )
	{
		sprite->autorelease();
		sprite->Init();
		return sprite;
	}
	CC_SAFE_DELETE(sprite);
	return NULL;

}
//----------------------------------------------------------------------------------
LiftDoorSprite::LiftDoorSprite()
{
}
//----------------------------------------------------------------------------------
void LiftDoorSprite::Init()
{
	_spriteGreen = CCSprite::spriteWithFile( Skins::GetSkinName( "Images/Blocks/LiftDoorGreen.png" ) );
	_spriteRed = CCSprite::spriteWithFile( Skins::GetSkinName( "Images/Blocks/LiftDoorRed.png" ) );
	
	addChild( _spriteGreen, 10 );
	addChild( _spriteRed, 10 );

	SetRed();
}
//----------------------------------------------------------------------------------
void LiftDoorSprite::SetRed()
{
	_spriteRed->setIsVisible( true );
	_spriteGreen->setIsVisible( false );
}
//----------------------------------------------------------------------------------
void LiftDoorSprite::SetGreen()
{
	_spriteGreen->setIsVisible( true );
	_spriteRed->setIsVisible( false );
}
//----------------------------------------------------------------------------------
void LiftDoorSprite::setPosition( const CCPoint& pos )
{
	_spriteRed->setPosition( pos );
	_spriteGreen->setPosition( pos );
}
//----------------------------------------------------------------------------------
void LiftDoorSprite::setRotation( float rotation )
{
	_spriteRed->setRotation( rotation );
	_spriteGreen->setRotation( rotation );
}
//----------------------------------------------------------------------------------
LiftDoorSprite::~LiftDoorSprite()
{
	stopAllActions();
	removeAllChildrenWithCleanup( true );
}
//----------------------------------------------------------------------------------



//----------------------------------------------------------------------------------
// FB Profile Sprite
//----------------------------------------------------------------------------------
FBProfileSprite* FBProfileSprite::Create()
{
	FBProfileSprite *sprite = new FBProfileSprite();
	if ( sprite && sprite->initWithFile( "Images/Facebook/fbDummyProfile.png" ))
	{
		sprite->autorelease();
		sprite->Init();
		return sprite;
	}
	CC_SAFE_DELETE(sprite);
	return NULL;
}
//----------------------------------------------------------------------------------
FBProfileSprite* FBProfileSprite::CreateBold( const char *fbId )
{
	FBProfileSprite *sprite = new FBProfileSprite();
	if ( sprite && sprite->initWithFile( "Images/Facebook/fbDummyProfile.png" ))
	{
		sprite->autorelease();
		sprite->Init( fbId, "Images/Facebook/fbYourFrame.png");
		return sprite;
	}
	CC_SAFE_DELETE(sprite);
	return NULL;
}
//----------------------------------------------------------------------------------
FBProfileSprite* FBProfileSprite::Create( const char *fbId )
{
	FBProfileSprite *sprite = new FBProfileSprite();
	if ( sprite && sprite->initWithFile( "Images/Facebook/fbDummyProfile.png" ))
	{
		sprite->autorelease();
		sprite->Init( fbId, "Images/Facebook/fbProfileFrame.png"  );
		return sprite;
	}
	CC_SAFE_DELETE(sprite);
	return NULL;
}
//----------------------------------------------------------------------------------
FBProfileSprite::FBProfileSprite()
{
    _fbProfileSprite = NULL;
    _processing = false;
}
//----------------------------------------------------------------------------------
void FBProfileSprite::Init( const char *userFbId, const char *fileframe )
{
    stringstream url;
    
    url << "https://graph.facebook.com/" << userFbId << "/picture?type=normal";
    WWWTool::Get()->HttpGet( url.str().c_str(), this );
    _processing = true;
    
    AddFrame( fileframe );
}
//----------------------------------------------------------------------------------
void FBProfileSprite::Init()
{
    AddFrame( "Images/Facebook/fbYourFrame.png" );
}
//----------------------------------------------------------------------------------
FBProfileSprite::~FBProfileSprite()
{
    if ( _processing )
        WWWTool::Get()->CancelHttpGet( this );
}
//----------------------------------------------------------------------------------
void FBProfileSprite::Http_FailedWithError( void *connection, int error )
{
    _processing = false;
}
//-------------------------------------------
void FBProfileSprite::Http_FinishedLoading( void *connection, const void *data, int len )
{
    _processing = false;
    _fbProfileSprite = ToolBox::SpriteFromData( data, len );
    ProfileSpriteSetup();
}
//-------------------------------------------
void FBProfileSprite::setPosition( CCPoint pos )
{
    CCSprite::setPosition( pos );
    if ( _fbProfileSprite )
        _fbProfileSprite->setPosition( pos );
}
//-------------------------------------------
void FBProfileSprite::ProfileSpriteSetup()
{
    _fbProfileSprite->setOpacity( 0 );
    _fbProfileSprite->setPosition( CCPoint( getContentSizeInPixels().width / 2.0f - 1.0f, getContentSizeInPixels().height / 2.0f - 1.0f));
    addChild( _fbProfileSprite, 5 );

    RescaleProfilePicture();
    
    _fbProfileSprite->runAction( CCFadeTo::actionWithDuration( 0.3f, 255 ));
    runAction( CCFadeTo::actionWithDuration( 0.3f, 0 ));
}
//-------------------------------------------
void FBProfileSprite::AddFrame( const char *framefile )
{
    _fbProfileFrameSprite = CCSprite::spriteWithFile( framefile );
    _fbProfileFrameSprite->setPosition( CCPoint( getContentSizeInPixels().width / 2.0f, getContentSizeInPixels().height / 2.0f ));
    _fbProfileFrameSprite->setOpacity( 0 );
    _fbProfileFrameSprite->runAction( CCFadeTo::actionWithDuration( 0.5f, 255 ));
    
    addChild( _fbProfileFrameSprite, 10 );
    
    setOpacity( 0 );
    runAction( CCFadeTo::actionWithDuration( 1.0f, 255 ));
}
//-------------------------------------------
void FBProfileSprite::RescaleProfilePicture()
{
   CCSize profileSize;
    profileSize = _fbProfileSprite->getContentSize();
    
    float maxSize = 65.0f;
    float scale;

    float smaller;
    if ( profileSize.width > profileSize.height )
        smaller = profileSize.height;
    else
        smaller = profileSize.width;
    
    scale = maxSize / smaller;
    _fbProfileSprite->setScale(scale);

    float xs;
    float ys;
    
    xs = ( _fbProfileSprite->getContentSize().width * scale - maxSize ) / 2.0f;
    ys = ( _fbProfileSprite->getContentSize().height * scale - maxSize ) / 2.0f;
    
    _fbProfileSprite->setTextureRect( CCRect( xs, ys, maxSize / scale, maxSize / scale ));
}
//-------------------------------------------




//----------------------------------------------------------------------------------
// FB Profile Sprite
//----------------------------------------------------------------------------------
float FBProfileBigSprite::_xFixDelta = 10.0f;
float FBProfileBigSprite::_yFixDelta = 10.0f;

FBProfileBigSprite* FBProfileBigSprite::Create()
{
	FBProfileBigSprite *sprite = new FBProfileBigSprite();
	if ( sprite && sprite->initWithFile( "Images/Other/none.png" ))
	{
		sprite->autorelease();
        sprite->AddDummyProfile();        
		sprite->Init();
		return sprite;
	}
	CC_SAFE_DELETE(sprite);
	return NULL;
}
//----------------------------------------------------------------------------------
FBProfileBigSprite* FBProfileBigSprite::Create( const char *fbId, const char *filename )
{
	FBProfileBigSprite *sprite = new FBProfileBigSprite();
	if ( sprite && sprite->initWithFile( "Images/Other/none.png" ))
	{
		sprite->autorelease();
        sprite->AddDummyProfile();        
		sprite->Init( fbId, filename );
		return sprite;
	}
	CC_SAFE_DELETE(sprite);
	return NULL;
}
//----------------------------------------------------------------------------------
FBProfileBigSprite* FBProfileBigSprite::CreateBold( const char *fbId )
{
    return Create( fbId, "Images/Facebook/fbYourFrame.png" );
}
//----------------------------------------------------------------------------------
FBProfileBigSprite* FBProfileBigSprite::Create( const char *fbId )
{
    return Create( fbId, "Images/Facebook/fbProfileBigFrame.png" );
}
//----------------------------------------------------------------------------------
FBProfileBigSprite* FBProfileBigSprite::Create( const char *fbId, GameLevelUsersType userType )
{
	FBProfileBigSprite *sprite = new FBProfileBigSprite();
	if ( sprite && sprite->initWithFile( "Images/Other/none.png" ))
	{
		sprite->autorelease();
        sprite->AddDummyProfile();
		sprite->Init( fbId, userType  );
		return sprite;
	}
	CC_SAFE_DELETE(sprite);
	return NULL;
}
//----------------------------------------------------------------------------------
FBProfileBigSprite::FBProfileBigSprite()
{
    _fbProfileSprite = NULL;
    _processing = false;
}
//----------------------------------------------------------------------------------
void FBProfileBigSprite::AddDummyProfile()
{
    CCSprite *dummy;
    dummy = CCSprite::spriteWithFile( "Images/Facebook/fbDummyProfileBig.png" );
    addChild( dummy, 1 );
    setContentSize( dummy->getContentSize() );
    
    dummy->setPosition( ccp(
                            dummy->getContentSize().width / 2.0f + _xFixDelta,
                            dummy->getContentSize().height / 2.0f + _yFixDelta ));
    dummy->setOpacity( 0 );
    dummy->runAction( CCFadeTo::actionWithDuration( 1.0f, 255 ));
}
//----------------------------------------------------------------------------------
void FBProfileBigSprite::Init( const char *userFbId, const char *fileframe )
{
    stringstream url;
    
    url << "https://graph.facebook.com/" << userFbId << "/picture?type=large";
    WWWTool::Get()->HttpGet( url.str().c_str(), this );
    _processing = true;
    
    AddFrame( fileframe );
}
//----------------------------------------------------------------------------------
void FBProfileBigSprite::Init( const char *userFbId, GameLevelUsersType userType )
{
    stringstream url;
    
    url << "https://graph.facebook.com/" << userFbId << "/picture?type=large";
    WWWTool::Get()->HttpGet( url.str().c_str(), this );
    _processing = true;
    
    stringstream frameFile;
    frameFile << "Images/Facebook/Frames/frame";
    
    switch ( userType)
    {
        case GameTypes::eGameUser_FriendLevelPassed :
            frameFile << "1";
            break;
            
        case GameTypes::eGameUser_FriendLevelNotPassed :
            frameFile << "1";
            break;
            
        case GameTypes::eGameUser_FriendGameNotInstalled :
            frameFile << "2";
            break;
            
        case GameTypes::eGameUser_UnknownPlayerLevelNotPassed :
            frameFile << "3";
            break;
            
        default:
            frameFile << "3";
            break;
    }
    
    frameFile << ".png";
    
    AddFrame( frameFile.str().c_str() );
}
//----------------------------------------------------------------------------------
void FBProfileBigSprite::Init()
{
    AddFrame( "Images/Facebook/fbYourFrame.png" );
}
//----------------------------------------------------------------------------------
FBProfileBigSprite::~FBProfileBigSprite()
{
    if ( _processing )
        WWWTool::Get()->CancelHttpGet( this );
}
//----------------------------------------------------------------------------------
void FBProfileBigSprite::Http_FailedWithError( void *connection, int error )
{
    _processing = false;
}
//-------------------------------------------
void FBProfileBigSprite::Http_FinishedLoading( void *connection, const void *data, int len )
{
    _processing = false;
    _fbProfileSprite = ToolBox::SpriteFromData( data, len );
    ProfileSpriteSetup();
}
//-------------------------------------------
void FBProfileBigSprite::setPosition( CCPoint pos )
{
    CCSprite::setPosition( pos );
    // if ( _fbProfileSprite )
      //  _fbProfileSprite->setPosition( pos );
}
//-------------------------------------------
void FBProfileBigSprite::ProfileSpriteSetup()
{
    _fbProfileSprite->setOpacity( 0 );
    _fbProfileSprite->setPosition( CCPoint(
                                           getContentSizeInPixels().width / 2.0f - 1.0f + _xFixDelta,
                                           getContentSizeInPixels().height / 2.0f - 1.0f + _yFixDelta
                                           ));
    addChild( _fbProfileSprite, 5 );

    RescaleProfilePicture();
    
    _fbProfileSprite->runAction( CCFadeTo::actionWithDuration( 0.3f, 255 ));
    runAction( CCFadeTo::actionWithDuration( 0.3f, 0 ));
}
//-------------------------------------------
void FBProfileBigSprite::AddFrame( const char *framefile )
{
    _fbProfileFrameSprite = CCSprite::spriteWithFile( framefile );
    _fbProfileFrameSprite->setPosition( CCPoint( getContentSizeInPixels().width / 2.0f + _xFixDelta,
                                                getContentSizeInPixels().height / 2.0f + _yFixDelta ));
    _fbProfileFrameSprite->setOpacity( 0 );
    _fbProfileFrameSprite->runAction( CCFadeTo::actionWithDuration( 0.5f, 255 ));
    
    addChild( _fbProfileFrameSprite, 10 );
    
    setOpacity( 0 );
}
//-------------------------------------------
void FBProfileBigSprite::RescaleProfilePicture()
{
    CCSize profileSize;
    profileSize = _fbProfileSprite->getContentSize();
    
    float maxSize = 96.0f;
    float scale;

    float smaller;
    if ( profileSize.width > profileSize.height )
        smaller = profileSize.height;
    else
        smaller = profileSize.width;
    
    scale = maxSize / smaller;
    _fbProfileSprite->setScale(scale);

    float xs;
    float ys;
    
    xs = ( _fbProfileSprite->getContentSize().width * scale - maxSize ) / 2.0f;
    ys = ( _fbProfileSprite->getContentSize().height * scale - maxSize ) / 2.0f;
    
    _fbProfileSprite->setTextureRect( CCRect( xs, ys, maxSize / scale, maxSize / scale ));
}
//-------------------------------------------




//----------------------------------------------------------------------------------
// FB User First Name
//----------------------------------------------------------------------------------
FBUserFirstNameNode* FBUserFirstNameNode::Create( const char *fbId )
{
    FBUserFirstNameNode* node;
    node = new FBUserFirstNameNode();
    if ( node )
    {
        node->autorelease();
        node->Init( fbId );
        return node;
    }
    return NULL;
}
//----------------------------------------------------------------------------------
FBUserFirstNameNode* FBUserFirstNameNode::CreateCenter( const char *fbId )
{
    return Create( fbId, CCSize( 300.0f, 30.0f ), CCTextAlignmentCenter );
}
//----------------------------------------------------------------------------------
FBUserFirstNameNode* FBUserFirstNameNode::Create( const char *fbId, const CCSize &dimision, CCTextAlignment textAligment )
{
    FBUserFirstNameNode* node;
    node = new FBUserFirstNameNode();
    if ( node )
    {
        node->autorelease();
        node->Init( fbId, dimision, textAligment );
        return node;
    }
    return NULL;
}
//----------------------------------------------------------------------------------
FBUserFirstNameNode::FBUserFirstNameNode()
{
    _processing = false;
    _label = NULL;
}
//----------------------------------------------------------------------------------
FBUserFirstNameNode::~FBUserFirstNameNode()
{
    if ( _processing )
        FBTool::Get()->CancelRequestForListener( this );
}
//----------------------------------------------------------------------------------
void FBUserFirstNameNode::Init( const char *fbId )
{
    _label = CCLabelTTF::labelWithString( "",  Config::FBHighScoreFont, 28 );
    InitCommon( fbId );
}
//----------------------------------------------------------------------------------
void FBUserFirstNameNode::Init( const char *fbId, const CCSize &dimision, CCTextAlignment textAligment )
{
   _label = CCLabelTTF::labelWithString(
                                         "",
                                         dimision,
                                         textAligment,
                                         Config::FBHighScoreFont,
                                         28
                                         );
    InitCommon( fbId );
}
//----------------------------------------------------------------------------------
void FBUserFirstNameNode::InitCommon( const char *fbId )
{
    _label->setOpacity( 0 );
    _label->setPosition( CCPoint( 0.0f, 0.0f ));
    addChild( _label );

    if ( ! FBTool::Get()->IsLogged() && strncmp( "0", fbId, strlen( fbId )))
    {
        _label->setString("........");
        _label->runAction( CCFadeTo::actionWithDuration( 0.3f, 255 ));
        return;
    }
    else if ( ! FBTool::Get()->IsLogged() && ! strncmp( "0", fbId, strlen( fbId )))
    {
        _label->setString( LocalString("You"));
        _label->runAction( CCFadeTo::actionWithDuration( 0.3f, 255 ));
        return;
    }
    
    FBTool::Get()->Read_UserFirstName( fbId, this );
    _processing = true;
}
//----------------------------------------------------------------------------------
void FBUserFirstNameNode::FBRequestCompleted( FBTool::Request requestType, void *data )
{
    _label->setString( (char*) data );
//    _label->setString("森田俊哉   Đỗ Tường Linh   ńś");
    _label->runAction( CCFadeTo::actionWithDuration( 0.3f, 255 ));
    _processing = false;
}
//----------------------------------------------------------------------------------
void FBUserFirstNameNode::FBRequestError( int error )
{
    _processing = false;
}
//----------------------------------------------------------------------------------
void FBUserFirstNameNode::SetColor( const ccColor3B &color )
{
    if ( _label )
        _label->setColor( color );   
}



//----------------------------------------------------------------------------------
// FB Coin Score Node
//----------------------------------------------------------------------------------
FBCoinScoreNode* FBCoinScoreNode::Create( int totalSeconds )
{
    FBCoinScoreNode* node;
    
    node = new FBCoinScoreNode();
    if ( node )
    {
        node->autorelease();
        node->Init( totalSeconds );
        return node;
    }
    return NULL;
}
//----------------------------------------------------------------------------------
FBCoinScoreNode::FBCoinScoreNode()
{
}
//----------------------------------------------------------------------------------
FBCoinScoreNode::~FBCoinScoreNode()
{
}
//----------------------------------------------------------------------------------
void FBCoinScoreNode::Init( int totalSeconds )
{
    string filename;
    int coinCount;
    float dx, x, y;
    CCSprite *coinSprite;
    
    x = 0.0f;
    y = 0.0f;
    dx = 20.0f;
    
    filename.append( "Images/Facebook/Highscore/");
    coinCount = ScoreTool::Get()->GetCoinCount( totalSeconds );
    
    switch ( coinCount )
    {
        case 0 :
            filename.append("Coin0.png");
            break;

        case 1 :
            filename.append("Coin1.png");
            break;

        case 2 :
            filename.append("Coin2.png");
            break;

        case 3 :
            filename.append("Coin3.png");
            break;
        
        case 4 :
            filename.append("Coin4.png");
            break;

        case 5 :
            filename.append("Coin5.png");
            break;

        default:
            filename.append("Coin0.png");
            break;
    }
    
    coinSprite = CCSprite::spriteWithFile( filename.c_str() );   
    coinSprite->setPosition( CCPoint( 0.0f, 0.0f ));
    coinSprite->setOpacity(0 );
    coinSprite->runAction( CCFadeTo::actionWithDuration( 1.0f, 255 ));
    addChild( coinSprite );
}
//----------------------------------------------------------------------------------


//----------------------------------------------------------------------------------
// Loading Anim
//----------------------------------------------------------------------------------
LoadingAnim* LoadingAnim::Create()
{
    LoadingAnim *node;
    node = new LoadingAnim();
//    if ( node )
    if ( node && node->initWithFile( "Images/Other/none.png" ) )
    {
        node->autorelease();
        node->Init();
        return node;
    }
    return NULL;
}
//----------------------------------------------------------------------------------
LoadingAnim::LoadingAnim()
{
}
//----------------------------------------------------------------------------------
LoadingAnim::~LoadingAnim()
{
   
}
//----------------------------------------------------------------------------------
void LoadingAnim::Init()
{
    RunAnim();
}
//----------------------------------------------------------------------------------
void LoadingAnim::RunAnim()
{
	CCAnimate *animation;
	CCActionInterval* animSeq;

	animation = AnimTools::Get()->GetAnimation( "LoadingAnim", 1.0f / 12.0f );
	animSeq = CCRepeatForever::actionWithAction( animation );
    
    setOpacity( 0 );
    
    runAction( CCFadeTo::actionWithDuration( 1.0f, 255 ));
	runAction( animSeq );
}
//----------------------------------------------------------------------------------
void LoadingAnim::Stop()
{
	CCAction *stopSeq;
    stopSeq = (CCActionInterval*)(
                                  CCSequence::actions(
                                                      CCFadeTo::actionWithDuration( 1.0f, 0 ),
                                                      CCCallFunc::actionWithTarget( this, callfunc_selector( LoadingAnim::StopFinal )),
                                                      NULL ));
    runAction( stopSeq );
}
//----------------------------------------------------------------------------------
void LoadingAnim::StopFinal()
{
    removeFromParentAndCleanup( true );
}
//----------------------------------------------------------------------------------
void LoadingAnim::ImediateCleanUp()
{
    stopAllActions();
    if ( getParent() )
        removeFromParentAndCleanup(true);
}
//----------------------------------------------------------------------------------
void LoadingAnim::SetPositionForFBRequestMenu()
{
    setPosition( ccp( Tools::GetScreenMiddleX(), Tools::GetScreenMiddleY() - 50.0f ));
}




//---------------------------------------------------------------
// FB Login Label
//---------------------------------------------------------------
float FBLoginLabel::FadeDur = 0.65f;
//---------------------------------------------------------------
FBLoginLabel* FBLoginLabel::Create()
{
    FBLoginLabel *node;
    node = new FBLoginLabel();

    if ( node && node->initWithFile( "Images/Other/none.png" ) )
    {
        node->autorelease();
        node->Init();
        return node;
    }
    return NULL;
}
//---------------------------------------------------------------
FBLoginLabel::FBLoginLabel()
{
    _arrowSprite = NULL;
    _label1 = NULL;
    _label2 = NULL;
}
//---------------------------------------------------------------
FBLoginLabel::~FBLoginLabel()
{
   
}
//----------------------------------------------------------------------------------
void FBLoginLabel::Init()
{
    _arrowSprite = CCSprite::spriteWithFile( "Images/Facebook/PromoNode/loginToFbNap.png" );
    _arrowSprite->setOpacity(0);
    _arrowSprite->setPosition(CCPoint( 0.0f, 0.0f ));
    addChild( _arrowSprite, 10 );

    _arrowSprite->runAction( CCSequence::actions(
                                   CCFadeTo::actionWithDuration(FadeDur, 255),
//                                   CCCallFunc::actionWithTarget(this,
//                                                                callfunc_selector(FBLoginLabel::ShowLabel1)),
                                   NULL ));
    
    //----------------------
    // Bouncing arrow action
    float scale;
    float scaleDur;
    
    scale = 0.05f;
    scaleDur = 0.7f;
        
    CCActionInterval *repeat;
    repeat = (CCActionInterval*)CCSequence::actions(
                                 CCScaleTo::actionWithDuration( scaleDur, 1 + scale ),
                                 CCScaleTo::actionWithDuration( scaleDur, 1 - scale ),
                                 NULL
                                 );
                                 
    
    runAction( CCRepeatForever::actionWithAction( repeat ));
    ShowLabel1();
}
//----------------------------------------------------------------------------------
void FBLoginLabel::ShowLabel1()
{
    int fontSize;
    fontSize = 29;
    
    if ( Game::Get()->GetGameStatus()->GetLang() == kLanguageGerman )
        fontSize = 25;
    else if ( Game::Get()->GetGameStatus()->GetLang() == kLanguageSpanish )
        fontSize = 26;
        
    _label1 =  CCLabelTTF::labelWithString( LocalString("CheckFriends1"), Config::FBHighScoreFont, fontSize );
    _label1->setColor( Config::FBMenuItemColor );
    float y;
    
    y = getContentSize().height;
    _label1->setPosition( CCPoint( getContentSize().width / 2.0f + 45.0f, 7.5f ));
    _label1->setOpacity( 0 );

    addChild( _label1, 50 );

    _label1->runAction( CCSequence::actions(
                                          CCFadeTo::actionWithDuration( FadeDur, 255 ),
                                          NULL));
}
//----------------------------------------------------------------------------------
void FBLoginLabel::ShowLabel2()
{
    return;
    _label2 =  CCLabelTTF::labelWithString( LocalString("CheckFriends2"), Config::FBHighScoreFont, 29 );
    float y;
    
    y = getContentSize().height;
    _label2->setPosition( CCPoint( getContentSize().width / 2.0f, -30.0f ));
    _label2->setOpacity( 0 );
    
    addChild( _label2, 50 );
    
    _label2->runAction( CCFadeTo::actionWithDuration( FadeDur, 255 ));
}


//----------------------------------------------------------------------------------
// Fade In Label BMP
//----------------------------------------------------------------------------------
FadeInLabelBMP* FadeInLabelBMP::Create( float dur, const char *text, const char *font, float delay )
{
    FadeInLabelBMP* node;
    node = new FadeInLabelBMP();
    if ( node )
    {
        node->autorelease();
        node->Init( dur, text, font, delay );
        return node;
    }
    return NULL;
}
//----------------------------------------------------------------------------------
FadeInLabelBMP::FadeInLabelBMP()
{
}
//----------------------------------------------------------------------------------
FadeInLabelBMP::~FadeInLabelBMP()
{
}
//----------------------------------------------------------------------------------
void FadeInLabelBMP::Init( float dur, const char *text, const char *font, float delay  )
{
    _label = CCLabelBMFont::labelWithString( text, font );
    _label->setPosition( CCPoint( 0.0f, 0.0f ));
    _label->setOpacity( 0 );
    _label->setColor( Config::FBMenuTitleColor );
    
    addChild( _label, 10 );
    
    if ( ! delay )
        _label->runAction( CCFadeTo::actionWithDuration( dur, 255 ));
    else
        _label->runAction( CCSequence::actions(
                                               CCDelayTime::actionWithDuration( delay ),
                                               CCFadeTo::actionWithDuration( dur, 255 ),
                                               NULL ));
}
//----------------------------------------------------------------------------------
void FadeInLabelBMP::SetColor( const ccColor3B& color )
{
    if ( _label )
        _label->setColor( color );
}




//----------------------------------------------------------------------------------
// Fade In Label
//----------------------------------------------------------------------------------
FadeInLabel* FadeInLabel::Create( float dur, const char *text, const char *font, float fontSize, float delay )
{
    FadeInLabel* node;
    node = new FadeInLabel();
    if ( node )
    {
        node->autorelease();
        node->Init( dur, text, font, fontSize, delay );
        return node;
    }
    return NULL;
}
//----------------------------------------------------------------------------------
FadeInLabel::FadeInLabel()
{
}
//----------------------------------------------------------------------------------
FadeInLabel::~FadeInLabel()
{
}
//----------------------------------------------------------------------------------
void FadeInLabel::Init( float dur, const char *text, const char *font, float fontSize, float delay  )
{
    _label = CCLabelTTF::labelWithString( text, font, fontSize );
    _label->setPosition( CCPoint( 0.0f, 0.0f ));
    _label->setOpacity( 0 );
    _label->setColor( Config::FBMenuTitleColor );
    
    addChild( _label, 10 );
    
    if ( ! delay )
        _label->runAction( CCFadeTo::actionWithDuration( dur, 255 ));
    else
        _label->runAction( CCSequence::actions(
                                               CCDelayTime::actionWithDuration( delay ),
                                               CCFadeTo::actionWithDuration( dur, 255 ),
                                               NULL ));
}
//----------------------------------------------------------------------------------
void FadeInLabel::SetColor( const ccColor3B& color )
{
    if ( _label )
        _label->setColor( color );
}





//----------------------------------------------------------------------------------
// Fade In Fade Out Label
//----------------------------------------------------------------------------------
FadeInFadeOutLabel* FadeInFadeOutLabel::Create( float fadeInDur,
                                      float fadeOutDur,
                                      float shownDur,
                                      float showDelay,
                                      const char *text, const char *font, float fontSize )
{
    FadeInFadeOutLabel* node;
    node = new FadeInFadeOutLabel();
    if ( node )
    {
        node->autorelease();
        node->Init( fadeInDur, fadeOutDur, shownDur, showDelay, text, font, fontSize);
        return node;
    }
    return NULL;
}
//----------------------------------------------------------------------------------
FadeInFadeOutLabel::FadeInFadeOutLabel()
{
}
//----------------------------------------------------------------------------------
FadeInFadeOutLabel::~FadeInFadeOutLabel()
{
}
//----------------------------------------------------------------------------------
void FadeInFadeOutLabel::Init(
                              float fadeInDur,
                              float fadeOutDur,
                              float shownDur,
                              float showDelay,
                              const char *text, const char *font, float fontSize )
{
    _label = CCLabelTTF::labelWithString( text, font, fontSize );
//    _label->setPosition( CCPoint( 0.0f, -20.0f ));
    _label->setPosition( CCPoint( 0.0f, -30.0f ));
    _label->setOpacity( 0 );
    _label->setColor( Config::FBMenuTitleColor );
    
    addChild( _label, 10 );
    
    _label->runAction( CCSequence::actions(
                                           CCDelayTime::actionWithDuration( showDelay + fadeInDur ),
                                           CCFadeTo::actionWithDuration( fadeInDur, 255 ),
                                           CCDelayTime::actionWithDuration( shownDur ),
                                           CCFadeTo::actionWithDuration( fadeOutDur, 0 ),
                                           CCCallFuncND::actionWithTarget(                                                                   this, callfuncND_selector( FadeInFadeOutLabel::CleanUpNode ), (void*)_label ),
                                           NULL ));
    
    CCSprite *bg;
    bg = CCSprite::spriteWithFile( "Images/Facebook/Requests/msgBackground.png" );
    bg->setPosition( ccp( 0.0f, 0.0f ));
    bg->setOpacity( 0 );
    bg->runAction( CCSequence::actions(
                                       CCDelayTime::actionWithDuration( showDelay ),
                                       CCFadeTo::actionWithDuration( fadeInDur, 255 ),
                                       CCDelayTime::actionWithDuration( shownDur + 2 * fadeInDur),
                                       CCFadeTo::actionWithDuration( fadeOutDur, 0 ),
                                       CCCallFuncND::actionWithTarget(                                                                   this, callfuncND_selector( FadeInFadeOutLabel::CleanUpNode ), (void*)bg ),
                                       NULL ));    
    addChild( bg, 5 );
    
}
//----------------------------------------------------------------------------------
void FadeInFadeOutLabel::SetColor( const ccColor3B& color )
{
    if ( _label )
        _label->setColor( color );
}
//----------------------------------------------------------------------------------
void FadeInFadeOutLabel::CleanUpNode( CCNode *sender, void *data )
{
    CCNode *node;
    node = (CCNode *) data;
    node->removeFromParentAndCleanup( true );
}


//----------------------------------------------------------------------------------
// Fade In Sprite
//----------------------------------------------------------------------------------
FadeInSprite *FadeInSprite::Create( float dur, const char *filename, float delay, unsigned int finalOpacity )
{
    FadeInSprite* node;
    node = new FadeInSprite();
    if ( node )
    {
        node->autorelease();
        node->Init( dur, filename, delay, finalOpacity );
        return node;
    }
    return NULL;
}
//----------------------------------------------------------------------------------
FadeInSprite::FadeInSprite()
{
}
//----------------------------------------------------------------------------------
FadeInSprite::~FadeInSprite()
{
    
}
//----------------------------------------------------------------------------------
void FadeInSprite::Init( float dur, const char *filename, float delay, unsigned int finalOpacity )
{
    _sprite = CCSprite::spriteWithFile(filename);
    _sprite->setPosition( CCPoint( 0.0f, 0.0f ));
    _sprite->setOpacity( 0 );
    
    addChild( _sprite, 10 );
    
    if ( ! delay )
        _sprite->runAction( CCFadeTo::actionWithDuration( dur, finalOpacity ));
    else
        _sprite->runAction( CCSequence::actions(
                                               CCDelayTime::actionWithDuration( delay ),
                                               CCFadeTo::actionWithDuration( dur, finalOpacity ),
                                               NULL ));
}
//----------------------------------------------------------------------------------
//void FadeInSprite::SetOpacity(unsigned int opacity)
//{
//    _sprite->stopAllActions();
//    _sprite->setOpacity(opacity);
//}
//----------------------------------------------------------------------------------
void FadeInSprite::Show()
{
    _sprite->stopAllActions();
    _sprite->runAction( CCFadeTo::actionWithDuration(0.5f, 255 ));
}
//----------------------------------------------------------------------------------
void FadeInSprite::Hide()
{
    _sprite->stopAllActions();
    _sprite->runAction( CCFadeTo::actionWithDuration(0.5f, 0 ));
}



//----------------------------------------------------------------------------------
// Notification Sprite
//----------------------------------------------------------------------------------
NotificationSprite *NotificationSprite::Create()
{
    NotificationSprite* node;
    node = new NotificationSprite();
    if ( node )
    {
        node->autorelease();
        node->Init();
        return node;
    }
    return NULL;
}
//----------------------------------------------------------------------------------
NotificationSprite::NotificationSprite()
{
    _sprite = NULL;
}
//----------------------------------------------------------------------------------
NotificationSprite::~NotificationSprite()
{
    
}
//----------------------------------------------------------------------------------
void NotificationSprite::Init()
{
    Init("Images/Facebook/Requests/notificationIcon.png");
}
//----------------------------------------------------------------------------------
void NotificationSprite::Init( const char *filename )
{
    _sprite = CCSprite::spriteWithFile(filename);
    _sprite->setPosition( CCPoint( 0.0f, 0.0f ));
    _sprite->setScale( 0.0001f);
    _sprite->setOpacity( 0 );
    
    addChild( _sprite, 10 );
    Show();
}
//----------------------------------------------------------------------------------
void NotificationSprite::Show()
{
    float fadeDur;
    float durLong;
    float durShort;
    CCAction *seq;
    
    
    durShort = 0.4f;
    durLong = 0.9f;
    fadeDur = durLong;
    
    seq = CCSequence::actions(
                              CCScaleTo::actionWithDuration( durLong, 1.4f ),
                              CCScaleTo::actionWithDuration( durShort, 0.8f ),
                              CCScaleTo::actionWithDuration( durShort, 1.1f ),
                              CCScaleTo::actionWithDuration( durShort, 0.9f ),
                              CCScaleTo::actionWithDuration( durShort, 1.0f ),
                              NULL );

    _sprite->runAction( CCFadeTo::actionWithDuration( fadeDur, 255 ));
    _sprite->runAction( seq );
}
//----------------------------------------------------------------------------------
void NotificationSprite::Hide()
{
    if ( ! _sprite )
        return;
    
    _sprite->stopAllActions();
    
    float fadeDur;
    float durLong;
    float durShort;
    CCAction *seq;
    
    
    durShort = 0.4f;
    durLong = 0.9f;
    fadeDur = durLong + durShort;
    
    seq = CCSequence::actions(
                              CCScaleTo::actionWithDuration( durShort, 1.1f ),
                              CCScaleTo::actionWithDuration( durLong, 0.001f ),
//                              CCCallFunc::actionWithTarget(
//                                                             this,
//                                                             callfunc_selector( NotificationSprite::CleanUpNode )),
                              NULL );

    _sprite->runAction( CCFadeTo::actionWithDuration( fadeDur, 0 ));
    _sprite->runAction( seq );
}
//----------------------------------------------------------------------------------
void NotificationSprite::CleanUpNode()
{
//    removeFromParentAndCleanup( true );
}




//----------------------------------------------------------------------------------
// Notification Sprite
//----------------------------------------------------------------------------------
RequestNotifySprite *RequestNotifySprite::Create( const char *url, bool recheckEnabled )
{
    RequestNotifySprite* node;
    node = new RequestNotifySprite( url, recheckEnabled );
    if ( node )
    {
        node->autorelease();
        node->Init();
        return node;
    }
    return NULL;
}
//----------------------------------------------------------------------------------
RequestNotifySprite::RequestNotifySprite( const char *url, bool recheckEnable )
{
    _checkUrl = url;
    _state = eState_Idle;
    _showState = eHidden;
    _sprite = NULL;
    _recheckEnabled = recheckEnable;
}
//----------------------------------------------------------------------------------
RequestNotifySprite::~RequestNotifySprite()
{
    stopAllActions();
    if (_sprite)
        _sprite->stopAllActions();
    
    if ( _state == eState_HTTPGet )
        WWWTool::Get()->CancelHttpGet( this );
}
//----------------------------------------------------------------------------------
void RequestNotifySprite::Init()
{
    Check();
}
//----------------------------------------------------------------------------------
void RequestNotifySprite::Check()
{
    if ( _state != eState_Idle )
        return;
    
    _state = eState_HTTPGet;
    WWWTool::Get()->HttpGet( _checkUrl.c_str(), this );
}
//----------------------------------------------------------------------------------
void RequestNotifySprite::Update( RequestNotifySprite::ShownState shown )
{
    if ( shown != _showState )
    {
        switch ( shown )
        {
            case eShown :
            {
                if ( ! _sprite )
                    NotificationSprite::Init();
                else
                    Show();
            }
            break;
            
            case eHidden :
                if ( _sprite )
                    Hide();
                
            default:
                break;
        }
        _showState = shown;
    }
    ScheduleCheck();
}
//----------------------------------------------------------------------------------
void RequestNotifySprite::ScheduleCheck()
{
    if ( ! FBTool::Get()->IsLogged() )
    {
        runAction( CCSequence::actions(
                                   CCDelayTime::actionWithDuration( Config::FBNotifyRequestCheckEverySec ),
                                   CCCallFunc::actionWithTarget( this, callfunc_selector( RequestNotifySprite::ScheduleCheck )),
                                   NULL ));
        return;
    }
    
    stopAllActions();
    if ( ! _recheckEnabled )
        return;
    
    runAction( CCSequence::actions(
                                   CCDelayTime::actionWithDuration( Config::FBNotifyRequestCheckEverySec ),
                                   CCCallFunc::actionWithTarget( this, callfunc_selector( RequestNotifySprite::Check )),
                                   NULL ));
}
//----------------------------------------------------------------------------------
void RequestNotifySprite::SetRecheckEnabled( bool enabled )
{
    _recheckEnabled = enabled;
    if ( _recheckEnabled )
        ScheduleCheck();
}
//----------------------------------------------------------------------------------
void RequestNotifySprite::Http_FailedWithError( void *connection, int error )
{
    _state = eState_Idle;
    ScheduleCheck();
}
//----------------------------------------------------------------------------------
void RequestNotifySprite::Http_FinishedLoading( void *connection, const void *data, int len )
{
    string temp;
    string strData;
    int value;

    temp.append( (const char*)data );
    strData = string( temp, 0, len );

    D_STRING( temp );
    D_STRING( strData );
      
    stringstream(strData) >> value;
    if ( value == 1 )
        Update( eShown );
    else
        Update( eHidden );

    _state = eState_Idle;
}
//----------------------------------------------------------------------------------
void RequestNotifySprite::Show()
{
    _showState = eShown;
    NotificationSprite::Show();
}
//----------------------------------------------------------------------------------
void RequestNotifySprite::Hide()
{
    _showState = eHidden;
    NotificationSprite::Hide();
}



//----------------------------------------------------------------------------------
//
//----------------------------------------------------------------------------------
AnyRequestNotify* AnyRequestNotify::Create()
{   
    AnyRequestNotify* node;
    node = new AnyRequestNotify( "" );
    if ( node )
    {
        node->autorelease();
        node->Init();
        return node;
    }
    return NULL;
}
//----------------------------------------------------------------------------------
AnyRequestNotify::AnyRequestNotify( const char *url ) : RequestNotifySprite( url, true )
{
    _initialized = false;
    _dTime = 0;
}
//----------------------------------------------------------------------------------
void AnyRequestNotify::Init()
{
    schedule(schedule_selector( AnyRequestNotify::Step));
}
//----------------------------------------------------------------------------------
void AnyRequestNotify::Step(ccTime time)
{
    _dTime += time;
    if ( _dTime < 0.5 )
        return;
    else
        _dTime = 0;
    
    if ( FBTool::Get()->IsLogged() && ! _initialized )
    {
        stringstream url;
        url << Config::FBRequestsURL << "checkAllNotify.php?fbId=" << FBTool::Get()->GetFBUserId().c_str();
        _checkUrl = url.str();
        
        stopAllActions();
        RequestNotifySprite::Init();

        _initialized = true;
    }
    
    if ( ! FBTool::Get()->IsLogged() && _initialized )
    {
        stopAllActions();
        Hide();
        
        _initialized = false;
    }
}
//----------------------------------------------------------------------------------