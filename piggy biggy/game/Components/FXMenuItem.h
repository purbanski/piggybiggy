#ifndef __FXMENUITEM_H__
#define __FXMENUITEM_H__

#include "cocos2d.h"
USING_NS_CC;

#include "Components/TrippleSprite.h"

//-----------------------------------------------------------------------------
class FXMenuItemToggle : public CCMenuItemToggle
{
public:
	static FXMenuItemToggle* itemWithTarget(CCObject* target, SEL_MenuHandler selector, CCMenuItem* item, ...);
	virtual void activate();
	virtual ~FXMenuItemToggle();
};
//-----------------------------------------------------------------------------
class FXMenuItemSprite : public CCMenuItemSprite
{
public:
	static FXMenuItemSprite* itemFromNormalSprite(CCNode* normalSprite, CCNode* selectedSprite, CCNode* disabledSprite, CCObject* target, SEL_MenuHandler selector);
	static FXMenuItemSprite* itemFromNormalSprite( TrippleSprite triSprite, CCObject* target, SEL_MenuHandler selector);
	virtual void activate();
	virtual ~FXMenuItemSprite();
};
//-----------------------------------------------------------------------------
class FXMenuItemSpriteWithAnim : public CCMenuItemSprite
{
public:
	static FXMenuItemSpriteWithAnim* Create( TrippleSprite *trippleSprite, CCObject* target, SEL_MenuHandler selector);
	static FXMenuItemSpriteWithAnim* Create(CCNode* normalSprite, CCNode* selectedSprite, CCNode* disabledSprite, CCObject* target, SEL_MenuHandler selector);
	static FXMenuItemSpriteWithAnim* Create( TrippleSprite triSprite, CCObject* target, SEL_MenuHandler selector);
	virtual void activate();
	virtual ~FXMenuItemSpriteWithAnim();

};
//-----------------------------------------------------------------------------
class FXMenuItemLabel : public CCMenuItemLabel
{
public:
	static FXMenuItemLabel * itemWithLabel(CCNode*label, CCObject* target, SEL_MenuHandler selector);
	virtual void activate();
	virtual ~FXMenuItemLabel();

};


#endif
