#include "TrippleSprite.h"
#include "Tools.h"
//-------------------------------------------------------------------
TrippleSprite::TrippleSprite()
{
	_normal = NULL;
	_disable = NULL;
	_selected = NULL;
}
//-------------------------------------------------------------------
TrippleSprite::TrippleSprite( const char *filename )
{
    _normal = CCSprite::spriteWithFile( filename );
    _disable = CCSprite::spriteWithFile( filename );
    _selected = CCSprite::spriteWithFile( filename );
}
//-------------------------------------------------------------------
TrippleSprite::TrippleSprite( const char *normal, const char *selected, const char *disable )
{
    _normal = CCSprite::spriteWithFile( normal );
    _disable = CCSprite::spriteWithFile( disable );
    _selected = CCSprite::spriteWithFile( selected );
//    Tools::RunButtonAnim( _selected );
}
//-------------------------------------------------------------------

