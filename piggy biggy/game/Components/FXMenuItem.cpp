#include "FXMenuItem.h"
#include "SoundEngine.h"
#include "Animation/AnimerSimpleEffect.h"

//---------------------------------------------------------------------------------
// Fx Toggle
//---------------------------------------------------------------------------------
FXMenuItemToggle* FXMenuItemToggle::itemWithTarget( CCObject* target, SEL_MenuHandler selector, CCMenuItem* item, ... )
{
	va_list args;
	va_start(args, item);
	FXMenuItemToggle *pRet = new FXMenuItemToggle();
	pRet->initWithTarget(target, selector, item, args);
	pRet->autorelease();
	va_end(args);
	return pRet;
}
//---------------------------------------------------------------------------------
void FXMenuItemToggle::activate()
{
   	SoundEngine::Get()->PlayEffect( SoundEngine::eSoundButtonClick );
	CCMenuItemToggle::activate();
}
//---------------------------------------------------------------------------------
FXMenuItemToggle::~FXMenuItemToggle()
{
	//CC_SAFE_RELEASE(m_pSubItems);
}


//---------------------------------------------------------------------------------
// FX Sprite
//---------------------------------------------------------------------------------
FXMenuItemSprite* FXMenuItemSprite::itemFromNormalSprite( CCNode* normalSprite, CCNode* selectedSprite, CCNode* disabledSprite, CCObject* target, SEL_MenuHandler selector )
{
	FXMenuItemSprite *pRet = new FXMenuItemSprite();
	pRet->initFromNormalSprite(normalSprite, selectedSprite, disabledSprite, target, selector); 
	pRet->autorelease();
	return pRet;
}
//---------------------------------------------------------------------------------
FXMenuItemSprite* FXMenuItemSprite::itemFromNormalSprite( TrippleSprite triSprite, CCObject* target, SEL_MenuHandler selector )
{
	return itemFromNormalSprite( triSprite._normal, triSprite._selected, triSprite._disable, target, selector );
}

//---------------------------------------------------------------------------------
void FXMenuItemSprite::activate()
{
	CCMenuItemSprite::activate();
   	SoundEngine::Get()->PlayEffect( SoundEngine::eSoundButtonClick );
}
//---------------------------------------------------------------------------------
FXMenuItemSprite::~FXMenuItemSprite()
{

}



//---------------------------------------------------------------------------------
// FX Sprite
//---------------------------------------------------------------------------------
FXMenuItemSpriteWithAnim* FXMenuItemSpriteWithAnim::Create(
                                                           TrippleSprite *tsprite,
                                                           CCObject* target,
                                                           SEL_MenuHandler selector)
{
    return Create(
                  tsprite->_normal,
                  tsprite->_selected,
                  tsprite->_disable,
                  target,
                  selector );
}
//---------------------------------------------------------------------------------
FXMenuItemSpriteWithAnim* FXMenuItemSpriteWithAnim::Create(
                                                           CCNode* normalSprite,
                                                           CCNode* selectedSprite,
                                                           CCNode* disabledSprite,
                                                           CCObject* target,
                                                           SEL_MenuHandler selector )
{
	FXMenuItemSpriteWithAnim *pRet = new FXMenuItemSpriteWithAnim();
	AnimerSimpleEffect::Get()->Anim_ButtonPulse( selectedSprite );
	pRet->initFromNormalSprite( normalSprite, selectedSprite, disabledSprite, target, selector); 
	pRet->autorelease();
	return pRet;
}
//---------------------------------------------------------------------------------
FXMenuItemSpriteWithAnim* FXMenuItemSpriteWithAnim::Create(
                                                           TrippleSprite triSprite,
                                                           CCObject* target,
                                                           SEL_MenuHandler selector )
{
	return Create( triSprite._normal,
                  triSprite._selected,
                  triSprite._disable,
                  target, selector );
}
//---------------------------------------------------------------------------------
void FXMenuItemSpriteWithAnim::activate()
{
	SoundEngine::Get()->PlayEffect( SoundEngine::eSoundButtonClick );
	CCMenuItemSprite::activate();
}
//---------------------------------------------------------------------------------
FXMenuItemSpriteWithAnim::~FXMenuItemSpriteWithAnim()
{

}



//---------------------------------------------------------------------------------
// FX Label
//---------------------------------------------------------------------------------
FXMenuItemLabel * FXMenuItemLabel::itemWithLabel( CCNode*label, CCObject* target, SEL_MenuHandler selector )
{
	FXMenuItemLabel *pRet = new FXMenuItemLabel();
	pRet->initWithLabel(label, target, selector);
	pRet->autorelease();
	return pRet;
}
//---------------------------------------------------------------------------------
void FXMenuItemLabel::activate()
{
	SoundEngine::Get()->PlayEffect( SoundEngine::eSoundButtonClick );
	CCMenuItemLabel::activate();
}

FXMenuItemLabel::~FXMenuItemLabel()
{

}
