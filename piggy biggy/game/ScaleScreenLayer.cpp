#include "Debug/MyDebug.h"
#include "ScaleScreenLayer.h"
#include "Tools.h"
#include "GameConfig.h"
#include "Game.h"

//----------------------------------------------------------------------------------------------------------------
ScaleScreenLayer::ScaleScreenLayer()
{
}
//----------------------------------------------------------------------------------------------------------------
bool ScaleScreenLayer::init()
{
	if ( ! CCLayer::init() )
		return false;

#ifndef BUILD_EDITOR
	//Clipping image 
	CCSprite *sprite;
	sprite = CCSprite::spriteWithFile( "Images/Other/ScaleLayer.png" );
	sprite->setPosition( ccp( 0.0f, 0.0f ));
	addChild( sprite );
#endif

	_miniaized = false;
	setIsTouchEnabled( false );

	return true;
}
//----------------------------------------------------------------------------------------------------------------
void ScaleScreenLayer::registerWithTouchDispatcher( void )
{

	CCTouchDispatcher::sharedDispatcher()->addTargetedDelegate( this, 1000, true );
}
//----------------------------------------------------------------------------------------------------------------
void ScaleScreenLayer::onExit()
{

	CCTouchDispatcher::sharedDispatcher()->removeDelegate( this );
}
//----------------------------------------------------------------------------------------------------------------
ScaleScreenLayer::~ScaleScreenLayer()
{

	removeAllChildrenWithCleanup( true );
}
//----------------------------------------------------------------------------------------------------------------
