#include "unFile.h"
#include "SimpleAudioEngine.h"
#include "unTypes.h"
#include "unDebug.h"

#include "Debug/MyDebug.h"
#include "Debug/DebugNode.h"
#include "SoundEngine.h"
#include "GameConfig.h"
#include "Game.h"
#include "GameStatus.h"
#include "RunLogger/RunLogger.h"

using namespace CocosDenshion;

WaveHeader::WaveHeader()
{
	memset( this, 0, sizeof( WaveHeader));
}

SoundEngine* SoundEngine::_sInstance = NULL;
const float SoundEngine::FADE_OUT_STEP = 0.02f;

//----------------------------------------------------------------------------------
void SoundEngine::InitSoundMap()
{
	_soundsMap[ eSoundNone					].filename.append( "Sounds/BugFix.mp3" );
	_soundsMap[ eSoundBugFix				].filename.append( "Sounds/BugFix.mp3" );
	_soundsMap[ eSoundCoinCollected 		].filename.append( "Sounds/CoinCollected.mp3" );
	_soundsMap[ eSoundMoneyMakeMoney 		].filename.append( "Sounds/MoneyMakeMoney.mp3" );
	_soundsMap[ eSoundFragileDestroyed1		].filename.append( "Sounds/FragileDestroyed1.mp3" );
	_soundsMap[ eSoundFragileDestroyed2		].filename.append( "Sounds/FragileDestroyed2.mp3" );
	_soundsMap[ eSoundFragileDestroyed3		].filename.append( "Sounds/FragileDestroyed3.mp3" );
	_soundsMap[ eSoundFragileDestroyed4		].filename.append( "Sounds/FragileDestroyed4.mp3" );
	_soundsMap[ eSoundBombExploded1			].filename.append( "Sounds/BombExploded1.mp3" );
	_soundsMap[ eSoundBombExploded2			].filename.append( "Sounds/BombExploded2.mp3" );
	_soundsMap[ eSoundBombExploded3			].filename.append( "Sounds/BombExploded3.mp3" );
	_soundsMap[ eSoundBoxerGlovePunch		].filename.append( "Sounds/BoxerGlovePunch.mp3" );
	_soundsMap[ eSoundThiefCaught			].filename.append( "Sounds/ThiefCaught.mp3" );
	_soundsMap[ eSoundLevelFinished			].filename.append( "Sounds/LevelFinished.mp3" );
	_soundsMap[ eSoundLevelFailed			].filename.append( "Sounds/LevelFailed.mp3" );
	_soundsMap[ eSoundLevelSkipped			].filename.append( "Sounds/LevelSkipped.mp3" );
	_soundsMap[ eSoundLevelRestart			].filename.append( "Sounds/LevelRestart.mp3" );
	_soundsMap[ eSoundBankLockUnlocked		].filename.append( "Sounds/BankLockUnlocked.mp3" );
	_soundsMap[ eSoundBankLockLocked		].filename.append( "Sounds/BankLockLocked.mp3" );
	_soundsMap[ eSoundBankSaveSmallWheel	].filename.append( "Sounds/BankSaveSmallWheel.mp3" );
	_soundsMap[ eSoundBankSettingWheelPryk	].filename.append( "Sounds/BankCombinationWheel.mp3" );
	_soundsMap[ eSoundBankSaveSlideUp		].filename.append( "Sounds/BankSaveSlideUp.mp3" );
	_soundsMap[ eSoundBankSaveBigWheel		].filename.append( "Sounds/BankSaveBigWheel.mp3" );
	_soundsMap[ eSoundBankLockBarsSlide		].filename.append( "Sounds/BankLockBarsSlide.mp3" );
	_soundsMap[ eSoundSlidingDoorOpen		].filename.append( "Sounds/SlidingDoorOpen.mp3" );
	_soundsMap[ eSoundSlidingDoorClose		].filename.append( "Sounds/SlidingDoorClose.mp3" );
	_soundsMap[ eSoundSpit					].filename.append( "Sounds/Spit.mp3" );
	_soundsMap[ eSoundSpitEmpty				].filename.append( "Sounds/SpitEmpty.mp3" );
	_soundsMap[ eSoundCarBreaking1			].filename.append( "Sounds/CarBreaking1.mp3" );
	_soundsMap[ eSoundCarBreaking2			].filename.append( "Sounds/CarBreaking2.mp3" );
	_soundsMap[ eSoundCarBreaking3			].filename.append( "Sounds/CarBreaking3.mp3" );
	_soundsMap[ eSoundCarStartAndRun		].filename.append( "Sounds/CarStartAndRun.mp3" );
	_soundsMap[ eSoundLevelNameShow			].filename.append( "Sounds/LevelNameShow.mp3" );
	_soundsMap[ eSoundLevelNameHide			].filename.append( "Sounds/LevelNameHide.mp3" );
   	_soundsMap[ eSoundPageTurn              ].filename.append( "Sounds/PageTurn.mp3" );
	
	//-------------------
	// Piggy Idle sounds
	_soundsMap[ eSoundAnim_PiggyUaa			].filename.append( "Sounds/Animation/PiggyUaa.mp3" );
	_soundsMap[ eSoundAnim_PiggySalto		].filename.append( "Sounds/Animation/PiggySalto.mp3" );
	_soundsMap[ eSoundAnim_PiggyFiuFiu		].filename.append( "Sounds/Animation/PiggyFiuFiu.mp3" );
	_soundsMap[ eSoundAnim_PiggyEeeOoo		].filename.append( "Sounds/Animation/PiggyEeeOoo.mp3" );
	_soundsMap[ eSoundAnim_PiggyHappyJump	].filename.append( "Sounds/Animation/PiggyHappyJump.mp3" );
	_soundsMap[ eSoundAnim_PiggyNoseBubble	].filename.append( "Sounds/Animation/PiggyNoseBubble.mp3" );
	_soundsMap[ eSoundAnim_PiggyLookAround	].filename.append( "Sounds/Animation/PiggyLookAround.mp3" );
	

	//-------------------
	// Piggy Events
	_soundsMap[ eSoundAnim_PiggyToRolling			].filename.append( "Sounds/Animation/PiggyToRolling.mp3" );
	_soundsMap[ eSoundAnim_PiggyFromRolling         ].filename.append( "Sounds/Animation/PiggyFromRolling.mp3" );
	_soundsMap[ eSoundAnim_PiggyLevelFailed_Sit     ].filename.append( "Sounds/Animation/PiggySitLevelFailed.mp3" );
	_soundsMap[ eSoundAnim_PiggyLevelFailed_Roll	].filename.append( "Sounds/Animation/PiggyRollLevelFailed.mp3" );
	_soundsMap[ eSoundAnim_PiggyLevelDone_Sit		].filename.append( "Sounds/Animation/PiggySitLevelDone.mp3" );
	_soundsMap[ eSoundAnim_PiggyLevelDone_Roll      ].filename.append( "Sounds/Animation/PiggyRollLevelDone.mp3" );
	_soundsMap[ eSoundAnim_PiggyCoinCollected_Sit	].filename.append( "Sounds/Animation/PiggySitCoinCollected.mp3" );
	_soundsMap[ eSoundAnim_PiggyCoinCollected_Roll  ].filename.append( "Sounds/Animation/PiggyRollCoinCollected.mp3" );
	_soundsMap[ eSoundAnim_PiggyRotationFix         ].filename.append( "Sounds/Animation/PiggyRotationFix.mp3" );
	_soundsMap[ eSoundAnim_PiggyStolen              ].filename.append( "Sounds/PiggyStolen.mp3" );


	_soundsMap[ eSoundPiggyNewBorn      ].filename.append( "Sounds/PiggyNewBorn.mp3" );
	_soundsMap[ eSoundCoinNewBorn		].filename.append( "Sounds/CoinNewBorn.mp3" );
	_soundsMap[ eSoundPrisonAlert		].filename.append( "Sounds/PrisonAlert.mp3" );
	_soundsMap[ eSoundFireworks1		].filename.append( "Sounds/Fireworks1.mp3" );
	_soundsMap[ eSoundFireworks2		].filename.append( "Sounds/Fireworks2.mp3" );
	_soundsMap[ eSoundChangeScene		].filename.append( "Sounds/ChangeScene.mp3" );
	_soundsMap[ eSoundButtonClick		].filename.append( "Sounds/ButtonClick.mp3" );
	_soundsMap[ eSoundNewClue			].filename.append( "Sounds/NewClue.mp3" );
	_soundsMap[ eSoundChainCut			].filename.append( "Sounds/FlyCut.mp3" );

	_soundsMap[ eSoundAirBubbleGrow			].filename.append( "Sounds/AirBubbleGrow.mp3" );
	_soundsMap[ eSoundAirBubblePop			].filename.append( "Sounds/AirBubblePop.mp3" );
	_soundsMap[ eSoundCoinScored			].filename.append( "Sounds/CoinScored.mp3" );
	_soundsMap[ eSoundCoinScoredReversed    ].filename.append( "Sounds/CoinScoredReversed.mp3" );

	_soundsMap[ eSoundPocketUnlock			].filename.append( "Sounds/PocketUnlock.mp3" );
	_soundsMap[ eSoundSlidingNode_Slide		].filename.append( "Sounds/SlidingMenuSlide.mp3" );
	_soundsMap[ eSoundSlidingNode_SlideBack	].filename.append( "Sounds/SlidingMenuSlideBack.mp3" );

	_soundsMap[ eSoundScoreTimeTick			].filename.append( "Sounds/ScoreTimeTick.mp3" );
	_soundsMap[ eSoundScoreTimeTickFinal	].filename.append( "Sounds/ScoreTimeTickFinal.mp3" );
    

	//----------------------------------------
	// Anims 
	//----------------------------------------
	_soundsMap[ eSoundAnim_PiggyFart			].filename.append( "Sounds/Animation/PiggyBankFart.mp3" );
	_soundsMap[ eSoundAnim_CoinBlink			].filename.append( "Sounds/Animation/CoinBlink.mp3" );
	_soundsMap[ eSoundAnim_FlyFly1              ].filename.append( "Sounds/Animation/FlyFly1.mp3" );
	_soundsMap[ eSoundAnim_FlyFly2              ].filename.append( "Sounds/Animation/FlyFly2.mp3" );
	_soundsMap[ eSoundAnim_FlyFly3              ].filename.append( "Sounds/Animation/FlyFly3.mp3" );
	_soundsMap[ eSoundAnim_ThiefSitItemStolen	].filename.append( "Sounds/Animation/ThiefSitItemStolen.mp3" );
	_soundsMap[ eSoundAnim_ThiefRollItemStolen  ].filename.append( "Sounds/Animation/ThiefRollItemStolen.mp3" );

	//------------------------ 
	// keep them lower case
	//------------------------
	//_filenameToSoundMap["PiggySnort"	]		= eSoundAnim_PiggySnort;
	_filenameToSoundMap["PiggyFart"		]		= eSoundAnim_PiggyFart;
	_filenameToSoundMap["CoinBlink"		]		= eSoundAnim_CoinBlink;

	_soundsAlwaysLoaded.insert( eSoundButtonClick );
	_soundsAlwaysLoaded.insert( eSoundChangeScene );
	for ( SoundSet::iterator it = _soundsAlwaysLoaded.begin(); it != _soundsAlwaysLoaded.end(); it++ )
		LoadSound( *it );
}
//----------------------------------------------------------------------------------
SoundEngine* SoundEngine::Get()
{
	if ( _sInstance == NULL )
		_sInstance = new SoundEngine();
	return _sInstance;
}
//----------------------------------------------------------------------------------
void SoundEngine::Destroy()
{
	if ( _sInstance )
	{
		delete _sInstance;
		_sInstance = NULL;
	}
}
//----------------------------------------------------------------------------------
SoundEngine::SoundEngine()
{
	SetMusicVolume( Config::SoundMusicVol );
	SetEffectsVolume( Config::SoundEffectVol ); 
    
	InitSoundMap();
    SimpleAudioEngine::sharedEngine()->setListener( this );

#ifdef CC_UNDER_MARMALADE
	s3eSoundSetInt( S3E_SOUND_DEFAULT_FREQ,  44100 * 2);
#endif
}
//----------------------------------------------------------------------------------
SoundEngine::~SoundEngine()
{
    SimpleAudioEngine::sharedEngine()->setListener( NULL );
	_soundsAlwaysLoaded.clear();
	Reset();
    
    _soundsPlayedMap.clear();
    _soundsPlayedCountMap.clear();
    _soundsMap.clear();
    _soundsToBeFadeOut.clear();
}
//----------------------------------------------------------------------------------
void SoundEngine::StopBackgroundMusic()
{
    SimpleAudioEngine::sharedEngine()->stopBackgroundMusic();
}
//----------------------------------------------------------------------------------
void SoundEngine::Update()
{
	FadeOutSounds();
    //    UnloadStoppedSounds();
}
//----------------------------------------------------------------------------------
int SoundEngine::PlayEffect( Sound sound, float freDeltaPerc )
{
	if ( GetEffectsMute() )
		return -1;

    int soundId;
    soundId = SimpleAudioEngine::sharedEngine()->playEffect( _soundsMap[sound].filename.c_str() );
    
    _soundsPlayedMap[soundId] = sound;
	_soundsPlayedCountMap[sound].hitCount++;

    return soundId;
}
//----------------------------------------------------------------------------------
int SoundEngine::PlayEffect( Sound sound, bool loop  )
{
	if ( GetEffectsMute() )
		return -1;

    int soundId;
    soundId = SimpleAudioEngine::sharedEngine()->playEffect( _soundsMap[sound].filename.c_str(), loop );

    _soundsPlayedMap[soundId] = sound;
	_soundsPlayedCountMap[sound].hitCount++;

    CCLog("Play SoundId : %d", soundId );
    return soundId;
}
//----------------------------------------------------------------------------------
void SoundEngine::EffectFinished( unsigned int soundId )
{
    CCLog("Finished SoundId: %d %d", soundId, _soundsPlayedMap[ soundId ] );
    if ( _soundsPlayedMap[soundId] == eSoundCarStartAndRun ||
        _soundsPlayedMap[soundId] == 0 ) // already fade out and remove from list
        return;
    
    if ( _soundsPlayedCountMap[ _soundsPlayedMap[ soundId ] ].hitCount > 0 )
        _soundsPlayedCountMap[ _soundsPlayedMap[ soundId ] ].hitCount--;
    
    _soundsPlayedMap.erase( _soundsPlayedMap.find( soundId ));
}
//----------------------------------------------------------------------------------
void SoundEngine::PlayRandomEffect( int effect, ... )
{
	va_list args;

	typedef vector<Sound> Sounds;
	Sounds sounds;

	va_start(args,effect);

	while(effect != eSoundNone )
	{
		sounds.push_back( (Sound) effect );
		effect = va_arg(args, int );
	}

	va_end(args);

	if ( sounds.size() > 5 )
	{
		unAssertMsg( SoundEngine, false, ("probalby somthing wrong. Souds random count: %d", sounds.size() ));
	}

	int randIndex;
	randIndex = Tools::Rand( 0, sounds.size() - 1 );
	
	PlayEffect( sounds[ randIndex ] );
}
//----------------------------------------------------------------------------------
void SoundEngine::PlayRandomEffect( float freDeltaPerc, int effect, ... )
{
	va_list args;

	typedef vector<Sound> Sounds;
	Sounds sounds;

	va_start(args,effect);

	while(effect != eSoundNone )
	{
		sounds.push_back( (Sound) effect );
		effect = va_arg(args, int );
	}

	va_end(args);

	if ( sounds.size() > 8 )
	{
		unAssertMsg( SoundEngine, false, ("Probably something wen't wrong. Sounds random count: %d", sounds.size() ));
	}

	int randIndex;
	randIndex = Tools::Rand( 0, sounds.size() - 1 );

	PlayEffect( sounds[ randIndex ], freDeltaPerc );
}
//----------------------------------------------------------------------------------
void SoundEngine::PlayEffect_CarStartAndRun()
{
	SoundId soundId;
	SoundPlayedMap::iterator it;
	
	for ( it=_soundsPlayedMap.begin(); it != _soundsPlayedMap.end(); it++ )
		if ( it->second == eSoundCarStartAndRun )
			break;

		if ( it != _soundsPlayedMap.end() )
	{
		soundId = it->first;
        CancelFadeOut( soundId );//cancel if fading
	}
	else
	{
            PlayEffect( eSoundCarStartAndRun, true );
	}
}
//----------------------------------------------------------------------------------
void SoundEngine::AdjustEffect_CarStartAndRun( float speed )
{
	SoundId soundId;
	SoundPlayedMap::iterator it;

	if ( _soundsPlayedCountMap[eSoundCarStartAndRun].hitCount < 1 )
		return;

	for ( it =_soundsPlayedMap.begin(); it != _soundsPlayedMap.end(); it++ )
		if ( it->second == eSoundCarStartAndRun )
			break;
	
	if ( it == _soundsPlayedMap.end() )
		return;

	soundId = it->first;

	//if ( s3eSoundChannelGetInt( channel, S3E_CHANNEL_STATUS ) != 1 /* 1 = playing */ )
	//	return;
	
    //	if ( _channelsToBeFadeOut.find( channel ) == _channelsToBeFadeOut.end() )
	//	return;
    
    /// spped max
    float speedmax = 30;
    float pitch;
    
    pitch = 1 + abs(speed) / speedmax;
    SimpleAudioEngine::sharedEngine()->setEffectPitch( soundId, pitch );
    
}
//----------------------------------------------------------------------------------
void SoundEngine::StopEffect_CarStartAndRun()
{
	SoundPlayedMap::iterator it;
	
	for ( it = _soundsPlayedMap.begin(); it != _soundsPlayedMap.end(); it++ )
		if ( it->second == eSoundCarStartAndRun )
			break;
	
	//check if effect is played
	if ( it != _soundsPlayedMap.end() )
	{
		AddToFadeOut( it->first );
	}
}
//----------------------------------------------------------------------------------
void SoundEngine::LoadSound( Sound sound )
{
	if ( ! _soundsMap.count( sound ))
		return;

    SimpleAudioEngine::sharedEngine()->preloadEffect( _soundsMap[sound].filename.c_str() );
}
//----------------------------------------------------------------------------------
void SoundEngine::UnloadSound( Sound sound )
{
	if ( ! _soundsMap.count(sound))
		return;

	if ( _soundsAlwaysLoaded.count( sound ))
		return;
	
    SimpleAudioEngine::sharedEngine()->unloadEffect( _soundsMap[sound].filename.c_str() );
}
//----------------------------------------------------------------------------------
void SoundEngine::UnloadAllSounds()
{
	SoundsHitMap::iterator it;
	for ( SoundsMap::iterator it = _soundsMap.begin(); it != _soundsMap.end(); it++ )
	{
		UnloadSound( Sound( it->first ));
	}
	
	_soundsPlayedCountMap.clear();
}
//----------------------------------------------------------------------------------
BackgroundMusic& SoundEngine::GetBackgroundMusic()
{
	return _backgroundMusic;
}
//----------------------------------------------------------------------------------
void SoundEngine::SetMusicVolume( float volume )
{ 
    SimpleAudioEngine::sharedEngine()->setBackgroundMusicVolume( volume );
}
//----------------------------------------------------------------------------------
void SoundEngine::SetEffectsVolume( float volume )
{
    SimpleAudioEngine::sharedEngine()->setEffectsVolume( volume );
}
//----------------------------------------------------------------------------------
void SoundEngine::PlayEffectCallback( CCObject *node, void *sound )
{
	_sInstance->PlayEffect( (Sound) (long)sound );
}
//----------------------------------------------------------------------------------
void SoundEngine::UnloadStoppedSounds()
{
	Sound snd;
	SoundsHitMap::iterator it;

	for ( it = _soundsPlayedCountMap.begin(); it != _soundsPlayedCountMap.end(); )
	{
		if ( it->second.hitCount == 0 )
		{
			snd = (Sound) it->first;
			UnloadSound( snd );
			_soundsPlayedCountMap.erase( it++ );
		}
		else
			++it;
	}
}
//----------------------------------------------------------------------------------
void SoundEngine::AddToFadeOut( SoundId soundId )
{
	//check if is not already fading out
	if ( _soundsToBeFadeOut.find( soundId ) == _soundsToBeFadeOut.end() )
    {
        CCLOG("AddToFadeOut SoundIt=%d", soundId );
		_soundsToBeFadeOut.insert( soundId );
    }
}
//----------------------------------------------------------------------------------
void SoundEngine::FadeOutSounds()
{
	float vol;
    SoundIdSet::iterator it;
    for ( it = _soundsToBeFadeOut.begin(); it != _soundsToBeFadeOut.end(); )
    {
        vol = SimpleAudioEngine::sharedEngine()->getEffectVolumne( *it );
		if ( vol > 0 )
		{
			vol -= FADE_OUT_STEP;
            if ( vol < 0.0f )
                vol = 0.0f;
            
            SimpleAudioEngine::sharedEngine()->setEffectVolumne( *it, vol );
            
            if ( _soundsPlayedMap[ *it ] == eSoundCarStartAndRun )
            {
                float pitch;
                pitch = SimpleAudioEngine::sharedEngine()->getEffectPitch( *it );
                
                if ( pitch > 0.75f )
                    pitch -= 0.05f;
                
                SimpleAudioEngine::sharedEngine()->setEffectPitch( *it, pitch );
            }
            
			++it;
		}
		else
		{
			Sound snd;
            SoundId sndId;
            
			snd = _soundsPlayedMap[*it];
            sndId =  *it;
            it++;
            
			_soundsPlayedMap.erase( _soundsPlayedMap.find( sndId ));
			_soundsPlayedCountMap[ snd ].hitCount = 0;

			_soundsToBeFadeOut.erase( sndId );
            SimpleAudioEngine::sharedEngine()->stopEffect( sndId );
		}
	}
}
//----------------------------------------------------------------------------------
void SoundEngine::CancelFadeOut( SoundId soundId )
{
	SoundIdSet::iterator it;

	if ( (it = _soundsToBeFadeOut.find( soundId )) != _soundsToBeFadeOut.end() )
	{
		D_LOG("Fadeout canceled");
		_soundsToBeFadeOut.erase( it );

		//fix me - vol should not come back to default but to current vol
        SimpleAudioEngine::sharedEngine()->setEffectVolumne( soundId, 1.0f );
	}
}
//----------------------------------------------------------------------------------
void SoundEngine::FadeOutAllEffects()
{
    for ( SoundPlayedMap::iterator it = _soundsPlayedMap.begin(); it != _soundsPlayedMap.end(); it++ )
    {
        AddToFadeOut( it->first );
    }
}
//----------------------------------------------------------------------------------
float SoundEngine::GetMusicVolume()
{
    return SimpleAudioEngine::sharedEngine()->getBackgroundMusicVolume();
}
//----------------------------------------------------------------------------------
float SoundEngine::GetEffectsVolume()
{
	return SimpleAudioEngine::sharedEngine()->getEffectsVolume();
}
//----------------------------------------------------------------------------------
void SoundEngine::SetEffectsMute( bool mute )
{
	Game::Get()->GetGameStatus()->SetEffectsMute( mute );
}
//----------------------------------------------------------------------------------
bool SoundEngine::GetEffectsMute()
{
	return Game::Get()->GetGameStatus()->GetEffectsMute();
}
//----------------------------------------------------------------------------------
void SoundEngine::PauseAllEffects()
{
    SimpleAudioEngine::sharedEngine()->stopAllEffects();
}
//----------------------------------------------------------------------------------
void SoundEngine::PauseBackgroundMusic()
{
    SimpleAudioEngine::sharedEngine()->pauseBackgroundMusic();	
}
//----------------------------------------------------------------------------------
void SoundEngine::ResumeBackgroundMusic()
{
    if ( SimpleAudioEngine::sharedEngine()->isBackgroundMusicPlaying() )
        return;
    
    if ( Game::Get()->GetGameStatus()->GetMusicMute() )
        return;
    
    SimpleAudioEngine::sharedEngine()->resumeBackgroundMusic();
}
//----------------------------------------------------------------------------------
void SoundEngine::ResumeAllEffects()
{
    //Game::Get()->GetGameStatus()->SetEffectsMute( false );
}
//----------------------------------------------------------------------------------
void SoundEngine::Reset()
{
    UnloadAllSounds();
}
//----------------------------------------------------------------------------------
void SoundEngine::PreloadAllEffects()
{

	int i;
	i = eSoundNone;

	for ( i = (int) eSoundNone; i < eSoundLast; i++ )
		LoadSound( (Sound) i );
}
//----------------------------------------------------------------------------------
float SoundEngine::GetEffectDuration( Sound effect )
{
    if ( ! _soundsMap[effect].duration )
        _soundsMap[effect].duration = SimpleAudioEngine::sharedEngine()->getEffectDuration( _soundsMap[effect].filename.c_str() );
    
    return _soundsMap[effect].duration;
}

//----------------------------------------------------------------------------------





//----------------------------------------------------------------------------------
//
//----------------------------------------------------------------------------------
BackgroundMusic::BackgroundMusic()
{
	_music = eMusicNone;
	_musicLastPlayed = eMusicNone;
}
//----------------------------------------------------------------------------------
BackgroundMusic::~BackgroundMusic()
{
}
//----------------------------------------------------------------------------------
void BackgroundMusic::ResumeMusic()
{
	Game::Get()->GetGameStatus()->SetMusicMute( false );
    PlayMusic();
}
//----------------------------------------------------------------------------------
void BackgroundMusic::PauseMusic()
{
    SimpleAudioEngine::sharedEngine()->pauseBackgroundMusic();
	Game::Get()->GetGameStatus()->SetMusicMute( true );
}
//----------------------------------------------------------------------------------
void BackgroundMusic::SetMusicMute( bool mute )
{
	Game::Get()->GetGameStatus()->SetMusicMute( mute );
}
//----------------------------------------------------------------------------------
bool BackgroundMusic::GetMusicMute()
{

	return Game::Get()->GetGameStatus()->GetMusicMute();
}
//----------------------------------------------------------------------------------
void BackgroundMusic::SetMusic_Intro()
{
    _music = eMusicIntro;
    if ( ! Game::Get()->GetGameStatus()->GetMusicMute() )
        PlayMusic();
}
//----------------------------------------------------------------------------------
void BackgroundMusic::SetMusic_Menu()
{
    _music = eMusicMenu;
    if ( ! Game::Get()->GetGameStatus()->GetMusicMute() )
        PlayMusic();
}
//----------------------------------------------------------------------------------
void BackgroundMusic::SetMusic_Game()
{
    _music = eMusicGame;
    if ( ! Game::Get()->GetGameStatus()->GetMusicMute() )
        PlayMusic();
}
//----------------------------------------------------------------------------------
void BackgroundMusic::PlayMusic()
{
    // is it played or stopped?
    if ( _music != _musicLastPlayed  )
    {
        switch ( _music )
        {
            case eMusicIntro:
                SimpleAudioEngine::sharedEngine()->playBackgroundMusic( "Sounds/MusicIntro.mp3", false );
                break;

            case eMusicMenu:
                SimpleAudioEngine::sharedEngine()->playBackgroundMusic( "Sounds/MusicMenu.mp3", true );
                break;
                
            case eMusicGame:
                SimpleAudioEngine::sharedEngine()->playBackgroundMusic( "Sounds/MusicGame.mp3", true );
                break;
            
            default:
                unAssertMsg(BackgroundMusic, false, ("Problem"));
        }
    }
    else
        if ( ! SimpleAudioEngine::sharedEngine()->isBackgroundMusicPlaying() )
            SimpleAudioEngine::sharedEngine()->resumeBackgroundMusic();

   	_musicLastPlayed = _music;
}
//----------------------------------------------------------------------------------

