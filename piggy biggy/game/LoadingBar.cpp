#include "LoadingBar.h"
#include "Skins.h"
#include "Scenes/MainMenuScene.h"
#include "Game.h"
#include "Platform/Lang.h"

//--------------------------------------------------------------------------------------------
// Loading Sprite
//--------------------------------------------------------------------------------------------
LoadingSprite* LoadingSprite::Get()
{
    if ( Game::Get()->GetLastLevelOrientation() == eLevel_Vertical )
        return GetP();
	else
        return GetL();
}
//--------------------------------------------------------------------------------------------
LoadingSprite* LoadingSprite::GetP()
{
    LoadingSprite *sprite;
    sprite = new LoadingSprite();
    if (sprite && sprite->initWithFile( "Images/Other/none.png" ))
    {
        sprite->autorelease();
        sprite->Init(
                     /*Lang::Get()->GetLang*/( "Images/Other/LoadingPortrait.png" ),
                     Lang::Get()->GetLang( "Images/Other/LoadingPortraitTxt.png"));
        
        return sprite;
    }
    return NULL; 
}
//--------------------------------------------------------------------------------------------
LoadingSprite* LoadingSprite::GetL()
{
    LoadingSprite *sprite;
    sprite = new LoadingSprite();
    if (sprite && sprite->initWithFile( "Images/Other/none.png" ))
    {
        sprite->autorelease();
        sprite->Init(
                     /*Lang::Get()->GetLang*/( "Images/Other/LoadingLandscape.png" ),
                     Lang::Get()->GetLang( "Images/Other/LoadingLandscapeTxt.png"));
        
        return sprite;
    }
    return NULL; 
}
//--------------------------------------------------------------------------------------------
LoadingSprite::LoadingSprite()
{
}
//--------------------------------------------------------------------------------------------
LoadingSprite::~LoadingSprite()
{
    
}
//--------------------------------------------------------------------------------------------
void LoadingSprite::Init( const char *bgFilename, const char *txtFilename )
{
    D_CSTRING(bgFilename)
    D_CSTRING(txtFilename)
    
    CCSize size;
	size = CCDirector::sharedDirector()->getWinSize();

    _background = CCSprite::spriteWithFile( bgFilename );
	_background->setPosition( CCPointMake( 0.0f, 0.0f ));
	addChild( _background );

    _txtSprite = CCSprite::spriteWithFile( txtFilename );
    _txtSprite->setPosition( CCPointMake( 0.0f, 0.0f ));
    addChild( _txtSprite );
}
//--------------------------------------------------------------------------------------------
void LoadingSprite::setOpacity(GLubyte var)
{
    CCSprite::setOpacity( var );
    _txtSprite->setOpacity(var);
    _background->setOpacity(var);
}


//--------------------------------------------------------------------------------------------
// LoadingBar
//--------------------------------------------------------------------------------------------
LoadingBar* LoadingBar::Get()
{
	return Get( NULL );
}
//--------------------------------------------------------------------------------------------
LoadingBar* LoadingBar::GetAdjusted()
{
    return Get( NULL );
}
//--------------------------------------------------------------------------------------------
LoadingBar* LoadingBar::Get( const char* filename )
{
	LoadingBar *pRet = new LoadingBar();
	if (pRet && pRet->init( filename ))
	{ 
		pRet->autorelease();
		return pRet;
	} 
	else
	{
		delete pRet;
		pRet = NULL;
		return NULL;	
	} 

	return pRet;
}
//--------------------------------------------------------------------------------------------
bool LoadingBar::init( const char *filename )
{
	if ( ! CCLayer::init() )
		return false;

    CCSize size;
	size = CCDirector::sharedDirector()->getWinSize();

    if ( !filename )
    {
        CCNode *node = LoadingSprite::Get();
        node->setPosition( CCPointMake( size.width / 2.0f, size.height / 2.0f ));
        addChild( node, 10 );
        return true;
    }

  	CCSprite *background;
    background = CCSprite::spriteWithFile( filename );
	background->setPosition( CCPointMake( size.width / 2.0f, size.height / 2.0f ));
	addChild( background );

	return true;
}
//--------------------------------------------------------------------------------------------
LoadingBar::LoadingBar()
{
	_isSchedule = false;
	_delayCounter = 0;
}
//--------------------------------------------------------------------------------------------
LoadingBar::~LoadingBar()
{
	D_HIT
}
//--------------------------------------------------------------------------------------------
void LoadingBar::AddToScene( FuncPtr func, void *data )
{
	CCScene *scene;
	scene = CCDirector::sharedDirector()->getRunningScene();

	_func = func;
	_funcData = data;

	if ( getParent() )
		removeFromParentAndCleanup( false );

	setPosition( ccp( 0.0f, 0.0f ));
	scene->addChild( this, 254 );
	StartScheduler();
}
//--------------------------------------------------------------------------------------------
void LoadingBar::Step( cocos2d::ccTime dt )
{
	if ( _delayCounter++ >= 1 )
	{
		_func( _funcData );
		unschedule( schedule_selector ( LoadingBar::Step ));
        removeFromParentAndCleanup( false );
		_isSchedule = false;
	}
}
//--------------------------------------------------------------------------------------------
void LoadingBar::StartScheduler()
{
	if ( ! _isSchedule )
	{
		schedule( schedule_selector( LoadingBar::Step ));
		_isSchedule = true;
	}
}
//--------------------------------------------------------------------------------------------



//--------------------------------------------------------------------------------------------
