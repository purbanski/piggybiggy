#ifndef __LEVELFACTORY_H__
#define __LEVELFACTORY_H__

#include <vector>
#include <map>
#include "Levels/Level.h"
#include "GameTypes.h"

using namespace std;
//-------------------------------------------------------------------------------------------
class LevelFactory
{
public:
	LevelFactory();
	~LevelFactory();

	Level* CreateLevel( GameTypes::LevelEnum levelEnum, bool managed = true );
	Level* CreateDemoLevel( GameTypes::LevelEnum levelEnum );
    Level* CreateViewLevel( GameTypes::LevelEnum levelEnum );

private:
	typedef Level* ( LevelFactory::*FuncPtr )( GameTypes::LevelEnum levelEnum, bool managed, bool viewMode );
	typedef map<GameTypes::LevelEnum, FuncPtr>	LevelCreationMap;

	LevelCreationMap	_levelMap;
	
//-------------------------------------------------------------------------------------------
	template <class L>
	Level *NewLevel( GameTypes::LevelEnum levelEnum, bool managed, bool viewMode = false )
	{
		Level *l = NULL;

		if ( managed )
		{
			l = (Level *) new L( levelEnum, viewMode );
			if ( l && l->init()) 
			{
				l->autorelease();
			}
			else
			{
				delete l;
				l = NULL;
			}
		}
		else
		{
			unAssertMsg( LevelFactory, false, ("Unmanaged level!"));
			l = (Level *)  new L( levelEnum, viewMode );
		}
		return l;
	}
};
//-------------------------------------------------------------------------------------------
#endif
