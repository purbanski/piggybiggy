#include "GameTypes.h"
#include "unDebug.h"

#ifdef BUILD_EDITOR
#include "Editor/puBlockDef.h"
#endif


//--------------------------------------------------

using namespace GameTypes;

//--------------------------------------------------

GameTypes::ChainConstInfo::ChainConstInfo()
{
	_type = eChainByUnknown;
	_isHookedBlockFixed = false;

	_chainFixPoint.SetZero();
	_chainEndPoint.SetZero();

	_radius = 0;
	_piece = 0;

	_lengthA = 0.0f;
	_lengthB = 0.0f;
	_lengthC = 0.0f;

}

GameTypes::ChainConstInfo::ChainConstInfo( const b2Vec2& chainFixPoint, const b2Vec2& chainEndPoint )
{
	_type = eChainByPoints;
	_isHookedBlockFixed = false;

	_chainFixPoint = chainFixPoint;
	_chainEndPoint = chainEndPoint;

	_lengthA = 0.0f;
	_lengthB = 0.0f;
	_lengthC = 0.0f;

	_radius = 0;
	_piece = 0;
}

GameTypes::ChainConstInfo::ChainConstInfo( float radius, float piece )
{
	_type = eChainByRadius;
	_isHookedBlockFixed = false;

	_chainFixPoint.SetZero();
	_chainEndPoint.SetZero();

	_lengthA = 0.0f;
	_lengthB = 0.0f;
	_lengthC = 0.0f;

	_radius = radius;
	_piece = piece;
}

GameTypes::ChainConstInfo::ChainConstInfo( Type type, float f1, float f2, float f3 )
{
	_type = type;
	_isHookedBlockFixed = false;

	switch ( _type )
	{
		case eChainByRadiusAndLength :
			_chainFixPoint.SetZero();
			_chainEndPoint.SetZero();
			
			_lengthA = 0.0f;
			_lengthB = f2;
			_lengthC = f3;

			_radius = f1;
			_piece = 0.0f;
		break;
		
		case eChainByCubicLengths :
			_chainFixPoint.SetZero();
			_chainEndPoint.SetZero();

			_lengthA = f1;
			_lengthB = f2;
			_lengthC = f3;

			_radius = 0.0f;
			_piece = 0.0f;
			break;

		default:
			//unAssertMsg(ChainConstInfo, false, (("Wrong arg %d"), _type ));
		;
	}

}

GameTypes::ChainConstInfo::ChainConstInfo( float length )
{
	_type = eChainByLength;
	_isHookedBlockFixed = false;

	_chainFixPoint.SetZero();
	_chainEndPoint.SetZero();

	_lengthA = length;
	_lengthB = 0.0f;
	_lengthC = 0.0f;

	_radius = 0;
	_piece = 0;
}


#ifdef BUILD_EDITOR
GameTypes::ChainConstInfo::ChainConstInfo( const puBlockDef *blockDef )
{
	_type = blockDef->_chainInfo._type;

	_chainFixPoint = blockDef->_chainInfo._chainFixPoint;
	_chainEndPoint = blockDef->_chainInfo._chainEndPoint;

	_lengthA = blockDef->_chainInfo._lengthA;
	_lengthB = blockDef->_chainInfo._lengthB;
	_lengthC = blockDef->_chainInfo._lengthC;

	_radius = blockDef->_chainInfo._radius;
	_piece = blockDef->_chainInfo._piece;

	_isHookedBlockFixed = blockDef->_chainInfo._isHookedBlockFixed;
}
#endif

void GameTypes::SecondsHolder::Reset()
{
	//_days = 0.0f;
	//_hours = 0.0f;
	//_minutes = 0.0f;
	_seconds = 0;

}

#define SECONDS_IN_MINUTE 60
#define MINUTE_IN_HOUR 60
#define HOUR_IN_DAY 24

//---------------------------------------------------------------------
unsigned int GameTypes::SecondsHolder::GetSecondsCount()
{
	return ( _seconds % SECONDS_IN_MINUTE  );
}
//---------------------------------------------------------------------
unsigned int GameTypes::SecondsHolder::GetMinutesCount()
{
	return ( _seconds / SECONDS_IN_MINUTE  ) %  MINUTE_IN_HOUR;
}
//---------------------------------------------------------------------
unsigned int GameTypes::SecondsHolder::GetHoursCount()
{
	return (( _seconds / ( SECONDS_IN_MINUTE  * MINUTE_IN_HOUR )) %  HOUR_IN_DAY );
}
//---------------------------------------------------------------------
unsigned int GameTypes::SecondsHolder::GetDaysCount()
{
	return ( _seconds / ( SECONDS_IN_MINUTE  * MINUTE_IN_HOUR *  HOUR_IN_DAY ));
}
//---------------------------------------------------------------------

#undef SECONDS_IN_MINUTE
#undef MINUTE_IN_HOUR
#undef HOUR_IN_DAY
