#include "GameConfig.h"

float RATIO = 10.0f;

const char* Config::GameName                = "Piggy Biggy";
const char* Config::AppStoreAppId           = "972278141"; //972293472" iap id
const char* Config::AppStoreProductName     = "PiggyBiggy.LevelPack.01";
const char* Config::AppVersion              = "1.0";
const char* Config::HansSFont               = "ChineseSFonts/test.ttf";

const float Config::FBRequesterCheckEverySec        = 5;
const float Config::FBNotifyRequestCheckEverySec    = 5;

const char* Config::FBAppId                 = "139856902854416";
const char* Config::FBOpenGraphURL          = "http://www.piggybiggy.com/scripts/facebook/ogp/";
const char* Config::FBCustomPostImagesURL   = "http://www.piggybiggy.com/scripts/facebook/post_images/";
const char* Config::FBScoreURL              = "http://www.piggybiggy.com/scripts/scores/";
const char* Config::FBRequestsURL           = "http://www.piggybiggy.com/scripts/scores/";
const char* Config::StatsUrl                = "http://www.piggybiggy.com/scripts/piggystats/";

const char* Config::FBHighScoreFont         = "TrebuchetMS-Bold";
const char* Config::FBHighScoreFontPlain    = "TrebuchetMS";
const char* Config::FBHighScorePositionFont = "Fonts/FBHighScorePlace.fnt";
const char* Config::FBRequestMsgFont        = "TrebuchetMS-Bold";

const unsigned int Config::FBFriendPickerFriendCount    = 20;
const unsigned int Config::FBLevelChallengeFrinedCount  = 15;
const unsigned int Config::FBRequestPiggyBgOpacity      = 50;

const ccColor3B Config::FBMenuTitleColor        = { 75, 2, 35 };
const ccColor3B Config::FBMenuItemColor         = { 50, 50, 50 };
const ccColor3B Config::FBRequestLabelsColor    = { 236, 233, 242 };
//const ccColor3B Config::FBRequestHelpMsgColor   = { 236, 233, 242 };
const ccColor3B Config::FBRequestHelpMsgColor   = { 102, 62, 48 };
const ccColor3B Config::ColorWhite              = { 255, 255, 255 };
const ccColor3B Config::ColorBlack              = { 0, 0, 0 };


//-------------------------------------
// URLs
//-------------------------------------
const char* Config::URLFacebook = "http://www.facebook.com/PiggyBiggy";
const char* Config::URLTwitter  = "https://twitter.com/piggy_biggy";
const char* Config::URLYouTube  = "http://youtu.be/Ox9YeYkfTXg"; //"http://www.youtube.com/user/PiggyBiggyTV";
const char* Config::URLAppStore = "https://itunes.apple.com/app/id972278141";
const char* Config::URLMarmaladeAppStore = "https://www.madewithmarmalade.com/publishing/apple/pb-56";

//-------------------------------------
// Appirater
//-------------------------------------
const int Config::AppiraterDaysUntilPrompt              = 0;
const int Config::AppiraterUsesUntilPrompt              = 0;
const int Config::AppiraterSignificantEventsUntilPrompt = 16; // completed level count
const int Config::AppiraterDaysBeforeReminding          = 2;
const bool Config::AppiraterDebug                       = false;

//-------------------------------------
// Game Watcher
//-------------------------------------
// describes the range from which randomly choosen number,
// will describe number of occurances after which
// event processing (such as facebook status update) will take place
// once this processing will be finished, random number from this
// range will be choosen and process will repeat
const RangeInt Config::Watcher_Activate_LevelCompleted( 1, 1 );
const RangeInt Config::Watcher_Activate_LevelSkipped( 1, 1 );

//-------------------------------------
// Fonts & Filenames
//-------------------------------------
const char* Config::LevelNumberFont			= "Fonts/LevelMsg.fnt";
const char* Config::LevelNameFont			= "Fonts/LevelNameFont.fnt";
const char* Config::LevelNamePlayFont		= "Fonts/LevelNamePlay.fnt";
const char* Config::LevelMsgFont			= "Fonts/LevelMsg.fnt";
const char* Config::LevelSubMenuFont		= "Fonts/LevelSubMenuFont.fnt";
const char* Config::LevelShowClueButton		= "Fonts/LevelSubMenuFont.fnt";
const char* Config::LevelClueTimerFont		= "Fonts/LevelClueTimer.fnt";
const char* Config::LevelSkipCountVioletFont= "Fonts/LevelSkipCountViolet.fnt";
const char* Config::LevelSkipCountRedFont	= "Fonts/LevelSkipCountRed.fnt";
const char* Config::LevelSkipCountDisabledVioletFont	= "Fonts/LevelSkipCountVioletDisabled.fnt";
const char* Config::LevelSkipCountDisabledRedFont		= "Fonts/LevelSkipCountRedDisabled.fnt";
const char* Config::LevelScoreFont			= "Fonts/LevelScoreCounter.fnt";
const char* Config::LevelStatsFont			= "Fonts/LevelStatsFont.fnt";
const char* Config::LevelFinishTimerFont	= "Fonts/LevelFinishTimerFont.fnt";
const char* Config::LevelStatsDisabledFont	= "Fonts/LevelStatsDisabledFont.fnt";
const char* Config::LevelScaleFont			= "Fonts/LevelScaleFont.fnt";

const char* Config::LevelDebugMenuFont		= "Fonts/LevelDebugMenu.fnt";
const char* Config::MemoryViewFont			= "Fonts/MemoryView.fnt";


const char* Config::SelectLevelMenuFont		= "Fonts/MotherCounter.fnt";
const char* Config::SelectLevelScoreTime	= "Fonts/SelectLevelScoreTime.fnt";

const char* Config::MainMenuFont			= "Fonts/MainMenuFont.fnt";
const char* Config::DefaultClueFont			= "Fonts/LevelSubMenuFont.fnt";
const char* Config::MotherCounterFont		= "Fonts/MotherCounter.fnt";

const char* Config::DebugNodeFont			= "Fonts/DebugNode.fnt";
const char* Config::GameStatusFilename		= "GameStatus.dat";


const char * Config::ConfigFileExt			= "ini";

//-------------------------------------
// Other
//-------------------------------------
const float Config::ShowBuySceneSkipTime            = 0.75f;
const float Config::LevelShotChangeDelay            = 2.0f;

const int Config::FreeLevelCount					= 24;
const int Config::GameSkipMaxCount					= 15;
const int Config::GameDemoChangeCounter				= 800;
const int Config::LevelStateChangeDelay				= 50;
const int Config::LevelStateChangeDelayIntroPointer	= 430;
const int Config::LevelStateChangeToCompletedDelay	= 90;

const int Config::LevelIconAnimateInitDelay			= 200;
const int Config::LevelIconAnimateCountDown			= 330;
const int Config::LevelButtonOpacity				= 120;
const int Config::LevelFirstMsgTick					= 150;

const b2Vec2 Config::LevelAccelerometrBallPosition( 38.0f, 26.0f );
const float Config::SoundEffectVol					= 1.0f;
const float Config::SoundMusicVol					= 0.7f;
const int Config::SoundChannelRate					= 44100 * 2;
const int Config::SoundStereoEnabled				= 1;
const int Config::SoundWavHeaderSize				= 598;
const int Config::GameTotalLevelCount                = 102;

const char *Config::TestFlightId        = "e715dad9ca822f835ccbe0c2efdf3f90_MTE5Njg3MjAxMi0wOC0wOSAxNDowMjo0OS45NDAyODQ";
const char *Config::GoogleAnalyticsID       = "UA-38130172-16";
const int   Config::GoogleAnalyticsDelay    = 5;

const int Config::ClueManagerTickDelay  = 20;
const unsigned int Config::ShowSkipIntro_LevelRestarCount = 3;

const b2Color Config::ChainStrongColor( 0.2f, 0.2f, 0.2f );
const b2Color Config::ChainCutableColor( 7.0f / 255.0f, 113.0f / 255.0f, 240.0f / 255.0f );

//-------------------------------------
// World
//-------------------------------------
const int Config::WorldVelocityIterations     = 9; // erico recommends 8
const int Config::WorldPositionIterations     = 15; // erico recommends 3
                                                    // dont go belowe 13 hitting block cause unaccurate coolision

const int Config::WorldVelocityIterationsHigh = 16;
const int Config::WorldPositionIterationsHigh = 16;

const int Config::WorldGunVelocityIterations = 25; // when we use bullets speed iterations should be more
const int Config::WorldGunPositionIterations = 11; // when we use bullets position iterations should be more



//-------------------------------------
// Anim
//-------------------------------------
const int Config::AnimCircleDefaultSize		= 60;
const float Config::AnimDayLong = 8.5f; 
const float Config::AnimRotationStartLimit = 10.0f;
const float Config::AnimRotationSlowMovingStartLimit = 45.0f;

// into sitting, into rolling
const b2Vec2 Config::AnimFastSpeedLinearLimits( 5.0f, 10.0f );
const b2Vec2 Config::AnimFastSpeedAngularLimits( 0.20f, 0.75f );

const b2Vec2 Config::AnimSlowSpeedLimits( 0.5f, 0.05f ); 
const float Config::AnimSlowSpeedTimeLimit = 2.0f;


const float Config::WaterBouyancy		= 10.0f;
const float Config::WaterWaveFrequency	= 0.2f;
const b2Vec2 Config::WaterWaveForce		= b2Vec2( 250.0f, 0.0f );


const CCSize Config::GameSize( 960.0f, 640.0f );

const int Config::EditorMenuOpacity		= 220;
const int Config::BlockTint				= 210;
const int Config::LevelNotifiersOpacity = 190;

const float Config::PageTurnDur         = 0.73f;

const int Config::AnimPiggy360FrameCount    = 60;
const int Config::AnimThief360FrameCount    = 54;
const int Config::AnimCop360FrameCount      = 54;

//-------------------------------------
// Scrore Limits
//-------------------------------------
const int Config::ScoreTime5Coins = 120;
const int Config::ScoreTime4Coins = 300;
const int Config::ScoreTime3Coins = 540;
const int Config::ScoreTime2Coins = 840;
const int Config::ScoreTime1Coins = 1200;
const int Config::MaxScoreLevelCoins = Config::ScoreTime1Coins + 120;

//const int Config::ScoreTime5Coins = 10;
//const int Config::ScoreTime4Coins = 15;
//const int Config::ScoreTime3Coins = 20;
//const int Config::ScoreTime2Coins = 25;
//const int Config::ScoreTime1Coins = 30;

//-------------------------------------
// Shadows, Glue etc
//-------------------------------------
const int Config::ShadowOpacity = 40;
const b2Vec2 Config::ShadowPosition( 15.0f, -15.0f );

const float Config::SpriteGlueOutBorder = 36.5f;
const float Config::SpriteGlueDestroyDelay = 4.0f;
const float Config::SpriteGlueDestroyFadeDur = 2.0f;


const int Config::PocketLevelOpenToFinish = 19;

const float Config::OrientationArrowsAnimDuration = 1.0f;
const float Config::OrientationArrowsAnimRotation = 45.0f;

const int Config::MotherCounterEndOpacity = 240;


//-------------------------------------
// Game Center
//-------------------------------------
// Leader boards
const char *Config::GameCenter_LeaderBoard_TotalScore       = "pg.leader.totalscore";
const char *Config::GameCenter_LeaderBoard_JeansScore       = "pg.leader.jeansscore";
const char *Config::GameCenter_LeaderBoard_MoroScore        = "pg.leader.moroscore";
const char *Config::GameCenter_LeaderBoard_RastaScore       = "pg.leader.rastascore";
const char *Config::GameCenter_LeaderBoard_KimonoScore      = "pg.leader.kimonoscore";
const char *Config::GameCenter_LeaderBoard_ProfessorScore   = "pg.leader.professorscore";

//------------
// Achievement
const char *Config::GameCenter_Achievement_GameProgress     = "pg.ach.gameprogress";
const char *Config::GameCenter_Achievement_Hacker           = "pg.ach.hacker";

const char *Config::GameCenter_Achievement_Bank             = "pg.ach.bank";
const char *Config::GameCenter_Achievement_Car              = "pg.ach.car";
const char *Config::GameCenter_Achievement_Reel             = "pg.ach.reel";
const char *Config::GameCenter_Achievement_Rotate           = "pg.ach.rotate";
const char *Config::GameCenter_Achievement_Bomb             = "pg.ach.bomb";
const char *Config::GameCenter_Achievement_Spitter          = "pg.ach.spitter";

const char *Config::GameCenter_Achievement_JeansPocket      = "pg.ach.jeanspocket";
const char *Config::GameCenter_Achievement_MoroPocket       = "pg.ach.moropocket";
const char *Config::GameCenter_Achievement_RastaPocket      = "pg.ach.rastapocket";
const char *Config::GameCenter_Achievement_KimonoPocket     = "pg.ach.kimonopocket";
const char *Config::GameCenter_Achievement_ProfessorPocket  = "pg.ach.professorpocket";
