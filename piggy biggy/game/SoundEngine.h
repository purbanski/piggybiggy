#ifndef __SOUNDENGINE_H__
#define __SOUNDENGINE_H__

#include <map>
#include <string>
#include <set>
#include "cocos2d.h"
#include "unTypes.h"
#include "SimpleAudioListener.h"

using namespace std;	
USING_NS_CC;

//----------------------------------------------------------------------------------
// Background
//----------------------------------------------------------------------------------
class BackgroundMusic
{
public:
	BackgroundMusic();
	~BackgroundMusic();

	typedef enum
	{
		eMusicNone = 0,
		eMusicGame,
		eMusicMenu,
  		eMusicIntro
	} BgMusic;

public:
	void SetMusicMute( bool mute );
	bool GetMusicMute();

	void SetMusic_Intro();
	void SetMusic_Menu();
	void SetMusic_Game();
	void PauseMusic();
	void ResumeMusic();

private:
	void PlayMusic();
	void doPlayMusic( BgMusic music );

private:
	BgMusic		_music;
	BgMusic		_musicLastPlayed;
};


//----------------------------------------------------------------------------------
// Sound Engine
//----------------------------------------------------------------------------------

struct WaveHeader
{
	char	_description[4];
	int		_size; 
	char	_descriptionWAV[4];
	char	_descriptionFormat[4];
	int		_wavSize;
	short	_waveType;
	short	_channelNumber;
	int		_samplePerSecond;
	int		_bytesPerSecond;
	short	_blockAlignment;
	short	_bitsPerSample;
	int		_descriptionData;
	int		_sizeData;
	WaveHeader();
};

typedef unsigned int SoundId;

class SoundEngine : public SimpleAudioListener
{
public:
	typedef enum
	{
		eSoundBugFix = -1,
		eSoundNone = 0,
		eSoundCoinCollected = 1,
		eSoundMoneyMakeMoney,
		eSoundThiefCaught,
		eSoundLevelRestart,
		eSoundLevelFailed,
		eSoundLevelSkipped,
		eSoundLevelFinished,
		eSoundFragileDestroyed1,
		eSoundFragileDestroyed2,
		eSoundFragileDestroyed3,
		eSoundFragileDestroyed4,
		eSoundBombExploded1,
		eSoundBombExploded2,
		eSoundBombExploded3,
		eSoundBankLockBarsSlide,
		eSoundBankLockUnlocked,
		eSoundBankLockLocked,
		eSoundBankSaveSmallWheel,
		eSoundBankSaveBigWheel,
		eSoundBankSaveSlideUp,
		eSoundBankSettingWheelPryk,
		eSoundBoxerGlovePunch,
		eSoundSlidingDoorOpen,
		eSoundSlidingDoorClose,
		eSoundSpit,
		eSoundSpitEmpty,
		eSoundCarBreaking1,
		eSoundCarBreaking2,
		eSoundCarBreaking3,
		eSoundCarStartAndRun,
		eSoundPigSnort,
		eSoundPiggyNewBorn,
		eSoundCoinNewBorn,
		eSoundPrisonAlert,
		eSoundFireworks1,
		eSoundFireworks2,
		eSoundChangeScene,			//not preloaded
		eSoundButtonClick,			//not preloaded
		eSoundChainCut,	
		eSoundNewClue,				//not preloaded
		eSoundCoinScored,
		eSoundCoinScoredReversed,
		eSoundAirBubblePop,			//not preloaded
		eSoundAirBubbleGrow,		//not preloaded
		eSoundPocketUnlock,
		eSoundSlidingNode_Slide,
		eSoundSlidingNode_SlideBack,
		eSoundLevelNameShow,
		eSoundLevelNameHide,
        eSoundPageTurn,
		
		eSoundScoreTimeTick,
		eSoundScoreTimeTickFinal,

		eSoundAnim_PiggyUaa,
		eSoundAnim_PiggyEeeOoo,
		eSoundAnim_PiggyFiuFiu,
		eSoundAnim_PiggySalto,
		eSoundAnim_PiggyNoseBubble,
		eSoundAnim_PiggyHappyJump,

		eSoundAnim_PiggyStolen,

		eSoundAnim_PiggyToRolling,
		eSoundAnim_PiggyFromRolling,
		
		eSoundAnim_PiggyLevelFailed_Sit,
		eSoundAnim_PiggyLevelFailed_Roll,
		
		eSoundAnim_PiggyLevelDone_Sit,
		eSoundAnim_PiggyLevelDone_Roll,

		eSoundAnim_PiggyCoinCollected_Sit,
		eSoundAnim_PiggyCoinCollected_Roll,
		eSoundAnim_PiggyLookAround,
		eSoundAnim_FlyFly1,
		eSoundAnim_FlyFly2,
		eSoundAnim_FlyFly3,

		eSoundAnim_ThiefSitItemStolen,
		eSoundAnim_ThiefRollItemStolen,


		eSoundAnim_PiggyFart,
		eSoundAnim_CoinBlink,
		eSoundAnim_PiggyRotationFix,
		eSoundLast,

	} Sound;

	typedef set<Sound> SoundSet;
	
	
//---------------------------------------------------------
	static const float FADE_OUT_STEP;
//---------------------------------------------------------
	static SoundEngine* Get();
	static void Destroy();
	~SoundEngine();
//---------------------------------------------------------
	void Update();

	void PlayRandomEffect( int effect, ... );
	void PlayRandomEffect( float freDeltaPerc, int effect, ... );

    virtual void EffectFinished( unsigned int soundId );
	int PlayEffect( Sound effect, bool loop = false );
	int PlayEffect( Sound effect, float freDeltaPerc );
	float GetEffectDuration( Sound effect );
	

	void PlayEffect_CarStartAndRun();
	void AdjustEffect_CarStartAndRun( float speed );
	void StopEffect_CarStartAndRun();
    void StopBackgroundMusic();
    
	void SetEffectsMute( bool mute );
	bool GetEffectsMute();

	void PauseAllEffects();
	void PauseBackgroundMusic();
	void ResumeBackgroundMusic();
	void ResumeAllEffects();
    void FadeOutAllEffects();
    
	void Reset();

	void SetMusicVolume( float volume);
	void SetEffectsVolume( float volume);
	float GetMusicVolume();
	float GetEffectsVolume();

	BackgroundMusic& GetBackgroundMusic(); 

	void LoadSound( Sound sound );
	void UnloadSound( Sound sound );
	void UnloadAllSounds();

    void PlayEffectCallback( CCObject *, void *sound );

private:
	SoundEngine();
	void InitSoundMap();
	void PreloadAllEffects();

    void UnloadStoppedSounds();
	
	void FadeOutSounds();
	void AddToFadeOut( SoundId soundId );
	void CancelFadeOut( SoundId soundId );


	static SoundEngine* _sInstance;


	//-----------------------------------
	// EffectData
	//-----------------------------------
	struct EffectData
	{
		string		filename;
        bool        isLoaded;
        float       duration;

		EffectData() 
		{
            duration = 0.0f;
			filename.clear();
            isLoaded = false;
		}
	};


	//------------------------------------
	// SoundHit - not sure why i need this
	//------------------------------------
	struct SoundHit
	{
		int hitCount;
		SoundHit() : hitCount(0)
		{}
	};


private:
	typedef std::map<SoundId, Sound>	SoundPlayedMap;
	typedef std::map<int, SoundHit>     SoundsHitMap;
	typedef std::map<int, EffectData>   SoundsMap;
	typedef std::map<string, Sound>     FilenameToSoundMap;
    
	SoundsMap           _soundsMap;
	SoundsHitMap		_soundsPlayedCountMap;
	SoundPlayedMap		_soundsPlayedMap;
    
	FilenameToSoundMap	_filenameToSoundMap;
	SoundSet			_soundsAlwaysLoaded;

	//------------------------------------
	// fading car sounds ??
	//------------------------------------
	typedef std::set<SoundId>	SoundIdSet;
	SoundIdSet					_soundsToBeFadeOut;

	BackgroundMusic			_backgroundMusic;
    bool                    _musicMute;
    bool                    _effectsMute;
};

#endif
