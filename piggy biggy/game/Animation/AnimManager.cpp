#include "AnimManager.h"
#include "AnimSpritesCreator.h"
#include "GameConfig.h"
#include "Debug/MyDebug.h"
#include "Skins.h"
#include "Tools.h"
#include <sstream>
#include "Game.h"
#include "Levels/Level.h"

using namespace GameTypes;

//----------------------------------------------------------------------------------
// AnimerManager
//----------------------------------------------------------------------------------
AnimManager * AnimManager::_sInstance = NULL;

AnimManager* AnimManager::Get()
{
	if ( ! _sInstance )
		_sInstance = new AnimManager();
	return _sInstance;
}
//----------------------------------------------------------------------------------
void AnimManager::Destroy()
{
	if (  _sInstance )
	{
		delete _sInstance;
		_sInstance = NULL;
	}
}
//----------------------------------------------------------------------------------
AnimManager::AnimManager()
{
	_enabled = true;
	Reset();
	D_SIZE( _animBlockDataMap )
}
//----------------------------------------------------------------------------------
AnimManager::~AnimManager()
{
	Reset();
}
//----------------------------------------------------------------------------------
void AnimManager::PlayAnimUnmanaged( GameTypes::CharactersAnim anim, puBlock *block )
{
	_animer.PlayAnim( anim, block );
}
//----------------------------------------------------------------------------------
void AnimManager::PlayAnim( GameTypes::CharactersAnim anim, GameTypes::CharactersAnimPiority piority, puBlock *block )
{
	if ( ! _enabled )
		return;

	if ( ! block || ! block->GetSprite() || ! block->GetSprite()->getIsVisible() )
		return;

	AnimIdType id;
	id = GetNextId();

	if ( _animBlockDataMap.count( block ))
	{
		AnimManagerData data;
		data = _animBlockDataMap[ block ];

	
		//fix me
		if ( _animBlockDataMap[ block ].GetAnim() == GameTypes::eAnim_PiggyBankRotationFix )
			return;
	
		if ( piority > data.GetAnimPiority() )
		{
			AnimFinished( block );
		}
		else
			return;
		
	}

	AnimManagerData data( id, anim, block, piority );
	_animBlockDataMap[ block ] =  data;
	_animBlockDataMap[ block ].SetAnimState( GameTypes::eAnimState_Running );

	_animer.PlayAnim( anim, block );
}
//----------------------------------------------------------------------------------
AnimIdType AnimManager::GetNextId()
{
	_currentId++;
	return _currentId;
}
//----------------------------------------------------------------------------------
AnimIdType AnimManager::GetCurrentId()
{
	return _currentId;
}
//----------------------------------------------------------------------------------
void AnimManager::AnimFinished( puBlock *block )
{

	if ( ! _animBlockDataMap.count( block ) )
		return;

	AnimManagerData animData;
	animData = _animBlockDataMap[ block ];
	animData.SetBounceAnim();

	AnimBlockDataMap::iterator it;
	it = _animBlockDataMap.find( block );

	if ( it == _animBlockDataMap.end() )
	{
		unAssertMsg(Game, false, ("Something wrong "));
		return;
	}
	else
		_animBlockDataMap.erase( it );

	//SetSpriteState( block, eSpriteState_Sit );
}
//----------------------------------------------------------------------------------
void AnimManager::AnimFinishedNoReset( puBlock *block )
{

	AnimManagerData animData;
	animData = _animBlockDataMap[ block ];
	_animBlockDataMap.erase( _animBlockDataMap.find( block ));

	//SetSpriteState( block, eSpriteState_Roll );
}
//----------------------------------------------------------------------------------
GameTypes::AnimState AnimManager::GetAnimState( puBlock *block )
{

	if ( _animBlockDataMap.count( block ))
		return _animBlockDataMap[ block ].GetAnimState();
	else
		return GameTypes::eAnimState_None;
}
//----------------------------------------------------------------------------------
void AnimManager::Reset()
{
	_currentId = 0;
	_animBlockDataMap.clear();
	_blockAnimSpriteStateMap.clear();
}
//----------------------------------------------------------------------------------
void AnimManager::PauseAllActions( Level *level )
{
	b2Body *body;
	puBlock *block;
	CCSprite *sprite;
	CCSprite *spriteShadow;

	body = level->GetWorld()->GetBodyList();

	while( body )
	{
		block = (puBlock*) body->GetUserData();
		if ( ! block )
		{
			body = body->GetNext();
			continue;
		}

		sprite = block->GetSprite();
		spriteShadow = block->GetSpriteShadow();

		if ( sprite )
		{
			CCScheduler::sharedScheduler()->pauseTarget( sprite );
			CCActionManager::sharedManager()->pauseTarget( sprite );
		}

		if ( spriteShadow )
		{
			CCScheduler::sharedScheduler()->pauseTarget( spriteShadow );
			CCActionManager::sharedManager()->pauseTarget( spriteShadow );
		}

		body = body->GetNext();
	}
}
//----------------------------------------------------------------------------------
void AnimManager::ResumeAllActions( Level *level )
{

	b2Body *body;
	puBlock *block;
	CCSprite *sprite;
	CCSprite *spriteShadow;

	body = level->GetWorld()->GetBodyList();

	while( body )
	{
		block = (puBlock*) body->GetUserData();
		if ( ! block )
		{
			body = body->GetNext();
			continue;
		}

		sprite = block->GetSprite();
		spriteShadow = block->GetSpriteShadow();

		if ( sprite )
		{
			CCScheduler::sharedScheduler()->resumeTarget( sprite );
			CCActionManager::sharedManager()->resumeTarget( sprite );
		}

		if ( spriteShadow )
		{
			CCScheduler::sharedScheduler()->resumeTarget( spriteShadow );
			CCActionManager::sharedManager()->resumeTarget( spriteShadow );
		}

		body = body->GetNext();
	}
}
//----------------------------------------------------------------------------------
void AnimManager::SetSpriteState( puBlock *block, AnimSpriteState state )
{
	_blockAnimSpriteStateMap[ block ] = state;
}
//----------------------------------------------------------------------------------
GameTypes::AnimSpriteState AnimManager::GetSpriteState( puBlock *block )
{
	return _blockAnimSpriteStateMap[ block ];
}
//----------------------------------------------------------------------------------
CCSprite* AnimManager::CreateSitSprite( puBlock *block, float fps )
{
	return _spriteAnimCreator.CreateSitSprite( block, fps );
}
//----------------------------------------------------------------------------------
CCSprite* AnimManager::CreateRollSprite( puBlock *block, float fps )
{
	return _spriteAnimCreator.CreateRollSprite( block, fps );
}
//----------------------------------------------------------------------------------
CCSprite* AnimManager::CreatePlainSprite( puBlock *block, float fps )
{
	return _spriteAnimCreator.CreatePlainSprite( block, fps );
}
//----------------------------------------------------------------------------------
void AnimManager::PauseAnimActions()
{
	puBlock *block;

	for ( AnimBlockDataMap::iterator it = _animBlockDataMap.begin(); it != _animBlockDataMap.end(); it++ )
	{
		block = it->first;
		if ( block && block->GetSprite() )
		{
			CCScheduler::sharedScheduler()->pauseTarget( block->GetSprite() );
			CCActionManager::sharedManager()->pauseTarget( block->GetSprite() );
		}
	}
}
//----------------------------------------------------------------------------------
void AnimManager::PreloadAnim( GameTypes::CharactersAnim anim, float radius )
{
	_animer.LoadToCache( anim, radius );
}
//----------------------------------------------------------------------------------
void AnimManager::Enable()
{
	_enabled = true;
}
//----------------------------------------------------------------------------------
void AnimManager::Disable()
{
	_enabled = false;
}
//----------------------------------------------------------------------------------
AnimCharacters::BlockMap AnimManager::GetRotationFixMap()
{
	return _animer.GetRotationFixMap();
}
//----------------------------------------------------------------------------------



//----------------------------------------------------------------------------------
// AnimerManagerData
//----------------------------------------------------------------------------------
AnimManagerData::AnimManagerData()
{
	_id = 0;
	_animState = GameTypes::eAnimState_None;
	_animEnum = GameTypes::eAnim_None;
	_block = NULL;
	_animPiority = GameTypes::eAnimPiority_Low;
}
//----------------------------------------------------------------------------------
AnimManagerData::AnimManagerData( AnimIdType id, GameTypes::CharactersAnim animEnum, puBlock *block, GameTypes::CharactersAnimPiority piority )
{
	_id = id;
	_animEnum = animEnum;
	_block = block;
	_animPiority = piority;

	
	//------------------------------
	// set orginals
	//------------------------------
	CCSprite *sprite = block->GetSprite();
	if ( block && block->GetBody() && sprite && sprite->getParent() )
	{
		sprite->removeFromParentAndCleanup( false );
		//sprite->setIsVisible( false );
	}

	CCSprite *spriteShadow = block->GetSpriteShadow();
	if ( block && block->GetBody() && spriteShadow && spriteShadow->getParent() )
	{
		spriteShadow->removeFromParentAndCleanup( false );
		//spriteShadow->setIsVisible( false );
	}

	//------------------------------
	// remove left overs // for example
	// ater piggy roll in
	//------------------------------
	//if ( block && block->GetSprite() && block->GetSprite()->getParent() )
	//{
	//	CCSprite *sprite = block->GetSprite();
	//	sprite->stopAllActions();
	//	sprite->setIsVisible( false );
	//}

	//if ( block->GetSpriteShadow() && block->GetSpriteShadow()->getParent() )
	//{
	//	CCSprite *spriteShadow = block->GetSpriteShadow();
	//	spriteShadow->stopAllActions();
	//	spriteShadow->setIsVisible( false );
	//}

	(*_block->GetSpritePtr()) = NULL;
	(*_block->GetSpriteShadowPtr()) = NULL;
}
//----------------------------------------------------------------------------------
void AnimManagerData::SetBounceAnim()
{
	if ( _block->GetSprite() && _block->GetSprite()->getParent() )
	{
		_block->GetSprite()->setIsVisible( false );
		_block->GetSprite()->stopAllActions();
		_block->GetSprite()->removeFromParentAndCleanup( false );
	}
	
	if ( _block->GetSpriteShadow() && _block->GetSpriteShadow()->getParent() )
	{
		_block->GetSpriteShadow()->setIsVisible( false );
		_block->GetSpriteShadow()->stopAllActions();
		_block->GetSpriteShadow()->removeFromParentAndCleanup( false );
	}
	
	CCSprite **sprite;
	CCSprite **spriteShadow;
	
	sprite = _block->GetSpritePtr();
	spriteShadow = _block->GetSpriteShadowPtr();

	if ( AnimManager::Get()->GetSpriteState( _block ) == GameTypes::eSpriteState_SitAnimating )
	{
		*sprite = AnimManager::Get()->CreateSitSprite( _block );
		*spriteShadow = AnimManager::Get()->CreateSitSprite( _block );

		AnimManager::Get()->SetSpriteState( _block, GameTypes::eSpriteState_Sit );
	}
	else if ( AnimManager::Get()->GetSpriteState( _block ) == GameTypes::eSpriteState_RollAnimating )
	{
		*sprite = AnimManager::Get()->CreateRollSprite( _block );
		*spriteShadow = AnimManager::Get()->CreateRollSprite( _block );

		AnimManager::Get()->SetSpriteState( _block, GameTypes::eSpriteState_Roll );
	}
	else
	{
		unAssertMsg(Game, false, ( "Problem, wrong state %d", AnimManager::Get()->GetSpriteState( _block ) ));
		if ( (*sprite) && (*sprite)->getParent() )
			(*sprite)->removeFromParentAndCleanup( false );

		if ( (*spriteShadow) && (*spriteShadow)->getParent() )
			(*spriteShadow)->removeFromParentAndCleanup( false );
		
		*sprite = AnimManager::Get()->CreateSitSprite( _block );
		*spriteShadow = AnimManager::Get()->CreateSitSprite( _block );
		AnimManager::Get()->SetSpriteState( _block, GameTypes::eSpriteState_SitAnimating );
	}

	_block->UpdateSprite();
}
//----------------------------------------------------------------------------------
void AnimManagerData::SetAnimState( GameTypes::AnimState state )
{
	_animState = state;
}
//----------------------------------------------------------------------------------
puBlock* AnimManagerData::GetBlock()
{
	return _block;
}
//----------------------------------------------------------------------------------
GameTypes::AnimState AnimManagerData::GetAnimState()
{
	return _animState;
}
//----------------------------------------------------------------------------------
GameTypes::CharactersAnim AnimManagerData::GetAnim()
{
	return _animEnum;
}
//----------------------------------------------------------------------------------
GameTypes::CharactersAnimPiority AnimManagerData::GetAnimPiority()
{
	return _animPiority;
}
//----------------------------------------------------------------------------------
AnimManagerData::~AnimManagerData()
{

}

//----------------------------------------------------------------------------------
// AnimBlockDataMap
//----------------------------------------------------------------------------------
void AnimBlockDataMap::clear()
{
    puBlock *block;
    AnimBlockDataMapBaseType::iterator it;
    
    for ( it = begin(); it != end(); it++ )
    {
        block = it->second.GetBlock();
        
        //wywalilo sie
        if ( block && block->GetSprite() )
            block->GetSprite()->stopAllActions();

        if ( block && block->GetSpriteShadow() )
            block->GetSpriteShadow()->stopAllActions();
    }
    
    AnimBlockDataMapBaseType::clear();

}
//----------------------------------------------------------------------------------
// AnimerDelayMessage
//----------------------------------------------------------------------------------
AnimerDelayMessage* AnimerDelayMessage::node( float delay, float fadeOutDur, const char *message )
{
	AnimerDelayMessage* node;
	node = new AnimerDelayMessage( delay, fadeOutDur, message );
	if ( node )
	{
		node->autorelease();
		return node;
	}
	CC_SAFE_DELETE( node );
	return NULL;
}
//----------------------------------------------------------------------------------
AnimerDelayMessage::AnimerDelayMessage( float sec , float fadeOutDur, const char *message )
{
	_startDelay = sec;
	_time = 0.0f;
	_fadeOutDur = fadeOutDur;
	_message.append( message );

	schedule( schedule_selector( AnimerDelayMessage::Step ));
}
//----------------------------------------------------------------------------------
void AnimerDelayMessage::Step( ccTime dt )
{
	_time += dt;
	if ( _time >= _startDelay )
	{
		unschedule( schedule_selector( AnimerDelayMessage::Step ));
		ShowMessage();
	}
}
//----------------------------------------------------------------------------------
void AnimerDelayMessage::ShowMessage()
{
	CCLabelBMFont *label;
	float dur;
	dur = 1.0f;

	label = CCLabelBMFont::labelWithString( _message.c_str(), Config::LevelMsgFont );
	label->setPosition( CCPointZero);
	label->setOpacity( 0 );

	addChild( label );

	{
		CCFadeTo *fadeIn = CCFadeTo::actionWithDuration( 0.5f, 255 );
		CCFadeTo *fadeOut = CCFadeTo::actionWithDuration(  _fadeOutDur, 0 );
		CCMoveBy *emptyAction = CCMoveBy::actionWithDuration( dur, CCPointZero );

		label->runAction( CCSequence::actions( fadeIn, emptyAction, fadeOut, NULL));
	}
}
//----------------------------------------------------------------------------------
