#ifndef __ANIMMANAGER_H__
#define __ANIMMANAGER_H__

#include "cocos2d.h"
#include "Blocks/puBlock.h"
#include "AnimerCharacters.h"
#include "GameTypes.h"
#include "AnimSpritesCreator.h"

//----------------------------------------------------------
USING_NS_CC;
using namespace GameTypes;
using namespace std;
//----------------------------------------------------------

typedef unsigned long AnimIdType;


//----------------------------------------------------------
// AnimManagerData
//----------------------------------------------------------
class AnimManagerData
{
public:
	AnimManagerData();
	AnimManagerData( AnimIdType id, GameTypes::CharactersAnim animEnum, puBlock *block, GameTypes::CharactersAnimPiority piority );
	~AnimManagerData();

public:
	void SetBounceAnim();
	void SetAnimState( GameTypes::AnimState state );
	
    puBlock* GetBlock();
	GameTypes::AnimState GetAnimState();
	GameTypes::CharactersAnim GetAnim();
	GameTypes::CharactersAnimPiority GetAnimPiority();

private:
	puBlock                             *_block;
	AnimIdType                          _id;
	GameTypes::AnimState                _animState;
	GameTypes::CharactersAnim           _animEnum;
	GameTypes::CharactersAnimPiority    _animPiority;
};

//----------------------------------------------------------
// AnimBlockDataMap
//----------------------------------------------------------
typedef map<puBlock*, AnimManagerData> AnimBlockDataMapBaseType;
class AnimBlockDataMap : public AnimBlockDataMapBaseType
{
public:
    void clear();
    
};

//----------------------------------------------------------
// AnimManager
//----------------------------------------------------------
class Level;
class AnimManager
{
public:
	typedef pair<CCSprite *, CCSprite*> TwoSprites;

	static AnimManager* Get();
	static void Destroy();
	
	~AnimManager();

	void PlayAnim( GameTypes::CharactersAnim anim, GameTypes::CharactersAnimPiority piority, puBlock *block );
	void PlayAnimUnmanaged( GameTypes::CharactersAnim anim, puBlock *block );
	void AnimFinished( puBlock *block );
	void AnimFinishedNoReset( puBlock *block );

	GameTypes::AnimState GetAnimState( puBlock *block );
	void Reset();
	void PreloadAnim( GameTypes::CharactersAnim anim, float radius );
	void PauseAllActions( Level *level );
	void PauseAnimActions();
	void ResumeAllActions( Level *level );

	void SetSpriteState( puBlock *block, AnimSpriteState state );
	AnimSpriteState GetSpriteState( puBlock *block );

	AnimCharacters::BlockMap GetRotationFixMap();

	CCSprite* CreateSitSprite( puBlock *block, float fps = 24.0f );
	CCSprite* CreateRollSprite( puBlock *block, float fps = 24.0f );
	CCSprite* CreatePlainSprite( puBlock *block, float fps = 24.0f );
	
	void Enable();
	void Disable();

private:
	AnimIdType GetNextId();
	AnimIdType GetCurrentId();
	AnimManager();

private:
	typedef map<puBlock*, AnimSpriteState>  BlockAnimSpriteStateMap;

	static AnimManager *_sInstance;
	
	AnimCharacters       _animer;
	AnimBlockDataMap     _animBlockDataMap;
	AnimSpritesCreator	_spriteAnimCreator;

	BlockAnimSpriteStateMap	_blockAnimSpriteStateMap;
	
	bool             _enabled;
	AnimIdType		_currentId; // reset after a while needed //fixme
};

//----------------------------------------------------------
// AnimDelayMessage
//----------------------------------------------------------
class AnimerDelayMessage : public CCNode
{
public:
	static AnimerDelayMessage* node( float delay, float fadeOutTime, const char *message );
	virtual void Step( ccTime dt );

private:
	AnimerDelayMessage( float delay, float fadeOutTime, const char *message );
	void ShowMessage();

private:
	string _message;
	float _fadeOutDur;
	ccTime _startDelay;
	ccTime _time;
};

//------------------------------------------------------------------------------------
#endif
