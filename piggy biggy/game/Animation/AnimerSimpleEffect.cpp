#include "AnimerSimpleEffect.h"
#include "AnimTools.h"
#include "GameConfig.h"
#include "Debug/MyDebug.h"
#include "Game.h"
#include "Levels/Level.h"
#include "Platform/Lang.h"

//----------------------------------------------------------------------------------
// AnimerSimpleEffect
//----------------------------------------------------------------------------------
AnimerSimpleEffect* AnimerSimpleEffect::_sInstance = NULL;

AnimerSimpleEffect* AnimerSimpleEffect::Get()
{
	if ( ! _sInstance )
		_sInstance = new AnimerSimpleEffect();
	return _sInstance;
}
//----------------------------------------------------------------------------------
void AnimerSimpleEffect::Destroy()
{
	if (  _sInstance )
	{
		delete _sInstance;
		_sInstance = NULL;
	}
}
//----------------------------------------------------------------------------------
AnimerSimpleEffect::AnimerSimpleEffect()
{
}
//----------------------------------------------------------------------------------
AnimerSimpleEffect::~AnimerSimpleEffect()
{
}
//----------------------------------------------------------------------------------
void AnimerSimpleEffect::Anim_BombDestroy( CCNode *node )
{
	float dur;
	dur = 0.3f;

	node->runAction( CCFadeTo::actionWithDuration( dur, 0 ));
	node->runAction( CCScaleTo::actionWithDuration( dur, 0.1f ));
}
//----------------------------------------------------------------------------------
void AnimerSimpleEffect::Anim_GlueDestroy( CCNode *node )
{
	if ( ! node )
	{
		unAssertMsg( Game, false, ("Node is Null. Shouldn't happend"));
		return;
	}
	CCDelayTime *wait = CCDelayTime::actionWithDuration( Config::SpriteGlueDestroyDelay );
	CCFadeTo *fadeTo = CCFadeTo::actionWithDuration( Config::SpriteGlueDestroyFadeDur, 0 );
	
	CCFiniteTimeAction *sequence = 	CCSequence::actions( wait, fadeTo, NULL );

	//sequence->setTag( GlueCircleSprite::Glue_Finish_Action );
	node->runAction( sequence );
}
//----------------------------------------------------------------------------------
void AnimerSimpleEffect::Anim_FragileDestroy_Depricated( CCNode *node )
{
	if ( node && node->getParent() )
	{
		node->stopAllActions();
		node->runAction( CCFadeTo::actionWithDuration( 0.3f, 0 ));
		node->runAction( CCScaleTo::actionWithDuration( 0.3f, 0.1f ));
	}
}
//----------------------------------------------------------------------------------
void AnimerSimpleEffect::Anim_FragileDestroy( puBlock *block  )
{
	stringstream filename;

	if ( block->GetWidth() && block->GetHeight() && 
		( block->GetBlockEnum() == eBlockBoxFragile || block->GetBlockEnum() == eBlockBoxFragileStatic ))
	{
		filename << "FragileDestroy" << block->GetWidth() * RATIO;
		filename << "x" << block->GetHeight() * RATIO;
	}
	else if ( block->GetWidth() && block->GetHeight() && 
		( block->GetBlockEnum() == eBlockCircleFragile || block->GetBlockEnum() == eBlockCircleFragileStatic ))
	{
		filename << "CircleFragileDestroy" << block->GetWidth() * RATIO / 2.0f;
	}
	else
	{
		filename << block->GetClassName2();
		filename << "Destroy";
	}

	//-----------------------
	// Load anim
	CCAnimate *animation;
	animation = AnimTools::Get()->GetAnimation( filename.str().c_str(), 1.0f / 20.0f );

	//-----------------------
	// If don't exist load 96x96
	if ( ! animation )
	{
		unAssertMsg( Animation, false, ("Efect %s does not exists.\nChaning to 96x96", filename.str().c_str() ));
		filename.str("");
		filename << "FragileDestroy96x96";
		animation = AnimTools::Get()->GetAnimation( filename.str().c_str(), 1.0f / 20.0f );
	}

	//-----------------------
	// Run anim
	CCSprite *animSprite;
	animSprite = CCSprite::spriteWithSpriteFrame( * ( animation->getAnimation()->getFrames()->begin()) );
	Game::Get()->GetRuningLevel()->addChild( animSprite, GameTypes::eSpritesAboveX3 );


	animSprite->setPosition( 
		ccp( 
		block->GetReversedPosition().x * RATIO - Config::GameSize.width / 2.0f,
		block->GetReversedPosition().y * RATIO - Config::GameSize.height / 2.0f ));

	animSprite->setRotation( block->GetSprite()->getRotation() );
	D_FLOAT( block->GetSprite()->getRotation() )
		animSprite->runAction( animation );

	//-----------------------
	// Remove old sprite
	block->RemoveSprite();
}
//----------------------------------------------------------------------------------
void AnimerSimpleEffect::Anim_TetrisDestroy( puBlock *block  )
{
	puTetrisBase *tetrisBlock;
	tetrisBlock = (puTetrisBase *) block;

	stringstream filename;
	
	filename << tetrisBlock->GetClassName2();
	filename << "Destroy";

	//-----------------------
	// Load anim
	CCAnimate *animation;
	animation = AnimTools::Get()->GetAnimation( filename.str().c_str(), 1.0f / 20.0f );

	//-----------------------
	// If don't exist load 96x96
	if ( ! animation )
	{
		unAssertMsg( Animation, false, ("Efect %s does not exists.\nChaning to 96x96", filename.str().c_str() ));
		return;
	}

	//-----------------------
	// Run anim
	CCSprite *animSprite;
	animSprite = CCSprite::spriteWithSpriteFrame( * ( animation->getAnimation()->getFrames()->begin()) );
	Game::Get()->GetRuningLevel()->addChild( animSprite, GameTypes::eSpritesAboveX3 );

	_terisFixer.FixPosition( animSprite, tetrisBlock );
	animSprite->runAction( animation );

	//-----------------------
	// Remove old sprite
	block->RemoveSprite();
}
//----------------------------------------------------------------------------------
void AnimerSimpleEffect::ShowMsg( CCNode *node, const char *message, int zOrder, float dur, bool mute )
{
	CCPoint pos;
    string msg;
	CCNode *label1;
    CCNode *label2;
    int breakPos;
    
	pos.x = 0.0f;
	pos.y = 0.0f;

    msg = message;
    breakPos = msg.find('\n');

    label1 = NULL;
    label2 = NULL;
   
    const char *fontFile;
    if ( Lang::Get()->IsRussian() )
        fontFile = Lang::Get()->GetLang( Config::LevelNamePlayFont );
    else
        fontFile = Config::LevelMsgFont;

    if ( breakPos > 1 )
    {
        float yDelta;
        
        if ( Lang::Get()->IsChinese() )
        {
            CCLabelTTF *labelTTF1;
            CCLabelTTF *labelTTF2;

            labelTTF1 = CCLabelTTF::labelWithString(msg.substr(0, breakPos).c_str(),
                                                    Config::LevelNamePlayFont, 90 );
            
            labelTTF2 = CCLabelTTF::labelWithString(msg.substr(breakPos+1, msg.length()-breakPos-1 ).c_str(),
                                                    Config::LevelNamePlayFont, 90 );
            
            labelTTF1->setOpacity( 0 );
            labelTTF2->setOpacity( 0 );

            label1 = labelTTF1;
            label2 = labelTTF2;

            yDelta = 52.5f;
        }
        else
        {
            CCLabelBMFont *labelBMP1;
            CCLabelBMFont *labelBMP2;

            labelBMP1 = CCLabelBMFont::labelWithString( msg.substr(0, breakPos).c_str(), fontFile );

            labelBMP2 = CCLabelBMFont::labelWithString( msg.substr(breakPos+1, msg.length()-breakPos-1 ).c_str(),
                                                       fontFile );
            labelBMP1->setOpacity( 0 );
            labelBMP2->setOpacity( 0 );

            label1 = labelBMP1;
            label2 = labelBMP2;

            yDelta = 27.5f;
        }

        label1->setPosition( ccp( pos.x, pos.y + yDelta ));
        label2->setPosition( ccp( pos.x, pos.y - yDelta ));

        node->addChild(label1, zOrder);
        node->addChild(label2, zOrder);
    }
    else
    {
        if ( Lang::Get()->IsChinese() )
        {
            CCLabelTTF *labelTTF1;
            labelTTF1 = CCLabelTTF::labelWithString(message, Config::LevelNamePlayFont, 100 );
            labelTTF1->setOpacity(0);
            label1 = labelTTF1;
        }
        else
        {
            CCLabelBMFont *labelBMP1;
            labelBMP1 = CCLabelBMFont::labelWithString( message, fontFile );
            labelBMP1->setOpacity(0);
            label1 = labelBMP1;
        }
        
        label1->setPosition( ccp( pos.x, pos.y ));
        node->addChild(label1, zOrder);
    }
    //
    
    if ( mute )
	{
		label1->runAction( CCSequence::actions(CCFadeTo::actionWithDuration(  0.6f, 0 ),
                                               CCFadeTo::actionWithDuration( 0.6f, 255 ),
                                               CCDelayTime::actionWithDuration( dur ),
                                               CCFadeTo::actionWithDuration( 0.6f, 0 ),
                                               NULL ));
        if ( label2 )
            label2->runAction( CCSequence::actions(CCFadeTo::actionWithDuration(  0.6f, 0 ),
                                                   CCFadeTo::actionWithDuration( 0.6f, 255 ),
                                                   CCDelayTime::actionWithDuration( dur ),
                                                   CCFadeTo::actionWithDuration( 0.6f, 0 ),
                                                   NULL ));
	}
    else
    {
        CCScene *scene;
        scene = CCDirector::sharedDirector()->getRunningScene();
        
    	CCFiniteTimeAction* animSeq;
        animSeq = CCSequence::actions(
                                      CCCallFuncND::actionWithTarget( scene, callfuncND_selector( SoundEngine::PlayEffectCallback ), (void*)(int) SoundEngine::eSoundLevelNameShow ),
                                      CCFadeTo::actionWithDuration( 0.4f, 255 ),
                                      CCDelayTime::actionWithDuration( dur ),
                                      CCCallFuncND::actionWithTarget( scene, callfuncND_selector( SoundEngine::PlayEffectCallback ), (void*)(int) SoundEngine::eSoundLevelNameHide ),
                                      CCFadeTo::actionWithDuration( 0.4f, 0 ),
                                      NULL );
        label1->runAction( animSeq );
        
        if ( label2 )
        {
            CCFiniteTimeAction* animSeq2;
            animSeq2 = CCSequence::actions(
                                          CCDelayTime::actionWithDuration(0.001f),
                                          CCFadeTo::actionWithDuration( 0.4f, 255 ),
                                          CCDelayTime::actionWithDuration( dur ),
                                          CCDelayTime::actionWithDuration(0.001f),
                                          CCFadeTo::actionWithDuration( 0.4f, 0 ),
                                          NULL );
            label2->runAction(animSeq2);
        }
    }
}
//----------------------------------------------------------------------------------
void AnimerSimpleEffect::Anim_ButtonPulse( CCNode* node )
{
	float duration;
	float deltaScale;

	duration = 0.4f;
	deltaScale = 0.05f;

	CCRepeatForever *repeat = CCRepeatForever::actionWithAction(
		(CCActionInterval *)
		CCSequence::actions( 
		CCScaleTo::actionWithDuration( duration, 1.0f - deltaScale, 1.0f + deltaScale),
		CCScaleTo::actionWithDuration( duration, 1.0f + deltaScale, 1.0f - deltaScale),
		NULL ));
	node->runAction( repeat );
}
//----------------------------------------------------------------------------------
void AnimerSimpleEffect::Anim_PiggyReadyToRock( CCNode *node )
{
	float duration;
	float deltaScale;

	duration = 0.5f;
	deltaScale = 0.1f;

	CCRepeatForever *repeat = CCRepeatForever::actionWithAction(
		(CCActionInterval *)
		CCSequence::actions( 
		CCScaleTo::actionWithDuration( duration, 1.0f - deltaScale, 1.0f + deltaScale),
		CCScaleTo::actionWithDuration( duration, 1.0f + deltaScale, 1.0f - deltaScale),
		NULL ));
	node->runAction( repeat );
}
//----------------------------------------------------------------------------------
void AnimerSimpleEffect::Anim_ZoomIn( CCSprite *sprite )
{
    float dur;
    dur = 0.4f;

    //-----------------------
	//----- scale action
    CCFiniteTimeAction*  actionScale;
	sprite->setScale( 0.01f );

    actionScale = CCSequence::actions(
                                      CCScaleTo::actionWithDuration( dur, 1.2f ),
                                      CCScaleTo::actionWithDuration( dur / 4.0f, 0.9f ),
                                      CCScaleTo::actionWithDuration( dur / 4.0f, 1.0f ),
                                      NULL);

    //-----------------------
	//----- fade action
    CCFiniteTimeAction*  actionFade;
	sprite->setOpacity( 0 );

    actionFade = CCSequence::actions(
                                     CCFadeTo::actionWithDuration( dur + dur / 2.0f, 255 ),
                                     NULL);

    sprite->runAction( actionFade );
	sprite->runAction( actionScale );
}
//----------------------------------------------------------------------------------
SpriteAndAction AnimerSimpleEffect::Anim_FlyFly( Level *level )
{
	CCActionInterval* seqWingsMove;
	CCSprite *animSprite;
	CCAnimate *animation;

	animation = AnimTools::Get()->GetAnimation( "FlyFly", 1.0f / 40.0f );

	seqWingsMove = (CCActionInterval*)(CCSequence::actions( 
		CCRepeat::actionWithAction( 
		animation, 40 ),
		NULL));

	animSprite = CCSprite::spriteWithSpriteFrame( *( animation->getAnimation()->getFrames()->begin() ));

	float dur;
	dur = 0.6f;

	AnimFlyData *data1 = AnimFlyData::Create( animSprite );
	AnimFlyData *data2 = AnimFlyData::Create( animSprite );
	AnimFlyData *data3 = AnimFlyData::Create( animSprite );

	CCActionInterval *seqMove = (CCActionInterval*) (
		CCSequence::actions(
		CCCallFuncND::actionWithTarget( level, callfuncND_selector( AnimerSimpleEffect::Anim_Fly_StillFly ), (void*) animSprite ),
		CCDelayTime::actionWithDuration( 1.0f ),
		CCCallFuncND::actionWithTarget( level, callfuncND_selector( AnimerSimpleEffect::Anim_Fly_RandomFly ), (void*) data1 ), 
		CCDelayTime::actionWithDuration( data1->GetDuration() ),
		CCCallFuncND::actionWithTarget( level, callfuncND_selector( AnimerSimpleEffect::Anim_Fly_RandomFly ), (void*) data2 ), 
		CCDelayTime::actionWithDuration( data2->GetDuration() ),
		CCCallFuncND::actionWithTarget( level, callfuncND_selector( AnimerSimpleEffect::Anim_Fly_RandomFly ), (void*) data3 ), 
		CCFadeTo::actionWithDuration( data3->GetDuration(), 0 ),
		NULL));
      
	return SpriteAndAction( animSprite, seqMove, seqWingsMove, data1, data2, data3 );
}
//----------------------------------------------------------------------------------//----------------------------------------------------------------------------------
CCSprite* AnimerSimpleEffect::Anim_BombExploded( puBlock *block )
{
	CCSprite *animSprite;
	CCAnimate *animation;

	animation = AnimTools::Get()->GetAnimation( "BombExplosion", 1.0f / 24.0f );

	animSprite = CCSprite::spriteWithSpriteFrame( *( animation->getAnimation()->getFrames()->begin() ));
	block->GetLevel()->addChild( animSprite, GameTypes::eSpritesBombExplosion );

	CCSize size;
	size = CCDirector::sharedDirector()->getWinSize();
	animSprite->setPosition( 
		ccp( 
		block->GetPosition().x * RATIO - size.width / 2.0f,
		block->GetPosition().y * RATIO - size.height / 2.0f ));


	animSprite->setRotation( (float)( Tools::Rand( 0, 36000 )) / 100.0f );
	animSprite->runAction( animation );

	return animSprite;
}
//----------------------------------------------------------------------------------
CCPoint AnimerSimpleEffect::GetRandomFlyConfig( CCSprite *animSprite )
{
	b2Vec2 pos;
	b2Vec2 endPos;
	b2Vec2 deltaMove;
	float rotation;
	b2Vec2 rotationCenter;

	rotation = (float) Tools::Rand( -90, 90 );
	animSprite->setRotation( animSprite->getRotation() + rotation );
	//D_FLOAT(rotation );

	pos.Set( animSprite->getPosition().x, animSprite->getPosition().y );
	rotationCenter.SetZero();

	deltaMove.x = (float) Tools::Rand( 150, 200 );
	deltaMove.y = 0.0f;

	deltaMove = Tools::RotatePointAroundPoint( deltaMove, rotationCenter, -animSprite->getRotation() + 90.0f );

	//D_POINT( deltaMove )
	endPos.x = pos.x + deltaMove.x;
	endPos.y = pos.y + deltaMove.y;

	return ccp( endPos.x, endPos.y );
}
//----------------------------------------------------------------------------------
void AnimerSimpleEffect::Anim_Fly_RandomFly( CCObject *sender, void *data )
{
	AnimFlyData *flyData;
	flyData = (AnimFlyData * ) data;

	flyData->GetSprite()->runAction( CCMoveTo::actionWithDuration( flyData->GetDuration(), GetRandomFlyConfig( flyData->GetSprite() )));
}
//----------------------------------------------------------------------------------
void AnimerSimpleEffect::Anim_Fly_StillFly( CCObject *sender, void *data )
{
	CCSprite *animSprite;
	animSprite = (CCSprite * ) data;

	if ( Game::Get()->GetRuningLevel()->GetOrientation() == GameTypes::eLevel_Horizontal )
		animSprite->runAction( 
			(CCActionInterval*) (
				CCSequence::actions(
					CCMoveBy::actionWithDuration( 0.125f, CCPoint( 0.0f, -20.0f )),
					CCMoveBy::actionWithDuration( 0.25f, CCPoint( 0.0f, 10.0f )),
					CCMoveBy::actionWithDuration( 0.25f, CCPoint( 0.0f, 15.0f )),
					CCMoveBy::actionWithDuration( 0.25f, CCPoint( 0.0f, 20.0f )),
					NULL )));
	else
		animSprite->runAction( 
			(CCActionInterval*) (
				CCSequence::actions(
					CCMoveBy::actionWithDuration( 0.125f, CCPoint( 20.0f, 0.0f )),
					CCMoveBy::actionWithDuration( 0.25f, CCPoint( -10.0f, 0.0f )),
					CCMoveBy::actionWithDuration( 0.25f, CCPoint( -15.0f, 0.0f )),
					CCMoveBy::actionWithDuration( 0.25f, CCPoint( -20.0f, 0.0f )),
					NULL )));


}




//----------------------------------------------------------------------------------
// Tetris Anim Fixer
//----------------------------------------------------------------------------------
TetrisAnimFixer::TetrisAnimFixer()
{
	//--------------------------------------------

	FunctionMap tetrisS;

	tetrisS[ eRot0   ] = &TetrisAnimFixer::FixS_0;
	tetrisS[ eRot90  ] = &TetrisAnimFixer::FixS_90;
	tetrisS[ eRot180 ] = &TetrisAnimFixer::FixS_0;
	tetrisS[ eRot270 ] = &TetrisAnimFixer::FixS_90;

	_blocksMap[ eBlockTetrisS ] = tetrisS;

	//--------------------------------------------

	FunctionMap tetrisSM;

	tetrisSM[ eRot0   ] = &TetrisAnimFixer::FixSM_0;
	tetrisSM[ eRot90  ] = &TetrisAnimFixer::FixSM_90;
	tetrisSM[ eRot180 ] = &TetrisAnimFixer::FixSM_0;
	tetrisSM[ eRot270 ] = &TetrisAnimFixer::FixSM_90;

	_blocksMap[ eBlockTetrisSM ] = tetrisSM;

	//--------------------------------------------

	FunctionMap tetrisSquare;

	tetrisSquare[ eRot0   ] = &TetrisAnimFixer::FixSquare;
	tetrisSquare[ eRot90  ] = &TetrisAnimFixer::FixSquare;
	tetrisSquare[ eRot180 ] = &TetrisAnimFixer::FixSquare;
	tetrisSquare[ eRot270 ] = &TetrisAnimFixer::FixSquare;

	_blocksMap[ eBlockTetrisSquare ] = tetrisSquare;

	//--------------------------------------------
	
	FunctionMap tetrisL;

	tetrisL[ eRot0   ] = &TetrisAnimFixer::FixL_0;
	tetrisL[ eRot90  ] = &TetrisAnimFixer::FixL_90;
	tetrisL[ eRot180 ] = &TetrisAnimFixer::FixL_180;
	tetrisL[ eRot270 ] = &TetrisAnimFixer::FixL_270;

	_blocksMap[ eBlockTetrisL ] = tetrisL;

	//--------------------------------------------

	FunctionMap tetrisLM;

	tetrisLM[ eRot0   ] = &TetrisAnimFixer::FixLM_0;
	tetrisLM[ eRot90  ] = &TetrisAnimFixer::FixLM_90;
	tetrisLM[ eRot180 ] = &TetrisAnimFixer::FixLM_180;
	tetrisLM[ eRot270 ] = &TetrisAnimFixer::FixLM_270;

	_blocksMap[ eBlockTetrisLM ] = tetrisLM;

	//--------------------------------------------

	FunctionMap tetrisT;

	tetrisT[ eRot0   ] = &TetrisAnimFixer::FixT_0;
	tetrisT[ eRot90  ] = &TetrisAnimFixer::FixT_90;
	tetrisT[ eRot180 ] = &TetrisAnimFixer::FixT_180;
	tetrisT[ eRot270 ] = &TetrisAnimFixer::FixT_270;

	_blocksMap[ eBlockTetrisT ] = tetrisT;

	//--------------------------------------------

	FunctionMap tetrisI;

	tetrisI[ eRot0   ] = &TetrisAnimFixer::FixI_0;
	tetrisI[ eRot90  ] = &TetrisAnimFixer::FixI_90;
	tetrisI[ eRot180 ] = &TetrisAnimFixer::FixI_0;
	tetrisI[ eRot270 ] = &TetrisAnimFixer::FixI_90;

	_blocksMap[ eBlockTetrisI ] = tetrisI;

	//--------------------------------------------

	

}
//----------------------------------------------------------------------------------
void TetrisAnimFixer::FixPosition( CCSprite *sprite, puTetrisBase *block )
{
	FixFuncPtr fn;
	FunctionMap fnMap;

	fnMap = _blocksMap[ block->GetBlockEnum() ];

	if ( ! fnMap.size() )
		return;

	fn = fnMap[ block->GetTetrisRotation() ];
	if ( !fn )
		return;

//	(*fn)( sprite, block );
	(this->*fn)( sprite, block );
}
//----------------------------------------------------------------------------------
void TetrisAnimFixer::FixS_0( CCSprite *sprite, puTetrisBase *block )
{
	CCPoint pos;
	sprite->setRotation( 0.0f );

	pos = GetBasedPoint( block );
	pos.x += ( RATIO * puTetrisBase::_sBlockHalfSize * 2.0f );
	pos.y += ( RATIO * puTetrisBase::_sBlockHalfSize );

	sprite->setPosition( pos );
}
//----------------------------------------------------------------------------------
void TetrisAnimFixer::FixS_90( CCSprite *sprite, puTetrisBase *block )
{
	CCPoint pos;
	sprite->setRotation( 90.0f );

	pos = GetBasedPoint( block );
	pos.x += ( RATIO * puTetrisBase::_sBlockHalfSize * 1.0f );

	sprite->setPosition( pos );
}
//----------------------------------------------------------------------------------
void TetrisAnimFixer::FixSM_0( CCSprite *sprite, puTetrisBase *block )
{
	CCPoint pos;
	sprite->setRotation( 0.0f );

	pos = GetBasedPoint( block );
	pos.y += ( RATIO * puTetrisBase::_sBlockHalfSize );

	sprite->setPosition( pos );
}
//----------------------------------------------------------------------------------
void TetrisAnimFixer::FixSM_90( CCSprite *sprite, puTetrisBase *block )
{
	CCPoint pos;
	sprite->setRotation( 90.0f );

	pos = GetBasedPoint( block );
	pos.x += ( RATIO * puTetrisBase::_sBlockHalfSize );

	sprite->setPosition( pos );
}
//----------------------------------------------------------------------------------
void TetrisAnimFixer::FixSquare( CCSprite *sprite, puTetrisBase *block )
{
	CCPoint pos;

	pos = GetBasedPoint( block );
	pos.x += ( RATIO * puTetrisBase::_sBlockHalfSize );
	pos.y += ( RATIO * puTetrisBase::_sBlockHalfSize );

	sprite->setPosition( pos );
}
//----------------------------------------------------------------------------------
cocos2d::CCPoint TetrisAnimFixer::GetBasedPoint( puBlock *block )
{
	return ccp( 
		block->GetReversedPosition().x * RATIO - Config::GameSize.width / 2.0f,
		block->GetReversedPosition().y * RATIO - Config::GameSize.height / 2.0f );
}
//----------------------------------------------------------------------------------
void TetrisAnimFixer::FixL_0( CCSprite *sprite, puTetrisBase *block )
{
	CCPoint pos;

	pos = GetBasedPoint( block );
	//pos.x += ( RATIO * puTetrisBase::_sBlockHalfSize );
	pos.y += ( RATIO * puTetrisBase::_sBlockHalfSize );

	sprite->setRotation( -90.0f );
	sprite->setPosition( pos );
}
//----------------------------------------------------------------------------------
void TetrisAnimFixer::FixL_90( CCSprite *sprite, puTetrisBase *block )
{
	CCPoint pos;

	pos = GetBasedPoint( block );
	pos.x += ( RATIO * puTetrisBase::_sBlockHalfSize );
	//pos.y += ( RATIO * puTetrisBase::_sBlockHalfSize );

	sprite->setRotation( 0.0f );
	sprite->setPosition( pos );
}
//----------------------------------------------------------------------------------
void TetrisAnimFixer::FixL_180( CCSprite *sprite, puTetrisBase *block )
{
	CCPoint pos;

	pos = GetBasedPoint( block );
	//pos.x += ( RATIO * puTetrisBase::_sBlockHalfSize );
	pos.y -= ( RATIO * puTetrisBase::_sBlockHalfSize );

	sprite->setRotation( 90.0f );
	sprite->setPosition( pos );
}
//----------------------------------------------------------------------------------
void TetrisAnimFixer::FixL_270( CCSprite *sprite, puTetrisBase *block )
{
	CCPoint pos;

	pos = GetBasedPoint( block );
	pos.x -= ( RATIO * puTetrisBase::_sBlockHalfSize );
	//pos.y += ( RATIO * puTetrisBase::_sBlockHalfSize );

	sprite->setRotation( 180.0f );
	sprite->setPosition( pos );
}
//----------------------------------------------------------------------------------
void TetrisAnimFixer::FixLM_0( CCSprite *sprite, puTetrisBase *block )
{
	CCPoint pos;

	pos = GetBasedPoint( block );
//	pos.x += ( RATIO * puTetrisBase::_sBlockHalfSize );
	pos.y += ( RATIO * puTetrisBase::_sBlockHalfSize );

	sprite->setRotation( 90.0f );
	sprite->setPosition( pos );
}
//----------------------------------------------------------------------------------
void TetrisAnimFixer::FixLM_90( CCSprite *sprite, puTetrisBase *block )
{
	CCPoint pos;

	pos = GetBasedPoint( block );
	pos.x += ( RATIO * puTetrisBase::_sBlockHalfSize );
	//pos.y += ( RATIO * puTetrisBase::_sBlockHalfSize );

	sprite->setRotation( 180.0f );
	sprite->setPosition( pos );
}
//----------------------------------------------------------------------------------
void TetrisAnimFixer::FixLM_180( CCSprite *sprite, puTetrisBase *block )
{
	CCPoint pos;

	pos = GetBasedPoint( block );
	//pos.x += ( RATIO * puTetrisBase::_sBlockHalfSize );
	pos.y -= ( RATIO * puTetrisBase::_sBlockHalfSize );

	sprite->setRotation( 270.0f );
	sprite->setPosition( pos );
}
//----------------------------------------------------------------------------------
void TetrisAnimFixer::FixLM_270( CCSprite *sprite, puTetrisBase *block )
{
	CCPoint pos;

	pos = GetBasedPoint( block );
	pos.x -= ( RATIO * puTetrisBase::_sBlockHalfSize );

	sprite->setRotation( 0.0f );
	sprite->setPosition( pos );
}
//----------------------------------------------------------------------------------
void TetrisAnimFixer::FixT_0( CCSprite *sprite, puTetrisBase *block )
{
	CCPoint pos;

	pos = GetBasedPoint( block );
	pos.y += ( RATIO * puTetrisBase::_sBlockHalfSize );

	sprite->setRotation( 0.0f );
	sprite->setPosition( pos );
}
//----------------------------------------------------------------------------------
void TetrisAnimFixer::FixT_90( CCSprite *sprite, puTetrisBase *block )
{
	CCPoint pos;

	pos = GetBasedPoint( block );
	pos.x += ( RATIO * puTetrisBase::_sBlockHalfSize );

	sprite->setRotation( 90.0f );
	sprite->setPosition( pos );
}
//----------------------------------------------------------------------------------
void TetrisAnimFixer::FixT_180( CCSprite *sprite, puTetrisBase *block )
{
	CCPoint pos;

	pos = GetBasedPoint( block );
	//pos.x += ( RATIO * puTetrisBase::_sBlockHalfSize );
	pos.y -= ( RATIO * puTetrisBase::_sBlockHalfSize );

	sprite->setRotation( 180.0f );
	sprite->setPosition( pos );
}
//----------------------------------------------------------------------------------
void TetrisAnimFixer::FixT_270( CCSprite *sprite, puTetrisBase *block )
{
	CCPoint pos;

	pos = GetBasedPoint( block );
	pos.x -= ( RATIO * puTetrisBase::_sBlockHalfSize );
	//pos.y += ( RATIO * puTetrisBase::_sBlockHalfSize );

	sprite->setRotation( 270.0f );
	sprite->setPosition( pos );
}
//----------------------------------------------------------------------------------
void TetrisAnimFixer::FixI_0( CCSprite *sprite, puTetrisBase *block )
{
	CCPoint pos;

	pos = GetBasedPoint( block );
	pos.x += ( RATIO * puTetrisBase::_sBlockHalfSize );
	//pos.y -= ( RATIO * puTetrisBase::_sBlockHalfSize );

	sprite->setRotation( 90.0f );
	sprite->setPosition( pos );
}
//----------------------------------------------------------------------------------
void TetrisAnimFixer::FixI_90( CCSprite *sprite, puTetrisBase *block )
{
	CCPoint pos;

	pos = GetBasedPoint( block );
	//pos.x += ( RATIO * puTetrisBase::_sBlockHalfSize );
	pos.y += ( RATIO * puTetrisBase::_sBlockHalfSize );

	sprite->setRotation( 0.0f );
	sprite->setPosition( pos );
}




//----------------------------------------------------------------------------------
// Anim Fly Data
//----------------------------------------------------------------------------------
AnimFlyData* AnimFlyData::Create( CCSprite *sprite )
{
	AnimFlyData *data;
	data = new AnimFlyData( sprite );
	if ( data )
	{
		data->autorelease();
		return data;
	}
	CC_SAFE_DELETE(data);
	return NULL;
}
//----------------------------------------------------------------------------------
AnimFlyData::AnimFlyData( CCSprite *sprite )
{
	_animSprite = sprite;
	_duration = ( float) ( Tools::Rand( 400, 800 ) ) / 1000.0f;
}


//----------------------------------------------------------------------------------
// SpriteAndAction
//----------------------------------------------------------------------------------
SpriteAndAction::SpriteAndAction()
{
	_sprite     = NULL;
	_action     = NULL;
	_action2    = NULL;
    _flyData1   = NULL;
    _flyData2   = NULL;
    _flyData3   = NULL;
}
//----------------------------------------------------------------------------------
SpriteAndAction::SpriteAndAction( CCSprite *sprite, CCAction *action, CCAction *action2, AnimFlyData *flyData1, AnimFlyData *flyData2, AnimFlyData *flyData3 )
{
	_sprite     = sprite;
	_action     = action;
	_action2    = action2;
    _flyData1   = flyData1;
    _flyData2   = flyData2;
    _flyData3   = flyData3;

}
//----------------------------------------------------------------------------------
SpriteAndAction::~SpriteAndAction()
{
	//Release();
}
//----------------------------------------------------------------------------------
void SpriteAndAction::Retain()
{
	CC_SAFE_RETAIN( _sprite );
	CC_SAFE_RETAIN( _action );
	CC_SAFE_RETAIN( _action2 );
    CC_SAFE_RETAIN( _flyData1 );
    CC_SAFE_RETAIN( _flyData2 );
    CC_SAFE_RETAIN( _flyData3 );
}
//----------------------------------------------------------------------------------
void SpriteAndAction::Release()
{
	CC_SAFE_RELEASE_NULL( _sprite );
	CC_SAFE_RELEASE_NULL( _action );
	CC_SAFE_RELEASE_NULL( _action2 );
	CC_SAFE_RELEASE_NULL( _flyData1 );
	CC_SAFE_RELEASE_NULL( _flyData2 );
	CC_SAFE_RELEASE_NULL( _flyData3 );
}
//----------------------------------------------------------------------------------
void SpriteAndAction::StopActions()
{
	if ( _sprite ) _sprite->stopAllActions();
    if ( _flyData1 && _flyData1->GetSprite() ) _flyData1->GetSprite()->stopAllActions();
    if ( _flyData2 && _flyData2->GetSprite() ) _flyData2->GetSprite()->stopAllActions();
    if ( _flyData3 && _flyData3->GetSprite() ) _flyData3->GetSprite()->stopAllActions();
}
//----------------------------------------------------------------------------------
