#ifndef __ANIMSPRITESCREATOR_H__
#define __ANIMSPRITESCREATOR_H__

#include "cocos2d.h"
#include "Blocks/puBlock.h"
#include "AnimTools.h"

USING_NS_CC;

//---------------------------------------------------------------------------
class AnimSpritesCreator
{
public:
	AnimSpritesCreator();
	CCSprite* CreatePlainSprite( puBlock *block, float fps = 24.0f );
	CCSprite* CreateSitSprite( puBlock *block, float fps = 24.0f );
	CCSprite* CreateRollSprite( puBlock *block, float fps = 24.0f );

	typedef CCSprite* ( AnimSpritesCreator::*FuncPtrCreateSprite )( void *data );
	//typedef map<string, FuncPtrCreateSprite> CreateSpriteMap;
	typedef map<BlockEnum, string> CreateSpriteMap;

private:
	CreateSpriteMap _spriteSitCreateMap;
	CreateSpriteMap _spriteRollCreateMap;
	CreateSpriteMap _spritePlainCreateMap;

	CCSprite* doCreateSprite( puBlock *block, const char *animname, float fps );
};
//---------------------------------------------------------------------------
#endif
