#ifndef __ANIMER_H__
#define __ANIMER_H__

#include "cocos2d.h"
#include "Blocks/puBlock.h"

USING_NS_CC;

//----------------------------------------------------------
// Fix rotation anim data
//----------------------------------------------------------
class FixRotationAnimData 
{
public:
	FixRotationAnimData();
	FixRotationAnimData( const char* fixRotStart, const char*  fixRotStop, const char* fixRot360, int frameCount );
	FixRotationAnimData( string fixRotStart, string fixRotStop, string fixRot360, int frameCount );

	string	_animStart;
	string	_animEnd;
	string	_anim360;
	int		_frameCount;
};

//----------------------------------------------------------------------------------
// Anim Character Data
//----------------------------------------------------------------------------------
class AnimCharacters;
class AnimCharacterData
{
public:	
	AnimCharacterData( AnimCharacters *animChar, puBlock *block );
	AnimCharacterData();

	AnimCharacters	*_animCharaters;
	puBlock			*_block;
};


//----------------------------------------------------------------------------------
// Anim Character Data
//----------------------------------------------------------------------------------
class AnimCharacterDataBuffer
{
public:
	AnimCharacterDataBuffer();
	AnimCharacterData* GetNext();

private:
	static const int BUFFER_SIZE = 20;

	AnimCharacterData	_buffer[ BUFFER_SIZE ];
	int					_bufferIndex;
};

//----------------------------------------------------------
// AnimerCharacters
//----------------------------------------------------------
// characters animations: like piggy roll, spitter spit etc
//----------------------------------------------------------
class AnimCharacters
{	
public:
	typedef void ( AnimCharacters::*AnimFuncPtr )( GameTypes::CharactersAnim anim, puBlock *block );
	typedef map<GameTypes::BlockEnum, FixRotationAnimData> BlockMap;

	struct AnimCreateData
	{
		AnimCreateData( const char *name, AnimFuncPtr fn, SoundEngine::Sound );
		AnimCreateData( const char *name, AnimFuncPtr fn );
		AnimCreateData( const char *name, SoundEngine::Sound );
		AnimCreateData( AnimFuncPtr fn );
		AnimCreateData( const char *name );
		AnimCreateData( SoundEngine::Sound );

		AnimCreateData();
		
		string				_animName;
		SoundEngine::Sound	_animSound;
		AnimFuncPtr			_createFunc;
	};
	//typedef pair<string, AnimFuncPtr> AnimCreateData;
	typedef map<GameTypes::CharactersAnim, AnimCreateData>	AnimFuncMap;

	AnimCharacters();
	~AnimCharacters();
	void PlayAnim( GameTypes::CharactersAnim anim, puBlock *block );
	void LoadToCache( GameTypes::CharactersAnim anim, float radius );
	BlockMap GetRotationFixMap();

private:
	void Anim_SpitterSpit( GameTypes::CharactersAnim anim, puBlock *block );
	void Anim_SpitterExplosiveSpit( GameTypes::CharactersAnim anim, puBlock *block );
	
	void Anim_PiggyBounce( GameTypes::CharactersAnim anim, puBlock *block );
	void Anim_ToRollingGeneric( GameTypes::CharactersAnim anim, puBlock *block );
	void Anim_ToSittingGeneric( GameTypes::CharactersAnim anim, puBlock *block );
	void Anim_PiggyFromRolling( GameTypes::CharactersAnim anim, puBlock *block );
	void Anim_PiggyLevelDone( GameTypes::CharactersAnim anim, puBlock *block );
	void Anim_PiggyLevelFailed( GameTypes::CharactersAnim anim, puBlock *block );
	void Anim_PiggyCoinCollected( GameTypes::CharactersAnim anim, puBlock *block );
	void Anim_CopCaughtThief( GameTypes::CharactersAnim anim, puBlock *block );
	void Anim_ThiefItemStolen( GameTypes::CharactersAnim anim, puBlock *block );
	void Anim_PiggyStolen( GameTypes::CharactersAnim anim, puBlock *block );
	void Anim_IdelGeneric( GameTypes::CharactersAnim anim, puBlock *block );

	void Anim_RotationFix_Start( GameTypes::CharactersAnim anim, puBlock *block );
	void Anim_RotationFix_Loop( CCNode *sender, void *data );
	void Anim_RotationFix_End( CCNode *sender, void *data );
	void Anim_RotationFixGeneric_SetSpriteDelta( CCNode *sender, void *data );
	void Anim_RotationFixGeneric_Finish( CCNode *sender, void *data );
	void RotationFixLoopSound( int frameCount, float fps );
	void RotationFixLoopSoundCallback();
    void EndLevelPiggyJumpFix( puBlock *block );
	void SetAnimMap();

private:
	AnimFuncMap				_animMap;
	BlockMap				_fixRotationAnimMap;
	AnimCharacterDataBuffer	_fixRotationDataBuffer;
};
//------------------------------------------------------------------------------------

#endif
