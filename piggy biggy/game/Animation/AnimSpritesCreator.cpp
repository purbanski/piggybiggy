#include "AnimSpritesCreator.h"
#include "AnimSprites.h"
#include "unDebug.h"

//------------------------------------------------------------------------------
AnimSpritesCreator::AnimSpritesCreator()
{
	_spriteSitCreateMap[ eBlockPiggyBank		]	= string( "PiggyBankSitBounce" );
	_spriteSitCreateMap[ eBlockPiggyBankStatic	]	= string( "PiggyBankSitBounce" );
	_spriteSitCreateMap[ eBlockSpitter			]	= string( "SpiterBounce" );
	_spriteSitCreateMap[ eBlockSpitterExplosive	]	= string( "SpiterExplosiveBounce" );
	_spriteSitCreateMap[ eBlockCop				]	= string( "CopSitBounce" );
	_spriteSitCreateMap[ eBlockThief			]	= string( "ThiefSitBounce" );
	_spriteSitCreateMap[ eBlockThiefStatic		]	= string( "ThiefSitBounce" );
	_spriteSitCreateMap[ eBlockMotherPiggy		]	= string( "MotherPiggySitBounce" );
	_spriteSitCreateMap[ eBlockMotherPiggyStatic]	= string( "MotherPiggySitBounce" );
	_spriteSitCreateMap[ eBlockCashCow			]	= string( "CashCowSitBounce" );
	_spriteSitCreateMap[ eBlockCashCowStatic	]	= string( "CashCowSitBounce" );
	_spriteSitCreateMap[ eBlockPrison			]	= string( "PrisonBounce" );

	_spriteRollCreateMap[ eBlockPiggyBank		]	= string( "PiggyBankRollBounce" );
	_spriteRollCreateMap[ eBlockPiggyBankRolled	]	= string( "PiggyBankRollBounce" );
	_spriteRollCreateMap[ eBlockCop				]	= string( "CopRollBounce" );
	_spriteRollCreateMap[ eBlockThief			]	= string( "ThiefRollBounce" );
	_spriteRollCreateMap[ eBlockThiefRolled		]	= string( "ThiefRollBounce" );
	
	_spritePlainCreateMap[ eBlockBoxBounce		]	= string( "BouncerBoxBounce" );
}
//------------------------------------------------------------------------------
CCSprite* AnimSpritesCreator::CreateSitSprite( puBlock *block, float fps )
{

	if ( ! _spriteSitCreateMap.count( block->GetBlockEnum() ))
	{
		unAssertMsg(Animation,false, ("Undefined sit anim for %s", block->GetClassName2() ));
		return NULL;
	}

	CCSprite *sprite;

	sprite = doCreateSprite( block, _spriteSitCreateMap[ block->GetBlockEnum() ].c_str(), fps );
	return sprite;
}
//------------------------------------------------------------------------------
CCSprite* AnimSpritesCreator::CreateRollSprite( puBlock *block, float fps )
{

	if ( ! _spriteRollCreateMap.count( block->GetBlockEnum() ))
	{
		unAssertMsg(Animation,false, ("Undefined roll anim for %s", block->GetClassName2() ));
		return NULL;
	}

	CCSprite *sprite;

	sprite = doCreateSprite( block, _spriteRollCreateMap[ block->GetBlockEnum() ].c_str(), fps );
	return sprite;
}
//------------------------------------------------------------------------------
CCSprite* AnimSpritesCreator::CreatePlainSprite( puBlock *block, float fps )
{

	if ( ! _spritePlainCreateMap.count( block->GetBlockEnum() ))
	{
		unAssertMsg(Animation,false, ("Undefined plain anim for %s", block->GetClassName2() ));
		return NULL;
	}

	CCSprite *sprite;

	sprite = doCreateSprite( block, _spritePlainCreateMap[ block->GetBlockEnum() ].c_str(), fps );
	return sprite;
}
//------------------------------------------------------------------------------
CCSprite* AnimSpritesCreator::doCreateSprite( puBlock *block, const char *animname, float fps )
{

	CCSprite *sprite;

	sprite = AnimSprite::Create( block, animname );

	if ( block && block->GetBody() )
	{
		AnimTools::Get()->SpriteSetFlip( block, sprite );
	}
	return sprite;
}

//------------------------------------------------------------------------------

