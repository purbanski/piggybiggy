#include "AnimerCharacters.h"
#include "AnimTools.h"
#include "AnimManager.h"
#include "GameConfig.h"
#include "Tools.h"
#include "Game.h"
#include "Debug/MyDebug.h"

using namespace GameTypes;


//----------------------------------------------------------------------------------
// Fix Rotation Anim Data
//----------------------------------------------------------------------------------
FixRotationAnimData::FixRotationAnimData( string fixRotStart, string fixRotStop, string fixRot360, int frameCount )
{
	_animStart = fixRotStart;
	_animEnd = fixRotStop;
	_anim360 = fixRot360;
	_frameCount = frameCount;
}
//----------------------------------------------------------------------------------
FixRotationAnimData::FixRotationAnimData( const char* fixRotStart, const char* fixRotStop, const char* fixRot360, int frameCount  )
{
	_animStart.append( fixRotStart );
	_animEnd.append( fixRotStop );
	_anim360.append( fixRot360 );
	_frameCount = frameCount;
}
//----------------------------------------------------------------------------------
FixRotationAnimData::FixRotationAnimData()
{
	_animStart.clear();
	_animEnd.clear();
	_anim360.clear();
	_frameCount = 0;
}


//----------------------------------------------------------------------------------
// Anim Create Data
//----------------------------------------------------------------------------------
AnimCharacters::AnimCreateData::AnimCreateData()
{

	_createFunc = NULL;
	_animName.clear();
	_animSound = SoundEngine::eSoundNone;
}
//----------------------------------------------------------------------------------
AnimCharacters::AnimCreateData::AnimCreateData( const char *name, AnimFuncPtr fn, SoundEngine::Sound sound )
{

	_animName = name;
	_createFunc = fn;
	_animSound = sound;
}
//----------------------------------------------------------------------------------
AnimCharacters::AnimCreateData::AnimCreateData( const char *name, AnimFuncPtr fn )
{

	_animName = name;
	_createFunc = fn;
	_animSound = SoundEngine::eSoundNone;
}
//----------------------------------------------------------------------------------
AnimCharacters::AnimCreateData::AnimCreateData( const char *name, SoundEngine::Sound sound )
{

	_animName = name;
	_createFunc = NULL;
	_animSound = sound;
}
//----------------------------------------------------------------------------------
AnimCharacters::AnimCreateData::AnimCreateData( AnimFuncPtr fn )
{

	_animName = "";
	_createFunc = fn;
	_animSound = SoundEngine::eSoundNone;
}
//----------------------------------------------------------------------------------
AnimCharacters::AnimCreateData::AnimCreateData( const char *name )
{

	_animName = name;
	_createFunc = NULL;
	_animSound = SoundEngine::eSoundNone;
}
//----------------------------------------------------------------------------------
AnimCharacters::AnimCreateData::AnimCreateData( SoundEngine::Sound sound )
{

	_animName = "";
	_createFunc = NULL;
	_animSound  = sound;
}


//----------------------------------------------------------------------------------
// AnimerCharacters
//----------------------------------------------------------------------------------
AnimCharacters::AnimCharacters()
{

	SetAnimMap();

}
//----------------------------------------------------------------------------------
void AnimCharacters::SetAnimMap()
{

	//--------------------------
	// Fix rotation map
	_fixRotationAnimMap[ eBlockPiggyBank ]	= FixRotationAnimData(
                                                                  "PiggyBankRotationFix_Start",
                                                                  "PiggyBankRotationFix_End",
                                                                  "PiggyBankRotationFix_360",
                                                                  Config::AnimPiggy360FrameCount );
    
	_fixRotationAnimMap[ eBlockThief ]		= FixRotationAnimData(
                                                                  "ThiefRotationFix_Start",
                                                                  "ThiefRotationFix_End",
                                                                  "ThiefRotationFix_360",
                                                                  Config::AnimThief360FrameCount );
    
	_fixRotationAnimMap[ eBlockCop ]		= FixRotationAnimData(
                                                                  "CopRotationFix_Start",
                                                                  "CopRotationFix_End",
                                                                  "CopRotationFix_360",
                                                                  Config::AnimCop360FrameCount );

		
	//--------------------------
	// Anim Map
	_animMap[ GameTypes::eAnim_SpitterSpit ]				= AnimCreateData( "SpiterSpit",	&AnimCharacters::Anim_SpitterSpit, SoundEngine::eSoundSpit );
	_animMap[ GameTypes::eAnim_SpitterSpitEmpty ]			= AnimCreateData( "SpiterSpit",	&AnimCharacters::Anim_SpitterSpit, SoundEngine::eSoundSpitEmpty );

	_animMap[ GameTypes::eAnim_SpitterExplosiveSpit ]		= AnimCreateData( "SpiterExplosiveSpit", &AnimCharacters::Anim_SpitterExplosiveSpit, SoundEngine::eSoundSpit );
	_animMap[ GameTypes::eAnim_SpitterExplosiveSpitEmpty ]	= AnimCreateData( "SpiterExplosiveSpit", &AnimCharacters::Anim_SpitterExplosiveSpit, SoundEngine::eSoundSpitEmpty );

	// preloading missing for hands move for piggy rotation and end
	_animMap[ GameTypes::eAnim_PiggyBankBounce ]		= AnimCreateData( &AnimCharacters::Anim_PiggyBounce );
	_animMap[ GameTypes::eAnim_PiggyBankBounce_Sit ]	= AnimCreateData( "PiggyBankSitBounce" );
	_animMap[ GameTypes::eAnim_PiggyBankBounce_Roll ]	= AnimCreateData( "PiggyBankRollBounce" );

	_animMap[ GameTypes::eAnim_PiggyBankLevelDone ]			= AnimCreateData( &AnimCharacters::Anim_PiggyLevelDone );
	_animMap[ GameTypes::eAnim_PiggyBankLevelDone_Sit ]		= AnimCreateData( "PiggyBankSitLevelDone",	SoundEngine::eSoundAnim_PiggyLevelDone_Sit );
	_animMap[ GameTypes::eAnim_PiggyBankLevelDone_Roll ]	= AnimCreateData( "PiggyBankRollLevelDone", SoundEngine::eSoundAnim_PiggyLevelDone_Roll );

	_animMap[ GameTypes::eAnim_PiggyBankLevelFailed ]		= AnimCreateData( &AnimCharacters::Anim_PiggyLevelFailed );
	_animMap[ GameTypes::eAnim_PiggyBankLevelFailed_Sit ]	= AnimCreateData( "PiggyBankSitLevelFailed",	SoundEngine::eSoundAnim_PiggyLevelFailed_Sit );
	_animMap[ GameTypes::eAnim_PiggyBankLevelFailed_Roll ]	= AnimCreateData( "PiggyBankRollLevelFailed",	SoundEngine::eSoundAnim_PiggyLevelFailed_Roll );

	_animMap[ GameTypes::eAnim_PiggyBankCoinCollected ]			= AnimCreateData( &AnimCharacters::Anim_PiggyCoinCollected );
	_animMap[ GameTypes::eAnim_PiggyBankCoinCollected_Sit ]		= AnimCreateData( "PiggyBankSitCoinCollected",	SoundEngine::eSoundAnim_PiggyCoinCollected_Sit );
	_animMap[ GameTypes::eAnim_PiggyBankCoinCollected_Roll ]	= AnimCreateData( "PiggyBankRollCoinCollected", SoundEngine::eSoundAnim_PiggyCoinCollected_Roll );

	_animMap[ GameTypes::eAnim_PiggyBankStolen ]				= AnimCreateData( &AnimCharacters::Anim_PiggyStolen );
	_animMap[ GameTypes::eAnim_PiggyBankStolen_Sit ]			= AnimCreateData( "PiggyBankSitStolen" );
	_animMap[ GameTypes::eAnim_PiggyBankStolen_Roll ]			= AnimCreateData( "PiggyBankRollStolen" );


	_animMap[ GameTypes::eAnim_ThiefSmiles ]				= AnimCreateData( &AnimCharacters::Anim_ThiefItemStolen );
	_animMap[ GameTypes::eAnim_ThiefSmiles_Sit ]			= AnimCreateData( "ThiefSitItemStolen",		SoundEngine::eSoundAnim_ThiefSitItemStolen );
	_animMap[ GameTypes::eAnim_ThiefSmiles_Roll ]			= AnimCreateData( "ThiefRollItemStolen",	SoundEngine::eSoundAnim_ThiefRollItemStolen);

	_animMap[ GameTypes::eAnim_CopSitBounce ] = AnimCreateData( "CopSitBounce",	NULL );

	//---------------
	// Rotation Fix
	_animMap[ GameTypes::eAnim_RotationFix ]= AnimCreateData( &AnimCharacters::Anim_RotationFix_Start ); // callback def

	_animMap[ eAnim_PiggyBankToRolling	]	= AnimCreateData( "PiggyBankToRolling", &AnimCharacters::Anim_ToRollingGeneric, SoundEngine::eSoundAnim_PiggyToRolling );
	_animMap[ eAnim_PiggyBankToSitting	]	= AnimCreateData( "PiggyBankToSitting", &AnimCharacters::Anim_ToSittingGeneric, SoundEngine::eSoundAnim_PiggyFromRolling );
	
	_animMap[ eAnim_PiggyBankLookAround ]	= AnimCreateData( "PiggyBankLookAround",	&AnimCharacters::Anim_IdelGeneric, SoundEngine::eSoundAnim_PiggyLookAround );
	_animMap[ eAnim_PiggyBankUaa		]	= AnimCreateData( "PiggyBankUaa",			&AnimCharacters::Anim_IdelGeneric, SoundEngine::eSoundAnim_PiggyUaa );

	_animMap[ eAnim_PiggyBankEeeOoo		]	= AnimCreateData( "PiggyBankEeeOoo",		&AnimCharacters::Anim_IdelGeneric, SoundEngine::eSoundAnim_PiggyEeeOoo );
	_animMap[ eAnim_PiggyBankFiuFiu		]	= AnimCreateData( "PiggyBankFiuFiu",		&AnimCharacters::Anim_IdelGeneric, SoundEngine::eSoundAnim_PiggyFiuFiu);

	_animMap[ eAnim_PiggyBankSalto		]	= AnimCreateData( "PiggyBankSalto",			&AnimCharacters::Anim_IdelGeneric, SoundEngine::eSoundAnim_PiggySalto );
	_animMap[ eAnim_PiggyBankHappyJump	]	= AnimCreateData( "PiggyBankHappyJump",		&AnimCharacters::Anim_IdelGeneric, SoundEngine::eSoundAnim_PiggyHappyJump );
	_animMap[ eAnim_PiggyBankNoseBubble ]	= AnimCreateData( "PiggyBankNoseBubble",	&AnimCharacters::Anim_IdelGeneric, SoundEngine::eSoundAnim_PiggyNoseBubble );
	_animMap[ eAnim_PiggyBankEyesBlink	]	= AnimCreateData( "PiggyBankEyesBlink",		&AnimCharacters::Anim_IdelGeneric );

	_animMap[ eAnim_CopJump				]	= AnimCreateData( "CopJump",			&AnimCharacters::Anim_IdelGeneric );
	_animMap[ eAnim_ThiefLookAround		]	= AnimCreateData( "ThiefLookAround",	&AnimCharacters::Anim_IdelGeneric );

	_animMap[ eAnim_CopToRolling		]	= AnimCreateData( "CopToRolling",		&AnimCharacters::Anim_ToRollingGeneric );
	_animMap[ eAnim_CopToSitting		]	= AnimCreateData( "CopToSitting",		&AnimCharacters::Anim_ToSittingGeneric );
	
	_animMap[ eAnim_CopSmiles			]	= AnimCreateData( &AnimCharacters::Anim_CopCaughtThief );
	_animMap[ eAnim_CopSmiles_Sit		]	= AnimCreateData( "CopSitSmiles",		SoundEngine::eSoundThiefCaught );
	_animMap[ eAnim_CopSmiles_Roll		]	= AnimCreateData( "CopRollSmiles",		SoundEngine::eSoundThiefCaught );

	_animMap[ eAnim_ThiefToRolling		]	= AnimCreateData( "ThiefToRolling",		&AnimCharacters::Anim_ToRollingGeneric );
	_animMap[ eAnim_ThiefToSitting		]	= AnimCreateData( "ThiefToSitting",		&AnimCharacters::Anim_ToSittingGeneric );

	_animMap[ eAnim_ThiefSitBounce		]	= AnimCreateData( "ThiefSitBounce",	NULL );
	_animMap[ eAnim_ThiefRollBounce		]	= AnimCreateData( "ThiefRollBounce", NULL );

	_animMap[ eAnim_PiggyBankRotationFix	]	= AnimCreateData( SoundEngine::eSoundAnim_PiggyRotationFix );
	_animMap[ eAnim_CopRotationFix			]	= AnimCreateData( SoundEngine::eSoundAnim_PiggyRotationFix );
	_animMap[ eAnim_ThiefRotationFix		]	= AnimCreateData( SoundEngine::eSoundAnim_PiggyRotationFix );
}
//----------------------------------------------------------------------------------
AnimCharacters::~AnimCharacters()
{
	_animMap.clear();
	_fixRotationAnimMap.clear();
}
//----------------------------------------------------------------------------------
void AnimCharacters::Anim_SpitterSpit( GameTypes::CharactersAnim anim, puBlock *block )
{
	AnimTools::Get()->AnimSimple( block, _animMap[anim]._animName.c_str(), 1.0f / 45.0f, true, _animMap[anim]._animSound );
	AnimManager::Get()->SetSpriteState( block, eSpriteState_SitAnimating );
}
//----------------------------------------------------------------------------------
void AnimCharacters::Anim_SpitterExplosiveSpit( GameTypes::CharactersAnim anim, puBlock *block )
{
	AnimTools::Get()->AnimSimple( block, _animMap[anim]._animName.c_str(), 1.0f / 45.0f, true, _animMap[anim]._animSound );
	AnimManager::Get()->SetSpriteState( block, eSpriteState_SitAnimating );
}
//----------------------------------------------------------------------------------
void AnimCharacters::Anim_PiggyBounce( GameTypes::CharactersAnim anim, puBlock *block )
{
	unAssertMsg(Game, false, ("Delete me"));
	return;
	
	AnimTools::Get()->AnimSimpleForever( block, _animMap[anim]._animName.c_str(), 1.0f / 20.0f );
	AnimManager::Get()->SetSpriteState( block, eSpriteState_SitAnimating );
}
//----------------------------------------------------------------------------------
void AnimCharacters::Anim_ToRollingGeneric( GameTypes::CharactersAnim anim, puBlock *block )
{
	AnimTools::Get()->AnimSimple( block, _animMap[anim]._animName.c_str(), 1.0f / 24.0f, true, _animMap[anim]._animSound );
	AnimManager::Get()->SetSpriteState( block, eSpriteState_RollAnimating );
}
//----------------------------------------------------------------------------------
void AnimCharacters::Anim_ToSittingGeneric( GameTypes::CharactersAnim anim, puBlock *block )
{
	AnimTools::Get()->AnimSimple( block, _animMap[anim]._animName.c_str(), 1.0f / 24.0f, true, _animMap[anim]._animSound );
	AnimManager::Get()->SetSpriteState( block, eSpriteState_SitAnimating );
}
//----------------------------------------------------------------------------------
void AnimCharacters::Anim_PiggyFromRolling( GameTypes::CharactersAnim anim, puBlock *block )
{
	AnimTools::Get()->AnimSimple( block, _animMap[anim]._animName.c_str(), 1.0f / 24.0f, true, _animMap[anim]._animSound );
	AnimManager::Get()->SetSpriteState( block, eSpriteState_SitAnimating );

	SoundEngine::Get()->PlayEffect( SoundEngine::eSoundAnim_PiggyFromRolling );
}
//----------------------------------------------------------------------------------
void AnimCharacters::Anim_PiggyLevelFailed( GameTypes::CharactersAnim anim, puBlock *block )
{

	if (( AnimManager::Get()->GetSpriteState( block ) == eSpriteState_Roll ) || 
		( block->GetBlockEnum() == GameTypes::eBlockPiggyBankRolled  ))
	{
		AnimTools::Get()->AnimSimple(
			block, 
			_animMap[ eAnim_PiggyBankLevelFailed_Roll ]._animName.c_str(), 
			1.0f / 24.0f, 
			true, 
			_animMap[ eAnim_PiggyBankLevelFailed_Roll ]._animSound 
		);
		AnimManager::Get()->SetSpriteState( block, eSpriteState_RollAnimating );
	}
	else if ( AnimManager::Get()->GetSpriteState( block ) == eSpriteState_Sit )
	{
		AnimTools::Get()->AnimSimple( 
			block, 
			_animMap[ eAnim_PiggyBankLevelFailed_Sit ]._animName.c_str(), 
			1.0f / 24.0f, 
			true, 
			_animMap[ eAnim_PiggyBankLevelFailed_Sit ]._animSound );
		AnimManager::Get()->SetSpriteState( block, eSpriteState_SitAnimating );
	}
}
//----------------------------------------------------------------------------------
void AnimCharacters::Anim_PiggyLevelDone( GameTypes::CharactersAnim anim, puBlock *block )
{

	if (( AnimManager::Get()->GetSpriteState( block ) == eSpriteState_Roll ) || 
		( block->GetBlockEnum() == GameTypes::eBlockPiggyBankRolled ))
	{
	
		AnimTools::Get()->AnimSimple( 
			block, 
			_animMap[ eAnim_PiggyBankLevelDone_Roll ]._animName.c_str(), 
			1.0f / 24.0f, 
			true, 
			_animMap[ eAnim_PiggyBankLevelDone_Roll ]._animSound 
		);
		AnimManager::Get()->SetSpriteState( block, eSpriteState_RollAnimating );
	}
	else if ( AnimManager::Get()->GetSpriteState( block ) == eSpriteState_Sit )
	{
        EndLevelPiggyJumpFix( block );
        
		AnimTools::Get()->AnimSimple( 
			block, 
			_animMap[ eAnim_PiggyBankLevelDone_Sit  ]._animName.c_str(), 
			1.0f / 24.0f, 
			true, 
			_animMap[ eAnim_PiggyBankLevelDone_Sit ]._animSound 
			);
		AnimManager::Get()->SetSpriteState( block, eSpriteState_SitAnimating );
	}
}
//----------------------------------------------------------------------------------
void AnimCharacters::EndLevelPiggyJumpFix( puBlock *block )
{
    LevelOrientation orient;
    orient = block->GetLevel()->GetOrientation();
       
    float angleLimit;
    float rotation;

    
    angleLimit = 45.0f;
    rotation = block->GetRotation();

    D_LOG( "rotation %f", block->GetRotation() );
    D_LOG( "sprite delta %f" , block->GetSpriteDeltaRotation() );

    if ( orient == GameTypes::eLevel_Vertical )
        rotation -= 90.0f;

    rotation = Tools::RotationNormalize( rotation );
    
    if ( abs(rotation) > angleLimit )
    {
        block->SetSpriteDeltaRotation( -rotation );
        block->UpdateSprite();
    }
}
//----------------------------------------------------------------------------------
void AnimCharacters::Anim_RotationFix_Start( GameTypes::CharactersAnim anim, puBlock *block )
{
	D_SIZE( _fixRotationAnimMap );
	if ( ! _fixRotationAnimMap.count( block->GetBlockEnum() ))
	{
		unAssertMsg(Game, false, ( "Missing fix rotation anim for %s", block->GetClassName2()  ));
		AnimManager::Get()->AnimFinished( block );
		return;
	}

	//-----------------------------
	// Init
	float scale;
	CCSprite **sprite;
	CCSprite **spriteShadow;

	CCAnimate *animStart		= NULL;
	CCAnimate *animShadowStart	= NULL;
	FixRotationAnimData rotationAnimData;

	float fps;

	fps = 24.0f;
	
	sprite			= block->GetSpritePtr();
	spriteShadow	= block->GetSpriteShadowPtr();
	scale			= block->GetScaleToDefault();
	rotationAnimData = _fixRotationAnimMap[ block->GetBlockEnum() ];


	//------------------------------------------
	// Find proper size, otherwise load default
	stringstream ssStart;
	ssStart << rotationAnimData._animStart.c_str();
	ssStart << block->GetWidth() * RATIO / 2.0f;
	
	animStart	= AnimTools::Get()->GetAnimation( ssStart.str().c_str(), 1.0f / fps );

	if ( ! animStart )
	{
		ssStart.str("");
		ssStart << rotationAnimData._animStart.c_str() << Config::AnimCircleDefaultSize;

		animStart		= AnimTools::Get()->GetAnimation( ssStart.str().c_str(), 1.0f / fps );
		animShadowStart = AnimTools::Get()->GetAnimation( ssStart.str().c_str(), 1.0f / fps );

		( *sprite )			= CCSprite::spriteWithSpriteFrame( *( animStart->getAnimation()->getFrames()->begin() ) );
		( *spriteShadow )	= CCSprite::spriteWithSpriteFrame( *( animStart->getAnimation()->getFrames()->begin() ) );
		
		( *sprite )->setScale( scale );
		( *spriteShadow )->setScale( scale );
	}
	else
	{
		( *sprite )			= CCSprite::spriteWithSpriteFrame( *( animStart->getAnimation()->getFrames()->begin() ) );
		( *spriteShadow )	= CCSprite::spriteWithSpriteFrame( *( animStart->getAnimation()->getFrames()->begin() ) );

		animShadowStart	= AnimTools::Get()->GetAnimation( ssStart.str().c_str(), 1.0f / fps );
	}

	//-------------------------------------------
	// Make a copy of block and this, as running level 
	// is callback caller, not AnimCharacter
	AnimCharacterData *animData;
	animData = _fixRotationDataBuffer.GetNext();
	animData->_animCharaters = this;
	animData->_block = block;

	
	//-------------------------------
	// Anim Start 
	CCActionInterval* animSeq;
	animSeq = (CCActionInterval*)(CCSequence::actions( 
		animStart,
		CCCallFuncND::actionWithTarget( Game::Get()->GetRuningLevel(), callfuncND_selector( AnimCharacters::Anim_RotationFix_Loop ), (void*) animData ),
		NULL ));

	AnimTools::Get()->SpriteSetFlip( block );
	( *sprite )->runAction( animSeq );
	
	
	//------------------------
	// Anim start Sprite shadow
	{
		ccColor3B shadow;
		shadow.r = 0;
		shadow.g = 0;
		shadow.b = 0;

		( *spriteShadow )->setColor( shadow );
		( *spriteShadow )->setOpacity( Config::ShadowOpacity );
		( *spriteShadow )->runAction( animShadowStart );
	}
	
	AnimManager::Get()->SetSpriteState( block, eSpriteState_SitAnimating );
	block->UpdateSprite();
}
//----------------------------------------------------------------------------------
void AnimCharacters::Anim_RotationFix_Loop( CCNode *sender, void *data )
{
	AnimCharacterData *animData;
	animData = (AnimCharacterData *) data;
	
	if ( ! animData )
		return;

    D_SIZE( animData->_animCharaters->_fixRotationAnimMap );
	D_INT( animData->_block->GetBlockEnum() );

	if ( ! animData->_animCharaters->_fixRotationAnimMap.count( animData->_block->GetBlockEnum() ))
	{
		unAssertMsg(Game, false, ( "Missing fix rotation anim for %s", animData->_block->GetClassName2()  ));
		AnimManager::Get()->AnimFinished( animData->_block );
		return;
	}

	CCAnimate *anim360		= NULL;
	CCAnimate *animShadow360	= NULL;

	float fps;
	fps = 24.0f;

	float scale;
	CCSprite **sprite;
	CCSprite **spriteShadow;
	float rotation;
	float rotationDelta;


	//-------------------------------
	// Rotation calculation
	rotation = animData->_block->GetRotation();
	rotation = Tools::RotationNormalize( rotation ) + animData->_block->GetSpriteDeltaRotation();
	if ( Game::Get()->GetRuningLevel()->GetOrientation() == GameTypes::eLevel_Vertical ) 
		rotationDelta = -90.0f;
	else 
		rotationDelta = 0.0f;

	rotation += rotationDelta;
	if ( abs(rotation) > 180.0f  )
	{
		float rotTemp;
		float sign;

		rotTemp = abs ( rotation );
		sign = rotation / rotTemp;

		rotTemp -= 180.0f ;
		rotTemp = 180.0f - rotTemp;
		rotation = rotTemp * sign * -1.0f;
	}

	//rotation = rotationDelta - rotation;

	sprite = animData->_block->GetSpritePtr();
	spriteShadow = animData->_block->GetSpriteShadowPtr();

	scale = animData->_block->GetScaleToDefault();

	int frameCountTotal;
	float degreePerFrame;
	int frameToPlayCount;
	FixRotationAnimData rotationAnimData;

	rotationAnimData = animData->_animCharaters->_fixRotationAnimMap[ animData->_block->GetBlockEnum() ];

	//frameCountTotal =  AnimTools::Get()->GetFrameCount( rotationAnimData._anim360.c_str() );
	frameCountTotal = rotationAnimData._frameCount;
	degreePerFrame = 360.0f / (float) frameCountTotal;
	frameToPlayCount = abs((int) ( rotation / degreePerFrame ));

	//------------------------------------------
	// Find proper size, otherwise load default
	stringstream ss360;

	ss360 << rotationAnimData._anim360.c_str();
	ss360 << animData->_block->GetWidth() * RATIO / 2.0f;

	anim360		= AnimTools::Get()->GetAnimation( ss360.str().c_str(), 1.0f / fps, frameToPlayCount );

	if ( ! anim360  )
	{
		ss360.str("");
		ss360 << rotationAnimData._anim360.c_str();
		ss360 << Config::AnimCircleDefaultSize;

		anim360			= AnimTools::Get()->GetAnimation( ss360.str().c_str(), 1.0f / fps, frameToPlayCount );
		animShadow360	= AnimTools::Get()->GetAnimation( ss360.str().c_str(), 1.0f / fps, frameToPlayCount );

		( *sprite )->setScale( scale );
		( *spriteShadow )->setScale( scale );
	}
	else
	{
		animShadow360	= AnimTools::Get()->GetAnimation( ss360.str().c_str(), 1.0f / fps, frameToPlayCount );
	}

	//-------------------------------------------
	{
		CCActionInterval* animSeq;
		if (( rotation >= 0  && animData->_block->GetFlipX() == puBlock::eAxisNormal )  ||
			( rotation < 0  && animData->_block->GetFlipX() == puBlock::eAxisFlipped ))
		{
			animSeq = (CCActionInterval*)(CCSequence::actions( 
				anim360,
				CCCallFuncND::actionWithTarget( 
					Game::Get()->GetRuningLevel(), 
					callfuncND_selector( AnimCharacters::Anim_RotationFixGeneric_SetSpriteDelta ), (void*) animData->_block ),
				CCCallFuncND::actionWithTarget( 
					Game::Get()->GetRuningLevel(), 
					callfuncND_selector( AnimCharacters::Anim_RotationFix_End ), (void*) animData ),
				NULL));
		}
		else
		{
			animSeq = (CCActionInterval*)(CCSequence::actions( 
				CCCallFuncND::actionWithTarget( 
					Game::Get()->GetRuningLevel(), 
					callfuncND_selector( AnimCharacters::Anim_RotationFixGeneric_SetSpriteDelta ), (void*) animData->_block ),
				anim360->reverse(),
				CCCallFuncND::actionWithTarget( 
					Game::Get()->GetRuningLevel(), 
					callfuncND_selector( AnimCharacters::Anim_RotationFix_End ), (void*) animData ),
				NULL));
		}

		AnimTools::Get()->SpriteSetFlip( animData->_block );
		( *sprite )->runAction( animSeq );
	}

	//------------------------
	// Sprite shadow
	{
		ccColor3B shadow;
		shadow.r = 0;
		shadow.g = 0;
		shadow.b = 0;

		( *spriteShadow )->setColor( shadow );
		( *spriteShadow )->setOpacity( Config::ShadowOpacity );

		if (
			( rotation >= 0  && animData->_block->GetFlipX() == puBlock::eAxisNormal )  ||
			( rotation < 0   && animData->_block->GetFlipX() == puBlock::eAxisFlipped )
			)

		{
			( *spriteShadow )->runAction( animShadow360 );
		}
		else
		{
			( *spriteShadow )->runAction( animShadow360->reverse() );
		}
	}
	RotationFixLoopSound( frameToPlayCount, fps );
}
//----------------------------------------------------------------------------------
void AnimCharacters::Anim_RotationFix_End( CCNode *sender, void *data )
{
	AnimCharacterData *animData;
	animData = (AnimCharacterData *) data;

	if ( ! animData )
		return;

	D_SIZE( animData->_animCharaters->_fixRotationAnimMap );
	if ( ! animData->_animCharaters->_fixRotationAnimMap.count( animData->_block->GetBlockEnum() ))
	{
		unAssertMsg(Game, false, ( "Missing fix rotation anim for %s", animData->_block->GetClassName2()  ));
		AnimManager::Get()->AnimFinished( animData->_block );
		return;
	}

	CCAnimate *animEnd		= NULL;
	CCAnimate *animShadowEnd	= NULL;

	float fps;
	fps = 24.0f;

	float scale;
	CCSprite **sprite;
	CCSprite **spriteShadow;
	FixRotationAnimData rotationAnimData;


	sprite = animData->_block->GetSpritePtr();
	spriteShadow = animData->_block->GetSpriteShadowPtr();

	scale = animData->_block->GetScaleToDefault();
	rotationAnimData = animData->_animCharaters->_fixRotationAnimMap[ animData->_block->GetBlockEnum() ];


	//------------------------------------------
	// Find proper size, otherwise load default
	stringstream ssEnd;

	ssEnd << rotationAnimData._animEnd.c_str();
	ssEnd << animData->_block->GetWidth() * RATIO / 2.0f;

	animEnd		= AnimTools::Get()->GetAnimation( ssEnd.str().c_str(), 1.0f / fps );

	if ( ! animEnd )
	{
		ssEnd.str("");
		ssEnd << rotationAnimData._animEnd.c_str();
		ssEnd << Config::AnimCircleDefaultSize;

		animEnd			= AnimTools::Get()->GetAnimation( ssEnd.str().c_str(), 1.0f  / fps );
		animShadowEnd	= AnimTools::Get()->GetAnimation( ssEnd.str().c_str(), 1.0f  / fps );

		( *sprite )->setScale( scale );
		( *spriteShadow )->setScale( scale );
	}
	else
	{
		animShadowEnd	= AnimTools::Get()->GetAnimation( ssEnd.str().c_str(), 1.0f / fps );
	}
	//-------------------------------------------


	CCActionInterval* animSeq;
	animSeq = (CCActionInterval*)(CCSequence::actions( 
		animEnd,	
		CCCallFuncND::actionWithTarget( 
			Game::Get()->GetRuningLevel(), 
			callfuncND_selector( AnimCharacters::Anim_RotationFixGeneric_Finish ), (void*) animData->_block ),
		NULL));
		
	AnimTools::Get()->SpriteSetFlip( animData->_block );
	( *sprite )->runAction( animSeq );


	//------------------------
	// Sprite shadow
	{
		ccColor3B shadow;
		shadow.r = 0;
		shadow.g = 0;
		shadow.b = 0;

		( *spriteShadow )->setColor( shadow );
		( *spriteShadow )->setOpacity( Config::ShadowOpacity );
		( *spriteShadow )->runAction( animShadowEnd );
	}
}
//----------------------------------------------------------------------------------
void AnimCharacters::Anim_RotationFixGeneric_SetSpriteDelta( CCNode *sender, void *data )
{
	D_SIZE( _fixRotationAnimMap );
	puBlock *block;
	block = (puBlock *) data;

	if ( ! block )
		return;

	float rotation;
	rotation = block->GetRotation();
	rotation = Tools::RotationNormalize( rotation );

	if ( Game::Get()->GetRuningLevel()->GetOrientation() == GameTypes::eLevel_Vertical ) 
		rotation -= 90.0f;

	block->SetSpriteDeltaRotation( -rotation );
	block->UpdateSprite();
}
//----------------------------------------------------------------------------------
void AnimCharacters::Anim_RotationFixGeneric_Finish( CCNode *sender, void *data )
{
	puBlock *block;
	block = (puBlock *) data;

	if ( ! block )
		return;

	AnimManager::Get()->AnimFinished( block );
}
//----------------------------------------------------------------------------------
void AnimCharacters::PlayAnim( GameTypes::CharactersAnim anim, puBlock *block )
{
	AnimFuncPtr fn;

	fn = _animMap[ anim ]._createFunc;
	
	if ( ! fn )
	{
		unAssertMsg( Game, false, ("Anim: %d don't exist", anim ));
		return;
	}

	(this->*fn)( anim, block );
}
//----------------------------------------------------------------------------------
void AnimCharacters::Anim_PiggyCoinCollected( GameTypes::CharactersAnim anim, puBlock *block )
{

	if (( AnimManager::Get()->GetSpriteState( block ) == eSpriteState_Roll ) || ( block->GetBlockEnum() == GameTypes::eBlockPiggyBankRolled ))
	{
		AnimTools::Get()->AnimSimple( 
			block, 
			_animMap[ eAnim_PiggyBankCoinCollected_Roll ]._animName.c_str(), 
			1.0f / 24.0f, 
			true, 
			_animMap[ eAnim_PiggyBankCoinCollected_Roll ]._animSound );

		AnimManager::Get()->SetSpriteState( block, eSpriteState_RollAnimating );
	}
	else if ( AnimManager::Get()->GetSpriteState( block ) == eSpriteState_Sit )
	{
		AnimTools::Get()->AnimSimple(
			block, 
			_animMap[ eAnim_PiggyBankCoinCollected_Sit ]._animName.c_str(), 
			1.0f / 24.0f, 
			true, 
			_animMap[ eAnim_PiggyBankCoinCollected_Sit]._animSound );

		AnimManager::Get()->SetSpriteState( block, eSpriteState_SitAnimating );
	}
}
//----------------------------------------------------------------------------------
void AnimCharacters::Anim_CopCaughtThief( GameTypes::CharactersAnim anim, puBlock *block )
{

	if ( AnimManager::Get()->GetSpriteState( block ) == eSpriteState_Roll )
	{
		AnimTools::Get()->AnimSimple( block, _animMap[ eAnim_CopSmiles_Roll ]._animName.c_str(), 1.0f / 24.0f, true, _animMap[ eAnim_CopSmiles_Roll ]._animSound );
		AnimManager::Get()->SetSpriteState( block, eSpriteState_RollAnimating );
	}
	else if ( AnimManager::Get()->GetSpriteState( block ) == eSpriteState_Sit )
	{
		AnimTools::Get()->AnimSimple( block, _animMap[ eAnim_CopSmiles_Sit ]._animName.c_str(), 1.0f / 24.0f, true, _animMap[ eAnim_CopSmiles_Sit ]._animSound );
		AnimManager::Get()->SetSpriteState( block, eSpriteState_SitAnimating );
	}
}
//----------------------------------------------------------------------------------
void AnimCharacters::Anim_ThiefItemStolen( GameTypes::CharactersAnim anim, puBlock *block )
{

	if (( AnimManager::Get()->GetSpriteState( block ) == eSpriteState_Roll ) ||
		( block->GetBlockEnum() == GameTypes::eBlockThiefRolled ))

	{
		AnimTools::Get()->AnimSimple( block, _animMap[ eAnim_ThiefSmiles_Roll ]._animName.c_str(), 1.0f / 24.0f, true, _animMap[ eAnim_ThiefSmiles_Roll ]._animSound );
		AnimManager::Get()->SetSpriteState( block, eSpriteState_RollAnimating );
	}
	else if ( AnimManager::Get()->GetSpriteState( block ) == eSpriteState_Sit )
	{
		AnimTools::Get()->AnimSimple( block, _animMap[ eAnim_ThiefSmiles_Sit ]._animName.c_str(), 1.0f / 24.0f, true, _animMap[ eAnim_ThiefSmiles_Sit ]._animSound );
		AnimManager::Get()->SetSpriteState( block, eSpriteState_SitAnimating );
	}
}
//----------------------------------------------------------------------------------
void AnimCharacters::Anim_PiggyStolen( GameTypes::CharactersAnim anim, puBlock *block )
{

	if (( AnimManager::Get()->GetSpriteState( block ) == eSpriteState_Roll ) ||
		( block->GetBlockEnum() == GameTypes::eBlockPiggyBankRolled ))

	{
		AnimTools::Get()->AnimSimpleAndRevers( block, _animMap[ eAnim_PiggyBankStolen_Roll ]._animName.c_str(), 1.0f / 24.0f, true, 2.0f, _animMap[ eAnim_PiggyBankStolen_Roll ]._animSound );
		AnimManager::Get()->SetSpriteState( block, eSpriteState_RollAnimating );
	}
	else if ( AnimManager::Get()->GetSpriteState( block ) == eSpriteState_Sit )
	{
		AnimTools::Get()->AnimSimpleAndRevers( block, _animMap[ eAnim_PiggyBankStolen_Sit ]._animName.c_str(), 1.0f / 24.0f, true, 2.0f, _animMap[ eAnim_PiggyBankStolen_Sit ]._animSound );
		AnimManager::Get()->SetSpriteState( block, eSpriteState_SitAnimating );
	}
}
//----------------------------------------------------------------------------------
void AnimCharacters::Anim_IdelGeneric( GameTypes::CharactersAnim anim, puBlock *block )
{
	AnimTools::Get()->AnimSimple( block, _animMap[ anim ]._animName.c_str(), 1.0f / 24.0f, true, _animMap[anim]._animSound );
	AnimManager::Get()->SetSpriteState( block, eSpriteState_SitAnimating );
}
//----------------------------------------------------------------------------------
void AnimCharacters::LoadToCache( GameTypes::CharactersAnim anim, float radius )
{

	AnimCreateData animData;

	animData = _animMap[anim];

	if ( animData._animSound != SoundEngine::eSoundNone )
		SoundEngine::Get()->LoadSound( animData._animSound );

	switch ( anim )
	{
		//---------------------------
		case eAnim_PiggyBankRotationFix :
			AnimTools::Get()->PreloadAdjustedAnimFrames( radius, _fixRotationAnimMap[ eBlockPiggyBank ]._animStart.c_str() );
			AnimTools::Get()->PreloadAdjustedAnimFrames( radius, _fixRotationAnimMap[ eBlockPiggyBank ]._animEnd.c_str() );
			AnimTools::Get()->PreloadAdjustedAnimFrames( radius, _fixRotationAnimMap[ eBlockPiggyBank ]._anim360.c_str() );
			break;

		//---------------------------
		case eAnim_CopRotationFix :
			AnimTools::Get()->PreloadAdjustedAnimFrames( radius, _fixRotationAnimMap[ eBlockCop ]._animStart.c_str() );
			AnimTools::Get()->PreloadAdjustedAnimFrames( radius, _fixRotationAnimMap[ eBlockCop ]._animEnd.c_str() );
			AnimTools::Get()->PreloadAdjustedAnimFrames( radius, _fixRotationAnimMap[ eBlockCop ]._anim360.c_str() );
		break;

		//---------------------------
		case eAnim_ThiefRotationFix :
			AnimTools::Get()->PreloadAdjustedAnimFrames( radius, _fixRotationAnimMap[ eBlockThief ]._animStart.c_str() );
			AnimTools::Get()->PreloadAdjustedAnimFrames( radius, _fixRotationAnimMap[ eBlockThief ]._animEnd.c_str() );
			AnimTools::Get()->PreloadAdjustedAnimFrames( radius, _fixRotationAnimMap[ eBlockThief ]._anim360.c_str() );
		break;

		//---------------------------
		default:
			if ( animData._animName.length() != 0 )
				AnimTools::Get()->PreloadAdjustedAnimFrames( radius, animData._animName.c_str() );
				
		;
	}

}
//----------------------------------------------------------------------------------
AnimCharacters::BlockMap AnimCharacters::GetRotationFixMap()
{
	return _fixRotationAnimMap;
}
//----------------------------------------------------------------------------------
void AnimCharacters::RotationFixLoopSound( int frameCount, float fps )
{

	float soundCount;
	int soundCountInt;
	float soundDur;

	soundDur = SoundEngine::Get()->GetEffectDuration( SoundEngine::eSoundAnim_PiggyRotationFix );
	soundCount = ( frameCount / fps ) / soundDur;
	soundCountInt = (int) soundCount;
	
	if ( soundCountInt <= 0 )
		return;

	CCActionInterval *seq = (CCActionInterval*) CCSequence::actions( 
		CCCallFunc::actionWithTarget( Game::Get()->GetRuningLevel(), callfunc_selector( AnimCharacters::RotationFixLoopSoundCallback )), 
		CCDelayTime::actionWithDuration( soundDur ),
		NULL
		);

	CCRepeat *repeat = CCRepeat::actionWithAction( seq, soundCountInt );
	Game::Get()->GetRuningLevel()->runAction( repeat );
}
//----------------------------------------------------------------------------------
void AnimCharacters::RotationFixLoopSoundCallback()
{
	SoundEngine::Get()->PlayEffect(	SoundEngine::eSoundAnim_PiggyRotationFix );
}



//----------------------------------------------------------------------------------
// AnimCharacterData
//----------------------------------------------------------------------------------
AnimCharacterData::AnimCharacterData()
{
	_animCharaters = NULL;
	_block = NULL;
}
//----------------------------------------------------------------------------------
AnimCharacterData::AnimCharacterData( AnimCharacters *animChar, puBlock *block )
{
	_animCharaters = animChar;
	_block = block;
}



//----------------------------------------------------------------------------------
// Anim Character Data Buffer
//----------------------------------------------------------------------------------
AnimCharacterDataBuffer::AnimCharacterDataBuffer()
{

	_bufferIndex = 0;
}
//----------------------------------------------------------------------------------
AnimCharacterData* AnimCharacterDataBuffer::GetNext()
{

	AnimCharacterData *ret;
	ret = & ( _buffer[_bufferIndex] );
	
	_bufferIndex++;
	
	if ( _bufferIndex >= BUFFER_SIZE )
		_bufferIndex = 0;

	return ret;
}
