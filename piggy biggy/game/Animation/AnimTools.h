#ifndef __ANIMTOOLS_H__
#define __ANIMTOOLS_H__

#include "cocos2d.h"
#include "Blocks/puBlock.h"
#include "SoundEngine.h"

USING_NS_CC;

//----------------------------------------------------------
// Anim Tools
//----------------------------------------------------------
class AnimTools
{
public:
	static AnimTools* Get();
	static void Destroy();

	~AnimTools();

	CCAnimation* GetAnimFrames( const char *filename, int frameCount = -1 );
	CCAnimation* GetAnimFrames( const char *filename, int frameStart, int frameCount );

	bool PreloadAnimFrames( const char *filename  );
	CCAnimate* GetAnimation( const char *filename, float framePerSecond, int frameCount = -1 );
	int GetFrameCount( const char *filename );
	
	CCAnimation *GetBlockAdjustedAnimFrames( puBlock *block, const char *filename, int frameCount = -1, bool setSprite = true  );
	void PreloadAdjustedAnimFrames( float size, const char *filename );
	
	void AnimSimple( puBlock *block, const char *name, float framePerSec, bool resetToOrginal, SoundEngine::Sound sound );
	void AnimReversed( puBlock *block, const char *name, float framePerSec, bool resetToOrginal, SoundEngine::Sound sound );
	void AnimSimpleAndRevers( puBlock *block, const char *name, float framePerSec, bool resetToOrginal, float holdSec );
	void AnimSimpleAndRevers( puBlock *block, const char *name, float framePerSec, bool resetToOrginal, float holdSec, SoundEngine::Sound sound );
	void AnimSimpleForever( puBlock *block, const char *name, float framePerSec );
	void SpriteSetFlip( puBlock *block );
	void SpriteSetFlip( puBlock *block, CCSprite*sprite );
	void AnimFinishCallback( CCNode *node, void *data );
	void AnimFinishNoResetCallback( CCNode *node, void *data );

private:
	AnimTools();
	string GetAnimFile( const char *filename );
	void AnimSimple( puBlock *block, const char *name, float framePerSec, bool resetToOrginal, bool reversed = false );

	

private:
	static AnimTools* _sInstance;
};
//------------------------------------------------------------------------------------
#endif
