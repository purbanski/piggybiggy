#ifndef __ANIMSPRITES_H__
#define __ANIMSPRITES_H__

#include "cocos2d.h"
#include "Blocks/puBlock.h"

USING_NS_CC;

//-----------------------------------------------------
class AnimSprite : public CCSprite
{
public:
	static AnimSprite* Create( puBlock *block, const char *animName, float fps = 24.0f );
	void Construct( puBlock *block, const char *animName, float fps );

private:
	AnimSprite(); 
};
//-----------------------------------------------------

#endif
