#include "AnimTools.h"
#include "AnimManager.h"
#include "Skins.h"
#include "Levels/Level.h"
#include <sstream>

//----------------------------------------------------------------------------------
// Anim Tools
//----------------------------------------------------------------------------------
AnimTools* AnimTools::_sInstance = NULL;
//----------------------------------------------------------------------------------
AnimTools::AnimTools()
{

}
//----------------------------------------------------------------------------------
AnimTools::~AnimTools()
{
}
//----------------------------------------------------------------------------------
AnimTools* AnimTools::Get()
{
	if ( ! _sInstance )
		_sInstance = new AnimTools();

	return _sInstance;
}
//----------------------------------------------------------------------------------
void AnimTools::Destroy()
{
	if ( _sInstance )
		delete _sInstance;

	_sInstance = NULL;
}
//----------------------------------------------------------------------------------
void AnimTools::AnimSimple( puBlock *block, const char *name, float framePerSec, bool resetToOrginal, bool reversed )
{
	float scale;
	CCSprite **sprite;
	CCSprite **spriteShadow;
	stringstream ss;

	sprite = block->GetSpritePtr();
	spriteShadow = block->GetSpriteShadowPtr();
	scale = block->GetScaleToDefault();

	CCAnimation *animation;
	animation = GetBlockAdjustedAnimFrames( block, name );
	
	if ( ! animation )
	{
		AnimManager::Get()->AnimFinished( block );
		return;
	}
	animation->setDelay( framePerSec );

	//------------------------
	// Sprite
	{
		CCAnimate *animate = CCAnimate::actionWithAnimation( animation, false );
		
		if ( reversed )
			animate->reverse();

		CCActionInterval* seq;

		if ( resetToOrginal )
		{
			seq = (CCActionInterval*)(CCSequence::actions( 
				animate,
				CCCallFuncND::actionWithTarget( 
				block->GetLevel(), 
				callfuncND_selector( AnimTools::AnimFinishCallback ), 
				(void*) block ), 
				NULL ));
		}
		else
		{
			seq = (CCActionInterval*)(CCSequence::actions( 
				animate,
				CCCallFuncND::actionWithTarget( 
				block->GetLevel(), 
				callfuncND_selector( AnimTools::AnimFinishNoResetCallback ), 
				(void*) block ), 
				NULL ));
		}
		( *sprite )->runAction( seq );

		SpriteSetFlip( block );
	}

	//------------------------
	// Sprite shadow
	{
		CCAnimation *animationShadow = CCAnimation::animationWithFrames( animation->getFrames() );
		animationShadow->setDelay( framePerSec );

		CCAnimate *animate = CCAnimate::actionWithAnimation(animationShadow, false);

		if ( reversed )
			animate->reverse();

		CCActionInterval* seq = (CCActionInterval*)(CCSequence::actions( 
			animate,
			NULL) );

		( *spriteShadow )->runAction( seq );
	}

	block->UpdateSprite();
}
//----------------------------------------------------------------------------------
void AnimTools::AnimReversed( puBlock *block, const char *name, float framePerSec, bool resetToOrginal, SoundEngine::Sound sound )
{
	AnimSimple( block, name, framePerSec, resetToOrginal, true );
	SoundEngine::Get()->PlayEffect( sound );
}
//----------------------------------------------------------------------------------
void AnimTools::AnimSimple( puBlock *block, const char *name, float framePerSec, bool resetToOrginal, SoundEngine::Sound sound )
{
	AnimSimple( block, name, framePerSec, resetToOrginal );
	SoundEngine::Get()->PlayEffect( sound );
}
//----------------------------------------------------------------------------------
void AnimTools::AnimSimpleAndRevers( puBlock *block, const char *name, float framePerSec, bool resetToOrginal, float holdSec, SoundEngine::Sound sound )
{
	AnimSimpleAndRevers( block, name, framePerSec, resetToOrginal, holdSec );
	SoundEngine::Get()->PlayEffect( sound );
}
//----------------------------------------------------------------------------------
void AnimTools::AnimSimpleAndRevers( puBlock *block, const char *name, float framePerSec, bool resetToOrginal, float holdSec  )
{
	CCSprite **sprite;
	CCSprite **spriteShadow;

	sprite = block->GetSpritePtr();
	spriteShadow = block->GetSpriteShadowPtr();

	CCAnimation *animation;
	animation = GetBlockAdjustedAnimFrames( block, name );
	if ( ! animation )
	{
		AnimManager::Get()->AnimFinished( block );
		return;
	}

	animation->setDelay( framePerSec );


	//------------------------
	// Sprite
	{
		
		CCAnimate *animate = CCAnimate::actionWithAnimation( animation, false );
		
		CCActionInterval* seq;

		if ( resetToOrginal )
		{
			seq = (CCActionInterval*)(CCSequence::actions( 
				animate,
				CCDelayTime::actionWithDuration( holdSec ),
				animate->reverse(),
				CCCallFuncND::actionWithTarget( 
				block->GetLevel(), 
				callfuncND_selector( AnimTools::AnimFinishCallback ), 
				(void*) block ), 
				NULL) );
		}
		else
		{
			seq = (CCActionInterval*)(CCSequence::actions( 
				animate,
				CCDelayTime::actionWithDuration( holdSec ),
				animate->reverse(),
				CCCallFuncND::actionWithTarget( 
				block->GetLevel(), 
				callfuncND_selector( AnimTools::AnimFinishNoResetCallback ), 
				(void*) block ), 
				NULL) );
		}

		( *sprite )->runAction( seq );

		SpriteSetFlip( block );
	}


	//------------------------
	// Sprite shadow
	{
		CCAnimation *animation = GetBlockAdjustedAnimFrames( block, name );
		animation->setDelay( framePerSec );

		CCAnimate *animate = CCAnimate::actionWithAnimation(animation, false);

		CCActionInterval* seq = (CCActionInterval*)(CCSequence::actions( 
			animate,
			CCDelayTime::actionWithDuration( holdSec ),
			animate->reverse(),
			NULL) );

		( *spriteShadow )->runAction( seq );
	}

	block->UpdateSprite();
}
//----------------------------------------------------------------------------------
CCAnimation* AnimTools::GetAnimFrames( const char *filename, int frameCount )
{
	return GetAnimFrames( filename, 1, frameCount );
}
//----------------------------------------------------------------------------------
CCAnimation* AnimTools::GetAnimFrames( const char *filename, int frameStart, int frameCount )
{
	stringstream frameFileStream;
	string file;
	int count;

	file = GetAnimFile( filename );
	count = frameStart;

	if ( ! unResourceFileCheckExists( file.c_str() ))
	{
		DS_LOG( "Missing %s", file.c_str() );
		return NULL;
	}
	
//	D_LOG("Loading %s", file.c_str() );
	CCSpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile( file.c_str() );

	CCSpriteFrame *frame;
	CCAnimation *animFrames = CCAnimation::animation();

	do
	{
		frameFileStream.str("");
		frameFileStream << filename << "-f" << count <<  ".png";

		frame = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName( frameFileStream.str().c_str() );
		if ( frame )
			animFrames->addFrame(frame);
		count++;
		frameCount--;
	} while ( frame != NULL && frameCount != 0 );

	return animFrames;
}
//----------------------------------------------------------------------------------
CCAnimate* AnimTools::GetAnimation( const char *filename, float framePerSecond, int frameCount )
{
	CCAnimation *animation = GetAnimFrames( filename, frameCount );
	if ( ! animation )
	{
		unAssertMsg(AnimTools, false, ("Animation not found %s", filename ));
		return NULL;
	}
	animation->setDelay( framePerSecond );

	CCAnimate *animate = CCAnimate::actionWithAnimation( animation, false );
	return animate;
}
//----------------------------------------------------------------------------------
void AnimTools::AnimSimpleForever( puBlock *block, const char *name, float framePerSec )
{

	float scale;
	CCSprite **sprite;
	CCSprite **spriteShadow;

	sprite = block->GetSpritePtr();
	spriteShadow = block->GetSpriteShadowPtr();
	scale =(*sprite)->getScale();
	
	CCAnimation *animation;
	animation = GetAnimFrames( name );

	if ( ! animation )
		return;
	animation->setDelay( framePerSec );

	//------------------------
	// Sprite
	{
		CCAnimate *animate = CCAnimate::actionWithAnimation( animation, false );
		CCActionInterval* seq = (CCActionInterval*)(CCSequence::actions( 
			animate,
			NULL) );

		( *sprite )= CCSprite::spriteWithSpriteFrame( *animation->getFrames()->begin() );
		( *sprite )->setScale( scale );
		( *sprite )->runAction( CCRepeatForever::actionWithAction( seq ));

		SpriteSetFlip( block );
	}

	//------------------------
	// Sprite shadow
	{
		CCAnimation *animationShadow;
		animationShadow = CCAnimation::animationWithFrames( animation->getFrames() );
		animationShadow->setDelay( framePerSec );
	
		CCAnimate *animate = CCAnimate::actionWithAnimation( animationShadow, false);

		CCActionInterval* seq = (CCActionInterval*)(CCSequence::actions( 
			animate,
			NULL) );

		( *spriteShadow )= CCSprite::spriteWithSpriteFrame( *animationShadow->getFrames()->begin() );
		( *spriteShadow )->setScale( scale );
		( *spriteShadow )->runAction( CCRepeatForever::actionWithAction( seq ));
	}

	block->UpdateSprite();
}
//----------------------------------------------------------------------------------
void AnimTools::SpriteSetFlip( puBlock *block )
{

	SpriteSetFlip( block, block->GetSprite() );
}
//----------------------------------------------------------------------------------
void AnimTools::SpriteSetFlip( puBlock *block, CCSprite *sprite )
{

	if ( block->GetFlipX() == puBlock::eAxisFlipped )
		sprite->setFlipX( true );

	if ( block->GetFlipY() == puBlock::eAxisFlipped )
		sprite->setFlipY( true );
}
//----------------------------------------------------------------------------------
void AnimTools::AnimFinishCallback( CCNode *node, void *data )
{
	puBlock *block;
	block = ( puBlock *) data;

	AnimManager::Get()->AnimFinished( block );
}
//----------------------------------------------------------------------------------
void AnimTools::AnimFinishNoResetCallback( CCNode *node, void *data )
{
	puBlock *block;
	block = ( puBlock *) data;

	AnimManager::Get()->AnimFinishedNoReset( block );
}
//----------------------------------------------------------------------------------
int AnimTools::GetFrameCount( const char *filename )
{
	stringstream frameFileStream;
	string file;
	int count;

	file = GetAnimFile( filename );
	count = 1;

	if ( ! unResourceFileCheckExists( file.c_str() ))
	{
		return 0;
	}

	CCSpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile( file.c_str() );
	CCSpriteFrame *frame;
	
	do
	{
		frameFileStream.str("");
		frameFileStream << filename << "-f" << count <<  ".png";

		frame = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName( frameFileStream.str().c_str() );
		if ( frame )
			count++;
	} while ( frame != NULL );

	return count;
}
//----------------------------------------------------------------------------------
string AnimTools::GetAnimFile( const char *filename )
{
	string file;
	file.append( "Images/Blocks/Animation/" );
	file.append( filename );
	file.append( ".plist" );
	file = Skins::GetSkinName( file.c_str() );

	return file;
}
//----------------------------------------------------------------------------------
CCAnimation* AnimTools::GetBlockAdjustedAnimFrames( puBlock *block, const char *filename, int frameCount, bool setSprite )
{
	float scale;
	stringstream ss;
	CCAnimation *animFrames;
	CCSprite **sprite;
	CCSprite **spriteShadow;

	sprite = block->GetSpritePtr();
	spriteShadow = block->GetSpriteShadowPtr();

	scale = block->GetScaleToDefault();

	//------------------------------------------
	// Find proper size, otherwise load default
	ss << filename;
	ss << block->GetWidth() * RATIO / 2.0f;
	animFrames = GetAnimFrames( ss.str().c_str(), frameCount );

	if ( ! animFrames )
	{
		DS_LOG_MISSING_PNG( "- %s", ss.str().c_str() );
		D_LOG("Animi %s missing, loading default", ss.str().c_str() );
		unAssertMsg( AnimTools, false, ("Animi %s missing, loading default", ss.str().c_str() ));

		ss.str("");
		ss << filename << Config::AnimCircleDefaultSize;
		animFrames = GetAnimFrames( ss.str().c_str(), frameCount );

		if ( ! animFrames )
			animFrames = GetAnimFrames( filename ); // this is redundant old stuff

		if ( setSprite )
		{
			( *sprite )= CCSprite::spriteWithSpriteFrame( *animFrames->getFrames()->begin() );
			( *spriteShadow )= CCSprite::spriteWithSpriteFrame( *animFrames->getFrames()->begin() );
			( *sprite )->setScale( scale );
			( *spriteShadow )->setScale( scale );
		}
	}
	else
	{
		if ( setSprite )
		{
			( *sprite )= CCSprite::spriteWithSpriteFrame( *animFrames->getFrames()->begin() );
			( *spriteShadow )= CCSprite::spriteWithSpriteFrame( *animFrames->getFrames()->begin() );
		}
	}

	return animFrames;
}
//----------------------------------------------------------------------------------
void AnimTools::PreloadAdjustedAnimFrames( float radius, const char *filename )
{
	stringstream ss;
	//------------------------------------------
	// Find proper size, otherwise load default
	ss << filename;
	ss << radius;

	if ( ! PreloadAnimFrames( ss.str().c_str() ))
	{
		ss.str("");
		ss << filename << Config::AnimCircleDefaultSize;
		if ( ! PreloadAnimFrames( ss.str().c_str() ))
			PreloadAnimFrames( filename );
	}
}
//----------------------------------------------------------------------------------
bool AnimTools::PreloadAnimFrames( const char *filename )
{
	stringstream frameFileStream;
	string file;
	int count;

	file = GetAnimFile( filename );
	count = 1;

	if ( ! unResourceFileCheckExists( file.c_str() ))
	{
		DS_LOG( "Missing %s", file.c_str() );
		return false;
	}

	CCSpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile( file.c_str() );
	return true;
}
//----------------------------------------------------------------------------------

