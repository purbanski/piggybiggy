#ifndef __ANIMSIMPLEEFFECT_H__
#define __ANIMSIMPLEEFFECT_H__

#include "cocos2d.h"
#include <map>
#include "GameTypes.h"
#include "Blocks/puBlock.h"

USING_NS_CC;
using namespace std;
using namespace GameTypes;


//----------------------------------------------------------
// Tetris Anim Fixer
//----------------------------------------------------------
class puTetrisBase;

class TetrisAnimFixer 
{
public:
	TetrisAnimFixer();
	void FixPosition( CCSprite *sprite, puTetrisBase *block );

private:
	void FixS_0( CCSprite *sprite, puTetrisBase *block );
	void FixS_90( CCSprite *sprite, puTetrisBase *block );

	void FixSM_0( CCSprite *sprite, puTetrisBase *block );
	void FixSM_90( CCSprite *sprite, puTetrisBase *block );

	void FixSquare( CCSprite *sprite, puTetrisBase *block );

	void FixL_0( CCSprite *sprite, puTetrisBase *block );
	void FixL_90( CCSprite *sprite, puTetrisBase *block );
	void FixL_180( CCSprite *sprite, puTetrisBase *block );
	void FixL_270( CCSprite *sprite, puTetrisBase *block );

	void FixLM_0( CCSprite *sprite, puTetrisBase *block );
	void FixLM_90( CCSprite *sprite, puTetrisBase *block );
	void FixLM_180( CCSprite *sprite, puTetrisBase *block );
	void FixLM_270( CCSprite *sprite, puTetrisBase *block );

	void FixT_0( CCSprite *sprite, puTetrisBase *block );
	void FixT_90( CCSprite *sprite, puTetrisBase *block );
	void FixT_180( CCSprite *sprite, puTetrisBase *block );
	void FixT_270( CCSprite *sprite, puTetrisBase *block );

	void FixI_0( CCSprite *sprite, puTetrisBase *block );
	void FixI_90( CCSprite *sprite, puTetrisBase *block );

	CCPoint GetBasedPoint( puBlock *block );

private:
	typedef void ( TetrisAnimFixer::*FixFuncPtr )( CCSprite *sprite, puTetrisBase *block );
	typedef map<TetrisBlockRotation, FixFuncPtr>	FunctionMap;
	typedef map<BlockEnum, FunctionMap>				BlockFixMap;

	BlockFixMap _blocksMap;
	FunctionMap	_blockRotationMap;
};
//----------------------------------------------------------
class AnimFlyData;
class SpriteAndAction
{
public:
	SpriteAndAction();
	SpriteAndAction( CCSprite *sprite, CCAction *action, CCAction *action2, AnimFlyData *flyData1, AnimFlyData *flyData2, AnimFlyData *flyData3 );
	~SpriteAndAction();

	void Retain();
	void Release();
	void StopActions();

	CCSprite	*_sprite;
	CCAction	*_action;
	CCAction	*_action2;
    AnimFlyData *_flyData1;
    AnimFlyData *_flyData2;
    AnimFlyData *_flyData3;
};

//----------------------------------------------------------
// AnimerSimpleEffects
//----------------------------------------------------------
// for simple effect, like 
// glue disappear, fragile destroy etc
//----------------------------------------------------------
class Level;
class AnimerSimpleEffect
{
public:
	static AnimerSimpleEffect* Get();
	static void Destroy();

	~AnimerSimpleEffect();

	void Anim_FragileDestroy_Depricated( CCNode *node );
	void Anim_FragileDestroy( puBlock *block );
	void Anim_TetrisDestroy( puBlock *block );
	SpriteAndAction Anim_FlyFly( Level *level );
	CCSprite* Anim_BombExploded( puBlock *block );

	void Anim_FragileColorDestroy( CCNode *node, const char *color );
	void Anim_BombDestroy( CCNode *node );
	void Anim_GlueDestroy( CCNode *node );
	void Anim_ButtonPulse( CCNode* node );
	void Anim_PiggyReadyToRock( CCNode *node );
    void Anim_ZoomIn( CCSprite *sprite );
    
	void ShowMsg( CCNode *node, const char *message, int zOrder, float dur = 2.0f, bool mute = true );

public:
	AnimerSimpleEffect();

	void Anim_Fly_RandomFly( CCObject *sender, void *data );
	void Anim_Fly_StillFly( CCObject *sender, void *data );

private:
	CCPoint  GetRandomFlyConfig( CCSprite *animSprite );

private:
	static AnimerSimpleEffect*	_sInstance;
	TetrisAnimFixer				_terisFixer;
};
//----------------------------------------------------------



//----------------------------------------------------------
// Anim Fly Data
//----------------------------------------------------------
class AnimFlyData : public CCNode
{
public:
	static AnimFlyData* Create(  CCSprite *sprite );

	float GetDuration() { return _duration; }
	CCSprite *GetSprite() { return _animSprite; }

private:
	AnimFlyData( CCSprite *sprite );

private:
	CCSprite *_animSprite;
	float	  _duration;
};
//----------------------------------------------------------





#endif
