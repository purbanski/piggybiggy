#include <sstream> 
#include "unDebug.h"
#include "Debug/MyDebug.h"
#include "AnimSprites.h"
#include "AnimManager.h"
#include "AnimTools.h"
#include "GameConfig.h"

//-------------------------------------------------------------------------------
// Anim Sprite
//-------------------------------------------------------------------------------
AnimSprite* AnimSprite::Create( puBlock *block, const char *animName, float fps )
{

	AnimSprite *sprite = new AnimSprite();
	if ( sprite && sprite->initWithFile( "Images/Other/none.png" ))
	{
		sprite->autorelease();
		sprite->Construct( block, animName, fps );
		return sprite;
	}
	CC_SAFE_DELETE( sprite );
	return NULL;
}
//-------------------------------------------------------------------------------
AnimSprite::AnimSprite()
{
}
//-------------------------------------------------------------------------------
void AnimSprite::Construct( puBlock *block, const char *animName, float fps )
{
	CCAnimate *animation;
	stringstream ss;

	if ( block->GetShapeType() == GameTypes::eShape_Circle)
	{
		ss << animName;
		ss << block->GetWidth() * RATIO / 2.0f;
	}
	else if ( block->GetShapeType() == GameTypes::eShape_Box)
	{
		ss << animName;
		ss << block->GetWidth() * RATIO;
		ss << "x";
		ss << block->GetHeight() * RATIO;
	}
	else
	{
		unAssertMsg(Animation, false, ("undefined block shape"));
		return;
	}

	animation = AnimTools::Get()->GetAnimation( ss.str().c_str(), 1.0f / fps);
	if ( ! animation )
	{
		DS_LOG_MISSING_PNG( "- %s", ss.str().c_str() );
		D_LOG("Animi %s missing, loading default", ss.str().c_str() );
		unAssertMsg( AnimTools, false, ("Animi %s missing, loading default", ss.str().c_str() ));

		if ( block->GetShapeType() == GameTypes::eShape_Circle)
		{
			ss.str("");
			ss << animName << Config::AnimCircleDefaultSize;
			animation = AnimTools::Get()->GetAnimation( ss.str().c_str(), 1.0f / fps );
		}
		else
		{
			animation = AnimTools::Get()->GetAnimation( animName, 1.0f / fps );
		}

		setScale( block->GetScaleToDefault() );
	}
	else
	{
		DS_LOG_MISSING_PNG( "+ %s", ss.str().c_str() );
	}
	
	CCRepeatForever* action = CCRepeatForever::actionWithAction( animation );
	runAction( action );
}
