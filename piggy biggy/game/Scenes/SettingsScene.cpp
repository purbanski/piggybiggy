#include <list>
#include "unDebug.h"
#include <Box2D/Box2D.h>

#include "MainMenuScene.h"
#include "Levels/LevelList.h"
#include "SoundEngine.h"
#include "Tools.h"
#include "Debug/MyDebug.h"
#include "Transitions.h"
#include "Game.h"
#include "GameGlobals.h"
#include "Skins.h"
#include "LoadingBar.h"
#include "Levels/Pockets.h"
#include "RunLogger/RunLogger.h"
#include "RunLogger/GLogger.h"
#include "SettingsScene.h"

USING_NS_CC;
using namespace std;

//--------------------------------------------------------------------------------------------------
CCScene* SettingsScene::CreateScene()
{
	CCScene *scene = CCScene::node();
	SettingsScene *layer = SettingsScene::node();

	scene->addChild(layer);
	return scene;
}
//--------------------------------------------------------------------------------------------------
void SettingsScene::ShowScene()
{
//	LoadingBar::Get()->AddToScene( &SettingsScene::DoShowScene, NULL );
    DoShowScene(NULL);
}
//--------------------------------------------------------------------------------------------------
void SettingsScene::DoShowScene( void *data )
{
	CCScene* pScene = SceneTransition::transitionWithDuration( CreateScene( ) );
	if (pScene)
	{
		SoundEngine::Get()->PlayEffect( SoundEngine::eSoundChangeScene );
		CCDirector::sharedDirector()->replaceScene( pScene );
	}
}
//--------------------------------------------------------------------------------------------------
SettingsScene::SettingsScene()
{
    gAppDisplayRotateMode = eDisplay_LandscapeRotating;
}
//--------------------------------------------------------------------------------------------------
SettingsScene::~SettingsScene()
{
	removeAllChildrenWithCleanup( true );
}
//--------------------------------------------------------------------------------------------------
bool SettingsScene::init()
{
    GSendView( "Settings scene" );
    
	if ( !CCLayer::init()  )
		return false;

    LanguageMenu *langMenu;
    langMenu = LanguageMenu::Create();
    
    addChild( langMenu );
    
	setPosition( Tools::GetScreenMiddle() );
	return true;
}
//--------------------------------------------------------------------------------------------------
void SettingsScene::Menu_SelectLevel( CCObject* pSender )
{
}
//-----------------------------------------------------------------------------------
CCMenu* SettingsScene::ConstructMainMenu()
{
	return NULL;
}
//-----------------------------------------------------------------------------------
void SettingsScene::Menu_SelectPocket( CCObject *sender )
{
}
//-----------------------------------------------------------------------------------
void SettingsScene::Menu_MainMenu( CCObject *sender )
{
}
//-----------------------------------------------------------------------------------
SettingsScene* SettingsScene::node()
{ 
	SettingsScene *pRet = new SettingsScene();

	if (pRet && pRet->init())
	{
		pRet->autorelease();
		return pRet;
	}
	else
	{
		delete pRet;
		pRet = NULL;
		return NULL;
	} 
}; 
//-----------------------------------------------------------------------------------
