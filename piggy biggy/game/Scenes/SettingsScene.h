#ifndef __SETTINGSSCENE_H__
#define __SETTINGSSCENE_H__

#include <sstream>
#include "cocos2d.h"
#include <Box2D/Box2D.h>
#include "pu/ui/SlidingNode.h"
#include "ScaleScreenLayer.h"
#include "LevelFactory.h"
#include "Levels/LevelList.h"

//------------------------------------------------------------------------------------------

using namespace std;
USING_NS_CC;

//------------------------------------------------------------------------------------------
class SettingsScene : public CCLayer
{
private:
	static void		DoShowScene( void* );

public:
	static void		ShowScene();
	static CCScene*	CreateScene();

	static SettingsScene* node();

	virtual bool init();  
	virtual ~SettingsScene();

private :
	SettingsScene();
	CCMenu* ConstructMainMenu();

    void Refresh();
	void Menu_SelectLevel( CCObject *sender );
	void Menu_MainMenu( CCObject *sender );
	void Menu_SelectPocket( CCObject *sender );

private:
};
//------------------------------------------------------------------------------------------
#endif 
