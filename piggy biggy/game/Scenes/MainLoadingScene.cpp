#include <Box2D/Box2D.h>
//#include <MessageBox.h>
#include "GLES-Render.h"
#include "Game.h"
#include "MainLoadingScene.h"
#include "ComicsSceneLite.h"
#include "IntroScene.h"
#include "Transitions.h"
#include "SoundEngine.h"
#include "Tools.h"
#include "GameConfig.h"
#include "Skins.h"
#include "LoadingBar.h"
#include "Levels/LevelMenu.h"
#include "Components/FXMenuItem.h"
#include "Platform/Lang.h"
#include "RunLogger/RunLogger.h"
#include "GLogger.h"
#include "MainMenuScene.h"

USING_NS_CC;

//--------------------------------------------------------------------------------------------------
// MainLoadingScene
//--------------------------------------------------------------------------------------------------
CCScene* MainLoadingScene::CreateScene()
{
	CCScene *scene = CCScene::node();
	MainLoadingScene *layer = MainLoadingScene::node();

	scene->setScale( Game::Get()->GetScale() );
	scene->addChild(layer);
	return scene;
}
//--------------------------------------------------------------------------------------------------
MainLoadingScene* MainLoadingScene::node()
{
    MainLoadingScene *layer = new MainLoadingScene();
    if ( layer && layer->init() )
    {
        layer->autorelease();
    }
    else
    {
        delete layer;
        layer = NULL;
    }
    
    return layer;
}; 
//--------------------------------------------------------------------------------------------------
MainLoadingScene::MainLoadingScene()
{
    gAppDisplayRotateMode = eDisplay_LandscapeRotating;
    _ending = false;
}
//--------------------------------------------------------------------------------------------------
MainLoadingScene::~MainLoadingScene()
{
	removeAllChildrenWithCleanup( true );
}
//--------------------------------------------------------------------------------------------------
bool MainLoadingScene::init()
{
	if ( !CCLayer::init() )
		return false;
    
    SoundEngine::Get()->StopBackgroundMusic();
    
    // get proper background
    // it doesn't fully work
    // for example for 960x640 report 480x320
    string filename;
    
    CCSize winSize;
    winSize = CCDirector::sharedDirector()->getWinSizeInPixels();
    D_LOG("size %f %f", winSize.width, winSize.height );
    
    float scale;
    float rotate;

    scale = 1.0f;
    rotate = 0.0f;

    filename.append("Images/Other/Logo.png");

    _background = CCSprite::spriteWithFile( filename.c_str() );
    _background->setRotation( rotate );
    _background->setScale( Game::Get()->GetScale() );
    _background->setScale( scale );
    _background->setOpacity( 255 );
    _background->setPosition( Tools::GetScreenMiddle() );
    addChild( _background );
   
    SoundEngine::Get()->LoadSound( SoundEngine::eSoundPageTurn );
    
    setIsTouchEnabled( true );
    
	return true;
}
//--------------------------------------------------------------------------------------------------
bool MainLoadingScene::ccTouchBegan(CCTouch *touch, CCEvent *pEvent)
{
    if ( _ending )
        return true;
    
    _ending = true;
    
    RLOG_S("BLACK_TED","SKIP" );
    GLogEvent("Intro", "Skip (BLACK TED scene)");

    IntroSkip();
    return true;
}
//--------------------------------------------------------------------------------------------------
void MainLoadingScene::ccTouchMoved(CCTouch *touch, CCEvent *pEvent)
{
}
//--------------------------------------------------------------------------------------------------
void MainLoadingScene::ccTouchEnded(CCTouch *touch, CCEvent *pEvent)
{
}
//--------------------------------------------------------------------------------------------------
void MainLoadingScene::registerWithTouchDispatcher()
{
    CCTouchDispatcher::sharedDispatcher()->addTargetedDelegate( this, 1, true );
}
//--------------------------------------------------------------------------------------------------
void MainLoadingScene::onEnterTransitionDidFinish()
{
	CCLayer::onEnterTransitionDidFinish();
    
    _presents = CCSprite::spriteWithFile( Lang::Get()->GetLang( "Images/Other/Presents.png" ));
    _presents->setOpacity( 0 );
    _presents->setPosition( ccp( Tools::GetScreenMiddle().x, Tools::GetScreenMiddle().y - 105.0f ));
    
    addChild( _presents, 100 );
    
    CCActionInterval* anim = (CCActionInterval*)
                (CCSequence::actions(
                        CCDelayTime::actionWithDuration( 0.1f ),
                        CCFadeTo::actionWithDuration( 0.5f, 255 ),
                        CCDelayTime::actionWithDuration( 1.25f ),
                        CCCallFunc::actionWithTarget( this, callfunc_selector( MainLoadingScene::AnimStop )),
                        NULL ));
    
    _presents->runAction( anim );
}
//--------------------------------------------------------------------------------------------------
void MainLoadingScene::AnimStop()
{
    CCTouchDispatcher::sharedDispatcher()->removeDelegate( this );
    _ending = true;
    
    CCScene *scene;
//    bool showIntro;

//    showIntro = Game::Get()->GetGameStatus()->ShouldIntroStart();
//    if ( showIntro )
        scene = IntroScene::CreateScene();
//    else
//        scene = MainMenuScene::CreateScene();
    
    TransitionScene1 *trans = TransitionScene1::transitionWithDuration( scene );
    SoundEngine::Get()->PlayEffect( SoundEngine::eSoundPageTurn );
    CCDirector::sharedDirector()->replaceScene( trans );
}
//---------------------------------------------------------------------------------------------------
void MainLoadingScene::IntroSkip()
{
    CCTouchDispatcher::sharedDispatcher()->removeDelegate( this );
    _ending = true;
    _presents->stopAllActions();
    
    CCScene *scene;
    scene = MainMenuScene::CreateScene();
    
    TransitionScene1 *trans = TransitionScene1::transitionWithDuration( scene );
    SoundEngine::Get()->PlayEffect( SoundEngine::eSoundPageTurn );
    CCDirector::sharedDirector()->replaceScene( trans );    
}

