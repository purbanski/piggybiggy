#include "Game.h"
#include "GLogger.h"
#include "Transitions.h"
#include "MainMenuScene.h"
#include "MarmaladeScene.h"
#include "MainLoadingScene.h"
#include "RunLogger/RunLogger.h"

USING_NS_CC;

//--------------------------------------------------------------------------------------------------
// MarmaladeScene
//--------------------------------------------------------------------------------------------------
CCScene* MarmaladeScene::CreateScene()
{
	CCScene *scene = CCScene::node();
	MarmaladeScene *layer = MarmaladeScene::node();

	scene->setScale( Game::Get()->GetScale() );
	scene->addChild(layer);
	return scene;
}
//--------------------------------------------------------------------------------------------------
MarmaladeScene* MarmaladeScene::node()
{
    MarmaladeScene *layer = new MarmaladeScene();
    if ( layer && layer->init() )
    {
        layer->autorelease();
    }
    else
    {
        delete layer;
        layer = NULL;
    }
    
    return layer;
}; 
//--------------------------------------------------------------------------------------------------
MarmaladeScene::MarmaladeScene()
{
    gAppDisplayRotateMode = eDisplay_LandscapeRotating;
    _ending = false;
}
//--------------------------------------------------------------------------------------------------
MarmaladeScene::~MarmaladeScene()
{
	removeAllChildrenWithCleanup( true );
}
//--------------------------------------------------------------------------------------------------
bool MarmaladeScene::init()
{
	if ( ! CCLayer::init() )
		return false;
    
//    string filename;
//    
//    CCSize winSize;
//    winSize = CCDirector::sharedDirector()->getWinSizeInPixels();
//    D_LOG("size %f %f", winSize.width, winSize.height );
    
    float scale;
    float rotate;

    scale = 1.0f;
    rotate = 0.0f;
  
    string filename;
    filename.append("Images/Other/MarmaladeLogo.png");
    
    _background = CCSprite::spriteWithFile( filename.c_str() );
    _background->setRotation( rotate );
    _background->setScale( Game::Get()->GetScale() );
    _background->setScale( scale );
    _background->setOpacity( 255 );
    _background->setPosition( Tools::GetScreenMiddle() );
    addChild( _background );
   
    SoundEngine::Get()->LoadSound( SoundEngine::eSoundPageTurn );
    
    setIsTouchEnabled( true );
    
	return true;
}
//--------------------------------------------------------------------------------------------------
bool MarmaladeScene::ccTouchBegan(CCTouch *touch, CCEvent *pEvent)
{
    if ( _ending )
        return true;
    
    _ending = true;
    
    RLOG_S("MARMALADE","SKIP" );
    GLogEvent("Intro", "Skip (MARMALADE scene)");

    Skip();
    return true;
}
//--------------------------------------------------------------------------------------------------
void MarmaladeScene::ccTouchMoved(CCTouch *touch, CCEvent *pEvent)
{
}
//--------------------------------------------------------------------------------------------------
void MarmaladeScene::ccTouchEnded(CCTouch *touch, CCEvent *pEvent)
{
}
//--------------------------------------------------------------------------------------------------
void MarmaladeScene::registerWithTouchDispatcher()
{
    CCTouchDispatcher::sharedDispatcher()->addTargetedDelegate( this, 1, true );
}
//--------------------------------------------------------------------------------------------------
void MarmaladeScene::onEnterTransitionDidFinish()
{
    CCLayer::onEnterTransitionDidFinish();
    
    CCActionInterval* delay = (CCActionInterval*)
                (CCSequence::actions(
                        CCDelayTime::actionWithDuration( 1.0f ),
                        CCCallFunc::actionWithTarget( this, callfunc_selector( MarmaladeScene::NextScene )),
                        NULL ));
    
    runAction( delay );
}
//--------------------------------------------------------------------------------------------------
void MarmaladeScene::Skip()
{
    CCTouchDispatcher::sharedDispatcher()->removeDelegate( this );
    _ending = true;
    
    CCScene *scene;
    scene = MainMenuScene::CreateScene();
    
    TransitionScene1 *trans = TransitionScene1::transitionWithDuration( scene );
    SoundEngine::Get()->PlayEffect( SoundEngine::eSoundPageTurn );
    CCDirector::sharedDirector()->replaceScene( trans );    
}
//---------------------------------------------------------------------------------------------------
void MarmaladeScene::NextScene()
{
     CCTouchDispatcher::sharedDispatcher()->removeDelegate( this );
    _ending = true;
    
    CCScene *scene;
    scene = MainLoadingScene::CreateScene();
      
    TransitionScene1 *trans = TransitionScene1::transitionWithDuration( scene );
    SoundEngine::Get()->PlayEffect( SoundEngine::eSoundPageTurn );
    CCDirector::sharedDirector()->replaceScene( trans );

}
//---------------------------------------------------------------------------------------------------

