#ifndef __NEXTLEVELSCENE_H__
#define __NEXTLEVELSCENE_H__


#include "cocos2d.h"
#include <Box2D/Box2D.h>
#include "pu/ui/SlidingNode.h"
#include "ScaleScreenLayer.h"
#include "LevelFactory.h"
#include "Levels/LevelList.h"

//------------------------------------------------------------------------------------------

using namespace std;
USING_NS_CC;

//------------------------------------------------------------------------------------------
class NextLevelScene : public CCLayer
{
private:
   static bool _sLock;

public:
   static void Unlock();
   static void Lock();

public:
	static CCScene*	CreateScene();
	static void       ShowScene();
	static void       DoShowScene( void *);
    
	LAYER_NODE_FUNC( NextLevelScene );
	virtual bool init();  
	virtual ~NextLevelScene();
   void Step( cocos2d::ccTime dt );

private :
	NextLevelScene();

	void Menu_GameMenu( CCObject *sender );
	void AddBackground();

private:
	ScaleScreenLayer	*_scaleScreenLayer;
};
//------------------------------------------------------------------------------------------

#endif
