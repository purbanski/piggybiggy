#ifndef __COMICSSCENE_H__
#define __COMICSSCENE_H__

#include "cocos2d.h"
#include "ScaleScreenLayer.h"
#include "LevelFactory.h"

USING_NS_CC;
using namespace std;

//-------------------------------------------------
// ComicsScene
//-------------------------------------------------
class ComicsScene : public CCLayer
{
public:
    static const float SceneShowDur;
    static const float SceneMoveDur;

	static CCScene* CreateScene();
	virtual bool init();  
	virtual ~ComicsScene();

    virtual bool ccTouchBegan(CCTouch *touch, CCEvent *pEvent);
	virtual void ccTouchMoved(CCTouch *touch, CCEvent *pEvent);
	virtual void ccTouchEnded(CCTouch *touch, CCEvent *pEvent);
	virtual void registerWithTouchDispatcher();
    
	virtual void onEnter();
	virtual void onEnterTransitionDidFinish();
	virtual void onExit();

    void RemoveSprite( CCNode *sender, void *data );
    void ShowMainMenuScene();
    
private:
	ComicsScene();
    CCSprite *GetSceneSprite( int index );

    void SetScene1();
    void SetScene2();
    void SetScene3();
    void SetScene4();
    void SetScene5();
    void SetScene6();
    void SetScene7();
    void ReleaseAll();
    
    CCSprite *_scene1;
    CCSprite *_scene2;
    CCSprite *_scene3;
    CCSprite *_scene4;
    CCSprite *_scene5;
    CCSprite *_scene6;
    CCSprite *_scene7;
    
    //delteme
    //CCSprite *_activeSprite;
    b2Vec2   _startPos;
    b2Vec2   _startTouch;
    
private:
   	CCSprite *_background;
    int       _debugIndex;
    bool      _skipPressed;
};

#endif 
