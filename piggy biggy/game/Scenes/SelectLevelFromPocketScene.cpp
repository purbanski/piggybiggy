#include <list>
#include "unDebug.h"
#include <Box2D/Box2D.h>

#include "MainMenuScene.h"
#include "SelectLevelFromPocketScene.h"
#include "SelectPocketScene.h"
#include "Levels/LevelList.h"
#include "SoundEngine.h"
#include "Tools.h"
#include "Debug/MyDebug.h"
#include "Transitions.h"
#include "Game.h"
#include "GameGlobals.h"
#include "Skins.h"
#include "LoadingBar.h"
#include "Levels/Pockets.h"
#include "RunLogger/RunLogger.h"
#include "RunLogger/GLogger.h"

USING_NS_CC;
using namespace std;

//--------------------------------------------------------------------------------------------------
CCScene* SelectLevelFromPocketScene::CreateScene( GameTypes::PocketType pocketEnum )
{
	CCScene *scene = CCScene::node();
	SelectLevelFromPocketScene *layer = SelectLevelFromPocketScene::node( pocketEnum );

	scene->addChild(layer);
	return scene;
}
//--------------------------------------------------------------------------------------------------
void SelectLevelFromPocketScene::ShowScene( GameTypes::PocketType pocketEnum )
{
    DoShowScene((void*) pocketEnum);
//	LoadingBar::Get()->AddToScene( &SelectLevelFromPocketScene::DoShowScene, (void*) pocketEnum );
}
//--------------------------------------------------------------------------------------------------
void SelectLevelFromPocketScene::DoShowScene( void *data )
{
	GameTypes::PocketType pocketEnum;
	pocketEnum = (GameTypes::PocketType) ((long)data);

	CCScene* pScene = SceneTransition::transitionWithDuration(CreateScene( pocketEnum ));
	if (pScene)
	{
		SoundEngine::Get()->PlayEffect( SoundEngine::eSoundChangeScene );
		CCDirector::sharedDirector()->replaceScene( pScene );
	}
}
//--------------------------------------------------------------------------------------------------
SelectLevelFromPocketScene::SelectLevelFromPocketScene( GameTypes::PocketType pocketEnum )
{
	_pocketEnum = pocketEnum;
    gAppDisplayRotateMode = eDisplay_LandscapeRotating;

    RLOG_SS("SELECT_LEVEL_MENU", "POCKET", pocketEnum );
}
//--------------------------------------------------------------------------------------------------
SelectLevelFromPocketScene::~SelectLevelFromPocketScene()
{
	removeAllChildrenWithCleanup( true );
}
//--------------------------------------------------------------------------------------------------
bool SelectLevelFromPocketScene::init()
{
    GSendView( "Select Level" );
    
	if ( !CCLayer::init()  )
		return false;
	
	int levelCount;
	int slidingScreenCount;

	levelCount = PocketManager::Get()->GetLevelsFromPocket( _pocketEnum ).size();

	slidingScreenCount = levelCount / 6;

	if (( levelCount % 6 ) != 0 )
		slidingScreenCount++;
	
	_slidingNode = SlidingNode::node( slidingScreenCount );
	_slidingNode->addChild( ConstructSlidingMenu(), 10 );
	_slidingNode->SetInitPosition( ccp ( 0.0f, 0.0f ) );
	
	SetIndex();
	addChild( _slidingNode, 10 );
	addChild( ConstructMainMenu(), 15 );

	_scaleScreenLayer = ScaleScreenLayer::node();
	_scaleScreenLayer->setPosition( ccp(0.0f, 0.0f) );
	addChild( _scaleScreenLayer, 50 );

	setPosition( Tools::GetScreenMiddle() );
	
	return true;
}
//--------------------------------------------------------------------------------------------------
void SelectLevelFromPocketScene::Menu_SelectLevel( CCObject* pSender )
{
	CCMenuItem *menuItem;
	long levelEnum;

	menuItem = (CCMenuItem*) pSender;
	levelEnum = (long) menuItem->getUserData();

#ifdef BUILD_EDITOR
	CCDirector::sharedDirector()->popScene();
#endif
	Preloader::Get()->Reset();
	Preloader::Get()->FreeResources();

	Game::Get()->GetGameStatus()->ResetPreviouslyPlayedLevel();
	Game::Get()->CreateAndPlayLevel( (GameTypes::LevelEnum) levelEnum, Game::eLevelToLevel );
}
//-----------------------------------------------------------------------------------
CCNode* SelectLevelFromPocketScene::ConstructSlidingMenu()
{
	CCNode *node;
	node = CCNode::node();

	node->addChild( PocketManager::Get()->GetLevelListNode( _pocketEnum, _slidingNode ), 10 );
	
	CCSprite *pocketBg = PocketManager::Get()->GetSelectPocketBackground( _pocketEnum );
	pocketBg->setPosition( ccp( 0.0f, 0.0f ));
	addChild( pocketBg, 5 );

	return node;
}
//-----------------------------------------------------------------------------------
CCMenu* SelectLevelFromPocketScene::ConstructMainMenu()
{
	CCMenu* menu;
	TrippleSprite triSprite;
	string spriteFile;

	spriteFile.append( "Images/Menus/SelectLevel/MainMenu" );

	switch ( _pocketEnum )
	{
	case GameTypes::ePocketBlue :
		spriteFile.append( "1.png" );
		break;

	case GameTypes::ePocketMoro :
		spriteFile.append( "2.png" );
		break;

	case GameTypes::ePocketRasta :
		spriteFile.append( "3.png" );
		break;

	case GameTypes::ePocketJapan :
		spriteFile.append( "4.png" );
		break;

	case GameTypes::ePocketScience :
		spriteFile.append( "5.png" );
		break;

	default:
		unAssertMsg(Game, false, ("Wrong packet %d, switching to blue "));
		spriteFile.append( "1.png" );
	}
	triSprite._normal	= CCSprite::spriteWithFile( spriteFile.c_str() );
	triSprite._selected = CCSprite::spriteWithFile( spriteFile.c_str() );
	triSprite._disable	= CCSprite::spriteWithFile( spriteFile.c_str() );


	FXMenuItemSpriteWithAnim *menuItem;
	menuItem = FXMenuItemSpriteWithAnim::Create( triSprite, this, menu_selector( SelectLevelFromPocketScene::Menu_SelectPocket ));


	menu = CCMenu::menuWithItems( menuItem, NULL );
	menu->alignItemsHorizontallyWithPadding( 25.0f );
	menu->setPosition( Config::GameSize.width / 2.0f - 55.0f, Config::GameSize.height / 2.0f - 65.0f ); 
	return menu;
}
//-----------------------------------------------------------------------------------
void SelectLevelFromPocketScene::Menu_SelectPocket( CCObject *sender )
{
	Preloader::Get()->FreeResources();
	Preloader::Get()->Reset();

	CCTouchDispatcher::sharedDispatcher()->removeDelegate( this );
	SelectPocketScene::ShowScene();
}
//-----------------------------------------------------------------------------------
void SelectLevelFromPocketScene::Menu_MainMenu( CCObject *sender )
{
#ifdef BUILD_EDITOR
	return;
#endif
	Preloader::Get()->FreeResources();
	Preloader::Get()->Reset();

	CCTouchDispatcher::sharedDispatcher()->removeDelegate( this );
	MainMenuScene::ShowScene();
}
//-----------------------------------------------------------------------------------
SelectLevelFromPocketScene* SelectLevelFromPocketScene::node( GameTypes::PocketType pocketEnum )
{ 
	SelectLevelFromPocketScene *pRet = new SelectLevelFromPocketScene( pocketEnum );

	if (pRet && pRet->init())
	{
		pRet->autorelease();
		return pRet;
	}
	else
	{
		delete pRet;
		pRet = NULL;
		return NULL;
	} 
}; 
//-----------------------------------------------------------------------------------
void SelectLevelFromPocketScene::SetIndex()
{
	GameTypes::LevelEnum level;
	level = Game::Get()->GetGameStatus()->GetCurrentLevel();

	if ( PocketManager::Get()->IsLevelInPocket( level, _pocketEnum ))
	{
		int count;
		PocketManager::PocketLevels levels;
		PocketManager::PocketLevels::iterator it;

		count = 0;
		levels = PocketManager::Get()->GetLevelsFromPocket( _pocketEnum );

		for ( it = levels.begin(); it != levels.end(); it++ )
		{
			if ( *it == level )
			{
				_slidingNode->SetIndex( count / 6 );
				return;
			}
			count++;
		}
	}
	
	_slidingNode->SetIndex( 0 );
}
//-----------------------------------------------------------------------------------
