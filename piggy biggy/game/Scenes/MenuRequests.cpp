#include "MenuRequests.h"
#include "Debug/MyDebug.h"
#include "Components/FXMenuItem.h"
#include "Components/TrippleSprite.h"
#include "Promotion/FBRequestMenu.h"
#include "Tools.h"
#include "MessageBox.h"
#include "Platform/Lang.h"
#include "RunLogger/RunLogger.h"
//---------------------------------------------------------------------
MenuRequests* MenuRequests::Create()
{
    MenuRequests *node;
    node = new MenuRequests();
    if ( node )
    {
        node->autorelease();
        node->Init();
        return node;
    }
    return NULL;
}
//---------------------------------------------------------------------
MenuRequests::MenuRequests()
{
    _isVisible = false;
    _state = eState_Idle;
}
//---------------------------------------------------------------------
MenuRequests::~MenuRequests()
{
    
}
//---------------------------------------------------------------------
void MenuRequests::Init()
{
    TrippleSprite tSprite;
    tSprite = TrippleSprite("Images/Menus/MainMenu/btChallange.png");
    
    _showRequestsItem = FXMenuItemSpriteWithAnim::Create( &tSprite, this, menu_selector( MenuRequests::Menu_ShowRequests ));
    _showRequestsItem->setPosition(CCPointZero);
    _menu = CCMenu::menuWithItem( _showRequestsItem );
    
//    _showRequestsItem->setIsEnabled( false );
    _menu->setPosition( CCPoint( 0.0f, 0.0f ));
//    _menu->setOpacity( 0 );
    addChild( _menu, 10 );
    
    //    StartFBChecking();
}
//---------------------------------------------------------------------
void MenuRequests::FBRequestCompleted( FBTool::Request requestType, void *data )
{
    if ( _state == eState_Exiting )
        return;
    
    _fbRequestMap = *((FBRequestsMap*)data);
    _state = eState_Idle;
    
    if ( ! _fbRequestMap.size() )
        return;
    
    ShowIcon();
}
//---------------------------------------------------------------------
void MenuRequests::FBRequestError( int error )
{
    _state = eState_Idle;
}
//---------------------------------------------------------------------
void MenuRequests::ShowIcon()
{
    float shortDur;
    float longDur;
    
    shortDur = 0.35f;
    longDur = 1.0f;
  
    _menu->setScale( 0.0001f );
    _menu->setOpacity( 0 );
    
    _menu->setAnchorPoint( CCPointZero );
    
    CCFiniteTimeAction *rSeq = (CCFiniteTimeAction*) CCSequence::actions(
                                                     CCScaleTo::actionWithDuration(longDur, 1.2f ),
                                                     CCScaleTo::actionWithDuration(shortDur, 1.0f ),
                                                     NULL
                                                     );
    
    CCAction *seq = (CCAction*) CCSequence::actions(
                                                    CCFadeTo::actionWithDuration( longDur, 255),
                                                    CCDelayTime::actionWithDuration(shortDur),                                                    NULL );
    

    
    _showRequestsItem->setIsEnabled( true );
    _menu->runAction( seq );
    _menu->runAction( rSeq );
    _isVisible = true;
}
//---------------------------------------------------------------------
void MenuRequests::HideIcon()
{
   float shortDur;
   float longDur;
    
    shortDur = 0.35f;
    longDur = 1.0f;
  
    _menu->setAnchorPoint( CCPointZero );
    
    CCFiniteTimeAction *rSeq = (CCFiniteTimeAction*) CCSequence::actions(
                                                     CCScaleTo::actionWithDuration(shortDur, 1.2f ),
                                                     CCScaleTo::actionWithDuration(longDur, 0.001f ),
                                                     NULL
                                                     );
    
    CCAction *seq = (CCAction*) CCSequence::actions(
                                                    CCDelayTime::actionWithDuration(shortDur),
                                                    CCFadeTo::actionWithDuration( longDur, 0),
                                                    NULL );
    
    _showRequestsItem->setIsEnabled( false );
    _menu->runAction( rSeq );
    _menu->runAction( seq );
    _isVisible = false;
}
//---------------------------------------------------------------------
void MenuRequests::Menu_ShowRequests( CCObject *sender )
{
    if ( FBTool::Get()->IsLogged() )
    {
        FBRequestMenu_MainMenu *menu;
        menu = FBRequestMenu_MainMenu::Create();
        menu->setPosition( CCPointZero );
        getParent()->addChild( menu, 200 );
    }
    else
    {
        RLOG_SS("MAIN_MENU", "CHALLENGE", "FB_LOGIN_REQUEST" );
        if ( MessageBox::CreateYesNoBox( LocalString( "MsgBoxLoginToFacebookTitle" ), LocalString( "MsgBoxLoginToFacebook" )))
        {
            RLOG_SSS("MAIN_MENU", "CHALLENGE", "FB_LOGIN_REQUEST", "YES" );
            FBTool::Get()->Login();
        }
        else
        {
            RLOG_SSS("MAIN_MENU", "CHALLENGE", "FB_LOGIN_REQUEST", "NO" );
        }
    }
}
//---------------------------------------------------------------------
void MenuRequests::Restart()
{
    if ( FBTool::Get()->IsLogged() && _state == eState_Idle )
    {
        FBTool::Get()->Request_ReadAll( this );
        _state = eState_FBProcessing;
    }
}
//---------------------------------------------------------------------
void MenuRequests::StartFBChecking()
{
    CCActionInterval *seq;
    CCRepeatForever *repeat;
    
    seq = (CCActionInterval *)
        CCSequence::actions(
                            CCDelayTime::actionWithDuration( 1.25f ),
                            CCCallFunc::actionWithTarget( this, callfunc_selector( MenuRequests::Step )),
                            NULL );
    
    repeat = CCRepeatForever::actionWithAction( seq );
    runAction( repeat );
}
//---------------------------------------------------------------------
void MenuRequests::Step()
{
    if ( _state != eState_Idle )
        return;
    
    if ( _isVisible && ! FBTool::Get()->IsLogged() )
    {
        HideIcon();
        // remove me
    }
    else if ( ! _isVisible && FBTool::Get()->IsLogged() )
    {
        ShowIcon();
//        Restart();
        // show me
    }
}
//---------------------------------------------------------------------
void MenuRequests::CleanUp()
{
    _menu->stopAllActions();
    FBTool::Get()->CancelRequestForListener( this );
    _state = eState_Exiting;
}