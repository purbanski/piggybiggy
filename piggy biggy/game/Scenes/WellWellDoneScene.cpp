#include <list>
#include <Box2D/Box2D.h>

#include "WellWellDoneScene.h"
#include "Levels/LevelList.h"
#include "SoundEngine.h"
#include "Tools.h"
#include "Debug/MyDebug.h"
#include "Transitions.h"
#include "Game.h"
#include "MainMenuScene.h"
#include "LoadingBar.h"
#include "RunLogger/GLogger.h"
#include "RunLogger/RunLogger.h"

USING_NS_CC;
using namespace std;


//--------------------------------------------------------------------------------------------------
CCScene* WellWellDoneScene::CreateScene()
{
	CCScene *scene = CCScene::node();
	WellWellDoneScene *layer = WellWellDoneScene::node();
	layer->setPosition( Tools::GetScreenMiddle() );
	scene->addChild(layer);
	return scene;
}
//--------------------------------------------------------------------------------------------------
void WellWellDoneScene::ShowScene()
{
	LoadingBar::Get()->AddToScene( &WellWellDoneScene::DoShowScene, NULL );
}
//--------------------------------------------------------------------------------------------------
void WellWellDoneScene::DoShowScene( void * )
{
	CCScene* pScene = LevelToLevelTransition::transitionWithDuration( 0.8f, CreateScene() ); 
	if (pScene)
	{
		SoundEngine::Get()->PlayEffect( SoundEngine::eSoundChangeScene );
		CCDirector::sharedDirector()->replaceScene( pScene );
	}
}
//--------------------------------------------------------------------------------------------------
WellWellDoneScene::WellWellDoneScene()
{
    gAppDisplayRotateMode = eDisplay_LandscapeRotating;
}
//--------------------------------------------------------------------------------------------------
WellWellDoneScene::~WellWellDoneScene()
{
	removeAllChildrenWithCleanup( true );
}
//--------------------------------------------------------------------------------------------------
bool WellWellDoneScene::init()
{
    GSendView("Well Done");
    RLOG("WELL_DONE" );
    
	if ( !CCLayer::init()  )
		return false;


	//-----------------
	// emitter
	ScaleScreenLayer* scaleScreenLayer = ScaleScreenLayer::node();
	scaleScreenLayer->setPosition( ccp( 0.0f, 0.0f ));
	addChild( scaleScreenLayer, 255 );


	//-----------------
	// emitter
	CCParticleSystemQuad *emitter = new CCParticleSystemQuad();
	emitter->initWithFile(  Skins::GetSkinName( "Images/Particles/FinalScreen.xml" ));
	emitter->autorelease();
	emitter->setPosition( 20, -50 );
	addChild( emitter, 10 );


	//-----------------
	// back button
	CCSprite *backSprite			= CCSprite::spriteWithFile( "Images/FinishGameScene/backBt.png" );
	CCSprite *backSpriteSelected	= CCSprite::spriteWithFile( "Images/FinishGameScene/backBt.png" );
	CCSprite *backSpriteDisabled	= CCSprite::spriteWithFile( "Images/FinishGameScene/backBt.png" );
	
	Tools::RunButtonAnim( backSpriteSelected );
	FXMenuItemSprite *backButton	= FXMenuItemSprite::itemFromNormalSprite( 
			backSprite, 
			backSpriteSelected, 
			backSpriteDisabled,
			this, 
			menu_selector( WellWellDoneScene::Menu_GameMenu ) );


	//-----------------
	// menu button
	CCMenu *menu = CCMenu::menuWithItems( backButton, NULL);
	menu->alignItemsHorizontally();
	menu->setPosition( CCPoint( Config::GameSize.width / 2.0f - 65.0f, Config::GameSize.height / 2.0f - 65.0f ));
	addChild( menu, 60 );


	//-----------------
	// Background
	AddBackground();


	//-----------------
	// Add piggy
	CCSprite* piggy = CCSprite::spriteWithFile( "Images/FinishGameScene/piggy.png" );
	piggy->setPosition( ccp( 0.0f, 85.0f ));
	addChild( piggy, 50 );


	//-----------------
	// Add coins
	CCSprite* coin = CCSprite::spriteWithFile( "Images/FinishGameScene/coins.png" );
	coin->setPosition( ccp( 30.0f, -75.0f ));
	addChild( coin, 40 );


	//-----------------
	// Add ray1
	CCSprite* ray1 = CCSprite::spriteWithFile( "Images/FinishGameScene/rayLight1.png" );
	ray1->setPosition( ccp( 0.0f, Config::GameSize.height / 2.0f + 50.0f ));
	ray1->setAnchorPoint( ccp( 0.5f, 1.0f ));

	CCRotateBy *rotateAction = CCRotateBy::actionWithDuration( 3.5f, 10.0);
	CCRepeatForever *repeat = CCRepeatForever::actionWithAction(
		(CCActionInterval *)
			CCSequence::actions( 
				rotateAction,
				rotateAction->reverse(),
				rotateAction->reverse(),
				rotateAction,
				NULL ));
	ray1->runAction( repeat );
	addChild( ray1, 45 );



	//-----------------
	// Add ray2
	CCSprite* ray2 = CCSprite::spriteWithFile( "Images/FinishGameScene/rayLight1.png" );
	ray2->setPosition( ccp( 0.0f, Config::GameSize.height / 2.0f + 50.0f ));
	ray2->setAnchorPoint( ccp( 0.5f, 1.0f ));

	CCFadeTo *fadeOut = CCFadeTo::actionWithDuration( 2.0f, 100 );
	CCFadeTo *fadeIn = CCFadeTo::actionWithDuration( 2.0f, 255 );

	CCRepeatForever *fadeRepeat = CCRepeatForever::actionWithAction(
		(CCActionInterval *)
			CCSequence::actions( 
				fadeOut,
				CCDelayTime::actionWithDuration( 0.1f ),
				fadeIn,
				CCDelayTime::actionWithDuration( 0.1f ),
				NULL 
			));

	ray2->runAction( fadeRepeat);
	addChild( ray2, 43 );

	return true;
}
//--------------------------------------------------------------------------------------------------
void WellWellDoneScene::AddBackground()
{
	CCSprite* pSprite = CCSprite::spriteWithFile( "Images/FinishGameScene/background.jpg" );
	pSprite->setPosition( ccp( 0.0f, 0.0f ));

	addChild(pSprite, 0);
}
//-----------------------------------------------------------------------------------
void WellWellDoneScene::Menu_GameMenu( CCObject *sender )
{
	MainMenuScene::ShowScene();
}
//-----------------------------------------------------------------------------------

