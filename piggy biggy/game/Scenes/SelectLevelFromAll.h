#ifndef __SELECTLEVELFROMALL_H__
#define __SELECTLEVELFROMALL_H__

#include <sstream>
#include "cocos2d.h"
#include <Box2D/Box2D.h>
#include "pu/ui/SlidingNode.h"
#include "ScaleScreenLayer.h"
#include "LevelFactory.h"
#include "Levels/LevelList.h"

//------------------------------------------------------------------------------------------

using namespace std;
USING_NS_CC;

//------------------------------------------------------------------------------------------
class SelectLevelFromAll : public CCLayer
{
private:
	static void		DoShowScene( void* );

public:
	typedef enum
	{
		eSelectLevel_FromAll = 1,
		eSelectLevel_FromList,
		eSelectLevel_FromPocket,
		eSelectLevel_FromUndone,
	} SelectLevel_SelectType;

	static void		ShowScene( SelectLevel_SelectType sceneType );
	static CCScene*	CreateScene( SelectLevel_SelectType sceneType );
	static SelectLevelFromAll* node( SelectLevel_SelectType sceneType );

	virtual bool init();  
	virtual ~SelectLevelFromAll();

private :
	SelectLevelFromAll( SelectLevel_SelectType sceneType );
	CCNode* ConstructSlidingMenu( SelectLevel_SelectType sceneType );

	CCNode* ConstructSlidingMenu_List();
	CCNode* ConstructSlidingMenu_All();
	CCNode* ConstructSlidingMenu_Undone();

	CCMenu* ConstructMainMenu();

	void Menu_SelectLevel( CCObject *sender );
	void Menu_SelectLevelFromList( CCObject *sender );
	void Menu_SelectLevelFromAll( CCObject *sender );
	void Menu_SelectPocket( CCObject *sender );
	void Menu_SelectUndone( CCObject *sender );

	void AddBackground();
	void SetIndex( GameTypes::LevelEnum level );

	CCMenuItem*	GetMenuItem( GameTypes::LevelEnum level );

private:
	typedef list<LevelList *>	LevelListList;

	SlidingNode			*_slidingNode;
	ScaleScreenLayer	*_scaleScreenLayer;
	LevelListList		_levelLists;
};
//------------------------------------------------------------------------------------------
#endif 
