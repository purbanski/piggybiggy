#include "NextLevelScene.h"

#include <list>
#include <Box2D/Box2D.h>

#include "NextLevelScene.h"
#include "Levels/LevelList.h"
#include "SoundEngine.h"
#include "Tools.h"
#include "Debug/MyDebug.h"
#include "Transitions.h"
#include "Game.h"
#include "MainMenuScene.h"
#include "LoadingBar.h"
#include "Preloader.h"
#include "RunLogger/GLogger.h"
#include "Platform/Lang.h"

USING_NS_CC;
using namespace std;

bool NextLevelScene::_sLock = true;
//--------------------------------------------------------------------------------------------------
void NextLevelScene::Unlock()
{
   _sLock = false;
}
//--------------------------------------------------------------------------------------------------
void NextLevelScene::Lock()
{
   _sLock = true;
}
//--------------------------------------------------------------------------------------------------
CCScene* NextLevelScene::CreateScene()
{
	CCScene *scene = CCScene::node();
	NextLevelScene *layer = NextLevelScene::node();
	layer->setPosition( Tools::GetScreenMiddle() );
	scene->addChild(layer);
	return scene;
}
//--------------------------------------------------------------------------------------------------
void NextLevelScene::ShowScene()
{
	LoadingBar::Get()->AddToScene( &NextLevelScene::DoShowScene, NULL );
}
//--------------------------------------------------------------------------------------------------
void NextLevelScene::DoShowScene( void * )
{
	CCScene* pScene = LevelToLevelTransition::transitionWithDuration( 0.8f, CreateScene() ); 
	if (pScene)
	{
		SoundEngine::Get()->PlayEffect( SoundEngine::eSoundChangeScene );
		CCDirector::sharedDirector()->replaceScene( pScene );
	}
}
//--------------------------------------------------------------------------------------------------
NextLevelScene::NextLevelScene()
{
    gAppDisplayRotateMode = eDisplay_LandscapeRotating;
   Lock();
}
//--------------------------------------------------------------------------------------------------
NextLevelScene::~NextLevelScene()
{
	removeAllChildrenWithCleanup( true );
}
//--------------------------------------------------------------------------------------------------
bool NextLevelScene::init()
{
	if ( !CCLayer::init()  )
		return false;

	//-----------------
	// Background
	AddBackground();

   schedule( schedule_selector( NextLevelScene::Step ));
    
	return true;
}
//--------------------------------------------------------------------------------------------------
void NextLevelScene::Step( cocos2d::ccTime dt )
{
    if ( ! _sLock )
    {
        Preloader::Get()->FreeResources();
        unschedule( schedule_selector( NextLevelScene::Step ));
        Game::Get()->CreateAndPlayLevel( Game::Get()->GetGameStatus()->GetCurrentLevel(), Game::eLevelToLevel, false );
    }
}
//--------------------------------------------------------------------------------------------------
void NextLevelScene::AddBackground()
{
    CCNode *node;
    node = LoadingSprite::GetL();
	node->setPosition( ccp( 0.0f, 0.0f ));
	addChild( node, 10 );
    
    CCSprite *background = CCSprite::spriteWithFile( Skins::GetSkinName( "Images/Levels/Backgrounds/horizontal.jpg") );
    background->setPosition( CCPointZero );
    addChild( background, 0 );
}
//-----------------------------------------------------------------------------------
void NextLevelScene::Menu_GameMenu( CCObject *sender )
{
	MainMenuScene::ShowScene();
}
//-----------------------------------------------------------------------------------


