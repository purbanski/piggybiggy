#ifndef __MENUREQUESTS_H__
#define __MENUREQUESTS_H__
//--------------------------------------------------------------
#include <cocos2d.h>
#include "Facebook/FBTool.h"
#include "Promotion/FBRequestsMap.h"
USING_NS_CC;
//--------------------------------------------------------------
class MenuRequests : public CCNode, public FBToolListener
{
public:
    static MenuRequests* Create();
    ~MenuRequests();
    
    virtual void FBRequestCompleted( FBTool::Request requestType, void *data );
    virtual void FBRequestError( int error );
    
    void Restart();
    void CleanUp();
    
private:
    MenuRequests();
    void Init();
    void ShowIcon();
    void HideIcon();
    
    void Menu_ShowRequests(CCObject *sender );
    void StartFBChecking();
    void Step();
    
private:
    FBRequestsMap _fbRequestMap;
    CCMenu      *_menu;
    CCMenuItem  *_showRequestsItem;
    bool        _isVisible;
    
    typedef enum
    {
        eState_Idle = 0,
        eState_FBProcessing,
        eState_Exiting
    } State;
    
    State   _state;
};

#endif

