#ifndef __WELLWELLDONESCENE_H__
#define __WELLWELLDONESCENE_H__

#include "cocos2d.h"
#include <Box2D/Box2D.h>
#include "pu/ui/SlidingNode.h"
#include "ScaleScreenLayer.h"
#include "LevelFactory.h"
#include "Levels/LevelList.h"

//------------------------------------------------------------------------------------------

using namespace std;
USING_NS_CC;

//------------------------------------------------------------------------------------------
class WellWellDoneScene : public CCLayer
{
private:
	static void		DoShowScene( void *);

public:
	static void		ShowScene();
	static CCScene*	CreateScene();

	LAYER_NODE_FUNC( WellWellDoneScene );
	virtual bool init();  
	virtual ~WellWellDoneScene();

private :
	WellWellDoneScene();

	void Menu_GameMenu( CCObject *sender );
	void AddBackground();

private:
	ScaleScreenLayer	*_scaleScreenLayer;
};
//------------------------------------------------------------------------------------------
#endif 
