#if !defined (BUILD_EDITOR)  && defined (BUILD_TEST)

#ifndef __TESTVIEWSCENE_H__
#define __TESTVIEWSCENE_H__

#include "cocos2d.h"
#include <Box2D/Box2D.h>
//------------------------------------------------------------------------------------------
using namespace std;
USING_NS_CC;
//------------------------------------------------------------------------------------------
class TestViewScene : public CCLayer
{
public:
	static void		ShowScene();
	static CCScene*	CreateScene();

	LAYER_NODE_FUNC( TestViewScene );
	virtual bool init();  
	virtual ~TestViewScene();
	
	virtual bool ccTouchBegan(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchMoved(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchEnded(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchCancelled(CCTouch *pTouch, CCEvent *pEvent);

	virtual void registerWithTouchDispatcher();
	virtual void onExit();

	void Menu_MainMenu( CCObject *sender );
	void Menu_ShowLog( CCObject *sender );

private :
	TestViewScene();
	void DisplayFile( const char *filename );

private:
	CCLabelTTF	*_displayLabel;
	b2Vec2		_firstTouch;
	CCPoint		_labelStartPos;
};
//------------------------------------------------------------------------------------------
#endif 

#endif
