#include "Transitions.h"
#include "Game.h"


//----------------------------------------
// SceneTransition
//----------------------------------------
SceneTransition* SceneTransition::transitionWithDuration( CCScene *scene )
{
	SceneTransition * pTransition = new SceneTransition();
	pTransition->initWithDuration(0.8f, scene);
	pTransition->autorelease();
	pTransition->m_pOutScene->setScale(Game::Get()->GetScale());
	pTransition->m_pInScene->setScale(Game::Get()->GetScale());
	return pTransition;
}
//----------------------------------------
void SceneTransition::onExit()
{
	m_pInScene->setScale( Game::Get()->GetScale() );

	CCNode *level;
	level = (CCNode *)m_pOutScene->getUserData();
	if ( level )
        level->removeFromParentAndCleanup( true );
}


//----------------------------------------
// LevelToLevelTransition
//----------------------------------------
LevelToLevelTransition* LevelToLevelTransition::transitionWithDuration( ccTime duration, CCScene *scene )
{
	LevelToLevelTransition * pTransition = new LevelToLevelTransition();
	pTransition->initWithDuration( duration, scene  );
	pTransition->autorelease();
	pTransition->m_pOutScene->setScale(  Game::Get()->GetScale() );
	pTransition->m_pInScene->setScale(  Game::Get()->GetScale() );
	return pTransition;
}
//----------------------------------------
void LevelToLevelTransition::onExit()
{
	m_pInScene->setScale( Game::Get()->GetScale() );

	CCNode *level;
	level = (CCNode *)m_pOutScene->getUserData();
	if ( level )
        level->removeFromParentAndCleanup( true );
    
//    CCTransitionFade::onExit();
}



//----------------------------------------
// LevelCrossFadeTransition
//----------------------------------------
LevelCrossFadeTransition* LevelCrossFadeTransition::transitionWithDuration( ccTime duration, CCScene *scene )
{

	LevelCrossFadeTransition * pTransition = new LevelCrossFadeTransition();
	pTransition->initWithDuration( 0.8f, scene );
	pTransition->autorelease();
	pTransition->m_pOutScene->setScale(  Game::Get()->GetScale() );
	pTransition->m_pInScene->setScale(  Game::Get()->GetScale() );
	return pTransition;
}
//----------------------------------------
void LevelCrossFadeTransition::onExit()
{
	m_pInScene->setScale( Game::Get()->GetScale() );

	CCNode *level;
	level = (CCNode *)m_pOutScene->getUserData();
	if ( level && level->getParent() )
		level->removeFromParentAndCleanup( true );
    
//    CCTransitionCrossFade::onExit();
}


//----------------------------------------
// LevelRestartTransition
//----------------------------------------
LevelRestartTransition* LevelRestartTransition::transitionWithDuration( ccTime duration, CCScene *scene, const ccColor3B& color )
{
	LevelRestartTransition * pTransition = new LevelRestartTransition();
	pTransition->initWithDuration( duration, scene, ccWHITE );
	pTransition->autorelease();
	pTransition->m_pOutScene->setScale(  Game::Get()->GetScale() );
	pTransition->m_pInScene->setScale(  Game::Get()->GetScale() );
	return pTransition;
}
//----------------------------------------
void LevelRestartTransition::onExit()
{
	m_pInScene->setScale( Game::Get()->GetScale() );

	Level *level;
	level = (Level *)m_pOutScene->getUserData();
	if ( level )
        level->removeFromParentAndCleanup( true );

//    CCTransitionFade::onExit();
}


//----------------------------------------
//
//----------------------------------------
TransitionScene1* TransitionScene1::transitionWithDuration( CCScene *scene )
{
	TransitionScene1 * pTransition = new TransitionScene1();
	pTransition->initWithDuration( Config::PageTurnDur, scene, false );
	pTransition->autorelease();
	pTransition->m_pOutScene->setScale(  Game::Get()->GetScale() );
	pTransition->m_pInScene->setScale(  Game::Get()->GetScale() );
	return pTransition;
}
//----------------------------------------
void TransitionScene1::onExit()
{
	m_pInScene->setScale( Game::Get()->GetScale() );
    m_pInScene->onEnterTransitionDidFinish();
}

