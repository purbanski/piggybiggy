#ifndef __BUYSCENE_H__
#define __BUYSCENE_H__

#include "cocos2d.h"
#include <deque>
#include "ScaleScreenLayer.h"
#include "LevelFactory.h"
#include "Billing/PriceGetterListener.h"

USING_NS_CC;
using namespace std;


//-------------------------------------------------
// LevelShotsVideo
//-------------------------------------------------
class LevelShotsVideo : public CCNode 
{
public:
	static LevelShotsVideo* Create();
	static int32 VideoStopped( void* systemData, void* userData );

	~LevelShotsVideo();

protected:
	LevelShotsVideo();

};


//-------------------------------------------------
// BillingProcessingAnim
//-------------------------------------------------
class BillingProcessingAnim : public CCNode 
{
public:
	static BillingProcessingAnim* Create();
	~BillingProcessingAnim();
	void StartAnim();
	void StopAnim();
	virtual void SetOpacity( int opacity );
	void Step( ccTime dTime );

private:
	void Init();
	
	BillingProcessingAnim();

	float	_stepCounter;
	bool	_dot1Started;
	bool	_dot2Started;
	bool	_dot3Started;

	CCSprite *_piggy;
	CCSprite *_coin;
	CCSprite *_dot1;
	CCSprite *_dot2;
	CCSprite *_dot3;
	CCSprite *_background;
};


//-------------------------------------------------
// LevelShotsAnim
//-------------------------------------------------
class LevelShotsAnim : public CCNode 
{
public:
	static LevelShotsAnim* Create();
	~LevelShotsAnim();

	void Init( );
	void Next();
	CCSprite* GetLevelShot();
	void Step( ccTime dt );
	
private:
	LevelShotsAnim();
	void SwapLevelShots();

private:
	typedef deque<CCSprite*> SpriteContainerType;

	SpriteContainerType	_spriteContainer;

	unsigned int	_shotIndex;
	float			_deltaTime;
};


//-------------------------------------------------
// BuyScene
//-------------------------------------------------
class BuyScene : public CCLayer, public PriceGetterListener
{
private:
	static void		DoShowScene( void *);
	static void		BillingDoneCallback( void *data );

public:
	static void		ShowScene();
	static CCScene* CreateScene();

	LAYER_NODE_FUNC( BuyScene );
	virtual bool init();  
	virtual ~BuyScene();
    
    virtual void PriceRequestCompleted( void *data, unsigned int len );
    virtual void PriceRequestError();

	void Menu_MainMenu(CCObject* pSender);
	void Menu_Buy(CCObject* pSender);

private:
	BuyScene();
	void StartProcessingAnim();
	void FadeOutProcessingAnim();
	void SetCameraNormal();
	void SetCameraFocused();

private:
	ScaleScreenLayer *_scaleScreenLayer;
	
	CCSprite *_camerasLight;
	CCSprite *_camerasLightFocused;

	BillingProcessingAnim	*_processingAnim;
	bool	_processing;
    bool    _gettingPrice;

};

#endif 
