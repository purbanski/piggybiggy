#include "IntroScene.h"
#include "Game.h"
#include "ComicsScene.h"
#include "MainMenuScene.h"
#include "ComicsSceneLite.h"
#include "Platform/ToolBox.h"
#include "Transitions.h"
#include "RunLogger/GLogger.h"
#include "Platform/Lang.h"
#include "RunLogger/RunLogger.h"

USING_NS_CC;

//--------------------------------------------------------------------------------------------------
// IntroScene
//--------------------------------------------------------------------------------------------------
CCScene* IntroScene::CreateScene()
{
	CCScene *scene = CCScene::node();
	IntroScene *layer = IntroScene::node();

    scene->setScale( Game::Get()->GetScale() );
	scene->addChild(layer);
	return scene;
}
//--------------------------------------------------------------------------------------------------
IntroScene::IntroScene()
{
    gAppDisplayRotateMode = eDisplay_LandscapeRotating;
    _ending = false;
}
//--------------------------------------------------------------------------------------------------
IntroScene::~IntroScene()
{
	CCTouchDispatcher::sharedDispatcher()->removeDelegate( this ); 
	removeAllChildrenWithCleanup( true );
}
//--------------------------------------------------------------------------------------------------
bool IntroScene::init()
{
	if ( !CCLayer::init() )
		return false;

	if ( Tools::GetScreenSize().width != Config::GameSize.width  || Tools::GetScreenSize().height != Config::GameSize.height )
	{
		CCLayer* scaleScreenLayer = ScaleScreenLayer::node();
		scaleScreenLayer->setPosition( Tools::GetScreenMiddle() );
		addChild( scaleScreenLayer, 250 );
	}


	_background = CCSprite::spriteWithFile( "Images/Other/LoadingInit.jpg" );
	_background->setPosition( Tools::GetScreenMiddle() );
    
	addChild( _background, 6 );
    
    SoundEngine::Get()->LoadSound( SoundEngine::eSoundPageTurn );
    setIsTouchEnabled( true );
    return true;
}
//--------------------------------------------------------------------------------------------------
void IntroScene::AnimStop()
{
    _ending = true;
    CCTouchDispatcher::sharedDispatcher()->removeDelegate( this );

    CCScene *scene;
    scene = ComicsScene::CreateScene();
    
    TransitionScene1 *trans = TransitionScene1::transitionWithDuration( scene );
    SoundEngine::Get()->PlayEffect( SoundEngine::eSoundPageTurn );
    CCDirector::sharedDirector()->replaceScene( trans );
}
//--------------------------------------------------------------------------------------------------
void IntroScene::onEnter()
{
	CCLayer::onEnter();
}
//--------------------------------------------------------------------------------------------------
void IntroScene::onEnterTransitionDidFinish()
{
    CCLayer::onEnterTransitionDidFinish();
    CCActionInterval* anim = (CCActionInterval*)
                ( CCSequence::actions(
                        CCDelayTime::actionWithDuration( 0.11f ),
                        CCCallFunc::actionWithTarget( this, callfunc_selector( IntroScene::AnimStop )),
                        NULL ));
    
    _background->runAction( anim );
}
//--------------------------------------------------------------------------------------------------
void IntroScene::onExit()
{
	CCLayer::onExit();
}
//--------------------------------------------------------------------------------------------------
bool IntroScene::ccTouchBegan(CCTouch *touch, CCEvent *pEvent)
{
    if ( _ending )
        return true;
    
    _ending = true;
    
    RLOG_S("PIGGY_TITLE", "SKIP");
    GLogEvent("Intro", "Skip (piggy title)");
    IntroSkip();
    
    return true;
}
//--------------------------------------------------------------------------------------------------
void IntroScene::ccTouchMoved(CCTouch *touch, CCEvent *pEvent)
{
}
//--------------------------------------------------------------------------------------------------
void IntroScene::ccTouchEnded(CCTouch *touch, CCEvent *pEvent)
{
}
//--------------------------------------------------------------------------------------------------
void IntroScene::registerWithTouchDispatcher()
{
    CCTouchDispatcher::sharedDispatcher()->addTargetedDelegate( this, 1, true );
}
//--------------------------------------------------------------------------------------------------
void IntroScene::IntroSkip()
{
    _ending = true;
    CCTouchDispatcher::sharedDispatcher()->removeDelegate( this );

    _background->stopAllActions();
    
    CCScene *scene;
    scene = MainMenuScene::CreateScene();
    
    TransitionScene1 *trans = TransitionScene1::transitionWithDuration( scene );
    SoundEngine::Get()->PlayEffect( SoundEngine::eSoundPageTurn );
    CCDirector::sharedDirector()->replaceScene( trans );    
}
//--------------------------------------------------------------------------------------------------

