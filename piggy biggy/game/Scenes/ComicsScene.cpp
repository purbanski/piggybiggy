#include "ComicsScene.h"
#include "Game.h"
#include "MainMenuScene.h"
#include "Transitions.h"
#include "IntroScene.h"
#include "TestFlight/Report.h"
#include "RunLogger/RunLogger.h"
#include "Platform/ToolBox.h"
#include "GLogger.h"
#include "Platform/Lang.h"

USING_NS_CC;

const float ComicsScene::SceneShowDur = 2.0f;
const float ComicsScene::SceneMoveDur = 0.45f;

//--------------------------------------------------------------------------------------------------
// ComicsScene
//--------------------------------------------------------------------------------------------------
CCScene* ComicsScene::CreateScene()
{
	CCScene *scene = CCScene::node();

    ComicsScene *layer = new ComicsScene();
    if ( layer && layer->init() )
    {
        layer->autorelease();
        scene->setScale( Game::Get()->GetScale() );
        scene->addChild( layer );
        return scene;
    }
    else
    {
        CC_SAFE_DELETE( layer );
        return NULL;
    }
}
//--------------------------------------------------------------------------------------------------
ComicsScene::ComicsScene()
{
    if ( ToolBox::LowMemDevice( gDevice ))
        CCTexture2D::setDefaultAlphaPixelFormat( kCCTexture2DPixelFormat_RGBA4444 );

    _skipPressed = false;
    _debugIndex = 0;
    gAppDisplayRotateMode = eDisplay_LandscapeRotating;
}
//--------------------------------------------------------------------------------------------------
ComicsScene::~ComicsScene()
{
	removeAllChildrenWithCleanup( true );
}
//--------------------------------------------------------------------------------------------------
bool ComicsScene::init()
{
    RLOG_S("INTRO","CONST" );
    GSendView( "Intro" );
    
	if ( ! CCLayer::init() )
		return false;

	if ( Tools::GetScreenSize().width != Config::GameSize.width  || Tools::GetScreenSize().height != Config::GameSize.height )
	{
		CCLayer* scaleScreenLayer = ScaleScreenLayer::node();
		scaleScreenLayer->setPosition( Tools::GetScreenMiddle() );
		addChild( scaleScreenLayer, 250 );
	}

    _scene1 = GetSceneSprite( 1 );
    _scene2 = GetSceneSprite( 2 );
    _scene3 = GetSceneSprite( 3 );
    _scene4 = GetSceneSprite( 4 );
    _scene5 = GetSceneSprite( 5 );
    _scene6 = GetSceneSprite( 6 );
    _scene7 = GetSceneSprite( 7 );
    
    _scene1->retain();
    _scene2->retain();
    _scene3->retain();
    _scene4->retain();
    _scene5->retain();
    _scene6->retain();
    _scene7->retain();
    
    SetScene1();
    
    int fadeDestDown = 190;
    int fadeDestUp = 230;
    float fadeDur = 0.45f;
    float delayDur = 1.0f;
    
    CCPoint touchToSkipPos;
    CCSprite *touchToSkip;
    CCSprite *touchToSkipFlash;
    
//    touchToSkipPos.x = Tools::GetScreenMiddleX() + 345.0f;
//    touchToSkipPos.y = Tools::GetScreenMiddleY() - 275.0f;
    
    touchToSkipPos.x = Tools::GetScreenMiddleX();
    touchToSkipPos.y = Tools::GetScreenMiddleY();
    
    touchToSkip = CCSprite::spriteWithFile( Lang::Get()->GetLang( "Images/Intro/Comics/touchToSkip.png" ));
    touchToSkipFlash = CCSprite::spriteWithFile( Lang::Get()->GetLang( "Images/Intro/Comics/touchToSkipFlash.png" ));
    
    touchToSkip->setPosition( touchToSkipPos );
    touchToSkipFlash->setPosition( touchToSkipPos );
    
    touchToSkip->setOpacity( 0 );
    touchToSkipFlash->setOpacity( 0 );
    
    int blinkRep;
    blinkRep = 6;
    
    CCFiniteTimeAction *seqLabel = CCSequence::actions(
                                          CCDelayTime::actionWithDuration( delayDur ),
                                          CCFadeTo::actionWithDuration( 1.0f, 255 ),
                                          CCDelayTime::actionWithDuration( fadeDur * (float) blinkRep * 2.0f ),
                                          CCFadeTo::actionWithDuration( fadeDur, 0 ),
                                          NULL );
    
    CCFiniteTimeAction *seqFlash = CCSequence::actions(
                                          CCDelayTime::actionWithDuration( delayDur ),
                                          CCFadeTo::actionWithDuration( 1.0f, fadeDestUp ),
                                          CCRepeat::actionWithAction(
                                                                     CCSequence::actions(
                                                                                       CCFadeTo::actionWithDuration( fadeDur, fadeDestDown ),
                                                                                         CCFadeTo::actionWithDuration( fadeDur, fadeDestUp ),
                                                                                         NULL ), blinkRep ),
                                          CCFadeTo::actionWithDuration( fadeDur, 0 ),
                                          NULL );
    
    
    addChild( touchToSkip, 250 );
    addChild( touchToSkipFlash, 240 );
    
    touchToSkip->runAction( seqLabel );
    touchToSkipFlash->runAction( seqFlash );

    SoundEngine::Get()->GetBackgroundMusic().SetMusic_Intro();

    setIsTouchEnabled( true );
    return true;
}
//--------------------------------------------------------------------------------------------------
CCSprite* ComicsScene::GetSceneSprite( int index )
{
    stringstream ss;
    ss << "Images/Intro/Comics/page" << index << ".jpg";
    
    CCSprite *sprite = CCSprite::spriteWithFile( ss.str().c_str() );
    return sprite;
}
//--------------------------------------------------------------------------------------------------
void ComicsScene::RemoveSprite( CCNode *sender, void *data )
{
    CCSprite *sprite;
    sprite = (CCSprite *) data;
    
    sprite->removeFromParentAndCleanup( true );
}
//--------------------------------------------------------------------------------------------------
void ComicsScene::onEnter()
{
	CCLayer::onEnter();
}
//--------------------------------------------------------------------------------------------------
void ComicsScene::onEnterTransitionDidFinish()
{
   CCLayer::onEnterTransitionDidFinish();
}
//--------------------------------------------------------------------------------------------------
void ComicsScene::onExit()
{
	CCLayer::onExit();
   	CCTouchDispatcher::sharedDispatcher()->removeDelegate( this );
}
//--------------------------------------------------------------------------------------------------
bool ComicsScene::ccTouchBegan(CCTouch *touch, CCEvent *pEvent)
{
    if ( _skipPressed )
        return true;
    
    _skipPressed = true;
    
    RLOG_SIII("INTRO", "SKIP", _debugIndex, 0, 0);
    GLogEvent("Intro", "Skip (scene:%d)", _debugIndex);

    ShowMainMenuScene();
    return true;
}
//--------------------------------------------------------------------------------------------------
void ComicsScene::ccTouchMoved(CCTouch *touch, CCEvent *pEvent)
{
//    b2Vec2 pos;
//    pos = Tools::TouchAdjust( touch );
//    
//    b2Vec2 newPos;
//    
//    newPos.x = _startPos.x - ( _startTouch.x - pos.x ) * RATIO;
//    newPos.y = _startPos.y - ( _startTouch.y - pos.y ) * RATIO;
//    
//    _activeSprite->setPosition( ccp( newPos.x, newPos.y ));
}
//--------------------------------------------------------------------------------------------------
void ComicsScene::ccTouchEnded(CCTouch *touch, CCEvent *pEvent)
{
//    D_LOG("Intro sprite x:%f, y:%f", Tools::GetScreenMiddleX() - _activeSprite->getPosition().x, Tools::GetScreenMiddleY() - _activeSprite->getPosition().y );
}
//--------------------------------------------------------------------------------------------------
void ComicsScene::registerWithTouchDispatcher()
{
    CCTouchDispatcher::sharedDispatcher()->addTargetedDelegate( this, 1, true );
}
//--------------------------------------------------------------------------------------------------
void ComicsScene::ShowMainMenuScene()
{
    ReleaseAll();
   	CCTouchDispatcher::sharedDispatcher()->removeDelegate( this );
    
    CCScene *scene;
    scene = MainMenuScene::CreateScene();
    
    TransitionScene1 *trans;
    trans = TransitionScene1::transitionWithDuration( scene );
    
    SoundEngine::Get()->PlayEffect( SoundEngine::eSoundPageTurn );
    CCDirector::sharedDirector()->replaceScene( trans );
}
//--------------------------------------------------------------------------------------------------
void ComicsScene::SetScene1()
{
    if ( _skipPressed )
        return;

    _debugIndex = 1;

    _scene1->setScale( 0.68f );
    _scene1->setPosition( ccp( Tools::GetScreenMiddle().x + 24.0f, Tools::GetScreenMiddle().y - 184.0f ));

    CCAction *trasAction = CCSequence::actions(
                                               CCMoveTo::actionWithDuration( SceneShowDur + 1.0f, ccp(
                                                                                               Tools::GetScreenMiddle().x + 40.0f,
                                                                                               Tools::GetScreenMiddle().y - 195.0f )),
                                               
                                               CCMoveTo::actionWithDuration( SceneMoveDur, ccp(
                                                                                               Tools::GetScreenMiddle().x + 410.0f,
                                                                                               Tools::GetScreenMiddle().y + 400.0f )),
                                               
                                               CCMoveTo::actionWithDuration( SceneShowDur, ccp(
                                                                                               Tools::GetScreenMiddle().x + 455.0f,
                                                                                               Tools::GetScreenMiddle().y + 410.0f )),

                                               CCMoveTo::actionWithDuration( SceneMoveDur, ccp(
                                                                                               Tools::GetScreenMiddle().x - 390.0f,
                                                                                               Tools::GetScreenMiddle().y + 440.0f )),
                                               
                                               CCMoveTo::actionWithDuration( SceneShowDur, ccp(
                                                                                               Tools::GetScreenMiddle().x - 420.0f,
                                                                                               Tools::GetScreenMiddle().y + 430.0f )),

                                               CCCallFunc::actionWithTarget( this, callfunc_selector( ComicsScene::SetScene2 )),
                                               
                                               CCMoveTo::actionWithDuration( SceneMoveDur, ccp(
                                                                                               Tools::GetScreenMiddle().x - 1364.0f,
                                                                                               Tools::GetScreenMiddle().y - 202.0f )),

                                               CCCallFuncND::actionWithTarget( this, callfuncND_selector( ComicsScene::RemoveSprite ), (void*) _scene1 ),

                                               NULL );
    
    CCAction *scaleAction = CCSequence::actions(CCDelayTime::actionWithDuration( SceneShowDur + 1.0f ),
                                                CCScaleTo::actionWithDuration( SceneMoveDur, 1.0f ),
                                                CCDelayTime::actionWithDuration( SceneShowDur ),
                                                NULL );
  
    addChild( _scene1, 1 );
    
    _scene1->runAction( trasAction );
    _scene1->runAction( scaleAction );
}
//--------------------------------------------------------------------------------------------------
void ComicsScene::SetScene2()
{
    if ( _skipPressed )
        return;

    float xmove;
    float ymove;
    
    xmove = 0.0f;
    ymove = 0.0f;
    
    _debugIndex = 2;

    _scene2->setScale( 1.025f );
	_scene2->setPosition( ccp( Tools::GetScreenMiddle().x + xmove + 1388.0f, Tools::GetScreenMiddle().y + ymove + 290.0f ));

    CCAction *trasAction = CCSequence::actions(
                                               
                                               CCMoveTo::actionWithDuration( SceneMoveDur, ccp(
                                                                                               Tools::GetScreenMiddle().x + xmove + 420.0f,
                                                                                               Tools::GetScreenMiddle().y + ymove - 328.0f )),
                                               
                                               CCMoveTo::actionWithDuration( SceneShowDur, ccp(
                                                                                               Tools::GetScreenMiddle().x + xmove + 396.0f,
                                                                                               Tools::GetScreenMiddle().y + ymove - 336.0f )),
                                               
                                               CCMoveTo::actionWithDuration( SceneMoveDur, ccp(
                                                                                               Tools::GetScreenMiddle().x + xmove - 444.0f,
                                                                                               Tools::GetScreenMiddle().y + ymove - 332.0f )),
                                               
                                               CCMoveTo::actionWithDuration( SceneShowDur, ccp(
                                                                                               Tools::GetScreenMiddle().x + xmove - 472.0f,
                                                                                               Tools::GetScreenMiddle().y + ymove - 341.0f )),

                                               CCMoveTo::actionWithDuration( SceneMoveDur, ccp(
                                                                                               Tools::GetScreenMiddle().x + xmove + 412.0f,
                                                                                               Tools::GetScreenMiddle().y + ymove + 236.0f )),

                                               CCMoveTo::actionWithDuration( SceneShowDur, ccp(
                                                                                               Tools::GetScreenMiddle().x + xmove + 436.0f,
                                                                                               Tools::GetScreenMiddle().y + ymove + 268.0f )),
                                               

                                               CCMoveTo::actionWithDuration( SceneMoveDur, ccp(
                                                                                               Tools::GetScreenMiddle().x + xmove - 452.0f,
                                                                                               Tools::GetScreenMiddle().y + ymove + 252.0f )),

                                               CCMoveTo::actionWithDuration( SceneShowDur, ccp(
                                                                                               Tools::GetScreenMiddle().x + xmove - 488.0f,
                                                                                               Tools::GetScreenMiddle().y + ymove + 236.0f )),

                                               CCCallFunc::actionWithTarget( this, callfunc_selector( ComicsScene::SetScene3 )),

                                               CCMoveTo::actionWithDuration( SceneMoveDur, ccp(
                                                                                               Tools::GetScreenMiddle().x + xmove - 1396.0f,
                                                                                               Tools::GetScreenMiddle().y + ymove - 146.0f )),
                                               
                                               CCCallFuncND::actionWithTarget( this, callfuncND_selector( ComicsScene::RemoveSprite ), (void*) _scene2 ),

                                               NULL );

    addChild( _scene2, 1 );
    _scene2->runAction( trasAction );
}
//--------------------------------------------------------------------------------------------------
void ComicsScene::SetScene3()
{
    if ( _skipPressed )
       return;

    float xmove;
    float ymove;
    
    xmove = 0.0f;
    ymove = 0.0f;
    
    _debugIndex = 3;

    _scene3->setScale( 0.975f );
  	_scene3->setPosition( ccp( Tools::GetScreenMiddle().x + xmove + 998.0f, Tools::GetScreenMiddle().y + ymove + 54.0f ));

    CCAction *trasAction = CCSequence::actions(
                                               
                                               CCMoveTo::actionWithDuration( SceneMoveDur, ccp(
                                                                                               Tools::GetScreenMiddle().x + xmove + 38.0f,
                                                                                               Tools::GetScreenMiddle().y + ymove - 276.0f )),
                                               
                                               CCMoveTo::actionWithDuration( SceneShowDur, ccp(
                                                                                               Tools::GetScreenMiddle().x + xmove + 50.0f,
                                                                                               Tools::GetScreenMiddle().y + ymove - 290.0f )),
                                               
                                               CCMoveTo::actionWithDuration( SceneMoveDur, ccp(
                                                                                               Tools::GetScreenMiddle().x + xmove + 65.0f,
                                                                                               Tools::GetScreenMiddle().y + ymove + 314.0f )),

                                               CCMoveTo::actionWithDuration( SceneShowDur, ccp(
                                                                                               Tools::GetScreenMiddle().x + xmove + 39.0f,
                                                                                               Tools::GetScreenMiddle().y + ymove + 342.0f )),

                                               CCCallFunc::actionWithTarget( this, callfunc_selector( ComicsScene::SetScene4 )),

                                               CCMoveTo::actionWithDuration( SceneMoveDur, ccp(
                                                                                               Tools::GetScreenMiddle().x + xmove - 931.0f,
                                                                                               Tools::GetScreenMiddle().y + ymove + 20.0f )),

                                               CCCallFuncND::actionWithTarget( this, callfuncND_selector( ComicsScene::RemoveSprite ), (void*) _scene3 ),

                                               NULL );

    CCAction *scaleAction = CCSequence::actions(
                                                CCDelayTime::actionWithDuration( SceneMoveDur ),
                                                CCScaleTo::actionWithDuration( SceneShowDur, 1.0f ),
                                                CCDelayTime::actionWithDuration( SceneMoveDur ),
                                                CCScaleTo::actionWithDuration( SceneShowDur, 1.05f ),
                                                NULL );

    addChild( _scene3, 1 );
    _scene3->runAction( trasAction );
    _scene3->runAction( scaleAction );
}
//--------------------------------------------------------------------------------------------------
void ComicsScene::SetScene4()
{
    if ( _skipPressed )
        return;

    float xmove;
    float ymove;
    
    xmove = 0.0f;
    ymove = 0.0f;
    
    _debugIndex = 4;
    
    _scene4->setScale( 0.82f );
  	_scene4->setPosition( ccp( Tools::GetScreenMiddle().x + xmove + 1194, Tools::GetScreenMiddle().y + ymove + 128.0f ));

    CCAction *trasAction = CCSequence::actions(
                                               
                                               CCMoveTo::actionWithDuration( SceneMoveDur, ccp(
                                                                                               Tools::GetScreenMiddle().x + xmove + 57.0f,
                                                                                               Tools::GetScreenMiddle().y + ymove - 244.0f )),
                                               CCMoveTo::actionWithDuration( SceneShowDur, ccp(
                                                                                               Tools::GetScreenMiddle().x + xmove + 23.0f,
                                                                                               Tools::GetScreenMiddle().y + ymove - 254.0f )),
                                               
                                               CCMoveTo::actionWithDuration( SceneMoveDur, ccp(
                                                                                               Tools::GetScreenMiddle().x + xmove + 451.0f,
                                                                                               Tools::GetScreenMiddle().y + ymove + 386.0f )),
                                               
                                               CCMoveTo::actionWithDuration( SceneShowDur, ccp(
                                                                                               Tools::GetScreenMiddle().x + xmove + 485.0f,
                                                                                               Tools::GetScreenMiddle().y + ymove + 396.0f )),
                                               
                                               
                                               CCMoveTo::actionWithDuration( SceneMoveDur, ccp(
                                                                                               Tools::GetScreenMiddle().x + xmove - 409.0f,
                                                                                               Tools::GetScreenMiddle().y + ymove + 362.0f )),
                                               
                                               CCMoveTo::actionWithDuration( SceneShowDur, ccp(
                                                                                               Tools::GetScreenMiddle().x + xmove - 451.0f,
                                                                                               Tools::GetScreenMiddle().y + ymove + 360.0f )),

                                               CCCallFunc::actionWithTarget( this, callfunc_selector( ComicsScene::SetScene5 )),
                                               
                                               CCMoveTo::actionWithDuration( SceneMoveDur, ccp(
                                                                                               Tools::GetScreenMiddle().x + xmove - 1393.0f,
                                                                                               Tools::GetScreenMiddle().y + ymove + 358.0f )),

                                               CCCallFuncND::actionWithTarget( this, callfuncND_selector( ComicsScene::RemoveSprite ), (void*) _scene4 ),
                                               
                                               NULL );

    CCAction *scaleAction = CCSequence::actions(
                                                CCDelayTime::actionWithDuration( SceneMoveDur + SceneShowDur ),
                                                CCScaleTo::actionWithDuration( SceneMoveDur, 1.05f ),
                                                NULL );

    addChild( _scene4, 1 );
    _scene4->runAction( trasAction );
    _scene4->runAction( scaleAction );
}
//--------------------------------------------------------------------------------------------------
void ComicsScene::SetScene5()
{
   if ( _skipPressed )
        return;

    float xmove;
    float ymove;
    
    xmove = 0.0f;
    ymove = 0.0f;

    _debugIndex = 5;
    
    _scene5->setScale( 1.075f );
  	_scene5->setPosition( ccp( Tools::GetScreenMiddle().x + xmove + 1396.0f, Tools::GetScreenMiddle().y + ymove + 38.0f ));

    CCAction *trasAction = CCSequence::actions(
                                               
                                               CCMoveTo::actionWithDuration( SceneMoveDur, ccp(
                                                                                               Tools::GetScreenMiddle().x + xmove + 454.0f,
                                                                                               Tools::GetScreenMiddle().y + ymove + 60.0f )),
                                               
                                               CCMoveTo::actionWithDuration( SceneShowDur, ccp(
                                                                                               Tools::GetScreenMiddle().x + xmove + 420.0f,
                                                                                               Tools::GetScreenMiddle().y + ymove + 46.0f )),
                                               
                                               CCMoveTo::actionWithDuration( SceneMoveDur, ccp(
                                                                                               Tools::GetScreenMiddle().x + xmove - 432.0f,
                                                                                               Tools::GetScreenMiddle().y + ymove + 65.0f )),

                                               CCMoveTo::actionWithDuration( SceneShowDur, ccp(
                                                                                               Tools::GetScreenMiddle().x + xmove - 468.0f,
                                                                                               Tools::GetScreenMiddle().y + ymove + 61.0f )),

                                               CCCallFunc::actionWithTarget( this, callfunc_selector( ComicsScene::SetScene6 )),

                                               CCMoveTo::actionWithDuration( SceneMoveDur, ccp(
                                                                                               Tools::GetScreenMiddle().x + xmove - 1406.0f,
                                                                                               Tools::GetScreenMiddle().y + ymove - 192.0f )),

                                               CCCallFuncND::actionWithTarget( this, callfuncND_selector( ComicsScene::RemoveSprite ), (void*) _scene5 ),

                                               NULL );


    addChild( _scene5, 1 );
    _scene5->runAction( trasAction );
}
//--------------------------------------------------------------------------------------------------
void ComicsScene::SetScene6()
{
    if ( _skipPressed )
        return;

    float xmove;
    float ymove;
    
    xmove = 0.0f;
    ymove = 0.0f;

    _debugIndex = 6;
  
    _scene6->setScale( 0.755f );
  	_scene6->setPosition( ccp( Tools::GetScreenMiddle().x + xmove + 1114.0f, Tools::GetScreenMiddle().y + ymove - 72.0f ));

    CCAction *trasAction = CCSequence::actions(
                                               
                                               CCMoveTo::actionWithDuration( SceneMoveDur, ccp(
                                                                                               Tools::GetScreenMiddle().x + xmove + 18.0f,
                                                                                               Tools::GetScreenMiddle().y + ymove - 248.0f )),
                                               
                                               CCMoveTo::actionWithDuration( SceneShowDur, ccp(
                                                                                               Tools::GetScreenMiddle().x + xmove + 10.0f,
                                                                                               Tools::GetScreenMiddle().y + ymove - 264.0f )),

                                        
                                               CCMoveTo::actionWithDuration( SceneMoveDur, ccp(
                                                                                               Tools::GetScreenMiddle().x + xmove + 422.0f,
                                                                                               Tools::GetScreenMiddle().y + ymove + 380.0f )),

                                               CCMoveTo::actionWithDuration( SceneShowDur, ccp(
                                                                                               Tools::GetScreenMiddle().x + xmove + 470.0f,
                                                                                               Tools::GetScreenMiddle().y + ymove + 388.0f )),

                                               
                                               CCMoveTo::actionWithDuration( SceneMoveDur, ccp(
                                                                                               Tools::GetScreenMiddle().x + xmove - 410.0f,
                                                                                               Tools::GetScreenMiddle().y + ymove + 372.0f )),

                                               CCMoveTo::actionWithDuration( SceneShowDur, ccp(
                                                                                               Tools::GetScreenMiddle().x + xmove - 444.0f,
                                                                                               Tools::GetScreenMiddle().y + ymove + 358.0f )),
                                               
                                               CCCallFunc::actionWithTarget( this, callfunc_selector( ComicsScene::SetScene7 )),

                                               CCMoveTo::actionWithDuration( SceneMoveDur, ccp(
                                                                                               Tools::GetScreenMiddle().x + xmove - 1374.0f,
                                                                                               Tools::GetScreenMiddle().y + ymove - 22.0f )),

                                               CCCallFuncND::actionWithTarget( this, callfuncND_selector( ComicsScene::RemoveSprite ), (void*) _scene6 ),

                                               NULL );


    CCAction *scaleAction = CCSequence::actions(
                                                CCDelayTime::actionWithDuration( SceneMoveDur + SceneShowDur ),
                                                CCScaleTo::actionWithDuration( SceneMoveDur, 1.05f ),
                                                NULL );
    addChild( _scene6, 1 );
    _scene6->runAction( trasAction );
    _scene6->runAction( scaleAction );
}
//--------------------------------------------------------------------------------------------------
void ComicsScene::SetScene7()
{
    if ( _skipPressed )
        return;

    _debugIndex = 7;
    
    float xmove;
    float ymove;
    
    xmove = 0.0f;
    ymove = 0.0f;
    
    _scene7->setScale( 0.78f );
  	_scene7->setPosition( ccp( Tools::GetScreenMiddle().x + xmove + 1115.0f, Tools::GetScreenMiddle().y + ymove - 308.0f ));

    CCAction *trasAction = CCSequence::actions(
                                               
                                               CCMoveTo::actionWithDuration( SceneMoveDur, ccp(
                                                                                               Tools::GetScreenMiddle().x + xmove + 48.0f,
                                                                                               Tools::GetScreenMiddle().y + ymove - 212.0f )),
                                               
                                               CCMoveTo::actionWithDuration( SceneShowDur, ccp(
                                                                                               Tools::GetScreenMiddle().x + xmove + 20.0f,
                                                                                               Tools::GetScreenMiddle().y + ymove - 234.0f )),

                                        
                                               CCMoveTo::actionWithDuration( SceneMoveDur, ccp(
                                                                                               Tools::GetScreenMiddle().x + xmove + 452.0f,
                                                                                               Tools::GetScreenMiddle().y + ymove + 416.0f )),

                                               CCMoveTo::actionWithDuration( SceneShowDur, ccp(
                                                                                               Tools::GetScreenMiddle().x + xmove + 486.0f,
                                                                                               Tools::GetScreenMiddle().y + ymove + 436.0f )),
//
//                                               
                                               CCMoveTo::actionWithDuration( SceneMoveDur, ccp(
                                                                                               Tools::GetScreenMiddle().x + xmove - 422.0f,
                                                                                               Tools::GetScreenMiddle().y + ymove + 412.0f )),
//
                                               CCMoveTo::actionWithDuration( SceneShowDur, ccp(
                                                                                               Tools::GetScreenMiddle().x + xmove - 452.0f,
                                                                                               Tools::GetScreenMiddle().y + ymove + 430.0f )),
//
                                               CCCallFunc::actionWithTarget( this, callfunc_selector( ComicsScene::ShowMainMenuScene )),
//
//                                               CCMoveTo::actionWithDuration( SceneMoveDur, ccp(
//                                                                                               Tools::GetScreenMiddle().x + xmove - 1374.0f,
//                                                                                               Tools::GetScreenMiddle().y + ymove - 22.0f )),
//
//                                               CCCallFuncND::actionWithTarget( this, callfuncND_selector( ComicsScene::RemoveSprite ), (void*) background ),
//
                                               NULL );


    CCAction *scaleAction = CCSequence::actions(
                                                CCDelayTime::actionWithDuration( SceneMoveDur + SceneShowDur ),
                                                CCScaleTo::actionWithDuration( SceneMoveDur, 1.1f ),
                                                NULL );
    addChild( _scene7, 1 );
    _scene7->runAction( trasAction );
    _scene7->runAction( scaleAction );
    
    RLOG_S("INTRO","LAST_SCENE");
    GLogEvent("Intro","Last scene");
}
//--------------------------------------------------------------------------------------------------
void ComicsScene::ReleaseAll()
{
    _scene1->stopAllActions();
    _scene2->stopAllActions();
    _scene3->stopAllActions();
    _scene4->stopAllActions();
    _scene5->stopAllActions();
    _scene6->stopAllActions();
    _scene7->stopAllActions();
    
    _scene1->release();
    _scene2->release();
    _scene3->release();
    _scene4->release();
    _scene5->release();
    _scene6->release();
    _scene7->release();
}
//--------------------------------------------------------------------------------------------------
