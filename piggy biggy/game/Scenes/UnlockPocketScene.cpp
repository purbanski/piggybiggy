#include "UnlockPocketScene.h"
#include "NextLevelScene.h"
#include "LoadingBar.h"
#include "Transitions.h"
#include "SoundEngine.h"
#include "Tools.h"
#include "Levels/Pockets.h"
#include "RunLogger/RunLogger.h"
#include "RunLogger/GLogger.h"
#include "LoadingBar.h"
#include "Game.h"
#include "Skins.h"
#include "Platform/Lang.h"

//-------------------------------------------------------------------------------
void UnlockPocketNode::ShowScene( GameTypes::PocketType pocket )
{
	LoadingBar::Get()->AddToScene( &UnlockPocketNode::DoShowScene, (void *)pocket );
}
//-------------------------------------------------------------------------------
void UnlockPocketNode::DoShowScene( void *data )
{
	GameTypes::PocketType pocket;
	pocket = (GameTypes::PocketType)(long) data;

	CCScene* scene = LevelToLevelTransition::transitionWithDuration( 0.8f, CreateScene( pocket )); 
	if ( scene )
	{
		SoundEngine::Get()->PlayEffect( SoundEngine::eSoundChangeScene );
		CCDirector::sharedDirector()->replaceScene( scene );
	}
}
//-------------------------------------------------------------------------------
CCScene* UnlockPocketNode::CreateScene( GameTypes::PocketType pocket )
{
	CCScene *scene = CCScene::node();
	UnlockPocketNode *layer = UnlockPocketNode::node( pocket );
	layer->setPosition( Tools::GetScreenMiddle() );
	scene->addChild(layer);
	return scene;
}
//-------------------------------------------------------------------------------
UnlockPocketNode::UnlockPocketNode( GameTypes::PocketType pocket )
{
	_started = false;
	_pocket = pocket;
}
//-------------------------------------------------------------------------------
bool UnlockPocketNode::init()
{
    RLOG_S("POCKET_UNLOCK", _pocket );
    
	if ( ! CCLayer::init() )
		return false;

	setIsVisible( false );

	_pocketSprite = PocketManager::Get()->GetSpritePocketUnlockAnim( _pocket );
	_padLockSprite = CCSprite::spriteWithFile( "Images/Pockets/PadlockAnim.png" );
    _loadingSprite = (CCSprite*) LoadingSprite::GetL();
    
	_pocketSprite->setOpacity( 0 );
    _pocketSprite->setScale( 1.20689f ); // rescale menu Pocket to anim Pocket size
    
	_padLockSprite->setOpacity( 0 );
    _loadingSprite->setOpacity( 0 );
    
    CCPoint pos;
    pos = CCPoint( 175.0f, 0.0f );
    
    _pocketSprite->setPosition( pos );
    _padLockSprite->setPosition( pos );
    _loadingSprite->setPosition( CCPointZero );
    
	addChild( _pocketSprite, 20 );
	addChild( _padLockSprite, 30 );
    addChild( _loadingSprite, 150 );

	setContentSizeInPixels( _pocketSprite->getContentSizeInPixels() );
    ConstructAnim();

    Skins::SetSkinName( Game::Get()->GetGameStatus()->GetCurrentPocket() );
    
    CCSprite *bg;
    CCSprite *person;
    
    person = CCSprite::spriteWithFile( Skins::GetSkinName( "Images/Levels/Backgrounds/person.png" ));
    bg = PocketManager::Get()->GetSelectPocketBackground( Game::Get()->GetGameStatus()->GetCurrentPocket() );
        
    bg->setPosition( CCPointZero );
    person->setPosition( ccp( -280.0f, 0.0f ));
    
    addChild( person, 5 );
    addChild( bg, 1 );
    
    StartAnim();
	return true;
}
//-------------------------------------------------------------------------------
UnlockPocketNode::~UnlockPocketNode()
{
}
//-------------------------------------------------------------------------------
UnlockPocketNode* UnlockPocketNode::node( GameTypes::PocketType pocket )
{
	UnlockPocketNode *ret = new UnlockPocketNode( pocket );
	if ( ret && ret->init() )
	{
		ret->autorelease();
		return ret;
	}
	else
	{	
		delete ret;
		ret = NULL;
		return NULL;
	}
}
//-------------------------------------------------------------------------------
void UnlockPocketNode::SceneFinished()
{
    Preloader::Get()->FreeResources();
    Game::Get()->CreateAndPlayLevel( Game::Get()->GetGameStatus()->GetCurrentLevel(), Game::eLevelToLevel, false );
 }
//-------------------------------------------------------------------------------
void UnlockPocketNode::StartAnim()
{
	if ( _started )
		return;

	_started = true;
	setIsVisible( true );

	//-----------------------------------
	// Pocket Open - status
	GameStatus *status;
	status = Game::Get()->GetGameStatus();

	if ( status->GetPocketStatus( _pocket ) == GameTypes:: ePocketStatusLocked )
		status->SetPocketStatus( _pocket, GameTypes::ePocketStatusOpen );
    
	status->Save();
    
	_pocketSprite->runAction( _fadeInAction );
	_padLockSprite->runAction( _fadeOutAction );
    _padLockSprite->runAction( _soundFadeAction );
    _loadingSprite->runAction( _loadingFadeInAction );
}
//-------------------------------------------------------------------------------
void UnlockPocketNode::ConstructAnim()
{
    //-----------------------------------
	// Pocket anims
	float initWait;
	initWait = 2.0f;

	_fadeInAction = CCSequence::actions(
		CCMoveBy::actionWithDuration( initWait, CCPointZero ),
		CCFadeTo::actionWithDuration( 1.0f, 255 ),
		NULL );

	_fadeOutAction = CCSequence::actions(
		CCDelayTime::actionWithDuration( initWait ),
		CCCallFunc::actionWithTarget(this, callfunc_selector( UnlockPocketNode::PlayUnlockSound  )),
		CCFadeTo::actionWithDuration( 1.0f, 255 ),
		CCMoveBy::actionWithDuration( 0.5f, CCPointZero ),
		CCFadeTo::actionWithDuration( 1.0f, 0 ),
		NULL );

   	_soundFadeAction = CCSequence::actions(
		CCRepeat::actionWithAction(
          CCSequence::actionOneTwo(
            CCCallFunc::actionWithTarget( this, callfunc_selector( UnlockPocketNode::FadeOutSound  )),
            CCDelayTime::actionWithDuration( initWait / 5 )
        ), 5 ),
        
        CCDelayTime::actionWithDuration( SoundEngine::Get()->GetEffectDuration( SoundEngine::eSoundPocketUnlock )),

        CCRepeat::actionWithAction(
          CCSequence::actionOneTwo(
            CCCallFunc::actionWithTarget( this, callfunc_selector( UnlockPocketNode::FadeInSound  )),
            CCDelayTime::actionWithDuration( initWait / 5 )
        ), 5 ),
                                           
		CCCallFunc::actionWithTarget(this, callfunc_selector( UnlockPocketNode::SceneFinished )),
		NULL );
    
    _loadingFadeInAction = CCSequence::actions(
                                               CCDelayTime::actionWithDuration( initWait * 1.2f + SoundEngine::Get()->GetEffectDuration(SoundEngine::eSoundPocketUnlock )),
                                               	
                                               CCFadeTo::actionWithDuration( 0.25f, 255 ),
                                               NULL );
    
}
//-------------------------------------------------------------------------------
void UnlockPocketNode::FadeOutSound()
{
    float volumne;
    float step;
    
    step = 0.15f;
    volumne = SoundEngine::Get()->GetMusicVolume();
    
    SoundEngine::Get()->SetMusicVolume( MAX( volumne - step, 0.0f ));
}
//-------------------------------------------------------------------------------
void UnlockPocketNode::FadeInSound()
{
    float volumne;
    float step;
    
    step = 0.15f;
    volumne = SoundEngine::Get()->GetMusicVolume();

    SoundEngine::Get()->SetMusicVolume( MIN( volumne + step, Config::SoundMusicVol ));
}
//-------------------------------------------------------------------------------
void UnlockPocketNode::PlayUnlockSound()
{
	SoundEngine::Get()->PlayEffect( SoundEngine::eSoundPocketUnlock );
}
//-------------------------------------------------------------------------------
void UnlockPocketNode::onExit()
{
    CCLayer::onExit();
}
//-------------------------------------------------------------------------------
