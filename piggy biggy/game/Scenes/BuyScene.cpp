#include "BuyScene.h"
#include "MessageBox.h"
#include "Game.h"
#include "LoadingBar.h"
#include "Transitions.h"
#include "Components/FXMenuItem.h"
#include "MainMenuScene.h"
#include "Billing/Billing.h"
#include "Tools.h"
#include "RunLogger/RunLogger.h"
#include "GLogger.h"
#include "Scenes/NextLevelScene.h"
#include "Platform/Lang.h"

//--------------------------------------------------------------------------------------------------
// LevelShotsVideo
//--------------------------------------------------------------------------------------------------
LevelShotsVideo* LevelShotsVideo::Create()
{
	LevelShotsVideo* node = new LevelShotsVideo();
	if ( node )
	{
		node->autorelease();
		//node->Init();
		return node;
	}
	CC_SAFE_DELETE(node);
	return NULL;
}
//--------------------------------------------------------------------------------------------------
int32 LevelShotsVideo::VideoStopped( void* systemData, void* userData )
{
	return true;
}
//--------------------------------------------------------------------------------------------------
LevelShotsVideo::LevelShotsVideo()
{
//	s3eResult res;
	 
//	res = s3eVideoPlay("Images/BuyScene/Intro.3gp", 0, 
//		(int)( Tools::GetScreenMiddleX() - 190.0f ),
//		(int)( Tools::GetScreenMiddleY() - 170.0f ),
//		400, 
//		266 );
//
//	if ( res != S3E_RESULT_SUCCESS )
//	{
//		unAssertMsg( Billing, false, ("Error playing video: %s", s3eVideoGetErrorString() ));
//		return;
//	}
//
//	res = s3eVideoRegister( S3E_VIDEO_STOP, &LevelShotsVideo::VideoStopped, NULL);
//	if ( res != S3E_RESULT_SUCCESS )
//	{
//		unAssertMsg( Billing, false, ("Error registering video stop function: %s", s3eVideoGetErrorString() ));
//		return;
//	}
}
//--------------------------------------------------------------------------------------------------
LevelShotsVideo::~LevelShotsVideo()
{
	
}



//--------------------------------------------------------------------------------------------------
// BillingProcessingAnim
//--------------------------------------------------------------------------------------------------
BillingProcessingAnim* BillingProcessingAnim::Create()
{
	BillingProcessingAnim* node = new BillingProcessingAnim();
	if ( node )
	{
		node->autorelease();
		node->Init();
		return node;
	}
	CC_SAFE_DELETE(node);
	return NULL;
}
//--------------------------------------------------------------------------------------------------
BillingProcessingAnim::BillingProcessingAnim()
{
}
//--------------------------------------------------------------------------------------------------
BillingProcessingAnim::~BillingProcessingAnim()
{
}
//--------------------------------------------------------------------------------------------------
void BillingProcessingAnim::Init()
{
	_dot1Started = false;
	_dot2Started = false;
	_dot3Started = false;

	_coin		= CCSprite::spriteWithFile( "Images/BuyScene/Processing/Coin.png" );
	_piggy		= CCSprite::spriteWithFile( "Images/BuyScene/Processing/Piggy.png" );
	_dot1		= CCSprite::spriteWithFile( "Images/BuyScene/Processing/Dot1.png" );
	_dot2		= CCSprite::spriteWithFile( "Images/BuyScene/Processing/Dot2.png" );
	_dot3		= CCSprite::spriteWithFile( "Images/BuyScene/Processing/Dot3.png" );
	_background	= CCSprite::spriteWithFile( "Images/BuyScene/Processing/Background.png" );


	addChild( _coin, 15 );
	addChild( _piggy, 15 );
	addChild( _dot1, 15 );
	addChild( _dot2, 15 );
	addChild( _dot3, 15 );
	addChild( _background, 10 );

	setContentSizeInPixels( CCSize( 400.0f, 266.0f ));
	SetOpacity( 0 );
}
//--------------------------------------------------------------------------------------------------
void BillingProcessingAnim::StartAnim()
{
	SetOpacity( 255 );

	float deltaX;
	float dotY;
	float durCoinMove;
	float durCoinFade;
	float durDotFade;
	float durDotDesynch;

	durCoinMove = 1.5f;
	durCoinFade = 0.5f;
	durDotFade = 1.0f;
	durDotDesynch = 0.3f;

	deltaX  = 32.0f;
	dotY	= -90.0f;

	//---------------------
	// Coin
	_coin->stopAllActions();
	_coin->setPosition( CCPoint( 0.0f, 90.0f ));

	CCActionInterval* seqCoin = (CCActionInterval*)(CCSequence::actions( 
		CCFadeTo::actionWithDuration( durCoinFade, 255 ),
		CCMoveBy::actionWithDuration( durCoinMove, CCPoint( 0.0f, -100.0f) ),
		CCFadeTo::actionWithDuration( 0.01f, 0 ),
		CCMoveBy::actionWithDuration( 0.01f, CCPoint( 0.0f, 100.0f) ),
		NULL ));

	_coin->runAction( CCRepeatForever::actionWithAction( seqCoin ));

	//------------------------
	// Background
	_background->setOpacity( 0 );
	_background->setPosition( CCPoint( 0.0f, 0.0f ));
	_background->runAction( CCFadeTo::actionWithDuration( 0.25f, 255 ));

	//------------------------
	// _piggy
	_piggy->setOpacity( 0 );
	_piggy->setPosition( CCPoint( 0.0f, -10.0f ));
	_piggy->runAction( CCFadeTo::actionWithDuration( 0.5f, 255 ));

	//------------------------
	// dots
	_dot1->stopAllActions();
	_dot2->stopAllActions();
	_dot3->stopAllActions();

	_dot1->setPosition( CCPoint( -deltaX, dotY ));
	_dot2->setPosition( CCPoint( 0.0f, dotY ));
	_dot3->setPosition( CCPoint( deltaX, dotY ));
	
	_dot1->setOpacity( 0 );
	_dot2->setOpacity( 0 );
	_dot3->setOpacity( 0 );

	_dot1Started = false;
	_dot2Started = false;
	_dot3Started = false;

	//_coin->runAction()
	_stepCounter = 0;
	schedule( schedule_selector( BillingProcessingAnim::Step ));
}
//--------------------------------------------------------------------------------------------------
void BillingProcessingAnim::StopAnim()
{
	_coin->stopAllActions();
	_piggy->stopAllActions();
	_dot1->stopAllActions();
	_dot2->stopAllActions();
	_dot3->stopAllActions();
	_background->stopAllActions();

	unschedule( schedule_selector( BillingProcessingAnim::Step ));

	float dur = 0.3f;

	_coin->runAction( CCFadeTo::actionWithDuration( dur, 0 ));
	_piggy->runAction( CCFadeTo::actionWithDuration( dur, 0 ));
	_dot1->runAction( CCFadeTo::actionWithDuration( dur, 0 ));
	_dot2->runAction( CCFadeTo::actionWithDuration( dur, 0 ));
	_dot3->runAction( CCFadeTo::actionWithDuration( dur, 0 ));
	_background->runAction( CCFadeTo::actionWithDuration( dur, 0 ));
}
//--------------------------------------------------------------------------------------------------
void BillingProcessingAnim::SetOpacity( int opacity )
{
	_coin->setOpacity( opacity );
	_piggy->setOpacity( opacity );
	_dot1->setOpacity( opacity );
	_dot2->setOpacity( opacity );
	_dot3->setOpacity( opacity );
	_background->setOpacity( opacity );
}
//--------------------------------------------------------------------------------------------------
void BillingProcessingAnim::Step( ccTime dTime )
{
	_stepCounter += dTime;

	float durDesynch = 0.3f;
	float durDotFade = 1.0f;
	float durDotFadeBreak = 0.25f;


	//-----------------------
	// dot1
	if ( _stepCounter >= durDesynch && ! _dot1Started )
	{
		CCActionInterval* seqDot1 = (CCActionInterval*)(CCSequence::actions( 
			CCFadeTo::actionWithDuration( durDotFade, 255 ),
			CCFadeTo::actionWithDuration( durDotFade, 0 ),
			CCFadeTo::actionWithDuration( durDotFadeBreak, 0 ),
			NULL ));

		_dot1->runAction( CCRepeatForever::actionWithAction( seqDot1 )) ;

		_dot1Started = true;
	}


	//-----------------------
	// dot2
	if ( ( _stepCounter >= ( durDesynch * 2.0f )) && ! _dot2Started )
	{
		CCActionInterval* seqDot = (CCActionInterval*)(CCSequence::actions( 
			CCFadeTo::actionWithDuration( durDotFade, 255 ),
			CCFadeTo::actionWithDuration( durDotFade, 0 ),
			CCFadeTo::actionWithDuration( durDotFadeBreak, 0 ),
			NULL ));

		_dot2->runAction( CCRepeatForever::actionWithAction( seqDot )) ;
		_dot2Started = true;
	}



	//-----------------------
	// dot3
	if ( ( _stepCounter >= (durDesynch * 3.0f )) && ! _dot3Started )
	{
		CCActionInterval* seqDot3 = (CCActionInterval*)(CCSequence::actions( 
			CCFadeTo::actionWithDuration( durDotFade, 255 ),
			CCFadeTo::actionWithDuration( durDotFade, 0 ),
			CCFadeTo::actionWithDuration( durDotFadeBreak, 0 ),
			NULL ));

		_dot3->runAction( CCRepeatForever::actionWithAction( seqDot3 )) ;
		_dot3Started = true;
		unschedule( schedule_selector( BillingProcessingAnim::Step ));
	}

}
//--------------------------------------------------------------------------------------------------



//--------------------------------------------------------------------------------------------------
// LevelShotsAnim
//--------------------------------------------------------------------------------------------------
LevelShotsAnim* LevelShotsAnim::Create()
{
	LevelShotsAnim* node = new LevelShotsAnim();
	if ( node )
	{
		node->autorelease();
		node->Init();
		return node;
	}
	CC_SAFE_DELETE(node);
	return NULL;
}
//--------------------------------------------------------------------------------------------------
LevelShotsAnim::LevelShotsAnim()
{
	_shotIndex = 0;
	_deltaTime = 0;
}
//--------------------------------------------------------------------------------------------------
LevelShotsAnim::~LevelShotsAnim()
{
	unschedule( schedule_selector( LevelShotsAnim::Step ));
}
//--------------------------------------------------------------------------------------------------
void LevelShotsAnim::Init()
{
	string dir;
	string fullFile;
	CCPoint pos;
	CCSprite *sprite;
    string displayOrder;
    
	dir.append( "Images/BuyScene/LevelShots/" );
    displayOrder = "fghijklmnopqrstuvwxyzabcde";
    
	pos = Tools::GetScreenMiddle();
	pos.x = 0.0f;
	pos.y = 0.0f;

    for ( int i = 0; i < displayOrder.length(); i++ )
    {
		fullFile.clear();
		fullFile.append( dir );
        fullFile.append( displayOrder.substr(i, 1));
        fullFile.append(".jpg");

		D_LOG( "file %s", fullFile.c_str() );
		sprite = CCSprite::spriteWithFile( fullFile.c_str() );
		sprite->setPosition( pos );
		sprite->setOpacity( 0 );
		addChild( sprite, 20 );
		_spriteContainer.push_back( sprite);
	}


	_spriteContainer.at(0)->runAction( CCFadeTo::actionWithDuration( 0.5f, 255 ));
	schedule( schedule_selector( LevelShotsAnim::Step ));
}
//--------------------------------------------------------------------------------------------------
void LevelShotsAnim::Next()
{
	_shotIndex++;
	if ( ( _shotIndex ) >= _spriteContainer.size() )
		_shotIndex = 0;
}
//--------------------------------------------------------------------------------------------------
cocos2d::CCSprite* LevelShotsAnim::GetLevelShot()
{
	return _spriteContainer.at( _shotIndex );
}
//--------------------------------------------------------------------------------------------------
void LevelShotsAnim::Step( ccTime dt )
{
	_deltaTime += dt;
	if ( _deltaTime > Config::LevelShotChangeDelay )
	{
		_deltaTime = 0.0f;
		SwapLevelShots();
	}
}
//--------------------------------------------------------------------------------------------------
void LevelShotsAnim::SwapLevelShots()
{
	CCSprite *sprite;
	CCSprite *spriteNext;
	float dur;

	dur = 0.5f;
	sprite = GetLevelShot();
	Next();
	spriteNext = GetLevelShot();

	sprite->runAction( CCFadeTo::actionWithDuration( dur, 0 ));
	spriteNext->runAction( CCFadeTo::actionWithDuration( dur, 255 ));
}


//--------------------------------------------------------------------------------------------------
// BuyScene
//--------------------------------------------------------------------------------------------------
CCScene* BuyScene::CreateScene()
{
	CCScene *scene = CCScene::node();
	BuyScene *layer = BuyScene::node();

	scene->setScale( Game::Get()->GetScale() );
	scene->addChild(layer);
	return scene;
}
//--------------------------------------------------------------------------------------------------
void BuyScene::ShowScene()
{
//	LoadingBar::Get()->AddToScene( &BuyScene::DoShowScene, NULL );
	DoShowScene( NULL );
}
//--------------------------------------------------------------------------------------------------
void BuyScene::DoShowScene( void *)
{
	CCScene* pScene = SceneTransition::transitionWithDuration(CreateScene());
	if (pScene)
	{
		CCDirector::sharedDirector()->replaceScene(pScene);
	}
}
//--------------------------------------------------------------------------------------------------
BuyScene::BuyScene()
{
	if ( Tools::GetScreenSize().width != 480 )
	{
		_scaleScreenLayer = ScaleScreenLayer::node();
		_scaleScreenLayer->setPosition( Tools::GetScreenMiddle() );
		addChild( _scaleScreenLayer, 250 );
	}
	_processing = false;
    _gettingPrice = false;
}
//--------------------------------------------------------------------------------------------------
BuyScene::~BuyScene()
{
    if (_gettingPrice)
        Billing::Get()->CancelGetPrice();
    
	removeAllChildrenWithCleanup( true );
    NextLevelScene::Unlock();
}
//--------------------------------------------------------------------------------------------------
bool BuyScene::init()
{
    GSendView( "Buy" );
    GLogEvent("Bill", "Constructed");
    RLOG_S("BILL","CONSTRUCTED");

	if ( !CCLayer::init() )
		return false;

	//--------------------
	// Background
	CCSprite *background = CCSprite::spriteWithFile( "Images/BuyScene/Background.jpg" );
	background->setPosition( Tools::GetScreenMiddle() );
	addChild( background, 1 );


	//--------------------
	// Processing label
	_processingAnim = BillingProcessingAnim::Create();
	_processingAnim->setPosition( Tools::GetScreenMiddleX() + 10.0f, Tools::GetScreenMiddleY() + 35.0f );
	addChild( _processingAnim, 35 );

	//--------------------
	// Menu
	CCMenuItemSprite *buyButton;
	CCMenuItemSprite *mainMenuButton;

	CCSprite* spriteNormal;
	CCSprite* spriteSelected;
	CCSprite* spriteDisabled;

	spriteNormal   = CCSprite::spriteWithFile( Lang::Get()->GetLang( "Images/BuyScene/BuyBt.png" ));
	spriteSelected = CCSprite::spriteWithTexture( spriteNormal->getTexture() );
	spriteDisabled = CCSprite::spriteWithTexture( spriteNormal->getTexture() );
	buyButton		= FXMenuItemSpriteWithAnim::Create( spriteNormal, spriteSelected, spriteDisabled, this, menu_selector( BuyScene::Menu_Buy ));

	spriteNormal   = CCSprite::spriteWithFile( "Images/BuyScene/MainMenuBt.png" );
	spriteSelected = CCSprite::spriteWithTexture( spriteNormal->getTexture() );
	spriteDisabled = CCSprite::spriteWithTexture( spriteNormal->getTexture() );
	mainMenuButton	= FXMenuItemSpriteWithAnim::Create( spriteNormal, spriteSelected, spriteDisabled, this, menu_selector( BuyScene::Menu_MainMenu ));

	mainMenuButton->setPosition( 420.0f, 260.0f );
	buyButton->setPosition( 0.0f, -200.0f );

	CCMenu *menu = CCMenu::menuWithItems( buyButton, mainMenuButton, NULL );
	menu->setPosition( Tools::GetScreenMiddle() );
	addChild( menu, 60 );

    
	//--------------------
	// Level shots
	LevelShotsAnim *levelShots;
	levelShots = LevelShotsAnim::Create();
	levelShots->setPosition( CCPoint( Tools::GetScreenMiddleX() + 10.0f, Tools::GetScreenMiddleY() + 35.0f ));
	addChild( levelShots, 30 );


	//--------------------
	// Description
	CCSprite* description = CCSprite::spriteWithFile( Lang::Get()->GetLang( "Images/BuyScene/Description.png" ));
	description->setPosition( CCPoint( Tools::GetScreenMiddleX(), Tools::GetScreenMiddleY() ));
//    description->setPosition( CCPoint( 0.0f, 0.0f ));
	addChild( description, 50 );


	//--------------------
	// Cameras light
	_camerasLight = CCSprite::spriteWithFile( "Images/BuyScene/CamerasLight.png" );
	_camerasLight->setPosition( Tools::GetScreenMiddle() );
	addChild( _camerasLight, 40 );

	_camerasLightFocused = CCSprite::spriteWithFile("Images/BuyScene/CamerasLightFocused.png");
	_camerasLightFocused->setPosition( Tools::GetScreenMiddle() );
	addChild( _camerasLightFocused, 40 );

	//--------------------
	// Projector and pig
	CCSprite *projectorAndPig = CCSprite::spriteWithFile( "Images/BuyScene/ProjectorAndPiggy.png" );
	projectorAndPig->setPosition( CCPoint( Tools::GetScreenMiddleX(), Tools::GetScreenMiddleY() - 140.0f ));
	addChild( projectorAndPig, 50 );

	SetCameraNormal();

    Billing::Get()->GetExtensionPackPrice( this );
    _gettingPrice = true;
	return true;
}
//--------------------------------------------------------------------------------------------------
void BuyScene::PriceRequestCompleted( void *data, unsigned int len )
{
    _gettingPrice = false;
    
    string price;
    CCSprite *priceTag;
    CCLabelTTF *priceLabel;
    
//    string priceCopy;
//    priceCopy = (const char *)data;
//    int a = priceCopy.find_first_of(" ");
//    price = priceCopy.substr(0,a);
    
    price = (const char *)data;
    
    priceTag = CCSprite::spriteWithFile("Images/BuyScene/PriceTag.png");
    priceLabel = CCLabelTTF::labelWithString( price.c_str(), Config::FBHighScoreFont, 32 );

    D_STRING(price);

    CCPoint labelPos;
    labelPos.x = priceTag->getContentSize().width / 2.0f - 11.5f;
    labelPos.y = priceTag->getContentSize().height / 2.0f - 5.50f;
    
    priceLabel->setPosition(labelPos);
    priceLabel->setColor(Config::ColorBlack);
    priceLabel->setRotation( -32.0f );
    
    CCPoint pos;
    pos.x = Tools::GetScreenMiddle().x - 292.5f;
    pos.y = Tools::GetScreenMiddle().y + 81.0f;
    
    priceTag->setPosition( pos);
    priceTag->addChild( priceLabel );
    addChild( priceTag, 100 );

    AnimerSimpleEffect::Get()->Anim_ZoomIn(priceTag);
    SoundEngine::Get()->PlayEffect( SoundEngine::eSoundLevelNameShow);
}
//--------------------------------------------------------------------------------------------------
void BuyScene::PriceRequestError()
{
    _gettingPrice = false;
}
//--------------------------------------------------------------------------------------------------
void BuyScene::Menu_MainMenu( CCObject* pSender )
{
    Billing::Get()->Cancel();
	MainMenuScene::ShowScene();
}
//--------------------------------------------------------------------------------------------------
void BuyScene::Menu_Buy( CCObject* pSender )
{
    GLogEvent("Bill", "Buy pressed");
    RLOG_S("BILL","BUY_PRESSED");

  	if ( ! _processing )
	{
		_processing = true;
		StartProcessingAnim();
		Billing::Get()->BuyExtensionPack( &BillingDoneCallback, this );
        // might be not started then shoudl be _processing = false
	}
}
//--------------------------------------------------------------------------------------------------
void BuyScene::BillingDoneCallback( void *data )
{
	BuyScene* scene;
	scene = ( BuyScene*) data;

	if ( scene )
	{
		scene->FadeOutProcessingAnim();
		scene->_processing = false;
	}
}
//--------------------------------------------------------------------------------------------------
void BuyScene::StartProcessingAnim()
{
	_processingAnim->StartAnim();
	SetCameraFocused();
}
//--------------------------------------------------------------------------------------------------
void BuyScene::FadeOutProcessingAnim()
{
	_processingAnim->StopAnim();
	SetCameraNormal();
}
//--------------------------------------------------------------------------------------------------
void BuyScene::SetCameraNormal()
{
	float dur;
	dur = 1.0f;

	CCActionInterval* seq = (CCActionInterval*)(CCSequence::actions( 
		CCFadeTo::actionWithDuration( dur, 170 ),
		CCFadeTo::actionWithDuration( dur, 255 ),
		NULL ));

	_camerasLight->stopAllActions();
	_camerasLightFocused->stopAllActions();

	_camerasLightFocused->runAction( CCFadeTo::actionWithDuration( dur / 2.0f, 0 ));
	_camerasLight->runAction( CCRepeatForever::actionWithAction( seq ) );
}
//--------------------------------------------------------------------------------------------------
void BuyScene::SetCameraFocused()
{
	float dur;
	dur = 1.0f;

	CCActionInterval* seq = (CCActionInterval*)(CCSequence::actions( 
		CCFadeTo::actionWithDuration( dur, 170 ),
		CCFadeTo::actionWithDuration( dur, 255 ),
		NULL ));

	_camerasLight->stopAllActions();
	_camerasLightFocused->stopAllActions();

	_camerasLight->runAction( CCFadeTo::actionWithDuration( dur / 2.0f, 0 ));
	_camerasLightFocused->runAction( CCRepeatForever::actionWithAction( seq ) );

}
//--------------------------------------------------------------------------------------------------
