#if !defined (BUILD_EDITOR)  && defined (BUILD_TEST)

#include <vector>
#include <string>
#include "Debug/DebugLogger.h"
#include "Tools.h"
#include "TestViewScene.h"
#include "MainMenuScene.h"
#include "Game.h"
//--------------------------------------------------------------------------------
using namespace std;
//--------------------------------------------------------------------------------
void TestViewScene::ShowScene()
{
	CCDirector::sharedDirector()->replaceScene( CreateScene() );
}
//--------------------------------------------------------------------------------
CCScene* TestViewScene::CreateScene()
{
	CCScene *scene = CCScene::node();
	TestViewScene *layer = TestViewScene::node();

	scene->setScale( Game::Get()->GetScale() );
	scene->addChild( layer );
	return scene;
}
//--------------------------------------------------------------------------------
TestViewScene::TestViewScene()
{
	_displayLabel = NULL;
}
//--------------------------------------------------------------------------------
bool TestViewScene::init()
{
	CCLayer::setIsTouchEnabled( true );

	string mainMenuFile;
	string timeLogFile;
	string debugLogFile;

	TrippleSprite mainMenuSprite;
	TrippleSprite timeLogSprite;
	TrippleSprite debugLogSprite;

	//-----------------
	// Main menu button
	mainMenuFile.append( "Images/Menus/SelectPocket/MainMenu1.png" );

	mainMenuSprite._normal		= CCSprite::spriteWithFile( mainMenuFile.c_str() );
	mainMenuSprite._selected	= CCSprite::spriteWithFile( mainMenuFile.c_str() );
	mainMenuSprite._disable		= CCSprite::spriteWithFile( mainMenuFile.c_str() );


	FXMenuItemSpriteWithAnim *menuItem;

	CCLabelBMFont *timersLabel	= CCLabelBMFont::labelWithString( "Timers",		Config::LevelDebugMenuFont );
	CCLabelBMFont *durationLabel= CCLabelBMFont::labelWithString( "Duration",	Config::LevelDebugMenuFont );
	CCLabelBMFont *testRunLabel	= CCLabelBMFont::labelWithString( "TestRun",	Config::LevelDebugMenuFont );
	CCLabelBMFont *memoryLabel	= CCLabelBMFont::labelWithString( "Memory",		Config::LevelDebugMenuFont );
	CCLabelBMFont *memWarnLabel	= CCLabelBMFont::labelWithString( "Notify",		Config::LevelDebugMenuFont );
	

	FXMenuItemLabel *timersItem		= FXMenuItemLabel::itemWithLabel( timersLabel,	this, menu_selector( TestViewScene::Menu_ShowLog ));
	FXMenuItemLabel *durationItem	= FXMenuItemLabel::itemWithLabel( durationLabel,this, menu_selector( TestViewScene::Menu_ShowLog ));
	FXMenuItemLabel *testRunItem	= FXMenuItemLabel::itemWithLabel( testRunLabel, this, menu_selector( TestViewScene::Menu_ShowLog ));
	FXMenuItemLabel *memoryItem		= FXMenuItemLabel::itemWithLabel( memoryLabel,	this, menu_selector( TestViewScene::Menu_ShowLog ));
	FXMenuItemLabel *memWarnItem	= FXMenuItemLabel::itemWithLabel( memWarnLabel, this, menu_selector( TestViewScene::Menu_ShowLog ));
	
	timersItem->setUserData( (void*) DebugLogger::LogTimers );
	durationItem->setUserData( (void*) DebugLogger::LogTestDuration );
	testRunItem->setUserData( (void*) DebugLogger::LogTestRun );
	memoryItem->setUserData( (void*) DebugLogger::LogMemory );
	memWarnItem->setUserData( (void*) DebugLogger::LogNotifications );

	menuItem	= FXMenuItemSpriteWithAnim::Create( mainMenuSprite, this, menu_selector( TestViewScene::Menu_MainMenu ));

	CCMenu* menu = CCMenu::menuWithItems(  testRunItem, memoryItem, memWarnItem, timersItem, durationItem, menuItem, NULL );
	menu->alignItemsHorizontallyWithPadding( 35.0f );
	menu->setPosition( Tools::GetScreenMiddleX(), Tools::GetScreenMiddleY() + 290.0f ); 

	addChild( menu, 10 );
	return true;
}
//--------------------------------------------------------------------------------
TestViewScene::~TestViewScene()
{
	CCTouchDispatcher::sharedDispatcher()->removeDelegate( this );
	removeAllChildrenWithCleanup( true );
}
//--------------------------------------------------------------------------------
void TestViewScene::Menu_MainMenu( CCObject *sender )
{
	CCTouchDispatcher::sharedDispatcher()->removeDelegate( this );
	MainMenuScene::ShowScene();
}
//--------------------------------------------------------------------------------
void TestViewScene::Menu_ShowLog( CCObject *sender )
{
	const char *file;
	CCNode *node;

	node = (CCNode*)sender;
	file = (const char*) node->getUserData();

	if ( ! unDocumentFileCheckExists( file ) )
		return;

	DisplayFile( file );
}
//--------------------------------------------------------------------------------
void TestViewScene::DisplayFile( const char *filename )
{
	//----------------------
	// Clean up
	if ( _displayLabel && _displayLabel->getParent() )
		_displayLabel->removeFromParentAndCleanup( true );



	//----------------------
	// File operations
	unFile *file;
	file = unDocumentFileOpen( filename, "r" );

	if ( !file )
		return;


	//-------------------
	// Reading file
	typedef vector<string> Strings;
	
	Strings strings;
	string tmpString;
    const int bufferSize = 4096;
	char buffer[bufferSize];

    while( unFileReadString( buffer, bufferSize, file ))
	{
		strings.push_back( buffer );
	}
	unFileClose( file );


	//-----------------------
	// reversing 
	string outStr;
	for ( Strings::reverse_iterator it = strings.rbegin(); it != strings.rend(); it++ )
	{
		outStr.append( *it );
	}

	//-----------------------
	// Display
	CCSize displaySize;
	displaySize.width	= Config::GameSize.width - 100.0f;
	displaySize.height	= Config::GameSize.height - 100.0f;

	_displayLabel =  CCLabelTTF::labelWithString( outStr.substr( 0, 1000 ).c_str(), displaySize, CCTextAlignmentLeft, "Arial", 23 );
	_displayLabel->setPosition( ccp( Tools::GetScreenMiddle().x, Tools::GetScreenMiddleY() - 50.0f ));

	addChild( _displayLabel, 10 );
}
//--------------------------------------------------------------------------------
bool TestViewScene::ccTouchBegan( CCTouch *touch, CCEvent *pEvent )
{
	_firstTouch = Tools::ConvertLocationNoRatio( touch );
	
	if ( _displayLabel )
		_labelStartPos = _displayLabel->getPosition();
	return true;
}
//--------------------------------------------------------------------------------
void TestViewScene::ccTouchMoved( CCTouch *touch, CCEvent *pEvent )
{
	b2Vec2 touchNow;
	float yDelta;
	touchNow = Tools::ConvertLocationNoRatio( touch );
	yDelta = touchNow.y - _firstTouch.y;

	if ( _displayLabel )
		_displayLabel->setPosition( ccp( _labelStartPos.x, _labelStartPos.y + yDelta ));
}
//--------------------------------------------------------------------------------
void TestViewScene::ccTouchEnded( CCTouch *pTouch, CCEvent *pEvent )
{

}
//--------------------------------------------------------------------------------
void TestViewScene::ccTouchCancelled( CCTouch *pTouch, CCEvent *pEvent )
{

}
//--------------------------------------------------------------------------------
void TestViewScene::registerWithTouchDispatcher()
{
	CCTouchDispatcher::sharedDispatcher()->addTargetedDelegate( this, Config::eMousePriority_MainMenu, true );
}
//--------------------------------------------------------------------------------
void TestViewScene::onExit()
{

}
//--------------------------------------------------------------------------------

#endif
