#ifndef __INTROSCENE_H__
#define __INTROSCENE_H__

#include "cocos2d.h"
#include "ScaleScreenLayer.h"
#include "LevelFactory.h"

USING_NS_CC;
using namespace std;

//-------------------------------------------------
// IntroScene
//-------------------------------------------------
class IntroScene : public CCLayer
{
public:
	static CCScene* CreateScene();
	
	LAYER_NODE_FUNC( IntroScene );
	virtual bool init();  
	virtual ~IntroScene();

    virtual bool ccTouchBegan(CCTouch *touch, CCEvent *pEvent);
	virtual void ccTouchMoved(CCTouch *touch, CCEvent *pEvent);
	virtual void ccTouchEnded(CCTouch *touch, CCEvent *pEvent);
	virtual void registerWithTouchDispatcher();

	virtual void onEnter();
	virtual void onEnterTransitionDidFinish();
	virtual void onExit();
    void AnimStop();
    
private:
	IntroScene();
    void IntroSkip();

private:
    CCSprite *_background;
    bool    _ending;

};

#endif 
