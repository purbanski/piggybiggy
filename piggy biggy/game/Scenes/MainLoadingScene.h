#ifndef __MAINLOADINGSCENE_H__
#define __MAINLOADINGSCENE_H__

#include "cocos2d.h"
#include "ScaleScreenLayer.h"
#include "LevelFactory.h"

USING_NS_CC;
using namespace std;

class Level;

//-------------------------------------------------
// MainLoadingScene
//-------------------------------------------------
class MainLoadingScene : public CCLayer
{
public:
	static CCScene* CreateScene();
    static MainLoadingScene* node();

	virtual bool init();  
	virtual ~MainLoadingScene();
	
    virtual bool ccTouchBegan(CCTouch *touch, CCEvent *pEvent);
	virtual void ccTouchMoved(CCTouch *touch, CCEvent *pEvent);
	virtual void ccTouchEnded(CCTouch *touch, CCEvent *pEvent);
	virtual void registerWithTouchDispatcher();

	virtual void onEnterTransitionDidFinish();
    void AnimStop();
    void IntroSkip();
    
private:
	MainLoadingScene();
    
    CCSprite *_background;
    CCSprite *_presents;
    
    bool _ending;
};

#endif 
