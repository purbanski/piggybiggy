#ifndef __SELECTLEVELFROMPOCKETSCENE_H__
#define __SELECTLEVELFROMPOCKETSCENE_H__

#include <sstream>
#include "cocos2d.h"
#include <Box2D/Box2D.h>
#include "pu/ui/SlidingNode.h"
#include "ScaleScreenLayer.h"
#include "LevelFactory.h"
#include "Levels/LevelList.h"

//------------------------------------------------------------------------------------------

using namespace std;
USING_NS_CC;

//------------------------------------------------------------------------------------------
class SelectLevelFromPocketScene : public CCLayer
{
private:
	static void		DoShowScene( void* );

public:
	static void		ShowScene( GameTypes::PocketType pocketEnum );
	static CCScene*	CreateScene( GameTypes::PocketType pocketEnum );

	static SelectLevelFromPocketScene* node( GameTypes::PocketType pocketEnum );

	virtual bool init();  
	virtual ~SelectLevelFromPocketScene();

private :
	SelectLevelFromPocketScene( GameTypes::PocketType pocketEnum );
	CCNode* ConstructSlidingMenu();
	CCMenu* ConstructMainMenu();

	void Menu_SelectLevel( CCObject *sender );
	void Menu_MainMenu( CCObject *sender );
	void Menu_SelectPocket( CCObject *sender );

	void SetIndex();

private:
	SlidingNode			*_slidingNode;
	ScaleScreenLayer	*_scaleScreenLayer;
	GameTypes::PocketType 	_pocketEnum;
};
//------------------------------------------------------------------------------------------
#endif 
