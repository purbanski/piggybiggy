#include <list>
#include "unDebug.h"
#include <Box2D/Box2D.h>

#include "MainMenuScene.h"
#include "SelectLevelFromAll.h"
#include "SelectLevelFromPocketScene.h"

#include "Levels/LevelList.h"
#include "SoundEngine.h"
#include "Tools.h"
#include "Debug/MyDebug.h"
#include "Transitions.h"
#include "Game.h"
#include "GameGlobals.h"
#include "Skins.h"
#include "LoadingBar.h"

USING_NS_CC;
using namespace std;

//--------------------------------------------------------------------------------------------------
SelectLevelFromAll* SelectLevelFromAll::node( SelectLevel_SelectType sceneType )
{
	SelectLevelFromAll* pRet = new SelectLevelFromAll( sceneType );
	if ( pRet && pRet->init() )
	{
		pRet->autorelease();
		return pRet;
	}
	else
	{
		delete pRet;
		pRet = NULL;
		return NULL;
	} 
}
//--------------------------------------------------------------------------------------------------
CCScene* SelectLevelFromAll::CreateScene( SelectLevel_SelectType sceneType )
{
	CCScene *scene = CCScene::node();
	SelectLevelFromAll *layer = SelectLevelFromAll::node( sceneType );
	layer->setPosition( Tools::GetScreenMiddle() );
	scene->addChild( layer );
	return scene;
}
//--------------------------------------------------------------------------------------------------
void SelectLevelFromAll::ShowScene( SelectLevel_SelectType sceneType )
{
	LoadingBar::Get()->AddToScene( &SelectLevelFromAll::DoShowScene, (void*) sceneType );
}

//--------------------------------------------------------------------------------------------------
void SelectLevelFromAll::DoShowScene( void *data )
{
	SelectLevel_SelectType sceneType;
	sceneType = ( SelectLevel_SelectType ) (long) data;

	CCScene* pScene = LevelToLevelTransition::transitionWithDuration( 0.8f, CreateScene( sceneType )); 
	if (pScene)
	{
		SoundEngine::Get()->PlayEffect( SoundEngine::eSoundChangeScene );
		CCDirector::sharedDirector()->replaceScene( pScene );
	}
}
//--------------------------------------------------------------------------------------------------
SelectLevelFromAll::SelectLevelFromAll( SelectLevel_SelectType sceneType )
{
	int slideSize;

#ifdef FREE_VERSION
	slideSize = 4;
#else 
	slideSize = 25;
#endif

	_slidingNode = SlidingNode::node( slideSize );
	_slidingNode->addChild( ConstructSlidingMenu( sceneType ), 10 );
	_slidingNode->SetInitPosition( ccp ( 0.0f, 0.0f ) );
	addChild( _slidingNode, 10 );
	addChild( ConstructMainMenu(), 15 );

	//if ( Tools::GetScreenSize().width != 480 )
	//{
	//_scaleScreenLayer = ScaleScreenLayer::node();
	//_scaleScreenLayer->setPosition( ccp(0.0f, 0.0f) );
	//addChild( _scaleScreenLayer, 50 );
	//}

	SetIndex( Game::Get()->GetGameStatus()->GetCurrentLevel() );
	AddBackground();
	setPosition( ccp( 0.0f, 0.0f ));

}
//--------------------------------------------------------------------------------------------------
SelectLevelFromAll::~SelectLevelFromAll()
{
	removeAllChildrenWithCleanup( true );
}
//--------------------------------------------------------------------------------------------------
bool SelectLevelFromAll::init()
{
	if ( !CCLayer::init()  )
		return false;

	return true;
}
//--------------------------------------------------------------------------------------------------
void SelectLevelFromAll::AddBackground()
{
	CCSprite* pSprite = CCSprite::spriteWithFile( "Editor/Images/Other/bg.png" );
	pSprite->setPosition( ccp( 0.0f , 0.0f));

	addChild(pSprite, 0);
}
//-----------------------------------------------------------------------------------
void SelectLevelFromAll::SetIndex( GameTypes::LevelEnum level )
{
	list<LevelList *>::iterator it;
	int count = 0;
	for ( it = _levelLists.begin(); it != _levelLists.end(); it++ )
	{
		if ((*it)->IsInList( level ))
		{
			_slidingNode->SetIndex( count / 2 );
			return;
		}
		count++;
	}
	_slidingNode->SetIndex( 0 );
}
//-----------------------------------------------------------------------------------
CCMenuItem* SelectLevelFromAll::GetMenuItem( GameTypes::LevelEnum level )
{
	GameStatus *status;
	stringstream ss;
	stringstream levelName;

	float xLabel = 60.0f;
	float yLabel = -20.0f;

	status =  Game::Get()->GetGameStatus();
	
	levelName << GameGlobals::Get()->LevelNames()[ level ];
	levelName << "\n" << "Enum: " << level;

	CCLabelBMFont	*label;
	label = CCLabelBMFont::labelWithString( levelName.str().c_str(), Config::LevelNameFont );
	label->setPosition( ccp( xLabel, yLabel ));

	CCLabelBMFont	*labelSelected;
	labelSelected = CCLabelBMFont::labelWithString( levelName.str().c_str(),  Config::LevelNameFont );
	labelSelected->setPosition( ccp( xLabel, yLabel ));

	CCLabelBMFont	*labelDisabled;
	labelDisabled = CCLabelBMFont::labelWithString( levelName.str().c_str(),  Config::LevelNameFont );
	labelDisabled->setPosition( ccp( xLabel, yLabel ));

	CCSprite *spriteNormal;
	CCSprite *spriteSelected;
	CCSprite *spriteDisabled;

	switch ( Game::Get()->GetGameStatus()->GetLevelStatus( level ))
	{
	case GameStatus::eLevelStatus_LastOpen :
	case GameStatus::eLevelStatus_Locked :
		spriteNormal	= CCSprite::spriteWithFile( "Images/Menus/SelectLevel/PiggyUndone.png" );
		spriteSelected  = CCSprite::spriteWithFile( "Images/Menus/SelectLevel/PiggyUndone.png" );
		spriteDisabled  = CCSprite::spriteWithFile( "Images/Menus/SelectLevel/PiggyUndone.png" );
		break;

	case GameStatus::eLevelStatus_Done :
		spriteNormal	= CCSprite::spriteWithFile( "Images/Menus/SelectLevel/PiggyCompleted.png" );
		spriteSelected  = CCSprite::spriteWithFile( "Images/Menus/SelectLevel/PiggyCompleted.png" );
		spriteDisabled  = CCSprite::spriteWithFile( "Images/Menus/SelectLevel/PiggyCompleted.png" );
		break;

	case GameStatus::eLevelStatus_Skipped :
		spriteNormal	= CCSprite::spriteWithFile( "Images/Menus/SelectLevel/PiggySkipped.png" );
		spriteSelected  = CCSprite::spriteWithFile( "Images/Menus/SelectLevel/PiggySkipped.png" );
		spriteDisabled  = CCSprite::spriteWithFile( "Images/Menus/SelectLevel/PiggySkipped.png" );
		break;

	default:
		unAssertMsg( LevelList, false, ("Unknown level status %d", Game::Get()->GetGameStatus()->GetLevelStatus( level )));

	}

	spriteNormal->addChild( label );
	spriteSelected->addChild( labelSelected );
	spriteDisabled->addChild( labelDisabled );


	/*if ( status->GetStatus()->_statusTable[ level ] == GameStatus::eLevelStatus_Done )
	{
		string completeTime;

		int days	= status->GetStatus()->_scoreTable[ level ].GetDaysCount() ;
		int hours	= status->GetStatus()->_scoreTable[ level ].GetHoursCount() ;
		int minutes	= status->GetStatus()->_scoreTable[ level ].GetMinutesCount() ;
		int seconds	= status->GetStatus()->_scoreTable[ level ].GetSecondsCount() ;

		stringstream ss;

		ss << "Time:\n";
		ss << hours ;

		ss << ":";

		if ( minutes < 10 )
			ss << 0;
		ss << minutes ;

		ss << ":";

		if ( seconds < 10 )
			ss << 0;
		ss << seconds ;


		CCLabelBMFont	*scoreLabel;
		scoreLabel = CCLabelBMFont::labelWithString( ss.str().c_str(), Config::LevelNameFont );
		scoreLabel->setPosition( ccp( xLabel, 80.0f ));

		spriteNormal->addChild( scoreLabel  );
	}*/

	CCMenuItemSprite *menuItem;
	menuItem = CCMenuItemSprite::itemFromNormalSprite( spriteNormal, spriteSelected, spriteDisabled, this, menu_selector( SelectLevelFromAll::Menu_SelectLevel ));
	menuItem->setUserData( (void *) level );

	if ( Game::Get()->GetGameStatus()->GetLevelStatus( level ) == GameStatus::eLevelStatus_Locked )
		menuItem->setIsEnabled( false );

	return menuItem;
}
//-----------------------------------------------------------------------------------
void SelectLevelFromAll::Menu_SelectLevel( CCObject* pSender )
{
	long levelEnum;

	CCMenuItem *menuItem;
	menuItem = (CCMenuItem*) pSender;

	levelEnum = (long) menuItem->getUserData();

#ifdef BUILD_EDITOR
	CCDirector::sharedDirector()->popScene();
#endif
	Game::Get()->CreateAndPlayLevel( (GameTypes::LevelEnum) levelEnum, Game::eLevelToLevel );
}
//-----------------------------------------------------------------------------------
CCNode* SelectLevelFromAll::ConstructSlidingMenu( SelectLevel_SelectType sceneType )
{
	CCNode *node;
	node = NULL;

	switch ( sceneType )
	{
	case SelectLevelFromAll::eSelectLevel_FromAll :
		node = ConstructSlidingMenu_All();
		break;

	case SelectLevelFromAll::eSelectLevel_FromList :
		node = ConstructSlidingMenu_List();
		break;

	case SelectLevelFromAll::eSelectLevel_FromUndone :
		node = ConstructSlidingMenu_Undone();
		break;

	default:
		unAssertMsg(SelectLevelFromAll, false, ("Undefined type %d", sceneType ));
	}

	return node;
}
//-----------------------------------------------------------------------------------
CCMenu* SelectLevelFromAll::ConstructMainMenu()
{
	CCMenu* menu;
	CCMenuItemLabel *itemSelectList;
	CCMenuItemLabel *itemSelectPockets;
	CCMenuItemLabel *itemSelectAll;
	CCMenuItemLabel *itemSelectUndone;
	CCLabelBMFont	*label;

	label = CCLabelBMFont::labelWithString( "Lists", Config::SelectLevelMenuFont );
	itemSelectList = CCMenuItemLabel::itemWithLabel( label, this, menu_selector( SelectLevelFromAll::Menu_SelectLevelFromList ));

	label = CCLabelBMFont::labelWithString( "All", Config::SelectLevelMenuFont );
	itemSelectAll = CCMenuItemLabel::itemWithLabel( label, this, menu_selector( SelectLevelFromAll::Menu_SelectLevelFromAll ));

	label = CCLabelBMFont::labelWithString( "Pockets", Config::SelectLevelMenuFont );
	itemSelectPockets = CCMenuItemLabel::itemWithLabel( label, this, menu_selector( SelectLevelFromAll::Menu_SelectPocket ));

	label = CCLabelBMFont::labelWithString( "Undone", Config::SelectLevelMenuFont );
	itemSelectUndone = CCMenuItemLabel::itemWithLabel( label, this, menu_selector( SelectLevelFromAll::Menu_SelectUndone ));

	menu = CCMenu::menuWithItems( itemSelectPockets, itemSelectAll, itemSelectList, itemSelectUndone, NULL );
	menu->alignItemsHorizontallyWithPadding( 45.0f );
	menu->setPosition( ccp( -70.0f, 275.0f ));  //ipad iphone fixme
	return menu;
}
//-----------------------------------------------------------------------------------
void SelectLevelFromAll::Menu_SelectLevelFromList( CCObject *sender )
{
	CCTouchDispatcher::sharedDispatcher()->removeDelegate( this );
	SelectLevelFromAll::ShowScene( SelectLevelFromAll::eSelectLevel_FromList );
}
//-----------------------------------------------------------------------------------
void SelectLevelFromAll::Menu_SelectUndone( CCObject *sender )
{
	CCTouchDispatcher::sharedDispatcher()->removeDelegate( this );
	SelectLevelFromAll::ShowScene( SelectLevelFromAll::eSelectLevel_FromUndone );
}
//-----------------------------------------------------------------------------------
void SelectLevelFromAll::Menu_SelectPocket( CCObject *sender )
{
	CCTouchDispatcher::sharedDispatcher()->removeDelegate( this );
	SelectLevelFromPocketScene::ShowScene( Game::Get()->GetGameStatus()->GetCurrentPocket() );
}
//-----------------------------------------------------------------------------------
void SelectLevelFromAll::Menu_SelectLevelFromAll( CCObject *sender )
{
	CCTouchDispatcher::sharedDispatcher()->removeDelegate( this );
	SelectLevelFromAll::ShowScene( SelectLevelFromAll::eSelectLevel_FromAll );
}
//-----------------------------------------------------------------------------------
CCNode* SelectLevelFromAll::ConstructSlidingMenu_List()
{
	CCNode *node;
	node = CCNode::node();

	int countX;
	int countY;
	float deltaX;
	float deltaY;
	LevelListList::iterator it;

	_levelLists.push_back( new LevelList_JacekZ() );
	_levelLists.push_back( new LevelList_Marzena() );
	_levelLists.push_back( new LevelList_TomekW() );
	_levelLists.push_back( new LevelList_Bogusia() );
	_levelLists.push_back( new LevelList_Pawel1() );
	_levelLists.push_back( new LevelList_Pawel2() );
	_levelLists.push_back( new LevelList_Pawel3() );

	_levelLists.push_back( new LevelList_Rafal1() );
	_levelLists.push_back( new LevelList_Rafal2() );
	_levelLists.push_back( new LevelList_Test() );

	_levelLists.push_back( new LevelList_Intro() );
	_levelLists.push_back( new LevelList_Synchronize() );
	_levelLists.push_back( new LevelList_Magnets() );
	_levelLists.push_back( new LevelList_Others() );
	_levelLists.push_back( new LevelList_Bombs1() );
	_levelLists.push_back( new LevelList_Bombs2() );
	_levelLists.push_back( new LevelList_Roll() );
	_levelLists.push_back( new LevelList_Swing() );
	_levelLists.push_back( new LevelList_Chain1() );
	_levelLists.push_back( new LevelList_Chain2() );
	_levelLists.push_back( new LevelList_Other() );
	_levelLists.push_back( new LevelList_Tetris() );
	_levelLists.push_back( new LevelList_Domino() );
	_levelLists.push_back( new LevelList_Construction_1() );
	_levelLists.push_back( new LevelList_Construction_2() );
	_levelLists.push_back( new LevelList_Gun() );
	_levelLists.push_back( new LevelList_Reel() );
	_levelLists.push_back( new LevelList_Bank() );
	_levelLists.push_back( new LevelList_Rotate1() );
	_levelLists.push_back( new LevelList_Rotate2() );
	_levelLists.push_back( new LevelList_Labyrinth() );
	_levelLists.push_back( new LevelList_Vehical() );

	countX = 0;
	countY = 1;

	deltaX = Config::GameSize.width;
	deltaY = 170.0f;

	float x;
	float y;


	for ( it = _levelLists.begin(); it != _levelLists.end(); it++ )
	{
		(*it)->autorelease();

		x = deltaX * (int) ( countX / 2 );
		y = ( countY * deltaY );

		node->addChild( *it, 10 );
		(*it)->setPosition( ccp( x, y ));

		countX++;
		countY *= -1 ;
	}

	return node;
}
//-----------------------------------------------------------------------------------
CCNode* SelectLevelFromAll::ConstructSlidingMenu_All()
{
	int count;
	int countY;
	float deltaX;
	float deltaY;
	int breakRow;
	int screenCount;

	float x;
	float y;

	SlidingNodeMenu *menu;
	CCMenuItem *item;
	GameLevelSequence levels;

	count = 0;
	countY = 1;

	breakRow = 5;

	x = 0.0f;
	y = 0.0f;
	screenCount = 0;

	deltaY = 150.0f * 2.0f;
	deltaX = 90.0f * 2.0f;

	menu = SlidingNodeMenu::menuWithItems( kCCMenuTouchPriority, _slidingNode, NULL, NULL );

	for ( GameTypes::LevelEnum level = GameTypes::eLevel_Start; level != GameTypes::eLevel_Finish; level = ( GameTypes::LevelEnum ) (((int)level ) + 1 ))
	{
		item = GetMenuItem( level );
		item->setPosition( ccp( x, y ));
		menu->addChild( item, 10 );

		if ( (count % breakRow ) == ( breakRow - 1 ))
		{
			if ( y == 0.0f )
			{
				y -= deltaY;
				x = (float) screenCount * Config::GameSize.width / 1.0f;
			}
			else
			{
				y = 0.0f;
				screenCount++;
				x = (float) screenCount * Config::GameSize.width / 1.0f;
			}

		}
		else
			x += deltaX;

		count++;
	}

	menu->setPosition( ccp( -360.0f, 150.0f ));
	return menu;
}
//-----------------------------------------------------------------------------------
CCNode* SelectLevelFromAll::ConstructSlidingMenu_Undone()
{
	
	int count;
	int countY;
	float deltaX;
	float deltaY;
	int breakRow;
	int screenCount;

	float x;
	float y;

	SlidingNodeMenu *menu;
	CCMenuItem *item;
	GameLevelSequence levels;

	count = 0;
	countY = 1;

	breakRow = 5;

	x = 0.0f;
	y = 0.0f;
	screenCount = 0;

	deltaY = 150.0f * 2.0f;
	deltaX = 90.0f * 2.0f;

	menu = SlidingNodeMenu::menuWithItems( kCCMenuTouchPriority, _slidingNode, NULL, NULL );

	GameLevelSequence gameLevels;
	GameLevelSequence allLevels;
	GameLevelSequence undoneLevels;
	bool undone;

	//gameLevels.clear();
	undoneLevels.clear();
	allLevels.clear();

	//for ( GameLevelSequence::iterator it = )
	//Game::Get()->GetGameStatus()->GetLevelSequence();
	//gameLevels = Game::Get()->GetGameStatus()->GetLevelSequence();
	//= Game::Get()->GetGameStatus()->GetLevelSequence();
	//return CCNode::node();
	for ( int i = (int) GameTypes::eLevel_Start; i < (int) GameTypes::eLevel_Finish; i++ )
		allLevels.push_back( (GameTypes::LevelEnum) i );
	
	D_SIZE( gameLevels ) 
	D_SIZE( allLevels )
	D_SIZE( undoneLevels )
	
	//	gameLevels.Dump();	
	// allLevels.Dump();
	
	//--------------------------------
	// Get which are not yet in the game sequence levels
	for ( GameLevelSequence::iterator allLevel = allLevels.begin(); allLevel != allLevels.end(); allLevel++ )
	{
		undone = true;
		for ( GameLevelSequence::iterator gameLevel = gameLevels.begin(); gameLevel != gameLevels.end(); gameLevel++ )
		{
			if ( (*gameLevel) ==  (*allLevel) ) 
			{
				undone = false; 
				break;
			}
		}
		if ( undone )
			undoneLevels.push_back( *allLevel );
	}

	D_SIZE( undoneLevels )
		
	//---------------------------------
	// Construct menu
	for ( GameLevelSequence::iterator level = undoneLevels.begin(); level != undoneLevels.end(); level++ )
	{
		item = GetMenuItem( *level );
		item->setPosition( ccp( x, y ));
		menu->addChild( item, 10 );

		if ( (count % breakRow ) == ( breakRow - 1 ))
		{
			if ( y == 0.0f )
			{
				y -= deltaY;
				x = (float) screenCount * Config::GameSize.width / 1.0f;
			}
			else
			{
				y = 0.0f;
				screenCount++;
				x = (float) screenCount * Config::GameSize.width / 1.0f;
			}

		}
		else
			x += deltaX;

		count++;
	}

	menu->setPosition( ccp( -360.0f, 150.0f ));
	return menu;
}
//-----------------------------------------------------------------------------------
