#ifndef __piggy_biggy__MarmaladeScene__
#define __piggy_biggy__MarmaladeScene__

#include "cocos2d.h"
#include "ScaleScreenLayer.h"
#include "LevelFactory.h"

USING_NS_CC;
using namespace std;

class Level;

//-------------------------------------------------
// MainLoadingScene
//-------------------------------------------------
class MarmaladeScene : public CCLayer
{
public:
	static CCScene* CreateScene();
    static MarmaladeScene* node();

	virtual bool init();  
	virtual ~MarmaladeScene();
	
    virtual bool ccTouchBegan(CCTouch *touch, CCEvent *pEvent);
	virtual void ccTouchMoved(CCTouch *touch, CCEvent *pEvent);
	virtual void ccTouchEnded(CCTouch *touch, CCEvent *pEvent);
	virtual void registerWithTouchDispatcher();

	virtual void onEnterTransitionDidFinish();

private:
    void Skip();
    void NextScene();

    
private:
	MarmaladeScene();
    
    CCSprite *_background;
    
    bool _ending;
};

#endif 
