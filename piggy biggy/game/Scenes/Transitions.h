#ifndef __TRANSITIONS_H__
#define __TRANSITIONS_H__

#include "cocos2d.h"

USING_NS_CC;
//----------------------------------------------------------------------------------------------------------
class SceneTransition : public CCTransitionFadeTR
{
public:
	static SceneTransition* transitionWithDuration(CCScene *scene );
	virtual void onExit();
};
//----------------------------------------------------------------------------------------------------------
class LevelToLevelTransition : public CCTransitionFade
{
public:
	static LevelToLevelTransition* transitionWithDuration(ccTime duration, CCScene *scene );
	virtual void onExit();
};
//----------------------------------------------------------------------------------------------------------
class LevelCrossFadeTransition : public CCTransitionCrossFade
{
public:
	static LevelCrossFadeTransition* transitionWithDuration(ccTime duration, CCScene *scene );
	virtual void onExit();
};
//----------------------------------------------------------------------------------------------------------
class LevelRestartTransition : public CCTransitionFade
{
public:
	static LevelRestartTransition* transitionWithDuration(ccTime duration, CCScene *scene, const ccColor3B& color );
	virtual void onExit();
};
//----------------------------------------------------------------------------------------------------------
class TransitionScene1 : public CCTransitionPageTurn
{
public:
	static TransitionScene1* transitionWithDuration( CCScene *scene );
	virtual void onExit();
};
//----------------------------------------------------------------------------------------------------------


#endif
