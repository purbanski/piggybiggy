#include <list>
#include <Box2D/Box2D.h>

#include "SelectPocketScene.h"
#include "SelectLevelFromPocketScene.h"
#include "MainMenuScene.h"
#include "Levels/LevelList.h"
#include "SoundEngine.h"
#include "Tools.h"
#include "Debug/MyDebug.h"
#include "Transitions.h"
#include "Game.h"
#include "LoadingBar.h"
#include "Levels/Pockets.h"
#include "Components/FXMenuItem.h"
#include "Components/CustomSprites.h"
#include "RunLogger/GLogger.h"
#include "RunLogger/RunLogger.h"
#include "Platform/Lang.h"

USING_NS_CC;
using namespace std;

//--------------------------------------------------------------------------------------------------
CCScene* SelectPocketScene::CreateScene()
{
	CCScene *scene = CCScene::node();
	SelectPocketScene *layer = SelectPocketScene::node();
	layer->setPosition( Tools::GetScreenMiddle() );
	scene->addChild(layer);
	return scene;
}
//--------------------------------------------------------------------------------------------------
void SelectPocketScene::ShowScene()
{
//    DoShowScene(NULL);
	LoadingBar::Get()->AddToScene( &SelectPocketScene::DoShowScene, NULL );
}
//--------------------------------------------------------------------------------------------------
void SelectPocketScene::DoShowScene( void * )
{
	CCScene* pScene = SceneTransition::transitionWithDuration( CreateScene() );
	if (pScene)
	{
		SoundEngine::Get()->PlayEffect( SoundEngine::eSoundChangeScene );
		CCDirector::sharedDirector()->replaceScene( pScene );
	}
}
//--------------------------------------------------------------------------------------------------
SelectPocketScene::SelectPocketScene()
{
	_slidingNode = SlidingNode::node( 5 );
	_slidingNode->SetListener( this );
	_slidingNode->SetSlideLength( Config::GameSize.width / 2.0f );
	_slidingNode->addChild( ConstructSlidingMenu(), 10 );
	_slidingNode->SetInitPosition( ccp ( 0.0f, 0.0f ) );
	addChild( _slidingNode, 10 );

	//addChild( ConstructMainMenu(), 15 );

	if ( Tools::GetScreenSize().width != 480 )
	{
		_scaleScreenLayer = ScaleScreenLayer::node();
		_scaleScreenLayer->setPosition( ccp(0.0f, 0.0f) );
		addChild( _scaleScreenLayer, 50 );
	}

	int index;
	index = Game::Get()->GetGameStatus()->GetCurrentPocket();
	_slidingNode->SetIndex( index - 1 );

	_background = NULL;
	_statsNode = NULL;

	_lastPocketDisplayed = GameTypes::ePocketNone;
}
//--------------------------------------------------------------------------------------------------
SelectPocketScene::~SelectPocketScene()
{
	removeAllChildrenWithCleanup( true );
}
//--------------------------------------------------------------------------------------------------
bool SelectPocketScene::init()
{
    GSendView("Select Pocket");
    
	Preloader loader;

	if ( !CCLayer::init()  )
		return false;

	// Preloading button icons 
	loader.AddImageDir( "Images/Menus/SelectPocket" );
	loader.Preload();

	// Preloading Backgrounds
	for ( int i = GameTypes::ePocketBlue; i <= GameTypes::ePocketScience; i++ )
	{
		PocketManager::Get()->GetSelectPocketBackground( (GameTypes::PocketType) i );
	}
	

	SlidingNode_SlideFinish();
	return true;
}
//--------------------------------------------------------------------------------------------------
void SelectPocketScene::SetIndex( GameTypes::LevelEnum level )
{
	list<LevelList *>::iterator it;
	int count = 0;
	for ( it = _levelLists.begin(); it != _levelLists.end(); it++ )
	{
		if ((*it)->IsInList( level ))
		{
			_slidingNode->SetIndex( count / 2 );
			return;
		}
		count++;
	}
	_slidingNode->SetIndex( 0 );
}
//-----------------------------------------------------------------------------------
void SelectPocketScene::Menu_MainMenu( CCObject *sender )
{
#ifdef BUILD_EDITOR
	return;
#endif
	CCTouchDispatcher::sharedDispatcher()->removeDelegate( this );
	MainMenuScene::ShowScene();
}
//-----------------------------------------------------------------------------------
CCMenu* SelectPocketScene::ConstructMainMenu( GameTypes::PocketType pocket )
{
	CCMenu* menu;
	TrippleSprite triSprite;
	string spriteFile;

	spriteFile.append( "Images/Menus/SelectPocket/MainMenu" );

	switch ( pocket )
	{
	case GameTypes::ePocketBlue :
		spriteFile.append( "1.png" );
		break;

	case GameTypes::ePocketMoro :
		spriteFile.append( "2.png" );
		break;

	case GameTypes::ePocketRasta :
		spriteFile.append( "3.png" );
		break;

	case GameTypes::ePocketJapan :
		spriteFile.append( "4.png" );
		break;

	case GameTypes::ePocketScience :
		spriteFile.append( "5.png" );
		break;

	default:
		unAssertMsg(Game, false, ("Wrong packet %d, switching to blue "));
		spriteFile.append( "1.png" );
	}
	triSprite._normal	= CCSprite::spriteWithFile( spriteFile.c_str() );
	triSprite._selected = CCSprite::spriteWithFile( spriteFile.c_str() );
	triSprite._disable	= CCSprite::spriteWithFile( spriteFile.c_str() );


	FXMenuItemSpriteWithAnim *menuItem;
	menuItem = FXMenuItemSpriteWithAnim::Create( triSprite, this, menu_selector( SelectPocketScene::Menu_MainMenu ));


	menu = CCMenu::menuWithItems( menuItem, NULL );
	menu->alignItemsHorizontallyWithPadding( 25.0f );
	menu->setPosition( Config::GameSize.width - 50.0f, Config::GameSize.height - 60.0f ); 
	return menu;
}
//-----------------------------------------------------------------------------------
CCNode* SelectPocketScene::ConstructSlidingMenu()
{
	CCNode *node;
	node = CCNode::node();

	int countX;
	int countY;
	float deltaX;
	float deltaY;

	countX = 0;
	countY = 1;

	deltaX = Config::GameSize.width / 2.0f;
	deltaY = -50.0f;

	float x;
	float y;

	SlidingNodeMenu *menu;
	menu = SlidingNodeMenu::menuWithItems( kCCMenuTouchPriority, _slidingNode, NULL, NULL );
	menu->setPosition( CCPointZero );

	CCMenuItem *menuItem;
	PocketManager::PocketList pocketList;
	PocketManager::PocketList::iterator it;
	
	pocketList = PocketManager::Get()->GetPocketList();

	for ( it = pocketList.begin(); it != pocketList.end(); it++ )
	{
		x = deltaX * (int) ( countX );
		y = deltaY;

		menuItem = GetPocketItem( *it );
		menuItem->setPosition( ccp( x, y ));
		menu->addChild( menuItem, 10 );

		countX++;
	}

	return menu;
}
//-----------------------------------------------------------------------------------
CCMenuItem* SelectPocketScene::GetPocketItem( GameTypes::PocketType pocket )
{
	CCSprite *spriteNormal;
	CCSprite *spriteSelected;
	CCSprite *spriteDisabled;

	spriteNormal   = PocketManager::Get()->GetSpritePocketSelect( pocket );
	spriteSelected = PocketManager::Get()->GetSpritePocketSelect( pocket );
	spriteDisabled = PocketManager::Get()->GetSpritePocketSelectLocked( pocket );
	
	float duration;
	duration = 0.5f;

	CCRepeatForever *repeat = CCRepeatForever::actionWithAction(
		(CCActionInterval *)
		CCSequence::actions( 
			CCScaleTo::actionWithDuration( duration, 0.96f, 1.04f),
			CCScaleTo::actionWithDuration( duration, 1.04f, 0.96f),
			NULL ));
    spriteSelected->setAnchorPoint(CCPointZero);
	spriteSelected->runAction( repeat );


	FXMenuItemSprite *menuItem;
	menuItem = FXMenuItemSprite::itemFromNormalSprite( spriteNormal, spriteSelected, spriteDisabled, this, menu_selector( SelectPocketScene::Menu_SelectPocket ));

	if ( Game::Get()->GetGameStatus()->GetPocketStatus( pocket ) == GameTypes::ePocketStatusLocked )
		menuItem->setIsEnabled( false );
	else
		menuItem->setIsEnabled( true );


	menuItem->setUserData( (void *)pocket );
	return menuItem;
}
//-----------------------------------------------------------------------------------
void SelectPocketScene::Menu_SelectPocket( CCObject *sender )
{
	CCNode *node;
	node = (CCNode*) sender;

	GameTypes::PocketType pocketEnum;
	pocketEnum = (GameTypes::PocketType)((long)node->getUserData());

	SelectLevelFromPocketScene::ShowScene( pocketEnum );
	Game::Get()->GetGameStatus()->SetCurrentPocket( pocketEnum );
}
//-----------------------------------------------------------------------------------
void SelectPocketScene::SlidingNode_SlideFinish()
{
	GameTypes::PocketType pocket;
	pocket = GetDisplayedPocketEnum();

	if ( _lastPocketDisplayed != pocket )
	{
		_lastPocketDisplayed = pocket;
		BackgroundChange();

		if ( Game::Get()->GetGameStatus()->GetPocketStatus( pocket ) != GameTypes::ePocketStatusLocked )
			ShowStats();
		else
        {
            RLOG_SS("POCKET_MENU", GetDisplayedPocketEnum(), "LOCKED" );
			HideStats();
        }
	}
}
//-----------------------------------------------------------------------------------
void SelectPocketScene::CleanOldBackground(CCNode* pTarget, void* data)
{
	CCSprite *oldBg;
	oldBg = (CCSprite* ) data;

	if ( oldBg && oldBg->getParent() )
	{
		oldBg->removeFromParentAndCleanup( true );
		D_LOG("Old background removed")
	}
}
//-----------------------------------------------------------------------------------
void SelectPocketScene::CleanOldStats( CCNode* pTarget, void* data )
{
	CCNode *oldStats;
	oldStats = (CCNode* ) data;

	if ( oldStats && oldStats->getParent() )
	{
		oldStats->removeFromParentAndCleanup( true );
		D_LOG("Old stats removed")
	}
}
//-----------------------------------------------------------------------------------
void SelectPocketScene::BackgroundChange()
{
	float duration;
	GameTypes::PocketType pocketEnum;

	pocketEnum = GetDisplayedPocketEnum();
	duration = 0.8f;

	if ( _background )
	{
		CCFiniteTimeAction*  fadeOutAndClean = CCSequence::actions(
			CCFadeTo::actionWithDuration( duration, 0 ),
			CCCallFuncND::actionWithTarget(this, callfuncND_selector(SelectPocketScene::CleanOldBackground), _background ), 
			NULL);
		_background->stopAllActions();
		_background->runAction( fadeOutAndClean );
	}

	CCFadeTo *fadeIn = CCFadeTo::actionWithDuration( duration, 255 );

	_background = PocketManager::Get()->GetSelectPocketBackground( pocketEnum );
	_background->setOpacity( 0 );

	addChild( _background, 3 );
	_background->runAction( fadeIn );

	CCMenu *buttonMenu = ConstructMainMenu( pocketEnum );
	buttonMenu->runAction( CCFadeTo::actionWithDuration( duration, 255 ));
	
	buttonMenu->setOpacity( 0 );
	_background->addChild( buttonMenu );
}
//-----------------------------------------------------------------------------------
void SelectPocketScene::ShowStats()
{
	float duration;
	duration = 0.8f;

	HideStats();

	_statsNode = GetStatsNode();
	_statsNode->setOpacity( 0 );
	_statsNode->setPosition( ccp( 0.0f + 280.0f, 490.0f ));

	addChild( _statsNode, 20 );
	
	CCFiniteTimeAction*  scaleIn = CCSequence::actions(
		CCMoveBy::actionWithDuration( duration / 2.0f, CCPointZero ), 
		CCFadeTo::actionWithDuration( duration / 4.0f, 255 ),
		CCMoveBy::actionWithDuration( duration / 4.0f, CCPointZero ), 
		
		CCMoveBy::actionWithDuration( duration / 2.0f, CCPoint( 0.0f, -_statsNode->getContentSizeInPixels().height - 25.0f )),
		NULL);

	_statsNode->stopAllActions();
	_statsNode->runAction( scaleIn );
	//_statsNode->runAction( fadeIn );
}
//-----------------------------------------------------------------------------------
void SelectPocketScene::HideStats()
{
	float duration;
	duration = 0.8f;

	if ( _statsNode )
	{
		CCFiniteTimeAction*  moveAction = CCSequence::actions(
			CCMoveBy::actionWithDuration( 
				duration / 2.0f, 
				ccp( 0.0f, _statsNode->getContentSizeInPixels().height + 25.0f )),
			CCMoveBy::actionWithDuration(  duration / 2.0f, CCPointZero ),
			CCCallFuncND::actionWithTarget(this, callfuncND_selector(SelectPocketScene::CleanOldStats), _statsNode ), 
			NULL);

		_statsNode->stopAllActions();
		_statsNode->runAction( moveAction );
		_statsNode = NULL;
	}
}
//-----------------------------------------------------------------------------------
CCSprite* SelectPocketScene::GetStatsNode()
{
	// fix me adding and adding
	stringstream ss;
	PocketStats stats;
	GameStatus	*status;

	status = Game::Get()->GetGameStatus();
	stats = PocketManager::Get()->GetPocketStats( GetDisplayedPocketEnum() );

    RLOG_SSIII("POCKET_MENU",
               GetDisplayedPocketEnum(),
               "STATS",
               stats._levelsCompletedCount,
               stats._levelsSkipCount,
               stats._levelsLockCount );
    
	ss.str("");
	ss << stats._levelsCompletedCount;
	CCLabelBMFont* labelCompleted = CCLabelBMFont::labelWithString( ss.str().c_str(), Config::LevelStatsFont );

	ss.str("");
	ss << stats._levelsSkipCount;
	CCLabelBMFont* labelSkip = CCLabelBMFont::labelWithString( ss.str().c_str(), Config::LevelStatsFont );
	
	ss.str("");
	ss << stats._levelsLockCount;
	CCLabelBMFont* labelLock = CCLabelBMFont::labelWithString( ss.str().c_str(), Config::LevelStatsDisabledFont );

	float baseY = 32.0f;
	float baseX = -248.0f;
	float deltaX = 200.0f;

	labelCompleted->setPosition( ccp( baseX, baseY ));
	labelSkip->setPosition( ccp( baseX + deltaX, baseY ));
	labelLock->setPosition( ccp( baseX + 2.0f * deltaX, baseY ));
	
    CCSprite *node;
    node = CCSprite::spriteWithFile( "Images/Other/none.png");
    node->setContentSizeInPixels( CCSize( 572.0f, 163.0f ));
    
	CCSprite *sprite;
    sprite = CCSprite::spriteWithFile( Lang::Get()->GetLang( "Images/Menus/SelectPocket/StatsBg.png" ));
    
    node->addChild( sprite, 10 );
	node->addChild( labelCompleted, 20 );
	node->addChild( labelSkip, 20 );
	node->addChild( labelLock, 20 );
	
	return node;
}
//-----------------------------------------------------------------------------------
GameTypes::PocketType SelectPocketScene::GetDisplayedPocketEnum()
{
	return ( GameTypes::PocketType )  ( _slidingNode->GetIndex() + 1 );
}
//-----------------------------------------------------------------------------------







PocketStatNode* PocketStatNode::node()
{
	PocketStatNode* node;
	node = new PocketStatNode();
	node->autorelease();
	return node;
}
//-----------------------------------------------------------------------------------
PocketStatNode::PocketStatNode()
{
	stringstream ss;
	PocketStats stats;
	GameStatus	*status;

	status = Game::Get()->GetGameStatus();
	//stats = PocketManager::Get()->GetPocketStats( GetDisplayedPocketEnum() );

	ss << "skipped: " << stats._levelsSkipCount << "\n";
	ss << "completed: " << stats._levelsCompletedCount << "\n";
	ss << "total: " << stats._levelsTotalCount << "\n";

	//CCLabelBMFont* statsLabel = CCLabelBMFont::labelWithString( ss.str().c_str(), Config::LevelCompletedScoreFont );
	//statsLabel->setPosition( ccp( 0.0f, 0.0f ));
	//statsLabel->setOpacity( 240 );

	//CCSprite *sprite;
	//sprite = CCSprite::spriteWithFile( Skins::GetSkinName("Images/Blocks/none.png") );
	//sprite->addChild( statsLabel );
	_label1 = CCLabelBMFont::labelWithString( "dupa", Config::LevelStatsFont );
	_label2 = CCLabelBMFont::labelWithString( "dupa", Config::LevelStatsFont );

}
//-----------------------------------------------------------------------------------
