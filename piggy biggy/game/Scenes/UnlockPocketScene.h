#ifndef __UNLOCKPOCKETSCENE_H__
#define __UNLOCKPOCKETSCENE_H__

#include "cocos2d.h"
#include <Box2D/Box2D.h>
#include "GameTypes.h"

//------------------------------------------------------------------------------------------

using namespace std;
USING_NS_CC;

//------------------------------------------------------------------------------------------
class UnlockPocketNode: public CCLayer
{
private:
	static void		DoShowScene( void* );

public:
	static void		ShowScene( GameTypes::PocketType pocket );
	static CCScene*	CreateScene( GameTypes::PocketType pocket );
	static UnlockPocketNode* node( GameTypes::PocketType pocket );

    virtual void onExit();
	virtual bool init();
	virtual ~UnlockPocketNode();

	void SceneFinished();
	void StartAnim();

	void PlayUnlockSound();
    
   	void FadeOutSound();
	void FadeInSound();

private :
	UnlockPocketNode( GameTypes::PocketType pocket );
    void ConstructAnim();

private:
	CCSprite		*_pocketSprite;
	//CCSprite		*_pocketLockedSprite;
	CCSprite		*_padLockSprite;
    CCSprite        *_loadingSprite;
    
	bool					_started;
	GameTypes::PocketType	_pocket;
	CCSprite				*_background;
    
    CCFiniteTimeAction    *_fadeInAction;
    CCFiniteTimeAction    *_fadeOutAction;
    CCFiniteTimeAction    *_soundFadeAction;
    CCFiniteTimeAction    *_loadingFadeInAction;
    
};

#endif
