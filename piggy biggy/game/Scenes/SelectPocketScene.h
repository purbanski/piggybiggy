#ifndef __SELECTPOCKETSCENE_H__
#define __SELECTPOCKETSCENE_H__

#include <sstream>
#include "cocos2d.h"
#include <Box2D/Box2D.h>
#include "pu/ui/SlidingNode.h"
#include "ScaleScreenLayer.h"
#include "LevelFactory.h"
#include "Levels/LevelList.h"

//------------------------------------------------------------------------------------------

using namespace std;
USING_NS_CC;

//------------------------------------------------------------------------------------------
class SelectPocketScene : public CCLayer, public SlidingNodeListener
{
private:
	static void		DoShowScene( void* );

public:
	static void		ShowScene();
	static CCScene*	CreateScene();

	LAYER_NODE_FUNC( SelectPocketScene );
	virtual bool init();  
	virtual ~SelectPocketScene();

	virtual void SlidingNode_SlideFinish();
	void CleanOldBackground(CCNode* pTarget, void* data);
	void CleanOldStats(CCNode* pTarget, void* data);

private :
	SelectPocketScene();
	void SetIndex( GameTypes::LevelEnum level );

	CCNode* ConstructSlidingMenu();
	CCMenu* ConstructMainMenu( GameTypes::PocketType pocket );

	void Menu_SelectLevel( CCObject *sender );
	void Menu_MainMenu( CCObject *sender );

	CCMenuItem* GetPocketItem( GameTypes::PocketType pocket );
	void Menu_SelectPocket( CCObject *sender );

	void BackgroundChange();
	void ShowStats();
	void HideStats();
	CCSprite* GetStatsNode();

	GameTypes::PocketType GetDisplayedPocketEnum();

private:
	CCSprite	*_background;
	CCSprite    *_statsNode;

	typedef list<LevelList *>	LevelListList;

	SlidingNode			*_slidingNode;
	ScaleScreenLayer	*_scaleScreenLayer;
	LevelListList		_levelLists;

	//int					_backgroundTag;
	GameTypes::PocketType	_lastPocketDisplayed;
};


//------------------------------------------------------------------------------------------
// Pocket Stat Node
//------------------------------------------------------------------------------------------
class PocketStatNode : public CCNode
{
public:
	static PocketStatNode* node();

	void FadeIn();
	void FadeOut();

private:
	PocketStatNode();

private:
	CCLabelBMFont *_label1;
	CCLabelBMFont *_label2;
	CCLabelBMFont *_label3;

};

//------------------------------------------------------------------------------------------
#endif 
