#include "ComicsSceneLite.h"
#include "Game.h"
#include "MainMenuScene.h"
#include "Transitions.h"
#include "IntroScene.h"
#include "TestFlight/Report.h"
#include "RunLogger/RunLogger.h"
#include "ComicsScene.h"

USING_NS_CC;
/*
 
 
 NOT IN USE 
 
 
 
 */
    
float ComicsSceneLite::SceneShowDur = ComicsScene::SceneShowDur;
float ComicsSceneLite::SceneMoveDur = ComicsScene::SceneMoveDur;

//--------------------------------------------------------------------------------------------------
// Comics Scene Lite
//--------------------------------------------------------------------------------------------------
CCScene* ComicsSceneLite::CreateScene()
{
	CCScene *scene = CCScene::node();

    ComicsSceneLite *layer = new ComicsSceneLite();
    if ( layer && layer->init() )
    {
        layer->autorelease();
        scene->setScale( Game::Get()->GetScale() );
        scene->addChild( layer );
        return scene;
    }
    else
    {
        CC_SAFE_DELETE( layer );
        return NULL;
    }
}
//--------------------------------------------------------------------------------------------------
ComicsSceneLite::ComicsSceneLite()
{
    _skipPressed = false;
    _debugIndex = 0;
    
    gAppDisplayRotateMode = eDisplay_LandscapeRotating;
    
    _scene1 = NULL;
    _scene2 = NULL;
    _scene3 = NULL;
    _scene4 = NULL;
    _scene5 = NULL;
    _scene6 = NULL;
    _scene7 = NULL;
}
//--------------------------------------------------------------------------------------------------
ComicsSceneLite::~ComicsSceneLite()
{
	removeAllChildrenWithCleanup( true );
}
//--------------------------------------------------------------------------------------------------
bool ComicsSceneLite::init()
{
	if ( ! CCLayer::init() )
		return false;

    Preloader::Get()->FreeResources();
    
	if ( Tools::GetScreenSize().width != Config::GameSize.width  || Tools::GetScreenSize().height != Config::GameSize.height )
	{
		CCLayer* scaleScreenLayer = ScaleScreenLayer::node();
		scaleScreenLayer->setPosition( Tools::GetScreenMiddle() );
		addChild( scaleScreenLayer, 250 );
	}
    
    CCTexture2D::setDefaultAlphaPixelFormat( kCCTexture2DPixelFormat_RGBA4444 );

    _loadingSprite = CCSprite::spriteWithFile( "Images/Intro/Comics/loading.png");
    _loadingSprite->setIsVisible( false );
    _loadingSprite->setPosition( Tools::GetScreenMiddle() );
    addChild( _loadingSprite, 20 );
    
    
    _scene1 = GetSceneSprite( 1 );
    _scene2 = GetSceneSprite( 2 );
    
    addChild( _scene1, 10 );
    addChild( _scene2, 10 );

    
    SetScene1();
    
    int fadeDestDown = 190;
    int fadeDestUp = 230;
    float fadeDur = 0.45f;
    float delayDur = 1.0f;
    
    CCPoint touchToSkipPos;
    CCSprite *touchToSkip;
    CCSprite *touchToSkipFlash;
    
    touchToSkipPos.x = Tools::GetScreenMiddleX() + 345.0f;
    touchToSkipPos.y = Tools::GetScreenMiddleY() - 275.0f;
    
    touchToSkip = CCSprite::spriteWithFile( "Images/Intro/Comics/touchToSkip.png");
    touchToSkipFlash = CCSprite::spriteWithFile( "Images/Intro/Comics/touchToSkipFlash.png");
    
    touchToSkip->setPosition( touchToSkipPos );
    touchToSkipFlash->setPosition( touchToSkipPos );
    
    touchToSkip->setOpacity( 0 );
    touchToSkipFlash->setOpacity( 0 );
    
    int blinkRep;
    blinkRep = 6;
    
    CCFiniteTimeAction *seqLabel = CCSequence::actions(
                                          CCDelayTime::actionWithDuration( delayDur ),
                                          CCFadeTo::actionWithDuration( 1.0f, 255 ),
                                          CCDelayTime::actionWithDuration( fadeDur * (float) blinkRep * 2.0f ),
                                          CCFadeTo::actionWithDuration( fadeDur, 0 ),
                                          NULL );
    
    CCFiniteTimeAction *seqFlash = CCSequence::actions(
                                          CCDelayTime::actionWithDuration( delayDur ),
                                          CCFadeTo::actionWithDuration( 1.0f, fadeDestUp ),
                                          CCRepeat::actionWithAction(
                                                                     CCSequence::actions(
                                                                                       CCFadeTo::actionWithDuration( fadeDur, fadeDestDown ),
                                                                                         CCFadeTo::actionWithDuration( fadeDur, fadeDestUp ),
                                                                                         NULL ), blinkRep ),
                                          CCFadeTo::actionWithDuration( fadeDur, 0 ),
                                          NULL );
    
    
    addChild( touchToSkip, 250 );
    addChild( touchToSkipFlash, 240 );
    
    touchToSkip->runAction( seqLabel );
    touchToSkipFlash->runAction( seqFlash );

    SoundEngine::Get()->GetBackgroundMusic().SetMusic_Intro();

    setIsTouchEnabled( true );
    return true;
}
//--------------------------------------------------------------------------------------------------
CCSprite* ComicsSceneLite::GetSceneSprite( int index )
{
    stringstream ss;
    ss << "Images/Intro/Comics/page" << index << ".jpg";
    
    CCSprite *sprite = CCSprite::spriteWithFile( ss.str().c_str() );
    sprite->setIsVisible( false );
    
    return sprite;
}
//--------------------------------------------------------------------------------------------------
void ComicsSceneLite::RemoveSprite( CCNode *sender, void *data )
{
    long spriteNr;
    CCSprite **pSprite;

    spriteNr = (long) data;
    pSprite = NULL;
    
    switch ( spriteNr )
    {
        case 1 :
            pSprite = & _scene1;
            break;
            
        case 2 :
            pSprite = & _scene2;
            break;
            
        case 3 :
            pSprite = & _scene3;
            break;
            
        case 4 :
            pSprite = & _scene4;
            break;
            
        case 5 :
            pSprite = & _scene5;
            break;
            
        case 6 :
            pSprite = & _scene6;
            break;
            
        case 7 :
            pSprite = & _scene7;
            break;
            
        default:
            return;
    }
    
    if ( pSprite && *pSprite )
    {
        (*pSprite)->removeFromParentAndCleanup( true );
        *pSprite = NULL;

        CCSpriteFrameCache::sharedSpriteFrameCache()->removeUnusedSpriteFrames();
    }
}
//--------------------------------------------------------------------------------------------------
void ComicsSceneLite::onEnter()
{
	CCLayer::onEnter();
}
//--------------------------------------------------------------------------------------------------
void ComicsSceneLite::onEnterTransitionDidFinish()
{
   CCLayer::onEnterTransitionDidFinish();
}
//--------------------------------------------------------------------------------------------------
void ComicsSceneLite::onExit()
{
	CCLayer::onExit();
   	CCTouchDispatcher::sharedDispatcher()->removeDelegate( this );
}
//--------------------------------------------------------------------------------------------------
bool ComicsSceneLite::ccTouchBegan(CCTouch *touch, CCEvent *pEvent)
{
    if ( _skipPressed )
        return true;
    
    _skipPressed = true;
   
    RLOG("Intro_Skip_%d", _debugIndex);

    ShowMainMenuScene();
    return true;
}
//--------------------------------------------------------------------------------------------------
void ComicsSceneLite::ccTouchMoved(CCTouch *touch, CCEvent *pEvent)
{
//    b2Vec2 pos;
//    pos = Tools::TouchAdjust( touch );
//    
//    b2Vec2 newPos;
//    
//    newPos.x = _startPos.x - ( _startTouch.x - pos.x ) * RATIO;
//    newPos.y = _startPos.y - ( _startTouch.y - pos.y ) * RATIO;
//    
//    _activeSprite->setPosition( ccp( newPos.x, newPos.y ));
}
//--------------------------------------------------------------------------------------------------
void ComicsSceneLite::ccTouchEnded(CCTouch *touch, CCEvent *pEvent)
{
//    D_LOG("Intro sprite x:%f, y:%f", Tools::GetScreenMiddleX() - _activeSprite->getPosition().x, Tools::GetScreenMiddleY() - _activeSprite->getPosition().y );
}
//--------------------------------------------------------------------------------------------------
void ComicsSceneLite::registerWithTouchDispatcher()
{
    CCTouchDispatcher::sharedDispatcher()->addTargetedDelegate( this, 1, true );
}
//--------------------------------------------------------------------------------------------------
void ComicsSceneLite::ShowMainMenuScene()
{
    ReleaseAll();
    Preloader::Get()->FreeResources();
   	CCTouchDispatcher::sharedDispatcher()->removeDelegate( this );
    
    CCScene *scene;
    scene = MainMenuScene::CreateScene();
    
    TransitionScene1 *trans;
    trans = TransitionScene1::transitionWithDuration( scene );
    
    SoundEngine::Get()->PlayEffect( SoundEngine::eSoundPageTurn );
    CCDirector::sharedDirector()->replaceScene( trans );
}
//--------------------------------------------------------------------------------------------------
void ComicsSceneLite::SetScene1()
{
    if ( _skipPressed )
        return;

    _debugIndex = 1;

    _scene1->setScale( 0.68f );
    _scene1->setPosition( ccp( Tools::GetScreenMiddle().x + 24.0f, Tools::GetScreenMiddle().y - 184.0f ));

    CCAction *trasAction = CCSequence::actions(
                                               CCMoveTo::actionWithDuration( SceneShowDur + 1.0f, ccp(
                                                                                               Tools::GetScreenMiddle().x + 40.0f,
                                                                                               Tools::GetScreenMiddle().y - 195.0f )),
                                               
                                               CCMoveTo::actionWithDuration( SceneMoveDur, ccp(
                                                                                               Tools::GetScreenMiddle().x + 410.0f,
                                                                                               Tools::GetScreenMiddle().y + 400.0f )),
                                               
                                               CCMoveTo::actionWithDuration( SceneShowDur, ccp(
                                                                                               Tools::GetScreenMiddle().x + 455.0f,
                                                                                               Tools::GetScreenMiddle().y + 410.0f )),

                                               CCMoveTo::actionWithDuration( SceneMoveDur, ccp(
                                                                                               Tools::GetScreenMiddle().x - 390.0f,
                                                                                               Tools::GetScreenMiddle().y + 440.0f )),
                                               
                                               CCMoveTo::actionWithDuration( SceneShowDur, ccp(
                                                                                               Tools::GetScreenMiddle().x - 420.0f,
                                                                                               Tools::GetScreenMiddle().y + 430.0f )),

                                               CCCallFunc::actionWithTarget( this, callfunc_selector( ComicsSceneLite::SetScene2 )),
                                               
                                               CCMoveTo::actionWithDuration( SceneMoveDur, ccp(
                                                                                               Tools::GetScreenMiddle().x - 1364.0f,
                                                                                               Tools::GetScreenMiddle().y - 202.0f )),

                                               CCCallFuncND::actionWithTarget( this, callfuncND_selector( ComicsSceneLite::RemoveSprite ), (void*) 1 ),

                                               NULL );
    
    CCAction *scaleAction = CCSequence::actions(CCDelayTime::actionWithDuration( SceneShowDur + 1.0f ),
                                                CCScaleTo::actionWithDuration( SceneMoveDur, 1.0f ),
                                                CCDelayTime::actionWithDuration( SceneShowDur ),
                                                NULL );
  
    _scene1->setIsVisible( true );
    //addChild( _scene1, 1 );
    
    _scene1->runAction( trasAction );
    _scene1->runAction( scaleAction );
}
//--------------------------------------------------------------------------------------------------
void ComicsSceneLite::SetScene2()
{
    if ( _skipPressed )
        return;

    float xmove;
    float ymove;
    
    xmove = 0.0f;
    ymove = 0.0f;
    
    _debugIndex = 2;

    _scene2->setScale( 1.025f );
	_scene2->setPosition( ccp( Tools::GetScreenMiddle().x + xmove + 1388.0f, Tools::GetScreenMiddle().y + ymove + 290.0f ));

    CCAction *trasAction = CCSequence::actions(
                                               
                                               CCMoveTo::actionWithDuration( SceneMoveDur, ccp(
                                                                                               Tools::GetScreenMiddle().x + xmove + 420.0f,
                                                                                               Tools::GetScreenMiddle().y + ymove - 328.0f )),
                                               
                                               CCMoveTo::actionWithDuration( SceneShowDur, ccp(
                                                                                               Tools::GetScreenMiddle().x + xmove + 396.0f,
                                                                                               Tools::GetScreenMiddle().y + ymove - 336.0f )),
                                               
                                               CCMoveTo::actionWithDuration( SceneMoveDur, ccp(
                                                                                               Tools::GetScreenMiddle().x + xmove - 444.0f,
                                                                                               Tools::GetScreenMiddle().y + ymove - 332.0f )),
                                               
                                               CCMoveTo::actionWithDuration( SceneShowDur, ccp(
                                                                                               Tools::GetScreenMiddle().x + xmove - 472.0f,
                                                                                               Tools::GetScreenMiddle().y + ymove - 341.0f )),

                                               CCMoveTo::actionWithDuration( SceneMoveDur, ccp(
                                                                                               Tools::GetScreenMiddle().x + xmove + 412.0f,
                                                                                               Tools::GetScreenMiddle().y + ymove + 236.0f )),

                                               CCMoveTo::actionWithDuration( SceneShowDur, ccp(
                                                                                               Tools::GetScreenMiddle().x + xmove + 436.0f,
                                                                                               Tools::GetScreenMiddle().y + ymove + 268.0f )),
                                               

                                               CCMoveTo::actionWithDuration( SceneMoveDur, ccp(
                                                                                               Tools::GetScreenMiddle().x + xmove - 452.0f,
                                                                                               Tools::GetScreenMiddle().y + ymove + 252.0f )),

                                               CCMoveTo::actionWithDuration( SceneShowDur, ccp(
                                                                                               Tools::GetScreenMiddle().x + xmove - 488.0f,
                                                                                               Tools::GetScreenMiddle().y + ymove + 236.0f )),

                                               CCCallFunc::actionWithTarget( this, callfunc_selector( ComicsSceneLite::SetScene3 )),

                                               CCMoveTo::actionWithDuration( SceneMoveDur, ccp(
                                                                                               Tools::GetScreenMiddle().x + xmove - 1396.0f,
                                                                                               Tools::GetScreenMiddle().y + ymove - 146.0f )),
                                               
                                               CCCallFuncND::actionWithTarget( this, callfuncND_selector( ComicsSceneLite::RemoveSprite ), (void*) 2 ),

                                               NULL );

    _scene2->setIsVisible( true );
    //  addChild( _scene2, 1 );
    _scene2->runAction( trasAction );
}
//--------------------------------------------------------------------------------------------------
void ComicsSceneLite::SetScene3()
{
    _loadingSprite->setIsVisible( true );
    
    if ( _skipPressed )
        return;

    SoundEngine::Get()->PauseBackgroundMusic();
    
    _scene3 = GetSceneSprite( 3 );
    _scene4 = GetSceneSprite( 4 );

    addChild( _scene3, 10 );
    addChild( _scene4, 10 );
    
    float xmove;
    float ymove;
    
    xmove = 0.0f;
    ymove = 0.0f;
    
    _debugIndex = 3;

    _scene3->setScale( 0.975f );
  	_scene3->setPosition( ccp( Tools::GetScreenMiddle().x + xmove + 998.0f, Tools::GetScreenMiddle().y + ymove + 54.0f ));

    CCAction *trasAction = CCSequence::actions(
                                               
                                               CCMoveTo::actionWithDuration( SceneMoveDur, ccp(
                                                                                               Tools::GetScreenMiddle().x + xmove + 38.0f,
                                                                                               Tools::GetScreenMiddle().y + ymove - 276.0f )),
                                               
                                               CCMoveTo::actionWithDuration( SceneShowDur, ccp(
                                                                                               Tools::GetScreenMiddle().x + xmove + 50.0f,
                                                                                               Tools::GetScreenMiddle().y + ymove - 290.0f )),
                                               
                                               CCMoveTo::actionWithDuration( SceneMoveDur, ccp(
                                                                                               Tools::GetScreenMiddle().x + xmove + 65.0f,
                                                                                               Tools::GetScreenMiddle().y + ymove + 314.0f )),

                                               CCMoveTo::actionWithDuration( SceneShowDur, ccp(
                                                                                               Tools::GetScreenMiddle().x + xmove + 39.0f,
                                                                                               Tools::GetScreenMiddle().y + ymove + 342.0f )),

                                               CCCallFunc::actionWithTarget( this, callfunc_selector( ComicsSceneLite::SetScene4 )),

                                               CCMoveTo::actionWithDuration( SceneMoveDur, ccp(
                                                                                               Tools::GetScreenMiddle().x + xmove - 931.0f,
                                                                                               Tools::GetScreenMiddle().y + ymove + 20.0f )),

                                               CCCallFuncND::actionWithTarget( this, callfuncND_selector( ComicsSceneLite::RemoveSprite ), (void*) 3 ),

                                               NULL );

    CCAction *scaleAction = CCSequence::actions(
                                                CCDelayTime::actionWithDuration( SceneMoveDur ),
                                                CCScaleTo::actionWithDuration( SceneShowDur, 1.0f ),
                                                CCDelayTime::actionWithDuration( SceneMoveDur ),
                                                CCScaleTo::actionWithDuration( SceneShowDur, 1.05f ),
                                                NULL );

    _scene3->setIsVisible( true );
    _scene3->runAction( trasAction );
    _scene3->runAction( scaleAction );

    _loadingSprite->setIsVisible( false );
    SoundEngine::Get()->ResumeBackgroundMusic();
}
//--------------------------------------------------------------------------------------------------
void ComicsSceneLite::SetScene4()
{
    if ( _skipPressed )
        return;

    float xmove;
    float ymove;
    
    xmove = 0.0f;
    ymove = 0.0f;
    
    _debugIndex = 4;
    
    _scene4->setScale( 0.82f );
  	_scene4->setPosition( ccp( Tools::GetScreenMiddle().x + xmove + 1194, Tools::GetScreenMiddle().y + ymove + 128.0f ));

    CCAction *trasAction = CCSequence::actions(
                                               
                                               CCMoveTo::actionWithDuration( SceneMoveDur, ccp(
                                                                                               Tools::GetScreenMiddle().x + xmove + 57.0f,
                                                                                               Tools::GetScreenMiddle().y + ymove - 244.0f )),
                                               CCMoveTo::actionWithDuration( SceneShowDur, ccp(
                                                                                               Tools::GetScreenMiddle().x + xmove + 23.0f,
                                                                                               Tools::GetScreenMiddle().y + ymove - 254.0f )),
                                               
                                               CCMoveTo::actionWithDuration( SceneMoveDur, ccp(
                                                                                               Tools::GetScreenMiddle().x + xmove + 451.0f,
                                                                                               Tools::GetScreenMiddle().y + ymove + 386.0f )),
                                               
                                               CCMoveTo::actionWithDuration( SceneShowDur, ccp(
                                                                                               Tools::GetScreenMiddle().x + xmove + 485.0f,
                                                                                               Tools::GetScreenMiddle().y + ymove + 396.0f )),
                                               
                                               
                                               CCMoveTo::actionWithDuration( SceneMoveDur, ccp(
                                                                                               Tools::GetScreenMiddle().x + xmove - 409.0f,
                                                                                               Tools::GetScreenMiddle().y + ymove + 362.0f )),
                                               
                                               CCMoveTo::actionWithDuration( SceneShowDur, ccp(
                                                                                               Tools::GetScreenMiddle().x + xmove - 451.0f,
                                                                                               Tools::GetScreenMiddle().y + ymove + 360.0f )),

                                               CCCallFunc::actionWithTarget( this, callfunc_selector( ComicsSceneLite::SetScene5 )),
                                               
                                               CCMoveTo::actionWithDuration( SceneMoveDur, ccp(
                                                                                               Tools::GetScreenMiddle().x + xmove - 1393.0f,
                                                                                               Tools::GetScreenMiddle().y + ymove + 358.0f )),

                                               CCCallFuncND::actionWithTarget( this, callfuncND_selector( ComicsSceneLite::RemoveSprite ), (void*) 4 ),
                                               
                                               NULL );

    CCAction *scaleAction = CCSequence::actions(
                                                CCDelayTime::actionWithDuration( SceneMoveDur + SceneShowDur ),
                                                CCScaleTo::actionWithDuration( SceneMoveDur, 1.05f ),
                                                NULL );

    _scene4->setIsVisible( true );
    _scene4->runAction( trasAction );
    _scene4->runAction( scaleAction );
}
//--------------------------------------------------------------------------------------------------
void ComicsSceneLite::SetScene5()
{
   if ( _skipPressed )
        return;

    SoundEngine::Get()->PauseBackgroundMusic();
    
    _scene5 = GetSceneSprite( 5 );
    addChild( _scene5, 10 );
    
    float xmove;
    float ymove;
    
    xmove = 0.0f;
    ymove = 0.0f;

    _debugIndex = 5;
    
    _scene5->setScale( 1.075f );
  	_scene5->setPosition( ccp( Tools::GetScreenMiddle().x + xmove + 1396.0f, Tools::GetScreenMiddle().y + ymove + 38.0f ));

    CCAction *trasAction = CCSequence::actions(
                                               
                                               CCMoveTo::actionWithDuration( SceneMoveDur, ccp(
                                                                                               Tools::GetScreenMiddle().x + xmove + 454.0f,
                                                                                               Tools::GetScreenMiddle().y + ymove + 60.0f )),
                                               
                                               CCMoveTo::actionWithDuration( SceneShowDur, ccp(
                                                                                               Tools::GetScreenMiddle().x + xmove + 420.0f,
                                                                                               Tools::GetScreenMiddle().y + ymove + 46.0f )),
                                               
                                               CCMoveTo::actionWithDuration( SceneMoveDur, ccp(
                                                                                               Tools::GetScreenMiddle().x + xmove - 432.0f,
                                                                                               Tools::GetScreenMiddle().y + ymove + 65.0f )),

                                               CCMoveTo::actionWithDuration( SceneShowDur, ccp(
                                                                                               Tools::GetScreenMiddle().x + xmove - 468.0f,
                                                                                               Tools::GetScreenMiddle().y + ymove + 61.0f )),

                                               CCCallFunc::actionWithTarget( this, callfunc_selector( ComicsSceneLite::SetScene6 )),

                                               CCMoveTo::actionWithDuration( SceneMoveDur, ccp(
                                                                                               Tools::GetScreenMiddle().x + xmove - 1406.0f,
                                                                                               Tools::GetScreenMiddle().y + ymove - 192.0f )),

                                               CCCallFuncND::actionWithTarget( this, callfuncND_selector( ComicsSceneLite::RemoveSprite ), (void*) 5 ),

                                               NULL );


   
    _scene5->setIsVisible( true );
    _scene5->runAction( trasAction );
    
    SoundEngine::Get()->ResumeBackgroundMusic();
}
//--------------------------------------------------------------------------------------------------
void ComicsSceneLite::SetScene6()
{
    if ( _skipPressed )
        return;

    SoundEngine::Get()->PauseBackgroundMusic();
    
    _scene6 = GetSceneSprite( 6 );
    _scene7 = GetSceneSprite( 7 );

    addChild( _scene6, 10 );
    addChild( _scene7, 10 );

    float xmove;
    float ymove;
    
    xmove = 0.0f;
    ymove = 0.0f;

    _debugIndex = 6;
  
    _scene6->setScale( 0.755f );
  	_scene6->setPosition( ccp( Tools::GetScreenMiddle().x + xmove + 1114.0f, Tools::GetScreenMiddle().y + ymove - 72.0f ));

    CCAction *trasAction = CCSequence::actions(
                                               
                                               CCMoveTo::actionWithDuration( SceneMoveDur, ccp(
                                                                                               Tools::GetScreenMiddle().x + xmove + 18.0f,
                                                                                               Tools::GetScreenMiddle().y + ymove - 248.0f )),
                                               
                                               CCMoveTo::actionWithDuration( SceneShowDur, ccp(
                                                                                               Tools::GetScreenMiddle().x + xmove + 10.0f,
                                                                                               Tools::GetScreenMiddle().y + ymove - 264.0f )),

                                        
                                               CCMoveTo::actionWithDuration( SceneMoveDur, ccp(
                                                                                               Tools::GetScreenMiddle().x + xmove + 422.0f,
                                                                                               Tools::GetScreenMiddle().y + ymove + 380.0f )),

                                               CCMoveTo::actionWithDuration( SceneShowDur, ccp(
                                                                                               Tools::GetScreenMiddle().x + xmove + 470.0f,
                                                                                               Tools::GetScreenMiddle().y + ymove + 388.0f )),

                                               
                                               CCMoveTo::actionWithDuration( SceneMoveDur, ccp(
                                                                                               Tools::GetScreenMiddle().x + xmove - 410.0f,
                                                                                               Tools::GetScreenMiddle().y + ymove + 372.0f )),

                                               CCMoveTo::actionWithDuration( SceneShowDur, ccp(
                                                                                               Tools::GetScreenMiddle().x + xmove - 444.0f,
                                                                                               Tools::GetScreenMiddle().y + ymove + 358.0f )),
                                               
                                               CCCallFunc::actionWithTarget( this, callfunc_selector( ComicsSceneLite::SetScene7 )),

                                               CCMoveTo::actionWithDuration( SceneMoveDur, ccp(
                                                                                               Tools::GetScreenMiddle().x + xmove - 1374.0f,
                                                                                               Tools::GetScreenMiddle().y + ymove - 22.0f )),

                                               CCCallFuncND::actionWithTarget( this, callfuncND_selector( ComicsSceneLite::RemoveSprite ), (void*) 6 ),

                                               NULL );


    CCAction *scaleAction = CCSequence::actions(
                                                CCDelayTime::actionWithDuration( SceneMoveDur + SceneShowDur ),
                                                CCScaleTo::actionWithDuration( SceneMoveDur, 1.05f ),
                                                NULL );
    _scene6->setIsVisible( true );
    _scene6->runAction( trasAction );
    _scene6->runAction( scaleAction );

    SoundEngine::Get()->ResumeBackgroundMusic();
}
//--------------------------------------------------------------------------------------------------
void ComicsSceneLite::SetScene7()
{
    if ( _skipPressed )
        return;

    RLOG("Intro_End");
    
    _debugIndex = 7;
    
    float xmove;
    float ymove;
    
    xmove = 0.0f;
    ymove = 0.0f;
    
    _scene7->setScale( 0.78f );
  	_scene7->setPosition( ccp( Tools::GetScreenMiddle().x + xmove + 1115.0f, Tools::GetScreenMiddle().y + ymove - 308.0f ));

    CCAction *trasAction = CCSequence::actions(
                                               
                                               CCMoveTo::actionWithDuration( SceneMoveDur, ccp(
                                                                                               Tools::GetScreenMiddle().x + xmove + 48.0f,
                                                                                               Tools::GetScreenMiddle().y + ymove - 212.0f )),
                                               
                                               CCMoveTo::actionWithDuration( SceneShowDur, ccp(
                                                                                               Tools::GetScreenMiddle().x + xmove + 20.0f,
                                                                                               Tools::GetScreenMiddle().y + ymove - 234.0f )),

                                        
                                               CCMoveTo::actionWithDuration( SceneMoveDur, ccp(
                                                                                               Tools::GetScreenMiddle().x + xmove + 452.0f,
                                                                                               Tools::GetScreenMiddle().y + ymove + 416.0f )),

                                               CCMoveTo::actionWithDuration( SceneShowDur, ccp(
                                                                                               Tools::GetScreenMiddle().x + xmove + 486.0f,
                                                                                               Tools::GetScreenMiddle().y + ymove + 436.0f )),
//
//                                               
                                               CCMoveTo::actionWithDuration( SceneMoveDur, ccp(
                                                                                               Tools::GetScreenMiddle().x + xmove - 422.0f,
                                                                                               Tools::GetScreenMiddle().y + ymove + 412.0f )),
//
                                               CCMoveTo::actionWithDuration( SceneShowDur, ccp(
                                                                                               Tools::GetScreenMiddle().x + xmove - 452.0f,
                                                                                               Tools::GetScreenMiddle().y + ymove + 430.0f )),
//
                                               CCCallFunc::actionWithTarget( this, callfunc_selector( ComicsSceneLite::ShowMainMenuScene )),
//
//                                               CCMoveTo::actionWithDuration( SceneMoveDur, ccp(
//                                                                                               Tools::GetScreenMiddle().x + xmove - 1374.0f,
//                                                                                               Tools::GetScreenMiddle().y + ymove - 22.0f )),
//
//                                               CCCallFuncND::actionWithTarget( this, callfuncND_selector( ComicsScene::RemoveSprite ), (void*) background ),
//
                                               NULL );


    CCAction *scaleAction = CCSequence::actions(
                                                CCDelayTime::actionWithDuration( SceneMoveDur + SceneShowDur ),
                                                CCScaleTo::actionWithDuration( SceneMoveDur, 1.1f ),
                                                NULL );
    // addChild( _scene7, 1 );
    _scene7->setIsVisible( true );
    _scene7->runAction( trasAction );
    _scene7->runAction( scaleAction );
}
//--------------------------------------------------------------------------------------------------
void ComicsSceneLite::ReleaseAll()
{
    if ( _scene1 ) _scene1->stopAllActions();
    if ( _scene2 ) _scene2->stopAllActions();
    if ( _scene3 ) _scene3->stopAllActions();
    if ( _scene4 ) _scene4->stopAllActions();
    if ( _scene5 ) _scene5->stopAllActions();
    if ( _scene6 ) _scene6->stopAllActions();
    if ( _scene7 ) _scene7->stopAllActions();
}
//--------------------------------------------------------------------------------------------------
