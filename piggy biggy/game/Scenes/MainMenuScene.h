#ifndef __MAINMENUSCENE_H__
#define __MAINMENUSCENE_H__

#include "cocos2d.h"
#include "ScaleScreenLayer.h"
#include "LevelFactory.h"
#include "MenuRequests.h"

USING_NS_CC;
using namespace std;

class Level;

//-------------------------------------------------
// RandLevelSequence
//-------------------------------------------------
class RandLevelSequence : public list<GameTypes::LevelEnum>
{
public:
	RandLevelSequence();
	void MoveNext();
	GameTypes::LevelEnum GetCurrent();

private:
	iterator	_index;
};


//-------------------------------------------------
// MainMenuScene
//-------------------------------------------------
class MainMenuScene : public CCLayer
{
private:
	static void		DoShowScene( void *);

public:
	static void		ShowScene();
	static void		ShowSceneFristTime();

	static CCScene* CreateScene();
	
	LAYER_NODE_FUNC( MainMenuScene );
	virtual bool init();  
	virtual ~MainMenuScene();

	virtual void onEnter();
	virtual void onExit();

	virtual void Menu_Play(CCObject* pSender);
	virtual void Menu_SelectLevel(CCObject* pSender);
    virtual void Menu_ShowSettings( CCObject* pSender);
	virtual void Menu_Exit(CCObject* pSender);
	virtual void Menu_UnlockLevels( CCObject *pSender );
	virtual void Menu_BillingToggle( CCObject *pSender );
	virtual void Menu_TestView( CCObject *pSender );
	virtual void Menu_WellWellDoneScene( CCObject *pSender );
	virtual void Menu_RateReset( CCObject *pSender );
	virtual void Menu_RateShow( CCObject *pSender );
    virtual void Menu_MoreGame( CCObject *sender );
    virtual void Menu_Story( CCObject* sender );

	virtual void Step( cocos2d::ccTime dt );

	virtual bool ccTouchBegan(CCTouch *touch, CCEvent *pEvent){ return true; }
	virtual void ccTouchMoved(CCTouch *touch, CCEvent *pEvent){}
	virtual void ccTouchEnded(CCTouch *touch, CCEvent *pEvent){}
	virtual void registerWithTouchDispatcher();

    void ExitClenaUp();
    
	void SwapDemoLevel( CCNode *node, void *data );
	static void DoSelectLevel( void * );

private:
	MainMenuScene();
	void AddMenu();

	//typedef void ( MainMenuScene::*FuncPtr )();
	typedef void ( MainMenuScene::*menuFnPtr )( CCObject *sender );

	CCMenuItem* GetMenuItem( const char *buttonName, menuFnPtr fn, bool anim = true );
	
private:
	int		_counter;
	Level	*_randLevel;

	ScaleScreenLayer*	_scaleScreenLayer;
	RandLevelSequence	_randLevelSequece;

	CCSprite        *_transitionSprite;
    MenuRequests    *_menuRequest;
};

#endif 
