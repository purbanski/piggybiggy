#include <Box2D/Box2D.h>
#include "Appirater/cppAppirater.h"
#include "Billing/Billing.h"
#include "Components/FXMenuItem.h"
#include "Facebook/FBLoginNode.h"
#include "Facebook/FBTool.h"
#include "GLES-Render.h"
#include "Game.h"
#include "GameConfig.h"
#include "Levels/LevelMenu.h"
#include "LoadingBar.h"
#include "MainMenuScene.h"
#include "MenuRequests.h"
#include "MessageBox/MessageBox.h"
#include "NextLevelScene.h"
#include "Platform/GameCenter.h"
#include "Platform/Lang.h"
#include "Platform/ScoreTool.h"
#include "Platform/ToolBox.h"
#include "Platform/WWWTool.h"
#include "RunLogger/GLogger.h"
#include "RunLogger/RunLogger.h"
#include "RunLogger/RunLogger.h"
#include "SelectLevelFromPocketScene.h"
#include "SelectPocketScene.h"
#include "SettingsScene.h"
#include "Skins.h"
#include "SoundEngine.h"
#include "TestViewScene.h"
#include "Tools.h"
#include "Transitions.h"
#include "UnlockPocketScene.h"
#include "WellWellDoneScene.h"
//#include "Scenes/IntroScene.h"
#include "Scenes/MainLoadingScene.h"

USING_NS_CC;

//--------------------------------------------------------------------------------------------------
// RandLevelSequence
//--------------------------------------------------------------------------------------------------
RandLevelSequence::RandLevelSequence()
{
	push_back( GameTypes::eLevelDemo_0 );
	push_back( GameTypes::eLevelDemo_1 );
	push_back( GameTypes::eLevelDemo_2 );

	_index = begin();
}
//--------------------------------------------------------------------------------------------------
void RandLevelSequence::MoveNext()
{
	_index++;
	if ( _index == end() )
		_index = begin();
}
//--------------------------------------------------------------------------------------------------
GameTypes::LevelEnum RandLevelSequence::GetCurrent()
{
	return *_index;
}



//--------------------------------------------------------------------------------------------------
// MainMenuScene
//--------------------------------------------------------------------------------------------------
CCScene* MainMenuScene::CreateScene()
{
	CCScene *scene = CCScene::node();
	MainMenuScene *layer = MainMenuScene::node();

    scene->setScale( Game::Get()->GetScale() );
	scene->addChild(layer);
	return scene;
}
//--------------------------------------------------------------------------------------------------
void MainMenuScene::ShowScene()
{
    MainMenuScene::DoShowScene(NULL);
//	LoadingBar::Get()->AddToScene( &MainMenuScene::DoShowScene, NULL );
}
//--------------------------------------------------------------------------------------------------
void MainMenuScene::ShowSceneFristTime()
{
	LoadingBar::Get( "Images/Other/LoadingInit.jpg" )->AddToScene( &MainMenuScene::DoShowScene, NULL );
}
//--------------------------------------------------------------------------------------------------
void MainMenuScene::DoShowScene( void *)
{
	CCScene* pScene = SceneTransition::transitionWithDuration( CreateScene() );
	if (pScene)
	{
        SoundEngine::Get()->PlayEffect( SoundEngine::eSoundChangeScene );
		CCDirector::sharedDirector()->replaceScene(pScene);
	}
}
//--------------------------------------------------------------------------------------------------
MainMenuScene::MainMenuScene()
{
    gAppDisplayRotateMode = eDisplay_LandscapeRotating;
    
    if ( ToolBox::LowMemDevice( gDevice ))
        CCTexture2D::setDefaultAlphaPixelFormat( kCCTexture2DPixelFormat_RGBA4444 );

    CCTexture2D::setDefaultAlphaPixelFormat( kCCTexture2DPixelFormat_RGBA8888 );

}
//--------------------------------------------------------------------------------------------------
MainMenuScene::~MainMenuScene()
{
	CCTouchDispatcher::sharedDispatcher()->removeDelegate( this ); 
	removeAllChildrenWithCleanup( true );
}
//--------------------------------------------------------------------------------------------------
bool MainMenuScene::init()
{  
    RLOG_S("MAIN_MENU", "SHOW");
    GSendView( "Main Menu" );

	if ( !CCLayer::init() )
		return false;

	if ( Tools::GetScreenSize().width != Config::GameSize.width  || Tools::GetScreenSize().height != Config::GameSize.height )
	{
		_scaleScreenLayer = ScaleScreenLayer::node();
		_scaleScreenLayer->setPosition( Tools::GetScreenMiddle() );
		addChild( _scaleScreenLayer, 250 );
	}

	AddMenu();

	_randLevel = Game::Get()->GetLevelFactory()->CreateLevel( _randLevelSequece.GetCurrent() );
    addChild( _randLevel, 5 );

	CCSize size;
	size = CCDirector::sharedDirector()->getWinSizeInPixels();

	CCSprite *background;
	background = CCSprite::spriteWithFile( "Images/Menus/MainMenu/Background.png" );
	background->setPosition( ccp( Config::GameSize.width / 2.0f, Config::GameSize.height / 2.0f ));
	background->setPosition( ccp( size.width / 2.0f, size.height / 2.0f ));

	addChild( background, 6 );

	// Set music menu
	float xPosDelta = 350.0f;
	float yPosDelta = -255.0f;

	float scale;
	float x; 
	float y;
	float xScreenDelta;
	float yScreenDelta;
	
	scale = Game::Get()->GetScale();
    scale =2 ;

	CCSize winSize = CCDirector::sharedDirector()->getWinSize();
	D_FLOAT(winSize.width)
	D_FLOAT(winSize.height)
	xScreenDelta = ( winSize.width - Config::GameSize.width * scale ) / 2.0f;
	yScreenDelta = ( winSize.height - Config::GameSize.height * scale ) / 2.0f;

	x = Config::GameSize.width / 2.0f * scale + xScreenDelta + xPosDelta;
	y = Config::GameSize.height / 2.0f * scale + yScreenDelta + yPosDelta;

	_transitionSprite = CCSprite::spriteWithFile( "Images/Menus/MainMenu/Flash.jpg" );
	_transitionSprite->setOpacity( 0 );
	_transitionSprite->setPosition( CCPoint( size.width / 2.0f, size.height / 2.0f ));
	addChild( _transitionSprite, 15 );

	_counter = Config::GameDemoChangeCounter;
	schedule( schedule_selector( MainMenuScene::Step ));

 	setIsTouchEnabled( true );
    
    static int once = 0;
    if ( ! once )
    {
        GameCenter::Get()->Authenticate();
        once = 1;
    }
   
 	return true;
}
//--------------------------------------------------------------------------------------------------
void MainMenuScene::AddMenu()
{
#ifdef BUILD_UNLOCK
    CCMenu *unlockMenu              = CCMenu::menuWithItem(NULL);
	CCLabelBMFont *unlockLabel		= CCLabelBMFont::labelWithString( "Unlock",			Config::LevelDebugMenuFont );
	FXMenuItemLabel *unlockItem		= FXMenuItemLabel::itemWithLabel( unlockLabel,		this, menu_selector( MainMenuScene::Menu_UnlockLevels ));
	
//	unlockItem->setPosition( ccp( 410.0f, 265.0f ));
	unlockMenu->addChild( unlockItem, -1  );
    unlockMenu->alignItemsHorizontally();
    unlockMenu->setPosition( ccp( Tools::GetScreenMiddleX() + 390.f,
                                 Tools::GetScreenMiddleY() + 300.0f ));
    addChild(unlockMenu, 30);
#endif

#ifdef BUILD_TEST	
	CCLabelBMFont *billingLabel		= CCLabelBMFont::labelWithString( "BillingON/OFF",	Config::LevelDebugMenuFont );
	CCLabelBMFont *testViewLabel	= CCLabelBMFont::labelWithString( "TestView",		Config::LevelDebugMenuFont );
	CCLabelBMFont *wellDoneLabel	= CCLabelBMFont::labelWithString( "WellDone",		Config::LevelDebugMenuFont );
	CCLabelBMFont *exitLabel		= CCLabelBMFont::labelWithString( "Exit",			Config::LevelDebugMenuFont );


	FXMenuItemLabel *billingItem;
	FXMenuItemLabel *testViewItem;
	FXMenuItemLabel *wellDoneItem;
	FXMenuItemLabel *exitItem;

    billingItem     = FXMenuItemLabel::itemWithLabel( billingLabel, this, menu_selector( MainMenuScene::Menu_BillingToggle ));
	testViewItem	= FXMenuItemLabel::itemWithLabel( testViewLabel, this, menu_selector( MainMenuScene::Menu_TestView ));
	wellDoneItem	= FXMenuItemLabel::itemWithLabel( wellDoneLabel, this, menu_selector( MainMenuScene::Menu_WellWellDoneScene ));
	exitItem		= FXMenuItemLabel::itemWithLabel( exitLabel,	this, menu_selector( MainMenuScene::Menu_Exit ));

	billingItem->setPosition( ccp( -280.0f, 265.0f ));
	wellDoneItem->setPosition( ccp( 120.0f, 265.0f ));
	testViewItem->setPosition( ccp( -135.0f, 265.0f ));
	exitItem->setPosition( ccp( -335.0f, 265.0f ));

	CCMenu *testMenu = CCMenu::menuWithItems( billingItem, testViewItem, wellDoneItem, exitItem, NULL );
	testMenu->alignItemsHorizontallyWithPadding( 50.0f );
	testMenu->setPosition( ccp( Tools::GetScreenMiddleX(), Tools::GetScreenMiddleY() - 300.0f ));
	addChild( testMenu, 30 );

    //---------------
#endif


    //-----------------------
    // Play menu
    CCMenuItem *playItem;
	CCMenu *pMenu;
    
    playItem = GetMenuItem( "Play", &MainMenuScene::Menu_Play, false );
    pMenu = CCMenu::menuWithItem( playItem );
    
//	pMenu->alignItemsHorizontallyWithPadding( -50.0f );
	pMenu->setPosition( ccp( Tools::GetScreenMiddleX(), Tools::GetScreenMiddleY() ) );
    addChild( pMenu, 200 );

    
    //-----------------------
    // Settings menu
    CCMenuItem* settingsItem;
    CCMenu *settingsMenu;
    
    settingsItem = GetMenuItem( "Settings", &MainMenuScene::Menu_ShowSettings );
    settingsItem->setPosition( CCPointZero );
    
    settingsMenu = CCMenu::menuWithItem( settingsItem );
    settingsMenu->setPosition( ccp( Tools::GetScreenMiddleX() + 405.0f, Tools::GetScreenMiddleY() - 255.0f ) );

//    settingsMenu->setPosition( ccp( Tools::GetScreenMiddleX() + 275.0f, Tools::GetScreenMiddleY() - 255.0f) );
    addChild( settingsMenu, 10 );
    

//    //-----------------------
//    // Facebook menu
//    FBLoginNode *fbLogin;
//    
//    fbLogin = FBLoginNode::Create();
//    fbLogin->setPosition( ccp( Tools::GetScreenMiddleX() + 405.0f, Tools::GetScreenMiddleY() - 255.0f ) );
//    addChild( fbLogin, 10 );
    
    
    //-----------------------
    // Challenge menu
//    _menuRequest = MenuRequests::Create();
//    _menuRequest->setPosition( CCPoint( Tools::GetScreenMiddleX() - 400.0f, Tools::GetScreenMiddleY() - 255.0f ));
//    addChild( _menuRequest, 9 );
//    
//    AnyRequestNotify *notify;
//    notify = AnyRequestNotify::Create();
//    notify->setPosition( CCPoint(
//                                 _menuRequest->getPosition().x + 50.0f,
//                                 _menuRequest->getPosition().y + 30.0f ));
//    addChild( notify, 12 );

     //-----------------------
    // More games menu
//    CCMenuItem* moreGamesItem;
//    CCMenu *moreGamesMenu;
//    
//    moreGamesItem = GetMenuItem( "MoreGames", &MainMenuScene::Menu_MoreGame );
//    moreGamesItem->setPosition( CCPointZero );
//    
//    moreGamesMenu = CCMenu::menuWithItem( moreGamesItem );
//    moreGamesMenu->setPosition( ccp( Tools::GetScreenMiddleX() + 275.0f, Tools::GetScreenMiddleY() - 255.0f ));
//    addChild( moreGamesMenu, 10 );

    
    //-----------------------
    // Story menu
    CCMenuItem* storeItem;
    CCMenu *storeMenu;

    storeItem = GetMenuItem( "Story", &MainMenuScene::Menu_Story );
    storeItem->setPosition( CCPointZero );
    
    storeMenu = CCMenu::menuWithItem( storeItem );
    storeMenu->setPosition( ccp( Tools::GetScreenMiddleX() - 388.0f, Tools::GetScreenMiddleY() - 255.0f ));
    addChild( storeMenu, 10 );

    
    //-----------------------
    // Select level menu
    CCMenuItem *selectItem;
    CCMenu* selectMenu;
    
    selectItem = GetMenuItem( "SelectLevel", &MainMenuScene::Menu_SelectLevel, false );
    selectMenu = CCMenu::menuWithItem( selectItem );
    
    selectMenu->setPosition( ccp( Tools::GetScreenMiddleX() - 190.0f, Tools::GetScreenMiddleY() ) );
    addChild( selectMenu, 10 );
}
//--------------------------------------------------------------------------------------------------
void MainMenuScene::Menu_Play(CCObject* pSender)
{
    RLOG_S("MAIN_MENU", "PLAY_GAME");
    
	Game *game;
	GameStatus *gameStatus;
	GameTypes::LevelEnum level;

	game = Game::Get();
	gameStatus = game->GetGameStatus();
	level = gameStatus->GetCurrentLevel();

	//-----------------------
	// clean up 
	ExitClenaUp();

	//-----------------------
	// play
    gameStatus->ResetPreviouslyPlayedLevel();
    game->PlayNextLevel();
}
//--------------------------------------------------------------------------------------------------
void MainMenuScene::Menu_Exit(CCObject* pSender)
{
    //WWWTool::Get()->HttpGet( "http://www.piggybiggy.com/test.html" );
//    FBScoreMenu *scoreMenu;
//    
//    scoreMenu = FBScoreMenu::Create( (LevelEnum) 1 );
//    scoreMenu->setPosition( Tools::GetScreenMiddle() );
//    scoreMenu->Show();
//    addChild( scoreMenu, 200 );
////    ScoreTool::Get()->GetScoresFromServer( eLevelIntro_DestroyBlock );
    
    return;
}
//--------------------------------------------------------------------------------------------------
void MainMenuScene::Menu_Story( CCObject* sender )
{
    RLOG("STORY_SHOW");

    CCScene *scene;
    scene = MainLoadingScene::CreateScene();
    
    
    ExitClenaUp();

    TransitionScene1 *trans = TransitionScene1::transitionWithDuration( scene );
    SoundEngine::Get()->PlayEffect( SoundEngine::eSoundPageTurn );
    CCDirector::sharedDirector()->replaceScene( trans );
}
//--------------------------------------------------------------------------------------------------
void MainMenuScene::Menu_MoreGame( CCObject* sender )
{
    RLOG("MORE_GAMES");
    WWWTool::Get()->VisitWWW(Config::URLMarmaladeAppStore);
}
//--------------------------------------------------------------------------------------------------
void MainMenuScene::Menu_ShowSettings( CCObject* sender )
{
    RLOG("MENU_SETTINGS");
    
    ExitClenaUp();
    SettingsScene::ShowScene();
}
//--------------------------------------------------------------------------------------------------
void MainMenuScene::Menu_SelectLevel( CCObject* pSender )
{
    RLOG_S("MAIN_MENU", "SELECT_POCKET");
    
    ExitClenaUp();
    SelectPocketScene::ShowScene();
}
//--------------------------------------------------------------------------------------------------
void MainMenuScene::Menu_UnlockLevels( CCObject *pSender )
{
    RLOG("MENU_UNLOCK_LEVELS");

	GameStatus *gameStatus;

	gameStatus = Game::Get()->GetGameStatus();
	gameStatus->UnlockLevels();
	gameStatus->RandomStatus();

    MessageBox::CreateInfoBox("Info","Levels unlocked");
}
//--------------------------------------------------------------------------------------------------
void MainMenuScene::Menu_BillingToggle( CCObject *pSender )
{
	Game::Get()->GetGameStatus()->ToggleBilling();
}
//--------------------------------------------------------------------------------------------------
void MainMenuScene::Menu_TestView( CCObject *pSender )
{
#if !defined (BUILD_EDITOR)  && defined (BUILD_TEST)
	CCTouchDispatcher::sharedDispatcher()->removeDelegate( this );
	TestViewScene::ShowScene();
#endif
}
//--------------------------------------------------------------------------------------------------
void MainMenuScene::Menu_WellWellDoneScene( CCObject *pSender )
{
	CCTouchDispatcher::sharedDispatcher()->removeDelegate( this );
	WellWellDoneScene::ShowScene();
    //NextLevelScene::ShowScene();
}
//--------------------------------------------------------------------------------------------------
void MainMenuScene::Menu_RateReset( CCObject *pSender )
{
    ccpAppirater::RestartTracking();
}
//--------------------------------------------------------------------------------------------------
void MainMenuScene::Menu_RateShow( CCObject *pSender )
{
    ccpAppirater::RestartTracking();
    for ( int i = 0; i < 2.0f * Config::AppiraterSignificantEventsUntilPrompt; i++ )
        ccpAppirater::UserDidSignificantEvent();
}
//--------------------------------------------------------------------------------------------------
void MainMenuScene::Step( cocos2d::ccTime dt )
{
	_counter--;
	if ( ! _counter )
	{
		CCActionInterval* seq4 = (CCActionInterval*)(CCSequence::actions( 
			CCFadeTo::actionWithDuration( 0.25f, 255 ),
			CCDelayTime::actionWithDuration( 0.2f ),
			CCCallFuncND::actionWithTarget( this, callfuncND_selector( MainMenuScene::SwapDemoLevel ), (void*) _randLevel ),
			//CCMoveBy::actionWithDuration( 0.25f, CCPointZero ),
			CCDelayTime::actionWithDuration( 0.8f ),
			CCFadeTo::actionWithDuration( 0.25f, 0 ),
			NULL) );
		
		_transitionSprite->runAction( seq4 );

		_counter = Config::GameDemoChangeCounter;
		_randLevelSequece.MoveNext();
	}
}
//--------------------------------------------------------------------------------------------------
void MainMenuScene::registerWithTouchDispatcher()
{
	CCTouchDispatcher::sharedDispatcher()->addTargetedDelegate( this, Config::eMousePriority_MainMenu, true );
}
//--------------------------------------------------------------------------------------------------
CCMenuItem* MainMenuScene::GetMenuItem( const char *buttonName, menuFnPtr fn, bool anim )
{
    string buttonFile;
    string buttonSelectedFile;

    buttonFile.append( "Images/Menus/MainMenu/bt" );
    buttonFile.append( buttonName );
    buttonFile.append( ".png" );
    
    D_LOG("%s button Temp", buttonName );

    buttonSelectedFile.append( "Images/Menus/MainMenu/bt" );
    buttonSelectedFile.append( buttonName );
    buttonSelectedFile.append( "-selected.png" );

    D_LOG("%s button", buttonFile.c_str() );
    
    CCMenuItemSprite *menuItem;
    CCSprite* spriteNormal   = CCSprite::spriteWithFile(  buttonFile.c_str() );
    CCSprite* spriteSelected = CCSprite::spriteWithFile(  buttonSelectedFile.c_str() );
    CCSprite* spriteDisabled = CCSprite::spriteWithFile(  buttonFile.c_str() );

    if ( anim )
        menuItem = FXMenuItemSpriteWithAnim::Create( spriteNormal, spriteSelected, spriteDisabled, this, (SEL_MenuHandler)( fn ));
    else
        menuItem = FXMenuItemSprite::itemFromNormalSprite( spriteNormal, spriteSelected, spriteDisabled, this, (SEL_MenuHandler)( fn ));


    return menuItem;
}
//--------------------------------------------------------------------------------------------------
void MainMenuScene::onEnter()
{
	SoundEngine::Get()->GetBackgroundMusic().SetMusic_Menu();
	CCLayer::onEnter();
}
//--------------------------------------------------------------------------------------------------
void MainMenuScene::SwapDemoLevel( CCNode *node, void *data )
{
	_randLevel->removeFromParentAndCleanup( true );
	_randLevel = Game::Get()->GetLevelFactory()->CreateDemoLevel( _randLevelSequece.GetCurrent() );
	addChild( _randLevel, 5 );
}
//--------------------------------------------------------------------------------------------------
void MainMenuScene::onExit()
{
	CCLayer::onExit();
}
//--------------------------------------------------------------------------------------------------
void MainMenuScene::ExitClenaUp()
{
	unschedule( schedule_selector( MainMenuScene::Step ));

    if ( _transitionSprite )
        _transitionSprite->stopAllActions();

    if ( _randLevel )
		_randLevel->Quiting();

    CCTouchDispatcher::sharedDispatcher()->removeDelegate( this );
    
    Preloader::Get()->Reset();
    Preloader::Get()->FreeResources();
//    _menuRequest->CleanUp();
}
//--------------------------------------------------------------------------------------------------


//--------------------------------------------------------------------------------------------------


//--------------------------------------------------------------------------------------------------

