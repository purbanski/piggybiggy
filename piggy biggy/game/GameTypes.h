#ifndef __GAMETYPES_H__
#define __GAMETYPES_H__

#include <Box2D/Box2D.h>
#include <string>
#include <list>
#include <set>
#include <map>
//--------------------------------------------------------------------------------------//
using namespace std;
class puBlockDef;
//--------------------------------------------------------------------------------------//

namespace GameTypes
{
//--------------------------------------------------------------------------------------
typedef enum
{
    eGameUser_Unknown = 0,
    eGameUser_FriendLevelNotPassed,
    eGameUser_FriendLevelPassed,
    eGameUser_FriendGameNotInstalled,
    eGameUser_UnknownPlayerLevelNotPassed
} GameLevelUsersType;
//--------------------------------------------------------------------------------------
typedef enum
{
    eFBChallangeStatus_Unknow = 0,
    eFBChallangeStatus_InProgress,
    eFBChallangeStatus_Lost,
    eFBChallangeStatus_Won,
    eFBChallangeStatus_Equal
} FBChallangeStatus;
//--------------------------------------------------------------------------------------
typedef enum
{
    eFBRequest_Unknown = 0,
    eFBRequest_LevelChallange,
    eFBRequest_LevelChallagne_Accpet,
    eFBRequest_LevelChallange_Final
} FBRequestType;
//--------------------------------------------------------------------------------------
typedef map<string,string> String2StringMap;
//--------------------------------------------------------------------------------------
typedef pair<int, int> RangeInt;
//--------------------------------------------------------------------------------------//
typedef enum
{
    eLevelTag_Unknown = 0,
    eLevelTag_Bank,
    eLevelTag_Bombs,
    eLevelTag_Car,
    eLevelTag_Hacker,
    eLevelTag_Spitter,
    eLevelTag_Reel,
    eLevelTag_Balancer,
    eLevelTag_Flies,
    eLevelTag_Tetris,
    eLevelTag_Cop,
    eLevelTag_Frigle,
    eLevelTag_Thief,
    eLevelTag_Boxing,
    eLevelTag_MagnetBomb,
    eLevelTag_Button
} LevelTag;
//--------------------------------------------------------------------------------------//
typedef enum
{
    eGameEvent_Unknown = 0,
    eGameEvent_FBLevelCompleted,
    eGameEvent_FBLevelSkipped,
    eGameEvent_FBScoreUpdate
} GameWatcherEvents;
//--------------------------------------------------------------------------------------//
typedef enum {
    eLevelTypeNormal = 0,
    eLevelTypeVertical,
    eLevelTypeAccelerometr,
    eLevelTypeCar,
    eLevelTypeDemo
} LevelType;
//--------------------------------------------------------------------------------------//
typedef enum {
		MEMBUCKET_FIXED = 1
} MemoryBucket;
//--------------------------------------------------------------------------------------//
typedef enum {
    eBillingTransactionErrorUnknown = 0,
    eBillingTransactionBlank,
    eBillingTransactionProductNotFound,
    eBillingTransactionCanceled,
    eBillingTransactionFinished,
} BillingTransactionState;
//--------------------------------------------------------------------------------------//
typedef enum {
	eBlockNone = 0,
	eBlockCoin,
	eBlockCoinSilver,
	eBlockCoinStatic,
	eBlockPiggyBank,
	eBlockPiggyBankRolled,
	eBlockPiggyBankStatic,
	eBlockThief,
	eBlockThiefRolled,
	eBlockThiefStatic,
	eBlockCop,
	eBlockSpitter,
	eBlockSpitterExplosive,
	eBlockButton,
	
	eBlockCircleWall,
	eBlockCircleFragile,
	eBlockCircleFragileStatic,
	eBlockBoxerGlove,
	eBlockTyre,

	eBlockBoxWall,
	eBlockBoxFragile,
	eBlockBoxFragileStatic,

	eBlockBoxWood,
	eBlockBoxLiftUp,
	eBlockBoxWood2,
	eBlockBoxBounce,
	eBlockBoxIron,

	eBlockCashCow,
	eBlockCashCowStatic,
	eBlockPrison,
	eBlockMotherPiggy,
	eBlockMotherPiggyStatic,
	
	eBlockScrew,
	eBlockBomb,
	eBlockBombStatic,
	eBlockMagnetBomb,
	eBlockMagnetBombPlus,
	eBlockMagnetBombMinus,
	eBlockBombRange,

	eBlockPolygonFragileCustom1,
	eBlockPolygonFragileCustom2,
	eBlockPolygonCoinCustom1,

	eBlockBankSaveWheel,
	eBlockChain,
	eBlockBullet,
	eBlockVehicalButtonLeft,
	eBlockVehicalButtonRight,
	eBlockGunSensor,
	eBlockBankLockCogWheel,
	eBlockChainBit,
	eBlockBankLock,
	eBlockBankLockBars,
	eBlockBankBorderWall,
	eBlockVehical,
	eBlockReel,
	eBlockVehicalControls,
	eBlockBoxerHand,
	eBlockPolygonWall_Custom1,
	eBlockPolygonWall_Custom2,
	eBlockNew5,

	eBlockTetrisS,
	eBlockTetrisSM,
	eBlockTetrisT,
	eBlockTetrisL,
	eBlockTetrisLM,
	eBlockTetrisI,
	eBlockTetrisSquare,
	eBlockMagnet,
	eBlockMagnetPlus,
	eBlockMagnetMinus,
	eBlockBorder,
} BlockEnum;
//--------------------------------------------------------------------------------------//
struct SecondsHolder
{
	SecondsHolder() 
		{ _seconds = 0; }
	SecondsHolder( int seconds ) 
		{ _seconds = seconds; }

	unsigned int _seconds;

	unsigned int GetDaysCount();
	unsigned int GetHoursCount();
	unsigned int GetMinutesCount();
	unsigned int GetSecondsCount();

	void Reset();
};

//--------------------------------------------------------------------------------------//
// One Time Messages
//--------------------------------------------------------------------------------------//
typedef enum
{
	eOTMsg_First = 0,
	eOTMsg_IntroBlockDestroy = 0,
	eOTMsg_IntroWelcome,
	eOTMsg_IntroGun,
	eOTMsg_IntroMoneyMakeMoney,
	eOTMsg_IntroRope,
	eOTMsg_IntroRell,
	eOTMsg_IntroTetirs,
	eOTMsg_IntroBank1,
	eOTMsg_IntroBank2,
	eOTMsg_IntroBank3,
    eOTMsg_SkipLevelIntro,
    eOTMsg_AccelIntro,
    eOTMsg_CarIntroA,
    eOTMsg_CarIntroB,
	eOTMsg_Last
} OneTimeMsg;
//--------------------------------------------------------------------------------------
typedef enum
{
	eOTMsgRequest_First = 0,
	eOTMsgRequest_ChooseLevel = 0,
	eOTMsgRequest_ChooseFriend,
	eOTMsgRequest_CheckChallangeRequests,
	eOTMsgRequest_ChallangeStats,
	eOTMsgRequest_Last
} OneTimeMsgRequests;
//--------------------------------------------------------------------------------------//
typedef enum 
{
	eShape_Unknown = 0,
	eShape_Circle,
	eShape_Box,
	eShape_Polygon
} ShapeType;
//--------------------------------------------------------------------------------------//
typedef enum 
{
	// must be sequential the same with
	// SelectPocketScene::ConstructSlidingMenu
	ePocketNone = 0,
	ePocketBlue,
	ePocketMoro,
	ePocketRasta,
	ePocketJapan,
	ePocketScience
	/*ePocketRed,*/
} PocketType;
//--------------------------------------------------------------------------------------//
typedef enum 
{
	ePocketStatusLocked = 0,
	ePocketStatusOpen,
	ePocketStatusAlmostCompleted,
	ePocketStatusCompleted
	/*ePocketRed,*/
} PocketStatusType;

//--------------------------------------------------------------------------------------//
typedef enum
{
	eLevel_Horizontal = 0,
	eLevel_Vertical
} LevelOrientation;
//--------------------------------------------------------------------------------------//
typedef enum 
{
	eLevel_Start = 0,
	
	eLevelIntro_Welcome = 0,
	eLevelIntro_DestroyBlock,
	eLevelIntro_Gun,
	eLevelIntro_Rope,
	eLevelIntro_MoneyMakeMoney,
	//eLevelIntro_3,


	eLevelOthers_1,
	eLevelOthers_2,
	eLevelOthers_3,
	eLevelOthers_4,
	eLevelOthers_5,
	eLevelOthers_6,
	eLevelOthers_7,

	eLevelSyncronize_1,
	eLevelSyncronize_2,
	eLevelSyncronize_3,

	eLevelMagnet_1,
	eLevelMagnet_2,
	eLevelAdamS_PlainLogic,

	eLevelBomb_1,
	eLevelBomb_1b,
	eLevelBomb_2,
	eLevelBomb_3,
	eLevelBomb_4,
	eLevelBomb_5,
	eLevelBomb_6,
	eLevelBomb_7,
	eLevelBomb_8,

	eLevelRoll_1,
	eLevelRoll_2,
	//eLevelRoll_3,
	eLevelRotateL,

	eLevelSwing_1,
	eLevelSwing_2,
	eLevelSwing_Impossible,

	eLevelTraps_1,
	eLevelTraps_2,
	eLevelTraps_3,

	eLevelScale_1,
	//eLevelTraps_2,
	//eLevelTraps_3,

	eLevelTetris_1,
	eLevelTetris_2,
	//eLevelTetris_3,

	eLevelDomino_1,
	//eLevelDomino_2,
	//eLevelDomino_3,

	eLevelConstruction_1a,
	eLevelConstruction_1b,
	eLevelConstruction_1c,
	eLevelConstruction_1d,

	eLevelConstruction_2a,

	eLevelConstruction_3a,
	
	eLevelConstruction_4a,
	eLevelConstruction_4b,
	eLevelConstruction_4c,

	eLevelGun_1,
	eLevelGun_2,
	eLevelGun_3,
	eLevelGun_4,

	eLevelReel_1,
	eLevelReel_2,
	eLevelReel_3,

	eLevelChain_1,
	eLevelChain_2a,
	eLevelChain_2b,
	eLevelChain_2c,
	eLevelChain_3,
	eLevelChain_4,
	eLevelChain_5,
	eLevelChain_6,
	eLevelChain_7a,
	eLevelChain_7b,

	eLevelBank_1,
	eLevelBank_2,
	eLevelBank_3,
	eLevelBank_Impossible,
	eLevelBank_Impossible_bak,

	eLevelRotate_1,
	eLevelRotate_2,
	eLevelRotate_3,
	eLevelRotate_4,
	eLevelRotate_5,
	eLevelRotate_6,
	eLevelRotate_7,
	eLevelRotate_Circle,

	eLevelLabyrinth_1,
	eLevelLabyrinth_Imposible,

	// rafal
	eLevelPathfinder_1,
	eLevelScale_2,
	eLevelScale_3,
	eLevelBombAndRoll_1,
	eLevelTunnel_1,
	eLevelExplosivePath,
	eLevelBouncingCoin_1,
	eLevelSpiderWeb,
	
	// pawel dabrowski
	eLevelPawel_1,
	eLevelPawel_2,
	eLevelPawel_3,
	eLevelPawel_4,
	eLevelPawel_5,
	eLevelPawel_6,
	eLevelPawel_7,
	eLevelPawel_8,
	eLevelPawel_9,
	eLevelPawel_10,
	eLevelPawel_11,
	eLevelPawel_12,
	eLevelPawel_13,
	eLevelPawel_14,
	eLevelPawel_15,
	eLevelPawel_16,
	eLevelPawel_17,

	// tomek wiertel
	eLevelTomek_1,
	eLevelTomek_2,
	eLevelTomek_3,
	eLevelTomek_4,
	eLevelTomek_CalmWood,
	eLevelTomek_SelfMadeCannon,

	// Jacek Zielinski
	eLevelJacekZ_1,

	//Marzena Kotaraska czy kantorska
	eLevelMarzena_1,

	// Bogusia
	eLevelBogusia_GoUnder,
	eLevelBogusia_2,
	eLevelBogusia_3,
	eLevelBogusia_4,

	// Lukasz Salata
	eLevelLukasz_1a,
	eLevelLukasz_1b,
	eLevelLukasz_2,
	eLevelLukasz_3,
	
	eLevelVehical_2,
	eLevelVehical_3,
	eLevelVehical_4,

	eLevelDemo_0,
	eLevelDemo_1,
	eLevelDemo_2,
	eLevelDemo_3,

	// editor 
	eLevelTestUser,
	eLevelTestSpace,
	eLevelTestSea,
	eLevelTestAquarium,
	eLevelTestAquariumBlank,
	
	eLevelTestDestroyBlocks,
	
	//eLevelEditorSea,
	
	//eLevelEditorEmptySkin,
	eLevelEditorScreenshot,
	eLevelEditorEmpty,
	eLevelEditorWithSkin,
	eLevelVehical_1,
	eLevelTestAccelerometr,
	eLevelTestVertical,
	eLevelEditorAquarium,

	eLevelKonkurs,
	eLevel_T1,
	eLevel_T2,
	eLevel_T3,
	eLevel_T4,
	eLevel_T5,

	eLevel_Finish
} LevelEnum;

typedef enum
{
	eLevelScaleLayer		= 230,
	eSpritesScreenshot      = 225,
	eLevelFinishMenu		= 220,
    eSpritesFBHighScore     = 215,
	eSpritesPocketOpen		= 210,
	eSpritesOrientMsg		= 200,
	eLevelClueScreen		= 195,
	eLevelScoreStopper		= 192,
    eSpritesSkipLevelIntro  = 191,
	eLevelMenu				= 190,
	eLevelMenuButtons		= 187,
	eLevelHoldFlatMsg		= 185,
	eLevelMsgAbove			= 180,
	eLevelMsg				= 178,
	eLevelMsgBelow			= 176,
	eLevelEditorMsg			= 174,
	eLevelClueManager		= 172,
	eLevelDebug				= 170,
	eSpriteNotifiers		= 160,
	eSpriteAnimFlies		= 155,
	eLevelAnimFront			= 150,
	eSpritesControls		= 140, // for vehical
	eSpritesEffects			= 130,
	eSpritesBombExplosion	= 130,
	eSpritesPiggyBankAbove	= 128,
	eSpritesPiggyBank		= 127,
	eSpritesCharacterAbove	= 126,
	eSpritesCharacter		= 125,
	eSpritesCharacterBelowe	= 124,
	eSpritesBlockWithRange	= 122,
	eSpritesBlockRange		= 121,
	eSpritesAboveX3			= 120,
	eSpritesAboveX2			= 115,
	eSpritesAbove			= 110,
	eSprites				= 100,
	eSpritesWood			= 100,
	eSpritesBelowe			= 90,
	eSpritesBeloweX2		= 80,
	eSpritesBeloweX3		= 70,
	eLevelScoreCoins		= 65,
	eSpritesWallAboveX2		= 62,
	eSpritesWallAbove		= 61,
	eSpritesWall			= 60,
	eSpritesWallBelowe		= 59,
	eSpritesWallBeloweX2	= 58,
	eSpritesShadows			= 55,
	eSpritesGlue			= 52,
	eLevelAnimBack			= 50,
	eSpritesRain			= 45,
	eBackground				= 20,
	eSpritesHidden			= 10,
	eNone					= -1
} SpriteLayers;

typedef enum
{
	eLevelState_Running		= 0x01,
	eLevelState_Finished	= 0x02,
	eLevelState_Failed		= 0x04,
	eLevelState_Skipped		= 0x08,
} LevelState;

typedef enum 
{
	eMoveable			= 0x0000001,	// body can be moved by user toch
	eDestroyable		= 0x0000002,	// body can be destroy
	eDynamic			= 0x0000004,	// body can move but only by other object actions
	eActivable			= 0x0000008,	// body can be activated by the touch
	eCoin				= 0x0000010,	// coin which needs to get to piggy bank
	ePiggyBank			= 0x0000020,	// self explaining - but if you still don't get it - this is the destination for a coin
	eCop				= 0x0000040,
	eThief				= 0x0000080,
	eOther				= 0x0000100,
	eSwitch				= 0x0000200,
	eTouchSwitch		= 0x0000400,
	eSwat				= 0x0000800,
	eBorder				= 0x0001000,
	eVehical			= 0x0002000,
	eBullet				= 0x0004000,
	eBulletExplosive	= 0x0008000,
	eCutable			= 0x0010000,
	//ePiggyIron		= 0x020000,
	eBlockRange			= 0x0040000,
	eIron				= 0x0080000,
	eCoinSilver			= 0x0100000,
	eAirBubble			= 0x0200000,
	eGlass				= 0x0400000,
	eCustom				= 0x0800000,
	eWall				= 0x1000000,
} BlockType;

typedef enum
{
	eAnim_None = 0,
	eAnim_SpitterSpit,
	eAnim_SpitterSpitEmpty,
	eAnim_SpitterBounce,
	eAnim_SpitterExplosiveSpit,
	eAnim_SpitterExplosiveSpitEmpty,
	eAnim_SpitterExplosiveBounce,
	
	eAnim_PiggyBankBounce,
	eAnim_PiggyBankBounce_Sit,
	eAnim_PiggyBankBounce_Roll,

	eAnim_PiggyBankToRolling,
	eAnim_PiggyBankToSitting,
		
	eAnim_PiggyBankLevelFailed,
	eAnim_PiggyBankLevelFailed_Sit,
	eAnim_PiggyBankLevelFailed_Roll,
	
	eAnim_PiggyBankLevelDone,
	eAnim_PiggyBankLevelDone_Sit,
	eAnim_PiggyBankLevelDone_Roll,
	
	eAnim_PiggyBankCoinCollected,
	eAnim_PiggyBankCoinCollected_Sit,
	eAnim_PiggyBankCoinCollected_Roll,

	eAnim_PiggyBankStolen,
	eAnim_PiggyBankStolen_Sit,
	eAnim_PiggyBankStolen_Roll,

	eAnim_PiggyBankLookAround,
	eAnim_PiggyBankUaa,
	eAnim_PiggyBankEeeOoo,
	eAnim_PiggyBankSalto,
	eAnim_PiggyBankFiuFiu,
	eAnim_PiggyBankEyesBlink,
	eAnim_PiggyBankHappyJump,
	eAnim_PiggyBankNoseBubble,

	eAnim_ThiefSmiles,
	eAnim_ThiefSmiles_Sit,
	eAnim_ThiefSmiles_Roll,
	eAnim_ThiefSitBounce,
	eAnim_ThiefRollBounce,
	eAnim_ThiefLookAround,
	eAnim_ThiefToRolling,
	eAnim_ThiefToSitting,

	eAnim_CashCowEyesBlink,

	eAnim_CoinFlash1,
	eAnim_CoinFlash2,

	eAnim_CopSitBounce,
	eAnim_CopRollBounce,
	eAnim_CopJump,
	eAnim_CopToRolling,
	eAnim_CopToSitting,
	eAnim_CopSmiles,	 // when thief caught
	eAnim_CopSmiles_Sit, 
	eAnim_CopSmiles_Roll,

	eAnim_RotationFix,
	eAnim_CopRotationFix,
	eAnim_ThiefRotationFix,
	eAnim_PiggyBankRotationFix,
} CharactersAnim;

typedef enum
{
	eAnimState_None = 0,
	eAnimState_Running
} AnimState;

typedef enum 
{
	eSpriteState_Sit = 0,
	eSpriteState_SitAnimating,
	eSpriteState_RollAnimating,
	eSpriteState_Roll,
} AnimSpriteState;

typedef enum
{
	eAnimPiority_Low = 1,
	eAnimPiority_Normal,
	eAnimPiority_High,
	eAnimPiority_Highest
} CharactersAnimPiority;

typedef enum 
{ 
	eRot0 = 1, 
	eRot90, 
	eRot180, 
	eRot270 
} TetrisBlockRotation;

// Chain const info

struct ChainConstInfo
{
	typedef enum 
	{
		eChainByUnknown = 1,
		eChainByPoints,
		eChainByRadius,
		eChainByRadiusAndLength,
		eChainByLength,
		eChainByCubicLengths
	} Type;

	ChainConstInfo();
	ChainConstInfo( const b2Vec2& chainFixPoint, const b2Vec2& chainEndPoint );
	ChainConstInfo( float radius, float piece );
	ChainConstInfo( Type type, float f1, float f2, float f3 );
	ChainConstInfo( float length );

#ifdef BUILD_EDITOR
	ChainConstInfo( const puBlockDef *blockDef );
#endif

	b2Vec2	_chainFixPoint;
	b2Vec2	_chainEndPoint;
	float	_radius;
	float	_piece;
	float	_lengthA;
	float	_lengthB;
	float	_lengthC;
	Type	_type;
	int		_hookedBlockUID;
	bool	_isHookedBlockFixed;
};

};

#endif
