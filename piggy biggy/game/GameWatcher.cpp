#include <cstddef>
#include "GameWatcher.h"
#include "Debug/MyDebug.h"
#include "Game.h"
#include "GameConfig.h"
#include "Tools.h"
#include <cocos2d.h>
#include "Promotion/FBPoster.h"
#include "Facebook/FBTool.h"

USING_NS_CC;
//------------------------------------------------------------------------
GameWatcher* GameWatcher::_sInstance = NULL;
//------------------------------------------------------------------------
GameWatcher* GameWatcher::Get()
{
    if ( ! _sInstance )
    {
        _sInstance = new GameWatcher();
    }
    return _sInstance;
}
//------------------------------------------------------------------------
void GameWatcher::Destroy()
{
    if ( _sInstance )
    {
        delete _sInstance;
        _sInstance = NULL;
    }
}
//------------------------------------------------------------------------
GameWatcher::GameWatcher()
{
    _eventRandomLimitsMap[ eGameEvent_FBLevelCompleted ] = Config::Watcher_Activate_LevelCompleted;
    _eventRandomLimitsMap[ eGameEvent_FBLevelSkipped ]   = Config::Watcher_Activate_LevelSkipped;
    _eventRandomLimitsMap[ eGameEvent_FBScoreUpdate ]  = RangeInt(1, 1);
    
    _eventActivateCountMap[ eGameEvent_FBLevelCompleted ] = 0;
    _eventActivateCountMap[ eGameEvent_FBLevelSkipped ]   = 0;
    _eventActivateCountMap[ eGameEvent_FBScoreUpdate ]  = 0;
    
    _activateFnMap[ eGameEvent_FBLevelCompleted ] = &Activate::LevelCompleted;
    _activateFnMap[ eGameEvent_FBLevelSkipped]    = &Activate::LevelSkipped;
    _activateFnMap[ eGameEvent_FBScoreUpdate ]  = &Activate::FBScoreUpdate;
    
    _tagToPostMap[ eLevelTag_Unknown   ] = FBPoster::eFBPost_LevelAnyCompleted;
    _tagToPostMap[ eLevelTag_Bank      ] = FBPoster::eFBPost_LevelBankCompleted;
    _tagToPostMap[ eLevelTag_Balancer  ] = FBPoster::eFBPost_LevelBalancerCompleted;
    _tagToPostMap[ eLevelTag_Bombs     ] = FBPoster::eFBPost_LevelBombsCompleted;
    _tagToPostMap[ eLevelTag_Car       ] = FBPoster::eFBPost_LevelCarCompleted;
    _tagToPostMap[ eLevelTag_Hacker    ] = FBPoster::eFBPost_LevelHackerCompleted;
    _tagToPostMap[ eLevelTag_Reel      ] = FBPoster::eFBPost_LevelReelCompleted;
    _tagToPostMap[ eLevelTag_Spitter   ] = FBPoster::eFBPost_LevelSpitterCompleted;

    SetActivateLimits();
}
//------------------------------------------------------------------------
GameWatcher::~GameWatcher()
{
    _eventRandomLimitsMap.clear();
    _eventActivateCountMap.clear();
}
//------------------------------------------------------------------------
void GameWatcher::RecordEvent( GameWatcherEvents event, void *data )
{
    if ( ! FBTool::Get()->IsLogged() )
        return;
    
    if ( _eventActivateCountMap.find(event) != _eventActivateCountMap.end() )
    {
        if ( _eventActivateCountMap[ event ] > 0  )
            _eventActivateCountMap[ event ]--;
        else
        {
            CCAssert(false, "wrong event activate count" );
        }
        D_INT(_eventActivateCountMap[ event ]);
    }
    else
    {
        CCAssert(false, "activate count index not found in map" );
    }
    
    Dump();
    Process();
}
//------------------------------------------------------------------------
void GameWatcher::Dump()
{
    for ( EventMap::iterator it = _eventActivateCountMap.begin(); it != _eventActivateCountMap.end(); it++ )
    {
        D_LOG("Event: %i count: %i", it->first, it->second );
    }
}
//------------------------------------------------------------------------
void GameWatcher::Process()
{
    for ( EventMap::iterator it = _eventActivateCountMap.begin(); it != _eventActivateCountMap.end(); it++ )
    {
        if ( it->second == 0 )
        {
            // set new count
            it->second = Tools::Rand( _eventRandomLimitsMap[ it->first ].first, _eventRandomLimitsMap[ it->first ].second );
            
            //excute
            FnPtr fn;
            fn = _activateFnMap[ it->first ];
            if ( fn )
                (*fn)();
        }
 
        D_LOG("Event: %i count: %i", it->first, it->second );
    }
}
//------------------------------------------------------------------------
void GameWatcher::SetActivateLimits()
{
    for ( EventMap::iterator it = _eventActivateCountMap.begin(); it != _eventActivateCountMap.end(); it++ )
    {
        if ( it->second == 0 )
        {
            it->second = Tools::Rand( _eventRandomLimitsMap[ it->first ].first, _eventRandomLimitsMap[ it->first ].second );
            D_INT( it->second );
        }
    }
}



//------------------------------------------------------------------------
// Activate Functions
//------------------------------------------------------------------------
void GameWatcher::Activate::LevelSkipped()
{
    LevelTag levelTag;
    LevelEnum levelEnum;
    
    levelTag = Game::Get()->GetRuningLevel()->GetLevelTag();
    levelEnum = Game::Get()->GetRuningLevel()->GetLevelEnum();

    FBPoster::Get()->Post_LevelSkipped( levelEnum, levelTag );
}
//------------------------------------------------------------------------
void GameWatcher::Activate::LevelCompleted()
{
    LevelTag levelTag;
    LevelEnum levelEnum;
    
    levelTag = Game::Get()->GetRuningLevel()->GetLevelTag();
    levelEnum = Game::Get()->GetRuningLevel()->GetLevelEnum();

    FBPoster::Get()->Post_LevelCompleted( levelEnum, levelTag );
}
//------------------------------------------------------------------------
void GameWatcher::Activate::FBScoreUpdate()
{
    int totalScore;
    totalScore = Game::Get()->GetGameStatus()->GetTotalScore();
    FBTool::Get()->PostScore( totalScore );
//
//    //----
//    int currentLevelScore;
//    currentLevelScore = Game::Get()->GetGameStatus()->GetLevelScore( Game::Get()->GetGameStatus()->GetCurrentLevel() );
//    
//    stringstream ss;
//    ss << "Level score: " << currentLevelScore << " ";
//    ss << "Total score: " << totalScore << " ";
    
//    FBTool::Get()->StatusUpdate( ss.str().c_str() );
}
//------------------------------------------------------------------------
