#ifndef __SCALESCREENLAYER_H__
#define __SCALESCREENLAYER_H__

#include "cocos2d.h"

USING_NS_CC;

class ScaleScreenLayer : public CCLayer
{
public:
	LAYER_NODE_FUNC( ScaleScreenLayer );
	bool init();
	
	virtual void registerWithTouchDispatcher(void);
	virtual void onExit();

	~ScaleScreenLayer();

private:
	ScaleScreenLayer();
	

private:
	float	_scale;
	bool	_miniaized;
};

#endif
