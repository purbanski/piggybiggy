#ifndef __WWWTOOL_H__
#define __WWWTOOL_H__
//---------------------------------------------------------------------------------
#include <map>
#include <set>
#include <vector>
//---------------------------------------------------------------------------------
using namespace std;
//---------------------------------------------------------------------------------
class WWWToolListener
{
public:
    virtual void Http_FailedWithError( void *connection, int error ) = 0;
    virtual void Http_FinishedLoading( void *connection, const void *data, int len ) = 0;
};
//---------------------------------------------------------------------------------
class WWWTool
{
public:
	static WWWTool* Get();
	static void Destroy();

	~WWWTool();
    
    void VisitWWW( const char *url );
    void HttpGet( const char *url, WWWToolListener *listener = NULL );
    void CancelHttpGet( WWWToolListener *listener = NULL );
    
    virtual void Http_FailedWithError( void *connection, int error );
    virtual void Http_FinishedLoading( void *connection, const void *data, int len );

private:
	WWWTool();

private:
 	static WWWTool *_sInstance;
    
    typedef map<void*,WWWToolListener *> ListenersMap;
    ListenersMap    _listenersMap;
};
//---------------------------------------------------------------------------------

#endif
