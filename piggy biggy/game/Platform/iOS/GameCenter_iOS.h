#import <Foundation/Foundation.h>
#import <Foundation/NSObject.h>
#import <GameKit/GameKit.h>
#import <GameKit/GKAchievementViewController.h>
#import "cocos2dx/platform/ios/EAGLView.h" 
#import <UIKit/UIKit.h>


@interface GameCenter_iOS : UIViewController<GKGameCenterControllerDelegate>
+ (GameCenter_iOS *)sharedInstance;
+ (void)destroy;

//- (void) showGameCenter;
- (void) authenticateLocalPlayer;
- (void) playerAuthOK;
- (void) playerAuthFailed:(NSError*) error;
- (void) gameCenterViewControllerDidFinish:(GKGameCenterViewController *)gameCenterViewController;
- (void) reportScore: (int64_t) score forLeaderboardID: (NSString*) category;
@end
