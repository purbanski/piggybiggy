#include "../LangShim.h"
#include "Lang_iOS.h"
#include "GameGlobals.h"
#include "cocos2d.h"
//----------------------------------------------------------------------
const char* LangShim::Translate( const char *key, const char *table )
{
    return [[Lang_iOS sharedInstance] translate:key table:table];
}
//----------------------------------------------------------------------
const char* LangShim::Translate( const char *key, const char *table, cocos2d::LanguageType lang )
{
    return [[Lang_iOS sharedInstance] translate:key table:table language:lang];
}
//----------------------------------------------------------------------
void LangShim::SetLang( cocos2d::LanguageType lang )
{
    [[Lang_iOS sharedInstance] setDefaultLang: lang];
}
//----------------------------------------------------------------------



//----------------------------------------------------------------------