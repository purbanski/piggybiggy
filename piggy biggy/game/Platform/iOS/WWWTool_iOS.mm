 #import "WWWTool_iOS.h"
#include <sstream>

static WWWTool_iOS *gWWWTool = NULL;

//------------------------------------------------------------------------
// WWWTool_iOS
//------------------------------------------------------------------------
@implementation WWWTool_iOS

@synthesize connectionData=_connectionData;

//------------------------------------------------------------------------
// statics
//------------------------------------------------------------------------
+ (WWWTool_iOS *)sharedInstance
{
  if ( ! gWWWTool )
    {
        gWWWTool = [[ WWWTool_iOS alloc] init ];
    }
    
    return gWWWTool;
}
//------------------------------------------------------------------------
+ (void)destroy
{
    if ( gWWWTool )
        [ gWWWTool dealloc ];
    
    gWWWTool = NULL;
}

//------------------------------------------------------------------------
// methods
//------------------------------------------------------------------------
-(id)init
{
    [super init];
    self.connectionData =[[NSMutableDictionary alloc] init];
    return self;
}
//------------------------------------------------------------------------
-(void)dealloc
{
    [self releaseConnectionData];
    [super dealloc ];
}
//------------------------------------------------------------------------
-(NSURLConnection*) httpGet:(const char *)url
{
//    NSLog(@"http get: %s", url );
    
    NSURLRequest *theRequest;
    NSURLConnection *connection;

    NSString *urlStringTemp;
    urlStringTemp = [NSString stringWithFormat:@"%s",url];

    NSString *urlString = [urlStringTemp stringByReplacingOccurrencesOfString:@" " withString:@"_"];
    
    theRequest = [NSURLRequest requestWithURL:[NSURL URLWithString: urlString]
                                  cachePolicy:NSURLRequestUseProtocolCachePolicy
                              timeoutInterval:60.0 ];
    
    connection = [[NSURLConnection alloc] initWithRequest:theRequest delegate:self];
    
           
    NSMutableData *storeData = [[NSMutableData alloc ] init ];
    NSString *connId;
        
    connId = [NSString stringWithFormat:@"%i", [connection hash]];
    [self.connectionData setObject:storeData forKey:connId];
    
    return connection;
}
//--------------------------------------------------------------------------------------------
- (void)visitWWW:(const char *)url
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString: [ NSString stringWithFormat:@"%s", url ]]];
}
//--------------------------------------------------------------------------------------------
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
//    NSLog(@"didRecRespons %i", [connection hash] );

    NSMutableData *mydata;
    NSString *connId;
        
    connId = [NSString stringWithFormat:@"%i", [connection hash]];
    mydata = [self.connectionData objectForKey:connId];
    [mydata appendData:data];
}
//--------------------------------------------------------------------------------------------
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
//    NSLog(@"didReceiveData %i", [connection hash] );
//    NSLog(@"%s", response )
    NSString *connId;
    NSMutableData *data;
    
    connId = [NSString stringWithFormat:@"%i", [connection hash]];
    data = [self.connectionData objectForKey:connId];
    
    if ( data != nil )
    {
        [ data setLength:0];
        NSLog(@"http received data reset --- ");
    }
}
//--------------------------------------------------------------------------------------------
- (void)connection: (NSURLConnection *)connection didFailWithError:(NSError *)error
{
    NSLog(@"didFail %i", [connection hash] );
    WWWTool::Get()->Http_FailedWithError((void*) connection, 1 );
    [self releaseConnectionDataForObject:connection];
    [ connection release ]; 
}
//--------------------------------------------------------------------------------------------
- (void)connectionDidFinishLoading: (NSURLConnection *)connection
{
//    NSLog(@"didFInish %i", [connection hash] );
    
    NSData *data;
    NSString *connId;

    connId = [NSString stringWithFormat:@"%i", [connection hash]];
    data = [self.connectionData objectForKey:connId];
    
#ifdef DEBUG
//    NSString *dataStr;
//    dataStr = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
//    NSLog(@"data: %s", [dataStr UTF8String ]);
#endif
    
    WWWTool::Get()->Http_FinishedLoading( (void*) connection, [data bytes], [data length] );
    
    [self releaseConnectionDataForObject:connection];
    [connection release ];
}
//-------------------------------------------------------------------------------------------
- (void)releaseConnectionData
{
    [self.connectionData enumerateKeysAndObjectsUsingBlock:^(id key, id object, BOOL *stop)
    {
        [object release];
    }];
    
    [self.connectionData removeAllObjects];
    [self.connectionData release];
}
//-------------------------------------------------------------------------------------------
- (void)releaseConnectionDataForObject:(NSURLConnection*)connection
{
    NSData *data;
    NSString *connId;

    connId = [NSString stringWithFormat:@"%i", [connection hash]];
    data = [self.connectionData objectForKey:connId];
    
    [self.connectionData removeObjectForKey:connId];
    [data release];
}
//-------------------------------------------------------------------------------------------

@end
