#import <Foundation/Foundation.h>
#import <Foundation/NSObject.h>
#import "Platform/WWWTool.h"
#include <string>

@interface WWWTool_iOS : NSObject<NSURLConnectionDelegate>
//----------------------------------------------------------------
@property (strong, nonatomic) NSMutableDictionary *connectionData;

//----------------------------------------------------------------
+ (WWWTool_iOS *)sharedInstance;
+ (void)destroy;
//----------------------------------------------------------------
-(NSURLConnection*) httpGet:(const char *)url;
-(void) visitWWW:(const char *)url;
//----------------------------------------------------------------
@end
