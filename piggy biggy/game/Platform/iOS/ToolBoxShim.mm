#include "../ToolBoxShim.h"
#import "ToolBox_iOS.h"
//-------------------------------------------------------------
DeviceType ToolBoxShim::GetDeviceType()
{
    return [ ToolBox_iOS getDeviceType ];
}
//-------------------------------------------------------------
string ToolBoxShim::GetMacAddress()
{
    string ret;
    ret.append( [[ToolBox_iOS getMacAddress] UTF8String] );

    return ret;
}
//-------------------------------------------------------------
string ToolBoxShim::GetMacHash()
{
    string ret;
    ret.append( [[ToolBox_iOS getHashMacAddress] UTF8String] );
    return ret;
}
//-------------------------------------------------------------
string ToolBoxShim::GetMD5( const char *msg )
{
    string ret;
    NSString *str;

    str = [ ToolBox_iOS sha1:[ NSString stringWithUTF8String: msg ]];
    ret.append( [str UTF8String] );
    return ret;
}
//-------------------------------------------------------------
void ToolBoxShim::ShowSendEmailView( const char *cc, const char *subject )
{
    [ToolBox_iOS showSendEmailView];
}
//-------------------------------------------------------------
bool ToolBoxShim::ShouldRotate90Degree()
{
    return [ToolBox_iOS is90RotationNeeded ];
}
//-------------------------------------------------------------
std::string ToolBoxShim::UTF8Encode( const char *msg )
{
    string ret;
    ret.append([[ToolBox_iOS utf8Encode:msg] UTF8String]);
    return ret;
}
//-------------------------------------------------------------
void ToolBoxShim::DebugMsgBox( const char *title, const char *msg )
{
    [ToolBox_iOS debugMsgBox:[NSString stringWithUTF8String:title]
                     message:[NSString stringWithUTF8String:msg]
                       error:nil];
}
//-------------------------------------------------------------
string ToolBoxShim::GetChallenge()
{
    string ret;
    ret.append( [[ToolBox_iOS getChallange] UTF8String] );
    return ret;
}
//-------------------------------------------------------------
cocos2d::CCSprite* ToolBoxShim::SpriteFromData( const void *data, int len )
{
    return [ToolBox_iOS spriteFromData:data len:len];
}
//-------------------------------------------------------------
cocos2d::CCSprite* ToolBoxShim::SpriteFromPlatofrmImage( void *data )
{
    return [ToolBox_iOS spriteFromPlatofrmImage:data];
}
//-------------------------------------------------------------


