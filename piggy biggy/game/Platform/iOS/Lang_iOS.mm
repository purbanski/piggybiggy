#import "Lang_iOS.h"

static Lang_iOS *gLang = NULL;

//------------------------------------------------------------------------
// Lang_iOS
//------------------------------------------------------------------------
@implementation Lang_iOS
{
    NSString *_lang;
}

//------------------------------------------------------------------------
// static
//------------------------------------------------------------------------
+ (Lang_iOS *)sharedInstance
{
  if ( ! gLang )
        gLang = [[ Lang_iOS alloc] init ];
    
    return gLang;
}
//------------------------------------------------------------------------
+ (void)destroy
{
    if ( gLang )
        [ gLang dealloc ];
    
    gLang = NULL;
}

//------------------------------------------------------------------------
// methods
//------------------------------------------------------------------------
-(id)init
{
    [super init];
    return self;
}
//------------------------------------------------------------------------
-(void)dealloc
{
    [_lang release];
    [super dealloc];
}
//------------------------------------------------------------------------
-(const char*)translate:(const char*)key table:(const char*)table
{
    NSString *translated;
    
    translated = [[self currentLanguageBundle] localizedStringForKey:[NSString stringWithFormat:@"%s", key]
                                                        value:@""
                                                        table:[NSString stringWithFormat:@"%s", table]];
    return [translated UTF8String ];
}
//------------------------------------------------------------------------
-(const char*)translate:(const char*)key table:(const char*)table language:(cocos2d::LanguageType)language
{
    NSString *translated;
    
    translated = [[self customLanguageBundle:language] localizedStringForKey:[NSString stringWithFormat:@"%s", key]
                                                                      value:@""
                                                                      table:[NSString stringWithFormat:@"%s", table]];
    return [translated UTF8String ];
}
//------------------------------------------------------------------------
-(void)setDefaultLang:(cocos2d::LanguageType)lang
{
    _lang = [self getLangStr:lang];
}
//------------------------------------------------------------------------
-(NSString*)getLangStr:(cocos2d::LanguageType)lang
{
    NSString *ret;
    
    switch (lang)
    {
        case cocos2d::kLanguageEnglish :
            ret = @"en";
        break;

        case cocos2d::kLanguageChineseS :
        case cocos2d::kLanguageChineseT :
            ret = @"zh-Hans";
        break;

//        case cocos2d::kLanguageFrench :
//            _lang = @"fr";
//        break;
            
        case cocos2d::kLanguageItalian :
            ret = @"it";
        break;
            
        case cocos2d::kLanguageGerman :
            ret = @"de";
        break;
            
        case cocos2d::kLanguageSpanish :
            ret = @"es";
        break;
            
        case cocos2d::kLanguageRussian :
            ret = @"ru";
        break;
            
//        case cocos2d::kLanguagePolish :
//            _lang = @"pl";
//        break;
//            
//        case cocos2d::kLanguageJapanese :
//            _lang = @"ja";
//        break;

        default :
            ret = @"en";
            ;
    }
    return ret;
}
//------------------------------------------------------------------------
-(NSBundle*)currentLanguageBundle
{
    NSString *path;
    path = [[NSBundle mainBundle] pathForResource:_lang ofType:@"lproj"];
    //NSLog(@"%s", [path UTF8String] );
    
    return [NSBundle bundleWithPath:path];
}
//------------------------------------------------------------------------
-(NSBundle*)customLanguageBundle:(cocos2d::LanguageType)lang
{
    NSString *langPath;
    NSString *path;
    
    langPath = [self getLangStr:lang];
    path = [[NSBundle mainBundle] pathForResource:langPath ofType:@"lproj"];
    
    return [NSBundle bundleWithPath:path];
}



@end


