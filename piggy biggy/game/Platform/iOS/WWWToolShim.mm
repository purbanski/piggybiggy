#include "../WWWToolShim.h"
#import "WWWTool_iOS.h"
//----------------------------------------------------------------
void WWWToolShim::Destroy()
{
    [WWWTool_iOS destroy];
}
//----------------------------------------------------------------
void* WWWToolShim::HttpGet( const char *url )
{
    return [[WWWTool_iOS sharedInstance] httpGet:url];
}
//----------------------------------------------------------------
void WWWToolShim::VisitWWW(const char *url)
{
    [[WWWTool_iOS sharedInstance] visitWWW:url];
}
//----------------------------------------------------------------