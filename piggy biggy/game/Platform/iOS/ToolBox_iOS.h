#import <Foundation/Foundation.h>
#include "cocos2d.h"
#import "GameGlobals.h"
//----------------------------------------------------------------------------
@interface ToolBox_iOS : NSObject
{}
+ (bool) is90RotationNeeded;
+ (DeviceType) getDeviceType;
+ (NSString *)getMacAddress;
+ (NSString *)getHashMacAddress;
+ (NSString *)getChallange;
+ (NSString *)sha1:(NSString*)input;
+ (void)visitWWW:(const char *)url;
+ (void)showSendEmailView;
+ (UIImage*)getScreenShot;

+ (cocos2d::CCSprite*) spriteFromData:(const void*)data len:(int)len ;
+ (cocos2d::CCSprite*) spriteFromPlatofrmImage:(void *)data;

+ (NSString*) utf8Encode:(const char*)msg;

+(void)debugMsgBox:(NSString *)title
           message:(NSString *)message
             error:(NSError *)error;
//---------------------------------------------------------------------------
@end
