#import "GameCenter_iOS.h"
#import "GameCenter.h"

static GameCenter_iOS *gGameCenter = NULL;

//------------------------------------------------------------------------
// GameCenter_iOS
//------------------------------------------------------------------------
@implementation GameCenter_iOS

UIViewController *g_myViewController;
//------------------------------------------------------------------------
// statics
//------------------------------------------------------------------------
+ (GameCenter_iOS *)sharedInstance
{
  if ( ! gGameCenter )
    {
        g_myViewController = [self getRootViewController];
        
        gGameCenter = [[ GameCenter_iOS alloc] init ];
    }
    
    return gGameCenter;
}
//------------------------------------------------------------------------
+ (void) destroy
{
    if ( gGameCenter )
        [ gGameCenter dealloc ];
    
    gGameCenter = NULL;
}
//------------------------------------------------------------------------
// methods
//------------------------------------------------------------------------
- (void)gameCenterViewControllerDidFinish:(GKGameCenterViewController *)gameCenterViewController
{
    [g_myViewController dismissViewControllerAnimated:YES completion:nil];
//    [g_myViewController release];
}
//-------------------------------------------------------------------------------------------
- (void) playerAuthOK
{
    GameCenter::Get()->PlayerAuthOK();
}
//-------------------------------------------------------------------------------------------
- (void) playerAuthFailed:(NSError *)error
{
    GameCenter::Get()->PlayerAuthFailed();
}
//-------------------------------------------------------------------------------------------
- (void) authenticateLocalPlayer
{

    if ([GKLocalPlayer localPlayer].authenticated == NO)
    {
        GKLocalPlayer *localPlayer = [GKLocalPlayer localPlayer];
        [localPlayer setAuthenticateHandler:^(UIViewController *view, NSError *error) {
            NSLog(@"%@", [error description]);

            if(localPlayer.isAuthenticated)
            {
              [ self playerAuthOK ];
            }
//            else if (view)
//            {
//                [g_myViewController presentViewController:view animated:YES completion:nil];
//            }
            else
            {
                [ self playerAuthFailed:error ];
            }
        }];

    }
    else
    {
        NSLog(@"Already authenticated!");
    }
}
//-------------------------------------------------------------------------------------------
- (void) reportScore: (int64_t) score forLeaderboardID: (NSString*) category
{
    GKScore *scoreReporter = [[GKScore alloc] initWithLeaderboardIdentifier:category];
    scoreReporter.value = score;
    scoreReporter.context = 0;
  
    [scoreReporter reportScoreWithCompletionHandler:^(NSError *error)
    {
        if ( error  != nil )
        {
            NSLog( @"%s", error.debugDescription.UTF8String );
        }
// Do something interesting here.
    }];
}

+ (id)getRootViewController {
    UIWindow *window = [[UIApplication sharedApplication] keyWindow];
    if (window.windowLevel != UIWindowLevelNormal) {
        NSArray *windows = [[UIApplication sharedApplication] windows];
        for(window in windows) {
            if (window.windowLevel == UIWindowLevelNormal) {
                break;
            }
        }
    }
    
    for (UIView *subView in [window subviews])
    {
        UIResponder *responder = [subView nextResponder];
        if([responder isKindOfClass:[UIViewController class]]) {
            return responder;
        }
    }
    
    return nil;
}

@end
