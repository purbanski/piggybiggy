    #import "ToolBox_iOS.h"
#include <sys/socket.h>
#include <sys/sysctl.h>
#include <net/if.h>
#include <net/if_dl.h>
#import <CommonCrypto/CommonDigest.h>
#include <sys/utsname.h>
#include <string>
#include "RootViewController.h"
#import <QuartzCore/QuartzCore.h>
#include "AppController.h"
#include <cocos2d.h>
#import <FacebookSDK/FacebookSDK.h>
#include <Game.h>
#include <Debug/MyDebug.h>

@implementation ToolBox_iOS
//--------------------------------------------------------------------------
+(bool) is90RotationNeeded
{
    NSString *systemVersion;
    NSString *mainSystemVersion;
    
    systemVersion = [[UIDevice currentDevice] systemVersion];
    mainSystemVersion = [ systemVersion substringToIndex: 2];
    
    // if iOS is 4.x or 5.x
    if ( [ mainSystemVersion isEqualToString: @"4." ] || [ mainSystemVersion isEqualToString: @"5." ] )
        return true;
    else
        return false;
}
//--------------------------------------------------------------------------
+ (NSString *)getMacAddress
{
  int                 mgmtInfoBase[6];
  char                *msgBuffer = NULL;
  size_t              length;
  unsigned char       macAddress[6];
  struct if_msghdr    *interfaceMsgStruct;
  struct sockaddr_dl  *socketStruct;
  NSString            *errorFlag = NULL;

  // Setup the management Information Base (mib)
  mgmtInfoBase[0] = CTL_NET;        // Request network subsystem
  mgmtInfoBase[1] = AF_ROUTE;       // Routing table info
  mgmtInfoBase[2] = 0;              
  mgmtInfoBase[3] = AF_LINK;        // Request link layer information
  mgmtInfoBase[4] = NET_RT_IFLIST;  // Request all configured interfaces

  // With all configured interfaces requested, get handle index
  if ((mgmtInfoBase[5] = if_nametoindex("en0")) == 0) 
    errorFlag = @"if_nametoindex failure";
  else
  {
    // Get the size of the data available (store in len)
    if (sysctl(mgmtInfoBase, 6, NULL, &length, NULL, 0) < 0) 
      errorFlag = @"sysctl mgmtInfoBase failure";
    else
    {
      // Alloc memory based on above call
      if ((msgBuffer = (char*)malloc(length)) == NULL)
        errorFlag = @"buffer allocation failure";
      else
      {
        // Get system information, store in buffer
        if (sysctl(mgmtInfoBase, 6, msgBuffer, &length, NULL, 0) < 0)
          errorFlag = @"sysctl msgBuffer failure";
      }
    }
  }

  // Befor going any further...
  if (errorFlag != NULL)
  {
    NSLog(@"Error: %@", errorFlag);
    return errorFlag;
  }

  // Map msgbuffer to interface message structure
  interfaceMsgStruct = (struct if_msghdr *) msgBuffer;

  // Map to link-level socket structure
  socketStruct = (struct sockaddr_dl *) (interfaceMsgStruct + 1);

  // Copy link layer address data in socket structure to an array
  memcpy(&macAddress, socketStruct->sdl_data + socketStruct->sdl_nlen, 6);

  // Read from char array into a string object, into traditional Mac address format
  //  NSString *macAddressString = [NSString stringWithFormat:@"%02X:%02X:%02X:%02X:%02X:%02X",
  NSString *macAddressString = [NSString stringWithFormat:@"%02X%02X%02X%02X%02X%02X",
                                macAddress[0], macAddress[1], macAddress[2], 
                                macAddress[3], macAddress[4], macAddress[5]];
  NSLog(@"Mac Address: %@", macAddressString);

  // Release the buffer memory
  free(msgBuffer);

  return macAddressString;
}
//--------------------------------------------------------------------------
+ (NSString *)getChallange
{
    NSString *ret;
    NSString *hashMac = [ ToolBox_iOS getHashMacAddress ];

//
    std::string sub1;
    std::string sub2;
    std::string sub3;
    std::string sub4;
    
    NSRange range1;
    NSRange range2;
    NSRange range3;
    NSRange range4;
    
    range1.location = 3;
    range1.length = 10;

    range2.location = 13;
    range2.length = 7;

    range3.location = 23;
    range3.length = 5;

    range4.location = 32;
    range4.length = 6;
    
    
    sub1.append( [[ hashMac substringWithRange: range1 ] UTF8String ] );
    sub2.append( [[ hashMac substringWithRange: range2 ] UTF8String ] );
    sub3.append( [[ hashMac substringWithRange: range3 ] UTF8String ] );
    sub4.append( [[ hashMac substringWithRange: range4 ] UTF8String ] );

    std::string rstr;
    
    rstr.append( sub3 );
    rstr.append( sub1 );
    rstr.append( sub4 );
    rstr.append( sub2 );
   
    ret = [ NSString stringWithUTF8String:rstr.c_str() ];
    
    return ret;
}
//--------------------------------------------------------------------------
+ (NSString *)getHashMacAddress
{
    return [ ToolBox_iOS sha1:[ ToolBox_iOS getMacAddress ]];
}
//--------------------------------------------------------------------------
+ (NSString *)sha1:(NSString*)input
{
 const char *cstr = [input cStringUsingEncoding:NSUTF8StringEncoding];
 NSData *data = [NSData dataWithBytes:cstr length:input.length];
 
 uint8_t digest[CC_SHA1_DIGEST_LENGTH];
 
 CC_SHA1(data.bytes, data.length, digest);
 
 NSMutableString* output = [NSMutableString stringWithCapacity:CC_SHA1_DIGEST_LENGTH * 2];
 
 for(int i = 0; i < CC_SHA1_DIGEST_LENGTH; i++)
 [output appendFormat:@"%02x", digest[i]];
    
 return output;
}
//--------------------------------------------------------------------------
+(void)visitWWW:(const char*)urlAdres
{
    NSURL *url = [NSURL URLWithString:[ NSString stringWithUTF8String: urlAdres ]];

    if (![[UIApplication sharedApplication] openURL:url])
    {}
}
//--------------------------------------------------------------------------
+(void)showSendEmailView
{
    UIViewController *viewController = [[[UIApplication sharedApplication] keyWindow] rootViewController];
    RootViewController *rootView = (RootViewController*) viewController;
    
    [ rootView displayComposerSheet ];
}
//--------------------------------------------------------------------------
+ (DeviceType)getDeviceType
{
    char *machine;
    
    DeviceType device;
    struct utsname systemInfo;
    
    uname( &systemInfo );
    machine = systemInfo.machine;
    D_CSTRING( machine );
    
    //-------------
    // simulator
     if ( ! strcmp( machine, "i386" ) || ! strcmp( machine, "x86_64" ))
        device = eDevice_simulator;
    
    //-------------
    // iPhone
    else if ( ! strcmp( machine, "iPhone5,1" ))
        device = eDevice_iPhone5_1;
    
    else if ( ! strcmp( machine, "iPhone5,2" ))
        device = eDevice_iPhone5_2;
    
    else if ( ! strcmp( machine, "iPhone4,1" ))
        device = eDevice_iPhone4_1;
    
    else if ( ! strcmp( machine, "iPhone3,1" ))
        device = eDevice_iPhone3_1;
    
    else if ( ! strcmp( machine, "iPhone2,1" ))
        device = eDevice_iPhone2_1;
    
    else if ( ! strcmp( machine, "iPhone1,2" ))
        device = eDevice_iPhone1_2;
    
    else if ( ! strcmp( machine, "iPhone1,1" ))
        device = eDevice_iPhone1_1;


    //-------------
    // iPad
    else if ( ! strncmp( machine, "iPad3,", 6 ))
        device = eDevice_iPad3_1;

    else if ( ! strcmp( machine, "iPad2,1" ))
        device = eDevice_iPad2_1;

    else if ( ! strcmp( machine, "iPad1,1" ))
        device = eDevice_iPad1_1;
    
    else if ( ! strcmp( machine, "iPad2,7" ))
        device = eDevice_iPad_Mini;
    

    
    //------------
    // iPod
    else if ( ! strcmp( machine, "iPod4,1" ))
        device = eDevice_iPod4_1;

    else if ( ! strcmp( machine, "iPod3,1" ))
        device = eDevice_iPod3_1;

    else if ( ! strcmp( machine, "iPod2,1" ))
        device = eDevice_iPod2_1;

    else if ( ! strcmp( machine, "iPod1,1" ))
        device = eDevice_iPod1_1;

    
    else device = eDevice_unknown;

    D_LOG("machine: (%s), type: %d", machine, device );
    
    return device;
}
//-------------------------------------------------------------
+ (cocos2d::CCSprite*) spriteFromData:(const void*)data len:(int)len
{
    UIImage* img = [UIImage imageWithData:[NSData dataWithBytes:data length:len]];
    return [ToolBox_iOS spriteFromPlatofrmImage:((void*) img)];
}
//-------------------------------------------------------------
+ (cocos2d::CCSprite*) spriteFromPlatofrmImage:(void *)data;
{
    UIImage *img = (UIImage*) data;
    
    CGImageRef imageRef = [img CGImage];
    NSUInteger width = CGImageGetWidth(imageRef);
    NSUInteger height = CGImageGetHeight(imageRef);
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    void *rawData =  calloc(height * width * 4, sizeof(unsigned char));
    NSUInteger bytesPerPixel = 4;
    NSUInteger bytesPerRow = bytesPerPixel * width;
    NSUInteger bitsPerComponent = 8;
    CGContextRef context = CGBitmapContextCreate(rawData, width, height,
                                                     bitsPerComponent, bytesPerRow, colorSpace,
                                                     kCGImageAlphaPremultipliedLast | kCGBitmapByteOrder32Big);
    CGColorSpaceRelease(colorSpace);
    CGContextDrawImage(context, CGRectMake(0, 0, width, height), imageRef);
    CGContextRelease(context);
    
/* create a texture with the raw data, and use that to create a CCSprite*/
    CCTexture2D *tempTexture = new CCTexture2D();
    tempTexture->autorelease();
    tempTexture->initWithData(rawData, kCCTexture2DPixelFormat_RGBA8888, width, height, CCSizeMake(width, height));
    free(rawData);
    
    return CCSprite::spriteWithTexture(tempTexture);
}


//+ (void)showAlert:(NSString *)message
//           result:(id)result
//            error:(NSError *)error
//{
//    NSString *alertMsg;
//    NSString *alertTitle;
//    
//    if (error)
//    {
//        alertMsg = error.localizedDescription;
//        alertTitle = @"Error";
//    }
//    else
//    {
//        NSDictionary *resultDict = (NSDictionary *)result;
//        alertMsg = [NSString stringWithFormat:@"Successfully posted '%@'.\nPost ID: %@",
//                    message, [resultDict valueForKey:@"id"]];
//        alertTitle = @"Success";
//    }
//
//    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:alertTitle
//                                                        message:alertMsg
//                                                       delegate:nil
//                                              cancelButtonTitle:@"OK"
//                                              otherButtonTitles:nil];
//    [alertView show];
//}
//--------------------------------------------------------------------------
+(void)debugMsgBox:(NSString *)title
           message:(NSString*)message
             error:(NSError*)error
{
    return;
#ifdef BUILD_TEST
    if ( !error)
    {
//        [[[UIAlertView alloc] initWithTitle:title
//                                    message:message
//                                   delegate:nil
//                          cancelButtonTitle:@"OK"
//                          otherButtonTitles:nil]
//         show];
    }
    else
    {
        [[[UIAlertView alloc] initWithTitle:[ NSString stringWithFormat:@"Errro %s", [[error localizedDescription ] UTF8String ]]
                                    message:message
                                   delegate:nil
                          cancelButtonTitle:@"OK"
                          otherButtonTitles:nil]
         show];
    }
#endif
}
//--------------------------------------------------------------------------
+(UIImage*)getScreenShot
{
    GLint backingWidth, backingHeight;
 
    // Bind the color renderbuffer used to render the OpenGL ES view
    // If your application only creates a single color renderbuffer which is already bound at this point,
    // this call is redundant, but it is needed if you're dealing with multiple renderbuffers.
    // Note, replace "_colorRenderbuffer" with the actual name of the renderbuffer object defined in your class.
    //glBindRenderbufferOES(GL_RENDERBUFFER_OES, _colorRenderbuffer);
 
    // Get the size of the backing CAEAGLLayer
    glGetRenderbufferParameterivOES(GL_RENDERBUFFER_OES, GL_RENDERBUFFER_WIDTH_OES, &backingWidth);
    glGetRenderbufferParameterivOES(GL_RENDERBUFFER_OES, GL_RENDERBUFFER_HEIGHT_OES, &backingHeight);
    
    D_FLOAT(Game::Get()->GetScale());
    

    NSInteger width = Config::GameSize.width * Game::Get()->GetScale();
    NSInteger height = Config::GameSize.height * Game::Get()->GetScale();

    NSInteger deltaX = ( backingWidth - width ) / 2;
    NSInteger deltaY = ( backingHeight - height ) / 2;

    NSInteger x = deltaX, y = deltaY;
    
    NSInteger dataLength = width * height * 4;
    GLubyte *data = (GLubyte*)malloc(dataLength * sizeof(GLubyte));
 
    // Read pixel data from the framebuffer
    glPixelStorei(GL_PACK_ALIGNMENT, 4);
    glReadPixels(x, y, width, height, GL_RGBA, GL_UNSIGNED_BYTE, data);
 
    // Create a CGImage with the pixel data
    // If your OpenGL ES content is opaque, use kCGImageAlphaNoneSkipLast to ignore the alpha channel
    // otherwise, use kCGImageAlphaPremultipliedLast
    CGDataProviderRef ref = CGDataProviderCreateWithData(NULL, data, dataLength, NULL);
    CGColorSpaceRef colorspace = CGColorSpaceCreateDeviceRGB();
    CGImageRef iref = CGImageCreate(
                                    width,
                                    height,
                                    8,
                                    32,
                                    width * 4,
                                    colorspace,
                                    kCGBitmapByteOrder32Big | kCGImageAlphaPremultipliedLast,
                                    ref,
                                    NULL,
                                    true,
                                    kCGRenderingIntentDefault
                                    );
 
    // OpenGL ES measures data in PIXELS
    // Create a graphics context with the target size measured in POINTS
    NSInteger widthInPoints, heightInPoints;
//    if (NULL != UIGraphicsBeginImageContextWithOptions) {
//        // On iOS 4 and later, use UIGraphicsBeginImageContextWithOptions to take the scale into consideration
//        // Set the scale parameter to your OpenGL ES view's contentScaleFactor
//        // so that you get a high-resolution snapshot when its value is greater than 1.0
//        CGFloat scale = eaglview.contentScaleFactor;
//        widthInPoints = width / scale;
//        heightInPoints = height / scale;
//        UIGraphicsBeginImageContextWithOptions(CGSizeMake(widthInPoints, heightInPoints), NO, scale);
//    }
//    else
    {
        // On iOS prior to 4, fall back to use UIGraphicsBeginImageContext
        widthInPoints = width;
        heightInPoints = height;
        UIGraphicsBeginImageContext(CGSizeMake(widthInPoints, heightInPoints));
    }
 
    CGContextRef cgcontext = UIGraphicsGetCurrentContext();
 
    // UIKit coordinate system is upside down to GL/Quartz coordinate system
    // Flip the CGImage by rendering it to the flipped bitmap context
    // The size of the destination area is measured in POINTS
    CGContextSetBlendMode( cgcontext, kCGBlendModeCopy);
    CGContextDrawImage( cgcontext, CGRectMake(0.0, 0.0, widthInPoints, heightInPoints), iref);
 
    // Retrieve the UIImage from the current context
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
 
    UIGraphicsEndImageContext();
 
    // Clean up
    free(data);
    CFRelease(ref);
    CFRelease(colorspace);
    CGImageRelease(iref);
    
    UIImageWriteToSavedPhotosAlbum( image, nil, nil, nil);
    return image;
}
//--------------------------------------------------------------------------------
+(NSString*) utf8Encode:(const char *)msg
{
    NSString *converted;
    converted = [[NSString stringWithFormat:@"%s", msg] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    return converted;
}
//-------------------------------------------------------------

@end
