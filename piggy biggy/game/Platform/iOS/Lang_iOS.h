#import <Foundation/Foundation.h>
#import <Foundation/NSObject.h>
#import <UIKit/UIKit.h>
#import <cocos2d.h>

@interface Lang_iOS : NSObject 
//----------------------------------------------------------------
+ (Lang_iOS *)sharedInstance;
+ (void)destroy;
//----------------------------------------------------------------
-(const char*)translate:(const char*)key table:(const char*)table language:(cocos2d::LanguageType)language;
-(const char*)translate:(const char*)key table:(const char*)table;
-(void)setDefaultLang:(cocos2d::LanguageType)lang;
-(NSString*)getLangStr:(cocos2d::LanguageType)lang;
//----------------------------------------------------------------
@end
