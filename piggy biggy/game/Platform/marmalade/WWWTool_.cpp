#include "Platform/WWWTool.h"
//---------------------------------------------------------------------------------
WWWTool::~WWWTool()
{
}
//---------------------------------------------------------------------------------
void WWWTool::HttpGet(const char *url, WWWToolListener *listner )
{
	for ( ListenersMap::iterator it = _listenersMap.begin(); it != _listenersMap.end(); it++ )
	{
		if ( listner == it->second )
			return;
	}

	void *connection;
//	connection = [[WWWTool_iOS sharedInstance] httpGet:url];
	connection = NULL;

	if ( listner && connection )
	{
		if ( _listenersMap.find(connection) == _listenersMap.end() )
		{
			_listenersMap[connection] = listner;
		}
	}
}
//---------------------------------------------------------------------------------


