#include "Platform/GameCenter.h"
#include <stddef.h>
#include "Game.h"
#include "Levels/Pockets.h"
#include "TestFlight/Report.h"
#include "RunLogger/RunLogger.h"

//-------------------------------------------------------
// Game Center Achievements
//-------------------------------------------------------
// Game Progress
float GameCenterAchievement_GameProgress::GetProgress( void *data )
{
    return 0.0f;
}
//-------------------------------------------------------
// Bank buster achivement
float GameCenterAchievement_Bank::GetProgress( void *data )
{
    return -1.0f;
}
//-------------------------------------------------------
// Car achivement
float GameCenterAchievement_Car::GetProgress( void *data )
{
    return -1.0f;
}
//-------------------------------------------------------
// Reel achivement
float GameCenterAchievement_Reel::GetProgress( void *data )
{
    float progress;
    GameStatus *status;
    
    GameStatus::LevelStatus state1;
    GameStatus::LevelStatus state2;
    GameStatus::LevelStatus state3;
    
    progress = 0.0f;
    status = Game::Get()->GetGameStatus();
    
    state1 = status->GetLevelStatus( GameTypes::eLevelReel_1 );
    state2 = status->GetLevelStatus( GameTypes::eLevelReel_2 );
    state3 = status->GetLevelStatus( GameTypes::eLevelReel_3 );
    
    if ( state1 == GameStatus::eLevelStatus_Done ) progress += 20.0f;
    if ( state2 == GameStatus::eLevelStatus_Done ) progress += 40.0f;
    if ( state3 == GameStatus::eLevelStatus_Done ) progress += 40.0f;

    return progress;
}
//-------------------------------------------------------
// Rotate achivement
float GameCenterAchievement_Rotate::GetProgress( void *data )
{
    float progress;
    GameStatus *status;
    
    GameStatus::LevelStatus state1;
    GameStatus::LevelStatus state2;
    GameStatus::LevelStatus state3;
    GameStatus::LevelStatus state4;
    GameStatus::LevelStatus state5;
    GameStatus::LevelStatus state6;
    GameStatus::LevelStatus state7;
    GameStatus::LevelStatus state8;
    
    progress = 0.0f;
    status = Game::Get()->GetGameStatus();
    
    state1 = status->GetLevelStatus( GameTypes::eLevelRotate_1 );
    state2 = status->GetLevelStatus( GameTypes::eLevelRotate_2 );
    state3 = status->GetLevelStatus( GameTypes::eLevelRotate_3 );
    state4 = status->GetLevelStatus( GameTypes::eLevelRotate_4 );
    state5 = status->GetLevelStatus( GameTypes::eLevelRotate_5 );
    state6 = status->GetLevelStatus( GameTypes::eLevelRotate_6 );
    state7 = status->GetLevelStatus( GameTypes::eLevelRotate_7 );
    state8 = status->GetLevelStatus( GameTypes::eLevelRotate_Circle );

    float step = 12.5f;
    
    if ( state1 == GameStatus::eLevelStatus_Done ) progress += step;;
    if ( state2 == GameStatus::eLevelStatus_Done ) progress += step;;
    if ( state3 == GameStatus::eLevelStatus_Done ) progress += step;;
    if ( state4 == GameStatus::eLevelStatus_Done ) progress += step;;
    if ( state5 == GameStatus::eLevelStatus_Done ) progress += step;;
    if ( state6 == GameStatus::eLevelStatus_Done ) progress += step;;
    if ( state7 == GameStatus::eLevelStatus_Done ) progress += step;;
    if ( state8 == GameStatus::eLevelStatus_Done ) progress += step;;

    if ( progress > 100.0f ) progress = 100.0f;
    
    return progress;
}
//-------------------------------------------------------
// Bomb achivement
float GameCenterAchievement_Bomb::GetProgress( void *data )
{
    float progress;
    GameStatus *status;
    
    GameStatus::LevelStatus state1;
    GameStatus::LevelStatus state2;
    GameStatus::LevelStatus state3;
    GameStatus::LevelStatus state4;
    GameStatus::LevelStatus state5;
    GameStatus::LevelStatus state6;
    GameStatus::LevelStatus state7;
    GameStatus::LevelStatus state8;
    GameStatus::LevelStatus state9;
    GameStatus::LevelStatus state10;
    GameStatus::LevelStatus state11;
    GameStatus::LevelStatus state12;
    GameStatus::LevelStatus state13;
    GameStatus::LevelStatus state14;
    GameStatus::LevelStatus state15;
    GameStatus::LevelStatus state16;
    GameStatus::LevelStatus state17;
    GameStatus::LevelStatus state18;
    
    progress = 0.0f;
    status = Game::Get()->GetGameStatus();
    
    state1 = status->GetLevelStatus( GameTypes::eLevelExplosivePath );
    state2 = status->GetLevelStatus( GameTypes::eLevelBombAndRoll_1 );
    state3 = status->GetLevelStatus( GameTypes::eLevelBomb_1b );
    state4 = status->GetLevelStatus( GameTypes::eLevelBomb_2 );
    state5 = status->GetLevelStatus( GameTypes::eLevelBomb_3 );
    state6 = status->GetLevelStatus( GameTypes::eLevelBomb_5 );
    state7 = status->GetLevelStatus( GameTypes::eLevelBomb_6 );
    state8 = status->GetLevelStatus( GameTypes::eLevelBomb_7 );
    state9 = status->GetLevelStatus( GameTypes::eLevelBomb_8 );
    state10 = status->GetLevelStatus( GameTypes::eLevelOthers_6 );
    state11 = status->GetLevelStatus( GameTypes::eLevelPawel_10 );
    state12 = status->GetLevelStatus( GameTypes::eLevelPawel_11 );
    state13 = status->GetLevelStatus( GameTypes::eLevelPawel_12 );
    state14 = status->GetLevelStatus( GameTypes::eLevelPawel_15 );
    state15 = status->GetLevelStatus( GameTypes::eLevelPawel_2 );
    state16 = status->GetLevelStatus( GameTypes::eLevelPawel_3 );
    state17 = status->GetLevelStatus( GameTypes::eLevelPawel_5 );
    state18 = status->GetLevelStatus( GameTypes::eLevelPawel_8 );
    
    float step = 5.6f;
    
    if ( state1 == GameStatus::eLevelStatus_Done ) progress += step;
    if ( state2 == GameStatus::eLevelStatus_Done ) progress += step;
    if ( state3 == GameStatus::eLevelStatus_Done ) progress += step;
    if ( state4 == GameStatus::eLevelStatus_Done ) progress += step;
    if ( state5 == GameStatus::eLevelStatus_Done ) progress += step;
    if ( state6 == GameStatus::eLevelStatus_Done ) progress += step;
    if ( state7 == GameStatus::eLevelStatus_Done ) progress += step;
    if ( state8 == GameStatus::eLevelStatus_Done ) progress += step;
    if ( state9 == GameStatus::eLevelStatus_Done ) progress += step;
    if ( state10 == GameStatus::eLevelStatus_Done ) progress += step;
    if ( state11 == GameStatus::eLevelStatus_Done ) progress += step;
    if ( state12 == GameStatus::eLevelStatus_Done ) progress += step;
    if ( state13 == GameStatus::eLevelStatus_Done ) progress += step;
    if ( state14 == GameStatus::eLevelStatus_Done ) progress += step;
    if ( state15 == GameStatus::eLevelStatus_Done ) progress += step;
    if ( state16 == GameStatus::eLevelStatus_Done ) progress += step;
    if ( state17 == GameStatus::eLevelStatus_Done ) progress += step;
    if ( state18 == GameStatus::eLevelStatus_Done ) progress += step;

    return progress;
}
//-------------------------------------------------------
// Spitter achivement
float GameCenterAchievement_Spitter::GetProgress( void *data )
{
    float progress;
    GameStatus *status;
    
    GameStatus::LevelStatus state0;
    GameStatus::LevelStatus state1;
    GameStatus::LevelStatus state2;
    GameStatus::LevelStatus state3;
    GameStatus::LevelStatus state4;
    
    progress = 0.0f;
    status = Game::Get()->GetGameStatus();
    
    state0 = status->GetLevelStatus( GameTypes::eLevelIntro_Gun );
    state1 = status->GetLevelStatus( GameTypes::eLevelGun_1 );
    state2 = status->GetLevelStatus( GameTypes::eLevelGun_2 );
    state3 = status->GetLevelStatus( GameTypes::eLevelGun_3 );
    state4 = status->GetLevelStatus( GameTypes::eLevelGun_4 );
    
    if ( state0 == GameStatus::eLevelStatus_Done ) progress += 3.0f;
    if ( state1 == GameStatus::eLevelStatus_Done ) progress += 7.0f;
    if ( state2 == GameStatus::eLevelStatus_Done ) progress += 20.0f;
    if ( state3 == GameStatus::eLevelStatus_Done ) progress += 30.0f;
    if ( state4 == GameStatus::eLevelStatus_Done ) progress += 40.0f;

    return progress;
}
//-------------------------------------------------------
// Hacker achivement
float GameCenterAchievement_Hacker::GetProgress( void *data )
{
    float progress;
    GameStatus *status;
    
    GameStatus::LevelStatus state1;
    GameStatus::LevelStatus state2;
    GameStatus::LevelStatus state3;
    GameStatus::LevelStatus state4;
    GameStatus::LevelStatus state5;
    GameStatus::LevelStatus state6;
    GameStatus::LevelStatus state7;
    GameStatus::LevelStatus state8;
    GameStatus::LevelStatus state9;
    GameStatus::LevelStatus state10;
    GameStatus::LevelStatus state11;
    GameStatus::LevelStatus state12;
    
    progress = 0.0f;
    status = Game::Get()->GetGameStatus();
    
    state1 = status->GetLevelStatus( GameTypes::eLevelTraps_2 );
    state2 = status->GetLevelStatus( GameTypes::eLevelDomino_1 );
    state3 = status->GetLevelStatus( GameTypes::eLevelAdamS_PlainLogic );
    state4 = status->GetLevelStatus( GameTypes::eLevelPawel_13 );
    state5 = status->GetLevelStatus( GameTypes::eLevelLukasz_1b );
    state6 = status->GetLevelStatus( GameTypes::eLevelChain_6 );
    state7 = status->GetLevelStatus( GameTypes::eLevelSwing_Impossible );
    state8 = status->GetLevelStatus( GameTypes::eLevelReel_3 );
    state9 = status->GetLevelStatus( GameTypes::eLevelTetris_1 );
    state10 = status->GetLevelStatus( GameTypes::eLevelConstruction_1c );
    state11 = status->GetLevelStatus( GameTypes::eLevelLabyrinth_Imposible );
    state12 = status->GetLevelStatus( GameTypes::eLevelBank_Impossible );
    
    float step = 8.5f;
    
    if ( state1 == GameStatus::eLevelStatus_Done ) progress += step;
    if ( state2 == GameStatus::eLevelStatus_Done ) progress += step;
    if ( state3 == GameStatus::eLevelStatus_Done ) progress += step;
    if ( state4 == GameStatus::eLevelStatus_Done ) progress += step;
    if ( state5 == GameStatus::eLevelStatus_Done ) progress += step;
    if ( state6 == GameStatus::eLevelStatus_Done ) progress += step;
    if ( state7 == GameStatus::eLevelStatus_Done ) progress += step;
    if ( state8 == GameStatus::eLevelStatus_Done ) progress += step;
    if ( state9 == GameStatus::eLevelStatus_Done ) progress += step;
    if ( state10 == GameStatus::eLevelStatus_Done ) progress += step;
    if ( state11 == GameStatus::eLevelStatus_Done ) progress += step;
    if ( state12 == GameStatus::eLevelStatus_Done ) progress += step;
    
    if ( progress > 100.0f ) progress = 100.0f;

    return progress;
}
//-------------------------------------------------------
// Pocket Achievement Base
float GameCenterAchievement_PocketBase::GetPocketProgress( GameTypes::PocketType pocket )
{
    float countDone;
    float countAll;
    float progress;
    
    PocketManager::PocketLevels levels;
    GameStatus::LevelStatus levelStatus;
    
    levels = PocketManager::Get()->GetLevelsFromPocket( pocket );

    progress = 0.0f;
    countDone = 0.0f;
    countAll = (float)levels.size();
    
    for ( PocketManager::PocketLevels::iterator it = levels.begin(); it != levels.end(); it++ )
    {
        levelStatus = Game::Get()->GetGameStatus()->GetLevelStatus( *it );
        if ( levelStatus == GameStatus::eLevelStatus_Done )
            countDone++;
    }

    progress = countDone / countAll * 100.0f;
    return progress;;
}

//-------------------------------------------------------
// Jeans Pocket Achievement
float GameCenterAchievement_JeansPocket::GetProgress( void *data )
{
    return GetPocketProgress( ePocketBlue );
}

//-------------------------------------------------------
// Moro Pocket Achievement
float GameCenterAchievement_MoroPocket::GetProgress(void *data)
{
    return GetPocketProgress( ePocketMoro );
}

//-------------------------------------------------------
// Rasta Pocket Achievement
float GameCenterAchievement_RastaPocket::GetProgress(void *data)
{
    return GetPocketProgress( ePocketRasta );
}

//-------------------------------------------------------
// Kimono Pocket Achievement
float GameCenterAchievement_KimonoPocket::GetProgress(void *data)
{
    return GetPocketProgress( ePocketJapan );
}

//-------------------------------------------------------
// Professor Pocket Achievement
float GameCenterAchievement_ProfessorPocket::GetProgress(void *data)
{
    return GetPocketProgress( ePocketScience );
}



//-------------------------------------------------------
// Game Center
//-------------------------------------------------------

GameCenter* GameCenter::_sInstance = NULL;
//-------------------------------------------------------
GameCenter* GameCenter::Get()
{
    if ( ! _sInstance )
        _sInstance = new GameCenter();
    
    return _sInstance;
}
//-------------------------------------------------------
void GameCenter::Destroy()
{
    if ( _sInstance )
        delete _sInstance;
    _sInstance = NULL;
}
//-------------------------------------------------------
GameCenter::GameCenter()
{
    _isPlayerAuth = false;
}
//-------------------------------------------------------
GameCenter::~GameCenter()
{
    _levelsCompleted.clear();
}
//-------------------------------------------------------
void GameCenter::Authenticate()
{
     //[[ GameCenter_iOS sharedInstance] authenticateLocalPlayer ];
}
//-------------------------------------------------------
void GameCenter::PlayerAuthOK()
{
    RLOG( "GC_Auth_OK" );
    _isPlayerAuth = true;
    Game::Get()->GameCenterUpdate();   
}
//-------------------------------------------------------
void GameCenter::PlayerAuthFailed()
{
    RLOG( "GC_Auth_Fail" );
    _isPlayerAuth = false;
}
//-------------------------------------------------------
bool GameCenter::IsPlayerAuth()
{
    return _isPlayerAuth;
}
//-------------------------------------------------------
void GameCenter::PushScoresToServer()
{
//    return;
    if ( ! _isPlayerAuth )
        return;
    
    // Leaderboards must be updated before achievements
    // as achievement gameProgress depends on _levelCompleted list
    // which gets update while running UpdateLeaderBoards();
    
    UpdateLeaderBoards();
    UpdateAchievements();
}
//-------------------------------------------------------
void GameCenter::UpdateAchievements()
{
//    GameCenterAchievement_GameProgress  workerGameProgress;
//
//    GameCenterAchievement_Hacker    workerHacker;
//    GameCenterAchievement_Bank      workerBank;
//    GameCenterAchievement_Car       workerCar;
//    GameCenterAchievement_Reel      workerReel;
//    GameCenterAchievement_Rotate    workerRotate;
//    GameCenterAchievement_Spitter   workerSpitter;
//    GameCenterAchievement_Bomb      workerBomb;
//    
//    GameCenterAchievement_JeansPocket       workerJeansPocket;
//    GameCenterAchievement_MoroPocket        workerMoroPocket;
//    GameCenterAchievement_RastaPocket       workerRastaPocket;
//    GameCenterAchievement_KimonoPocket      workerKimonoPocket;
//    GameCenterAchievement_ProfessorPocket   workerProfessorPocket;
//
//    ProcessAchievement( Config::GameCenter_Achievement_GameProgress,    workerGameProgress, (void*) &_levelsCompleted );
//
//    ProcessAchievement( Config::GameCenter_Achievement_Hacker,          workerHacker );
//    ProcessAchievement( Config::GameCenter_Achievement_Bank,            workerBank );
//    ProcessAchievement( Config::GameCenter_Achievement_Car,             workerCar );
//    ProcessAchievement( Config::GameCenter_Achievement_Reel,            workerReel );
//    ProcessAchievement( Config::GameCenter_Achievement_Rotate,          workerRotate );
//    ProcessAchievement( Config::GameCenter_Achievement_Spitter,         workerSpitter );
//    ProcessAchievement( Config::GameCenter_Achievement_Bomb,            workerBomb );
//    
//    ProcessAchievement( Config::GameCenter_Achievement_JeansPocket,     workerJeansPocket );
//    ProcessAchievement( Config::GameCenter_Achievement_MoroPocket,      workerMoroPocket );
//    ProcessAchievement( Config::GameCenter_Achievement_RastaPocket,     workerRastaPocket );
//    ProcessAchievement( Config::GameCenter_Achievement_KimonoPocket,    workerKimonoPocket );
//    ProcessAchievement( Config::GameCenter_Achievement_ProfessorPocket, workerProfessorPocket );
}
//-------------------------------------------------------
void GameCenter::ProcessAchievement( const char *achievementName, GameCenterAchievementBase &achvImpl, void *data )
{
//    NSString *achievementId     = [[NSString alloc] initWithUTF8String: achievementName ] ;
//    GKAchievement *achievement  = [[GKAchievement alloc] initWithIdentifier: achievementId];
//
//    if (achievement)
//    {
//         achievement.percentComplete = achvImpl.GetProgress( data );
//       
//         [achievement reportAchievementWithCompletionHandler:^(NSError *error)
//         {
//             if (error != nil)
//            {
//                NSLog(@"Error in reporting achievements: %@", error);
//                // assert(false);
//            }
//        }];
//    }
//
}
//-------------------------------------------------------
void GameCenter::UpdateLeaderBoards()
{
    //GameCenterLeaderScores scores;
    //scores = CalculateLeaderBoardsScore();
    //
    //if ( ! _isPlayerAuth )
    //    return;
    //
    //NSString *boardTotalScore       = [[NSString alloc] initWithUTF8String: Config::GameCenter_LeaderBoard_TotalScore       ];
    ////    NSString *boardTotalScore       = [[NSString alloc] initWithUTF8String: "simpleTest"      ];
    //NSString *boardJeansScore       = [[NSString alloc] initWithUTF8String: Config::GameCenter_LeaderBoard_JeansScore       ];
    //NSString *boardMoroScore        = [[NSString alloc] initWithUTF8String: Config::GameCenter_LeaderBoard_MoroScore        ];
    //NSString *boardRastaScore       = [[NSString alloc] initWithUTF8String: Config::GameCenter_LeaderBoard_RastaScore       ];
    //NSString *boardJapanScore       = [[NSString alloc] initWithUTF8String: Config::GameCenter_LeaderBoard_KimonoScore      ];
    //NSString *boardProffesorScore   = [[NSString alloc] initWithUTF8String: Config::GameCenter_LeaderBoard_ProfessorScore   ];
    //
    //[[ GameCenter_iOS sharedInstance ] reportScore:scores._totalScore           forLeaderboardID: boardTotalScore ];
    //[[ GameCenter_iOS sharedInstance ] reportScore:scores._jeansPocketScore     forLeaderboardID: boardJeansScore ];
    //[[ GameCenter_iOS sharedInstance ] reportScore:scores._moroPocketScore      forLeaderboardID: boardMoroScore ];
    //[[ GameCenter_iOS sharedInstance ] reportScore:scores._rastaPocketScore     forLeaderboardID: boardRastaScore ];
    //[[ GameCenter_iOS sharedInstance ] reportScore:scores._ladyPocketScore      forLeaderboardID: boardJapanScore ];
    //[[ GameCenter_iOS sharedInstance ] reportScore:scores._profesorPocketScore  forLeaderboardID: boardProffesorScore ];
}
//-------------------------------------------------------
GameCenterLeaderScores GameCenter::CalculateLeaderBoardsScore()
{
    //_levelsCompleted.clear();
    //
    //GameStatus *gameStatus;
    //GameTypes::LevelEnum levelEnum;
    //PocketManager::PocketList pocketList;
    //PocketManager::PocketLevels levelList;
    //
    //typedef map<GameTypes::PocketType, PocketManager::PocketLevels> PocketToLevelMap;
    //typedef map<GameTypes::PocketType, unsigned int> PocketScoreMap;
    //
    //PocketScoreMap pocketScoreMap;
    //PocketToLevelMap pocket2levelMap;
    //
    //gameStatus = Game::Get()->GetGameStatus();
    //
    ////----------------------------------
    //// get open levels
    //pocketList = PocketManager::Get()->GetPocketList();
    //for ( PocketManager::PocketList::iterator pocket = pocketList.begin(); pocket != pocketList.end(); pocket++ )
    //{
    //    levelList = PocketManager::Get()->GetLevelsFromPocket( *pocket );
    //    PocketManager::PocketLevels siglePocketLevelList;
    //    
    //    for ( PocketManager::PocketLevels::iterator lit = levelList.begin(); lit != levelList.end(); lit++)
    //    {
    //        levelEnum = *lit;
    //        if ( gameStatus->GetLevelStatus( levelEnum ) == GameStatus::eLevelStatus_Done )
    //        {
    //            _levelsCompleted.insert( levelEnum );
    //            siglePocketLevelList.push_back( levelEnum );
    //        }
    //    }
    //    
    //    pocket2levelMap[ *pocket ] = siglePocketLevelList;
    //}
    //
    ////----------------------------------
    //// get scores
    //GameTypes::PocketType pocket;
    //PocketScoreMap scoreMap;
    //
    //for( PocketToLevelMap::iterator it = pocket2levelMap.begin(); it != pocket2levelMap.end(); it++ )
    //{
    //    PocketManager::PocketLevels levels;
    //    unsigned int score;
    //    
    //    score = 0;
    //    levels = it->second;
    //    pocket = it->first;
    //    
    //    for ( PocketManager::PocketLevels::iterator lit = levels.begin(); lit != levels.end(); lit++ )
    //        score += GetLevelScore( *lit );

    //    scoreMap[pocket] = score;
    //}
    //    
    //
    ////----------------------------------
    //// count open level coins
    //int score;
    //score = 0;
    //
    //for ( LevelList::iterator it = _levelsCompleted.begin(); it != _levelsCompleted.end(); it++ )
    //    score += GetLevelScore( *it );

    GameCenterLeaderScores gameCenterScore;
    //gameCenterScore._jeansPocketScore   = scoreMap[ GameTypes::ePocketBlue ];
    //gameCenterScore._moroPocketScore    = scoreMap[ GameTypes::ePocketMoro ];
    //gameCenterScore._rastaPocketScore   = scoreMap[ GameTypes::ePocketRasta ];
    //gameCenterScore._ladyPocketScore    = scoreMap[ GameTypes::ePocketJapan ];
    //gameCenterScore._profesorPocketScore= scoreMap[ GameTypes::ePocketScience ];
    //gameCenterScore._totalScore         = score;
    //
    //D_INT( gameCenterScore._jeansPocketScore );
    //D_INT( gameCenterScore._moroPocketScore );
    //D_INT( gameCenterScore._rastaPocketScore );
    //D_INT( gameCenterScore._ladyPocketScore );
    //D_INT( gameCenterScore._profesorPocketScore );
    //
    //unsigned int checkSum;
    //checkSum = 0;
    //checkSum += gameCenterScore._jeansPocketScore ;
    //checkSum += gameCenterScore._moroPocketScore ;
    //checkSum += gameCenterScore._rastaPocketScore ;
    //checkSum += gameCenterScore._ladyPocketScore ;
    //checkSum += gameCenterScore._profesorPocketScore;
    //
    //if ( checkSum != gameCenterScore._totalScore )
    //{
    //    D_SIZE( pocket2levelMap[ GameTypes::ePocketBlue ] );
    //    D_SIZE( pocket2levelMap[ GameTypes::ePocketMoro ] );
    //    D_SIZE( pocket2levelMap[ GameTypes::ePocketRasta ] );
    //    D_SIZE( pocket2levelMap[ GameTypes::ePocketJapan ] );
    //    D_SIZE( pocket2levelMap[ GameTypes::ePocketScience ] );
    //    D_SIZE( _levelsCompleted );
    //    
    //    CCAssert( false, "Game center score checksum problem");
    //}
    //D_LOG( "%d check %d", gameCenterScore._totalScore, checkSum)
    //
    return gameCenterScore;
}
//-------------------------------------------------------
//unsigned int GameCenter::GetLevelScore( GameTypes::LevelEnum level )
//{
//    int score;
//    unsigned int sec;
//    GameStatus::Status *status;
//
//    status = Game::Get()->GetGameStatus()->GetStatus();
//    
//    sec = status->_scoreTable[ level ]._seconds;
//    score = Config::MaxScoreLevelCoins - sec;
//    
//    if ( score < 0 )
//        score = 0;
//    
//    return score;
//}
