#include "../ToolBoxShim.h"
//-------------------------------------------------------------
DeviceType ToolBoxShim::GetDeviceType()
{
    return eDevice_iPad_Mini;
}
//-------------------------------------------------------------
string ToolBoxShim::GetMacAddress()
{
    string ret;
    ret.append( "00:00:00:00:00" );

    return ret;
}
//-------------------------------------------------------------
string ToolBoxShim::GetMacHash()
{
    string ret;
    ret.append( "MacHash Unimplemented");
    return ret;
}
//-------------------------------------------------------------
string ToolBoxShim::GetMD5( const char *msg )
{
    string ret;
    ret.append( "GetMD5 Unimplemented" );
    return ret;
}
//-------------------------------------------------------------
void ToolBoxShim::ShowSendEmailView( const char *cc, const char *subject )
{
    //[ToolBox_iOS showSendEmailView];
}
//-------------------------------------------------------------
bool ToolBoxShim::ShouldRotate90Degree()
{
    return false;
}
//-------------------------------------------------------------
std::string ToolBoxShim::UTF8Encode( const char *msg )
{
    string ret;
    ret.append("Unimplemented");
    return ret;
}
//-------------------------------------------------------------
void ToolBoxShim::DebugMsgBox( const char *title, const char *msg )
{
   /* [ToolBox_iOS debugMsgBox:[NSString stringWithUTF8String:title]
                     message:[NSString stringWithUTF8String:msg]
                       error:nil];*/
}
//-------------------------------------------------------------
string ToolBoxShim::GetChallenge()
{
    string ret;
    ret.append( "Challenge unimplemented" );
    return ret;
}
//-------------------------------------------------------------
cocos2d::CCSprite* ToolBoxShim::SpriteFromData( const void *data, int len )
{
    return NULL;
}
//-------------------------------------------------------------
cocos2d::CCSprite* ToolBoxShim::SpriteFromPlatofrmImage( void *data )
{
    return NULL;
}
//-------------------------------------------------------------


