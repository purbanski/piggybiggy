#ifndef __piggy_biggy__UserScoreMap__
#define __piggy_biggy__UserScoreMap__

#include <map>
#include <string>

using namespace std;

//----------------------------------------------------------------
// User Score Map
//----------------------------------------------------------------
/* fbUserId, level complition seconds */
class UserScoreMap : public map<string,int>
{
public:
    UserScoreMap();
    void Dump();
};

#endif

