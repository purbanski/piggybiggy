#include <sstream>
#include <vector>
#include "ScoreTool.h"
#include "Platform/WWWTool.h"
#include "GameConfig.h"
#include "Game.h"
#include "Facebook/FBTool.h"
#include "Tools.h"
using namespace std;
//-------------------------------------------------------
ScoreTool* ScoreTool::_sInstance = NULL;
//-------------------------------------------------------
ScoreTool* ScoreTool::Get()
{
    if ( ! _sInstance )
    {
        _sInstance = new ScoreTool();
    }
    return _sInstance;
}
//-------------------------------------------------------
void ScoreTool::Destroy()
{
    if ( _sInstance )
    {
        delete _sInstance;
        _sInstance = NULL;
    }
}
//-------------------------------------------------------
ScoreTool::ScoreTool()
{
    _networkMode = eMode_Idle;
//    _updating = false;
}
//-------------------------------------------------------
ScoreTool::~ScoreTool()
{
    switch ( _networkMode )
    {
        case eMode_HTTPGetRequested :
            WWWTool::Get()->CancelHttpGet( this );
            break;
        
        case eMode_FBGetRequested :
            FBTool::Get()->CancelRequestForListener( this );
            
        default:
            ;
    }
}
//-------------------------------------------------------
void ScoreTool::PushLevelScore( const char *fbId, LevelEnum level )
{
    if ( ! FBTool::Get()->IsLogged() )
        return;
    
    GameStatus *status;
    stringstream url;
    unsigned int score;
    int levelIndex;
    int levelTime;
    
    status = Game::Get()->GetGameStatus();
    levelTime = status->GetLevelTime( level );
    score = status->GetLevelScore( level );
    levelIndex = status->GetLevelIndex( level ) + 1;
    
    url << Config::FBScoreURL;
    url << "updateLevelScore.php?";
    url << "fbId="  << fbId << "&";
    url << "levelEnum=" << (int) level << "&";
    url << "levelIndex=" << (int) levelIndex << "&";
    url << "time="  << levelTime << "&";
    url << "score="  << score;
    
    WWWTool::Get()->HttpGet( url.str().c_str() );
}
//-------------------------------------------------------
void ScoreTool::PushAllScores()
{
    if ( ! FBTool::Get()->IsLogged() )
        return;
    
    if ( ! ( FBTool::Get()->GetFBUserId().length() > 0 ))
        return;
    
    stringstream ssUrl;
    stringstream ssData;
    GameStatus* status = Game::Get()->GetGameStatus();
    GameLevelSequence& levelSequence = status->GetLevelSequence();
    
    bool init = true;
    for ( GameLevelSequence::iterator it = levelSequence.begin(); it != levelSequence.end(); it++ )
    {
        if ( status->GetLevelStatus( *it ) == GameStatus::eLevelStatus_Done )
        {
            if ( init )
                init = false;
            else
                ssData << ":";
            
            ssData << *it << ",";
            ssData << (status->GetLevelIndex( *it ) + 1) << ",";
            ssData << status->GetLevelTime( *it ) << ",";
            ssData << status->GetLevelScore( *it );
        }
    }
    
    ssUrl << Config::FBScoreURL;
    ssUrl << "updateAllScore.php?";
    ssUrl << "fbId=" << FBTool::Get()->GetFBUserId() << "&";
    ssUrl << "scores=" << ssData.str();
    
    D_STRING(ssUrl.str());
    CCLOG("%s", ssUrl.str().c_str());
    
    WWWTool::Get()->HttpGet( ssUrl.str().c_str() );
}
//-------------------------------------------------------
void ScoreTool::GetScoresFromServer( ScoreToolListener *listener, LevelEnum level )
{
    // start to get list of player's friends
    // who played piggy biggy - from facebook
    if ( ! FBTool::Get()->IsLogged() )
        return;
    
    _level = level;
    
    D_LOG("%s", FBTool::Get()->GetFBUserId().c_str() );
    
    if ( ! ( FBTool::Get()->GetFBUserId().length() > 0 ))
        return;

    _listenerSet.insert( listener );

    if ( _networkMode != eMode_Idle )
        return;
    
    _networkMode = eMode_FBGetRequested;
    FBTool::Get()->ReadScore( this );
}
//-------------------------------------------------------
void ScoreTool::FBRequestCompleted( FBTool::Request requestType, void *data )
{
    // we got here list of palyers friends who played piggy biggy
    // we need to prepare and execute http request
    // to score server to read user and his friends scores
    FBScoreList *scoreList;
    string users;
    scoreList = (FBScoreList*)data;
    
    users = scoreList->GetUsersString();
    
    _userNameMap.clear();
    scoreList->FillUserMap( _userNameMap );
    
    stringstream ssUrl;
    ssUrl << Config::FBScoreURL;
    ssUrl << "getFriendsLevelScore.php?";
    ssUrl << "level=" << _level << "&";
    ssUrl << "ids=" << users;
    
    D_STRING(ssUrl.str());
    WWWTool::Get()->HttpGet( ssUrl.str().c_str(), this );
    
    _networkMode = eMode_HTTPGetRequested;
}
//-------------------------------------------------------
void ScoreTool::FBRequestError( int error )
{
    for ( ListenerSet::iterator it = _listenerSet.begin(); it != _listenerSet.end(); it++ )
        (*it)->ScoreTool_RequestError();
    
    _networkMode = eMode_Idle;
    _listenerSet.clear();
}
//-------------------------------------------------------
void ScoreTool::GetRandomScoreFromServer( ScoreToolListener *listener )
{
    if ( _networkMode != eMode_Idle )
        return;
    
    _listenerSet.insert( listener );
    
    stringstream ssUrl;
    ssUrl << Config::FBScoreURL;
    ssUrl << "getRandomScores.php?level=" << Game::Get()->GetGameStatus()->GetCurrentLevel();
    
    D_STRING(ssUrl.str());
    WWWTool::Get()->HttpGet( ssUrl.str().c_str(), this );

    _networkMode = eMode_HTTPGetRequested;
}
//-------------------------------------------------------
void ScoreTool::Http_FailedWithError( void *connection, int error )
{
    _networkMode = eMode_Idle;
    
    for ( ListenerSet::iterator it = _listenerSet.begin(); it != _listenerSet.end(); it++ )
        (*it)->ScoreTool_RequestError();

    _listenerSet.clear();
}
//-------------------------------------------------------
void ScoreTool::Http_FinishedLoading( void *connection, const void *data, int len )
{
    string temp;
    string strData;
    
    temp.append( (const char*) data );
    strData = string( temp, 0, len );
    
    D_STRING(strData);
    _userScoreMap.clear();
    
    typedef vector<string> Vec;
    Vec usersScores;
    
    // example
    // 100005595183657,216:100005625662657,17
    
    Tools::StringSplit( strData.c_str(), ':', usersScores );
    for ( Vec::iterator userScore = usersScores.begin(); userScore != usersScores.end(); userScore++ )
    {
        //D_CSTRING( userScore->c_str() );
        Vec finalVec;
        Tools::StringSplit( userScore->c_str(), ',', finalVec );
        
        if ( finalVec.size() != 2 )
            continue;
        
        //D_LOG("%s %s", finalVec[0].c_str(), finalVec[1].c_str() );
        string fbId;
        int levelSecondsCount;
        
        fbId = finalVec[0];
        stringstream(finalVec[1]) >> levelSecondsCount;
        
        _userScoreMap[fbId] = levelSecondsCount;
    }
    
    _userScoreMap.Dump();
    NotifyListeners();
}
//-------------------------------------------------------
void ScoreTool::NotifyListeners()
{
    _networkMode = eMode_Idle;
    for ( ListenerSet::iterator it = _listenerSet.begin(); it != _listenerSet.end(); it++ )
    {
        (*it)->ScoreTool_ScoreMapLoaded();
    }
    
    _listenerSet.clear();
}
//-------------------------------------------------------
void ScoreTool::GetHighscore( FBFriendsScoresSortedSet &scoreMap )
{
    _userScoreMap.Dump();
    
    for ( UserScoreMap::iterator it = _userScoreMap.begin(); it != _userScoreMap.end(); it++ )
    {
        string fbId;
        string username;
        int levelTime;
        
        //---------------
        // Get FB user id
        fbId = it->first;
        
        //---------------
        // Get username
        username = _userNameMap[fbId];
        levelTime = it->second;
        scoreMap.insert(
                        FBScoreMenuRecord(
                                          fbId.c_str(),
                                          username.c_str(),
                                          levelTime ));
        D_SIZE(scoreMap);
    }
}
//-------------------------------------------------------
void ScoreTool::CancelRequest(ScoreToolListener *listener )
{
    _networkMode = eMode_Idle;
    WWWTool::Get()->CancelHttpGet( this );

    if ( listener )
    for ( ListenerSet::iterator it = _listenerSet.begin(); it != _listenerSet.end(); it++ )
    {
        if ( listener == (*it) )
        {
            listener->ScoreTool_RequestError();
            _listenerSet.erase(listener);
            break;
        }
    }
}
//-------------------------------------------------------
int ScoreTool::GetCoinCount( int secondsTotal )
{
    int coinCount;
    coinCount = 0;
    
    if ( secondsTotal <= Config::ScoreTime5Coins )
		coinCount = 5;
	else if ( secondsTotal <= Config::ScoreTime4Coins )
		coinCount = 4;
	else if ( secondsTotal <= Config::ScoreTime3Coins )
		coinCount = 3;
	else if ( secondsTotal <= Config::ScoreTime2Coins )
		coinCount = 2;
	else if ( secondsTotal <= Config::ScoreTime1Coins )
		coinCount = 1;
    
    return coinCount;
}
//-------------------------------------------------------

