#ifndef __SCORETOOL_H__
#define __SCORETOOL_H__
//----------------------------------------------------------------
#include <string>
#include <map>
#include <set>
#include "GameTypes.h"
#include "Facebook/Menus/FBScoreMenu.h"
#include "Platform/WWWTool.h"
#include "Facebook/FBTool.h"
#include "UserScoreMap.h"
//----------------------------------------------------------------
using namespace GameTypes;
using namespace std;

//----------------------------------------------------------------
// Score Tools
//----------------------------------------------------------------
class ScoreTool : public FBToolListener, public WWWToolListener
{
public:
    static ScoreTool* Get();
    static void Destroy();
    ~ScoreTool();

    virtual void Http_FailedWithError( void *connection, int error );
    virtual void Http_FinishedLoading( void *connection, const void *data, int len );

    virtual void FBRequestCompleted( FBTool::Request requestType, void *data );
    virtual void FBRequestError( int error );
   
    void CancelRequest( ScoreToolListener *listener );
    void GetRandomScoreFromServer( ScoreToolListener *listener );
    
    void PushLevelScore( const char *fbId, LevelEnum level );
    void PushAllScores();
    void GetScoresFromServer( ScoreToolListener *listener, LevelEnum level );
    void GetHighscore( FBFriendsScoresSortedSet &scoreMap );
    int GetCoinCount( int seconds );
    
private:
    ScoreTool();
    void NotifyListeners();
    
    static ScoreTool* _sInstance;   
    
    typedef set<ScoreToolListener*> ListenerSet;
    typedef enum
    {
        eMode_Idle = 0,
        eMode_HTTPGetRequested,
        eMode_FBGetRequested
    } NetworkMode;
    
    NetworkMode         _networkMode;
    UserScoreMap        _userScoreMap;
    String2StringMap    _userNameMap;
    ListenerSet         _listenerSet;
    LevelEnum           _level;
};
//----------------------------------------------------------------
#endif
