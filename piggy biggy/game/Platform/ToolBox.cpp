#include "ToolBox.h"
#include "ToolBoxShim.h"

//-------------------------------------------------------------
bool ToolBox::ShouldRotate90Degree()
{
    return ToolBoxShim::ShouldRotate90Degree();
}
////-------------------------------------------------------------
std::string ToolBox::GetMacAddress()
{
    return ToolBoxShim::GetMacAddress();
}
//-------------------------------------------------------------
std::string ToolBox::GetMacHash()
{
    return ToolBoxShim::GetMacHash();
}
//-------------------------------------------------------------
std::string ToolBox::UTF8Encode( const char *msg )
{
    return ToolBoxShim::UTF8Encode( msg );
}
//-------------------------------------------------------------
std::string ToolBox::GetMD5( const char *msg )
{
    return ToolBoxShim::GetMD5( msg );
}
//-------------------------------------------------------------
void ToolBox::DebugMsgBox( const char *title, const char *msg )
{
    ToolBoxShim::DebugMsgBox( title, msg );
}
//-------------------------------------------------------------
std::string ToolBox::GetChallange()
{
    return ToolBoxShim::GetChallenge();
}
//-------------------------------------------------------------
DeviceType ToolBox::GetDeviceType()
{
    return ToolBoxShim::GetDeviceType();
}
//-------------------------------------------------------------
void ToolBox::ShowSendEmailView(const char *cc, const char *subject )
{
    ToolBoxShim::ShowSendEmailView( cc, subject );
}
//-------------------------------------------------------------
CCSprite* ToolBox::SpriteFromData(const void *data, int len)
{
    return ToolBoxShim::SpriteFromData(data, len);
}
//-------------------------------------------------------------
CCSprite* ToolBox::SpriteFromPlatofrmImage(void *data)
{
    return ToolBoxShim::SpriteFromPlatofrmImage(data);
}
//-------------------------------------------------------------
// shim not needed
bool ToolBox::LowMemDevice( DeviceType device )
{
    bool ret;
    
    switch ( device )
    {
        case eDevice_iPad1_1 :      // ipad1
        case eDevice_iPhone2_1 :    // iphone 3gs
        case eDevice_iPod3_1 :      // ipod 3rd gen
        case eDevice_iPod4_1 :      // ipod 4rd gen
            ret = true;
        break;

        default:
            ret = false;
    }
    
    return ret;
}
//-------------------------------------------------------------
