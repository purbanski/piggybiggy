#ifndef __TOOLBOX_H__
#define __TOOLBOX_H__
//---------------------------------------------------------
#include <string>
#include <cocos2d.h>
#include "GameGlobals.h"
//---------------------------------------------------------
USING_NS_CC;
//---------------------------------------------------------
class ToolBox
{
public:
    static bool ShouldRotate90Degree();
    
    static std::string GetMacAddress();
    static std::string GetMacHash();
    static std::string GetMD5( const char *string );
    static std::string GetChallange();
    static DeviceType  GetDeviceType();
    
    static std::string UTF8Encode( const char *msg );
    static void ShowSendEmailView( const char *cc, const char *subject );
    
    static cocos2d::CCSprite *SpriteFromData( const void *data, int len );
    static cocos2d::CCSprite *SpriteFromPlatofrmImage( void *data );

    static void DebugMsgBox( const char *title, const char *msg );

    static bool LowMemDevice( DeviceType device );
};
//---------------------------------------------------------

#endif
