#include "unFile.h"
#include "Platform/Lang.h"
#include "Debug/MyDebug.h"
#include "cocos2d.h"
#include "Game.h"
#include "GameStatus.h"
#include "LangShim.h"

USING_NS_CC;

Lang* Lang::_sInstance = NULL;
//----------------------------------------------------------------------
Lang* Lang::Get()
{
	if ( ! _sInstance )
		_sInstance = new Lang();
    
    return _sInstance;
}
//----------------------------------------------------------------------
void Lang::Destroy()
{
	if ( _sInstance )
		delete _sInstance;

	_sInstance = NULL;
}
//----------------------------------------------------------------------
Lang::Lang()
{
    //    SetResourcePaths();
}
//----------------------------------------------------------------------
Lang::~Lang()
{
}
//----------------------------------------------------------------------
const char* Lang::GetLang( const char *file )
{
    ClearFullpath();
    strcpy( _langFullpath, _langPrefix);
    strcat( _langFullpath, file );

    D_LOG( "Lang %s", _langFullpath );

	if ( ! unResourceFileCheckExists( _langFullpath ))
	{
        ClearFullpath();
        strcpy( _langFullpath, file );
	}

	return _langFullpath;
}
//----------------------------------------------------------------------
void Lang::SetResourcePaths( LanguageType lang )
{
    switch ( lang )
	{
        case kLanguageEnglish :
            SetResourcePath( "" );
        break;

        case kLanguageChineseT :
            SetResourcePath( "Localization/ChineseT" );
        break;

        case kLanguageChineseS :
            SetResourcePath( "Localization/ChineseS" );
        break;

        case kLanguageFrench :
            SetResourcePath( "Localization/French" );
        break;

        case kLanguageItalian :
            SetResourcePath( "Localization/Italian" );
        break;

        case kLanguageGerman :
            SetResourcePath( "Localization/German" );
        break;

        case kLanguageRussian :
            SetResourcePath( "Localization/Russian" );
        break;
            
        case kLanguageSpanish :
            SetResourcePath( "Localization/Spanish" );
        break;

        case kLanguagePolish :
            SetResourcePath( "Localization/Polish" );
        break;

        case kLanguageJapanese :
            SetResourcePath( "Localization/Japanese" );
        break;

        default:
            SetResourcePath( "" );
    }
}
//----------------------------------------------------------------------
void Lang::SetResourcePath( const char *lang )
{
	memset( _langPrefix, 0, LANG_PREFIX_SIZE );
	strcpy( _langPrefix, lang );
}
//----------------------------------------------------------------------
void Lang::ClearFullpath()
{
	memset( _langFullpath, 0, LANG_FULLPATH_SIZE );
}
//----------------------------------------------------------------------
bool Lang::IsChinese()
{
    if ( Game::Get()->GetGameStatus()->GetLang() ==  kLanguageChineseS ||
        Game::Get()->GetGameStatus()->GetLang() == kLanguageChineseT )
    {
        return true;
    }
    return false;
}
//----------------------------------------------------------------------
bool Lang::IsJapanese()
{
    if ( Game::Get()->GetGameStatus()->GetLang() ==  kLanguageJapanese )
    {
        return true;
    }
    return false;
}
//----------------------------------------------------------------------
bool Lang::IsRussian()
{
    if ( Game::Get()->GetGameStatus()->GetLang() ==  kLanguageRussian )
    {
        return true;
    }
    return false;
}
//----------------------------------------------------------------------
const char* Lang::Translate( const char *key, const char *table )
{
    return LangShim::Translate(key, table);
}
//----------------------------------------------------------------------
const char* Lang::Translate( const char *key, const char *table, LanguageType lang )
{
    return LangShim::Translate(key, table, lang);
}
//----------------------------------------------------------------------
void Lang::SetLang( LanguageType lang )
{
    LangShim::SetLang( lang );
    
    GameGlobals::Get()->SetLevelNames();
    SetResourcePaths( lang );
}

