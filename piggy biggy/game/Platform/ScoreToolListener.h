#ifndef __SCORETOOLLISTENER_H__
#define __SCORETOOLLISTENER_H__

//----------------------------------------------------------------
// Score Tools Listener
//----------------------------------------------------------------
class ScoreToolListener
{
public:
    virtual void ScoreTool_ScoreMapLoaded() = 0;
    virtual void ScoreTool_RequestError() = 0;
};
//----------------------------------------------------------------
#endif
