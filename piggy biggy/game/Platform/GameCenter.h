#ifndef __GAMECENTER_PU_H__
#define __GAMECENTER_PU_H__

#include "GameTypes.h"

//-------------------------------------------------------
// Game Center Achievements
//-------------------------------------------------------
class GameCenterAchievementBase
{
public:
    virtual float GetProgress( void *data ) = 0;
};


//------------------
//-- Bank Achievement
class GameCenterAchievement_Bank : public GameCenterAchievementBase
{
public:
    virtual float GetProgress( void *data);
};


//------------------
//-- Car Achievement
class GameCenterAchievement_Car : public GameCenterAchievementBase
{
public:
    virtual float GetProgress( void *data );
};


//------------------
//-- Reel Achievement
class GameCenterAchievement_Reel: public GameCenterAchievementBase
{
public:
    virtual float GetProgress( void *data );
};


//------------------
//-- Acceleromentr achievment
class GameCenterAchievement_Rotate : public GameCenterAchievementBase
{
public:
    virtual float GetProgress( void *data );
};

//------------------
//-- Bomb achievment
class GameCenterAchievement_Bomb : public GameCenterAchievementBase
{
public:
    virtual float GetProgress( void *data );
};

//------------------
//-- Spitter achievement
class GameCenterAchievement_Spitter : public GameCenterAchievementBase
{
public:
    virtual float GetProgress( void *data );
};

//------------------
//-- Hacker achievement
class GameCenterAchievement_Hacker : public GameCenterAchievementBase
{
public:
    virtual float GetProgress( void *data );
};

//------------------
//-- Game Progress
class GameCenterAchievement_GameProgress : public GameCenterAchievementBase
{
public:
    virtual float GetProgress( void *data );
};

//------------------
//--  Pocket Achievement Base
class GameCenterAchievement_PocketBase : public GameCenterAchievementBase
{
public:
    virtual float GetProgress( void *data ) = 0;
    
protected:
    float GetPocketProgress( GameTypes::PocketType pocket );
};

//------------------
//-- Jeans Pocket
class GameCenterAchievement_JeansPocket : public GameCenterAchievement_PocketBase
{
public:
    virtual float GetProgress( void *data);
};

//------------------
//-- Moro Pocket
class GameCenterAchievement_MoroPocket : public GameCenterAchievement_PocketBase
{
public:
    virtual float GetProgress( void *data);
};

//------------------
//-- Rasta Pocket
class GameCenterAchievement_RastaPocket : public GameCenterAchievement_PocketBase
{
public:
    virtual float GetProgress( void *data);
};

//------------------
//-- Kimono Pocket
class GameCenterAchievement_KimonoPocket : public GameCenterAchievement_PocketBase
{
public:
    virtual float GetProgress( void *data);
};

//------------------
//-- Professor Pocket
class GameCenterAchievement_ProfessorPocket : public GameCenterAchievement_PocketBase
{
public:
    virtual float GetProgress( void *data);
};



//-------------------------------------------------------
// Game Center LeaderBoard Scores
//-------------------------------------------------------
class GameCenterLeaderScores
{
public:
    unsigned int _totalScore;
    unsigned int _jeansPocketScore;
    unsigned int _moroPocketScore;
    unsigned int _rastaPocketScore;
    unsigned int _ladyPocketScore;
    unsigned int _profesorPocketScore;
};


//-------------------------------------------------------
// Game Center
//-------------------------------------------------------
class GameCenter
{
public:
    typedef set<GameTypes::LevelEnum> LevelList;
    
    static GameCenter* Get();
    static void Destroy();
    
    ~GameCenter();

    void Authenticate();
    void PlayerAuthOK();
    void PlayerAuthFailed();
    
    bool IsPlayerAuth();
    void PushScoresToServer();
    
    
private:
    GameCenter();
    
    GameCenterLeaderScores CalculateLeaderBoardsScore();
    void ProcessAchievement( const char *achievementName, GameCenterAchievementBase &achImpl, void *data = NULL );

    void UpdateLeaderBoards();
    void UpdateAchievements();
    
private:
    static GameCenter* _sInstance;

    bool        _isPlayerAuth;
    LevelList   _levelsCompleted;
    
};
//-------------------------------------------------------

#endif
