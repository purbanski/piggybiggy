#ifndef piggy_biggy_WWWToolShim_h
#define piggy_biggy_WWWToolShim_h
//------------------------------------------------------------
class WWWToolShim
{
public:
    static void Destroy();
    static void *HttpGet( const char *url );
    static void VisitWWW( const char *url );
};
//------------------------------------------------------------
#endif
