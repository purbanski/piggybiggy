#ifndef __PU_LANG_H__
#define __PU_LANG_H__
//----------------------------------------------------------------------
#include <string>
#include <cocos2d.h>
#include "GameTypes.h"
//----------------------------------------------------------------------
#define LocalStrFile(key, table)    Lang::Get()->Translate(key, table)
#define LocalLevel(key)             Lang::Get()->Translate(key, "LevelsNames")
#define LocalString(key)            Lang::Get()->Translate(key, "Other")

#define EnglishString(key)          Lang::Get()->Translate(key, "Other", kLanguageEnglish )
#define EnglishLevel(key)           Lang::Get()->Translate(key, "LevelsNames", kLanguageEnglish )

//#define LocalStrFile(key, table)    "un"
//#define LocalLevel(key)             "un"
//#define LocalString(key)            "un"
//----------------------------------------------------------------------
using namespace std;
USING_NS_CC;

//----------------------------------------------------------------------
class Lang
{
public:
	static Lang* Get();
	static void Destroy();
	~Lang();
    
    bool IsChinese();
    bool IsJapanese();
    bool IsRussian();
    
    const char *Translate( const char *key, const char *table );
    const char *Translate( const char *key, const char *table, LanguageType lang );
	const char* GetLang( const char *file );
    void SetLang( LanguageType lang );

private:
	Lang();
   
	void ClearFullpath();
//	void ClearFilename();
	void SetResourcePath( const char *langName );
    void SetResourcePaths( LanguageType lang );

private:    
	static Lang  *_sInstance;
	static const int  LANG_FULLPATH_SIZE	=   256;
	static const int  LANG_PREFIX_SIZE	= 32;
	
	char	_langFullpath[LANG_FULLPATH_SIZE];
	char	_langPrefix[LANG_PREFIX_SIZE];
};
//----------------------------------------------------------------------
#endif
