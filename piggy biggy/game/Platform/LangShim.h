#ifndef piggy_biggy_LangShim_h
#define piggy_biggy_LangShim_h
//---------------------------------------------------------------
#include "cocos2d.h"
//---------------------------------------------------------------
class LangShim
{
public:
    static const char* Translate( const char *key, const char *table );
    static const char* Translate( const char *key, const char *table, cocos2d::LanguageType lang );
    static void SetLang( cocos2d::LanguageType lang );
};
//---------------------------------------------------------------
#endif
