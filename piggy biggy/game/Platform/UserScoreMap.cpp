#include "UserScoreMap.h"
#include "Debug/MyDebug.h"
//-------------------------------------------------------
// User Score Map
//-------------------------------------------------------
UserScoreMap::UserScoreMap()
{
}
//-------------------------------------------------------
void UserScoreMap::Dump()
{
#ifdef DEBUG
    D_LOG("\n");
    for ( iterator it = begin(); it != end(); it++ )
    {
        D_LOG("User: %s, seocnds: %d", it->first.c_str(), it->second );
    }
#endif
}