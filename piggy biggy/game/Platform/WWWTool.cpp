#include "WWWTool.h"
#include <stddef.h>
#include <Debug/MyDebug.h>
#include "WWWToolShim.h"

//---------------------------------------------------------------------------------
WWWTool* WWWTool::_sInstance = NULL;
//---------------------------------------------------------------------------------
WWWTool* WWWTool::Get()
{
    if ( ! _sInstance )
    {
        _sInstance = new WWWTool();
    }
    return _sInstance;
}
//---------------------------------------------------------------------------------
void WWWTool::Destroy()
{
    if ( _sInstance )
    {
        delete _sInstance;
        _sInstance = NULL;
    }
}
//---------------------------------------------------------------------------------
WWWTool::WWWTool()
{
    
}
//---------------------------------------------------------------------------------
void WWWTool::Http_FailedWithError( void *connection, int error )
{
    ListenersMap::iterator listener;
    listener = _listenersMap.find( connection );
    
    if ( listener != _listenersMap.end() )
    {
        if ( listener->second )
            listener->second->Http_FailedWithError( connection, error );
        _listenersMap.erase( listener );
    }
}
//---------------------------------------------------------------------------------
void WWWTool::Http_FinishedLoading( void *connection, const void *data, int len )
{
    ListenersMap::iterator listener;
    listener = _listenersMap.find( connection );
    
    if ( listener != _listenersMap.end() )
    {
        _listenersMap.erase( listener );

        if ( listener->second )
            listener->second->Http_FinishedLoading( connection, data, len );
    }
}
//---------------------------------------------------------------------------------
void WWWTool::CancelHttpGet( WWWToolListener *listener )
{
    if ( ! listener )
        return;
    
    for ( ListenersMap::iterator it = _listenersMap.begin(); it != _listenersMap.end(); it++ )
        if ( listener == it->second )
        {
            _listenersMap.erase( it );
            break;
        }
}
//---------------------------------------------------------------------------------
WWWTool::~WWWTool()
{
    WWWToolShim::Destroy();
    _listenersMap.clear();
}
//---------------------------------------------------------------------------------
void WWWTool::HttpGet(const char *url, WWWToolListener *listner )
{
    for ( ListenersMap::iterator it = _listenersMap.begin(); it != _listenersMap.end(); it++ )
    {
        if ( listner == it->second )
            return;
    }
    
    void *connection;
    connection = WWWToolShim::HttpGet( url );
    
    if ( listner && connection )
    {
        if ( _listenersMap.find(connection) == _listenersMap.end() )
        {
            _listenersMap[connection] = listner;
        }
    }
}
//---------------------------------------------------------------------------------
void WWWTool::VisitWWW(const char *url)
{
    WWWToolShim::VisitWWW(url);
}
//---------------------------------------------------------------------------------
