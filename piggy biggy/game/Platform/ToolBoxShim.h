#ifndef piggy_biggy_ToolBoxShim_h
#define piggy_biggy_ToolBoxShim_h
//------------------------------------------------------------------------
#include "GameGlobals.h"
#include <string>
#include "cocos2d.h"
//------------------------------------------------------------------------
using namespace std;
USING_NS_CC;
//------------------------------------------------------------------------
class ToolBoxShim
{
public:
    static DeviceType GetDeviceType();
    static std::string GetMacAddress();
    static std::string GetMacHash();
    static std::string GetMD5( const char *msg );
    static std::string GetChallenge();
    
    static std::string UTF8Encode( const char *msg );

    static cocos2d::CCSprite* SpriteFromData( const void *data, int len );
    static cocos2d::CCSprite* SpriteFromPlatofrmImage( void *data );

    static void ShowSendEmailView( const char *cc, const char *subject );
    static bool ShouldRotate90Degree();
    
    static void DebugMsgBox( const char *title, const char *msg );
};
//------------------------------------------------------------------------
#endif
