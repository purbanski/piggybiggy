#ifndef __GAME_H__
#define __GAME_H__

#include "cocos2d.h"
#include "LevelFactory.h"
#include "Levels/Level.h"
#include "Levels/LevelMenu.h"
#include "GameStatus.h"
#include "Levels/BuyChecker.h"

USING_NS_CC;

//--------------------------------------------------------------------------------
class Game
{
public :
typedef enum
	{
		eLayerLevel		= -10,
		eLayerEditor	= 20,
		eLayerDrawDebug	= 15,
		eLayerGameMenu	= 25,
	} Layers;

typedef enum
	{
		eLevelToLevel = 1,
		eLevelRestart
	} LevelTransitionType;

public:
	static Game* Get();
	static void Destroy();

	~Game();

	Level* CreateLevel( GameTypes::LevelEnum levelEnum );
	void CreateAndPlayLevel( GameTypes::LevelEnum levelEnum, LevelTransitionType transitionType, bool asyncLoad = true );
	static void DoCreateLevel( void * );

	Level* GetRuningLevel() { return _level; }
	void  ResetRuningLevel();
	GameStatus::LevelStatus GetRuningLevelStatus();
	GameStatus* GetGameStatus(){ return &_gameStatus; }
    LevelOrientation GetLastLevelOrientation() { return _lastPlayedLevelOrient; }
    
	void LevelFinished();
	void PlayNextLevel();
	void SkipLevel();
	void RestartLevel();

	void ToggleDebugDraw();
	LevelFactory *GetLevelFactory() { return &_levelFactory; }

	// This is scale for a game
	// level scale is for editor
	void AdjustScale();
	void SetScale( float scale ){ _scale = scale; }
	float GetScale(){ return _scale; }
	void DumpMissingSprites();

	void PauseGame();
	void ResumeGame();
    void GameCenterUpdate();
    
    bool CheckForSkipLevelIntro();
        
private:
	Game();
	void Init();
//    bool SkipLevelIntro();

private:
	static Game*	_sInstance;
	Level			*_level;
	LevelFactory	_levelFactory;
	bool			_isSchedule;
	GameStatus		_gameStatus;
	float			_scale;
	BuyChecker		_buyChecker;

	GameTypes::LevelEnum	_levelToCreate;
	LevelTransitionType		_transitionType;
    LevelOrientation        _lastPlayedLevelOrient;
};
//--------------------------------------------------------------------------------
#endif
