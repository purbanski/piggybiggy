#include "unFile.h"
#include "Game.h"
#include "GameStatus.h"
#include "GameConfig.h"
#include "Levels/LevelList.h"
#include "Levels/Pockets.h"
#include "Platform/ToolBox.h"
#include "Platform/GameCenter.h"
#include "RunLogger/RunLogger.h"
#include "Appirater/cppAppirater.h"
#include "Platform/ScoreTool.h"
#include "Platform/Lang.h"

//-------------------------------------------------------------------------------------------------------
// GameLevelStatus
//------------------------------------------------------------------------------------------------------
GameLevelSequence::GameLevelSequence()
{
	PocketManager::PocketLevels levels;
	PocketManager::PocketLevels::iterator it;
	
	levels = PocketManager::Get()->GetLevelsFromPocket( GameTypes::ePocketBlue );
	for ( it = levels.begin(); it != levels.end(); it++ )
		push_back( (*it) );

	levels = PocketManager::Get()->GetLevelsFromPocket( GameTypes::ePocketMoro );
	for ( it = levels.begin(); it != levels.end(); it++ )
		push_back( (*it) );
	
	levels = PocketManager::Get()->GetLevelsFromPocket( GameTypes::ePocketRasta );
	for ( it = levels.begin(); it != levels.end(); it++ )
		push_back( (*it) );

	levels = PocketManager::Get()->GetLevelsFromPocket( GameTypes::ePocketJapan );
	for ( it = levels.begin(); it != levels.end(); it++ )
		push_back( (*it) );

	levels = PocketManager::Get()->GetLevelsFromPocket( GameTypes::ePocketScience );
	for ( it = levels.begin(); it != levels.end(); it++ )
		push_back( (*it) );

	return;
}
//-------------------------------------------------------------------------------------------------------
int GameLevelSequence::GetLevelIndex( GameTypes::LevelEnum level )
{
	int index;
	index = 0;

	for ( iterator it = begin(); it != end(); it++ )
	{
		if ( *it == level )
			return index;
		index++;
	}
	return -1;
}
//-------------------------------------------------------------------------------------------------------
void GameLevelSequence::Dump()
{
	D_LOG("------------ DUMP ---------------")
	for ( iterator it = begin(); it != end(); it++ )
	{
		D_LOG("%d", (*it))
	}
}

//-------------------------------------------------------------------------------------------------------




//-------------------------------------------------------------------------------------------------------
// Game Status
//-------------------------------------------------------------------------------------------------------
GameStatus::GameStatus()
{
	_skipsLeft = Config::GameSkipMaxCount;
	_status._boughtCheck = false;
    _encryptionKey = string("12345678");
   
    _status._isMusicMute    = false;
    _status._isEffectsMute  = false;
    _status._fbLogged       = false;
    _status._skipUsed       = false;
    _status._language       = (LanguageType) -1;
    
	ResetPreviouslyPlayedLevel();

	if ( ! CheckIfFileExists() )
		InitStatusFile();

	LoadStatusFile();

    if ( _status._language == -1 )
        _status._language = CCApplication::getCurrentLanguage();
    Lang::Get()->SetLang( GetLang() );
    
    _status._appRunTimes++;
	UpdateCurrentLevel();

#ifdef BUILD_EDITOR
	UnlockLevels();
#endif 
}
//------------------------------------------------------------------------------------------------------
GameStatus::~GameStatus()
{
    _encryptionKey.clear();
	Save();
}
//-------------------------------------------------------------------------------------------------------
void GameStatus::UpdateLanguage()
{
    Lang::Get()->SetLang( _status._language );
}
//-------------------------------------------------------------------------------------------------------
LanguageType GameStatus::GetLang()
{
    return _status._language;
}
//-------------------------------------------------------------------------------------------------------
void GameStatus::SetLang( LanguageType lang )
{
    _status._language = lang;
    RLOGLANG( lang );
    
    Save();
    UpdateLanguage();
}
//-------------------------------------------------------------------------------------------------------
void GameStatus::Save()
{
    D_INT(_status._currentLevel )

	unFile* file = unDocumentFileOpen( Config::GameStatusFilename, "w+b");
	if ( ! file )
	{
		unAssertMsg(GameStatus, false, ("Status file can't be saved"));
		return;
	}

	unFileWrite( &_status, sizeof( StatusV2 ), 1, file );
	unFileClose( file );
    
//    CCLog("key %s", _encryptionKey.c_str() );
//    //------------------------
//    // encrypt
//    NSData *data = [ NSData dataWithBytes:(const void *)&_status length:sizeof( Status ) ];
//    NSData *dataKey = [ NSData dataWithBytes:_encryptionKey.c_str() length:_encryptionKey.length() ];
//    
//    NSData* encryptedData = [FBEncryptorAES encryptData:data
//                                                    key:dataKey
//												     iv:NULL];
//    string fullpath;
//    fullpath = IOSgetFilePath();
//    fullpath.append( Config::GameStatusFilename );
//
//    NSString *path = [ NSString stringWithUTF8String:(fullpath.c_str())];
//    
//    [encryptedData writeToFile:path atomically:true ];
}
//------------------------------------------------------------------------------------------------------
void GameStatus::InitStatusFile()
{
	ResetStatus();
    _status._version = 2;
    
#ifdef BUILD_FOR_FRIEND
	UnlockLevels();
#endif
    Save();
}
//------------------------------------------------------------------------------------------------------
bool GameStatus::CheckIfFileExists()
{
	unFile *file = unDocumentFileOpen( Config::GameStatusFilename, "rb" );
	if ( file )
	{
		unFileClose( file );
		return true;
	}
	else 
		return false;
}
//------------------------------------------------------------------------------------------------------
void GameStatus::LoadStatusFile()
{
	unFile *file = unDocumentFileOpen( Config::GameStatusFilename, "rb" );
	if ( ! file )
	{
		unAssertMsg(GameStatus, false, ("Status file does not exist - fatal"));
		return;
	}

	unFileRead( &_status , sizeof( StatusV2 ), 1, file ) ;
	unFileClose( file );

    if ( _status._version == 1 )
    {
        UpdateStatusFileToV2();
    }
	D_INT(_status._currentLevel )

	CountSkippedLevels();
    
    //    if ( ! unDocumentFileCheckExists( Config::GameStatusFilename ))
    //  return;
    
//     CCLog("key %s", _encryptionKey.c_str() );
//    //------------------------
//    // decrypt
//    string fullpath;
//    NSData *key;
//    NSData *encryptedData;
//    NSData* decryptedData;
//    
//    fullpath = IOSgetFilePath();
//    fullpath.append( Config::GameStatusFilename );
//
//    key = [ NSData dataWithBytes:_encryptionKey.c_str() length:sizeof( _encryptionKey.length() ) ];
//    
//    encryptedData = [ NSData dataWithContentsOfFile: [NSString stringWithUTF8String:( fullpath.c_str() )]];
//    decryptedData = [ FBEncryptorAES decryptData:encryptedData
//                                             key:key
//                                              iv:NULL];
//    void *pStatus;
//    pStatus = (void *) &_status;
//    memset(pStatus, 0, sizeof(Status));
//    
//    [ decryptedData getBytes:pStatus length:sizeof(Status) ];
    
  	CountSkippedLevels();
}
//------------------------------------------------------------------------------------------------------
GameStatus::LevelStatus GameStatus::GetLevelStatus( GameTypes::LevelEnum level )
{
	return _status._statusTable[ (int) level ];
}
//------------------------------------------------------------------------------------------------------
GameStatus::LevelStatus GameStatus::GetCurrentLevelStatus()
{
	return _status._statusTable[ _status._currentLevel ];
}
//------------------------------------------------------------------------------------------------------
GameStatus::LevelClueStatus GameStatus::GetLevelClueStatus( GameTypes::LevelEnum level )
{
	return _status._clueStatusTable[ (int) level ];
}
//------------------------------------------------------------------------------------------------------
GameStatus::LevelClueStatus GameStatus::GetCurrentLevelClueStatus()
{
	return GetLevelClueStatus( _status._currentLevel );
}
//------------------------------------------------------------------------------------------------------
void GameStatus::ResetGameStatus()
{
	ResetPreviouslyPlayedLevel();
	InitStatusFile();
	LoadStatusFile();
    
    ccpAppirater::RestartTracking();
}
//------------------------------------------------------------------------------------------------------
void GameStatus::ResetStatus()
{
    D_INT( _status._boughtCheck )
    D_INT( _status._isMusicMute )
    D_INT( _status._isEffectsMute )
    
	//--------------------------
	// Set Levels locked
	for ( int i = GameTypes::eLevel_Start; i < GameTypes::eLevel_Finish; i++ )
	{
		_status._statusTable[i] =  eLevelStatus_Locked;
		_status._clueStatusTable[i]._clueProcessed = 0;
		_status._clueStatusTable[i]._timeout = -1;
		_status._levelNameShowed [i] = false;
		_status._scoreTable[ i ].Reset();
        
        _status._levelRestartCount[i] = 0;
	}

	//--------------------------
	// Pocket played anim to false
	for ( int i = GameTypes::ePocketNone; i <= GameTypes::ePocketScience; i++ )
		_status._pocketOpenAnimPlayed[ i ] = false;

    _status._pocketOpenAnimPlayed[ ePocketNone ] = true;
    //_status._pocketOpenAnimPlayed[ ePocketBlue ] = true;
	

	//--------------------------
	// Set Levels locked
	_status._currentLevel = _levelSeq.at( 0 );
	_status._statusTable[ _status._currentLevel ] =  eLevelStatus_LastOpen;


	//--------------------------
	// Set Music
	//_status._isEffectsMute	= false;
	//_status._isMusicMute	= false;


	//--------------------------
	// Set Skips
	_skipsLeft = Config::GameSkipMaxCount;
	
	
	//--------------------------
	// Set Pocket Status
	for ( int i = 0; i < 10; i++ )
		_status._pocketStatusTable[ i ] = GameTypes::ePocketStatusLocked;
	
	_status._pocketStatusTable[ 0 ] = GameTypes::ePocketStatusOpen;
	_status._pocketStatusTable[ 1 ] = GameTypes::ePocketStatusOpen;


	//--------------------------
	// Reset one time messages
	for ( int i = GameTypes::eOTMsg_First; i < GameTypes::eOTMsg_Last; i++ )
	{
		_status._oneTimeMessageStatus[i] = 0;
	}

    //--------------------------
	// Reset one time messages
	for ( int i = GameTypes::eOTMsgRequest_First; i < GameTypes::eOTMsgRequest_Last; i++ )
	{
		_status._oneTimeMessageRequests[i] = 0;
	}

    //--------------------------
	// App Run
    _status._appRunTimes = 0;
    
    _status._skipUsed = false;
}
//------------------------------------------------------------------------------------------------------
void GameStatus::SetCurrentLevelStatus( LevelStatus status )
{
	int i;
	i = ( int ) _status._currentLevel;
	_status._statusTable[ i ] = status;

	CountSkippedLevels();
}
//------------------------------------------------------------------------------------------------------
void GameStatus::UnlockLevels()
{
#ifdef BUILD_FOR_FRIEND
	for ( GameLevelSequence::iterator it = _levelSeq.begin(); it != _levelSeq.end(); it++ )
		_status._statusTable[ (*it) ] = eLevelStatus_Done;
#else
	for ( int i = 0; i < GameTypes::eLevel_Finish; i++)
		_status._statusTable[ i ] = eLevelStatus_Done;

	for ( int i = 0; i <= GameTypes::ePocketScience; i++)
		_status._pocketStatusTable[ i ] = ePocketStatusOpen;
#endif
}
//------------------------------------------------------------------------------------------------------
void GameStatus::CountSkippedLevels()
{
	_skipsLeft = Config::GameSkipMaxCount;
	for ( int i = 0; i < GameTypes::eLevel_Finish; i++ )
	{
		if ( _status._statusTable[ i ] == eLevelStatus_Skipped )
			_skipsLeft--;
	}
}
//------------------------------------------------------------------------------------------------------
void GameStatus::SetCurrentLevel( GameTypes::LevelEnum level )
{
	_status._currentLevel = level;
	Save();
}
//------------------------------------------------------------------------------------------------------
void GameStatus::SetLevelStatus( GameTypes::LevelEnum level, LevelStatus status )
{
	_status._statusTable[ (int) level ] = status;
}
//------------------------------------------------------------------------------------------------------
void GameStatus::SetMusicMute( bool mute )
{
	_status._isMusicMute = mute;
    Save();
}
//------------------------------------------------------------------------------------------------------
void GameStatus::SetEffectsMute( bool mute )
{
	_status._isEffectsMute = mute;
    Save();
}
//------------------------------------------------------------------------------------------------------
bool GameStatus::GetMusicMute()
{
	return _status._isMusicMute;
}
//------------------------------------------------------------------------------------------------------
bool GameStatus::GetEffectsMute()
{
	return _status._isEffectsMute;
}
//------------------------------------------------------------------------------------------------------
void GameStatus::SetLevelClueStatus( GameTypes::LevelEnum level, GameStatus::LevelClueStatus clueStatus )
{
	_status._clueStatusTable[ level ] = clueStatus;
}
//------------------------------------------------------------------------------------------------------
void GameStatus::SetCurrentLevelClueStatus( LevelClueStatus clueStatus )
{
	SetLevelClueStatus( _status._currentLevel, clueStatus );
}
//------------------------------------------------------------------------------------------------------
void GameStatus::LevelNext()
{
	_previousPlayedLevel = _status._currentLevel;
	_status._currentLevel = GetNextUndoneLevel();
	Save();
}
//------------------------------------------------------------------------------------------------------
void GameStatus::UpdateCurrentLevel()
{
	if ( ! HasGameFinished() )
	{
		_status._currentLevel = GetNextUndoneLevel();
		Save();
	}
}
//------------------------------------------------------------------------------------------------------
GameTypes::LevelEnum GameStatus::GetNextUndoneLevel()
{
	for ( GameLevelSequence::iterator it = _levelSeq.begin(); it != _levelSeq.end(); it++ )
	{
		if ( _status._statusTable[ (*it) ] == eLevelStatus_Locked || 
			_status._statusTable[  (*it) ] == eLevelStatus_LastOpen )
		{
			return *it;
		}
	}


	//----------------------------------------
	// if we didn't find LastOpen level
	// check for SkippedLevel
	for ( GameLevelSequence::iterator it = _levelSeq.begin(); it != _levelSeq.end(); it++ )
	{
		if ( _status._statusTable[ *it ] == eLevelStatus_Skipped )
			return *it;
	}


	//----------------------------------------
	// All level done no skips
	if ( _levelSeq.GetLevelIndex( _status._currentLevel ) != _levelSeq.size() - 1 )
	{
		return _levelSeq.at( _levelSeq.GetLevelIndex( _status._currentLevel ) + 1 );
	}
	else
		return _levelSeq.at( 0 );
}
//------------------------------------------------------------------------------------------------------
bool GameStatus::HasGameFinished()
{
	CountSkippedLevels();
	
	if ( _skipsLeft != Config::GameSkipMaxCount )
		return false;

	
	for ( GameLevelSequence::iterator it = _levelSeq.begin(); it != _levelSeq.end(); it++ )
		if ( _status._statusTable[ *it ] != eLevelStatus_Done )
			return false;

	return true;
}
//------------------------------------------------------------------------------------------------------
bool GameStatus::NoMoreSkips()
{
	GameTypes::LevelEnum lastLevel;

	lastLevel = _levelSeq.at( _levelSeq.size() - 1 );

	if ( 
		( _status._currentLevel == lastLevel ) ||
		( _status._statusTable[ lastLevel ] == eLevelStatus_Done ) ||
		( _status._statusTable[ lastLevel ] == eLevelStatus_Skipped ))

	{
		return true;
	}

	return false;
}
//------------------------------------------------------------------------------------------------------
void GameStatus::SetToFirstLevel()
{
	_status._currentLevel = _levelSeq.at( 0 );
}
//------------------------------------------------------------------------------------------------------
void GameStatus::SetCurrentPocket( GameTypes::PocketType pocketEnum )
{
	//_status._currentPocket = pocketEnum;
}
//------------------------------------------------------------------------------------------------------
bool GameStatus::GetPocketOpenAnimPlayed( GameTypes::PocketType pocket )
{
    return _status._pocketOpenAnimPlayed[ pocket ];
}
//------------------------------------------------------------------------------------------------------
void GameStatus::SetPocketOpenAnimPlayed( GameTypes::PocketType pocket, bool played )
{
    _status._pocketOpenAnimPlayed[ pocket ] = played;
    Save();
}
//------------------------------------------------------------------------------------------------------
void GameStatus::SetPocketStatus( GameTypes::PocketType pocket, GameTypes::PocketStatusType status )
{
	_status._pocketStatusTable[ (int) pocket ] = status;
}
//------------------------------------------------------------------------------------------------------
GameTypes::PocketStatusType GameStatus::GetPocketStatus( GameTypes::PocketType pocket )
{
	return _status._pocketStatusTable[ (int) pocket ];
}
//------------------------------------------------------------------------------------------------------
GameTypes::PocketType GameStatus::GetCurrentPocket()
{
	return PocketManager::Get()->LevelInWhichPocket( _status._currentLevel );
}
//------------------------------------------------------------------------------------------------------
int GameStatus::GetLockedLevelCount( GameTypes::PocketType pocket )
{
	D_INT( Game::Get()->GetGameStatus()->GetPocketStatus( pocket ) )
	if ( Game::Get()->GetGameStatus()->GetPocketStatus( pocket ) != GameTypes::ePocketStatusLocked )
		return 10;

	//--------------------------
	// First pocket can't be checked against
	// previous pocket
	if ( pocket == GameTypes::ePocketBlue )
		return 10;

	//----------------------------------
	// Check
	int count;
	GameTypes::PocketType previousPocket;
	PocketManager::PocketLevels levels;

	count = 0;
	previousPocket = ( GameTypes::PocketType ) ( (int) pocket - 1 );

	levels = PocketManager::Get()->GetLevelsFromPocket( previousPocket );
	D_SIZE(levels )
	for ( PocketManager::PocketLevels::iterator it = levels.begin(); it != levels.end(); it++ )
	{
		if ( _status._statusTable[ *it ] == eLevelStatus_Locked || _status._statusTable[ *it ] == eLevelStatus_LastOpen )
			count++;
	}	

	D_INT( count )
	return count;
}

//------------------------------------------------------------------------------------------------------
unsigned int GameStatus::GetCoinCount( GameTypes::LevelEnum level )
{
	int hours	= _status._scoreTable[ level ].GetHoursCount() ;
	int minutes	= _status._scoreTable[ level ].GetMinutesCount() ;
	int seconds	= _status._scoreTable[ level ].GetSecondsCount() ;
	int secondsTotal;

	secondsTotal = seconds + minutes * 60 + hours * 60 * 60;

    return ScoreTool::Get()->GetCoinCount( secondsTotal );
}
//------------------------------------------------------------------------------------------------------
void GameStatus::RandomStatus()
{
	GameTypes::LevelEnum level;

	for ( level = GameTypes::eLevel_Start; level != GameTypes::eLevel_Finish; level = (GameTypes::LevelEnum) (((int) level) +1) )
		_status._scoreTable[ level ]._seconds = ( rand() % 900 + 1 );
}
//------------------------------------------------------------------------------------------------------
bool GameStatus::GetMsgPlayed( GameTypes::OneTimeMsg msg )
{
#ifdef INTRO_POINTER
    return true;
#endif
    
	if ( _status._oneTimeMessageStatus[ msg ] == 0 )
		return false;
	else
		return true;
}
//------------------------------------------------------------------------------------------------------
void GameStatus::SetMsgPlayed( GameTypes::OneTimeMsg msg )
{
	_status._oneTimeMessageStatus[ msg ] = 1;
}
//------------------------------------------------------------------------------------------------------
bool GameStatus::GetMsgPlayed( GameTypes::OneTimeMsgRequests msg )
{
#ifdef INTRO_POINTER
    return true;
#endif
    
	if ( _status._oneTimeMessageRequests[ msg ] == 0 )
		return false;
	else
		return true;
}
//------------------------------------------------------------------------------------------------------
void GameStatus::SetMsgPlayed( GameTypes::OneTimeMsgRequests msg )
{
	_status._oneTimeMessageRequests[ msg ] = 1;
    Save();
}
//------------------------------------------------------------------------------------------------------
bool GameStatus::HasBeenBought()
{
#if defined INTRO_POINTER
    return true;
#endif
    
#if defined BUILD_FREE && !defined BUILD_TEST
    return true;
#endif
	return _status._boughtCheck;
}
//------------------------------------------------------------------------------------------------------
void GameStatus::GameHasBeenBought()
{
	_status._boughtCheck = true;
	Save();
}
//------------------------------------------------------------------------------------------------------
void GameStatus::ToggleBilling()
{
	if ( _status._boughtCheck )
		_status._boughtCheck = false;
	else
		_status._boughtCheck = true;
	
	Save();
}
//------------------------------------------------------------------------------------------------------
void GameStatus::NutralizePreviouslyPlayedLevel()
{
	_previousPlayedLevel = _status._currentLevel;
}
//------------------------------------------------------------------------------------------------------
void GameStatus::ResetPreviouslyPlayedLevel()
{
	_previousPlayedLevel = (GameTypes::LevelEnum) -1;
}
//------------------------------------------------------------------------------------------------------
bool GameStatus::GetLevelNameShowed( GameTypes::LevelEnum level )
{
	return _status._levelNameShowed[ level ];
}
//------------------------------------------------------------------------------------------------------
void GameStatus::SetLevelNameShowed( GameTypes::LevelEnum level, bool showed )
{
	_status._levelNameShowed[level] = showed;
	Save();
}
//------------------------------------------------------------------------------------------------------
int GameStatus::GetLevelIndex( GameTypes::LevelEnum level )
{
    return _levelSeq.GetLevelIndex( level );
}
//------------------------------------------------------------------------------------------------------
int GameStatus::GetCurrentLevelIndex()
{
    return _levelSeq.GetLevelIndex( _status._currentLevel );
}
//------------------------------------------------------------------------------------------------------
unsigned int GameStatus::GetLevelScore( GameTypes::LevelEnum level )
{
    int score;
    unsigned int sec;
    
    sec = _status._scoreTable[ level ]._seconds;
    score = Config::MaxScoreLevelCoins - sec;
    
    if ( score < 0 )
        score = 0;
    
    return score;
}
//------------------------------------------------------------------------------------------------------
unsigned int GameStatus::GetLevelTime( GameTypes::LevelEnum level )
{
    return GetStatus()->_scoreTable[ level ]._seconds;
}
//------------------------------------------------------------------------------------------------------
unsigned int GameStatus::GetTotalScore()
{
    unsigned int score;
    score = 0;
    
    //---------------------------
    // Get all completed levels
    //
    GameTypes::LevelEnum levelEnum;
    PocketManager::PocketList pocketList;
    PocketManager::PocketLevels levelList;

    typedef set<LevelEnum> LevelSet;
    LevelSet completedLevels;
    
    pocketList = PocketManager::Get()->GetPocketList();
    
    for ( PocketManager::PocketList::iterator pocket = pocketList.begin(); pocket != pocketList.end(); pocket++ )
    {
        levelList = PocketManager::Get()->GetLevelsFromPocket( *pocket );
        PocketManager::PocketLevels siglePocketLevelList;
        
        for ( PocketManager::PocketLevels::iterator lit = levelList.begin(); lit != levelList.end(); lit++)
        {
            levelEnum = *lit;
            if ( GetLevelStatus( levelEnum ) == GameStatus::eLevelStatus_Done )
            {
                completedLevels.insert( levelEnum );
            }
        }
    }
    
    //-------------------
    // Count score
    for ( LevelSet::iterator it = completedLevels.begin(); it != completedLevels.end(); it++ )
    {
        score += GetLevelScore( *it );
        D_INT(score)        
    }
    
    return score;
}
//------------------------------------------------------------------------------------------------------
string GameStatus::GetLevelTimeString( LevelEnum level )
{
    int hours;
	int minutesA;
	int minutesB;
	int secondsA;
   	int secondsB;

	hours	= GetStatus()->_scoreTable[ level ].GetHoursCount() ;

    minutesA	= GetStatus()->_scoreTable[ level ].GetMinutesCount()  / 10;
	minutesB	= GetStatus()->_scoreTable[ level ].GetMinutesCount() ;
	minutesB	= minutesB - ( minutesB / 10 ) * 10;
    
	secondsA	= GetStatus()->_scoreTable[ level ].GetSecondsCount() / 10;
    secondsB	= GetStatus()->_scoreTable[ level ].GetSecondsCount() ;
	secondsB	= secondsB - ( secondsB / 10 ) * 10;
    
    stringstream ss;
    ss << hours << ":" << minutesA << minutesB << ":" << secondsA << secondsB;
    
    return ss.str();
}
//------------------------------------------------------------------------------------------------------
void GameStatus::UpdateStatusFileToV2()
{
    _status._fbLogged = false;
    _status._version = 2;
    Save();
}
//------------------------------------------------------------------------------------------------------
bool GameStatus::IsFbLogged()
{
    return _status._fbLogged;
}
//------------------------------------------------------------------------------------------------------
void GameStatus::SetFbLogged( bool logged )
{
    _status._fbLogged = logged;
    Save();
}
//------------------------------------------------------------------------------------------------------
int GameStatus::GetAppRunTime()
{
    return _status._appRunTimes;
}
//------------------------------------------------------------------------------------------------------
bool GameStatus::ShouldIntroStart()
{
    if ( Game::Get()->GetGameStatus()->GetAppRunTime() == 1 ||                      // run demo first time
        Game::Get()->GetGameStatus()->GetAppRunTime() == 5 ||                      // run demo fifth time
        ( Game::Get()->GetGameStatus()->GetAppRunTime() > 10 &&                     // run demo every 10 times
         ! ( ( Game::Get()->GetGameStatus()->GetAppRunTime() + 7 ) % 10 )))         // starting from 15th
        return true;
    else
        return false;
}
//------------------------------------------------------------------------------------------------------
void GameStatus::IncCoinCollectedCount()
{
    _status._coinCollected++;
    D_INT( _status._coinCollected );
}
//------------------------------------------------------------------------------------------------------
void GameStatus::IncBombDestroyedCount()
{
    _status._bombExploded++;
    D_INT( _status._bombExploded );
}
//------------------------------------------------------------------------------------------------------
void GameStatus::IncFrigleDestroyedCount()
{
    _status._frigleDestroyed++;
    D_INT( _status._frigleDestroyed );
}
//------------------------------------------------------------------------------------------------------
void GameStatus::IncFliesCutCount()
{
    D_INT( _status._fliesCut );
    _status._fliesCut++;
}
//------------------------------------------------------------------------------------------------------
void GameStatus::IncLevelRestart(LevelEnum level)
{
    D_INT( _status._levelRestartCount[level] );
    _status._levelRestartCount[level]++;
}
//------------------------------------------------------------------------------------------------------
unsigned int GameStatus::GetLevelRestartCount( LevelEnum level )
{
    return _status._levelRestartCount[level];
}
//------------------------------------------------------------------------------------------------------
bool GameStatus::GetSkipUsed()
{
    return _status._skipUsed;
}
//------------------------------------------------------------------------------------------------------
void GameStatus::SetSkipUsed()
{
    _status._skipUsed = true;
}
//------------------------------------------------------------------------------------------------------
void GameStatus::LogStats()
{
    unsigned int countDone;
    unsigned int countUndone;
    unsigned int countSkipped;

    countDone = 0;
    countSkipped = 0;
    countUndone = 0;
    
    GameLevelSequence levelSequence;
    
	for ( GameLevelSequence::iterator it = levelSequence.begin();
         it != levelSequence.end(); it++ )
	{
		if ( _status._statusTable[ *it ] == eLevelStatus_Skipped )
			countSkipped++;
        
        if ( _status._statusTable[ *it ] == eLevelStatus_Done )
			countDone++;

   		if ( _status._statusTable[ *it ] == eLevelStatus_Locked || _status._statusTable[ *it ] == eLevelStatus_LastOpen )
			countUndone++;
    }
    
    RLOG_SIII("GAME", "STATS", countDone, countSkipped, countUndone );
    RLOG_SS("GAME", "SKIPS_LEFT", _skipsLeft );
}

//unsigned int GameStatus::GetCountSkippedLevels()
//{
//    unsigned int count;
//    count = 0;
//    
//    GameLevelSequence levelSequence;
//    
//	for ( GameLevelSequence::iterator it = levelSequence.begin();
//         it != levelSequence.end(); it++ )
//	{
//		if ( _status._statusTable[ *it ] == eLevelStatus_Skipped )
//			count++;
//	}
//    return count;}
////------------------------------------------------------------------------------------------------------
//unsigned int GameStatus::GetCountDoneLevels()
//{
//    unsigned int count;
//    count = 0;
//    
//    GameLevelSequence levelSequence;
//    
//	for ( GameLevelSequence::iterator it = levelSequence.begin();
//         it != levelSequence.end(); it++ )
//	{
//		if ( _status._statusTable[ *it ] == eLevelStatus_Done )
//			count++;
//	}
//    return count;
//}
////------------------------------------------------------------------------------------------------------
//unsigned int GameStatus::GetCountUndoneLevels()
//{
//    unsigned int count;
//    count = 0;
//    
//    GameLevelSequence levelSequence;
//    
//	for ( GameLevelSequence::iterator it = levelSequence.begin();
//         it != levelSequence.end(); it++ )
//	{
//		if ( _status._statusTable[ *it ] == eLevelStatus_Locked || _status._statusTable[ *it ] == eLevelStatus_LastOpen )
//			count++;
//	}
//    return count;
//}
//------------------------------------------------------------------------------------------------------





//------------------------------------------------------------------------------------------------------


