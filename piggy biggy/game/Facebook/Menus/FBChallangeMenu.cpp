#include "FBChallangeMenu.h"
#include <string>
#include "Debug/MyDebug.h"
#include "GameConfig.h"
#include "Components/TrippleSprite.h"
#include "Components/FXMenuItem.h"
#include "Facebook/FBTool.h"
#include "Platform/ToolBox.h"
#include "Platform/ScoreTool.h"
#include "Platform/WWWTool.h"
#include "Components/CustomSprites.h"
#include "Tools.h"
#include <sstream>
#include "Game.h"
#include "Platform/Lang.h"
#include "RunLogger/RunLogger.h"
#include "RunLogger/GLogger.h"

using namespace std;

//-------------------------------------------
// FB Challange Menu
//--------------------------------------------
FBChallangeMenu* FBChallangeMenu::Create( LevelEnum level )
{
    FBChallangeMenu *node;
    node = new FBChallangeMenu( level );
    
    if ( node )
    {
        node->autorelease();
        node->Init();
        return node;
    }
    return NULL;
}
//------------------------------------------------------------------------
FBChallangeMenu::FBChallangeMenu( LevelEnum level )
{
    _levelTime = 0;
    _levelEnum = level;
    _networkMode = eMode_Idle;
}
//------------------------------------------------------------------------
FBChallangeMenu::~FBChallangeMenu()
{
    CancelNetworkRequest();
}
//------------------------------------------------------------------------
void FBChallangeMenu::Init()
{
    FBMenuBase::Init(CCSize( 100, 640.0f ));
    _slidingNode->SetClickableArea( CCPoint( 130.0f, 0.0f), CCPoint( 418.0f, 640.0f));
    
    AddBackground();
}
//------------------------------------------------------------------------
void FBChallangeMenu::CancelNetworkRequest()
{
    switch (_networkMode)
    {
        case eMode_FBGetRequested :
            FBTool::Get()->CancelRequestForListener( this );
            break;
            
        case eMode_HTTPGetUsers :
        case eMode_HTTPGetMyScore :
            WWWTool::Get()->CancelHttpGet( this );
            break;
            
        default:
            break;
    }
    
    _networkMode = eMode_Idle;
}
//------------------------------------------------------------------------
void FBChallangeMenu::Show()
{
    RLOG_SS("MENU_LEVEL_FB", "CHALLENGE", "LOADING");
    
    if ( _processing )
        return;

    if ( Game::Get()->GetGameStatus()->GetLang() == kLanguageGerman )
        SetupLabel( LocalString("Challenge"), 39 );
    else
        SetupLabel( LocalString("Challenge"));


    FBMenuBase::Show();
    SetupFades();
    
    if ( ! _internetAccess )
    {
        ShowNoInternetSprite();
        return;
    }
    
    if ( FBTool::Get()->IsLogged() )
    {
        FBTool::Get()->Read_FriendsList( this );
        _networkMode = eMode_FBGetRequested;
    }
    else
    {
        _processing = false;
        _networkMode = eMode_Idle;
    }
}
//------------------------------------------------------------------------
void FBChallangeMenu::FBRequestCompleted( FBTool::Request requestType, void *data )
{
    FBFriendsMap *friendsMap;
    friendsMap = (FBFriendsMap*)data;
    
    _fbFriendsMap = *friendsMap;
    D_SIZE(_fbFriendsMap);
    GetMyLevelScore();
}
//------------------------------------------------------------------------
void FBChallangeMenu::GetGameUsers()
{
    stringstream iUrl;
    stringstream iEnabledFriends;
    stringstream iAllFriends;
    
    iUrl << Config::FBRequestsURL << "challangeMultiFriends.php?";
    iUrl << "level=" << _levelEnum << "&";
    iUrl << "fbId=" << FBTool::Get()->GetFBUserId() << "&";
    iUrl << "friends=" << _fbFriendsMap.GetUsersString().c_str();
    
    D_STRING( iUrl.str());
    
    WWWTool::Get()->HttpGet( iUrl.str().c_str(), this );
    _networkMode = eMode_HTTPGetUsers;
}
//------------------------------------------------------------------------
void FBChallangeMenu::GetMyLevelScore()
{
    stringstream ssUrl;
    ssUrl << Config::FBScoreURL;
    ssUrl << "getMyLevelScore.php?";
    ssUrl << "fbId=" << FBTool::Get()->GetFBUserId().c_str() << "&";
    ssUrl << "level=" << _levelEnum;
    
    D_STRING(ssUrl.str());
    
    WWWTool::Get()->HttpGet( ssUrl.str().c_str(), this );
    _networkMode = eMode_HTTPGetMyScore;
}
//------------------------------------------------------------------------
void FBChallangeMenu::Http_FinishedLoading( void *connection, const void *data, int len )
{
    //---------------
    // Prepare data
    string temp;
    string strData;
    
    temp.append( (const char*)data );
    strData = string( temp, 0, len );

    if ( _networkMode == eMode_HTTPGetMyScore )
    {
        //process then
        stringstream(strData) >> _levelTime;
        GetGameUsers();
        
    }
    else if ( _networkMode == eMode_HTTPGetUsers )
    {
        _gameUsers = FBGameUsersVector::Create( _fbFriendsMap, strData.c_str() );
        AllDataLoaded();
    }
}
//------------------------------------------------------------------------
void FBChallangeMenu::Http_FailedWithError( void *connection, int error )
{
    _networkMode = eMode_Idle;
    _processing = false;

    if ( _loadingAnim )
    {
        _loadingAnim->Stop();
        _loadingAnim = NULL;
    }
    
    ShowNoInternetSprite();
}
//------------------------------------------------------------------------
void FBChallangeMenu::FBRequestError( int error )
{
    _networkMode = eMode_Idle;
    _processing = false;
    
    if ( _loadingAnim )
    {
        _loadingAnim->Stop();
        _loadingAnim = NULL;
    }
    
    ShowNoInternetSprite();
}
//------------------------------------------------------------------------
void FBChallangeMenu::AllDataLoaded()
{
    int count;
    float padding;

    padding = 175.0f;
    count = 0;
    
    _processing = false;
    _networkMode = eMode_Idle;
    
    if ( _loadingAnim )
    {
        _loadingAnim->Stop();
        _loadingAnim = NULL;
    }

    
    //---------------
    // Create menu
    SlidingNodeMenu *menu;
    menu = SlidingNodeMenu::menuWithItems( kCCMenuTouchPriority, _slidingNode, NULL, NULL );
    _slidingNode->GetContentNode()->addChild( menu, 100 );
   
    FBGameUsersVector gameUsers;
    gameUsers = _gameUsers.GetWithLimits( 10, 10, 5 );
    
    int countFriendsGameEnabled = 0;
    int countFriendsGameDisable = 0;
    int countUnknownGameEnabled = 0;

    for ( FBGameUsersVector::iterator it = gameUsers.begin(); it != gameUsers.end(); it++ )
    {
        CCMenuItem *menuItem;
        CCNode *profileNode;

        profileNode = GetProfileNode( _fbFriendsMap[ it->_fbId ].c_str(), it->_fbId.c_str(), it->_levelUserType );
        menuItem = GetMenuItem( it->_fbId.c_str(), count, padding );

        _slidingNode->AddToContent( profileNode );
        menu->addChild( menuItem, 10 );

        count++;
    
        switch (it->_levelUserType)
        {
            case GameTypes::eGameUser_FriendGameNotInstalled :
                countFriendsGameDisable++;
                break;
                
            case GameTypes::eGameUser_FriendLevelPassed :
            case GameTypes::eGameUser_FriendLevelNotPassed :
                countFriendsGameEnabled++;
                break;
                
            case GameTypes::eGameUser_UnknownPlayerLevelNotPassed :
                countUnknownGameEnabled++;
                break;

            case eGameUser_Unknown :
            default:
                break;
        }
    }

    
    CCPoint startingPos;
    startingPos = CCPoint( 0.0f, 0.0f );
    
    _slidingNode->GetContentNode()->AlignVertical( padding );
    _slidingNode->GetContentNode()->setPosition( CCPoint( 75.0f, 0.0f ));
    _slidingNode->GetContentNode()->setContentSize( CCSize( 100.0f, count * padding ));
   
	_slidingNode->SetInitPosition( startingPos );
    
    if ( count > 3 )
    {
        _slidingNode->ShowItem( 2 );
        _slidingNode->SetUpperLimitDelta( 130.0f );
        EnableSliding();
        menu->setPosition( CCPoint( 70.0f, 55.0f ));
    }
    else
    {
        _slidingNode->setPosition( CCPoint( 0.0f, _slidingNode->GetContentNode()->getContentSize().height / 4.0f ));
        DisableSliding();
        menu->setPosition( CCPoint( 70.0f, 65.0f ));
    }
    
    RLOG_SSIII("MENU_LEVEL_FB", "CHALLENGE", count, countFriendsGameEnabled, countFriendsGameDisable, countUnknownGameEnabled );

}
//------------------------------------------------------------------------
CCNode* FBChallangeMenu::GetProfileNode( const char *username, const char *fbUserId, GameLevelUsersType userType )
{
    CCSprite *profileSprite;
    FBUserFirstNameNode *labelUsername;
    TrippleSprite triSprite;
    
    string bgSpriteURL;
    string yourUserFbId;
    stringstream ss;

    float xbase;
    float ybase;
    
    xbase = -20.0f;
    ybase = 55.0f;
    
    profileSprite = FBProfileBigSprite::Create( fbUserId, userType  );
    
    labelUsername = FBUserFirstNameNode::Create( fbUserId, CCSize( 300.0f, 30.0f ), CCTextAlignmentCenter );
    labelUsername->SetColor( Config::FBMenuItemColor );
    
    switch ( userType )
    {
        case eGameUser_FriendLevelNotPassed :
        case eGameUser_FriendLevelPassed :
        case eGameUser_UnknownPlayerLevelNotPassed :
                labelUsername->setPosition( CCPoint ( xbase + 65.0f, ybase - 68.5f ));
            break;
            
        case eGameUser_FriendGameNotInstalled :
        case eGameUser_Unknown :
        default:
                labelUsername->setPosition( CCPoint ( xbase + 108.5f, ybase - 68.5f ));
            break;
    }


    bgSpriteURL.append( "Images/Other/none.png" );
    triSprite._normal	= CCSprite::spriteWithFile( bgSpriteURL.c_str() );

    profileSprite->setPosition( CCPoint( xbase + 100.0f, ybase ));

    
    CCSprite *retNode;
    retNode = CCSprite::spriteWithFile("Images/Other/none.png");
    retNode->addChild( profileSprite, 10 );
    retNode->addChild( labelUsername, 10 );
    
    return retNode;
}
//------------------------------------------------------------------------
CCMenuItem* FBChallangeMenu::GetMenuItem( const char *fbUserId, int count, float padding )
{
    CCSprite *profileSprite;
    TrippleSprite triSprite;
    
    string bgSpriteURL;
    string yourUserFbId;
    stringstream ss;

    float xbase;
    float ybase;
    
    xbase = 8.0f;
    profileSprite = FBProfileSprite::Create( fbUserId );
    
    bgSpriteURL.append( "Images/Other/none.png" );
    triSprite._normal	= CCSprite::spriteWithFile( "Images/Facebook/PromoNode/Challenge/button.png" );
    triSprite._selected	= CCSprite::spriteWithFile( "Images/Facebook/PromoNode/Challenge/button-selected.png" );
    triSprite._disable	= CCSprite::spriteWithFile( "Images/Facebook/PromoNode/Challenge/button.png" );

    profileSprite->setPosition( CCPoint( xbase + 70.0f, ybase + 55.0f ));

    FXMenuItemSpriteWithAnim *item;
    item = FXMenuItemSpriteWithAnim::Create( &triSprite, this, menu_selector( FBChallangeMenu::Menu_SelectUser ));

    FBFriendsMap::iterator it;
    it = _fbFriendsMap.find( fbUserId );
    
    item->setUserData( (void*) &(it->first) );
    item->setPosition( CCPoint( 0.0f, -(float)count * padding ));
    return item;
}
//------------------------------------------------------------------------
void FBChallangeMenu::Menu_SelectUser( CCObject *object )
{
    if ( ! ( _levelTime > 0 ))
        return;
        
    string *fbId;
    CCMenuItem *item;

    item = (CCMenuItem*) object;
    fbId = (string*) item->getUserData();

    D_CSTRING(fbId->c_str());
    FBTool::Get()->Request_LevelChallange(
                                          fbId->c_str(),
                                          Game::Get()->GetGameStatus()->GetCurrentLevel(),
                                          _levelTime );
}
//------------------------------------------------------------------------
void FBChallangeMenu::Hide()
{
    FBMenuBase::Hide();
    CancelNetworkRequest();
}
//------------------------------------------------------------------------


