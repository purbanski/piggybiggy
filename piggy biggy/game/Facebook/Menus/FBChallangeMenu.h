#ifndef __FBCHALLANGEMENU_H__
#define __FBCHALLANGEMENU_H__
//------------------------------------------------------------------------------
#include "GameTypes.h"
#include "FBMenuBase.h"
#include "Facebook/FBTool.h"
#include "Platform/WWWTool.h"
#include "Facebook/Menus/FBScoreMenu.h"
#include "Facebook/FBGameUsers.h"
//------------------------------------------------------------------------------
using namespace GameTypes;
//------------------------------------------------------------------------------
class FBChallangeMenu : public FBMenuBase, public FBToolListener, public WWWToolListener
{
public :
   static FBChallangeMenu* Create( LevelEnum level );
    ~FBChallangeMenu();

    virtual void Show();
    virtual void Hide();
    
    virtual void FBRequestCompleted( FBTool::Request requestType, void *data );
    virtual void FBRequestError( int error );

    virtual void Http_FailedWithError( void *connection, int error );
    virtual void Http_FinishedLoading( void *connection, const void *data, int len );

    void Menu_SelectUser(CCObject *sender);
    
private :
    FBChallangeMenu( LevelEnum level );
    void Init();
    void AllDataLoaded();
    void GetMyLevelScore();
    void GetGameUsers();
    
    CCNode* GetProfileNode( const char *username, const char *fbUserId, GameLevelUsersType userType );
    CCMenuItem* GetMenuItem( const char *fbUserId, int count, float padding );
    
    void CancelNetworkRequest();
    
private :
    typedef enum
    {
        eMode_Idle = 0,
        eMode_HTTPGetUsers,
        eMode_HTTPGetMyScore,
        eMode_FBGetRequested
    } NetworkMode;

    NetworkMode         _networkMode;
    FBFriendsMap        _fbFriendsMap;
    FBGameUsersVector   _gameUsers;
    int                 _levelTime;

};
//------------------------------------------------------------------------------
#endif

