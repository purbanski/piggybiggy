#include "FBMenuBase.h"
#include "Debug/MyDebug.h"
#include "Platform/Lang.h"
//-------------------------------------------------------------------------
FBMenuBase::FBMenuBase()
{
    _processing = false;
    _loadingAnim = NULL;
    _background = NULL;
    _menuLabel = NULL;
    _topFade = NULL;
    _bottomFade = NULL;
    
    _internetAccess = true;
    _noInternetSprite = NULL;

}
//-------------------------------------------------------------------------
FBMenuBase::~FBMenuBase()
{
    
}
//-------------------------------------------------------------------------
void FBMenuBase::Init( const CCSize &windowSize )
{
    _slidingNode = SlidingNode2::node( windowSize, Config::eMousePriority_FBHighScore );
    _slidingNode->setIsTouchEnabled( false );
    _slidingNode->SetLimitsType( SlidingNode2::eLimit_InLevel_Menu );
    addChild( _slidingNode, 10 );
}
//-------------------------------------------------------------------------
void FBMenuBase::Show()
{
    if ( _processing )
        return;
    
    _processing = true;
    
    if ( _loadingAnim && _loadingAnim->getParent() )
    {
        _loadingAnim->Stop();
    }
    _loadingAnim = LoadingAnim::Create();
    _loadingAnim->setPosition( CCPoint( 150.0, -10.0f ));
    _loadingAnim->setScale( 0.6f );
    addChild( _loadingAnim, 100 );
}
//-------------------------------------------------------------------------
void FBMenuBase::Hide()
{
    if ( _noInternetSprite )
        _noInternetSprite->Hide();
//        _noInternetSprite->SetOpacity(0);
    
    _slidingNode->ClearContent();
    if ( _menuLabel )
    {
        _menuLabel->runAction( CCSequence::actions(
                                                   CCFadeTo::actionWithDuration( 0.75f, 0 ),
                                                   CCCallFuncND::actionWithTarget( this, callfuncND_selector( FBMenuBase::RemoveMe ), _menuLabel),
                                                   NULL));
        _menuLabel = NULL;
    }
    
    if ( _topFade )
    {
        _topFade->removeFromParentAndCleanup( true );
        _topFade = NULL;
    }
    
    if ( _bottomFade )
    {
        _bottomFade->removeFromParentAndCleanup( true );
        _bottomFade = NULL;
    }
    
    if ( _background )
    {
        _background->removeFromParentAndCleanup(true);
        _background = NULL;
    }
    
    if ( _loadingAnim )
    {
        _loadingAnim->Stop();
        _loadingAnim=NULL;
    }
    
    _processing = false;
}
//-------------------------------------------------------------------------
void FBMenuBase::RemoveMe( CCNode *node, void *data )
{
    CCNode *n;
    n = (CCNode *) data ;
    n->removeFromParentAndCleanup( true );
}
//-------------------------------------------------------------------------
void FBMenuBase::SetupFades()
{
    float deltaX;
    float deltaY;
    
    _topFade = CCSprite::spriteWithFile( "Images/Facebook/PromoNode/topFade.png");
    _bottomFade = CCSprite::spriteWithFile( "Images/Facebook/PromoNode/bottomFade.png");
    
    deltaX = 10.0f;
    deltaY = 320.0f;
    
    D_POINT( _slidingNode->getPosition());
    D_POINT( _slidingNode->GetContentNode()->getPosition());
    
    _topFade->setPosition( CCPoint( _topFade->getContentSize().width / 2.0f + deltaX, deltaY - _topFade->getContentSize().height / 2.0f ));
    _bottomFade->setPosition( CCPoint( _bottomFade->getContentSize().width / 2.0f + deltaX, -deltaY + _bottomFade->getContentSize().height / 2.0f ));

    addChild( _topFade, 100 );
    addChild( _bottomFade, 100 );
}
//-------------------------------------------------------------------------
void FBMenuBase::AddBackground()
{
    if ( _background )
        return;
    
    _background = CCSprite::spriteWithFile( "Images/Facebook/PromoNode/background.png");
    _background->setOpacity(0);
    _background->setPosition( CCPoint( 135.5f, 0.0f ));
    addChild( _background, 0 );
    _background->runAction(CCFadeTo::actionWithDuration( 0.5f, 255 ));
}
//-------------------------------------------------------------------------
void FBMenuBase::ShowNoInternetSprite()
{
    _internetAccess = false;
    
    if ( _loadingAnim )
    {
        _loadingAnim->Stop();
        _loadingAnim = NULL;
    }
    
    if ( _noInternetSprite )
    {
        _noInternetSprite->Show();
    }
    
    else if ( ! _noInternetSprite )
    {
        _noInternetSprite = FadeInSprite::Create(2.0f, "Images/Facebook/PromoNode/noInternet.png");
        _noInternetSprite->setPosition( CCPoint( 160.0f, -16.0f ));
        addChild( _noInternetSprite, 255.0f );
    }
}
//-------------------------------------------------------------------------
void FBMenuBase::EnableSliding()
{
    // i don't think it really works
    // this only work when set on construction
    _slidingNode->setIsTouchEnabled( true );
}
//-------------------------------------------------------------------------
void FBMenuBase::DisableSliding()
{
    _slidingNode->setIsTouchEnabled( false );
    CCTouchDispatcher::sharedDispatcher()->removeDelegate( _slidingNode );    
}
//-------------------------------------------------------------------------
void FBMenuBase::SetupLabel( const char *text, unsigned int fontSize )
{
    _menuLabel = CCLabelTTF::labelWithString( text, Config::FBHighScoreFont, fontSize );
    _menuLabel->setColor( Config::FBMenuTitleColor );
    _menuLabel->setOpacity( 0 );
    addChild( _menuLabel, 120 );
    _menuLabel->setPosition( ccp( 145.0f, Config::GameSize.height / 2.0f - 30.0f ));
    _menuLabel->runAction( CCFadeTo::actionWithDuration( 0.75f, 255 ));
}
//-------------------------------------------------------------------------
