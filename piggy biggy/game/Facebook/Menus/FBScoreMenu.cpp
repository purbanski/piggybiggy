#include "FBScoreMenu.h"
#include <string>
#include "Debug/MyDebug.h"
#include "GameConfig.h"
#include "Components/TrippleSprite.h"
#include "Components/FXMenuItem.h"
#include "Facebook/FBTool.h"
#include "Platform/ToolBox.h"
#include "Platform/ScoreTool.h"
#include "Platform/WWWTool.h"
#include "Components/CustomSprites.h"
#include "Tools.h"
#include <sstream>
#include "Game.h"
#include "Platform/Lang.h"
#include "RunLogger/RunLogger.h"

using namespace std;

//-------------------------------------------
// FB Friends Scores Sorted Set
//--------------------------------------------
void FBFriendsScoresSortedSet::Dump()
{
#ifdef DEBUG
    for ( iterator it = begin(); it != end(); it++ )
    {
        (*it).Dump();;
        
    }
#endif
}


//-------------------------------------------
// FB Score Menu
//--------------------------------------------
FBScoreMenu* FBScoreMenu::Create( LevelEnum level )
{
    FBScoreMenu *node;
    node = new FBScoreMenu( level );
    
    if ( node )
    {
        node->autorelease();
        node->Init();
        return node;
    }
    return NULL;
}
//------------------------------------------------------------------------
FBScoreMenu::FBScoreMenu( LevelEnum level )
{
    _levelEnum = level;
    _loginToFbNode = NULL;
}
//------------------------------------------------------------------------
FBScoreMenu::~FBScoreMenu()
{
    if ( _processing )
        ScoreTool::Get()->CancelRequest( this );
}
//------------------------------------------------------------------------
void FBScoreMenu::Init()
{
    FBMenuBase::Init(CCSize( 100, 640.0f ));
    _slidingNode->SetClickableArea( CCPoint( 130.0f, 0.0f), CCPoint( 418.0f, 640.0f));
}
//------------------------------------------------------------------------
void FBScoreMenu::Show()
{
    RLOG_SS("MENU_LEVEL_FB", "HIGHSCORE", "LOADING");
    
    if ( _processing )
        return;
        
//    AddBackground();
    CleanUp();
    FBMenuBase::Show();
    
    if ( Game::Get()->GetGameStatus()->GetLang() == kLanguageSpanish )
        SetupLabel( LocalString("Highscore"), 39 );
    else
        SetupLabel( LocalString("Highscore"));
    
    
    if ( ! _internetAccess )
    {
        ShowNoInternetSprite();
        return;
    }

    
    if ( ! FBTool::Get()->IsLogged() )
    {
        ScoreTool::Get()->GetRandomScoreFromServer( this );
    }
    else
    {
        ScoreTool::Get()->GetScoresFromServer( this, _levelEnum );
    }
}
//------------------------------------------------------------------------
void FBScoreMenu::Hide()
{
    if ( _loginToFbNode )
    {
        _loginToFbNode->removeFromParentAndCleanup( true );
        _loginToFbNode = NULL;
    }
    
    if ( _processing )
    {
        ScoreTool::Get()->CancelRequest( this );
        _processing = false;
    }
    
    FBMenuBase::Hide();
}
//------------------------------------------------------------------------
void FBScoreMenu::ScoreTool_ScoreMapLoaded()
{
    D_HIT;
    
    if ( _loadingAnim )
    {
        _loadingAnim->Stop();
        _loadingAnim=NULL;
    }
    
    SetupFades();
    
    _processing = false;
    
    FBFriendsScoresSortedSet scoreMap;
      
    ScoreTool::Get()->GetHighscore( scoreMap );

    D_SIZE( scoreMap );
    scoreMap.Dump();
    
    if ( ! FBTool::Get()->IsLogged() )
    {
        AddYourScore( &scoreMap );
        D_SIZE( scoreMap );
    }
       
    int count;
    int myIndex;
    string myFbId;
        
    
    myFbId = FBTool::Get()->GetFBUserId();
    count = 0;
    myIndex = -1;
    
    for ( FBFriendsScoresSortedSet::iterator it = scoreMap.begin(); it != scoreMap.end(); it++ )
    {
//        for ( int i = 0; i < 8; i++ )
        {
            count++;
            if ( ! myFbId.compare( it->_fbId ))
                myIndex = count;
            
            _slidingNode->AddToContent(
                        GetMenuItem(
                                     it->_fbId.c_str(),
                                     it->_time,
                                     it->_score,
                                     count
                                     ));
        }
        
    }
    
    float padding;
    padding = 135.0f;

    CCPoint startingPos;
    startingPos = CCPoint( 0.0f, 0.0f );
    
    _slidingNode->GetContentNode()->AlignVertical( padding );
    _slidingNode->GetContentNode()->setPosition( CCPoint( 0.0f, 0.0f ));
    _slidingNode->GetContentNode()->setContentSize( CCSize( 100.0f, count * padding ));
   
	_slidingNode->SetInitPosition( startingPos );
   
    if ( count > 4 )
    {
        _slidingNode->ShowItem( myIndex );
        EnableSliding();
    }
    else
    {
        _slidingNode->setPosition( CCPoint( 0.0f, _slidingNode->GetContentNode()->getContentSize().height / 4.0f ));
        DisableSliding();
    }
  
    if ( ! FBTool::Get()->IsLogged() )
    runAction( CCSequence::actions(
                                   CCDelayTime::actionWithDuration( 4.0f ),
                                   CCCallFunc::actionWithTarget( this, callfunc_selector( FBScoreMenu::ShowArrow )),
                                   NULL ));
    
    RLOG_SS("MENU_LEVEL_FB", "HIGHSCORE", count );
}
//------------------------------------------------------------------------
void FBScoreMenu::ShowArrow()
{
    _loginToFbNode = FBLoginLabel::Create();
    addChild( _loginToFbNode, 255 );
        
    _loginToFbNode->setPosition( CCPoint( 170.0f, -210.0f ));
}
//------------------------------------------------------------------------
void FBScoreMenu::ScoreTool_RequestError()
{
    _processing = false;
    
    if ( _loadingAnim )
    {
        _loadingAnim->Stop();
        _loadingAnim=NULL;
    }
    
    ShowNoInternetSprite();
}
//------------------------------------------------------------------------
void FBScoreMenu::AddYourScore( FBFriendsScoresSortedSet* scoreMap )
{
    LevelEnum levelEnum;
    levelEnum = Game::Get()->GetGameStatus()->GetCurrentLevel();
    
    D_SIZE( *scoreMap );
    scoreMap->insert( FBScoreMenuRecord(
                                        "",
//                                       LocalString("FBNotLoggedYourScore"),
                                        "",
                                        Game::Get()->GetGameStatus()->GetLevelTime( levelEnum ) // time
                                       )
                    );
    D_SIZE( *scoreMap );
                                                 
}
//------------------------------------------------------------------------
CCNode* FBScoreMenu::GetMenuItem( const char *userFbId, int time, int score, int position )
{
    CCSprite *profileSprite;
    CCLabelTTF *labelTime;
    CCLabelBMFont *labelPosition;
    FBUserFirstNameNode *labelUsername;
    FBCoinScoreNode *coinScoreNode;

    string yourUserFbId;
    stringstream ss;

    float xbase;
    float ybase;
    
    xbase = 8.0f;
    ybase = 0.0f;
    
    //---------------------
    // if not logged and  me
    if ( ! FBTool::Get()->IsLogged()  &&
        ( ! strncmp("0", userFbId, strlen(userFbId) ) )
        )
    {
        profileSprite = FBProfileSprite::Create();
    }
    // if logged and me
    else if ( FBTool::Get()->IsLogged()  &&
             ( ! strncmp(FBTool::Get()->GetFBUserId().c_str(), userFbId, strlen(userFbId) ) ) )
    {
        profileSprite = FBProfileSprite::CreateBold( userFbId );
    }
    else
    {
        profileSprite = FBProfileSprite::Create( userFbId );
    }
    
    ss << position ;
    labelPosition = CCLabelBMFont::labelWithString( ss.str().c_str(), Config::FBHighScorePositionFont );
    labelPosition->setPosition( CCPoint( xbase + 40.0f, ybase + 22.5f ));
    
    coinScoreNode = FBCoinScoreNode::Create( time );
    coinScoreNode->setPosition( CCPoint( xbase + 190.0f, ybase + 20.0f ));
    
    labelUsername = FBUserFirstNameNode::Create( userFbId );
    labelUsername->SetColor( Config::FBMenuItemColor );
    labelUsername->setPosition( CCPoint ( xbase + 190.0f, ybase + 80.0f ));

    labelTime = CCLabelTTF::labelWithString( Tools::ConvertSecToTime( time ).c_str(),  Config::FBHighScoreFont, 28 );
    
    
    labelTime->setColor( Config::FBMenuItemColor );
    labelTime->setPosition( CCPoint( xbase + 190.0f, ybase + 50.0f ));
    labelTime->setScale( 0.9f );
    labelTime->setOpacity( 0 );
    labelTime->runAction( CCFadeTo::actionWithDuration( 1.0f, 255 ));

    profileSprite->setPosition( CCPoint( xbase + 70.0f, ybase + 55.0f ));

    CCSprite *retNode;
    retNode = CCSprite::spriteWithFile("Images/Other/none.png");

    retNode->addChild( labelTime, 10 );
    retNode->addChild( labelPosition, 50 );
    retNode->addChild( profileSprite, 10 );
    retNode->addChild( labelUsername, 20 );
    retNode->addChild( coinScoreNode, 20 );
    
    
    D_LOG("%f %f", retNode->getContentSize().width, retNode->getContentSize().height)
    return retNode;
}
//-------------------------------------------
void FBScoreMenu::Menu_What( CCObject* pSender )
{
}
//-------------------------------------------
void FBScoreMenu::CleanUp()
{
    stopAllActions();
    if ( _loginToFbNode )
    {
        _loginToFbNode->removeFromParentAndCleanup( true );
    }
}


//-------------------------------------------
// FB User Score
//--------------------------------------------
FBUserScore::FBUserScore()
{
}
//------------------------------------------------------------------------
FBUserScore::FBUserScore( int score, const char *username)
{
    _score = score;
    _username.append( username );
}



//--------------------------------------------
// FB User List
//--------------------------------------------
FBScoreList::~FBScoreList()
{
    clear();
}
//------------------------------------------------------------------------
void FBScoreList::Add( int score, const char *username, const char *userid )
{
    string fbUserId;
    fbUserId.append( userid );
    operator[]( fbUserId ) = FBUserScore( score, username );
}
//------------------------------------------------------------------------
void FBScoreList::Dump()
{
    for ( iterator it = begin(); it != end(); it++ )
    {
        FBUserScore sb = it->second;
        D_LOG("username: %s, userId: %s, score: %i", sb._username.c_str(), it->first.c_str(), sb._score );
    }
}
//------------------------------------------------------------------------
string FBScoreList::GetUsersString()
{
    stringstream users;
    bool init;
    init = true;

    for ( const_iterator it = begin(); it != end(); it++ )
    {
        if ( init )
            init = false;
        else
            users << ":";
        
        users << it->first.c_str();
    }
    return users.str();
}
//------------------------------------------------------------------------
void FBScoreList::FillUserMap( String2StringMap& map )
{
    for ( const_iterator it = begin(); it != end(); it++ )
    {
        map[it->first] = it->second._username;
    }
}
//------------------------------------------------------------------------
void FBScoreList::RemoveMyself()
{
    iterator it;
    it = find( FBTool::Get()->GetFBUserId() );
    
    if ( it != end())
    {
        erase( it );
    }
}
//------------------------------------------------------------------------





//--------------------------------------------
// FB Friends List
//--------------------------------------------
FBFriendsMap::~FBFriendsMap()
{
    clear();
}
//------------------------------------------------------------------------
void FBFriendsMap::Add( const char *userFbId, const char *username )
{
    string fbUserId;
    fbUserId.append( userFbId );
    operator[]( fbUserId ) = string( username );
}
//------------------------------------------------------------------------
void FBFriendsMap::Dump()
{
    for ( iterator it = begin(); it != end(); it++ )
    {
        D_LOG("fbId: %s, username: %s", it->first.c_str(), it->second.c_str() );
    }
}
//------------------------------------------------------------------------
void FBFriendsMap::RemoveEnabledFriends( const FBScoreList &scoreList )
{
    iterator item;
    for ( FBScoreList::const_iterator it = scoreList.begin(); it != scoreList.end(); it++ )
    {
        item = find( it->first.c_str() );
        if ( item != end() )
        {
            erase( item );
        }
    }
}
//------------------------------------------------------------------------
FBFriendsMap FBFriendsMap::GetRandom(int count )
{
    if ( size() <= (count+1) )
        return *this;
    
    int rand;
    iterator it;
    FBFriendsMap retMap;
    FBFriendsMap srcMap;
    
    srcMap = *this;
    for ( int i = 0; i < count; i++ )
    {
        it = srcMap.begin();
        rand = Tools::Rand(0, srcMap.size() - 1 );
        for ( int j = 0; j < rand; j++ )
        {
            it++;
        }
        
        retMap[ it->first ] = it->second;
        srcMap.erase( it );
    }
    
    return retMap;
}
//------------------------------------------------------------------------
string FBFriendsMap::GetUsersString()
{
    stringstream users;
    bool init;
    init = true;

    for ( const_iterator it = begin(); it != end(); it++ )
    {
        if ( init )
            init = false;
        else
            users << ":";
        
        users << it->first.c_str();
    }
    return users.str();
}
//------------------------------------------------------------------------




//------------------------------------------------------------------------
// FB Score Menu Record
//------------------------------------------------------------------------
FBScoreMenuRecord::FBScoreMenuRecord( const char *fbId, const char *username, int time )
{
    _fbId.append( fbId );
    _username.append( username );
    _time = time;
    _score = ScoreTool::Get()->GetCoinCount( time );
}
//------------------------------------------------------------------------
FBScoreMenuRecord::FBScoreMenuRecord()
{
    _fbId       = string("");
    _username   = string("");
    _time       = 0;
    _score      = 0;
}
//------------------------------------------------------------------------
FBScoreMenuRecord::~FBScoreMenuRecord()
{
}
//------------------------------------------------------------------------
bool FBScoreMenuRecord::operator< ( const FBScoreMenuRecord& other ) const
{
    return ( this->_time <= other._time );
}
//------------------------------------------------------------------------
void FBScoreMenuRecord::Dump() const
{
    D_LOG("fbId:%s\tuser:%s\ttime:%i\tscore:%i", _fbId.c_str(), _username.c_str(), _time, _score );
}
//------------------------------------------------------------------------

