#ifndef __piggy_biggy__FBAsk4HelpMenu__
#define __piggy_biggy__FBAsk4HelpMenu__
//------------------------------------------------------------------------------
#include "GameTypes.h"
#include "FBMenuBase.h"
#include "Facebook/FBTool.h"
#include "Platform/WWWTool.h"
//------------------------------------------------------------------------------
using namespace GameTypes;
//------------------------------------------------------------------------------
class FBAsk4HelpMenu : public FBMenuBase, public FBToolListener, public WWWToolListener
{
public :
   static FBAsk4HelpMenu* Create( LevelEnum level );
    ~FBAsk4HelpMenu();

    virtual void Show();
    virtual void Hide();
    
    virtual void FBRequestCompleted( FBTool::Request requestType, void *data );
    virtual void FBRequestError( int error );

    virtual void Http_FailedWithError( void *connection, int error );
    virtual void Http_FinishedLoading( void *connection, const void *data, int len );

    void Menu_What(CCObject *sender);
    
private :
    FBAsk4HelpMenu( LevelEnum level );
    void Init();
    
    CCNode* GetProfileNode( const char *username, const char *fbUserId );
    CCMenuItem* GetMenuItem( const char *fbUserId, int count, float padding );
    
private :
    typedef enum
    {
        eMode_Idle = 0,
        eMode_HTTPGetRequested,
        eMode_FBGetRequested
    } NetworkMode;

    NetworkMode         _networkMode;
    String2StringMap    _fbUsersMap;

};
//------------------------------------------------------------------------------
#endif

