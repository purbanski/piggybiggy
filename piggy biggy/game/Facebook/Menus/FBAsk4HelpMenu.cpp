#include "FBAsk4HelpMenu.h"
#include <string>
#include "Debug/MyDebug.h"
#include "GameConfig.h"
#include "Components/TrippleSprite.h"
#include "Components/FXMenuItem.h"
#include "Facebook/FBTool.h"
#include "Platform/ToolBox.h"
#include "Platform/ScoreTool.h"
#include "Platform/WWWTool.h"
#include "Components/CustomSprites.h"
#include "Tools.h"
#include <sstream>
#include "Game.h"
#include "Platform/Lang.h"

using namespace std;

//-------------------------------------------
// FB Ask 4 Help Menu
//--------------------------------------------
FBAsk4HelpMenu* FBAsk4HelpMenu::Create( LevelEnum level )
{
    FBAsk4HelpMenu *node;
    node = new FBAsk4HelpMenu( level );
    
    if ( node )
    {
        node->autorelease();
        node->Init();
        return node;
    }
    return NULL;
}
//------------------------------------------------------------------------
FBAsk4HelpMenu::FBAsk4HelpMenu( LevelEnum level )
{
    _levelEnum = level;
    _networkMode = eMode_Idle;
}
//------------------------------------------------------------------------
FBAsk4HelpMenu::~FBAsk4HelpMenu()
{
    switch ( _networkMode)
    {
        case  eMode_FBGetRequested :
            FBTool::Get()->CancelRequestForListener( this );
            break;
            
        case eMode_HTTPGetRequested :
            WWWTool::Get()->CancelHttpGet( this );
            break;
        default:
            break;
    }
}
//------------------------------------------------------------------------
void FBAsk4HelpMenu::Init()
{
    FBMenuBase::Init(CCSize( 100, 640.0f ));
    _slidingNode->SetClickableArea( CCPoint( 130.0f, 0.0f), CCPoint( 418.0f, 640.0f));
    AddBackground();
}
//------------------------------------------------------------------------
void FBAsk4HelpMenu::Show()
{
    if ( _processing )
        return;
    
    FBMenuBase::Show();
   
    if ( FBTool::Get()->IsLogged() )
    {
        FBTool::Get()->ReadScore( this );
        _networkMode = eMode_FBGetRequested;
    }
}
//------------------------------------------------------------------------
void FBAsk4HelpMenu::FBRequestCompleted( FBTool::Request requestType, void *data )
{
    FBScoreList *scoreList;
    string users;
    scoreList = (FBScoreList*)data;
    
    users = scoreList->GetUsersString();
    
    _fbUsersMap.clear();
    scoreList->FillUserMap( _fbUsersMap );
    
    stringstream ssUrl;
    ssUrl << Config::FBScoreURL;
    ssUrl << "getFriendsCompletedLevel.php?";
    ssUrl << "levelEnum=" << _levelEnum << "&";
    ssUrl << "ids=" << users;
    
    D_STRING(ssUrl.str());
    WWWTool::Get()->HttpGet( ssUrl.str().c_str(), this );
    _networkMode = eMode_HTTPGetRequested;
}
//------------------------------------------------------------------------
void FBAsk4HelpMenu::Http_FinishedLoading( void *connection, const void *data, int len )
{
    if ( _loadingAnim )
    {
        _loadingAnim->Stop();
        _loadingAnim = NULL;
    }
    
    //---------------
    // Prepare data
    string temp;
    string strData;
    
    temp.append( (const char*)data );
    strData = string( temp, 0, len );
    
    typedef vector<string> Vec;
    Vec users;
    Tools::StringSplit( strData.c_str(), ':', users );

    //---------------
    // Create menu
    CCMenu *menu;
    menu = CCMenu::menuWithItems( NULL, NULL );
    
    //---------------
    // Get items
    CCNode *profileNode;
    CCMenuItem *menuItem;
    
    int count;
    float padding;
    
    count = 0;
    padding = 135.0f;
    
    for ( Vec::iterator it = users.begin(); it != users.end(); it++ )
    {
        if ( ! it->compare( FBTool::Get()->GetFBUserId() ))
            continue;
        
        D_LOG("%s ", it->c_str() );
        profileNode = GetProfileNode( _fbUsersMap[ it->c_str() ].c_str(), it->c_str() );
        menuItem = GetMenuItem( it->c_str(), count, padding );
        
        _slidingNode->AddToContent( profileNode );
        menu->addChild( menuItem, 10 );

        count++;
    }
    
    CCPoint startingPos;
    startingPos = CCPoint( 0.0f, 0.0f );
    
    _slidingNode->GetContentNode()->AlignVertical( padding );
    _slidingNode->GetContentNode()->setPosition( CCPoint( 0.0f, 0.0f ));
    _slidingNode->GetContentNode()->setContentSize( CCSize( 100.0f, count * padding ));
   
	_slidingNode->SetInitPosition( startingPos );
   
//    if ( count > 4 )
//    {}
//        //        _slidingNode->ShowItem( myIndex );
//    else
//    {
        _slidingNode->setPosition( CCPoint( 0.0f, _slidingNode->GetContentNode()->getContentSize().height / 4.0f ));
        _slidingNode->setIsTouchEnabled( false );
//    }

    //    if ( ! FBTool::Get()->IsLogged() )
//    runAction( CCSequence::actions(
//                                   CCDelayTime::actionWithDuration( 4.0f ),
//                                   CCCallFunc::actionWithTarget( this, callfunc_selector( FBScoreMenu::ShowArrow )),
//                                   NULL ));

    
//    //---------------
//    // Finish setting up
//    CCPoint startingPos;
//    startingPos = CCPoint( 0.0f, 0.0f );
////    startingPos.x -= _background->getContentSize().width / 2.0f - 40.0f;
////    startingPos.x =
////    _contentNode->SetSingleComponentSize( CCSize( 0.0f, _sItemHeight ));
//    _slidingNode->GetContentNode()->setPosition( startingPos );
//   
////    _slidingNode->SetContentSize( 0.0f, 10 * ( padding ));
////    _slidingNode->SetSingleElementSize( CCSize( 0.0f, padding ));
//	_slidingNode->SetInitPosition( startingPos );
//    _slidingNode->ShowItem( 2 );
//    _slidingNode->setIsTouchEnabled( true );
//
//    //----------
//    //
//    _slidingNode->GetContentNode()->AlignVertical( padding );
    _slidingNode->GetContentNode()->addChild( menu );
    
//    menu->alignItemsVerticallyWithPadding( 0.0f );
    menu->setPosition( CCPoint( 210.0f, 55.0f ));
    
    
//    _background->setPosition( CCPoint( 0.0f, 0.0f ));
    // addChild( _contentNode ,10 );
}
//------------------------------------------------------------------------
void FBAsk4HelpMenu::Http_FailedWithError( void *connection, int error )
{
    _networkMode = eMode_Idle;
    _processing = false;

    if ( _loadingAnim )
    {
        _loadingAnim->Stop();
        _loadingAnim = NULL;
    }
}
//------------------------------------------------------------------------
void FBAsk4HelpMenu::FBRequestError( int error )
{
    _networkMode = eMode_Idle;
    _processing = false;
    
    if ( _loadingAnim )
    {
        _loadingAnim->Stop();
        _loadingAnim = NULL;
    }
}
//------------------------------------------------------------------------
CCNode* FBAsk4HelpMenu::GetProfileNode( const char *username, const char *fbUserId )
{
    CCSprite *profileSprite;
    FBUserFirstNameNode *labelUsername;
    TrippleSprite triSprite;
    
    string bgSpriteURL;
    string yourUserFbId;
    stringstream ss;

    float xbase;
    float ybase;
    
    xbase = 8.0f;
    ybase = 0.0f;
    profileSprite = FBProfileSprite::Create( fbUserId );
    
    labelUsername = FBUserFirstNameNode::Create( fbUserId, CCSize( 300.0f, 30.0f ), CCTextAlignmentLeft );
    labelUsername->SetColor( Config::FBMenuItemColor );
    labelUsername->setPosition( CCPoint ( xbase + 180.0f, ybase - 0.0f ));

    bgSpriteURL.append( "Images/Other/none.png" );
    triSprite._normal	= CCSprite::spriteWithFile( bgSpriteURL.c_str() );

    profileSprite->setPosition( CCPoint( xbase + 70.0f, ybase + 55.0f ));

    
    CCSprite *retNode;
    retNode = CCSprite::spriteWithFile("Images/Other/none.png");
    retNode->addChild( profileSprite, 10 );
    retNode->addChild( labelUsername, 10 );
    
    return retNode;
}
//------------------------------------------------------------------------
CCMenuItem* FBAsk4HelpMenu::GetMenuItem( const char *fbUserId, int count, float padding )
{
    CCSprite *profileSprite;
    TrippleSprite triSprite;
    
    string bgSpriteURL;
    string yourUserFbId;
    stringstream ss;

    float xbase;
    float ybase;
    
    xbase = 8.0f;
    ybase = 0.0f;
    profileSprite = FBProfileSprite::Create( fbUserId );
    
 
    bgSpriteURL.append( "Images/Other/none.png" );
    triSprite._normal	= CCSprite::spriteWithFile( "Images/Facebook/Ask4Help/helpMeBt.png" );
    triSprite._selected	= CCSprite::spriteWithFile( "Images/Facebook/Ask4Help/helpMeBt-selected.png" );
    triSprite._disable	= CCSprite::spriteWithFile( "Images/Facebook/Ask4Help/helpMeBt.png" );

    profileSprite->setPosition( CCPoint( xbase + 70.0f, ybase + 55.0f ));

    FXMenuItemSpriteWithAnim *item;
    item = FXMenuItemSpriteWithAnim::Create( &triSprite, this, menu_selector( FBAsk4HelpMenu::Menu_What ));

    String2StringMap::iterator it;
    it = _fbUsersMap.find( fbUserId );
    
    item->setUserData( (void*) &(it->first) );
    item->setPosition( CCPoint( 0.0f, -(float)count * padding ));
    return item;
}
//------------------------------------------------------------------------
void FBAsk4HelpMenu::Menu_What( CCObject *object )
{
    string *fbId;
    CCMenuItem *item;
    
    item = (CCMenuItem*) object;
    fbId = (string*) item->getUserData();
    
    D_CSTRING(fbId->c_str());
    FBTool::Get()->Request_LevelHelp( fbId->c_str(), Game::Get()->GetGameStatus()->GetCurrentLevel() );
}
//------------------------------------------------------------------------
void FBAsk4HelpMenu::Hide()
{
    FBMenuBase::Hide();
    if ( _background )
    {
        _background->removeFromParentAndCleanup(true);
        _background = NULL;
    }
}