#ifndef __FBSCOREMENU_H__
#define __FBSCOREMENU_H__
//---------------------------------------------------------------------------------
#include <map>
#include <string>
#include <cocos2d.h>
#include "GameTypes.h"
#include "Platform/ScoreToolListener.h"
#include "Platform/WWWTool.h"
#include "pu/ui/menus/SlidingMenu.h"
#include "pu/ui/SlidingNode2.h"
#include "Components/CustomNodes.h"
#include "FBMenuBase.h"

using namespace std;
using namespace GameTypes;

USING_NS_CC;

//---------------------------------------------------------------------------------
// FB User Score
// Single record of: user score, user iD
//---------------------------------------------------------------------------------
// Is used to get score records from Facebook
class FBUserScore
{
public:
    FBUserScore();
    FBUserScore( int score, const char *userId );
    
    int _score;
    string _username;
};


//---------------------------------------------------------------------------------
// FB Score list
//---------------------------------------------------------------------------------
// Is used to get score records from Facebook
typedef map<string /* fb user id */, FBUserScore> FBUserScoreMap;
//---------------------------------------------------------------------------------
class FBScoreList : public FBUserScoreMap
{
public:
    ~FBScoreList();
    void Add( int score, const char *username, const char *userid );
    void Dump();
    string GetUsersString();
    void RemoveMyself();
    void FillUserMap( String2StringMap& map );
};


//---------------------------------------------------------------------------------
// FB Friends List
//---------------------------------------------------------------------------------
// Is used to get friends list from facebook
class FBFriendsMap : public std::map<string /* fb user id */, string /*fb user name */>
{
public:
    ~FBFriendsMap();
    void Add( const char *fbId, const char *username );
    void Dump();
    void RemoveEnabledFriends( const FBScoreList &scoreList );
    FBFriendsMap GetRandom( int count );
    
    string GetUsersString();
};


//---------------------------------------------------------------------------------
// FB Score Menu Record
//---------------------------------------------------------------------------------
struct FBScoreMenuRecord
{
public:
    FBScoreMenuRecord();
    FBScoreMenuRecord( const char *fbId, const char *username, int time );
    ~FBScoreMenuRecord();

    void Dump() const;
    bool operator< ( const FBScoreMenuRecord &other) const;

public:
    string      _fbId;
    string      _username;
    int         _time;
    int         _score;
};

//---------------------------------------------------------------------------------
// FB Sorted by score friends highscores
//---------------------------------------------------------------------------------
// Is used to display data in FBMenu
class FBFriendsScoresSortedSet : public multiset<FBScoreMenuRecord>
{
public:
    void Dump();
};


//---------------------------------------------------------------------------------
// FB Score Menu
//---------------------------------------------------------------------------------
class LoadingAnim;
class FBScoreMenu : public FBMenuBase, public ScoreToolListener
{
public:
    static FBScoreMenu* Create( LevelEnum level );
    ~FBScoreMenu();

    virtual void Show();
    virtual void Hide();
    
    virtual void ScoreTool_ScoreMapLoaded();
    virtual void ScoreTool_RequestError();

    void Menu_What(CCObject *sender);
    
private:
    FBScoreMenu( LevelEnum level );
    void Init();
    CCNode* GetMenuItem( const char *userFbId, int time, int score, int count );
    void AddYourScore( FBFriendsScoresSortedSet *scoreMap );
    void ShowArrow();
    void CleanUp();

private:
    CCNode  *_loginToFbNode;

};


#endif

