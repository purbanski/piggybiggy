#ifndef __piggy_biggy__FBMenuBase__
#define __piggy_biggy__FBMenuBase__
//-------------------------------------------------------------------
#include <cocos2d.h>
#include "GameTypes.h"
#include "Components/CustomNodes.h"
#include "pu/ui/SlidingNode2.h"
#include "Components/CustomSprites.h"

//-------------------------------------------------------------------
USING_NS_CC;
using namespace GameTypes;
//-------------------------------------------------------------------
class FBMenuBase : public CCNode
{
public:
    ~FBMenuBase();
    virtual void Show();
    virtual void Hide();
    
protected:
    FBMenuBase();
    virtual void Init( const CCSize &windowSize );
    virtual void SetupFades();
    virtual void SetupLabel( const char *text, unsigned int fontSize = 42 );
    void AddBackground();
    void ShowNoInternetSprite();
    void RemoveMe( CCNode *node, void *data );
    void EnableSliding();
    void DisableSliding();
    
protected:
    bool            _processing;
    bool            _internetAccess;
    
    LevelEnum       _levelEnum;
    SlidingNode2    *_slidingNode;
    
    CCSprite        *_topFade;
    CCSprite        *_bottomFade;
    CCSprite        *_highscoreLabel;
    LoadingAnim     *_loadingAnim;
    FadeInSprite    *_noInternetSprite;
    CCLabelTTF      *_menuLabel;
    CCSprite        *_background;
    
protected:
    
};
//-------------------------------------------------------------------
#endif
