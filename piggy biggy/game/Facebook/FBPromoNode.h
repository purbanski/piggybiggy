#ifndef __PU_FBPROMONODE_H__
#define __PU_FBPROMONODE_H__

#include <cocos2d.h>
#include "GameTypes.h"
#include "Menus/FBScoreMenu.h"
#include "Menus/FBChallangeMenu.h"
#include "Facebook/FBTool.h"
#include <map>

using namespace GameTypes;
using namespace std;
USING_NS_CC;

typedef enum
{
    ePromoNodeCmd_Hidden = 0,
    ePromoNodeCmd_ViewHighscore,
    ePromoNodeCmd_ViewChallange,
    ePromoNodeCmd_ViewStats,
    ePromoNodeCmd_FacebookLogin
} FBPromoNodeViewType;


//---------------------------------------------------------------------------------
// FB Promo Menu Button Items
//---------------------------------------------------------------------------------
struct FBPromoMenuItemButtons
{
    FBPromoMenuItemButtons();
    FBPromoMenuItemButtons( FBPromoNodeViewType mode,
                           const CCPoint &pos,
                           const char *buttonName,
                           CCObject *reciever,
                           SEL_MenuHandler fn );
    
    void SetOpenButton();
    void SetClosedButton();
    void SetDisabledButton();
    void SetPosition( const CCPoint &point );
    void Disable();
    
    CCMenuItemSprite *_buttonOpen;
    CCMenuItemSprite *_buttonClosed;
    CCMenuItemSprite *_buttonDisabled;
};

//---------------------------------------------------------------------------------
// FB Promo Menu Button Items Map
//---------------------------------------------------------------------------------
class FBPromoMenuItemButtonMap : public map<FBPromoNodeViewType, FBPromoMenuItemButtons>
{
};

//---------------------------------------------------------------------------------
// FB Promo Node Menu
//---------------------------------------------------------------------------------
class FBPromoNode;
class FBPromoNodeMenu : public CCNode, public FBToolListener
{
public:
     static FBPromoNodeMenu* Create( FBPromoNode *promoNode );
    ~FBPromoNodeMenu();
    
    void MyInit();
    void Show();
    void Hide();
    void HideAllButtons();
    
    virtual void FBRequestCompleted( FBTool::Request requestType, void *data );
    virtual void FBRequestError( int error );

    void Menu_ButtonPressed( CCObject *object );
    void Menu_ShowStats( CCObject *object );
    void Menu_FacebookLogin( CCObject *object );
    
private:
    FBPromoNodeMenu( FBPromoNode* promoNode );
    void UpdateButtons();
    void Shown();
    
private:
    CCSprite    *_background;
    CCMenu      *_menu;
    FBPromoNode *_promoNode; // parent
    //  CCParticleSystem *_emitter;
    FBPromoMenuItemButtonMap _buttonMap;
};

//---------------------------------------------------------------------------------
// FB Promo Node
//---------------------------------------------------------------------------------
class FBPromoNode : public CCNode
{
public:
    static FBPromoNode* Create( LevelEnum level );
    ~FBPromoNode();

    void Init();
    void Hide();
    void Show();
    void MenuShown();
    
    FBPromoNodeViewType GetViewMode();
    void SetViewMode( FBPromoNodeViewType mode, bool force = false );
    
private:
    FBPromoNode( LevelEnum level );
    void UpdateContent();

private:
    LevelEnum           _level;
    FBPromoNodeViewType _viewMode;
    
    FBPromoNodeMenu *_menu;
    FBScoreMenu     *_highscoreMenu;
    FBChallangeMenu *_challangeMenu;
    
    friend class FBPromoNodeMenu;
};

#endif
