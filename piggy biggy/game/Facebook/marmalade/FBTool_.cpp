#include <sstream>
#include "Facebook/FBTool.h"
#include "Platform/ToolBox.h"
#include "Debug/MyDebug.h"
#include "GameConfig.h"
#include "Platform/ScoreTool.h"
#include "Game.h"
#include "Platform/Lang.h"
#include "GameGlobals.h"
#include "RunLogger/RunLogger.h"

//------------------------------------------------------------------------
FBTool * FBTool::_sInstance = NULL;
//------------------------------------------------------------------------
FBTool* FBTool::Get()
{
	if ( ! _sInstance )
	{
		_sInstance= new FBTool();

	}
	return _sInstance;
}
//------------------------------------------------------------------------
void FBTool::Destroy()
{
	if ( _sInstance	)
		delete _sInstance;

	_sInstance = NULL;
}
//------------------------------------------------------------------------
FBTool::FBTool()
{
	//[ FBTool_iOS sharedInstance ];
}
//------------------------------------------------------------------------
FBTool::~FBTool()
{
	//[ FBTool_iOS destroy ];
}
//------------------------------------------------------------------------
void FBTool::StatusUpdate( const char *msg )
{
	//[[ FBTool_iOS sharedInstance ] statusUpdate:msg ];
}
//------------------------------------------------------------------------
void FBTool::ScreenShotUpload( const char *msg )
{
	/*UIImage *image;
	image = [ ToolBox_iOS getScreenShot ];

	[[ FBTool_iOS sharedInstance ] screenshotUpload:image msg:msg ];*/
}
//------------------------------------------------------------------------
void FBTool::PostCustomPost( const char *link,
	const char *imageURL,
	const char *name,
	const char *caption,
	const char *description )
{
//	[[ FBTool_iOS sharedInstance ] postCustomPost:link
//imageURL:imageURL
//name:name
//caption:caption
//description:description ];
}
//------------------------------------------------------------------------
void FBTool::PostCustomPost(FBPost &post)
{
	//PostCustomPost( post.GetLink().c_str(),
	//	post.GetImageURL().c_str(),
	//	post.GetName().c_str(),
	//	post.GetCaption().c_str(),
	//	post.GetDescription().c_str() );
}
//------------------------------------------------------------------------
void FBTool::Post_LevelCompleted( LevelEnum level, const char *imgURL )
{
	//stringstream timeString;
	//stringstream coinsString;

	//stringstream url;
	//stringstream iRequestUrl;
	//stringstream iLevel;
	//stringstream iTimeScore;
	//stringstream iCoinsCount;
	//stringstream iImgUrl;
	//stringstream iUsername;

	////--------------
	//// Getting data
	//timeString  << LocalString("Time")  << ": " << Game::Get()->GetGameStatus()->GetLevelTimeString( level );
	//coinsString << LocalString("Coins") << ": " << Game::Get()->GetGameStatus()->GetCoinCount( level );

	////---------------
	//// Preparing for url
	//iRequestUrl << Config::FBOpenGraphURL << "levelComplete.php";
	//iLevel      << level;
	//iTimeScore  << ToolBox::UTF8Encode( timeString.str().c_str() );
	//iCoinsCount << ToolBox::UTF8Encode( coinsString.str().c_str() );
	//iImgUrl     << ToolBox::UTF8Encode( imgURL );
	//iUsername   << ToolBox::UTF8Encode( [[[FBTool_iOS sharedInstance] fbFirstname] UTF8String] );

	////----------------
	//// Getting url
	//url << iRequestUrl.str() << "?";
	//url << "level=" << iLevel.str() << "&";
	//url << "img_url=" << iImgUrl.str() << "&";
	//url << "username=" << iUsername.str() << "&";
	//url << "time=" << iTimeScore.str() << "&";
	//url << "coins=" << iCoinsCount.str();

	//D_LOG("url============\n%s", url.str().c_str());

	//[[ FBTool_iOS sharedInstance ] postActivity:"complete" url:url.str().c_str()];
}
//------------------------------------------------------------------------
void FBTool::Post_LevelSkipped( LevelEnum level, const char *imgURL )
{
	//stringstream url;
	//stringstream iRequestUrl;
	//stringstream iLevel;
	//stringstream iImgUrl;
	//stringstream iUsername;

	////---------------
	//// Preparing for url
	//iRequestUrl << Config::FBOpenGraphURL << "levelSkip.php";
	//iLevel      << level;
	//iImgUrl     << ToolBox::UTF8Encode( imgURL );
	//iUsername   << ToolBox::UTF8Encode( [[[FBTool_iOS sharedInstance] fbFirstname] UTF8String] );

	////----------------
	//// Getting url
	//url << iRequestUrl.str() << "?";
	//url << "level=" << iLevel.str() << "&";
	//url << "img_url=" << iImgUrl.str() << "&";
	//url << "username=" << iUsername.str() ;

	//D_LOG("url============\n%s", url.str().c_str());

	//[[ FBTool_iOS sharedInstance ] postActivity:"skip" url:url.str().c_str()];
}
//------------------------------------------------------------------------
void FBTool::Request_LevelHelp( const char *fbId, LevelEnum level )
{
//	stringstream message;
//	stringstream iMessage;
//	stringstream iTitle;
//
//	message << GetUserFirstName() << " " << LocalString("GotStuck") ;
//	message << " \"" << GameGlobals::Get()->LevelNames()[ level ] << "\"";
//	message << "\n" << LocalString("GetSolution");
//
//	iMessage << ToolBox::UTF8Encode( message.str().c_str() );
//	iTitle << ToolBox::UTF8Encode( LocalString("Help"));
//
//	[[FBTool_iOS sharedInstance] sendRequest:fbId
//title:iTitle.str().c_str()
//message:message.str().c_str()
//data:NULL ];
}
//------------------------------------------------------------------------
void FBTool::Request_LevelChallange( const char *fbId, LevelEnum level, unsigned int seconds )
{
//	stringstream message;
//	stringstream iMessage;
//	stringstream iTitle;
//	stringstream iData;
//
//	string levelName;
//	int levelIndex;
//
//	levelIndex = Game::Get()->GetGameStatus()->GetLevelIndex( level ) + 1;
//	levelName.append( GameGlobals::Get()->LevelNames()[level] );
//
//	std::replace( levelName.begin(), levelName.end(), '\n', ' ');
//
//	message << EnglishString("ChallangeStr1") << " ";
//	message << "Level: " << levelIndex;
//
//	iMessage << ToolBox::UTF8Encode( message.str().c_str() );
//	iTitle << ToolBox::UTF8Encode( EnglishString("Challenge"));
//
//	iData << level << ":";
//	iData << seconds;
//
//	NSLog(@"%s" ,iData.str().c_str() );;
//	NSLog(@"%s", iMessage.str().c_str());
//
//	[[FBTool_iOS sharedInstance] sendRequest:fbId
//title:iTitle.str().c_str()
//message:message.str().c_str()
//data:iData.str().c_str() ];
}
//------------------------------------------------------------------------
void FBTool::Request_SetNotify( const char *fbId )
{
	//stringstream url;
	//url << Config::FBRequestsURL << "setRequestNotify.php?";
	//url << "fbId=" << fbId;

	//D_LOG("url============\n%s", url.str().c_str() );
	//WWWTool::Get()->HttpGet( url.str().c_str() );
}
//------------------------------------------------------------------------
void FBTool::Request_DeleteNotify( const char *fbId )
{
	//stringstream url;
	//url << Config::FBRequestsURL << "deleteRequestNotify.php?";
	//url << "fbId=" << fbId;

	//D_LOG("url============\n%s", url.str().c_str() );
	//WWWTool::Get()->HttpGet( url.str().c_str() );
}
//------------------------------------------------------------------------
void FBTool::Request_DeleteRequest( const char *requestId, FBToolListener *listener )
{
	//[[FBTool_iOS sharedInstance] deleteRequest:requestId listener:listener];
}
//------------------------------------------------------------------------
void FBTool::PostUserAchievement()
{
	//[[FBTool_iOS sharedInstance] postUserAchievement ];
}
//------------------------------------------------------------------------
void FBTool::Login( FBToolListener* listener )
{
	//if ( IsLogged() )
	//{
	//	if ( listener )
	//		listener->FBRequestCompleted( eReq_Login, NULL );
	//}
	//else
	//{
	//	RLOG_S("FACEBOOK_LOGIN","LOGIN_REQUEST");
	//	[[ FBTool_iOS sharedInstance ] login:listener ];
	//}
}
//------------------------------------------------------------------------
void FBTool::Logout()
{
	//RLOG_S("FACEBOOK_LOGIN","LOGOUT");
	//[[ FBTool_iOS sharedInstance ] logout ];
}
//------------------------------------------------------------------------
bool FBTool::IsLogged()
{  
	return false;
	//return [[ FBTool_iOS sharedInstance ] isLogged ];
}
//------------------------------------------------------------------------
string FBTool::GetUsername()
{
	return string();

	//string username;
	//username = [[FBTool_iOS sharedInstance ] getUsername ];
	//D_STRING( username );
	//return username;
}
//------------------------------------------------------------------------
string FBTool::GetUserFirstName()
{
	return string();
	//string username;
	//username = [[FBTool_iOS sharedInstance ] getUserFirstName ];
	//D_STRING( username );
	//return username;
}
//------------------------------------------------------------------------
string FBTool::GetFBUserId()
{
	return string();
	//string ret;
	//ret = [[FBTool_iOS sharedInstance ] getFbId ];
	//D_STRING( ret );
	//return ret;
}
//------------------------------------------------------------------------
void FBTool::PostScore(int score)
{
	//[[ FBTool_iOS sharedInstance] postScore:score];
}
//------------------------------------------------------------------------
void FBTool::ReadScore( FBToolListener *listener )
{
	//[[FBTool_iOS sharedInstance] readScore: listener ];
}
//------------------------------------------------------------------------
void FBTool::Read_FriendsList( FBToolListener *listener )
{
	//[[FBTool_iOS sharedInstance] readFriendsList: listener];
}
//------------------------------------------------------------------------
void FBTool::Read_UserFirstName( const char *fbId, FBToolListener *listener )
{
	//[[FBTool_iOS sharedInstance] readUserFirstName:fbId listener:listener ];
}
//------------------------------------------------------------------------
void FBTool::CancelRequestForListener( FBToolListener *listener )
{
	//[[FBTool_iOS sharedInstance] cancelRequestForListener:listener];
}
//------------------------------------------------------------------------
void FBTool::Request_ReadAll( FBToolListener *listener )
{
	//[[FBTool_iOS sharedInstance] readAllRequests: listener ];
}
//------------------------------------------------------------------------

void FBTool::Post_test()
{

	//    
	//    
	//       // Put together the dialog parameters
	//    NSMutableDictionary *params =
	//    [NSMutableDictionary dictionaryWithObjectsAndKeys:
	//     @"Facebook SDK for iOS", @"name",
	//     @"Build great social apps and get more installs.", @"caption",
	//     @"The Facebook SDK for iOS makes it easier and faster to develop Facebook integrated iOS apps.", @"description",
	//     @"https://developers.facebook.com/ios", @"link",
	//     @"https://raw.github.com/fbsamples/ios-3.x-howtos/master/Images/iossdk_logo.png", @"picture",
	//     nil];
	//
	//    // Invoke the dialog
	//    [FBWebDialogs presentFeedDialogModallyWithSession:nil
	//                                           parameters:params
	//                                              handler:
	//     ^(FBWebDialogResult result, NSURL *resultURL, NSError *error) {
	//         if (error) {
	//             // Case A: Error launching the dialog or publishing story.
	//             NSLog(@"Error publishing story.");
	//         } else {
	//             if (result == FBWebDialogResultDialogNotCompleted) {
	//                 // Case B: User clicked the "x" icon
	//                 NSLog(@"User canceled story publishing.");
	//             } else {
	//                 // Case C: Dialog shown and the user clicks Cancel or Share
	////                 NSDictionary *urlParams = [self parseURLParams:[resultURL query]];
	////                 if (![urlParams valueForKey:@"post_id"]) {
	////                     // User clicked the Cancel button
	////                     NSLog(@"User canceled story publishing.");
	////                 } else {
	////                     // User clicked the Share button
	////                     NSString *postID = [urlParams valueForKey:@"post_id"];
	////                     NSLog(@"Posted story, id: %@", postID);
	////                 }
	//             }
	//         }
	//    }];
	//    
	//    return;

//	[FBWebDialogs
//presentRequestsDialogModallyWithSession:nil
//message:@"Learn how to make your iOS apps social."
//title:nil
//parameters:nil
//handler:^(FBWebDialogResult result, NSURL *resultURL, NSError *error)
//	{
//		if (error)
//		{
//			// Case A: Error launching the dialog or sending request.
//			NSLog(@"Error sending request.");
//		}
//		else
//		{
//			if (result == FBWebDialogResultDialogNotCompleted) {
//				// Case B: User clicked the "x" icon
//				NSLog(@"User canceled request.");
//			}
//			else
//			{
//				// Case C: Dialog shown and the user clicks Cancel or Send
//				NSDictionary *urlParams ;
//				NSArray *pairs = [ [resultURL query] componentsSeparatedByString:@"&"];
//				NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
//				for (NSString *pair in pairs) {
//					NSArray *kv = [pair componentsSeparatedByString:@"="];
//					NSString *val =
//						[[kv objectAtIndex:1]
//stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
//
//					[params setObject:val forKey:[kv objectAtIndex:0]];
//				}
//				urlParams = params;
//
//
//
//				if (![urlParams valueForKey:@"request"]) {
//					// User clicked the Cancel button
//					NSLog(@"User canceled request.");
//				} else {
//					// User clicked the Send button
//					NSString *requestID = [urlParams valueForKey:@"request"];
//					NSLog(@"Request ID: %@", requestID);
//				}
//			}
//		}
//	}];
	////    [[UIApplication sharedApplication] openURL:[NSURL URLWithString: @"https://www.facebook.com/dialog/send?app_id=123050457758183&name=People%20Argue%20Just%20to%20Win&link=http://www.nytimes.com/2011/06/15/arts/people-argue-just-to-win-scholars-assert.html&redirect_uri=https://www.bancsabadell.com/cs/Satellite/SabAtl/"]];
	////                                                 
	////     // -- check permission mission
	// 
	//    NSMutableDictionary* params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
	//                                   @"http://www.facebook.com/piggybiggy", @"link",
	//                                   @"http://www.piggybiggy.com/facebook/post_images/fbSkippedLevel.png", @"picture",
	//                                   @"name", @"name",
	//                                   @"caption", @"caption",
	//                                   @"Description", @"description",
	//                                   nil];
	//
	//    [FBRequestConnection startWithGraphPath:@"me/feed"
	//                                 parameters:params
	//                                 HTTPMethod:@"POST"
	//                          completionHandler:^(FBRequestConnection *connection, id result, NSError *error)
	//    {
	//        [ToolBox_iOS debugMsgBox:@"debug"
	//                         message:@"Test"
	//                           error:error ];
	//    }];
	//
	////    
	//
	//    https://graph.facebook.com/brent/feed?
	//  link=https://developers.facebook.com/docs/reference/dialogs/&
	//  picture=http://fbrell.com/f8.jpg&
	//  name=Facebook%20Dialogs&
	//  caption=Reference%20Documentation&
	//  description=Using%20Dialogs%20to%20interact%20with%20users.&
}