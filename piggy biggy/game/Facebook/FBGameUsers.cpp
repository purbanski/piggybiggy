#include "FBGameUsers.h"
#include "Tools.h"
#include "Debug/MyDebug.h" 
#include "GameConfig.h"
#include "Menus/FBScoreMenu.h"
#include "Components/FXMenuItem.h"
#include "Components/TrippleSprite.h"
#include "Components/CustomSprites.h"

//------------------------------------------------------------------------------
// Level User Data
//------------------------------------------------------------------------------
LevelUserData::LevelUserData()
{
    _fbId = "";
    _levelUserType = eGameUser_Unknown;
}
//------------------------------------------------------------------------------
LevelUserData::LevelUserData( const char *fbId, GameLevelUsersType userType )
{
    _fbId.append( fbId );
    _levelUserType = userType;
}
//------------------------------------------------------------------------------
CCMenuItem* LevelUserData::GetMenuItem( CCObject *target, SEL_MenuHandler fn )
{
    FXMenuItemSpriteWithAnim *menuItem;
    TrippleSprite sprite;
    
    FBProfileBigSprite *profileSprite;
    CCSprite *selectedSprite;
    
    profileSprite = FBProfileBigSprite::Create( _fbId.c_str(), _levelUserType );
    selectedSprite = CCSprite::spriteWithFile("Images/Facebook/Requests/userClick.png");
    selectedSprite->setAnchorPoint( CCPoint(0.0f, 0.0f));
    
    sprite._normal = profileSprite;
    sprite._selected = selectedSprite;
    sprite._disable = CCSprite::spriteWithFile( "Images/Other/none.png" );
    
    menuItem = FXMenuItemSpriteWithAnim::Create( &sprite, target, fn );
    menuItem->setUserData( (void *) this );
    return menuItem;
}


//------------------------------------------------------------------------------
// FB Game Users Vector
//------------------------------------------------------------------------------
FBGameUsersVector::FBGameUsersVector()
{
    
}
//------------------------------------------------------------------------------
FBGameUsersVector FBGameUsersVector::GetWithLimits( int friendsGameInstalled, int friendsGameNotInsalled, int otherGameInstalled )
{
    FBGameUsersVector ret;
    
    for ( iterator it = begin(); it != end(); it++ )
    {
        switch ( it->_levelUserType )
        {
            case eGameUser_FriendLevelNotPassed :
            case eGameUser_FriendLevelPassed :
                if ( friendsGameInstalled-- > 0 )
                    ret.push_back( *it );
                break;
                
            case eGameUser_FriendGameNotInstalled :
                if ( friendsGameNotInsalled-- > 0 )
                    ret.push_back( *it );
                break;
                
            case eGameUser_UnknownPlayerLevelNotPassed :
                if ( otherGameInstalled-- > 0 )
                    ret.push_back( *it );
                break;

            case eGameUser_Unknown :
            default:
                break;
        }
    }
    
    return ret;
}
//------------------------------------------------------------------------------
FBGameUsersVector FBGameUsersVector::Create( const FBFriendsMap& friendsMap, const char *strData )
{
    FBGameUsersVector gameUsers;
    
    typedef vector<string> Vec;
    Vec users;
    // example
    // fbId1,fbId2;fbid3,fbid4;fbid5,fbid6;fbid7,fbid8
    // friend enabld not pass; friend enabled passed level;frineds game not installed;random fb user - game installed, level not passed

    Tools::StringSplit( strData, ';', users );
    
    D_SIZE(users);
    if ( users.size() != 2 && users.size() != 3 )
    {
        return gameUsers;
    }
    
    D_STRING( users[0] );

    //----------------------
    // Add user friends - who not passed level
    if ( users[0].length() )
    {
        Vec usersNotPass;
        Tools::StringSplit( users[0].c_str(), ',', usersNotPass );
        
        for ( Vec::iterator it = usersNotPass.begin(); it != usersNotPass.end(); it++ )
            gameUsers.PushBack( it->c_str(), eGameUser_FriendLevelNotPassed );
    }

    //----------------------
    // Add user friends - who passed level
    if ( users[1].length() )
    {
        Vec usersPass;
        Tools::StringSplit( users[1].c_str(), ',', usersPass );
        
        for ( Vec::iterator it = usersPass.begin(); it != usersPass.end(); it++ )
            gameUsers.PushBack( it->c_str(), eGameUser_FriendLevelPassed );
    }

    //----------------------
    // Add user friends - who haven't played
    FBGameUsersVector tempUsers;
    for ( FBFriendsMap::const_iterator it = friendsMap.begin(); it != friendsMap.end(); it++)
    {
        if ( ! gameUsers.UserIn( it->first.c_str() ))
        {
            tempUsers.PushBack( it->first.c_str(), eGameUser_FriendGameNotInstalled );
        }
    }

    gameUsers.AddRandomValues( tempUsers, Config::FBFriendPickerFriendCount );
    
    //----------------------
    // Add random unknownd users
    if ( users.size() == 3 && users[2].length() )
    {
        Vec usersRandom;
        Tools::StringSplit( users[2].c_str(), ',', usersRandom );
        
        for ( Vec::iterator it = usersRandom.begin(); it != usersRandom.end(); it++ )
            gameUsers.PushBack( it->c_str(), eGameUser_UnknownPlayerLevelNotPassed );
    }
    
    return gameUsers;
}

//------------------------------------------------------------------------------
void FBGameUsersVector::PushBack( const char *fbId, GameLevelUsersType userType )
{
    push_back( LevelUserData(fbId, userType));
}
//------------------------------------------------------------------------------
bool FBGameUsersVector::UserIn( const char *fbId )
{
    for ( const_iterator it = begin(); it != end(); it++ )
    {
//        D_LOG("%s %s", fbId, (*it)._fbId.c_str());
        if ( ! strncmp( (*it)._fbId.c_str(), fbId, strlen( fbId )))
            return true;
    }
    return false;
}
//------------------------------------------------------------------------------
void FBGameUsersVector::Dump()
{
#ifdef DEBUG
    D_HIT
    for ( const_iterator it = begin(); it != end(); it++ )
    {
        D_LOG("%s %d", it->_fbId.c_str(), it->_levelUserType );
    }
#endif
}
//------------------------------------------------------------------------------
void FBGameUsersVector::AddRandomValues( FBGameUsersVector src, unsigned int count )
{
    D_SIZE(src)
    if ( src.size() > count )
    {
        int icount;
        int rand;
        iterator it;
        
        icount = 0;
        
        while ( icount < count )
        {
            rand = Tools::Rand( 0, src.size()-1 );
            it = src.begin();
            
            for ( int i = 0; i < rand; i++ )
                it++;
            
            if ( it != src.end() )
            {
                push_back( *it );
                src.erase( it );
                icount++;
            }
        }
    }
    else
    {
        for ( const_iterator it = src.begin(); it != src.end(); it++ )
            push_back( *it );
    }
}