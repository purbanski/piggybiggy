#include "FBPromoNode.h"
#include "Components/FXMenuItem.h"
#include <sstream>
#include "Debug/MyDebug.h"
#include "Facebook/FBTool.h"
#include "Tools.h"
#include "Promotion/FBRequestMenu.h"
#include "RunLogger/RunLogger.h"

//--------------------------------------------------------------------------------
// FB Promo Menu Item Buttons
//--------------------------------------------------------------------------------
FBPromoMenuItemButtons::FBPromoMenuItemButtons()
{
    _buttonOpen     = NULL;
    _buttonDisabled = NULL;
    _buttonClosed   = NULL;
}
//--------------------------------------------------------------------------------
FBPromoMenuItemButtons::FBPromoMenuItemButtons(
                                               FBPromoNodeViewType mode,
                                               const CCPoint &pos,
                                               const char *buttonName,
                                               CCObject *target,
                                               SEL_MenuHandler fn )
{
    stringstream fileNormal;
    stringstream fileSelected;
    stringstream fileDisabled;
    string path;
    
    TrippleSprite triSprite;
    path.append( "Images/Facebook/PromoNode/" );
    
    //----------------
    // Open Menu Item
    fileNormal.str("");
    fileSelected.str("");
    fileDisabled.str("");
    fileNormal      << path << buttonName << "-open.png";
    fileSelected    << path << buttonName << "-open.png";
    fileDisabled    << path << buttonName << "-open.png";
    triSprite = TrippleSprite( fileNormal.str().c_str(), fileSelected.str().c_str(), fileDisabled.str().c_str() );
    _buttonOpen = FXMenuItemSprite::itemFromNormalSprite( triSprite, target,   fn );
    _buttonOpen->setUserData( (void*) mode );
    
    D_STRING( fileNormal.str() );
    D_STRING( fileSelected.str() );
    D_STRING( fileDisabled.str() );

    //----------------
    // Closed Menu Item
    fileNormal.str("");
    fileSelected.str("");
    fileDisabled.str("");
    fileNormal      << path << buttonName << "-closed.png";
    fileSelected    << path << buttonName << "-closedSelected.png";
    fileDisabled    << path << buttonName << "-disabled.png";
    triSprite = TrippleSprite( fileNormal.str().c_str(), fileSelected.str().c_str(), fileDisabled.str().c_str() );
    _buttonClosed = FXMenuItemSprite::itemFromNormalSprite( triSprite, target,   fn );
    _buttonClosed->setUserData( (void*) mode );
    
    //----------------
    // Disabled Menu Item
    fileNormal.str("");
    fileSelected.str("");
    fileDisabled.str("");
    fileNormal      << path << buttonName << "-disabled.png";
    fileSelected    << path << buttonName << "-disabled.png";
    fileDisabled    << path << buttonName << "-disabled.png";
    triSprite = TrippleSprite( fileNormal.str().c_str(), fileSelected.str().c_str(), fileDisabled.str().c_str() );
    _buttonDisabled = FXMenuItemSprite::itemFromNormalSprite( triSprite, target,   fn );
    _buttonDisabled->setUserData( (void*) mode );
    
    SetPosition( pos );
};
//--------------------------------------------------------------------------------
void FBPromoMenuItemButtons::SetOpenButton()
{
    _buttonOpen->setIsVisible(true);
    _buttonOpen->setIsEnabled(false);
    
    _buttonClosed->setIsVisible(false);
    _buttonClosed->setIsEnabled(false);
    
    _buttonDisabled->setIsVisible(false);
    _buttonDisabled->setIsEnabled(false);
}
//--------------------------------------------------------------------------------
void FBPromoMenuItemButtons::SetClosedButton()
{
    _buttonOpen->setIsVisible(false);
    _buttonOpen->setIsEnabled(false);
    
    _buttonClosed->setIsVisible(true);
    _buttonClosed->setIsEnabled(true);
    
    _buttonDisabled->setIsVisible(false);
    _buttonDisabled->setIsEnabled(false);
}
//--------------------------------------------------------------------------------
void FBPromoMenuItemButtons::SetDisabledButton()
{
    _buttonOpen->setIsVisible(false);
    _buttonOpen->setIsEnabled(false);
    
    _buttonClosed->setIsVisible(false);
    _buttonClosed->setIsEnabled(false);
    
    _buttonDisabled->setIsVisible(true);
    _buttonDisabled->setIsEnabled(false);
}
//--------------------------------------------------------------------------------
void FBPromoMenuItemButtons::SetPosition( const CCPoint &var )
{
    _buttonOpen->setPosition( var );
    _buttonClosed->setPosition( var );
    _buttonDisabled->setPosition( var );
}
//--------------------------------------------------------------------------------
void FBPromoMenuItemButtons::Disable()
{
    _buttonOpen->setIsVisible(false);
    _buttonOpen->setIsEnabled(false);
    
    _buttonClosed->setIsVisible(false);
    _buttonClosed->setIsEnabled(false);
    
    _buttonDisabled->setIsVisible(false);
    _buttonDisabled->setIsEnabled(false);
}


//--------------------------------------------------------------------------------
// FB Promo Node Menu
//--------------------------------------------------------------------------------
FBPromoNodeMenu* FBPromoNodeMenu::Create( FBPromoNode *promoNode )
{
    FBPromoNodeMenu *menu;
    menu = new FBPromoNodeMenu( promoNode );
    
    if ( menu )
    {
        menu->autorelease();
        menu->MyInit();
        return menu;
    }
    return NULL;
}
//--------------------------------------------------------------------------------
FBPromoNodeMenu::FBPromoNodeMenu( FBPromoNode *promoNode )
{
    _promoNode = promoNode;
}
//--------------------------------------------------------------------------------
FBPromoNodeMenu::~FBPromoNodeMenu()
{
    removeAllChildrenWithCleanup( true );
}
//--------------------------------------------------------------------------------
void FBPromoNodeMenu::MyInit()
{
    _background = CCSprite::spriteWithFile("Images/Facebook/PromoNode/background.png");
    _background->setPosition( CCPoint( 0.0f, 0.0f ));
    _background->setIsVisible( false );
    addChild( _background, 5 );
    
    float x;
    float y;
    float yDelta;
    int count;
    
    count=0;
    x = -190.0f;
    y = -210.0f;
    yDelta = 120.0f;
    
    _buttonMap[ePromoNodeCmd_ViewHighscore]
        = FBPromoMenuItemButtons( ePromoNodeCmd_ViewHighscore,
                                 CCPoint( x, y + count++ * yDelta ),
                                 "btHighscore", this, menu_selector( FBPromoNodeMenu::Menu_ButtonPressed));

    _buttonMap[ePromoNodeCmd_ViewChallange]
        = FBPromoMenuItemButtons( ePromoNodeCmd_ViewChallange,
                                 CCPoint( x, y + count++ * yDelta ),
                                 "btChallange", this, menu_selector( FBPromoNodeMenu::Menu_ButtonPressed));
    
    _buttonMap[ePromoNodeCmd_ViewStats]
        = FBPromoMenuItemButtons( ePromoNodeCmd_ViewStats,
                                 CCPoint( x, y + count++ * yDelta ),
                                 "btStats", this, menu_selector( FBPromoNodeMenu::Menu_ShowStats ));
    
    _buttonMap[ePromoNodeCmd_FacebookLogin]
        = FBPromoMenuItemButtons( ePromoNodeCmd_FacebookLogin,
                                 CCPoint( x, y + 0 * yDelta ),
                                 "btFacebook", this, menu_selector( FBPromoNodeMenu::Menu_FacebookLogin));
    
//    _emitter = Tools::GetFacebookEmitter();
//    _emitter->setPosition( -170.0f, -210.0f );
//    addChild( _emitter , -5 );

    
    _menu = CCMenu::menuWithItem(NULL);
    _menu->setPosition( CCPoint( 0.0f, 0.0f ));
    _menu->setIsTouchEnabled( false );
    _menu->setIsVisible( false );
    addChild( _menu, 10 );
    
    for ( FBPromoMenuItemButtonMap::iterator it = _buttonMap.begin(); it != _buttonMap.end(); it++ )
    {
        it->second._buttonOpen->setOpacity( 0 );
        it->second._buttonDisabled->setOpacity( 0 );
        it->second._buttonClosed->setOpacity( 0 );
        
        _menu->addChild( it->second._buttonOpen , 10 );
        _menu->addChild( it->second._buttonDisabled , 10 );
        _menu->addChild( it->second._buttonClosed , 10 );
    }

    UpdateButtons();
}
//--------------------------------------------------------------------------------
void FBPromoNodeMenu::Show()
{
    _menu->setIsTouchEnabled( true );
    _menu->setIsVisible( true );

    float dur = 0.5f;

    _background->setOpacity( 0 );
    _background->setIsVisible( true );
    
    CCAction *seq;
    seq = (CCAction*) CCSequence::actions(
                                          CCFadeTo::actionWithDuration(dur, 255 ),
                                          NULL );
    
    _background->stopAllActions();
    _background->runAction( seq );


    for ( FBPromoMenuItemButtonMap::iterator it = _buttonMap.begin(); it != _buttonMap.end(); it++ )
    {
        it->second._buttonOpen->stopAllActions();
        it->second._buttonDisabled->stopAllActions();
        it->second._buttonClosed->stopAllActions();

        it->second._buttonOpen->runAction( CCFadeTo::actionWithDuration( dur, 255 ));
        it->second._buttonDisabled->runAction( CCFadeTo::actionWithDuration( dur, 255 ));
        it->second._buttonClosed->runAction( CCFadeTo::actionWithDuration( dur, 255 ));
    }

    UpdateButtons();

    AnyRequestNotify *notify;
    notify = AnyRequestNotify::Create();
    notify->setPosition( CCPoint( -220.0f,  74.0f ));
    addChild( notify, 60 );
}
//--------------------------------------------------------------------------------
void FBPromoNodeMenu::Shown()
{
    _promoNode->MenuShown();
}
//--------------------------------------------------------------------------------
void FBPromoNodeMenu::Hide()
{
    HideAllButtons();
}
//--------------------------------------------------------------------------------
void FBPromoNodeMenu::Menu_ShowStats( CCObject *object )
{
    RLOG_SS("MENU_LEVEL_FB", "SHOW_STATS", "LOADING");
    
    FBRequestMenu_LevelMenu *menu;
    menu = FBRequestMenu_LevelMenu::Create();
    menu->setPosition( CCPointZero );
    CCDirector::sharedDirector()->getRunningScene()->addChild( menu, 250 );
}
//--------------------------------------------------------------------------------
void FBPromoNodeMenu::Menu_ButtonPressed( CCObject *object )
{
    FBPromoNodeViewType mode;
    mode = (FBPromoNodeViewType)(long) ((CCMenuItem*)object)->getUserData();
    
    _promoNode->SetViewMode( mode );
    UpdateButtons();
}
//--------------------------------------------------------------------------------
void FBPromoNodeMenu::Menu_FacebookLogin( CCObject* object )
{
    FBTool::Get()->Login( this );
}
//--------------------------------------------------------------------------------
void FBPromoNodeMenu::UpdateButtons()
{
    FBPromoNodeViewType currentViewMode;
    FBPromoNodeViewType buttonCmd;
    
    currentViewMode = _promoNode->GetViewMode();

    //-------------
    // Logged to fb
    if ( FBTool::Get()->IsLogged() )
    {
//        _emitter->stopSystem();
        FBPromoMenuItemButtons *button;
        for ( FBPromoMenuItemButtonMap::iterator it = _buttonMap.begin(); it != _buttonMap.end(); it++ )
        {
            buttonCmd = it->first;
            button = &(it->second);
            
            if ( buttonCmd == ePromoNodeCmd_FacebookLogin )
            {
                button->Disable();
            }
            else if ( currentViewMode == buttonCmd )
                button->SetOpenButton();
            else
                button->SetClosedButton();
        }
    }
    else
    //------------
    // Not logged
    {
        for ( FBPromoMenuItemButtonMap::iterator it = _buttonMap.begin(); it != _buttonMap.end(); it++ )
        {
            if ( it->first == ePromoNodeCmd_FacebookLogin )
            {
                it->second.SetClosedButton();
//                _emitter->resetSystem();
            }
            else if ( it->first == ePromoNodeCmd_ViewHighscore )
                it->second.Disable();
           else
                it->second.SetDisabledButton();
        }
    }
}
//--------------------------------------------------------------------------------
void FBPromoNodeMenu::FBRequestCompleted( FBTool::Request requestType, void *data )
{
#ifdef BUILD_TEST
//    CCLabelTTF *label;
//    label = CCLabelTTF::labelWithString( "Completed", Config::FBHighScoreFont, 30 );
//    addChild( label, 100 );
#endif
    
    if ( requestType == FBTool::eReq_Login )
    {
        _promoNode->Hide();
        _promoNode->SetViewMode( ePromoNodeCmd_ViewHighscore, true );
        UpdateButtons();
    }
}
//--------------------------------------------------------------------------------
void FBPromoNodeMenu::FBRequestError( int error )
{
#ifdef BUILD_TEST
//    CCLabelTTF *label;
//    label = CCLabelTTF::labelWithString( "Connection Problem", Config::FBHighScoreFont, 30 );
//    addChild( label, 100 );
#endif
    
    _promoNode->Hide();
    _promoNode->SetViewMode( ePromoNodeCmd_ViewHighscore, true );
    UpdateButtons();
}
//--------------------------------------------------------------------------------
void FBPromoNodeMenu::HideAllButtons()
{
    FBPromoMenuItemButtons *button;
    
//    _emitter->stopSystem();
    for ( FBPromoMenuItemButtonMap::iterator it = _buttonMap.begin(); it != _buttonMap.end(); it++ )
    {
        button = &(it->second);
        button->Disable();
    }
}




//--------------------------------------------------------------------------------
// FB Promo Node
//--------------------------------------------------------------------------------
FBPromoNode* FBPromoNode::Create( LevelEnum level )
{
    FBPromoNode* node = new FBPromoNode( level );
    if ( node )
    {
        node->autorelease();
        node->Init();
    }
    return node;
}
//--------------------------------------------------------------------------------
FBPromoNode::FBPromoNode( LevelEnum level )
{
    _level = level;
    _viewMode = ePromoNodeCmd_Hidden;
    
    _menu = NULL;
    _highscoreMenu = NULL;
    _challangeMenu = NULL;
    _challangeMenu      = NULL;
}
//--------------------------------------------------------------------------------
FBPromoNode::~FBPromoNode()
{
    
}
//--------------------------------------------------------------------------------
void FBPromoNode::Init()
{
    float x;
    
    x = -115.0f;
    
    //---------------
    // Main  menu
    _menu = FBPromoNodeMenu::Create(this);
    _menu->setPosition(CCPoint( x, 0.0f));
    addChild( _menu, 5 );
    
    //---------------
    // High score menu
    _highscoreMenu = FBScoreMenu::Create(_level);
    _highscoreMenu->Hide();
    _highscoreMenu->setPosition(CCPoint( x - 137.0f, 0.0f ));
    addChild( _highscoreMenu, 10 );
    
    //---------------
    // Challange Menu
    _challangeMenu = FBChallangeMenu::Create(_level);
    _challangeMenu->Hide();
    _challangeMenu->setPosition( CCPoint( x - 137.0f, 0.0f ));
    addChild( _challangeMenu, 11 );
    
    UpdateContent();
}
//--------------------------------------------------------------------------------
void FBPromoNode::Hide()
{
    _menu->HideAllButtons();
    _highscoreMenu->Hide();
    _challangeMenu->Hide();
}
//--------------------------------------------------------------------------------
void FBPromoNode::Show()
{
    _menu->Show();
}
//--------------------------------------------------------------------------------
void FBPromoNode::MenuShown()
{
    SetViewMode( ePromoNodeCmd_ViewHighscore, true );
}
//--------------------------------------------------------------------------------
FBPromoNodeViewType FBPromoNode::GetViewMode()
{
    return _viewMode;
}
//--------------------------------------------------------------------------------
void FBPromoNode::SetViewMode( FBPromoNodeViewType mode, bool force )
{
    if ( mode == _viewMode && ! force )
        return;
    
    _viewMode = mode;
    UpdateContent();
}
//--------------------------------------------------------------------------------
void FBPromoNode::UpdateContent()
{
    switch ( _viewMode )
    {
        case ePromoNodeCmd_Hidden :
            _highscoreMenu->Hide();
            _challangeMenu->Hide();
            break;
            
        case ePromoNodeCmd_ViewHighscore :
            _challangeMenu->Hide();
            _highscoreMenu->Show();
            break;

        case ePromoNodeCmd_ViewChallange :
        case ePromoNodeCmd_ViewStats :
            _highscoreMenu->Hide();
            _challangeMenu->Show();
            break;

        default:
            _challangeMenu->Hide();
            _highscoreMenu->Hide();
            break;
    }
}