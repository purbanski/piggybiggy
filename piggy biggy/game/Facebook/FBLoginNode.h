#ifndef __FBLOGINNODE_H__
#define __FBLOGINNODE_H__

#include <cocos2d.h>
//------------------------------------------------------
USING_NS_CC;

//------------------------------------------------------
class FBLoginNode : public CCNode
{
public:
    static FBLoginNode* Create();
    static FBLoginNode* LastUsed();
    
    ~FBLoginNode();
    
    void Menu_Login( CCObject* pSender );
    void Menu_Logout( CCObject* pSender );
    void UpdateButtons( bool init = false );
    
private:
    FBLoginNode();
    void Init();
    void Step( ccTime dTime );
    
	typedef void ( FBLoginNode::*menuFnPtr )( CCObject *sender );
    CCMenuItem* GetMenuItem( const char *buttonName, menuFnPtr fn );

private:
    static FBLoginNode *_sLastUsed;
    CCMenuItem *_fbLogin;
    CCMenuItem *_fbLogout;
//    CCParticleSystem *_emitter;
    ccTime      _lastSchedule;
    
    typedef enum
    {
        eMode_LoggedOut = 0,
        eMode_LoggedIn
    } FBMode;
    
    FBMode _mode;
};
//------------------------------------------------------

#endif