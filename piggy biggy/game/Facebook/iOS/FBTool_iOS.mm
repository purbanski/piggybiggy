#import "FBTool_iOS.h"
#import "ToolBox_iOS.h"
#import "Lang.h"
#import <FacebookSDK/FacebookSDK.h>
#import <FacebookSDK/FBSessionTokenCachingStrategy.h>
#import "Game.h"
#import "GameConfig.h"
#import "GameGlobals.h"
#import "FBTool.h"
#include <map>
#include "Platform/ScoreTool.h"
#include "Promotion/FBRequestsMap.h"
#include "RunLogger/RunLogger.h"

static FBTool_iOS *gFBTool = NULL;

typedef std::pair<FBTool::Request,FBToolListener*> RequestData;
typedef std::map<FBRequestConnection*, RequestData> RequestMap;

static RequestMap gListeners;

//------------------------------------------------------------------------
// FBTool_iOS
//------------------------------------------------------------------------
@implementation FBTool_iOS

@synthesize postParams          = _postParams;
@synthesize postActivityParams  = _postActivityParams;

@synthesize username        = _username;
@synthesize fbId            = _fbId;
@synthesize profileImageURL = _profileImageURL;
@synthesize fbFirstname     = _fbFirstname;
@synthesize fbLastname      = _fbLastname;
@synthesize fbLocale        = _fbLocale;

@synthesize level           = _level;
@synthesize imgURL          = _imgURL;

//------------------------------------------------------------------------
// statics
//------------------------------------------------------------------------
+ (FBTool_iOS *)sharedInstance
{
  if ( ! gFBTool )
    {
        gFBTool = [[ FBTool_iOS alloc] init ];
    }
    
    return gFBTool;
}
//------------------------------------------------------------------------
+ (void) destroy
{
    if ( gFBTool )
        [ gFBTool dealloc ];
    
    gFBTool = NULL;
}
//------------------------------------------------------------------------
// methods
//------------------------------------------------------------------------
- (void)dealloc
{
    gListeners.clear();
    [super dealloc];
}
//------------------------------------------------------------------------
- (id)init
{
    self.postParams = nil;
    self.postActivityParams = nil;
    
    [FBSettings setLoggingBehavior:[NSSet setWithObjects:FBLoggingBehaviorFBRequests, FBLoggingBehaviorFBURLConnections, nil]];
    return [ super init ];
}
//-----------------------------------------------------------------------------------------------
- (BOOL)openURL:(NSURL *)url
{
    // Facebook SDK * login flow *
    // Attempt to handle URLs to complete any auth (e.g., SSO) flow.
    if ([[FBSession activeSession] handleOpenURL:url])
    {
        return YES;
    }
    else
    {
        // Facebook SDK * App Linking *
        // For simplicity, this sample will ignore the link if the session is already
        // open but a more advanced app could support features like user switching.
        // Otherwise extract the app link data from the url and open a new active session from it.
        NSString *appID;
        FBAccessTokenData *appLinkToken;
        
        appID = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"FacebookAppID"];
        appLinkToken = [FBAccessTokenData createTokenFromFacebookURL:url
                                                               appID:appID
                                                     urlSchemeSuffix:nil];
        if (appLinkToken)
        {
            if ([FBSession activeSession].isOpen)
            {
                NSLog(@"INFO: Ignoring app link because current session is open.");
            }
            else
            {
                [self handleAppLink:appLinkToken];
                return YES;
            }
        }
    }
    return NO;
}
//-----------------------------------------------------------------------------------------------
// Helper method to wrap logic for handling app links.
- (void)handleAppLink:(FBAccessTokenData *)appLinkToken
{
    // Initialize a new blank session instance...
    FBSession *appLinkSession = [[FBSession alloc] initWithAppID:nil
                                                     permissions:nil
                                                 defaultAudience:FBSessionDefaultAudienceNone
                                                 urlSchemeSuffix:nil
                                              tokenCacheStrategy:[FBSessionTokenCachingStrategy nullCacheInstance] ];
    
    [FBSession setActiveSession:appLinkSession];
    // ... and open it from the App Link's Token.
    [appLinkSession openFromAccessTokenData:appLinkToken
                          completionHandler:^(FBSession *session, FBSessionState status, NSError *error) {
                              // Forward any errors to the FBLoginView delegate.
                              if (error)
                              {
                                  //[self.loginViewController loginView:nil handleError:error];
                              }
                          }];
}
//-----------------------------------------------------------------------------------------------
-(void)testPost1
{
     [FBRequestConnection startForPostStatusUpdate:@"message"
                                 completionHandler:^(FBRequestConnection *connection, id result, NSError *error)
                                {
                                    [ToolBox_iOS debugMsgBox:@"message" message:result error:error];
                                }
      ];
}
//-------------------------------------------------------------------------------------------
-(void)statusUpdate:(const char *)msg
{
    if ( ! [self isLogged] )
        return;

    if ([FBSession.activeSession.permissions indexOfObject:@"publish_actions"] == NSNotFound)
    {
        [self requestPermissionAndPost:msg ];
    } else
    {
        [self doStatusUpdate:msg];
    }
}
//-------------------------------------------------------------------------------------------
-(void)screenshotUpload:(UIImage *)image msg:(const char *)msg
{
    if ( ! [self isLogged] )
        return;

    if ([FBSession.activeSession.permissions indexOfObject:@"publish_actions"] == NSNotFound)
    {
        [self requestPermissionAndUpload:image msg:msg ];
    } else
    {
        [self doScreenshotUpload:image msg:msg];
    }
}
//-------------------------------------------------------------------------------------------
-(void)postActivity:(const char*)activity url:(const char*)url
{
    NSString *graphPath;
    NSMutableDictionary<FBGraphObject>  *action;

    //D_CSTRING(url);
    action = [FBGraphObject graphObject];
    
    NSString *urlstr = [NSString stringWithFormat:@"%s", url];
    NSLog(@"%s", [urlstr UTF8String]);
    
    action[@"level"] = urlstr;
    graphPath = [NSString stringWithFormat:@"me/fb_piggybiggy:%s", activity];

    [FBRequestConnection startForPostWithGraphPath:graphPath
                                       graphObject:action
                                 completionHandler:^(FBRequestConnection *connection,
                                                     id result,
                                                     NSError *error)
    {
        if ( ! error )
        {
            RLOG_SS("FB","ACTIVITY","POSTING_OK");
//            [ToolBox_iOS debugMsgBox:@"Custom post" message:@"Posted" error:error ];
        }
        else
        {
            RLOG_SS("FB","ACTIVITY","POSTING_DISABLED");
//            [self presentAlertForError:error];
        }
    }];
}
//-------------------------------------------------------------------------------------------
- (void)postUserAchievement
{
    if ( ! [self isLogged] )
        return;
    
    if ([FBSession.activeSession.permissions indexOfObject:@"publish_actions"] == NSNotFound)
    {
        [self requestPermissionAndDo:@selector(doPostUserAchievement) object:self];
    } else
    {
        [self doPostUserAchievement ];
    }
}
//-------------------------------------------------------------------------------------------
- (void)sendRequest:(const char *)fbId
              title:(const char *)title
            message:(const char *)message
               data:(const char*)data
{
    NSMutableDictionary *param;
    
    if ( data )
    {
        param = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
             [NSString stringWithUTF8String:fbId ], @"to",
             [NSString stringWithUTF8String:data ], @"data",
             nil];
        
        typedef vector<string> Strings;
        Strings strs;
        
        Tools::StringSplit(data, ':', strs );
        
        RLOG_SSS("FACEBOOK_CHALLENGE", "REQUEST", strs[0].c_str(), strs[1].c_str() );
    }
    else
    {
        param = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
             [NSString stringWithUTF8String:fbId ], @"to",
             nil];
        
        RLOG_S("FACEBOOK_CHALLENGE", "REQUEST");
    };
    
    [ FBWebDialogs presentRequestsDialogModallyWithSession:nil
                                                   message:[NSString stringWithFormat:@"%s", message ]
                                                     title:[NSString stringWithFormat:@"%s", title ]
                                                parameters:param
                                                   handler:^(FBWebDialogResult result, NSURL *resultURL, NSError *error)
     {
         if (error)
         {
             RLOG_S("FACEBOOK_CHALLENGE", "FAILED");
         }
         else
         {
             if (result == FBWebDialogResultDialogNotCompleted)
             {
                 RLOG_S("FACEBOOK_CHALLENGE", "FAILED");
             }
             else
             {
                 // Case C: Dialog shown and the user clicks Cancel or Send
                 NSDictionary *urlParams ;
                 NSArray *pairs = [ [resultURL query] componentsSeparatedByString:@"&"];
                 NSMutableDictionary *params = [[NSMutableDictionary alloc] init];

                 for (NSString *pair in pairs) {
                     NSArray *kv = [pair componentsSeparatedByString:@"="];
                     NSString *val =
                     [[kv objectAtIndex:1]
                      stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                     
                     [params setObject:val forKey:[kv objectAtIndex:0]];
                     NSLog(@"%@ %@", val, [kv objectAtIndex:0]);
                 }

                 urlParams = params;
                 
                 if (![urlParams valueForKey:@"request"])
                 {
                     // User clicked the Cancel button
                     RLOG_S("FACEBOOK_CHALLENGE", "CANCELED");
                     NSLog(@"User canceled request.");
                 }
                 else
                 {
                     NSArray *keys = [urlParams allKeys];
                     id aKey = [keys objectAtIndex:1];
                     FBTool::Get()->Request_SetNotify( [[urlParams objectForKey: aKey] UTF8String] );
                     RLOG_S("FACEBOOK_CHALLENGE", "SENT");
//                     NSString *requestID = [urlParams valueForKey:@"request"];
                 }
                 
                [params release];
             }
         }
     }];
    
    [param release];
}
//-------------------------------------------------------------------------------------------
- (void)postCustomPost:(const char *)link
              imageURL:(const char *)imageURL
                  name:(const char *)name
               caption:(const char *)caption
           description:(const char *)description

{
    if ( ! [self isLogged] )
        return;
 
    if ( self.postParams == nil )
        [ self.postParams release ];
    
    self.postParams =
        [[NSMutableDictionary alloc] initWithObjectsAndKeys:
         [NSString stringWithUTF8String:link ],         @"link",
         [NSString stringWithUTF8String:imageURL ],     @"picture",
         [NSString stringWithUTF8String:name ],         @"name",
         [NSString stringWithUTF8String:caption ],      @"caption",
         [NSString stringWithUTF8String:description ],  @"description",
         nil];

    if ([FBSession.activeSession.permissions indexOfObject:@"publish_actions"] == NSNotFound)
    {
        [self requestPermissionAndDo:@selector(doPostCustomPost) object:self];
    } else
    {
        [self doPostCustomPost ];
    }
}
//-------------------------------------------------------------------------------------------
-(void)postScore:(int)score
{
    // -- check permission mission
    NSLog(@"Score: %i", score );
    NSMutableDictionary* params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                              [NSString stringWithFormat:@"%d", score], @"score",
                              nil];

    [FBRequestConnection startWithGraphPath:[NSString stringWithFormat:@"%s/scores", self.fbId.UTF8String ]
                                 parameters:params
                                 HTTPMethod:@"POST"
                          completionHandler:^(FBRequestConnection *connection, id result, NSError *error)
    {
        if ( ! error )
        {
            RLOG_SS("FB","SCORE","POSTING_OK");
        }
        else
        {
            RLOG_SS("FB","SCORE","POSTING_DISABLED");
        }
        
//        [ToolBox_iOS debugMsgBox:@"debug"
//                         message:[NSString stringWithFormat:@"post score: %i", score]
//                           error:error ];
    }];
}
//-------------------------------------------------------------------------------------------
-(void)readAllRequests:(FBToolListener *)listener
{
    FBRequestConnection *request;
    request = [FBRequestConnection startWithGraphPath:@"me/apprequests"
                                           parameters:nil
                                           HTTPMethod:@"GET"
                                    completionHandler:^(FBRequestConnection *connection, id result, NSError *error)
    {
        if ( !error )
            [self readAllRequestsProcess:connection result:result];
        else
            [self requestError:connection error:error];
        
        [ToolBox_iOS debugMsgBox:@"debug"
                         message:@"getting requests"
                           error:error ];

    }];
    
    [self storeListener:request requestType:FBTool::eReq_ReadAllRequests listener:listener];
}
//-------------------------------------------------------------------------------------------
-(void)readAllRequestsProcess:(FBRequestConnection *)connection
                    result:(id)result
{
    FBRequestsMap requestMap;
    
    for ( id userData in [result objectForKey:@"data"] )
    {
        NSString *objId         = [userData valueForKey:@"id"];
        NSString *fromUsername  = [[userData valueForKey:@"from"] valueForKey:@"name" ];
        NSString *fromUserFbId  = [[userData valueForKey:@"from"] valueForKey:@"id" ];
        NSString *message       = [userData valueForKey:@"message"] ;
        NSString *data          = [userData valueForKey:@"data"] ;
        NSString *createDate    = [userData valueForKey:@"created_time"];
        
        NSLog(@"%@", data );
        
        requestMap.Add(
                       [objId UTF8String],
                       [fromUsername UTF8String],
                       [fromUserFbId UTF8String],
                       [createDate UTF8String],
                       [message UTF8String],
                       [data UTF8String]
                       );
    }
        
    requestMap.Dump();
    [self notifyRequestComplete:connection data:(void*)&requestMap];
}
//-------------------------------------------------------------------------------------------
-(void)deleteRequest:(const char *)requestId listener:(FBToolListener*)listener
{
    FBRequestConnection *request;
    request = [FBRequestConnection startWithGraphPath:[NSString stringWithFormat:@"/%s", requestId]
                                           parameters:nil
                                           HTTPMethod:@"DELETE"
                                    completionHandler:^(FBRequestConnection *connection, id result, NSError *error)
    {
        if ( !error )
            [self notifyRequestComplete:connection data:nil];
        else
            [self requestError:connection error:error];
        
        [ToolBox_iOS debugMsgBox:@"debug"
                         message:@"delete request"
                           error:error ];

    }];
    
    [self storeListener:request requestType:FBTool::eReq_DeleteRequest listener:listener];
}
//-------------------------------------------------------------------------------------------
-(void)readScore:(FBToolListener *)listener
{
    FBRequestConnection *request;
    request = [FBRequestConnection startWithGraphPath:[NSString stringWithFormat:@"%s/scores", Config::FBAppId ]
                                           parameters:nil
                                           HTTPMethod:@"GET"
                                    completionHandler:^(FBRequestConnection *connection, id result, NSError *error)
    {
        if ( !error )
            [self readScoreProcess:connection result:result];
        else
            [self requestError:connection error:error];
        
        [ToolBox_iOS debugMsgBox:@"debug"
                         message:[NSString stringWithFormat:@"read score: %i", 3]
                           error:error ];

    }];
    
    [self storeListener:request requestType:FBTool::eReq_ReadScore listener:listener];
}
//-------------------------------------------------------------------------------------------
-(void)readScoreProcess:(FBRequestConnection *)connection result:(id)result
{
    FBScoreList scoreList;
    for ( id userData in [result objectForKey:@"data"] )
    {
        NSInteger score     = [[userData valueForKey:@"score"] integerValue];
        NSString *username  = [[userData valueForKey:@"user"] valueForKey:@"name" ];
        NSString *userId    = [[userData valueForKey:@"user"] valueForKey:@"id" ];
            
        scoreList.Add(score, [username UTF8String], [userId UTF8String] );
    }
    
#ifdef DEBUG
    scoreList.Dump();
#endif
    [self notifyRequestComplete:connection data:(void*)&scoreList];
}
//-------------------------------------------------------------------------------------------
- (void)requestPermissionAndPost:(const char*)msg
{
    [FBSession.activeSession requestNewPublishPermissions:[NSArray arrayWithObject:@"publish_actions"]
                                          defaultAudience:FBSessionDefaultAudienceEveryone
                                        completionHandler:^(FBSession *session, NSError *error)
    {
        if (!error)
        {
            [self doStatusUpdate:msg];
        }
        else
        {
            [self presentAlertForError:error];
        }
    }];
}
//-------------------------------------------------------------------------------------------
- (void)requestPermissionAndUpload:(UIImage*)image msg:(const char*)msg
{
    [FBSession.activeSession requestNewPublishPermissions:[NSArray arrayWithObject:@"publish_actions"]
                                          defaultAudience:FBSessionDefaultAudienceEveryone
                                        completionHandler:^(FBSession *session, NSError *error)
    {
        if (!error)
        {
            [self doScreenshotUpload:image msg:msg];
        }
        else
        {
            [self presentAlertForError:error];
        }
    }];
}
//-------------------------------------------------------------------------------------------
- (void)requestPermissionAndDo:(SEL)func object:(id)object
{
    [FBSession.activeSession requestNewPublishPermissions:[NSArray arrayWithObject:@"publish_actions"]
                                          defaultAudience:FBSessionDefaultAudienceEveryone
                                        completionHandler:^(FBSession *session, NSError *error)
    {
        if (!error)
        {
            [object performSelector:func];
        }
        else
        {
            [self presentAlertForError:error];
        }
    }];
}

//-------------------------------------------------------------------------------------------
- (void)doStatusUpdate:(const char*)msg
{
    [FBRequestConnection startForPostStatusUpdate:[NSString stringWithUTF8String:msg]
                                completionHandler:^(FBRequestConnection *connection,
                                                    id result,
                                                    NSError *error)
    {
        if ( ! error )
            [ToolBox_iOS debugMsgBox:@"Title" message:@"status update" error:error ];
        else
            [self presentAlertForError:error];
    }];
}
//-------------------------------------------------------------------------------------------
- (void)doScreenshotUpload:(UIImage*)image msg:(const char*)msg
{
    [FBRequestConnection startForUploadPhoto:image
                           completionHandler:^(FBRequestConnection *connection,
                                               id result,
                                               NSError *error)
    {
        if ( ! error )
            [ToolBox_iOS debugMsgBox:@"Title" message:@"screen shot upload" error:error ];
        else
            [self presentAlertForError:error];
    }];
}
//-------------------------------------------------------------------------------------------
-(void)doPostUserAchievement
{
    NSMutableDictionary* params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                   @"http://wwww.piggybiggy.com/facebook/ogp/test.php",
                                   @"achievement",
                                   nil];

    [FBRequestConnection startWithGraphPath:[NSString stringWithFormat:@"%s/achievements", Config::FBAppId ]
                                 parameters:params
                                 HTTPMethod:@"POST"
                          completionHandler:^(FBRequestConnection *connection, id result, NSError *error)
    {
        [ToolBox_iOS debugMsgBox:@"debug"
                         message:@"reg achivment"
                           error:error ];
    }];
}
//-------------------------------------------------------------------------------------------
-(void)doPostCustomPost
{
    [FBRequestConnection startWithGraphPath:@"me/feed"
                                 parameters:self.postParams
                                 HTTPMethod:@"POST"
                          completionHandler:^(FBRequestConnection *connection,
                                              id result,
                                              NSError *error)
   {
        if ( ! error )
            [ToolBox_iOS debugMsgBox:@"Custom post" message:@"Posted" error:error ];
        else
            [self presentAlertForError:error];
    }];
 }
//-------------------------------------------------------------------------------------------
-(void)doPostScore
{
//    [FBRequestConnection startWithGraphPath:@"me/games.highscores"
//                      completionHandler:^(FBRequestConnection *connection,
//                                          id result,
//                                          NSError *error)
//                    {
//                        [ToolBox_iOS debugMsgBox:@"debug"
//                                         message:[ NSString stringWithFormat:@"score %i", 1 ]
//                                           error:error
//                         ];
//                    }];
//    
////    NSMutableDictionary<FBGraphObject> *action = [FBGraphObject graphObject];
////    action[@"new_high_score"] = 40;
////    action[@"old_high_score"] = 39;
////    action[@"game"] = @"http://samples.ogp.me/163382137069945";
////
////    [FBRequestConnection startForPostWithGraphPath:@"me/games.highscores"
////                                       graphObject:action
////                                 completionHandler:^(FBRequestConnection *connection,
////                                                     id result,
////                                                     NSError *error)
////    {
////
////    }];
}
//-------------------------------------------------------------------------------------------
- (void) presentAlertForError:(NSError *)error
{
    [ToolBox_iOS debugMsgBox:@"Error" message:@"Error" error:error ];

//    if ( [error fberrorCategory ] == FBErrorCategoryUserCancelled )
//        return;
//    
//    // Facebook SDK * error handling *
//    // Error handling is an important part of providing a good user experience.
//    // When fberrorShouldNotifyUser is YES, a fberrorUserMessage can be
//    // presented as a user-ready message
//    if (error.fberrorShouldNotifyUser)
//    {
//        // The SDK has a message for the user, surface it.
//        [[[UIAlertView alloc] initWithTitle:@"Something Went Wrong"
//                                    message:error.fberrorUserMessage
//                                   delegate:nil
//                          cancelButtonTitle:@"OK"
//                          otherButtonTitles:nil] show];
//    }
}
//-------------------------------------------------------------------------------------------
-(void)login:(FBToolListener *)listener
{
    if ( ! [[ FBSession activeSession ] isOpen ] )
        [ FBSession openActiveSessionWithPublishPermissions:[NSArray arrayWithObjects:
                                                             @"publish_actions",
                                                             //@"user_games_activity",
                                                             @"friends_games_activity",
                                                             nil ]
                                            defaultAudience:FBSessionDefaultAudienceEveryone
                                               allowLoginUI:YES
                                          completionHandler:^(FBSession *session,
                                                              FBSessionState status,
                                                              NSError *error )
     {
         [ FBSession setActiveSession:session ];
         
         if ( !error )
         {
             if ( status == FBSessionStateOpen  )
             {
                [self populateUserDetails:listener ];
             }
         }
         else
         {
             //error
             RLOG_S("FACEBOOK_LOGIN","FAILED");
             if ( listener )
                 listener->FBRequestError( -1 );
         }
     }];
}
//-------------------------------------------------------------------------------------------
-(void)logout
{
    if ( [[ FBSession activeSession ] isOpen ] )
    {
        [[ FBSession activeSession] closeAndClearTokenInformation ];
        Game::Get()->GetGameStatus()->SetFbLogged( false );
    }
}
//-------------------------------------------------------------------------------------------
-(bool)isLogged
{
    if ( [[ FBSession activeSession] isOpen ] )
        return true;
    else
        return false;
}
//-------------------------------------------------------------------------------------------
- (void)populateUserDetails:(FBToolListener *)listener
{
    if (FBSession.activeSession.isOpen)
    {
        NSMutableDictionary* params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
//                                       @"picture", @"fields",
                                       @"fields", @"picture", nil];
        
        [FBRequestConnection startWithGraphPath:@"me"
                                     parameters:params
                                     HTTPMethod:@"GET"
                              completionHandler:^(FBRequestConnection *connection,
                                                  id user,
                                                  NSError *error)
        {
            if (!error)
            {
                self.username = [user valueForKey:@"user"] ;
                self.fbId = [user valueForKey:@"id"];
                self.profileImageURL = [ user valueForKey:@"picture"]; // ain't working
                self.fbFirstname = [user valueForKey:@"first_name"];
                self.fbLastname = [user valueForKey:@"last_name"];
                self.fbLocale = [user valueForKey:@"locale"];
                
                NSLog(@"fb id: %s", [[self fbId] UTF8String ] );
                NSLog(@"fb profile img url: %s", [[self profileImageURL] UTF8String ] );
                NSLog(@"fb first name: %s", [[self fbFirstname] UTF8String ] );
                NSLog(@"fb last name: %s", [[self fbLastname] UTF8String ] );
                NSLog(@"fb locale: %s", [[self fbLocale] UTF8String ] );
                NSLog(@"fb username: %s", [[self username] UTF8String ] );
                
                RLOG_SS("FACEBOOK_LOGIN","LOGGED", [[self fbId] UTF8String] );
                
                ScoreTool::Get()->PushAllScores();
                Game::Get()->GetGameStatus()->SetFbLogged( true );

                if ( listener )
                    listener->FBRequestCompleted( FBTool::eReq_Login, NULL );
            }
        }];
    }
}
//-------------------------------------------------------------------------------------------
-(std::string)getUserFirstName
{
    if ( [[FBSession activeSession ] isOpen ] )
    {
        std::string username;
        username.append( [ self.fbFirstname UTF8String ] );
        
        NSLog(@"Username: %s", username.c_str() );
        return username;
    }
    
    return (std::string(""));
}
//-------------------------------------------------------------------------------------------
-(std::string)getUsername
{
    if ( [[FBSession activeSession ] isOpen ] )
    {
        std::string username;
        const char *tmp;
        tmp =  [ self.username UTF8String ] ;
        
        NSLog(@"Username: %s", tmp );
        if ( tmp )
            username.append(tmp);
        
        return username;
    }
    
    return (std::string(""));
}
//-------------------------------------------------------------------------------------------
-(std::string)getFbId
{
    if ( [[FBSession activeSession ] isOpen ] )
    {
        std::string ret;
        const char *tmp;
        tmp =  [ self.fbId UTF8String ] ;
        
        NSLog(@"FB Id: %s", tmp );
        if ( tmp )
            ret.append(tmp);
        
        return ret;
    }
    
    return (std::string(""));
}
//-------------------------------------------------------------------------------------------
-(void)cleanUp
{
    if ( self.postParams != nil )
    {
        [ self.postParams release ];
        self.postParams = nil;
    }
}
//-------------------------------------------------------------------------------------------
-(void)cleanUpActivity
{
    if ( self.postActivityParams != nil )
    {
        [ self.postActivityParams release ];
        self.postActivityParams = nil;
    }
}
//-------------------------------------------------------------------------------------------
-(void)readFriendsList:(FBToolListener*) listener
{
    FBRequestConnection *request;
    request = [FBRequestConnection startWithGraphPath:@"me/friends"
                                           parameters:nil
                                           HTTPMethod:@"GET"
                                    completionHandler:^(FBRequestConnection *connection, id result, NSError *error)
    {
        if ( !error )
            [self readFriendsListProcess:connection result:result];
        else
            [self requestError:connection error:error];
        
        [ToolBox_iOS debugMsgBox:@"debug"
                         message:[NSString stringWithFormat:@"read score: %i", 3]
                           error:error ];

    }];

    [self storeListener:request requestType:FBTool::eReq_ReadFriends listener:listener];
}
//-------------------------------------------------------------------------------------------
-(void)readFriendsListProcess:(FBRequestConnection *)connection result:(id)result
{
    FBFriendsMap friendsMap;
       
    for ( id userData in [result objectForKey:@"data"] )
    {
        NSString *userId    = [userData valueForKey:@"id"];
        NSString *username  = [userData valueForKey:@"name"];
        
        friendsMap.Add( [userId UTF8String], [username UTF8String] );
    }
        
    friendsMap.Dump();
    [self notifyRequestComplete:connection data:(void*)&friendsMap];
}
//-------------------------------------------------------------------------------------------
-(void)readUserFirstName:(const char *)fbId listener:(FBToolListener*)listener
{
    FBRequestConnection *request;
    request = [FBRequestConnection startWithGraphPath:[NSString stringWithFormat:@"%s", fbId]
                                           parameters:nil
                                           HTTPMethod:@"GET"
                                    completionHandler:^(FBRequestConnection *connection, id result, NSError *error)
    {
        if ( !error )
        {
//            NSLog(@"%s", [[result objectForKey:@"first_name" ] UTF8String] );
            [self notifyRequestComplete:connection data: (void*)[[result objectForKey:@"first_name" ] UTF8String]];
        }
        else
            [self requestError:connection error:error];
        
        [ToolBox_iOS debugMsgBox:@"debug"
                         message:[NSString stringWithFormat:@"read user first name: %i", 3]
                           error:error ];

    }];

    [self storeListener:request requestType:FBTool::eReq_ReadUserFirstName listener:listener];
}
//-------------------------------------------------------------------------------------------
-(void)storeListener:(FBRequestConnection*)connection
         requestType:(FBTool::Request)requestType
            listener:(FBToolListener *)listener
{
    if ( listener )
        gListeners[connection] = RequestData( requestType, listener );
}
//-------------------------------------------------------------------------------------------
-(void)notifyRequestComplete:(FBRequestConnection *)connection data:(void*)data
{
    RequestMap::iterator it;
    it = gListeners.find( connection );
    
    if ( it != gListeners.end() )
    {
        it->second.second->FBRequestCompleted( it->second.first, data );
        gListeners.erase( it );
    }
}
//-------------------------------------------------------------------------------------------
-(void)requestError:(FBRequestConnection *)connection error:(NSError*)error
{
    RequestMap::iterator it;
    it = gListeners.find( connection );
        
    if ( it != gListeners.end() )
    {
        it->second.second->FBRequestError( -1 );
        gListeners.erase( it );
    }
}
//-------------------------------------------------------------------------------------------
-(void)cancelRequestForListener:(FBToolListener*)listener
{

    if ( listener )
    {
        for ( RequestMap::iterator it = gListeners.begin(); it != gListeners.end(); it++ )
        {
            if ( it->second.second == listener )
            {
                it->second.second->FBRequestError( -2 );
                gListeners.erase( it );
                break;
            }
        }
    }
}
//-------------------------------------------------------------------------------------------
@end
