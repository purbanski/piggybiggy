#import <Foundation/Foundation.h>
#import <Foundation/NSObject.h>
#import <FacebookSDK/FacebookSDK.h>
#include <string>
#include "Facebook/FBTool.h"
#include "GameTypes.h"

@interface FBTool_iOS : NSObject
//----------------------------------------------------------------
@property (strong, nonatomic) NSMutableDictionary *postParams;
@property (strong, nonatomic) NSMutableDictionary *postActivityParams;

//---------------------
// fb me data
@property (strong, nonatomic) IBOutlet NSString *fbId;
@property (strong, nonatomic) IBOutlet NSString *username;
@property (strong, nonatomic) IBOutlet NSString *profileImageURL;
@property (strong, nonatomic) IBOutlet NSString *fbFirstname;
@property (strong, nonatomic) IBOutlet NSString *fbLastname;
@property (strong, nonatomic) IBOutlet NSString *fbLocale;
//--------------------------
// post level completed
@property (nonatomic) LevelEnum level;
@property (strong, nonatomic) IBOutlet NSString *imgURL;


//@property (strong, nonatomic) long userFBid;

//----------------------------------------------------------------
+ (FBTool_iOS *)sharedInstance;
+ (void)destroy;
//----------------------------------------------------------------
-(void)dealloc;
-(BOOL)openURL:(NSURL *)url;

-(void)statusUpdate:(const char *)msg;
-(void)screenshotUpload:(UIImage *)image msg:(const char *)msg;

-(void)postActivity:(const char*)activity url:(const char*)url;

-(void)postUserAchievement;

-(void)postCustomPost:(const char *)link
             imageURL:(const char *)imageURL
                 name:(const char *)name
              caption:(const char *)caption
          description:(const char *)description;

-(void)postScore:(int)score;

-(void)sendRequest:(const char *)fbId
             title:(const char*)title
           message:(const char *)message
              data:(const char*)data;

-(void)readFriendsList:(FBToolListener*) listener;

-(void)readScore:(FBToolListener*) listener;
-(void)readAllRequests:(FBToolListener *)listener;
-(void)deleteRequest:(const char *)requestId listener:(FBToolListener*)listener;


-(void)readUserFirstName:(const char *)fbId listener:(FBToolListener*)listener;
-(void)cancelRequestForListener:(FBToolListener*)listener;

-(void)login:(FBToolListener*)listener;
-(void)logout;
-(bool)isLogged;

-(std::string)getUsername;
-(std::string)getUserFirstName;
-(std::string)getFbId;


//----------------------------------------------------------------
@end
