#ifndef __FBTOOL_H__
#define __FBTOOL_H__
//---------------------------------------------------------------------------------
#include "Promotion/FBPosts.h"
//#include "FBScoreMenu.h"
#include "GameTypes.h"

using namespace GameTypes;

//---------------------------------------------------------------------------------
class FBToolListener;
class FBTool
{
public:
    typedef enum
    {
        eReq_Unknown = 0,
        eReq_ReadScore,
        eReq_ReadFriends,
        eReq_ReadUserFirstName,
        eReq_ReadAllRequests,
        eReq_DeleteRequest,
        eReq_Login
    } Request;
    
	static FBTool* Get();
	static void Destroy();

	~FBTool();
    
    void StatusUpdate( const char *msg );
    void ScreenShotUpload( const char *msg );
    void PostCustomPost(const char *link,
                        const char *imageURL,
                        const char *name,
                        const char *caption,
                        const char *description );
    void PostCustomPost( FBPost &post );
    
    void PostScore( int score );
    
    
    ///---
    void ReadScore( FBToolListener *listener );
    void Read_UserFirstName( const char *fbId, FBToolListener *listener );
    
    void Read_FriendsList( FBToolListener *listener );
    
    void CancelRequestForListener( FBToolListener *listener );
    //---
    
    void Post_LevelCompleted( LevelEnum level, const char *imgURL );
    void Post_LevelSkipped( LevelEnum level, const char *imgURL );
    void Post_test();
    
    //----------------
    //
    void Request_LevelHelp( const char *fbId, LevelEnum levelEnum );
    void Request_LevelChallange( const char *fbId, LevelEnum levelEnum, unsigned int seconds );
    void Request_ReadAll( FBToolListener *listener );
    void Request_SetNotify( const char *fbId );
    void Request_DeleteNotify( const char *fbId );
    void Request_DeleteRequest( const char *requestId, FBToolListener *listener = NULL );
    
    void PostUserAchievement();
    
    void Login( FBToolListener *listener = NULL );
    void Logout();
    bool IsLogged();
    
    string GetFBUserId();
    string GetUsername();
    string GetUserFirstName();
    
private:
	FBTool();

private:
 	static FBTool *_sInstance;
};
//---------------------------------------------------------------------------------
class FBToolListener
{
public:
    virtual void FBRequestCompleted( FBTool::Request requestType, void *data ) = 0;
    virtual void FBRequestError( int error ) = 0;
};
//---------------------------------------------------------------------------------

    

#endif
