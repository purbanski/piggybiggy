#ifndef __piggy_biggy__FBGameUsers__
#define __piggy_biggy__FBGameUsers__

#include <vector>
#include <string>
#include "GameTypes.h"
#include <cocos2d.h>

using namespace std;
using namespace GameTypes;
USING_NS_CC;

//------------------------------------------------------------------------------
// Level User Data
//------------------------------------------------------------------------------
class LevelUserData
{
public:
    LevelUserData();
    LevelUserData( const char *fbId, GameLevelUsersType userType );
    CCMenuItem* GetMenuItem( CCObject* target, SEL_MenuHandler fn );
    
public:
    string _fbId;
    GameLevelUsersType  _levelUserType;
};


//------------------------------------------------------------------------------
// FB Game User Vector
//------------------------------------------------------------------------------
class FBFriendsMap;
class FBGameUsersVector : public vector<LevelUserData>
{
public:
    FBGameUsersVector();
    static FBGameUsersVector Create( const FBFriendsMap &friendsMap, const char *data );
    
    void PushBack( const char* fbId, GameLevelUsersType levelUserType );
    bool UserIn( const char * fbId );
    FBGameUsersVector GetWithLimits( int friendsGameInstalled, int friendsGameNotInsalled, int otherGameInstalled );
    void AddRandomValues( FBGameUsersVector src, unsigned int count );
    void Dump();
};

#endif