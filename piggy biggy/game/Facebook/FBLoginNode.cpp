#include "FBLoginNode.h"
#include "Debug/MyDebug.h"
#include "Platform/Lang.h"
#include "Components/FXMenuItem.h"
#include "FBTool.h"
#include "Game.h"
#include "Tools.h"

//------------------------------------------------------------------------------
FBLoginNode* FBLoginNode::_sLastUsed = NULL;
//------------------------------------------------------------------------------
FBLoginNode* FBLoginNode::Create()
{
    FBLoginNode* node;
    node = new FBLoginNode();
    if ( node )
    {
        node->autorelease();
        node->Init();
        _sLastUsed = node;
        return node;
    }
    return NULL;
}
//------------------------------------------------------------------------------
FBLoginNode* FBLoginNode::LastUsed()
{
    return _sLastUsed;
}
//------------------------------------------------------------------------------
FBLoginNode::FBLoginNode()
{
    _lastSchedule = 0.0f;
    _mode = eMode_LoggedOut;
}
//------------------------------------------------------------------------------
FBLoginNode::~FBLoginNode()
{
    if ( _sLastUsed == this )
        _sLastUsed = NULL;
    
    unschedule( schedule_selector( FBLoginNode::Step ));
}
//------------------------------------------------------------------------------
void FBLoginNode::Init()
{
//    _emitter = Tools::GetFacebookEmitter();
//    _emitter->setPosition( CCPoint( 0.0f, 0.0f ));
//    addChild( _emitter , 5 );
    
    _fbLogin = GetMenuItem( "fbLoginBt", &FBLoginNode::Menu_Login );
    _fbLogout = GetMenuItem( "fbLogoutBt", &FBLoginNode::Menu_Logout );
    
    _fbLogin->setPosition( CCPoint( 0.0f, 0.0f ));
    _fbLogout->setPosition( CCPoint( 0.0f, 0.0f ));
    
    CCMenu* pMenu = CCMenu::menuWithItems( _fbLogin, _fbLogout, NULL );
    pMenu->setPosition( CCPoint( 0.0f, 0.0f ));

    UpdateButtons( true );
    addChild( pMenu, 10 );

    if ( Game::Get()->GetGameStatus()->IsFbLogged() )
        FBTool::Get()->Login();
    
    schedule( schedule_selector( FBLoginNode::Step));
}
//------------------------------------------------------------------------------
CCMenuItem* FBLoginNode::GetMenuItem( const char *buttonName, menuFnPtr fn )
{
    stringstream buttonFile;
    buttonFile << "Images/Facebook/" << buttonName << ".png";

    stringstream buttonPressedFile;
    buttonPressedFile << "Images/Facebook/" << buttonName << "Pressed.png";
    
    D_LOG("%s button", buttonFile.str().c_str() );
    
    CCMenuItemSprite *menuItem;
    CCSprite* spriteNormal   = CCSprite::spriteWithFile(  buttonFile.str().c_str() );
    CCSprite* spriteSelected = CCSprite::spriteWithFile(  buttonPressedFile.str().c_str() );
    CCSprite* spriteDisabled = CCSprite::spriteWithFile(  buttonFile.str().c_str() );

    menuItem = FXMenuItemSpriteWithAnim::Create(
                                                spriteNormal,
                                                spriteSelected,
                                                spriteDisabled,
                                                this,
                                                (SEL_MenuHandler)( fn ));
    return menuItem;
}
//------------------------------------------------------------------------------
void FBLoginNode::Menu_Login( CCObject* pSender )
{
    FBTool::Get()->Login();
}
//------------------------------------------------------------------------------
void FBLoginNode::Menu_Logout( CCObject* pSender )
{
    FBTool::Get()->Logout();
}
//------------------------------------------------------------------------------
void FBLoginNode::UpdateButtons( bool init )
{
    if ( FBTool::Get()->IsLogged() && ( _mode == eMode_LoggedOut || init ))
    {
        _fbLogin->setIsEnabled( false );
        _fbLogin->runAction( CCFadeTo::actionWithDuration( 0.5f, 0 ));
        
        _fbLogout->setIsEnabled( true );
        _fbLogout->runAction( CCFadeTo::actionWithDuration( 0.5f, 255 ));
        
//        _emitter->stopSystem();
        _mode = eMode_LoggedIn;
        return;
    }
    
    if ( ! FBTool::Get()->IsLogged() && ( _mode == eMode_LoggedIn || init ))
    {
        _fbLogin->setIsEnabled( true );
        _fbLogin->runAction( CCFadeTo::actionWithDuration( 0.5f, 255 ));
        
        _fbLogout->setIsEnabled( false );
        _fbLogout->runAction( CCFadeTo::actionWithDuration( 0.5f, 0 ));

//        _emitter->resetSystem();
        _mode = eMode_LoggedOut;
        return;
    }
}
//------------------------------------------------------------------------------
void FBLoginNode::Step( ccTime dTime )
{
    _lastSchedule += dTime;
    if ( _lastSchedule < 0.6f )
        return;
    
    _lastSchedule = 0;
    UpdateButtons();
}



