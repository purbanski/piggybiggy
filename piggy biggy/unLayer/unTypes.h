#ifndef __S3ETYPES_H__
#define __S3ETYPES_H__
//-----------------------------------------------------------------------

#ifdef CC_UNDER_IOS
	typedef enum
	{
		UN_RESULT_SUCCESS = 0,
		UN_RESULT_ERROR = 1
	} unResult;
#endif


#ifdef CC_UNDER_MARMALADE
	
	#include <s3eTypes.h>
	typedef enum
	{
		UN_RESULT_SUCCESS	= S3E_RESULT_SUCCESS,
		UN_RESULT_ERROR		= S3E_RESULT_ERROR
	} unResult;
#endif

#endif
