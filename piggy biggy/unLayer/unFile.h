#ifndef __S3ELAYER_H__
#define __S3ELAYER_H__
//-----------------------------------------------------------------------
#include <sys/stat.h>
#include <dirent.h>
#include <stdio.h>
#include <string>
#include "unTypes.h"

using namespace std;
//-----------------------------------------------------------------------
#ifdef CC_UNDER_IOS
	typedef FILE    unFile;
	typedef DIR     unFileList;
#endif

#ifdef CC_UNDER_MARMALADE
	typedef s3eFile		unFile;
	typedef s3eFileList unFileList;
#endif
//-----------------------------------------------------------------------
bool        unResourceFileCheckExists( const char *filename );
bool        unDocumentFileCheckExists( const char *filename );
unFile*       unResourceFileOpen( const char *filename, const char *mode );
unFile*       unDocumentFileOpen( const char *filename, const char *mode );

unResult    unFileClose( unFile *filehandle );
long        unFileGetSize( unFile *filehandle );
int         unFileWrite(const void* buffer, unsigned int elemSize, unsigned int noElems, unFile* file);
int         unFileRead(void* buffer, unsigned int elemSize, unsigned int noElems, unFile* file);
char*       unFileReadString( void *buffer, unsigned int maxsize, unFile *file );

unResult    unFileListNext( unFileList* handle, char* filename, int filenameLen );
unFileList* unFileListDirectory(const char* dirName );
unResult    unFileListClose( unFileList* handle );


string IOSgetFilePath();
//-----------------------------------------------------------------------

#endif
