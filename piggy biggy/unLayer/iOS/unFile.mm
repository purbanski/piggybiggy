#ifdef CC_UNDER_IOS

#include "unFile.h"
#include "cocos2d.h"
#include <iostream>
#include <fstream>
#include <string>
#include <cstdio>

using namespace std;

USING_NS_CC;

//---------------------------------------------------------------------------------------
string IOSgetFilePath()
{
	// save to document folder
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	string path = [documentsDirectory UTF8String];
	path.append( "/" );

	return path;
}
//---------------------------------------------------------------------------------------
bool unResourceFileCheckExists( const char *filename )
{
    string fullpath;
    fullpath = CCFileUtils::fullPathFromRelativePath( filename );
    
    NSString* file = [NSString stringWithUTF8String:fullpath.c_str() ];
    bool fileExists = [[NSFileManager defaultManager] fileExistsAtPath:file];

    return fileExists;
}
//---------------------------------------------------------------------------------------
bool unDocumentFileCheckExists( const char *filename )
{
    string fullpath;
    fullpath = IOSgetFilePath();
    fullpath.append( filename );
    
    NSString* file = [NSString stringWithUTF8String:fullpath.c_str() ];
    bool fileExists = [[NSFileManager defaultManager] fileExistsAtPath:file];

    return fileExists;
}
//---------------------------------------------------------------------------------------
FILE* unResourceFileOpen( const char *filename, const char *mode )
{
    FILE *filehandle;
    string fullpath;
    
    fullpath = CCFileUtils::fullPathFromRelativePath( filename );
    filehandle = fopen( fullpath.c_str(), "r" );
    return filehandle;
}
//---------------------------------------------------------------------------------------
FILE* unDocumentFileOpen( const char *filename, const char *mode )
{
    FILE* filehandle;
    string fullpath;
    
    fullpath = IOSgetFilePath();
    fullpath.append(filename);
    
    filehandle = fopen( fullpath.c_str(), mode );
    return filehandle;
 }
//---------------------------------------------------------------------------------------
long unFileGetSize( FILE *filehandle )
{
    fseek( filehandle, 0L, SEEK_END);
    return ( ftell( filehandle ));
}
//---------------------------------------------------------------------------------------
unResult unFileClose( FILE *filehandle )
{
    if ( ! fclose( filehandle ))
        return UN_RESULT_SUCCESS;
    else
        return UN_RESULT_ERROR;
}
//---------------------------------------------------------------------------------------
int unFileWrite(const void* buffer, unsigned int elemSize, unsigned int noElems, unFile* file)
{
    return fwrite(buffer, elemSize, noElems, file );
}
//--------------------------------------------------------------------------------
int unFileRead(void* buffer, unsigned int elemSize, unsigned int noElems, unFile* file)
{
    return fread( buffer, elemSize, noElems, file );
}
//--------------------------------------------------------------------------------
char* unFileReadString( void *buffer, unsigned int maxsize, unFile *file )
{
    return fgets( (char*)buffer, maxsize, file );
}
//--------------------------------------------------------------------------------


//--------------------------------------------------------------------------------
// File List
//--------------------------------------------------------------------------------
unFileList* unFileListDirectory(const char* dirName)
{
    string fullpath;
    fullpath = CCFileUtils::fullPathFromRelativePath( dirName );
    
   return opendir( fullpath.c_str() );
}
//--------------------------------------------------------------------------------
unResult unFileListNext( unFileList* handle, char* filename, int filenameLen )
{
    if ( handle )
    {
        struct dirent *ep;
        ep = readdir ( handle );
        
        if ( ! ep )
            return UN_RESULT_ERROR;
        
        while ( ! strncmp( ep->d_name, ".", 1 ) ||  ! strncmp( ep->d_name, "..", 2 ))
            ep = readdir ( handle );
        
        if ( ep )
        {
            strncpy( filename, ep->d_name, filenameLen );
            return UN_RESULT_SUCCESS;
        }
    }
    
    return UN_RESULT_ERROR;
}
//--------------------------------------------------------------------------------
unResult unFileListClose( unFileList* handle )
{
    if ( ! closedir( handle ))
        return UN_RESULT_SUCCESS;
    else
        return UN_RESULT_ERROR;
}
//--------------------------------------------------------------------------------

#endif