#ifndef __UNDEBUG_H__
#define __UNDEBUG_H__
//--------------------------------------------------------------------
#include <stdio.h>
//--------------------------------------------------------------------
#ifdef CC_UNDER_IOS
    #define unAssertMsg( x, y, z )
    #define unAssert( x, y )
#endif
//----------------------------------------------
#ifdef CC_UNDER_MARMALADE
	#include <IwDebug.h>
	#define unAssertMsg( x, y, z )
	#define unAssert( x, y )

//	#define unAssertMsg(chan, expr, args) 
//// 
////		do 
////		{ IwAssertFullN(0, chan, expr, args, (NULL)); } 
////		while (0)
//
//	#define unAssert(chan, expr) 
//unAssertMsg(chan, expr, (NULL))
#endif
//----------------------------------------------
#ifndef unAssert
    #define unAssert( x, y )
#endif
//----------------------------------------------
#ifndef unAssertMsg
    #define unAssertMsg( x, y, z )
#endif
//--------------------------------------------------------------------
void unDebugOutputString( const char *msg );
//--------------------------------------------------------------------
#endif
