#include "unFile.h"
#include <s3eFile.h>
#include <cstdio>
//-----------------------------------------------------------------------------------
bool unResourceFileCheckExists( const char *filename )
{
	if ( s3eFileCheckExists( filename ))
		return true;
	else
		return false;
}
//-----------------------------------------------------------------------------------
bool unDocumentFileCheckExists( const char *filename )
{
	if ( s3eFileCheckExists( filename ))
		return true;
	else
		return false;
}
//-----------------------------------------------------------------------------------
unFile* unResourceFileOpen( const char *filename, const char *mode )
{
	return s3eFileOpen( filename, mode );
}
//-----------------------------------------------------------------------------------
unFile* unDocumentFileOpen( const char *filename, const char *mode )
{
	return s3eFileOpen( filename, mode );
}
//-----------------------------------------------------------------------------------
unResult unFileClose( unFile *filehandle )
{
	return (unResult) s3eFileClose( filehandle );
}
//-----------------------------------------------------------------------------------
long unFileGetSize( unFile *filehandle )
{
	return s3eFileGetSize( filehandle );
}
//-----------------------------------------------------------------------------------
int unFileWrite(const void* buffer, unsigned int elemSize, unsigned int noElems, unFile* file)
{
	return s3eFileWrite( buffer,  elemSize,  noElems, file );
}
//-----------------------------------------------------------------------------------
int unFileRead(void* buffer, unsigned int elemSize, unsigned int noElems, unFile* file)
{
	return s3eFileRead( buffer, elemSize, noElems, file );
}
//-----------------------------------------------------------------------------------
char* unFileReadString( void *buffer, unsigned int maxsize, unFile *file )
{
	//char *ptr;
	//ptr = fgets( (char*)buffer, maxsize, file );
	return "not implemented";
}
//-----------------------------------------------------------------------------------
unResult unFileListNext( unFileList* handle, char* filename, int filenameLen )
{
	return (unResult)s3eFileListNext( handle, filename, filenameLen);
}
//-----------------------------------------------------------------------------------
unFileList* unFileListDirectory(const char* dirName )
{
	return s3eFileListDirectory( dirName );
}
//-----------------------------------------------------------------------------------
unResult unFileListClose( unFileList* handle )
{
	 return (unResult)s3eFileListClose( handle);
}
//-----------------------------------------------------------------------------------
string IOSgetFilePath()
{
	return string("not implemented");
}
//-----------------------------------------------------------------------
