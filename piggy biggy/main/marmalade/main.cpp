#include "AppDelegate.h"
#include "Debug/MyDebug.h"
#include "ThiefCheck.h"
#include <cstdio>
#include <time.h>

int main()
{
	ThiefCheck thief;

	if ( thief.IsStolen() )
		return -1;

	srand ( time( NULL ));

#ifdef _DEBUG
	Debug::Init();
#endif

	AppDelegate* app;
	int nRet = 0;

	app = new AppDelegate;
	nRet = cocos2d::CCApplication::sharedApplication().Run();

	delete app;
	return nRet;
}
