//
//  pocket_moneyAppController.h
//  pocket money
//
//  Created by Przemek Urbanski on 12/7/12.
//  Copyright __MyCompanyName__ 2012. All rights reserved.
//

#import "RootViewController.h"
#include "GameGlobals.h"
#include "Lang.h"
#include "RunLogger/RunLogger.h"
#import <QuartzCore/QuartzCore.h>

@implementation RootViewController

/*
 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
        // Custom initialization
    }
    return self;
}
*/

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView {
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
//- (void)viewDidLoad
//{
//    [ super viewDidLoad ];
//}


/*
- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}
 
*/
//-----------------------------------------------------------------------------
// ios 5.0
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    if ( gAppDisplayRotateMode == eDisplay_LandscapeFixed || gAppDisplayRotateMode == eDisplay_PortraitFixed )
        return ( interfaceOrientation == [UIApplication sharedApplication].statusBarOrientation );
    else
        return UIInterfaceOrientationIsLandscape( interfaceOrientation );
}
//-----------------------------------------------------------------------------
// ios 6.0
- (BOOL)shouldAutorotate
{
    if ( gAppDisplayRotateMode == eDisplay_LandscapeFixed || gAppDisplayRotateMode == eDisplay_PortraitFixed )
        return false;
    else
        return true;
}
//-----------------------------------------------------------------------------
//// ios 6.0
- (NSUInteger)supportedInterfaceOrientations
{
    NSUInteger ret;
    
    switch ( gAppDisplayRotateMode )
    {
        case eDisplay_LandscapeRotating :
            ret = UIInterfaceOrientationMaskLandscape;
            break;

        case eDisplay_LandscapeFixed :
            ret = UIInterfaceOrientationMaskLandscapeRight;
            break;

        case eDisplay_PortraitFixed :
            ret = UIInterfaceOrientationMaskLandscapeRight;
            break;
    
        default:
            ret = UIInterfaceOrientationMaskLandscape;
            break;
    }

    return ret;
}
//-----------------------------------------------------------------------------
-(void)displayComposerSheet
{
    RLOG_S("FEEDBACK", "REQUEST");
    
    MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
    if ( !picker )
        return;
    
    [picker setMailComposeDelegate:self ];
    [picker setSubject:[NSString stringWithUTF8String: LocalString("EmailFeedbackSubject")]];
 
    // Set up the recipients.
    NSArray *toRecipients = [NSArray arrayWithObjects: [ NSString stringWithUTF8String: LocalString("EmailFeedbackAddress")], nil];
    [picker setToRecipients:toRecipients];

    // Fill out the email body text.
    NSString *emailBody = [NSString stringWithUTF8String:LocalString( "EmailFeedbackBody" )];
    [picker setMessageBody:emailBody isHTML:NO];
 
    // Present the mail composition interface.
    [self presentViewController:picker animated:YES completion:nil];
    [picker release]; // Can safely release the controller now.
}
//-----------------------------------------------------------------------------
// The mail compose view controller delegate method
- (void)mailComposeController:(MFMailComposeViewController *)controller
              didFinishWithResult:(MFMailComposeResult)result
              error:(NSError *)error
{
    if ( MFMailComposeResultSent == result )
    {
        RLOG_S("FEEDBACK", "SEND" );
    }
    else
    {
        RLOG_S("FEEDBACK", "CANCELLED" );
    }
    
    [self dismissViewControllerAnimated:YES completion: nil];
}
//-----------------------------------------------------------------------------
- (void)dealloc
{
    [super dealloc];
}
//-----------------------------------------------------------------------------
- (void)screenShot
{

 
}

@end
