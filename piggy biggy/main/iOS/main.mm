#import <UIKit/UIKit.h>
#include "GameGlobals.h"
#include "Platform/ToolBox.h"

//-----------------------------------------------------------------------------
int main(int argc, char *argv[])
{
    gDevice = ToolBox::GetDeviceType();
    
    NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];
    int retVal = UIApplicationMain(argc, argv, nil, @"AppController");
    [pool release];
    return retVal;
}
//-----------------------------------------------------------------------------