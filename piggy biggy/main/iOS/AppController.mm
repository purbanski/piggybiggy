#import <UIKit/UIKit.h>
#import "AppController.h"
#import "cocos2d.h"
#import "EAGLView.h"
#import "AppDelegate.h"
#import "RootViewController.h"

#include "Platform/ToolBox.h"
#include "SoundEngine.h"
#include "GameGlobals.h"
#include "GameConfig.h"
#include "Appirater.h"

#include <sstream>
#import <FacebookSDK/FacebookSDK.h>
#import <FacebookSDK/FBSessionTokenCachingStrategy.h>
#import "Facebook/iOS/FBTool_iOS.h"

#include "RunLogger/RunLogger.h"
#include "RunLogger/GLogger.h"

#ifdef BUILD_TEST
#include "Debug/DebugLogger.h"
#endif

//#import "GoogleConversionPing.h"

@implementation AppController

#pragma mark -
#pragma mark Application lifecycle

// cocos2d application instance
static AppDelegate s_sharedApplication;
static EAGLView *__glView = NULL;
static AppController *s_sharedApplicationiOS = NULL;

+ (AppController *)sharedController
{
    return s_sharedApplicationiOS;
}
//-----------------------------------------------------------------------------------------------
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
//    [GoogleConversionPing pingWithConversionId:@"981739796" label:@"tkFNCMyD0AcQlNKQ1AM" value:@"0" isRepeatable:NO];
   
    // Add the view controller's view to the window and display.
    window = [[UIWindow alloc] initWithFrame: [[UIScreen mainScreen] bounds]];
    __glView = [EAGLView viewWithFrame: [window bounds]
                                     pixelFormat: kEAGLColorFormatRGBA8
                                     depthFormat: GL_DEPTH_COMPONENT16_OES
						      preserveBackbuffer: NO
                                      sharegroup: nil
								   multiSampling: NO
								 numberOfSamples:0 ];
    
    [__glView setMultipleTouchEnabled:YES];

    // Use RootViewController manage EAGLView
    viewController = [[RootViewController alloc] initWithNibName:nil bundle:nil];
    viewController.wantsFullScreenLayout = YES;
    viewController.view = __glView;

    [ self initLoggers];
    
    // Set RootViewController to window
    if ( ToolBox::ShouldRotate90Degree() )
    {
        [window addSubview:__glView];
    }
    else
    {
        window.rootViewController = viewController;
    }
    
    [window makeKeyAndVisible];
    [[UIApplication sharedApplication] setStatusBarHidden: YES];

    //----------------
    // Set appirater
    [ Appirater setAppId:[ NSString stringWithUTF8String:Config::AppStoreAppId ]];
    [ Appirater setDaysUntilPrompt: Config::AppiraterDaysUntilPrompt ];
    [ Appirater setUsesUntilPrompt: Config::AppiraterUsesUntilPrompt ];
    [ Appirater setSignificantEventsUntilPrompt:Config::AppiraterSignificantEventsUntilPrompt ];
    [ Appirater setTimeBeforeReminding: Config::AppiraterDaysBeforeReminding ];
    [ Appirater setDebug: Config::AppiraterDebug ];
    
    [ Appirater appLaunched:YES];
    
   
    s_sharedApplicationiOS = self;
    cocos2d::CCApplication::sharedApplication().run();
    
    return YES;
}
//-----------------------------------------------------------------------------------------------
- (void)applicationWillResignActive:(UIApplication *)application
{
#ifdef BUILD_TEST
    stringstream ss;
	ss << "App become Deactive: " << ++gAppEventDeactiveCount;
	DebugLogger::Get( DebugLogger::LogNotifications )->Write( ss.str().c_str() );
#endif

    /*
     Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
     Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
     */
    
    RLOG_SS("APP","ACTIVE","RESIGN");
    
    cocos2d::CCDirector *director;
    director = cocos2d::CCDirector::sharedDirector();
    
    if ( director && ! director->isPaused() )
        director->pause();
}
//-----------------------------------------------------------------------------------------------
- (void)applicationDidBecomeActive:(UIApplication *)application
{
#ifdef BUILD_TEST
    stringstream ss;
	ss << "App become Active: " << ++gAppEventActiveCount;
	DebugLogger::Get( DebugLogger::LogNotifications )->Write( ss.str().c_str() );
#endif
    
    RLOG_S("APP","ACTIVE");
    
    [FBSession.activeSession handleDidBecomeActive];
    cocos2d::CCApplication::sharedApplication().applicationWillEnterForeground();
}
//-----------------------------------------------------------------------------------------------
- (void)applicationDidEnterBackground:(UIApplication *)application
{
#ifdef BUILD_TEST
    stringstream ss;
	ss << "App enter Background: " << ++gAppEventEnterBackgroundCount;
	DebugLogger::Get( DebugLogger::LogNotifications )->Write( ss.str().c_str() );
#endif
    
    RLOG_SS("APP","ENTER", "BACKGROUND");

    /*
     Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
     If your application supports background execution, called instead of applicationWillTerminate: when the user quits.
     */
    
    gGLBufferSwapEnabled = false;
    cocos2d::CCApplication::sharedApplication().applicationDidEnterBackground();
}
//-----------------------------------------------------------------------------------------------
- (void)applicationWillEnterForeground:(UIApplication *)application
{
#ifdef BUILD_TEST
    stringstream ss;
	ss << "App enter Foreground: " << ++gAppEventnterForegroundCount;
	DebugLogger::Get( DebugLogger::LogNotifications )->Write( ss.str().c_str() );
#endif

    RLOG_SS("APP", "ENTER", "FOREGROUND");
    
    [Appirater appEnteredForeground:YES];
    gGLBufferSwapEnabled = true;
    cocos2d::CCApplication::sharedApplication().applicationWillEnterForeground();
}
//-----------------------------------------------------------------------------------------------
- (void)applicationWillTerminate:(UIApplication *)application
{
    RLOG_S("APP","TERMINATE");
    [FBSession.activeSession close];
}
//-----------------------------------------------------------------------------------------------
- (NSUInteger)application:(UIApplication*)application supportedInterfaceOrientationsForWindow:(UIWindow*)window
{
    return UIInterfaceOrientationMaskAllButUpsideDown;
}
//-----------------------------------------------------------------------------------------------
#pragma mark -
#pragma mark Memory management

- (void)applicationDidReceiveMemoryWarning:(UIApplication *)application
{
#ifdef BUILD_TEST
    stringstream ss;
	ss << "Warning received: " << ++gAppEventMemoryWarningCount;
	DebugLogger::Get( DebugLogger::LogNotifications )->Write( ss.str().c_str() );
#endif
    
    SoundEngine::Get()->UnloadAllSounds();
    cocos2d::CCDirector::sharedDirector()->purgeCachedData();
    cocos2d::CCSpriteFrameCache::sharedSpriteFrameCache()->removeSpriteFrames();
   
    RLOG_S("APP","MEM_WARN");
}
//-----------------------------------------------------------------------------------------------
- (void)dealloc
{
    RLOG_DESTROY;
    GLogDestroy;
    
    [super dealloc];
}
//-----------------------------------------------------------------------------------------------
- (void)initLoggers
{
#ifdef REMOTE_LOG
    //-----------------------
    // Google Analytics
    GLogInit;
    
    //------------------------
    // My Logger
//    RLOG_INIT;
#endif
}
//-----------------------------------------------------------------------------------------------
- (BOOL)application:(UIApplication *)application 
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication 
         annotation:(id)annotation
{
    return [[ FBTool_iOS sharedInstance ] openURL: url ];
}
//-----------------------------------------------------------------------------------------------

@end


