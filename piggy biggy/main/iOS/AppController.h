
#import <FacebookSDK/FacebookSDK.h>

@class RootViewController;

@interface AppController : NSObject <
    UIAccelerometerDelegate,
    UIAlertViewDelegate,
    UITextFieldDelegate,
    UIApplicationDelegate,
    FBLoginViewDelegate
    >
{
    UIWindow *window;
    RootViewController	*viewController;
}

+ (AppController *)sharedController;

@end

