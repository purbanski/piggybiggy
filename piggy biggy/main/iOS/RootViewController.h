//
//  pocket_moneyAppController.h
//  pocket money
//
//  Created by Przemek Urbanski on 12/7/12.
//  Copyright __MyCompanyName__ 2012. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>

@interface RootViewController : UIViewController<MFMailComposeViewControllerDelegate>
{
}

-(void)displayComposerSheet;

@end
