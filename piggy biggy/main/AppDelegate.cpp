#include "AppDelegate.h"
#include "Scenes/MainMenuScene.h"
#include "Scenes/MainLoadingScene.h"
#include "Scenes/ComicsScene.h"
#include "Scenes/ComicsSceneLite.h"
#include "Scenes/MarmaladeScene.h"
#include "Game.h"
#include "cocos2d.h"
#include "SoundEngine.h"
#include "GameStatus.h"
#include "Platform/ToolBox.h"
#include "RunLogger/RunLogger.h"
#include "Facebook/FBLoginNode.h"
#ifdef BUILD_EDITOR
#include "Editor/GameEditor.h"
#endif

//------------------------------------------------------------------------------------
USING_NS_CC;
//------------------------------------------------------------------------------------
AppDelegate::AppDelegate()
{

}
//------------------------------------------------------------------------------------
AppDelegate::~AppDelegate()
{
    Game::Destroy();
}
//------------------------------------------------------------------------------------
bool AppDelegate::initInstance()
{
    bool bRet = false;
    do 
    {  
        bRet = true;
    } while (0);
    return bRet;
}
//------------------------------------------------------------------------------------
bool AppDelegate::applicationDidFinishLaunching()
{
    CCDirector *director = CCDirector::sharedDirector();
	
	director->setOpenGLView( &CCEGLView::sharedOpenGLView() );
    //director->enableRetinaDisplay( true );
    //director->setContentScaleFactor( 1.0f );
	director->setDepthTest( false );
	director->setAnimationInterval( 1.0 / 60 );
    director->setDeviceOrientation( CCDeviceOrientationPortrait );
	director->setDisplayFPS( false );

    
#ifdef BUILD_EDITOR
	director->setDisplayFPS( true );
	director->runWithScene( GameEditor::CreateScene() );
	GameEditor::Get()->SetEnabled( true );
    SoundEngine::Get()->GetBackgroundMusic().PauseMusic();
#else

    
#ifdef BUILD_TEST
    director->runWithScene( MainMenuScene::CreateScene() );

#else
    //--------------
    // Arcade game
    director->runWithScene( MainMenuScene::CreateScene());
//    director->runWithScene( MarmaladeScene::CreateScene());
#endif

    
#endif

	return true;
}
//------------------------------------------------------------------------------------
void AppDelegate::applicationDidEnterBackground()
{
    SoundEngine::Get()->PauseAllEffects();
    SoundEngine::Get()->PauseBackgroundMusic();
	Game::Get()->PauseGame();
    
    CCDirector::sharedDirector()->pause();
}
//------------------------------------------------------------------------------------
void AppDelegate::applicationWillEnterForeground()
{
    SoundEngine::Get()->ResumeBackgroundMusic();
	Game::Get()->ResumeGame();
 
    if ( CCDirector::sharedDirector()->isPaused() )
        CCDirector::sharedDirector()->resume();
}
//------------------------------------------------------------------------------------



