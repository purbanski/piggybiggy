![piggy-biggy-3d.png](https://bitbucket.org/repo/ayGEAd/images/1780585033-piggy-biggy-3d.png)

# Piggy Biggy #

Help Piggy Biggy collect the coins! 

This physics based adventure presents the player with wonderfully frustrating challenges using a variety of objects such as bombs, blocks and a small car while avoiding the bad piggies.

These objects must be destroyed or moved to allow Piggy Biggy access to the precious coins. The golden coins are placed tantalisingly close, yet they will seem so far away when the objects are not behaving as you want them to.

Wonderfully frustrating and wonderfully rewarding, Piggy Biggy will tease out logic, you never knew you had.

### Features:  ###
* Funny and adorable Piggy character

* Physics-based game

* Gradually more and more logically challenging 

* Facebook friend challenges

* Variety of user actions: destroy blocks, spit explosive seeds, steer a little car, explode magnetic bombs and several more..

### Download it free!  ###
[iOS release](https://itunes.apple.com/pa/app/piggy-biggy/id972278141?l=en&mt=8)

[Android release](https://play.google.com/store/apps/details?id=com.blackted.piggybiggy&hl=en)

[Behind the scenes](http://urbanski.tech/cocos2dx/)

![icon-160.png](https://bitbucket.org/repo/ayGEAd/images/1310570315-icon-160.png)